//---------------------------------------------------------------------------

#pragma hdrstop

#include "PLC_Drv.h"
//#include "Option.h"
#include "..\First\Simulate_PLC.h"
#include "..\First\Simulate_BUSENTX11.h"
#include "strconv.h"
#include "stdio.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------
// ���������� �������� ������� � �����������
//---------------------------
TLineToPLC LoadBuf;
void TPLCDriver::Handler() {
	int Div;
	if (Connected()==true) {
 //-----------------------------------------------------------------------------
		try
		{
			SliceCount++;  // ��� ���������� ��������
			// ��������� �� �����������
			ReadBiArray(M_ADDR+M_ADDR_CONTROL, (BYTE*)&M_ArrayCopy[M_ADDR+M_ADDR_CONTROL],32);

			// ��������� � ��������� �����������
			if(SimulPLC_Form) SimulPLC_Form->SetControl(M_ArrayCopy[M_ADDR+M_ADDR_CONTROL]); // ����� ���� ��������� - �������� � ���������

			// SaveParam	  ��������� ���������		 �210     (��� �����)
			if((M_ArrayCopy[M_ADDR+M_ADDR_CONTROL]&  (1 << 10))&&((M_Array[M_ADDR+M_ADDR_CONTROL]&  (1 << 10)) == 0))
			{
				ReadWArray(D_ADDR + D_ADDRWORK_KADR, &M_ArrayCopy[D_ADDR + D_ADDRWORK_KADR],20);
			}
			// AlarmOn	  �������� ������		 �208     (��� �����)
			if((M_ArrayCopy[M_ADDR+M_ADDR_CONTROL]&  (1 << 8))&&((M_Array[M_ADDR+M_ADDR_CONTROL]&  (1 << 8)) == 0))
			{
				SliceCount &= ~3;
			}
			// ����  - �������� ������     �205 - ������ ������� ������  ����
			Div =(M_ArrayCopy[M_ADDR+M_ADDR_CONTROL]& (1 << 5))?1:3;
			if((SliceCount & Div) == 0)
			{
				ReadWArray(D_ADDR + D_ADDR_PROCESS_VALUES, &M_ArrayCopy[D_ADDR + D_ADDR_PROCESS_VALUES],20);
				// ��������� � ��������� ���������� �������� �216- �231
				if(BUSINTX11_Form)
				{   // ��������� �� ����������� �� ������
					BUSINTX11_Form->SetControl(&M_ArrayCopy[D_ADDR + D_ADDR_PROCESS_RENT_X],M_ArrayCopy[M_ADDR+M_ADDR_CONTROL+1]); // ����� ���� ��������� - �������� � ���������
					// C�������� �� ����������� (�� ������)
					BUSINTX11_Form->SetStatus(&M_ArrayCopy[D_ADDR + D_ADDR_PROCESS_VALUES],M_ArrayCopy[M_ADDR+M_ADDR_CONTROL+1]);
				}
			}

			// ���������� � ������� �������
			CS_get->Enter();
			try
			{
				memcpy(&M_Array[D_ADDR+D_ADDR_KADR],&M_ArrayCopy[D_ADDR+D_ADDR_KADR],400);
				memcpy(&M_Array[M_ADDR+M_ADDR_CONTROL],&M_ArrayCopy[M_ADDR+M_ADDR_CONTROL],32);   // 64 �����
			}
			__finally {CS_get->Leave();}

		}
		catch (int e)
		{
			ErrorCount++;
		}
	}
	Delay(DelayInterval);

}

//-----------------------------------------------------------------------------
bool TPLCDriver::SetBuffer(TLineToPLC *pLoadBuf)
{
	return WriteWArray(D_ADDR+D_ADDR_KADR, (short *)pLoadBuf,(sizeof(TLineToPLC)+1)/2);
}
//-----------------------------------------------------------------------------
bool TPLCDriver::SetSysParam(sSys *SysPar)
{
	return WriteWArray(D_ADDR+D_ADDR_PARAM, (short *)SysPar,(sizeof(sSys)+1)/2);
}

//-----------------------------------------------------------------------------
bool TPLCDriver::SetControlBufBusy()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 0, 1);	// PC : 1-����� ������ �����   �200
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] |= 1<<0;
	CS_get->Leave();
	return Res;

}
//-----------------------------------------------------------------------------
bool TPLCDriver::SetControl_LasrKadr()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 4, 1);	// PC : ��������� ����         �204
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] |= 1<<4;
	CS_get->Leave();
	return Res;
}
//-----------------------------------------------------------------------------
bool TPLCDriver::SetControlPC_Err()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 14, 1);	// PC :     ������         �214
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] |= 1<<14;
	CS_get->Leave();
	return Res;
}
//-----------------------------------------------------------------------------
bool TPLCDriver::SetControlPC_Ready()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 11, 1);	// PC Ready                �211
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] |= 1<<11;
	CS_get->Leave();
	return Res;
}
//-----------------------------------------------------------------------------
bool TPLCDriver::ReSetControlPC_Ready()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 11, 0);	// PC Ready                �211
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] &= ~(1<<11);
	CS_get->Leave();
	return Res;
}

//-----------------------------------------------------------------------------
bool TPLCDriver::ClearControlSaveParam()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 10, 0);	// ��������� ���������	 �210
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] &= ~(1<<10);
	CS_get->Leave();
	return Res;
}
//-----------------------------------------------------------------------------
bool TPLCDriver::SetControlPause()
{
	bool Res = WriteBit(M_ADDR+M_ADDR_CONTROL + 9, 1);	// PC : ������� �� �����       �209
	CS_get->Enter();
		M_Array[M_ADDR+M_ADDR_CONTROL] |= 1<<9;
	CS_get->Leave();
	return Res;
 }
//-----------------------------------------------------------------------------
TPosStruct TPLCDriver::GetPosition()
{
	CS_get->Enter();
	TPosStruct PosStruct = *(TPosStruct *)&M_Array[D_ADDR + D_ADDR_PROCESS_FEED_VALUES]; //	340   ����� ������� ������ (��������������� 40 ����)
	CS_get->Leave();
	return  PosStruct;
}
//-----------------------------------------------------------------------------
short TPLCDriver::GetErrorPLC()
{
	CS_get->Enter();
	short Err = M_Array[D_ADDR + D_ADDR_PROCESS_VALUES+0]; //	340   ����� ������� ������ (��������������� 40 ����)
	CS_get->Leave();
	return Err;

}

//-----------------------------------------------------------------------------
DWORD TPLCDriver::GetProsStatusData()
{
	CS_get->Enter();
	DWORD V = M_Array[M_ADDR+M_ADDR_CONTROL];  // ����� �������u�
	V |= (M_Array[D_ADDR + 742]<<16);          // ������� ����
	CS_get->Leave();
	return V;

}
//-----------------------------------------------------------------------------
int TPLCDriver::GetProsStatus()
{
	CS_get->Enter();
	TBitStatusByte CurBitStatusByte = *(TBitStatusByte *)&M_Array[M_ADDR+M_ADDR_CONTROL];  // ����� �������u�

	CS_get->Leave();

	if((CurBitStatusByte.AlarmOn) && (PredBitStatusByte.AlarmOn == 0))	  // ������ - �����  ���� ��������� ������
	{
		RobotError = M_Array[D_ADDR + 740];          					  // ������
		if(RobotError >= DAMAGE_ERR)
			Status = robERROR;
	}else
	{
//		if((PredBitStatusByte.AlarmOn) && (CurBitStatusByte.AlarmOn == 0))// ���������� ������ - �����  ���� ���������� � ������� ������

		if(CurBitStatusByte.DriveReady==0)
			Status = robNOT_READY;                                        // ������� ���������
		else // ���� DriveReady == 1
		if(PredBitStatusByte.DriveReady==0)                               // ����� ��������� �������
			Status = robREADY;
		else // ���� DriveReady == 1
		if(CurBitStatusByte.Pusk)
		{

			if(CurBitStatusByte.Pause)               // ���� + ����� = �����
			{
				Status = robPAUSE;
			}else
			if(CurBitStatusByte.PC_Ready == 0)       // ���� +
				Status = robSTARTING;
			else
				Status = robSTART;                   // ���� + �� ����� = �����
		}else
		if(CurBitStatusByte.LasrKadr)                // ����� ���, �� ���� ��������� ���� - ��� "������ ���������"
		{
			Status = robFINISH;
		}else
		if(CurBitStatusByte.BufBusy) 	 			 // ��������� ��� ������� ������ �����. ������ = ������
			Status = robSTOP;
	}
	PredBitStatusByte = CurBitStatusByte;
	return Status;
}

//---------------------------------------------------------------------------
double TPLCDriver::Get_Cur_I()             // �������� ������� ���
{
	short V = M_ArrayCopy[D_ADDR + 743];
	return (double)V;//10;// *800/0x0fff;
}
//---------------------------------------------------------------------------
double TPLCDriver::Get_Cur_B()             // �������� ������� ����������
{
	short V = M_ArrayCopy[D_ADDR + 745];
	return (double)V/10;// *100/0x0fff;
}
//---------------------------------------------------------------------------
double TPLCDriver::Get_Cur_V()             // �������� ������� �������� ���������
{
	short V = M_ArrayCopy[D_ADDR + 746];
	return (double)V/10;// *10/0x0fff;
}

