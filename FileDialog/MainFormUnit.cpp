//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainFormUnit.h"
#include "OpenFileUnit.h"
#include "IniFiles.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------
__fastcall TFormMain::TFormMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::Button1Click(TObject *Sender)
{
	Path = RadioGroup2->ItemIndex==0?"\\BC-3":"\\BC-2";
	Path += RadioGroup1->ItemIndex == 0?"\\TiG":"\\MiG";
	if(RadioGroup3->ItemIndex == 0) Path += "\\Macros";
	Path = "C:\\Promos"+Path+"\\";
	Ext = RadioGroup3->ItemIndex == 0?"mcr":"prg";

	TSearchRec search_rec;
//	FlCanSelected = false;
	FormOpenFile->ClearFileList();
	TStringList *S = new TStringList;
	try
	{
		AnsiString FullFileName = Path+"*." + Ext;
		int error_code = FindFirst(FullFileName, faAnyFile, search_rec);
		while (error_code == 0)
		{
			AnsiString ss = ExtractFileName(search_rec.Name).SubString(1,search_rec.Name.Pos(".")-1);
//			if (FileExists(search_rec.Name))
			{
				TMemIniFile *File = NULL;
				try {
					//�������� ����������� �� INI-�����
					File = new TMemIniFile(Path+search_rec.Name);
					//"[Comments]"
					ss = ss + "=" + File->ReadString("Comments","Comment1","����������� ����������");
					FormOpenFile->AddFileName(ss);

					delete File; File = NULL;
				} catch ( ... ) {
					if(File)	delete File;
					return;// 2;
				}
			}//else  		return;// 1;
			error_code = FindNext(search_rec);
		}
	}
	__finally
	{
		FindClose(search_rec);
//		FlCanSelected = true;
	}
	FormOpenFile->ShowModal();

}
//---------------------------------------------------------------------------
