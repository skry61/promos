object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1092#1091#1085#1082#1094#1080#1080
  ClientHeight = 158
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 96
    Top = 104
    Width = 100
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 232
    Top = 104
    Width = 100
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1092#1072#1081#1083
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 25
    Top = 15
    Width = 105
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'TiG'
      'MiG')
    TabOrder = 2
  end
  object RadioGroup2: TRadioGroup
    Left = 136
    Top = 15
    Width = 105
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'BC-3'
      'BC-2')
    TabOrder = 3
  end
  object RadioGroup3: TRadioGroup
    Left = 247
    Top = 15
    Width = 122
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Macro'
      'Prog')
    TabOrder = 4
  end
end
