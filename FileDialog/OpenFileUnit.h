//---------------------------------------------------------------------------

#ifndef OpenFileUnitH
#define OpenFileUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
//---------------------------------------------------------------------------
class TFormOpenFile : public TForm
{
__published:	// IDE-managed Components
	TValueListEditor *ValueListEditor1;
	void __fastcall ValueListEditor1Click(TObject *Sender);
	void __fastcall ValueListEditor1SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
	bool FlCanSelected;

public:		// User declarations
	__fastcall TFormOpenFile(TComponent* Owner);
	void __fastcall AddFileName(AnsiString s);
	void __fastcall ClearFileList();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormOpenFile *FormOpenFile;
//---------------------------------------------------------------------------
#endif
