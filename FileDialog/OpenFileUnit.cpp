//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OpenFileUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormOpenFile *FormOpenFile;
//---------------------------------------------------------------------------
__fastcall TFormOpenFile::TFormOpenFile(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormOpenFile::ClearFileList()
{
	ValueListEditor1->Strings->Clear();
	FlCanSelected = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormOpenFile::AddFileName(AnsiString s)
{
	ValueListEditor1->Strings->Add(s);
}

//---------------------------------------------------------------------------
void __fastcall TFormOpenFile::ValueListEditor1Click(TObject *Sender)
{
	int RowCount;
	int Disp = 0;
	FlCanSelected = true;
	try
	{
		if(List!=NULL)
			delete List;
		List = new TStringList();
//		List->Clear();
		try
		{
			List->LoadFromFile(FullFileName);

			if(Ext == "prg")
			{
				if(!List->Find("[Comments]",Disp))
					RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				else
					if(Disp == 0)
					{
						RowCount = List->Strings[1].ToInt();               // ��������� ���������� �����
						Disp = 1;
					}
					else
						RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				if((RowCount<2)||(RowCount>500))
				{
					ShowMessage("� ��������� ���������������� ���������� �����.");
					return;
				}
			}else
				RowCount = List->Count;               // ��������� ���������� �����

			SaveForm->SetList(&List);

			Memo1->Clear();
			for (int i=0; i<RowCount; i++)
			{
				AnsiString s = List->Strings[i+Disp];
				Memo1->Lines->Add(s);
			}

		}__finally
		{
 //			delete List;
		}

	} catch ( ... ) {
		ShowMessage("������ ��� �������� ��������� ������.");
		return;
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormOpenFile::ValueListEditor1SelectCell(TObject *Sender,
      int ACol, int ARow, bool &CanSelect)
{
	if(FlCanSelected == false) return;
	FlCanSelected == false;
	AnsiString FileName = ValueListEditor1->Keys[ARow];
	FullFileName = Path+FileName+"." + Ext;
	SaveForm->SetFileName(FileName.c_str(),FullFileName.c_str());

}
//---------------------------------------------------------------------------
void __fastcall TFormOpenFile::FormShow(TObject *Sender)
{
	FlCanSelected = true;
	
}
//---------------------------------------------------------------------------
