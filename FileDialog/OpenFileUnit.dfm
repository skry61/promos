object FormOpenFile: TFormOpenFile
  Left = 0
  Top = 0
  Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
  ClientHeight = 169
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ValueListEditor1: TValueListEditor
    Left = 0
    Top = 0
    Width = 590
    Height = 169
    Align = alClient
    KeyOptions = [keyEdit, keyAdd, keyUnique]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    Strings.Strings = (
      'Macro   1='#1069#1090#1086' '#1087#1077#1088#1074#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   2='#1069#1090#1086' '#1074#1090#1086#1088#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   3='#1069#1090#1086' '#1090#1088#1077#1090#1100#1077' '#1084#1072#1082#1088#1086
      'Macro   4='#1069#1090#1086' '#1095#1077#1090#1074#1077#1088#1090#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   5='
      'Macro   6='
      'Macro   7='
      'Macro   8='
      'Macro   9='
      'Macro  10='
      'Macro  11='
      'Macro  12='
      'Macro  13='
      '')
    TabOrder = 0
    TitleCaptions.Strings = (
      ' '#1052#1072#1082#1088#1086
      ' '#1054#1087#1080#1089#1072#1085#1080#1077)
    OnClick = ValueListEditor1Click
    OnSelectCell = ValueListEditor1SelectCell
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 553
    ExplicitHeight = 153
    ColWidths = (
      150
      417)
  end
end
