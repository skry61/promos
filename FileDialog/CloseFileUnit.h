//---------------------------------------------------------------------------

#ifndef CloseFileUnitH
#define CloseFileUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormSaveFile : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TLabel *Label2;
	TEdit *Edit_FileName;
	TEdit *Edit_Comment;
	TButton *Button1;
private:	// User declarations
public:		// User declarations
	__fastcall TFormSaveFile(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSaveFile *FormSaveFile;
//---------------------------------------------------------------------------
#endif
