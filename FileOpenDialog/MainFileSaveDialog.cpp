//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainFileSaveDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSaveForm *SaveForm;
//---------------------------------------------------------------------------
__fastcall TSaveForm::TSaveForm(TComponent* Owner)
	: TForm(Owner)
{
	FileTypePRG = false;
	List = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TSaveForm::SetFileName(char * FN,char * FFN)
{
	FullFileName = AnsiString(FFN) ;
	FileName = AnsiString(FN) ;
	Edit_FileName->Text = FileName;
	Ext = ExtractFileExt(FullFileName);
//	FileName = ExtractFileName(FullFileName);
}
//---------------------------------------------------------------------------



void __fastcall TSaveForm::Button1Click(TObject *Sender)
{
	int Index;
	if((List!=NULL)&&(*List!=NULL))
	{
//		(*List)->Add("[Comments]");

		if(!(*List)->Find("[Comments]",Index))
		{
//			(*List)->Insert(0,"[Comments]");
			(*List)->Add("[Comments]");
			(*List)->Add("Comment1="+ Edit_Comment->Text);
		}else
			(*List)->Values["Comment1"] = Edit_Comment->Text;
		(*List)->SaveToFile(FullFileName);

		delete *List; *List=NULL;List=NULL;

	}

}
//---------------------------------------------------------------------------

