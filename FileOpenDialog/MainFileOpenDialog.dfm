object OpenForm: TOpenForm
  Left = 0
  Top = 0
  Caption = #1054#1090#1082#1088#1099#1090#1100' '#1052#1072#1082#1088#1086#1089
  ClientHeight = 576
  ClientWidth = 552
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ValueListEditor1: TValueListEditor
    Left = 0
    Top = -2
    Width = 553
    Height = 153
    KeyOptions = [keyEdit, keyAdd, keyUnique]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    Strings.Strings = (
      'Macro   1='#1069#1090#1086' '#1087#1077#1088#1074#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   2='#1069#1090#1086' '#1074#1090#1086#1088#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   3='#1069#1090#1086' '#1090#1088#1077#1090#1100#1077' '#1084#1072#1082#1088#1086
      'Macro   4='#1069#1090#1086' '#1095#1077#1090#1074#1077#1088#1090#1086#1077' '#1084#1072#1082#1088#1086
      'Macro   5='
      'Macro   6='
      'Macro   7='
      'Macro   8='
      'Macro   9='
      'Macro  10='
      'Macro  11='
      'Macro  12='
      'Macro  13='
      '')
    TabOrder = 0
    TitleCaptions.Strings = (
      ' '#1052#1072#1082#1088#1086
      ' '#1054#1087#1080#1089#1072#1085#1080#1077)
    OnClick = ValueListEditor1Click
    OnSelectCell = ValueListEditor1SelectCell
    ColWidths = (
      150
      381)
  end
  object Memo1: TMemo
    Left = 0
    Top = 184
    Width = 553
    Height = 393
    Lines.Strings = (
      #1060#1072#1081#1083' '#1085#1077' '#1079#1072#1075#1088#1091#1078#1077#1085' !')
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 9
    Top = 151
    Width = 105
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'TiG'
      'MiG')
    TabOrder = 2
  end
  object RadioGroup2: TRadioGroup
    Left = 120
    Top = 151
    Width = 105
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'BC-3'
      'BC-2')
    TabOrder = 3
  end
  object Button1: TButton
    Left = 358
    Top = 157
    Width = 104
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
    TabOrder = 4
    OnClick = Button1Click
  end
  object RadioGroup3: TRadioGroup
    Left = 231
    Top = 151
    Width = 122
    Height = 31
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Macro'
      'Prog')
    TabOrder = 5
    OnClick = RadioGroup3Click
  end
  object Button2: TButton
    Left = 465
    Top = 157
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 6
    OnClick = Button2Click
  end
end
