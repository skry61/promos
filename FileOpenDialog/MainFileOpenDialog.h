//---------------------------------------------------------------------------

#ifndef MainFileOpenDialogH
#define MainFileOpenDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TOpenForm : public TForm
{
__published:	// IDE-managed Components
	TValueListEditor *ValueListEditor1;
	TMemo *Memo1;
	TRadioGroup *RadioGroup1;
	TRadioGroup *RadioGroup2;
	TButton *Button1;
	TRadioGroup *RadioGroup3;
	TButton *Button2;
	void __fastcall ValueListEditor1Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ValueListEditor1SelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall RadioGroup3Click(TObject *Sender);
private:	// User declarations
	AnsiString Path;
	AnsiString Ext;
	bool FlCanSelected;
	AnsiString FullFileName;
	TStringList *List;
//	AnsiString FileName;

public:		// User declarations
	__fastcall TOpenForm(TComponent* Owner);
	__fastcall ~TOpenForm();
//	void __fastcall SetFileName(char * FN,char * FFN);
};
//---------------------------------------------------------------------------
extern PACKAGE TOpenForm *OpenForm;
//---------------------------------------------------------------------------
#endif
