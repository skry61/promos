//---------------------------------------------------------------------------

#ifndef MainFileSaveDialogH
#define MainFileSaveDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TSaveForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *Edit_FileName;
	TLabel *Label1;
	TEdit *Edit_Comment;
	TLabel *Label2;
	TButton *Button1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
	AnsiString FullFileName;
	AnsiString FileName;
	AnsiString Ext;
	bool FileTypePRG;
	TStringList **List;

public:		// User declarations
	__fastcall TSaveForm(TComponent* Owner);
	void __fastcall SetFileName(char * FN,char * FFN);
	void __fastcall SetList(TStringList **aList){List = aList;};
};
//---------------------------------------------------------------------------
extern PACKAGE TSaveForm *SaveForm;
//---------------------------------------------------------------------------
#endif
