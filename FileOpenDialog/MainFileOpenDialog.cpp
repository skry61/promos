//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainFileOpenDialog.h"
#include "MainFileSaveDialog.h"
#include "IniFiles.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOpenForm *OpenForm;
//---------------------------------------------------------------------------
__fastcall TOpenForm::TOpenForm(TComponent* Owner)
	: TForm(Owner)
{
	FlCanSelected = false;
	List = NULL;
	Caption = "������� ������";
}
//---------------------------------------------------------------------------
__fastcall TOpenForm::~TOpenForm()
{
	if(List!=NULL)	delete List;
}

//---------------------------------------------------------------------------
void __fastcall TOpenForm::ValueListEditor1Click(TObject *Sender)
{
	int RowCount;
	int Disp = 0;
	FlCanSelected = true;
	try
	{
		if(List!=NULL)
			delete List;
		List = new TStringList();
//		List->Clear();
		try
		{
			List->LoadFromFile(FullFileName);

			if(Ext == "prg")
			{
				if(!List->Find("[Comments]",Disp))
					RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				else
					if(Disp == 0)
					{
						RowCount = List->Strings[1].ToInt();               // ��������� ���������� �����
						Disp = 1;
					}
					else
						RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				if((RowCount<2)||(RowCount>500))
				{
					ShowMessage("� ��������� ���������������� ���������� �����.");
					return;
				}
			}else
				RowCount = List->Count;               // ��������� ���������� �����

			SaveForm->SetList(&List);

			Memo1->Clear();
			for (int i=0; i<RowCount; i++)
			{
				AnsiString s = List->Strings[i+Disp];
				Memo1->Lines->Add(s);
			}

		}__finally
		{
 //			delete List;
		}

	} catch ( ... ) {
		ShowMessage("������ ��� �������� ��������� ������.");
		return;
	}

}
//---------------------------------------------------------------------------

void __fastcall TOpenForm::Button1Click(TObject *Sender)
{
	Path = RadioGroup2->ItemIndex==0?"\\BC-3":"\\BC-2";
	Path += RadioGroup1->ItemIndex == 0?"\\TiG":"\\MiG";
	if(RadioGroup3->ItemIndex == 0) Path += "\\Macros";
	Path = "C:\\Promos"+Path+"\\";
	Ext = RadioGroup3->ItemIndex == 0?"mcr":"prg";

	TSearchRec search_rec;
	FlCanSelected = false;
	ValueListEditor1->Strings->Clear();
	TStringList *S = new TStringList;
	try
	{
		AnsiString FullFileName = Path+"*." + Ext;
		int error_code = FindFirst(FullFileName, faAnyFile, search_rec);
		while (error_code == 0)
		{
			AnsiString ss = ExtractFileName(search_rec.Name).SubString(1,search_rec.Name.Pos(".")-1);
//			if (FileExists(search_rec.Name))
			{
				TMemIniFile *File = NULL;
				try {
					//�������� ����������� �� INI-�����
					File = new TMemIniFile(Path+search_rec.Name);
					//"[Comments]"
					ss = ss + "=" + File->ReadString("Comments","Comment1","����������� ����������");
					ValueListEditor1->Strings->Add(ss);

					delete File; File = NULL;
				} catch ( ... ) {
					if(File)	delete File;
					return;// 2;
				}
			}//else  		return;// 1;
			error_code = FindNext(search_rec);
		}
	}
	__finally
	{
		FindClose(search_rec);
		FlCanSelected = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOpenForm::ValueListEditor1SelectCell(TObject *Sender, int ACol,
	  int ARow, bool &CanSelect)
{
	if(FlCanSelected == false) return;
	FlCanSelected == false;
	AnsiString FileName = ValueListEditor1->Keys[ARow];
	FullFileName = Path+FileName+"." + Ext;
	SaveForm->SetFileName(FileName.c_str(),FullFileName.c_str());

}
//---------------------------------------------------------------------------



void __fastcall TOpenForm::Button2Click(TObject *Sender)
{
	SaveForm->Show();
}
//---------------------------------------------------------------------------
/*void __fastcall TOpenForm::SetFileName(char * FN,char * FFN)
{
	SaveForm->
}
*/
//---------------------------------------------------------------------------

void __fastcall TOpenForm::RadioGroup3Click(TObject *Sender)
{
	if(RadioGroup3->ItemIndex == 0)
	{
		Caption = "������� ������";
		ValueListEditor1->TitleCaptions->CommaText  = "Macro,��������";
	}
	else
	{
		Caption = "������� ���������";
		ValueListEditor1->TitleCaptions->CommaText = "���������,��������";
	}

}
//---------------------------------------------------------------------------

