------------------------
class TComPort {
	HANDLE	       		FHandle;		//�������������							- ������ ������ ����� CS
	TComPortRXThread	*FRXThread;		//��������� �� ������ ��������� ������
 }

------------------------
class TComPortRXThread : public TThread {
	TComPort	*FComPort;		//��������� �� ������, ����������� �������� �����
	__fastcall TComPortRXThread(HANDLE AHandle, TComPort *AComPort);	//�����������
}
------------------------

class TSerialIOHandler {                                //��������� �� ��������� �������
	TSerialDriver *Driver;
	void SetDriver(TSerialDriver *ADriver);	     	//������� ���������� ���������� ��������
}

------------------------ #drvbase.h
class TDriverThread : public TThread {      // ����� ����������� �����

	TDriver	*Driver;										//��������� �� ��������� �������
	__fastcall TDriverThread(TDriver *ADriver, TThreadPriority APriority);	//�����������
	void __fastcall CallExternalProc(); //�����, ���������� ���������; ������������ ��� ������ ������� ���������, ��������� � ExternalProc

}

------------------------ #serialdrv.h
class TDriver {
	TDriverThread		*Thread;      				//��������� �� ����� ���������� �����������
	void __fastcall CallExternalProcSync();			//������� ����������� ������ ������� ���������
	void __fastcall Run(TThreadPriority APriority);	//������� ������� ��������
	virtual void Handler() = 0;		        		//����������� ������� ����������� ��������
}

------------------------
class TSerialDriver : public TDriver {
	TSerialIOHandler *IOHandler;										//��������� ���������� �����-������
	void RXInterruptHandler(char AByte);	        //���������� ���������� �� ������ �����
	char RXCompleted();													//������� �������� ���������� ������ ������
	void FlushRX();														//������� ������ ��������� ������
	char CheckMessageInternal(const char *Text, unsigned char TextLen, char Wildcard, unsigned short RXLen);	//������� ������ �������� ������ ��� ������������������ � �������� ������
	unsigned short TXInterruptHandler();								//���������� ���������� �� ���������� �������� �����
}

------------------------
class TBridgeDriver : public TSerialDriver {
	TBridgeDriver() : TSerialDriver(4096,4096) {}
	virtual void Handler();
}
------------------------
	   "Com.h"					 "ComPort.h"      "serialDrv.h"
class TComPortIOHandler : public TComPort, public TSerialIOHandler {
								 /                        |
								/ "ComPort.h"             | "serialDrv.h"
(/*create by Connect*/TComPortRXThread)          (TSerialDriver/*InitBy SetDriver*/)
						 /       |                        |
					TThread  (TComPort/*IByC*/)        TDriver  "drvBase.h"
														  |
													 (TDriverThread/*create by Run*/)
													  /	      \
												  TThread    (TDriver/*init by cnstr*/})


	ComPort = new TComPortIOHandler();

	BridgeDriver = new TBridgeDriver();

	ComPort->SetDriver(BridgeDriver);

	BridgeDriver->Run(tpNormal);
}
