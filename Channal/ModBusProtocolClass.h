#ifndef ModBusProtocolClassH
#define ModBusProtocolClassH
//------------------------------------------------------------------------------
#include "ServiceClass.h"
// ������� ����������� ���������
#define Cmd_READWORD         1
#define Cmd_READNBITS        2
#define Cmd_READNWORDS       3
#define Cmd_WRITE1BIT        4
#define Cmd_WRITE1WORD       5
#define Cmd_WRITENWORDS      6


class TProtocolClass;

class TProtocol {
protected:
	BYTE	ProtocolCMD;		//������� � ���������

public:
	TProtocolClass *ProtocolClass;
	virtual int Parser(char *OutBuf) = 0;
	virtual bool FillBuffer(unsigned char Cmd, WORD Addr, unsigned char *Len, char * Data,char * OutBuf)=0;
	virtual bool isASCII(){return false;}
public:
	TProtocol(){};
	virtual bool	ReadWArray(U16 Addr,short Count, short *pVs)=0;
	virtual char 	GetCMD(char CommonCMD,WORD Addr) = 0;
	void SetProtocolClass(TProtocolClass *aProtocolClass){ProtocolClass = aProtocolClass;}
};

//------------------------------------------------------------------------------
// ����� ��������� ModBus RTU
//------------------------------------------------------------------------------
class TModBusProtocolRTU : public TProtocol {
protected:
	int MsgCount;	//������� ���������� ����

	virtual int  Parser(char *OutBuf);
	virtual bool FillBuffer(unsigned char Cmd, WORD Addr, unsigned char *Len,char * Data, char * OutBuf);
	virtual WORD CalculateCRC( BYTE* Buf, BYTE Count );
	virtual bool CheckCRC( char * Buffer, int Len);


public:
	TModBusProtocolRTU () : TProtocol(){};
	virtual bool	ReadWArray(U16 Addr,short Count, short *pVs){return true;};
	virtual char 	GetCMD(char CommonCMD,WORD Addr);


};

//------------------------------------------------------------------------------
// ����� ��������� ModBus ASCII
//------------------------------------------------------------------------------
class TModBusProtocolASCII : public TModBusProtocolRTU {
protected:
	virtual int  Parser(char *OutBuf);
	virtual bool FillBuffer(unsigned char Cmd, WORD Addr, unsigned char *Len,char * Data, char * OutBuf);
	virtual WORD CalculateCRC( BYTE* Buf, BYTE Count );
	virtual bool CheckCRC( char * Buffer, int Len);
	virtual bool isASCII(){return true;}
public:
//	TModBusProtocolRTU () : TProtocol(){};
	virtual bool	ReadWArray(U16 Addr,short Count, short *pVs);//{return true;};

};
//------------------------------------------------------------------------------
#endif
