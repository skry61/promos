//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Grafer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Grapher"
#pragma link "Fron_Grapher"

#pragma resource "*.dfm"
TGraferForm *GraferForm;
//---------------------------------------------------------------------------
__fastcall TGraferForm::TGraferForm(TComponent* Owner)
	: TForm(Owner)
{
	ZoomCount = 4;
	ShowCount = 0;
	ShowCountLimit = 5;
	Grapher1 = new TFron_Grapher(Owner);
	Grapher1->Parent = this;
	Grapher1->Left = 0;
	Grapher1->Top = 0;
	Grapher1->Width = 688;
	Grapher1->Height = 453;
//	Grapher1->H_Disp = 50000;
//	Grapher1->V_Disp = 40000;
	Grapher1->Align = alClient;
	Grapher1->CaptionSize = 14;
	Grapher1->CaptionFont = "Arial";
	Grapher1->Caption = " ������ �������� ";// + Supp->SupportNomber;
	Grapher1->BorderColor = clNavy;
	Grapher1->BorderWidth = 1;
	Grapher1->LineWidth = 0;
	Grapher1->ArrowDraw = false;

	Grapher1->VMax = 100000;
	Grapher1->VStep = 10000;
	Grapher1->HMax = 100000;
	Grapher1->HStep = 10000;
	Grapher1->VDivisor = 1000;
	Grapher1->HDivisor = 1000;
	Grapher1->H_Disp = 50000;
	Grapher1->V_Disp = 40000;

	Grapher1->MarkColor = clBlack;
	Grapher1->MarkerColor = clBlack;
	Grapher1->ValueFormat = ffFixed;
	Grapher1->LimitColor = clRed;
	Grapher1->PenColor = clHotLight;
	Grapher1->Marker = true;

	MP4_Unit = new TMP4_Unit();
}
//---------------------------------------------------------------------------
 //---------------------------------------------------------------------------
void __fastcall TGraferForm::ZoomIn(void)
{
  Grapher1->VMax /= 2;
  Grapher1->HMax /= 2;
  Grapher1->VStep /= 2;
  Grapher1->HStep /= 2;
  Grapher1->Repaint();
  ZoomCount++;

}
//---------------------------------------------------------------------------
void __fastcall TGraferForm::ZoomOut(void)
{
  if (ZoomCount){
	  Grapher1->VMax *= 2;
	  Grapher1->HMax *= 2;
	  Grapher1->VStep *= 2;
	  Grapher1->HStep *= 2;
	  Grapher1->Repaint();
	  ZoomCount--;
  }
}

//---------------------------------------------------------------------------
void __fastcall TGraferForm::Add(int X,int Y)
{
/*
	if(cbAxisChange->Checked)
		Grapher1->Add(Y,X);
	else
*/
		Grapher1->Add(X,Y);
	if(ShowCount++ > ShowCountLimit){
		Grapher1->Repaint();
		ShowCount = 0;
  }
}

//---------------------------------------------------------------------------
void __fastcall TGraferForm::AddData(int X,int Y)
{
/*
	if(cbAxisChange->Checked)
		Grapher1->Add(Y,X);
	else
*/
		Grapher1->Add(X,Y);
	if(ShowCount++ > ShowCountLimit){
		FlShow = true;
		ShowCount = 0;
	}

}

//---------------------------------------------------------------------------
void __fastcall TGraferForm::EraseGraph(void)
{
  Grapher1->ClearData();
  Grapher1->ShowData();
}

//---------------------------------------------------------------------------
void __fastcall TGraferForm::Timer1Timer(TObject *Sender)
{
		if(FlShow)
		{
			Grapher1->ShowData();
			//ShowData();

        }

}
//---------------------------------------------------------------------------
void __fastcall TGraferForm::eDrawFriqKeyPress(TObject *Sender, char &Key)
{
   if ((Key == VK_RETURN)){
		try {
			ShowCountLimit = eDrawFriq->Text.ToInt();
		} catch (...) {
			MessageBox( Handle, "���������������� ����", "������ ��������", MB_ICONINFORMATION );
		}
   }
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::EditdXKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key == VK_RETURN)
	  Grapher1->H_Disp = EditdX->Text.ToInt();

}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::EditdZKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key == VK_RETURN)
	  Grapher1->V_Disp = EditdZ->Text.ToInt();

}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::Button1Click(TObject *Sender)
{
	MP4_Unit->StartCNC();	
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::Button2Click(TObject *Sender)
{
	MP4_Unit->StopCNC();	
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::TrackBar1Change(TObject *Sender)
{
	int Period = -TrackBar1->Position;
	MP4_Unit->set_Period( Period);
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::ColorBox1Change(TObject *Sender)
{
	Grapher1->AddColor(ColorBox1->Selected);

}
//---------------------------------------------------------------------------
void Clear( void );
void __fastcall TGraferForm::Button3Click(TObject *Sender)
{
	Grapher1->ClearData();
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::Button4Click(TObject *Sender)
{
	ZoomIn();	
}
//---------------------------------------------------------------------------

void __fastcall TGraferForm::Button5Click(TObject *Sender)
{
	ZoomOut();
}
//---------------------------------------------------------------------------

