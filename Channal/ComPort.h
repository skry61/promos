//---------------------------------------------------------------------------
// ���� �������� ������ COM-�����
//---------------------------------------------------------------------------

#ifndef ComPortH
#define ComPortH
//---------------------------------------------------------------------------

#include <System.hpp>
#include <Classes.hpp>
#include <SyncObjs.hpp>
#include <Dialogs.hpp>		//��� ���������� ���������

class TComPort;

//-------------------
// ������� ���-�����
//-------------------
enum TComEvent {evRxChar, evRxFlag, evTxEmpty, evCTS, evDSR, evRLSD, evBreak, evError, evRing, evPErr, evRx80Full};
typedef Set<TComEvent,evRxChar,evRx80Full> TComEvents;

//------------------------------------
// ����������� ���� ������� ���-�����
//------------------------------------
typedef void __fastcall (__closure *TComPortRXBufferEvent) (TComPort *AComPort, char *ABuffer, unsigned long ACount);

//---------------------------
// �������� ������ ���-�����
//---------------------------
class TComPort {

friend class TComPortRXThread;

private:
	AnsiString			FPortName;		//��� �����, ��������, "COM1"
	int					FBaudRate;		//�������� ������, ��������, 9600
	unsigned char		FParity;		//��������
	unsigned char		FStopBits;		//���������� ����-�����
	unsigned char		FByteSize;		//���������� ����� ������

	bool				FConnected;		//���������/�������� (���� ������/������)	- ������ ������ ����� CS
	HANDLE				FHandle;		//�������������							- ������ ������ ����� CS
	TComPortRXThread	*FRXThread;		//��������� �� ������ ��������� ������
	TCriticalSection	*CS;			//����������� ������ ��� ������� � FConnected, FHandle, FRXThread

	int					FRXBufferSize;	//������ ������ ������
	int					FTXBufferSize;	//������ ������ ��������

	TComEvents			FEvents;		//������� ���-�����, ��� ������� ������ ���������� ��������������� �����������

private:	
	void   __fastcall SetConnected(bool AConnected);	//������� ��������� ��������� �����
	void   __fastcall SetHandle(HANDLE AHandle);		//������� ������ �������������� � FHandle ����� CS
	HANDLE __fastcall GetHandle();						//������� ������ �������������� �� FHandle ����� CS

	bool __fastcall InputCount(unsigned long *ACount);			  		//�������� ���������� ���� ������ � �������� ������ �������� ���-�����

public:
	TComPortRXBufferEvent	OnRXChar;					//��������� �� ���������-���������� ������� ������ ������

	__fastcall TComPort();								//�����������
	__fastcall ~TComPort();								//����������

	bool __fastcall Connected();						//������� ������ ��������� ����� (���������/��������)

	bool __fastcall SetPortName(AnsiString APortName);	//������ ��� �����
	bool __fastcall SetBaudRate(int ABaudRate);			//������ �������� ������
	bool __fastcall SetFormat(AnsiString AFormat);		//������ ������ ������/�������� (��������, 8N1)
	bool __fastcall SetRXBufferSize(int ASize);			//������ ������ ������ ������
	bool __fastcall SetTXBufferSize(int ASize);			//������ ������ ������ ��������
	bool __fastcall SetEvents(TComEvents AEvents);		//������ ��������� ������� ���-�����
	bool __fastcall SetRTS(){EscapeCommFunction(FHandle,SETRTS);return true;}							//RTS - 1
	bool __fastcall ClrRTS(){EscapeCommFunction(FHandle,CLRRTS);return true;}							//RTS - 0

	AnsiString __fastcall GetPortName() {return FPortName;}	//������ ��� �����

	bool __fastcall Connect();							//������� ����
	bool __fastcall Disconnect();						//������� ����

	bool __fastcall SetupComPort();						//������������� ���-�����
	bool __fastcall ApplyBuffers();						//��������� ��������� �������
	bool __fastcall ApplyDCB();							//��������� ��������� DCB
	bool __fastcall ApplyTimeouts();					//��������� ��������� ����-�����

	TComEvents	IntToEvents(int AMask);					//������������� ����� � ��������� �������
	int			EventsToInt(TComEvents AEvents);		//������������� ��������� ������� � �����

	void __fastcall CallOnRXChar();
	void __fastcall CallOnRXFlag();
	void __fastcall CallOnTXEmpty();
	void __fastcall CallOnCTS();
	void __fastcall CallOnDSR();
	void __fastcall CallOnRLSD();
	void __fastcall CallOnBreak();
	void __fastcall CallOnError();
	void __fastcall CallOnRing();
	void __fastcall CallOnPErr();
	void __fastcall CallOnRx80Full();

	bool WriteBuffer(char *ABuffer, int ASize);			//�������� ������ � ���-����

	bool __fastcall ReadBuffer(char **ABuffer, unsigned long *ACount);	//������� ������ �� ��������� ������ �������� ���-�����

	virtual bool InterceptEvents(TComEvents	Events) {return false;}		//����������� �������, ����� ���� ������������� � ����������
};

//--------------------------------------------
// �������� ������ ��������� ������ ���-�����
//--------------------------------------------
class TComPortRXThread : public TThread {

private:
	TComPort	*FComPort;		//��������� �� ������, ����������� �������� �����
	HANDLE		FHandle;		//������������� ���-�����
	HANDLE		FStopEvent;		//������������� ������� �������� ������
	TComEvents	FEvents;		//������� ���-�����

	void __fastcall Execute();	//������� ��������� ���������� ��������� ������

public:

	__fastcall TComPortRXThread(HANDLE AHandle, TComPort *AComPort);	//�����������
	__fastcall ~TComPortRXThread();										//����������

	void __fastcall Stop();		//������� � ���������� ��������� ������
	void __fastcall DoEvents();	//����� ������������ ������� ���-�����


};


//---------------------------------------------------------------------------
#endif
