//-------------------------------------------
// ���� �������� ������-������� ��� TComPort
//-------------------------------------------

#ifndef COMH
#define COMH

#include "serialdrv.h"
#include "ComPort.h"

//----------------------------
// �����-������� ��� TComPort
//----------------------------
class TComPortIOHandler : public TComPort, public TSerialIOHandler {

public:
	virtual void Write();								//��������� �������� ������
	virtual bool InterceptEvents(TComEvents	Events);	//����������� �������
};

#endif
