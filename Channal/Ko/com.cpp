//-------------------------------------------
// ���� �������� ������-������� ��� TComPort
//-------------------------------------------

#include "com.h"

//=============================================================================================================================================================
// TComPortIOHandler - �����-������� ��� TComPort
//=============================================================================================================================================================

//---------------------------
// ��������� �������� ������
//---------------------------
void TComPortIOHandler::Write() {
	if (Driver) {
		WriteBuffer(&(Driver->TXBuffer[Driver->TXIndex]),Driver->TXCounter);
		Driver->TXCounter = 0;
	}
}

//---------------------
// ����������� �������
//---------------------
bool TComPortIOHandler::InterceptEvents(TComEvents	Events) {

	if (Events.Contains(evRxChar)) {

		char *Buffer=NULL;
		unsigned long Count;

		if (ReadBuffer(&Buffer,&Count)&&(Count>0)) {
			if (Driver) {
				char *ptr = Buffer;
				while (Count--) Driver->RXInterruptHandler(*ptr++);
			}
		}
		if (Buffer) delete [] Buffer;
		return true;
	}
	return false;
}




