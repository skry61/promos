//---------------------------------------------------------------------------

#ifndef ThreadSafeH
#define ThreadSafeH

#include "SyncObjs.hpp"

//-----------------------
// Потокобезопасный bool
//-----------------------
class TThreadSafeBool {

private:
	TCriticalSection *CS;
	bool Value;

public:
	TThreadSafeBool()	{CS = new TCriticalSection(); Value = false;}
	~TThreadSafeBool()	{delete CS;}

	TThreadSafeBool& operator=(bool AValue);
	bool operator==(bool AValue);
	bool operator!=(bool AValue);
	bool operator!();

};

//----------------------
// Потокобезопасный U64
//----------------------
class TThreadSafeU64 {

private:
	TCriticalSection *CS;
	unsigned long long Value;

public:
	TThreadSafeU64()	{CS = new TCriticalSection(); Value = 0;}
	~TThreadSafeU64()	{delete CS;}

	TThreadSafeU64& operator=(unsigned long long AValue);
	unsigned long long operator~();

};

//-----------------------------
// Потокобезопасный AnsiString
//-----------------------------
class TThreadSafeAnsiString {

private:
	TCriticalSection *CS;
	AnsiString String;

public:
	TThreadSafeAnsiString()		{CS = new TCriticalSection();}
	~TThreadSafeAnsiString()	{delete CS;}

	TThreadSafeAnsiString& operator=(AnsiString AString);
	AnsiString operator~();

};


//---------------------------------------------------------------------------
#endif
