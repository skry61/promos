//-------------------------------
// ���� �������� ������� �������
//-------------------------------

#ifndef BUFFERSH
#define BUFFERSH

//-------------------------
// ����� ���������� ������
//-------------------------
class TCircularBuffer {

public:
	char *Buffer;									//��������� �� �����, ����������� � ����
	unsigned short Size;							//������ ������ � ������
	unsigned short WRIndex;							//������ ������
	unsigned short RDIndex;							//������ ������

	TCircularBuffer(unsigned short ASize);			//�����������
	~TCircularBuffer();								//����������

	void Reset();									//����� ������
	void Flush();									//������� ������
	void Write(char AByte);							//������ ����� � ����� � ��������� ������� ������
	char Read();									//������ ����� �� ������ � ��������� ������� ������
	char IsEmpty();									//��������, ���� �� �����
	char IsFull();									//��������, ����� �� �����, � ��������� ������� ������

	unsigned short Count();							//���������� ���� � ������ � ��������� ������� ������
	char ReadOffset(signed short AOffset);			//������ ����� �� ������ �� �������� � ��������� ������� ������
	void IncrementRDIndex(unsigned short AValue);	//��������� ������ ������ � ��������� ������� ������

};

#endif

