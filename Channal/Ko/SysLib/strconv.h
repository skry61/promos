//--------------------------------------------------------------------------------------
// ���� �������� ��� �������������� ��������� ���������� � ������ ��������������� �����
//--------------------------------------------------------------------------------------

#ifndef STRCONVH
#define STRCONVH

#pragma anon_unions		//���������� ������������� �������� � �����������

//-------------------------------------------
// ����������� ���� ��������� ��������
// ������� ������ � ����� ��������� � ������
//-------------------------------------------
//#pragma pack(push,1)
typedef struct {
	unsigned short begin, end;
} TSC_ParamBounds;

//-------------------------------------------
// ����������� ���� ��������� �������� �����
// � ������� ����� ����� � ��������� ������
//-------------------------------------------
typedef union {
	struct {
		unsigned char fraction_digits;	//������� ��������: ������������� ���������� ���� � ������� �����
	};
	struct {
		unsigned long int f;			//�������� ��������: ������� �����
		unsigned long int i;			//�������� ��������: ����� �����
		unsigned char sign;				//�������� ��������: ���� (0-����, 1-�����)
	};
} TSC_FloatStruct;
//#pragma pack(pop)

//------------------------------------
// ������ �������� ������ �� ��������
//------------------------------------
#define SC_IsParamEmpty(a) (a.end<=a.begin)

//------------------------
// ������ ����� ���������
//------------------------
#define SC_ParamLen(a) (a.end-a.begin)			//24.06.2013 - BUGFIX: ������ <a> ���� <pos>

//---------------------
// ��������� � �������
//---------------------
//TSC_ParamBounds SC_ExtractParameter(char *str, unsigned char skipcount, char **str_out);

void			SC_SkipParameter(char *str, unsigned char skipcount, char **str_out);
TSC_ParamBounds	SC_ExtractParameter(char *str, char **str_out);
void			SC_DoubleQuotes(char *Source, char *Dest, unsigned short DestSize);

//TSC_ParamBounds SC_ExtractQuotedParameter(char *str, unsigned char skipcount, char **str_out);

unsigned short SC_ConvertStrTo(unsigned char type, char *str, unsigned char skipcount, char **str_out, void *value);

//unsigned short SC_ConvertQuotedStr(char *str, unsigned char skipcount, char **str_out, char **value, unsigned short maxlen);

unsigned char SC_StrToHex32(char *str, unsigned char len, unsigned long int *value);

unsigned char SC_StrToHex64(char *str, unsigned char len, unsigned long long int *value);

unsigned char SC_StrToUInt16(char *str, unsigned char len, unsigned short int *value);

unsigned char SC_StrToInt32(char *str, unsigned char len, signed long int *value);

unsigned char SC_StrToUInt32(char *str, unsigned char len, unsigned long int *value);

unsigned char SC_StrToUInt64(char *str, unsigned char len, unsigned long long int *value);

unsigned char SC_StrToFloatStruct(char *str, unsigned char len, TSC_FloatStruct *value);

//-------------------------------------
// ����� ����������� ������ ����������
//-------------------------------------
class TParamStr {

public:
	char *FParams;

	unsigned short Result;

	union {
		TSC_FloatStruct		Float;
		char				Char;
		unsigned short		UInt16;
		unsigned long		UInt32;
		unsigned long long	UInt64;
		signed short		Int16;
		signed long			Int32;
		struct {
			char *		  	String;
			unsigned short	Len;
		};
	};

	TParamStr(char *AString);

	char Empty();
	char EndOfString();

	void Skip(unsigned char SkipCount);

	char GetChar();
	char GetUInt16();
	char GetUInt32();
	char GetInt16();
	char GetInt32();
	char GetHex32();
	char GetHex64();

	char GetFloat(char FractionDigits);

//	char GetQuotedString();
	char GetString();

};

#endif
