//--------------------------------------------
// ���� ������� � �������� ��������� ��������
//--------------------------------------------

#include "timers.h"

#include "windows.h"

//=============================================================================================================================================================
// TOneShotTimer - ����� ������� � ����������� ��������
//=============================================================================================================================================================

//------------------------------------------------------
// ��������� ������� ������� (�������� � �������������)
//------------------------------------------------------
void TOneShotTimer::Start(unsigned long Interval) {
	StartTickCount = GetTickCount();
	DeltaTickCount = Interval;
}

//---------------------------------------
// ������� �������� ������������ �������
//---------------------------------------
char TOneShotTimer::IsTimeOut() {
	return (GetTickCount()-StartTickCount) >= DeltaTickCount;
}




