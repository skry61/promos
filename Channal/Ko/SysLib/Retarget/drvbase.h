﻿//----------------------------------------
// Файл описания базового класса драйвера
//----------------------------------------
// target OS: Win32
//----------------------------------------

#ifndef DRVBASEH
#define DRVBASEH

#include <SyncObjs.hpp>

class TDriverThread;

//------------------------
// Базовый класс драйвера
//------------------------
class TDriver {

	friend class TDriverThread;

private:
	TCriticalSection	*CS;						 		//указатель на критическую секцию для потока выполнения обработчика
	TDriverThread		*Thread;					 		//указатель на поток выполнения обработчика 

public:
	__fastcall TDriver();									//конструктор
	__fastcall ~TDriver();									//деструктор

	void (*ExternalProc) ();						 		//указатель на внешнюю процедуру

	void __fastcall CallExternalProcSync();					//функция синхронного вызова внешней процедуры

	void __fastcall Run(TThreadPriority APriority);   	   	//функция запуска драйвера
	void __fastcall Stop();									//функция останова драйвера (завершение потока)
	void __fastcall Suspend();								//функция преостанова драйвера (преостановки потока)

	char __fastcall DelayInternal(unsigned long Interval);	//функция выполнения задержки с контролем команды останова потока

	virtual void Handler() = 0;								//абстрактная функция обработчика драйвера
};

//------------------------------------------------
// Макроопределение для упрощения читаемости кода
//------------------------------------------------
#define Delay(interval)		if (DelayInternal(interval)) return		//задержка на заданный интервал времени в миллисекундах

//--------------------------------------------------------
// Описание класса потока выполнения обработчика драйвера
//--------------------------------------------------------
class TDriverThread : public TThread {

	friend class TDriver;

private:
	TDriver	*Driver;										//указатель на связанный драйвер
	HANDLE	StopEvent;										//идентификатор события останова потока

	void __fastcall Execute();								//главная процедура выполнения потока

public:
	__fastcall TDriverThread(TDriver *ADriver, TThreadPriority APriority);	//конструктор
	__fastcall ~TDriverThread();							//деструктор

	void __fastcall Stop();									//функция подачи команды останова и завершения потока

	void __fastcall CallExternalProc();						//метод, вызываемый синхронно; предназначен для вызова внешней процедуры, указанной в ExternalProc
};


#endif

