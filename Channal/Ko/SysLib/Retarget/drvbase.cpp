//----------------------------------------
// ���� �������� �������� ������ ��������
//----------------------------------------
// target OS: Win32
//----------------------------------------

#include "drvbase.h"
#include <vcl.h>

//=============================================================================================================================================================
// TDriver - ������� ����� ��������
//=============================================================================================================================================================

//-------------
// �����������
//-------------
__fastcall TDriver::TDriver() {
	CS = new TCriticalSection;
	Thread = NULL;
}

//------------
// ����������
//------------
__fastcall TDriver::~TDriver() {
	if(Thread != NULL)
	{
		Stop();
		if(Thread != NULL)
			delete Thread;
	}
	delete CS;
}

//--------------------------
// ������� ������� ��������
//--------------------------
void __fastcall TDriver::Run(TThreadPriority APriority = tpNormal) {
	if(Thread == NULL)
		Thread = new TDriverThread(this,APriority);
	else if(Thread->Suspended)
		Thread->Resume();
}

//-------------------------------
// ������� ������������ ��������
//-------------------------------
void __fastcall TDriver::Suspend() {
	if((Thread != NULL) && !Thread->Suspended)
		Thread->Suspend();
}

//-----------------------------------------------
// ������� �������� �������� (���������� ������)
//-----------------------------------------------
void __fastcall TDriver::Stop() {

	//���� ������� �� ������� � ���������� ������
	if(Thread != NULL)
	{
		CS->Enter();
		if(Thread->Suspended)
		{
			Thread->Resume();
			Sleep(50);
		}
		try {Thread->Stop();}
		__finally {CS->Leave();}
	}
	//������� ��������������� ������
	bool terminated=false;
	DWORD start=GetTickCount();
	while ((GetTickCount()-start)<2000) {	//����-��� 2 �������
		CS->Enter();
		try {if (Thread==NULL) {terminated = true; break;}}
		__finally {CS->Leave();}
		Sleep(50);
		Application->ProcessMessages();
	}
	if (!terminated) Application->MessageBox(AnsiString("Can't terminate protocol handler thread"/* of " + ComPort->GetPortName()*/).c_str(),"Message",MB_OK);
}

//-----------------------------------------------------------------
// ������� ���������� �������� � ��������� ������� �������� ������
//-----------------------------------------------------------------
char __fastcall TDriver::DelayInternal(unsigned long Interval) {
	if(Thread)
		return WaitForSingleObject(Thread->StopEvent,Interval) == WAIT_OBJECT_0;
	else
		Sleep(Interval);
	return true;
}

//----------------------------------------------
// ������� ����������� ������ ������� ���������
//----------------------------------------------
void __fastcall TDriver::CallExternalProcSync() {
	Thread->Synchronize(&Thread->CallExternalProc);
}




//=============================================================================================================
// TDriverThread - ����� ������ ���������� ����������� ��������
//=============================================================================================================

//-------------
// �����������
//-------------
__fastcall TDriverThread::TDriverThread(TDriver *ADriver, TThreadPriority APriority) : TThread(true) {	//������� ����� � ������������ ��������� - Suspended

	Driver = ADriver;

	StopEvent = CreateEvent(NULL, true, false, NULL);				//������� ������������� ������� �������� ������

	Priority = APriority;											//��������� ������������ ��������� ������
	FreeOnTerminate = true;											//��������������� ��� ����������

	Resume();														//��������� �����
}

//------------
// ����������
//------------
__fastcall TDriverThread::~TDriverThread() {
	//������ � ����������� ������ � �������� � �������� ��������� �� ����� - ��� ������� ����, ��� ����� ����� �������
	Driver->CS->Enter();
	try {Driver->Thread = NULL;}
	__finally {Driver->CS->Leave();}
}

//---------------------------------------------------
// ������� ��������� ���������� ����������� ��������
//---------------------------------------------------
void __fastcall TDriverThread::Execute() {

	for (;;) {

		Driver->Handler();		//�������� ���������� ��������

		if (WaitForSingleObject(StopEvent,0) == WAIT_OBJECT_0) break;	//���� ������ ������� ��������, �� ������� �� ����� � ��������� �����
		else Sleep(0);
	};

	CloseHandle(StopEvent);		//����������� ������������� ������� �������� ������
}

//---------------------------------------------------------
// ��������� ������ ������� �� ������� � ���������� ������
//---------------------------------------------------------
void __fastcall TDriverThread::Stop() {
	SetEvent(StopEvent);
	Sleep(0);
}

//------------------------------------------------------------------------------------------------
// �����, ���������� ���������; ������������ ��� ������ ������� ���������, ��������� � ExternProc
//------------------------------------------------------------------------------------------------
void __fastcall TDriverThread::CallExternalProc() {
	if (Driver->ExternalProc) Driver->ExternalProc();
}


