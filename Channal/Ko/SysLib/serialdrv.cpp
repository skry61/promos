//-----------------------------------------------------------------------
// ���� �������� ������� � ������� ��� ��������� ���������������� ������
//-----------------------------------------------------------------------

#include "serialdrv.h"

#include <string.h>

//=============================================================================================================================================================
// TSerialIOHandler - ������� ����� ����������� ����������������� �����/������
//=============================================================================================================================================================

//-------------
// �����������
//-------------
TSerialIOHandler::TSerialIOHandler() {
	Driver = NULL;
}

//----------------------------------------
// ������� ���������� ���������� ��������
//----------------------------------------
void TSerialIOHandler::SetDriver(TSerialDriver *ADriver) {

	if (Driver) Driver->IOHandler = NULL;							//���� ������ ������� �� �������, �� ��������� ����� ������� �������� � ������������ �����/������

	if (ADriver) {													//���� ����� ������� �����...
		if (ADriver->IOHandler) ADriver->IOHandler->Driver = NULL;	//���� ����� ������� ��� ������ � ������������ �����/������, �� � ���� ����������� ��������� ����� � ���� ���������
		ADriver->IOHandler = this;								  	//��������� ����� ������� � ������� ������������ �����/������
	}
	Driver = ADriver;												//���������� ��������� �� ����� �������
}

//=============================================================================================================================================================
// TSerialDriver - ������� ����� �������� ����������������� �����
//=============================================================================================================================================================

//-------------
// �����������
//-------------
TSerialDriver::TSerialDriver(unsigned short RXBufSize, unsigned short TXBufSize) {
	RXBuffer = new TCircularBuffer(RXBufSize);		//������� ��������� ����� ������
	TXBuffer = new char[TXBufSize];					//������� ����� ��������
	TXBufferSize = TXBufSize;
	RXIntercept = NULL;
	TXIntercept = NULL;
	IOHandler = NULL;
   	RXTotalCounter = TXTotalCounter = 0;

}

//------------
// ����������
//------------
TSerialDriver::~TSerialDriver() {
	if (RXBuffer) delete RXBuffer;
	if (TXBuffer) delete [] TXBuffer;
}

//--------------------------------------------------------
// ������� ���������� ���������� ����������� �����/������
//--------------------------------------------------------
void TSerialDriver::SetIOHandler(TSerialIOHandler *AIOHandler) {
	
	if (IOHandler) IOHandler->Driver = NULL;							//���� ������ ���������� �� �������, �� ��������� ����� ������� ����������� � ���������
	
	if (AIOHandler) {													//���� ����� ���������� �����/������ �����...
		if (AIOHandler->Driver) AIOHandler->Driver->IOHandler = NULL; 	//���� ����� ���������� �����/������ ��� ������ � ���������, �� � ���� �������� ��������� ����� � ���� ������������
		AIOHandler->Driver = this;									  	//��������� ����� ���������� � ������� ���������
	}

	IOHandler = AIOHandler;												//���������� ��������� �� ����� ���������� �����/������
}

//---------------------------------------------------------
// ������� ��������� ���������� �� ������ �����
//---------------------------------------------------------
// ��������! ������ ������� ���������� � ���������� __irq!
//---------------------------------------------------------
void TSerialDriver::RXInterruptHandler(char AByte) {

	RXStarted = true;
	RXDoneTimer.Start(40);

	if (RXBuffer->IsFull()) {		//���� ����� �����...
		RXBuffer->Read();				//���������� �� ������ ���� ���� (����� ���������� ����� ��� ������ ������)
		OverflowCounter++;				//����������� ������� ������������ ������
	}

	RXBuffer->Write(AByte);

	RXTotalCounter++;
 	if (RXIntercept) RXIntercept(AByte);

/*	if (!RXBuffer->IsFull()) {
		RXBuffer->Write(AByte);

		//����� ������� ����������� ������������� ���������� ������
		//...
	}
	else {
		OverflowCounter++;
	}*/
}

//----------------------------------------------------------------------------------------
// ������� �������� ���������� ������ ������
//----------------------------------------------------------------------------------------
// ���������� 1, ���� ���� ������� �����-�� ������ � ������ ����� ����� ���������� �����.
// ������ ������� ����� �������� ������ ����������, ��������� ����� ���� ��������� 0.
//----------------------------------------------------------------------------------------
char TSerialDriver::RXCompleted() {
	RXCountWhenCompleted = RXBuffer->Count();			//���������� ���������� ���� � �������� ������ ��� ������������ ������������ �� Release()
	if (RXStarted && RXDoneTimer.IsTimeOut()) {			//���� ����� ��� ����� � ��� ������� ����� ����� ���������� ��������� �����...
		RXStarted = false;									//���������� ���� ������ ������
		return 1;											//������� � 1
	}
	else return 0;
}

//---------------------------------
// ������� ������ ��������� ������
//---------------------------------
void TSerialDriver::FlushRX() {
//	RXBuffer->IncrementRDIndex(RXCountWhenCompleted,GetRXBufferSize());
	RXBuffer->Flush();
}

//------------------------------------------------------
// ��������� ������ ������ � wildcard � �������� ������
//------------------------------------------------------
char TSerialDriver::CheckMessageInternal(const char *Text, unsigned char TextLen, char Wildcard, unsigned short RXLen) {

	//if ((!RXStarted)||(!RXDoneTimer.IsTimeOut())) return false;		//���� ����� �� ������� � ��� �� ����������, �� �������

	//if (Text[0]==0) return true;									//���� ������ ������ ������, �� ������� - ����� ������

	unsigned short rxcounter = RXBuffer->Count();
	unsigned short i=0;												//��������� ������� ������ � ������ = 0

	if (!TextLen) TextLen = strlen(Text);							//���� ����� �������, �� ���������� ����� �� ������

	//---------------------------------
	// ���� ��������� ��������� ������
	//---------------------------------
	for (;; i++) {

		if (rxcounter<(i+TextLen)) return false;					//���� �������� ������ ����� �� �����, �� ������� (��������� �� �������)

		bool found = true;
		const char *TextPtr = Text;

		for (unsigned char j=0; j<TextLen; j++) {
			char ch = *TextPtr++;
			if ((ch!=RXBuffer->ReadOffset(i+j))&&((Wildcard==0)||(ch!=Wildcard))) {found = false; break;}
		}

		if (found) {									//���� ��������� �������...
			MessagePos = i + TextLen;					//���������� ��������� �� ���������� ������� � ������

			if (!RXLen) return true;						//���� �� ������ ����� ������������ ������, �� ������� (��������� �������)
			if (rxcounter>=(i+RXLen)) return true;			//���� ������� ������ �������� ����� ������, �� ������� (��������� �������)
		}
	}
}

//-----------------------------------------------------------
// ������� ��������� ���������� �� ���������� �������� �����
//-----------------------------------------------------------
// ��������! ������ ������� ���������� � ���������� __irq!
//-----------------------------------------------------------
// ���������� ���� �� �������� � ���������� ��������
//-----------------------------------------------------------
unsigned short TSerialDriver::TXInterruptHandler() {
	if (TXIndex < TXCounter) return TXBuffer[TXIndex++]|0xFF00;
	else {TXCounter = TXIndex = 0; return 0;}
}

//---------------------------
// �������� ������ �� ������
//---------------------------
void TSerialDriver::SendBufferOffset(unsigned short Offset, unsigned short Len, unsigned short Timeout) {

	char *TXBufPtr;
	TXBufPtr = TXBuffer + Offset;			//��������� ��������� �� ������ ������ � ������

	if (!Len) Len = strlen(TXBufPtr);		//���� ����� �������, �� ���������� ����� �� ������
	Len += Offset;							//��������� �������� � �����

	RXStarted = false;						//���������� �������� ������

	if (NeedToFlushRXBeforeTX()) RXBuffer->Flush();		//������� �������� �����, ���� �����

	TXIndex = Offset;						//������������� ��������� ������ ��������
	TXCounter = Len;						//������������� ����� ������������� ������

	if (IOHandler) IOHandler->Write();		//�������� ������ ������

	RXTimeoutTimer.Start(Timeout);			//��������� ������ ����-����
 	RXTimeoutTimerInterval = Timeout;		//���������� ��������

	TXTotalCounter += Len-Offset;			//����������� ������� ������������ ������

	if (TXIntercept) TXIntercept(TXBufPtr,Len-Offset);

	//Statistics.Sent++;					//����������� ������� ������������ �������
}

//-----------------------------------------------------------------------
// ������� ������ ����� ��������� ������� (��� ��������� ������� ������)
//-----------------------------------------------------------------------
void TSerialDriver::ReadBuffer(signed short Offset, void *Dest, unsigned short Size) {
	char *Buf = (char *)Dest;
	unsigned short pos = MessagePos + Offset;
	unsigned short maxpos = pos + Size;
	for (; pos<maxpos; Buf++) {
		*Buf = RXBuffer->ReadOffset(pos++);
	}
}

//---------------------------------------------------------------------------------
// ������� ������ ������ � ��������� ������� ������ (��� ��������� ������� ������)
//---------------------------------------------------------------------------------
void TSerialDriver::ReadStr(signed short Offset, char *Dest, unsigned short MaxSize) {
	char *Buf = (char *)Dest;
	unsigned short pos = MessagePos + Offset;
	unsigned short maxpos = pos + MaxSize;
	for (; (pos<maxpos)&&(pos<RXCountWhenCompleted); Buf++) {
		if ((*Buf = RXBuffer->ReadOffset(pos++))==0) break;
	}
	*Buf = 0;
}

//------------------------------
// ������� ������ ����� (8 ���)
//------------------------------
unsigned char TSerialDriver::ReadByte(signed short Offset) {
	return RXBuffer->ReadOffset(MessagePos + Offset);
}

//-------------------------------
// ������� ������ ����� (16 ���)
//-------------------------------
unsigned short TSerialDriver::ReadWord(signed short Offset) {
	return					RXBuffer->ReadOffset(MessagePos + Offset)
		| (((unsigned short)RXBuffer->ReadOffset(MessagePos + Offset + 1))<<8);
}

//----------------------------------------
// ������� ������ �������� ����� (32 ���)
//----------------------------------------
unsigned long TSerialDriver::ReadDWord(signed short Offset) {
	return					RXBuffer->ReadOffset(MessagePos + Offset)
		| (((unsigned short)RXBuffer->ReadOffset(MessagePos + Offset + 1))<<8)
		| (((unsigned  long)RXBuffer->ReadOffset(MessagePos + Offset + 2))<<16)
		| (((unsigned  long)RXBuffer->ReadOffset(MessagePos + Offset + 3))<<24);
}


