//-----------------------------------------------------------------------
// ���� �������� ������� � ������� ��� ��������� ���������������� ������
//-----------------------------------------------------------------------

#ifndef SERIALDRVH
#define SERIALDRVH

#include "buffers.h"
#include "timers.h"
#include "drvbase.h"

//------------------------
// ����� �������� �������		//����������!!! ������� ���-�� �������, ���� ������ (������ �������� �� ������)
//------------------------
class TEventCounter {

private:
	unsigned long	PrevTotalCount;
	unsigned long	PrevMs;
	unsigned long	RatePerSecond;

public:
	unsigned long	TotalCount;

	void Increment() {TotalCount++;}

	unsigned long GetRatePerSecond() {
		unsigned long TempTotalCount = TotalCount;
		unsigned long NowMs = GetTickCount();

		if (NowMs-PrevMs<1000) return RatePerSecond;

		RatePerSecond = ((TempTotalCount-PrevTotalCount)*1000ULL)/(NowMs-PrevMs);
		PrevMs = NowMs;
		PrevTotalCount = TempTotalCount;
		return RatePerSecond;
	}

};




class TSerialDriver;

//----------------------------------------------------------
// ������� ����� ����������� ����������������� �����/������
//----------------------------------------------------------
class TSerialIOHandler {

public:
	TSerialDriver *Driver;							//��������� �� ��������� �������

	TSerialIOHandler();								//�����������
	void SetDriver(TSerialDriver *ADriver);			//������� ���������� ���������� ��������

	virtual void Write() = 0;						//��������� �������� ������ - ������ ���� ���������� � ������-����������

};

//------------------------------------------------
// ������� ����� �������� ����������������� �����
//------------------------------------------------
class TSerialDriver : public TDriver {

public:
	TSerialIOHandler *IOHandler;										//��������� ���������� �����-������

	TCircularBuffer	*RXBuffer;											//����� ������

	unsigned long	OverflowCounter;									//������� ������ ������ �� ������ ��-�� ������������ ������

	char			RXStarted;											//���� ������ ������
	TOneShotTimer	RXDoneTimer;										//������ ����������� ���������� ������
	TOneShotTimer	RXTimeoutTimer;										//������ ����-���� �������� ������
	unsigned short	RXCountWhenCompleted;								//���������� ���� � �������� ����� ��� ������������ ������������ �� Release()
  	unsigned long	RXTotalCounter;										//������� ������ ���������� �������� ����

 	virtual unsigned char RXDoneTimerInterval() {return 30;}			//����������� �������, ������������ ��������� ������� ����� ����������� ��������� ������

	unsigned short	MessagePos;											//������� � ������, � ������� ���������� ���������, ��������� �������� CheckMessageInternal()

	char			*TXBuffer;											//����� ��������
	unsigned short	TXBufferSize;										//������ ������ ��������
	unsigned short	TXIndex;											//������ �� ��������
	unsigned short	TXCounter;											//������� ���������� ����
	unsigned long	TXTotalCounter;										//������� ������ ���������� ���������� ����


	TSerialDriver(unsigned short RXBufSize, unsigned short TXBufSize);	//�����������
	~TSerialDriver();													//����������

  	void SetIOHandler(TSerialIOHandler *AIOHandler);					//������� ���������� ���������� ����������� �����/������ (����������� ������� SetDriver)

	void RXInterruptHandler(char AByte);								//���������� ���������� �� ������ �����
	char RXCompleted();													//������� �������� ���������� ������ ������
	void FlushRX();														//������� ������ ��������� ������

	char CheckMessageInternal(const char *Text, unsigned char TextLen, char Wildcard, unsigned short RXLen);	//������� ������ �������� ������ ��� ������������������ � �������� ������

	unsigned short TXInterruptHandler();								//���������� ���������� �� ���������� �������� �����
	char inline TXCompleted() {return (TXCounter==0);}					//������� �������� ���������� �������� ������

	void SendBufferOffset(unsigned short Offset, unsigned short Len, unsigned short Timeout);	//�������� ������ �� ������ �� ��������� � �������� ����-�����

	void ReadBuffer(signed short Offset, void *Dest, unsigned short Size);	//������ ����� ��������� �������
	void ReadStr(signed short Offset, char *Dest, unsigned short MaxSize);	//������ ������ � ��������� ������� ������
	unsigned char  ReadByte(signed short Offset);							//������� ������ ����� (8 ���)
	unsigned short ReadWord(signed short Offset);							//������� ������ ����� (16 ���)
	unsigned long ReadDWord(signed short Offset);							//������� ������ �������� ����� (32 ���)

	//TEventCounter	RXTrafficMeter;

	virtual char NeedToFlushRXBeforeTX() {return true;}					//�������, ������������ ������� ������������� ���������� �������� ����� ����� ������ ���������, ����� ���� �������������� � ����������
 	void (* RXIntercept)(char Byte);									//��������� �� �������-����������� ������
	void (* TXIntercept)(char *Buffer, unsigned short Len);				//��������� �� �������-����������� ��������
 	unsigned long RXTimeoutTimerInterval;								//�������� ������� ����-����

};

//------------------------------------------------
// ���������������� ��� ��������� ���������� ����
//------------------------------------------------
#define CheckMessageStr(str)						CheckMessageInternal(str,0,0,0)					//����� ������
#define CheckMessageStrWildcard(str,wildcard)		CheckMessageInternal(str,0,wildcard,0)			//����� ������ � wildcard (��������, "abcd?egh", ��� '?' - ��� wildcard)
#define CheckMessageStrLen(str,msglen)				CheckMessageInternal(str,0,0,msglen)			//����� ������ � ������������� ������ ����� ��������� ���������
#define CheckMessageBinary(str,strlen)				CheckMessageInternal(str,strlen,0,0)			//����� �������� ������������������
#define CheckMessageBinaryLen(str,strlen,msglen)	CheckMessageInternal(str,strlen,0,msglen)		//����� �������� ������������������ � ������������� ������ ����� ��������� ���������

#define SendBuffer(len,timeout)			SendBufferOffset(0,len,timeout)			//�������� ������ � ������ ������
#define SendStr(timeout)				SendBufferOffset(0,0,timeout)			//�������� ������ � ������ ������
#define SendStrOffset(offset,timeout)	SendBufferOffset(offset,0,timeout)		//�������� ������ �� ��������� ������ �� ���������

#endif



