//-------------------------------
// ���� �������� ������� �������
//-------------------------------

#include "buffers.h"

//=============================================================================================================================================================
// TCircularBuffer - ����� ���������� ������
//=============================================================================================================================================================

//-------------
// �����������
//-------------
TCircularBuffer::TCircularBuffer(unsigned short ASize) {
	Buffer = new char[ASize];
	Size = ASize;
	Reset();
}

//------------
// ����������
//------------
TCircularBuffer::~TCircularBuffer() {
	delete [] Buffer;
}

//-------------------------
// ��������� ������ ������
//-------------------------
void TCircularBuffer::Reset() {
	RDIndex = WRIndex = 0;
}

//--------------------------
// ��������� ������� ������
//--------------------------
void TCircularBuffer::Flush() {
	RDIndex = WRIndex;
}

//------------------------
// ������� ������ � �����
//------------------------
void TCircularBuffer::Write(char AByte) {
	Buffer[WRIndex] = AByte;
	WRIndex = (WRIndex + 1) % Size;
}

//--------------------------
// ������� ������ �� ������
//--------------------------
char TCircularBuffer::Read() {
	char result = Buffer[RDIndex];
	RDIndex = (RDIndex + 1) % Size;
	return result;
}

//--------------------------------
// ������� �������� ���� �� �����
//--------------------------------
char TCircularBuffer::IsEmpty() {
	return RDIndex == WRIndex;
}

//------------------------------------
// ������� �������� �������� �� �����
//------------------------------------
char TCircularBuffer::IsFull() {
	return RDIndex == ((WRIndex + 1) % Size);
}

//---------------------------------------------
// ������� ���������� ���������� ���� � ������
//---------------------------------------------
unsigned short TCircularBuffer::Count() {
	unsigned short __WRIndex = WRIndex;
	return __WRIndex - RDIndex + ((__WRIndex>=RDIndex)?(0):(Size));
}

//--------------------------------------
// ������� ������ �� ������ �� ��������
//--------------------------------------
char TCircularBuffer::ReadOffset(signed short AOffset) {
	if (AOffset>=0) return Buffer[(RDIndex + AOffset) % Size];
	else return Buffer[RDIndex + AOffset + Size];
}

//-----------------------------------
// ������� ���������� ������� ������
//-----------------------------------
void TCircularBuffer::IncrementRDIndex(unsigned short AValue) {
	unsigned short count = Count();
	if (AValue > count) AValue = count;
	RDIndex = (RDIndex + AValue) % Size;
}



