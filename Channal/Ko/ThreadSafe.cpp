//---------------------------------------------------------------------------


#pragma hdrstop

#include "ThreadSafe.h"
#include "SyncObjs.hpp"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//=============================================================================================================================================================
// ����� TThreadSafeBool - ���������������� bool
//=============================================================================================================================================================

//-----------------------
// �������� ������������
//-----------------------
TThreadSafeBool& TThreadSafeBool::operator= (bool AValue) {
	CS->Enter();
	try {Value = AValue;}
	__finally {CS->Leave();}
	return *this;
}

//----------------------------
// �������� ��������� "�����"
//----------------------------
bool TThreadSafeBool::operator==(bool AValue) {
	bool value;
	CS->Enter();
	try {value = Value;}
	__finally {CS->Leave();}
	return value==AValue;
}

//-------------------------------
// �������� ��������� "�� �����"
//-------------------------------
bool TThreadSafeBool::operator!=(bool AValue) {
	bool value;
	CS->Enter();
	try {value = Value;}
	__finally {CS->Leave();}
	return value!=AValue;
}

//--------------------
// �������� ���������
//--------------------
bool TThreadSafeBool::operator!() {
	bool value;
	CS->Enter();
	try {value = Value;}
	__finally {CS->Leave();}
	return !value;
}


//=============================================================================================================================================================
// ����� TThreadSafeU64 - ���������������� U64
//=============================================================================================================================================================

//-----------------------
// �������� ������������
//-----------------------
TThreadSafeU64& TThreadSafeU64::operator= (unsigned long long AValue) {
	CS->Enter();
	try {Value = AValue;}
	__finally {CS->Leave();}
	return *this;
}

//-------------------
// �������� ��������
//-------------------
unsigned long long TThreadSafeU64::operator~() {
	unsigned long long result;
	CS->Enter();
	try {result = ~Value;}
	__finally {CS->Leave();}
	return result;
}


//=============================================================================================================================================================
// ����� TThreadSafeAnsiString - ���������������� AnsiString
//=============================================================================================================================================================

//-----------------------
// �������� ������������
//-----------------------
TThreadSafeAnsiString& TThreadSafeAnsiString::operator=(AnsiString AString) {
	CS->Enter();
	try {String = AString;}
	__finally {CS->Leave();}
	return *this;
}

//--------------------------------------------
// �������� �������� (��� ��������� ��������)
//--------------------------------------------
AnsiString TThreadSafeAnsiString::operator~() {
	AnsiString result;
	CS->Enter();
	try {result = String;}
	__finally {CS->Leave();}
	return result;
}


