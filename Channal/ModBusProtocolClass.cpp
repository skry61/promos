//---------------------------------------------------------------------------


#pragma hdrstop

#include "ModBusProtocolClass.h"
#include "ProtocolClass.h"

//*****    ������� � �����   *******
enum mb {
	mbNODE,			// ������� Node
	bmCMD,			// ������� �������
	bmADDRH,		// ������� ������ ������
	bmADDRL,
	mbCOUNTH,		// ������� ��������
	mbCOUNTL,
	mbDATA,			// ������� ������
	mbCRC		=6
};

// ���� ������ ��������� MODBUS
#define Cmd_READ_N_OBITS       1
#define Cmd_READ_N_IBITS       2
#define Cmd_READ_N_WORDS       3
#define Cmd_WRITE_1_BIT        5
#define Cmd_WRITE_1_WORD       6
#define Cmd_WRITE_N_WORDS      16


//---------------------------------------------------------------------------
#pragma package(smart_init)
//******************************************************************************
//*                                                                            *
//*            ��������� �������� CRC ���������� ��������� MODBUS              *
//*                                                                            *
//******************************************************************************
/* Table of CRC values for high�order byte */
static unsigned char auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40
} ;
/* Table of CRC values for low�order byte */
static char auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
0x40
} ;

//******************************************************************************
//*                                                                            *
//*            ��������� �������� CRC ���������� ��������� MODBUS              *
//*                                                                            *
//******************************************************************************
WORD TModBusProtocolRTU::CalculateCRC( BYTE* Buf, BYTE Count ){
	unsigned char uchCRCHi = 0xFF ;  /* high byte of CRC initialized     */
	unsigned char uchCRCLo = 0xFF ;  /* low byte of CRC initialized      */
	unsigned uIndex ; 				 /* will index into CRC lookup table */
	while(Count--){
	   uIndex = uchCRCHi ^ *Buf++ ;  /* calculate the CRC */
	   uchCRCHi = uchCRCLo ^ auchCRCHi[uIndex] ;
	   uchCRCLo = auchCRCLo[uIndex] ;
	}
	return ((uchCRCHi << 8) | uchCRCLo) ;
}

//******************************************************************************
//*                                                                            *
//*            ��������� �������� CRC ���������� ��������� MODBUS              *
//*                                                                            *
//******************************************************************************
bool TModBusProtocolRTU::CheckCRC( char * Buffer, int Len){
 int i = Len - 2;
 return(( Buffer[ i +1 ] | (Buffer[ i ] << 8) ) == CalculateCRC(Buffer,i ));
}

//---------------------------------------------------------------------------
int TModBusProtocolRTU::Parser(char *OutBuf)
{
	int Err = SERIALERROR;
	int Len;
	char * pData;


	if(ProtocolClass)
	{
			ProtocolClass->MessagePos = 0;
			Len = ProtocolClass->RXCountWhenCompleted > 128? 128:ProtocolClass->RXCountWhenCompleted;
			char *Buf = new char[Len+4];
			Buf[0] = 'R';
			ProtocolClass->ReadBuffer(0, &Buf[1], Len);	//������ ����� ��������� �������
			Buf[Len] = 0;
			ProtocolClass->Log(AnsiString(Buf));
			delete Buf;
//!!!!
 //			Sleep(100);

			if(ProtocolClass->Echo)                                                       // ���� � ������� ���
				ProtocolClass->RXBuffer->IncrementRDIndex(MsgCount); // �������� ���
			if (ProtocolClass->CheckMessageBinary("\x0D\x0A",2))
			{

				int MessageEnd = ProtocolClass->MessagePos-2;
				if (ProtocolClass->CheckMessageBinary(":",1))
				{
					int i = (MessageEnd - ProtocolClass->MessagePos);
					if (i>=8)
					{
						char BufASCII[512];
						char BufRTU[255];

						ProtocolClass->ReadBuffer(0, BufASCII, i);	//������ ����� ��������� �������
						if(DecodeTextToBinModBus(BufRTU,BufASCII, i))
						{
							if(CheckCRC( BufRTU,i/2))
							{
								switch(BufRTU[1])
								{
									case  Cmd_READ_N_OBITS:
									case  Cmd_READ_N_IBITS:
										Len = BufRTU[2];
										pData = &BufRTU[3];
										while(Len--)
										{
											*OutBuf++ = *pData++;
										}
										break;

									case  Cmd_READ_N_WORDS:
										Len = BufRTU[2]/2;
										pData = &BufRTU[3];
										while(Len--)
										{
											char b = *pData++;
											*OutBuf++ = *pData++;
											*OutBuf++ = b;
										}
										break;
								}
								return 0;
							}
							Err++;
						}
						Err++;
					}
					Err++;
				}
				Err++;
			}
			Err++;
	}
	Err++;
	return Err;
}

//---------------------------------------------------------------------------
bool TModBusProtocolRTU::FillBuffer(unsigned char Cmd, WORD Addr, unsigned char *Len,char * Data, char * OutBuf)
{
	MsgCount = 6;
	if(ProtocolClass)
	{
		OutBuf[0] = ProtocolClass->Node;
		OutBuf[1] = ProtocolCMD = GetCMD(Cmd,Addr);
		OutBuf[2] = Addr >> 8;
		OutBuf[3] = Addr & 0x0ff;
		switch(ProtocolCMD)
		{
			case Cmd_READ_N_IBITS:
			case Cmd_READ_N_OBITS:
			case Cmd_READ_N_WORDS:
			case Cmd_WRITE_N_WORDS:
				OutBuf[4] = 0;//*Len >> 8;
				OutBuf[5] = *Len & 0x0ff;
				break;
			case Cmd_WRITE_1_BIT:
				OutBuf[4] = *Data?0xff:00;
				OutBuf[5] = 0;
				break;
			case Cmd_WRITE_1_WORD:
				OutBuf[4] = Data[1];
				OutBuf[5] = Data[0];
				break;
		}
		if(ProtocolCMD == Cmd_WRITE_N_WORDS)
		{
			OutBuf[MsgCount++] = *Len << 1;
			while((*Len)--)
			{
				OutBuf[MsgCount++] = Data[1];
				OutBuf[MsgCount++] = Data[0];
				Data +=2;
			}
		}
		WORD CRC = CalculateCRC(OutBuf,MsgCount);
		OutBuf[MsgCount++] = CRC >> 8;
		OutBuf[MsgCount++] = CRC & 0x0ff;
		*Len = MsgCount;
		return true;
	}else
	return false;
}

//---------------------------------------------------------------------------
char  TModBusProtocolRTU::GetCMD(char CommonCMD,WORD Addr)
{
	switch (CommonCMD) {
	case Cmd_READWORD:  	return  Cmd_READ_N_WORDS;
	case Cmd_READNBITS:
		if((Addr>=0x500)&&(Addr<0x600))
			return  Cmd_READ_N_OBITS;
		if((Addr>=0x800)&&(Addr<0xB100))
			return  Cmd_READ_N_OBITS;
		if((Addr>=0x400)&&(Addr<0x500))
			return  Cmd_READ_N_IBITS;
	case Cmd_READNWORDS:  	return  Cmd_READ_N_WORDS;
	case Cmd_WRITE1BIT:   	return  Cmd_WRITE_1_BIT;
	case Cmd_WRITE1WORD:   	return  Cmd_WRITE_1_WORD;
	case Cmd_WRITENWORDS:  	return  Cmd_WRITE_N_WORDS;

	default:  return 0;
	}
}


//---------------------------------------------------------------------------
int TModBusProtocolASCII::Parser(char *OutBuf)
{
	int Err = SERIALERROR;
	int Len;
	char * pData;


	if(ProtocolClass)
	{
			ProtocolClass->MessagePos = 0;
			Len = ProtocolClass->RXCountWhenCompleted > 128? 128:ProtocolClass->RXCountWhenCompleted;
			char *Buf = new char[Len+4];
			Buf[0] = 'R';
			ProtocolClass->ReadBuffer(0, &Buf[1], Len);	//������ ����� ��������� �������
			Buf[Len] = 0;
			ProtocolClass->Log(AnsiString(Buf));
			delete [] Buf;
//!!!!
 //			Sleep(100);

			if(ProtocolClass->Echo)                                                       // ���� � ������� ���
				if (ProtocolClass->CheckMessageBinary("\x0D\x0A",2))
					ProtocolClass->RXBuffer->IncrementRDIndex(ProtocolClass->MessagePos); // �������� ���
				else
				{
					Err++;
					return Err;
				}
			if (ProtocolClass->CheckMessageBinary("\x0D\x0A",2))
			{

				int MessageEnd = ProtocolClass->MessagePos-2;
				if (ProtocolClass->CheckMessageBinary(":",1))
				{
					int i = (MessageEnd - ProtocolClass->MessagePos);
					if (i>=8)
					{
						char BufASCII[512];
						char BufRTU[255];

						ProtocolClass->ReadBuffer(0, BufASCII, i);	//������ ����� ��������� �������
						if(DecodeTextToBinModBus(BufRTU,BufASCII, i))
						{
							if(CheckCRC( BufRTU,i/2))
							{
								switch(BufRTU[1])
								{
									case  Cmd_READ_N_OBITS:
									case  Cmd_READ_N_IBITS:
										Len = BufRTU[2];
										pData = &BufRTU[3];
										while(Len--)
										{
											*OutBuf++ = *pData++;
										}
										break;

									case  Cmd_READ_N_WORDS:
										Len = BufRTU[2]/2;
										pData = &BufRTU[3];
										while(Len--)
										{
											char b = *pData++;
											*OutBuf++ = *pData++;
											*OutBuf++ = b;
										}
										break;
								}
								return 0;
							}
							Err++;
						}
						Err++;
					}
					Err++;
				}
				Err++;
			}
			Err++;
	}
	Err++;
	return Err;
}

//---------------------------------------------------------------------------
bool TModBusProtocolASCII::FillBuffer(unsigned char Cmd, WORD Addr, unsigned char *Len,char * Data, char * OutBuf)
{   char Buf[255];
	TModBusProtocolRTU::FillBuffer(Cmd, Addr, Len,Data,Buf);
	*Len = CodeBinToTextModBus(OutBuf,Buf,*Len);
	return true;
}

//---------------------------------------------------------------------------
WORD TModBusProtocolASCII::CalculateCRC( BYTE* Buf, BYTE Count )
{
	char CRC = 0;
	while(Count--)	CRC+=*Buf++;
	return (~CRC+1) << 8;
}
//---------------------------------------------------------------------------
bool TModBusProtocolASCII::CheckCRC( char * Buffer, int Len)
{
	return( (BYTE)Buffer[Len-1] == (CalculateCRC(Buffer,Len-1 )>>8));
}

//---------------------------------------------------------------------------
bool TModBusProtocolASCII::ReadWArray(U16 Addr,short Count, short *pVs){return true;};

//	AnsiString sCtrl     = "08 10 00 02 00 01 02 00 30 00 00"; //������ ����������
//	byte BytesOut_x10 [] = {8,0x10,0, 2, 0, 1, 2,0,0x30, 0, 0};
//	byte BytesOut_x03[] = {0x01, 0x03, 0x04, 0x00, 0x00, 0x04};



