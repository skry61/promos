 //---------------------------------------------------------------------------

#ifndef PLC_DrvH
#define PLC_DrvH
//---------------------------------------------------------------------------
#include "ProtocolClass.h"        /*ifd 95 07*/
#include "ModBusProtocolClass.h"
#include "..\Src\Robot.h"
#include "..\Src\SysPar.h"
#define DAMAGE_ERR        10        /*����������� ����� - ���� ��������� ������*/
//------------ D ������� -------------
#define D_ADDR		  	  0x1000  	/* ����� D0 �������*/
#define D_ADDR_KADR		  600       /* ����� ����� (��������������� 40 ����)*/

#define D_ADDR_PARAM	  640       /* ����� ���������� (��������������� 40 ����)*/
/*"[Feed]"
						  640		short parfG0_Hor;      		//  ������� ��� ��������������
						  641		short parfG0_Vert;          //  ������� ��� ������������
						  642		short parfvStep;            //  �������� ����
						  643		short parTableCycle;        //  ���� ������ �������������:

//"[AVC]"
						  644		short parAVCsens;           //  ����������������
//"[Torch]"
						  645		short parI_StepCorr;        //  ��� ��������� ����
//"[Feed]"
						  646		short parfG1_Hor;         	//  ������������ �������� �� �or
						  647		short parfG1_Vert;         	//  ������������ �������� �� Vert
						  648		short parfG1_Table;       	//  ������������ �������� ���������

//"[Wire]"
						  649		short parWireV_StepCorr;    //  ��� ��������� ��������
//"[Feed]"
						  650		short parfG0_Table;       	//  �������� �������� ���������

//"[Oscillation]"); ��� ������ ������
						  651		short parosV_StepCorr;      //  ��� ��������� �������� ������
						  652		short parosS_StepCorr;      //  ��� ��������� ���������� ��������
//[Delay]
						  653		short parDelFeedWireStart;  // ����� ������ ���������
						  654		short parDelFeedAxis;       // ������ ����
						  655		short parDelFeedWireStop;   // ���� ������ ���������
						  656		short parDelPowerOff;       // ���������� ���������

						  657		short parREADY;  		    //  ������� ��� ����������� - ��������� ���������  = 1
*/


#define D_ADDR_RESERV	  680       /* ����� ������� (��������������� 20 ����)*/

#define D_ADDRWORK_KADR   700    	/* ����� �������� ����� (��������������� 40 ����)*/
#define D_ADDR_PROCESS_VALUES  740  /* ����� ������� ������ (��������������� 40 ����)*/
									/*  740 - Error,   ������ > 10 - ������, ���� ��������� ������
										741 - ��������� ��������,
										742 - ������� ����,
										743 - ������� ��� �������
										744 - ������� ��� ���������
										745 - ����������
										746 - ������ ���������
										747 - ��� ��������� ������ ���������
										748 - ������ ����  TiG
										749 - ������ ����  MiG
									*/
#define D_ADDR_PROCESS_FEED_VALUES  750  /* ����� ������� ������ (��������������� 10 ����)*/
									/*
										750 - ����
										751 - ������� �� �����������
										752 - ������� �� ���������
										753 - ������� ���������
									*/

#define D_ADDR_PROCESS_RENT_X    754 /*	754 - ������ RentX			*/

//D760..D773   - ����������  � BUSINTX11  14 ����
#define D_ADDRBUSINTX11_Ctrl   760    	/* ����� ����������  � BUSINTX11*/
//D774..D779   - ���������� �� BUSINTX11    6 ����
#define D_ADDRBUSINTX11_Stat   780    	/* ����� - ���������� �� BUSINTX11*/
//------------ � ������� -------------
#define M_ADDR  		  0x800		/* ����� �0 �������*/
#define M_ADDR_CONTROL 	  200
//------------ XY ������� -------------
#define Y_ADDR  	      0x500     /* ����� Y0 �������*/
#define X_ADDR 			  0x400     /* ����� X0 �������*/


class TPLCDriver : public TProtocolClass {
	int SliceCount;
	int ErrorCount;
	BYTE	Status;		//��� ���������
	short	RobotError;	//��� ������ ������
	TBitStatusByte PredBitStatusByte;                	    // ����� ��������� ����������
	TBitStatusByte CurBitStatusByte;                 	 	// ����� ��������� �������

public:
	TPLCDriver(TLogMemoQueue *aLogQueue) : TProtocolClass(aLogQueue) {
		TProtocol *Protocol = new TModBusProtocolASCII();
		SetProtocol(Protocol);
		SliceCount = 0;
		Status = robNONE;
		ErrorCount = 0;
		RobotError = 0;	//��� ������ ������
	}
	virtual void Handler();
	virtual const char * Name()	{return "PLCDriver";}			//�������, ������������ ��� ��������
	int GetError(){return ErrorCount;};
	int GetErrorCode(){return RobotError;};
public:
	virtual char NeedToFlushRXBeforeTX() {return false;}
	bool SetBuffer(TLineToPLC *pLoadBuf);
	bool SetSysParam(sSys *SysPar);
	DWORD GetProsStatusData();
//	TBitStatusByteEx GetProsStatusDataEx();
//-----------------------------------------------------------------------------
	bool SetControlBufBusy();
	bool SetControl_LasrKadr();
	bool SetControlPC_Ready();
	bool ReSetControlPC_Ready();
	bool SetControlPC_Err();

	bool ClearControlSaveParam();
	int	 GetWorkBuffer(TLineToPLC *pLoadBuf);
	bool SetControlPause();
	TPosStruct GetPosition();
	short GetErrorPLC();
	int GetProsStatus();
	int GetSliceCount(){return SliceCount;}
	double Get_Cur_I();//{return BUSintX11? BUSintX11->Get_Cur_I():10;};            // �������� ������� ���
	double Get_Cur_B();//{return BUSintX11? BUSintX11->Get_Cur_B():2;};             // �������� ������� ����������
	double Get_Cur_V();//{return BUSintX11? BUSintX11->Get_Cur_V():1;};             // �������� ������� �������� ���������
	double Get_Cur_F();//{return BUSintX11? BUSintX11->Get_Cur_V():1;};             // �������� ������� ������ ����
};


#endif
