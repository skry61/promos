object OptionForm: TOptionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1081
  ClientHeight = 542
  ClientWidth = 248
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 151
    Top = 13
    Width = 90
    Height = 25
    Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 151
    Top = 38
    Width = 90
    Height = 25
    Caption = #1054#1090#1082#1083#1102#1095#1080#1090#1100
    TabOrder = 2
    OnClick = Button2Click
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 137
    Height = 217
    Caption = ' '#1055#1072#1088#1072#1084#1077#1090#1088#1099' TCP'
    TabOrder = 0
    object TCPRXLabel: TLabel
      Left = 106
      Top = 185
      Width = 24
      Height = 13
      Align = alCustom
      Alignment = taRightJustify
      Caption = '0000'
      Layout = tlCenter
    end
    object TCPTXLabel: TLabel
      Left = 106
      Top = 197
      Width = 24
      Height = 13
      Align = alCustom
      Alignment = taRightJustify
      Caption = '0000'
      Layout = tlCenter
    end
    object Label7: TLabel
      Left = 75
      Top = 185
      Width = 13
      Height = 13
      Caption = 'Rx'
    end
    object Label8: TLabel
      Left = 75
      Top = 197
      Width = 12
      Height = 13
      Caption = 'Tx'
    end
    object Label6: TLabel
      Left = 16
      Top = 141
      Width = 53
      Height = 13
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object AddrEdit: TLabeledEdit
      Left = 10
      Top = 36
      Width = 118
      Height = 21
      EditLabel.Width = 79
      EditLabel.Height = 13
      EditLabel.Caption = #1040#1076#1088#1077#1089' '#1089#1077#1088#1074#1077#1088#1072':'
      TabOrder = 0
      Text = '127.0.0.1'
    end
    object PortEdit: TLabeledEdit
      Left = 10
      Top = 76
      Width = 118
      Height = 21
      EditLabel.Width = 29
      EditLabel.Height = 13
      EditLabel.Caption = #1055#1086#1088#1090':'
      TabOrder = 1
      Text = '20332'
    end
    object cbSimulTCP: TCheckBox
      Left = 10
      Top = 185
      Width = 46
      Height = 17
      Caption = 'Simul'
      TabOrder = 2
    end
    object NodeEditTCP: TLabeledEdit
      Left = 10
      Top = 116
      Width = 118
      Height = 21
      EditLabel.Width = 29
      EditLabel.Height = 13
      EditLabel.Caption = 'Node:'
      TabOrder = 3
      Text = '2'
    end
  end
  object TCPStateLabel: TPanel
    Left = 151
    Top = 69
    Width = 90
    Height = 20
    BevelOuter = bvNone
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 239
    Width = 137
    Height = 295
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' COM-'#1087#1086#1088#1090#1072' 1'
    TabOrder = 5
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 29
      Height = 13
      Caption = #1055#1086#1088#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 60
      Width = 76
      Height = 13
      Caption = #1041#1080#1090' '#1074' '#1089#1077#1082#1091#1085#1076#1091':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 100
      Width = 72
      Height = 13
      Caption = #1041#1080#1090#1099' '#1076#1072#1085#1085#1099#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 140
      Width = 52
      Height = 13
      Caption = #1063#1077#1090#1085#1086#1089#1090#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 74
      Top = 263
      Width = 13
      Height = 13
      Caption = 'Rx'
    end
    object COMRXLabel: TLabel
      Left = 106
      Top = 263
      Width = 24
      Height = 13
      Align = alCustom
      Alignment = taRightJustify
      Caption = '0000'
      Layout = tlCenter
    end
    object COMTXLabel: TLabel
      Left = 106
      Top = 275
      Width = 24
      Height = 13
      Align = alCustom
      Alignment = taRightJustify
      Caption = '0000'
      Layout = tlCenter
    end
    object Label12: TLabel
      Left = 74
      Top = 275
      Width = 12
      Height = 13
      Caption = 'Tx'
    end
    object Label5: TLabel
      Left = 16
      Top = 220
      Width = 53
      Height = 13
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object PortComboBox: TComboBox
      Left = 10
      Top = 36
      Width = 118
      Height = 21
      Style = csDropDownList
      ItemHeight = 0
      TabOrder = 0
      OnKeyPress = PortComboBoxKeyPress
    end
    object BaudrateComboBox: TComboBox
      Left = 10
      Top = 76
      Width = 118
      Height = 21
      Style = csDropDownList
      ItemHeight = 0
      TabOrder = 1
    end
    object BitCountComboBox: TComboBox
      Left = 10
      Top = 116
      Width = 118
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        '5'
        '6'
        '7'
        '8')
    end
    object ParityComboBox: TComboBox
      Left = 10
      Top = 156
      Width = 118
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        #1063#1077#1090
        #1053#1077#1095#1077#1090
        #1053#1077#1090
        #1052#1072#1088#1082#1077#1088
        #1055#1088#1086#1073#1077#1083)
    end
    object cbSimulCOM: TCheckBox
      Left = 10
      Top = 259
      Width = 46
      Height = 17
      Caption = 'Simul'
      TabOrder = 4
      OnClick = cbSimulCOMClic
    end
    object NodeEditMB: TLabeledEdit
      Left = 10
      Top = 196
      Width = 118
      Height = 21
      EditLabel.Width = 35
      EditLabel.Height = 13
      EditLabel.Caption = '  Node:'
      TabOrder = 5
      Text = '1'
    end
    object cbEcho: TCheckBox
      Left = 10
      Top = 274
      Width = 46
      Height = 17
      Caption = #1069#1093#1086
      TabOrder = 6
    end
  end
  object ConnectButton: TButton
    Left = 150
    Top = 245
    Width = 90
    Height = 25
    Caption = '&'#1055#1086#1076#1082#1083#1102#1095#1080#1090#1100
    TabOrder = 6
    OnClick = ConnectButtonClick
  end
  object ResetTCPButton: TButton
    Left = 151
    Top = 200
    Width = 88
    Height = 25
    Caption = #1054#1073#1085#1091#1083#1080#1090#1100
    TabOrder = 3
    WordWrap = True
    OnClick = ResetTCPButtonClick
  end
  object COMStateLabel: TPanel
    Left = 150
    Top = 276
    Width = 90
    Height = 20
    BevelOuter = bvNone
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 7
  end
  object btLog: TButton
    Left = 150
    Top = 312
    Width = 90
    Height = 28
    Caption = '&'#1051#1086#1075
    TabOrder = 8
    OnClick = btLogClick
  end
  object ResetCOMButton: TButton
    Left = 151
    Top = 510
    Width = 88
    Height = 25
    Caption = #1054#1073#1085#1091#1083#1080#1090#1100
    TabOrder = 9
    WordWrap = True
    OnClick = ResetCOMButtonClick
  end
  object COM_ProtocolComboBox: TComboBox
    Left = 18
    Top = 476
    Width = 118
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 10
    Text = 'ModBus ASCII'
    Items.Strings = (
      'ModBus ASCII'
      'ModBus RTU')
  end
  object TCP_ProtocolComboBox: TComboBox
    Left = 18
    Top = 166
    Width = 118
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 11
    Text = 'ModBus ASCII'
    Items.Strings = (
      'ModBus ASCII'
      'ModBus RTU')
  end
  object LErrCount: TLabeledEdit
    Left = 152
    Top = 369
    Width = 89
    Height = 21
    BiDiMode = bdLeftToRight
    EditLabel.Width = 44
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdLeftToRight
    EditLabel.Caption = #1054#1096#1080#1073#1086#1082':'
    EditLabel.ParentBiDiMode = False
    ParentBiDiMode = False
    TabOrder = 12
    Text = '0'
  end
  object cbPLC_to_Simul: TCheckBox
    Left = 151
    Top = 396
    Width = 89
    Height = 17
    Caption = 'PLC=>Simul'
    TabOrder = 13
  end
  object GroupBox4: TGroupBox
    Left = 151
    Top = 420
    Width = 89
    Height = 78
    Caption = #1040#1082#1090#1080#1074#1085#1086#1089#1090#1100
    TabOrder = 14
    object Bevel1: TBevel
      Left = 37
      Top = 24
      Width = 49
      Height = 17
    end
    object Bevel2: TBevel
      Left = 37
      Top = 47
      Width = 49
      Height = 17
    end
    object Label16: TLabel
      Left = 40
      Top = 26
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label17: TLabel
      Left = 40
      Top = 49
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label20: TLabel
      Left = 3
      Top = 26
      Width = 26
      Height = 13
      Caption = 'Timer'
    end
    object Label21: TLabel
      Left = 3
      Top = 49
      Width = 34
      Height = 13
      Caption = 'Thread'
    end
  end
  object TCPClient: TTcpClient
    RemoteHost = '127.0.0.1'
    RemotePort = '20332'
    Left = 193
    Top = 137
  end
  object RefreshTimer: TTimer
    Interval = 300
    OnTimer = RefreshTimerTimer
    Left = 153
    Top = 137
  end
  object PortScanTimer: TTimer
    Enabled = False
    OnTimer = PortScanTimerTimer
    Left = 154
    Top = 89
  end
  object XPManifest1: TXPManifest
    Left = 194
    Top = 89
  end
end
