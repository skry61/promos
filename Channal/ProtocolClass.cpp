#pragma hdrstop
#include "ModBusProtocolClass.h"
#include "ProtocolClass.h"
#include "ServiceClass.h"

TProtocolClass::TProtocolClass(TLogMemoQueue *aLogQueue) : TSerialDriver(4096,4096)
{
	LogQueue = aLogQueue;
	Protocol = NULL;
	CS_get = new TCriticalSection();
	CS_set = new TCriticalSection();
	DelayInterval = 80;
	memset(M_ArrayCopy,0,sizeof(M_ArrayCopy));
	memset(M_Array,0,sizeof(M_Array));
	Command	= 0;		//������� � ����������
	Status	= 0;		//��� ���������
	Blocked	= 0;		//������� ������������ ������� (Open/Close)

	CountStabilError	= 0;		// ������� ������� ���������� ������ (��� �������)
	FlagStabilError		= false; 	// ������� ���������� ������
	FrontStabilErrorOn	= false;	// ����� ��������� ���������� ������
	FrontStabilErrorOff	= false;	// ����� ���������� ���������� ������
	LastError			= 0;		// ������� ������
	OldLastError		= 0;		// ������ ������ ��� ����������� ����� ������ �����
	FrontError			= false; 	// ������ �������� ������

}


//-----------------------------------
// ��������� ������ ������� ��������
//-----------------------------------
void TProtocolClass::ExecCommand(unsigned char ACommand) {
	Command	= ACommand;
	Status	= dBUSY;
//	BreakWaitFor();
}

//--------------------------------------
// ������� ��������� ��������� ��������
//--------------------------------------
unsigned char TProtocolClass::GetStatus() {
	return Status;
}

//-----------------------------------------------------
// ������� �������� ������ ������ � ��������� ��������
//-----------------------------------------------------
char TProtocolClass::Open() {
	char result;
	
 //	tsk_lock();									//��������� ������������ �����
	if (Blocked) result = 0;					//���� ������� ������������, �� ��������� = 0
	else		{result = 1; Blocked = 1;}		//���� ������� �� ������������, �� ��������� = 1 � ���������� ���������� ��������
 //	tsk_unlock();								//��������� ������������ �����

	return result;								//���������� ���������
}

//-----------------------------------------------------
// ������� �������� ������ ������ � ��������� 
//-----------------------------------------------------
void TProtocolClass::Close() {
	Blocked = 0;								//������������ �������
}

short TProtocolClass::GetWord(int Index)
{
	CS_get->Enter();
	short v = M_Array[Index];
	CS_get->Leave();
	return v;
}

AnsiString TProtocolClass::GetString(int Index,int Len)
{
	char Str[128];

	CS_get->Enter();
	CodeBinToText(Str,(char*)&M_Array[Index],Len);
	AnsiString s = AnsiString(Str);
	CS_get->Leave();
	return s;
}
//----------------------------------------------------------------------------------------------------
// ������� �������� ��������� ������������������ � ��������� ���������� �������� ��� ��������� ������
//----------------------------------------------------------------------------------------------------
bool TProtocolClass::SendCommand(char Cmd, unsigned char Len,WORD Addr,char *OutBuf, char NoLogTimeout) {
	if(Protocol)
	{
		if (Connected()==false)  return false;
		CS_set->Enter();
		try
		{

			int ErrCount = 3;
			if(LastError == dTIMEOUT) LastError = 0;
			Protocol->FillBuffer(Cmd, Addr,&Len, OutBuf,TXBuffer);
			char Buf[255];
			Buf[0] = 'S';
			if(!Protocol->isASCII())
			{
				//int Count =
				CodeBinToText(&Buf[1],TXBuffer, Len);
				Log(AnsiString(Buf));
			}else
			{
				memcpy(&Buf[1],TXBuffer,Len);
				Buf[Len+1] = 0;
				Log(AnsiString(Buf));
			}

		 //	memcpy(TXBuffer,Command,Len);
			do {
				LastError = 0;
				FlushRX();
				SendBuffer(Len,300);
				for (;;) {
					Delay(20) true;    //������� �� �������� ������ ���� ��������� ����������
					if (RXCompleted())
					{
						LastError = Protocol->Parser(OutBuf);
						break;
					}
					if (RXTimeoutTimer.IsTimeOut()) {
						if (!NoLogTimeout) {
							Status = dTIMEOUT;
							Log(AnsiString(Name()) +" TimeOut");
						}
						LastError = dTIMEOUT;
						break;
					}
				}
				Delay(100) true;
			}
			while ((LastError)&&(--ErrCount > 0));
		}
		__finally {CS_set->Leave();}
		int Err = GetLastError();
		if(Err)
		{
			if(OldLastError == 0)       // �����
			{
				FrontError = true;
				CountStabilError = 0;	// ������� ������� ���������� ������ (��� �������)
			}else
			{
				if(CountStabilError >= 20)
				{
					if(FlagStabilError == false)	// ����������� ������ ���������� ������
						FrontStabilErrorOn = true;
					FlagStabilError = true; 		// ������� ���������� ������
				}
				else
					CountStabilError++;
			}
			OldLastError = Err;
			throw(Err);
		}else
		{
			if(CountStabilError <= 0)
			{
				if(FlagStabilError == true)	    // ����������� ������ ���������� ���������� ������
					FrontStabilErrorOff = true;
				FlagStabilError = false; 		// ������� ���������� ������
			}
			else
				CountStabilError--;

		}
		OldLastError = Err;
		return true;
	}
	throw(-1);
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
void TProtocolClass::SetProtocol(TProtocol *aProtocol)
{
	if (aProtocol) {													//���� ����� ���������� �����/������ �����...
		aProtocol->SetProtocolClass(this);									  	//��������� ����� ���������� � ������� ���������
	}
	if(Protocol) delete Protocol;
	Protocol = aProtocol;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool TProtocolClass::ReadWord(U16 Addr, short *pV)
{
	return SendCommand(Cmd_READWORD, 1,Addr ,(char*)pV);
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------
// ������� �������� ����������
//-----------------------------
bool TProtocolClass::WriteWord(U16 Addr, short V)
{
	short v = V,l=2;
	try
	{
		return SendCommand(Cmd_WRITE1WORD, l,Addr ,(char*)&v);
	}catch (int e)
	{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool TProtocolClass::ReadWArray(U16 Addr, short *pVs,int n)  //������� f579bc  f59f84
{
	return SendCommand(Cmd_READNWORDS, n,Addr ,(char*)pVs);
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool TProtocolClass::ReadBiArray(U16 Addr, BYTE *pVs,int n)
{
	return SendCommand(Cmd_READNBITS, n,Addr ,(char*)pVs);
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool TProtocolClass::WriteBit(U16 Addr, bool BoolValue)
{
	try
	{
		return SendCommand(Cmd_WRITE1BIT,1, Addr ,(char*)&BoolValue);
	}catch (int e)
	{
		return false;
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
bool TProtocolClass::WriteWArray(U16 Addr, short *pVs,int n)
{
	if(Protocol)
	{
		//
		try
		{
			return SendCommand(Cmd_WRITENWORDS, n,Addr ,(char*)pVs);
		}catch (int e)
		{
			return false;
		}
	}
	return false;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------

