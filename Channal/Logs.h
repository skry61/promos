//---------------------------------------------------------------------------

#ifndef LogsH
#define LogsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Sockets.hpp>
#include "ServiceClass.H"
//---------------------------------------------------------------------------
class TULogs : public TForm
{
	int	RequestCount;
	int	CountConnection;
	bool Connected;
	bool ToDisConnect;
	TLogMemoQueue *LogQueueCOM,*LogQueueTCP;
	int	TotalRxCount,TotalTxCount;

__published:	// IDE-managed Components
	TStatusBar *StatusBar1;
	TPanel *Panel1;
	TCheckBox *CheckBoxLogProtocol;
	TButton *Button1;
	TMemo *Memo1;
	TTimer *Timer1;
	TSplitter *Splitter1;
	TMemo *Memo2;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall CheckBoxLogProtocolClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
	__fastcall TULogs(TComponent* Owner);
	__fastcall ~TULogs();
	void __fastcall SetStatLine(int Index,AnsiString str) {StatusBar1->Panels->Items[Index]->Text = str;}
	void LogCOM(AnsiString s){	if(CheckBoxLogProtocol->Checked) LogQueueCOM->AddList(s);}
	void LogTCP(AnsiString s){	if(CheckBoxLogProtocol->Checked) LogQueueTCP->AddList(s);}
	void Init(){ LogQueueCOM->Init(Memo1); LogQueueTCP->Init(Memo2);}
	static TLogMemoQueue *GetLogCOM();//{return LogQueueCOM;}
	static TLogMemoQueue *GetLogTCP();//{return LogQueueTCP;}

};
//---------------------------------------------------------------------------
extern PACKAGE TULogs *ULogs;
//---------------------------------------------------------------------------
#endif
