//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Logs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TULogs *ULogs;

TLogMemoQueue *TULogs::GetLogCOM()
{
	if(ULogs)
//		return LogQueueCOM;
		return ULogs->LogQueueCOM;
	else return NULL;
}

TLogMemoQueue *TULogs::GetLogTCP()
{
	if(ULogs)
		return ULogs->LogQueueTCP;
	else return NULL;
}

//---------------------------------------------------------------------------
__fastcall TULogs::TULogs(TComponent* Owner)   	: TForm(Owner)
{
	LogQueueCOM = new TLogMemoQueue;
	LogQueueTCP = new TLogMemoQueue;
	Init();
}
//---------------------------------------------------------------------------
__fastcall TULogs::~TULogs()
{
	LogQueueCOM->Clear();
	LogQueueTCP->Clear();
	Memo1->Clear();
	Memo2->Clear();
	delete LogQueueCOM;
	delete LogQueueTCP;
	LogQueueCOM = NULL;
	LogQueueTCP = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TULogs::Timer1Timer(TObject *Sender)
{
	LogQueueCOM->LogList();
	LogQueueTCP->LogList();
		SetStatLine(1,"���1. ����� : " + IntToStr(Memo1->Lines->Count));// Count));
		SetStatLine(2,"���2. ����� : " + IntToStr(Memo2->Lines->Count));

}        
//---------------------------------------------------------------------------

void __fastcall TULogs::Button1Click(TObject *Sender)
{
	Memo1->Clear();
	Memo2->Clear();
}
//---------------------------------------------------------------------------



void __fastcall TULogs::CheckBoxLogProtocolClick(TObject *Sender)
{
	Timer1->Enabled = CheckBoxLogProtocol->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TULogs::FormDestroy(TObject *Sender)
{
	LogQueueCOM->Clear();
	LogQueueTCP->Clear();

}
//---------------------------------------------------------------------------

void __fastcall TULogs::FormClose(TObject *Sender, TCloseAction &Action)
{
	LogQueueCOM->Clear();
	LogQueueTCP->Clear();

}
//---------------------------------------------------------------------------

