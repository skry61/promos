object GraferForm: TGraferForm
  Left = 0
  Top = 0
  Caption = 'GraferForm'
  ClientHeight = 519
  ClientWidth = 804
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 804
    Height = 25
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 3
      Top = 5
      Width = 111
      Height = 13
      Caption = #1056#1080#1089#1086#1074#1072#1090#1100' '#1095#1077#1088#1077#1079' ('#1088#1072#1079'):'
    end
    object Label2: TLabel
      Left = 381
      Top = 5
      Width = 44
      Height = 13
      Caption = #1057#1076#1074#1080#1075' Y:'
    end
    object Label3: TLabel
      Left = 292
      Top = 5
      Width = 44
      Height = 13
      Caption = #1057#1076#1074#1080#1075' '#1061':'
    end
    object Label4: TLabel
      Left = 470
      Top = 5
      Width = 30
      Height = 13
      Caption = #1062#1074#1077#1090':'
    end
    object eDrawFriq: TEdit
      Left = 120
      Top = 1
      Width = 39
      Height = 21
      TabOrder = 0
      Text = '5'
      OnKeyPress = eDrawFriqKeyPress
    end
    object ColorBox1: TColorBox
      Left = 502
      Top = 1
      Width = 97
      Height = 22
      DefaultColorColor = clNavy
      Selected = clNavy
      ItemHeight = 16
      TabOrder = 1
      OnChange = ColorBox1Change
    end
    object EditdZ: TEdit
      Left = 427
      Top = 2
      Width = 37
      Height = 21
      TabOrder = 2
      Text = '40000'
      OnKeyDown = EditdZKeyDown
    end
    object EditdX: TEdit
      Left = 339
      Top = 1
      Width = 36
      Height = 21
      TabOrder = 3
      Text = '50000'
      OnKeyDown = EditdXKeyDown
    end
    object TrackBar1: TTrackBar
      Left = 156
      Top = 0
      Width = 129
      Height = 23
      Max = 20
      Min = -4
      TabOrder = 4
      ThumbLength = 16
      OnChange = TrackBar1Change
    end
    object cbDirRotation: TCheckBox
      Left = 605
      Top = 4
      Width = 64
      Height = 17
      Caption = #1048#1085#1074'.'#1042#1088#1097
      TabOrder = 5
    end
    object cbWCS_MCS: TCheckBox
      Left = 669
      Top = 4
      Width = 76
      Height = 17
      Caption = 'MCS/WCS'
      TabOrder = 6
    end
  end
  object Panel2: TPanel
    Left = 664
    Top = 25
    Width = 140
    Height = 494
    Align = alRight
    Caption = 'Panel2'
    TabOrder = 1
    object Button1: TButton
      Left = 24
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Start CNC'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 24
      Top = 37
      Width = 75
      Height = 25
      Caption = 'Stop CNC'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 24
      Top = 68
      Width = 75
      Height = 25
      Caption = 'Clear All'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 24
      Top = 99
      Width = 75
      Height = 25
      Caption = 'Zoom in'
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 24
      Top = 124
      Width = 75
      Height = 25
      Caption = 'Zoom out'
      TabOrder = 4
      OnClick = Button5Click
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 48
    Top = 48
  end
end
