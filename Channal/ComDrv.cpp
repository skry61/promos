//---------------------------------------------------------------------------

#pragma hdrstop

#include "ComDrv.h"
#include "Option.h"
#include "strconv.h"
#include "stdio.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//-----------------------------------
// ������� �������� ������� � ������
//-----------------------------------
AnsiString DoubleQuotes(AnsiString ASource) {
	char buf[65535];
	SC_DoubleQuotes(ASource.c_str(),buf,sizeof(buf));
	return "\"" + AnsiString(buf) + "\"";
}

AnsiString Addr;
AnsiString Port;

//-----------------------------------------------------------
// ������� ��������� ���������� �� ����� ����� ������� �����
//-----------------------------------------------------------
void GetParamsSync() {
	Addr				= OptionForm->AddrEdit->Text;
	Port				= OptionForm->PortEdit->Text;
}

//-----------------------------
// ������� �������� ����������
//-----------------------------
bool TBridgeDriver::IsConnectionClosed() {
	for (;;) {

		if (!OptionForm->TCPClient->Connected) break;

		bool R;
		if (OptionForm->TCPClient->Select(&R,NULL,NULL,1)) if (R) {
			char ch;
			if (OptionForm->TCPClient->PeekBuf(&ch,sizeof(ch))==0) break;
		}

		return false;
	}
	return true;
}

//---------------------------
// ���������� �������� �����
//---------------------------
void TBridgeDriver::Handler() {

	Delay(10);

	if (ConnectCommand==true) {
		DisconnectCommand	= false;

		ExternalProc = GetParamsSync;
		CallExternalProcSync();

		OptionForm->TCPClient->RemoteHost = Addr;
		OptionForm->TCPClient->RemotePort = Port;
		OptionForm->TCPClient->Connect();

		ConnectCommand		= false;
	}

	if (!OptionForm->TCPClient->Connected) return;


	for (;;) {

		Delay(1);

		if (IsConnectionClosed()) {OptionForm->TCPClient->Disconnect(); break;}

		if (DisconnectCommand==true) {
			OptionForm->TCPClient->Disconnect();
			ConnectCommand		= false;
			DisconnectCommand	= false;
			break;
		}

		char Buf[4000];

		//���������, ������� �� ���-�� �� COM-�����
		if (RXCompleted()) {

			if (RXCountWhenCompleted>sizeof(Buf)) RXCountWhenCompleted = sizeof(Buf);

			ReadBuffer(0,Buf,RXCountWhenCompleted);				//������ ������, �������� �� COM-�����

			OptionForm->TCPClient->SendBuf(Buf,RXCountWhenCompleted);	//���������� ������ ���������� ����� �� TCP

			RXBuffer->IncrementRDIndex(RXCountWhenCompleted);

			COMRXCounter += RXCountWhenCompleted;

		}

		//������� ��������� ������; ���� ������ ���, �� �������� ���� ������
		if (!OptionForm->TCPClient->WaitForData(10)) continue;

		//�������, ������� ������ ������
		ZeroMemory(Buf,sizeof(Buf));
		int len = OptionForm->TCPClient->PeekBuf(Buf,sizeof(Buf));
		if (len==0) continue;

		OptionForm->TCPClient->ReceiveBuf(Buf,len);				//��������� �������� ������ � �����

		//OptionForm->TCPClient->SendBuf(Buf,len);				//����������� ������ (loop-back)

		//sprintf(Buf,"DIAGCMDCMD info");
		//len = strlen(Buf);

		memcpy(TXBuffer,Buf,len);								//�������� ������ � ����� ��������
		SendBuffer(len,0);										//���������� � COM-����

		COMTXCounter += len;

	}

}


