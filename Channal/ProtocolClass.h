//---------------------------------------------------------------------------

#ifndef ProtocolClassH
#define ProtocolClassH

#include "SyncObjs.hpp"
#include "com.h"
#include "serialdrv.h"
//#include "ModBusProtocolClass.h"
#include "ServiceClass.h"
#include <Sockets.hpp>

//-------------------------
// ���� ��������� ��������
//-------------------------
#define dREADY			0x00	//������� ����� � ������ ������
#define dBUSY			0x01	//������� ����� ���������� �������
#define dTIMEOUT		0x02	//������ - �� ������� �� ��������� �������� �������
#define dERROR			0x03	//������ - ���������� ������
#define MAINTENANCE		0x04	//������ - ������� � ������������ (???)

#define BUSYTIMEOUT		0x05	//������ - ����-���, ������� �����
#define OPENTIMEOUT		0x06	//������ - ����-��� �������� ��������
#define SERIALERROR		0x10	//������ ������



class TProtocol;

class TProtocolClass : public TSerialDriver {
public:		//   ������ �������� ������
	short M_Array[0x2000];
	int   DelayInterval;

private:
	TLogMemoQueue *LogQueue;
	unsigned long	Command;	//������� � ����������
	unsigned long	Status;		//��� ���������
	int				Blocked;	//������� ������������ ������� (Open/Close)

	int 			CountStabilError;// ������� ������� ���������� ������ (��� �������)
	bool			FlagStabilError; // ������� ���������� ������
//	bool			OldFlagStabilError; // ������� ���������� ������  ������
	bool 			FrontStabilErrorOn;// ����� ��������� ���������� ������
	bool 			FrontStabilErrorOff;// ����� ���������� ���������� ������
//	bool 			SpadStabilError; // ���� ���������� ������

	int 			LastError;	// ������� ������
	int 			OldLastError;// ������ ������ ��� ����������� ����� ������ �����
	bool 			FrontError; // ������ �������� ������
	TProtocol		*Protocol;	// �������� ��������� ��������� ���������

protected:
	BYTE OutBuf[256];	// �������� �����
	TCriticalSection *CS_get;
	TCriticalSection *CS_set;
	short M_ArrayCopy[0x2000];
public:
	BYTE Node;	   		// ����� �������
	bool Echo;			// ������� ���������� ��� � ������

	TProtocolClass(TLogMemoQueue *aLogQueue);
	~TProtocolClass(){if(Protocol) delete Protocol;delete CS_get;delete CS_set;}
	void SetLogQueue(TLogMemoQueue *aLogQueue){LogQueue = aLogQueue;}
	void CloseLogQueue(){LogQueue = NULL;}

//	virtual void Handler();
	virtual const char * Name()	{return "None";}			//�������, ������������ ��� ��������

	void ExecCommand(unsigned char ACommand);
	unsigned char GetStatus();

	int GetLastError(){int t = LastError; LastError = 0; return t;}
	bool isFrontError(){bool r = FrontError;FrontError = false;return r;}
	bool isFrontStabilErrorOn(){bool r = FrontStabilErrorOn;FrontStabilErrorOn = false;return r;}
	bool isFrontStabilErrorOff(){bool r = FrontStabilErrorOff;FrontStabilErrorOff = false;return r;}
	bool isStabilError(){return FlagStabilError;}

	void SetInterval(int Interval){/*CS->Enter();*/ if (Interval >= 10) DelayInterval = Interval; /*CS->Leave();*/}
	short GetWord(int Index);
	AnsiString GetString(int Index,int Len);
	char Open();
	void Close();

	bool SendCommand(char Cmd, unsigned char Len,WORD Addr,char *OutBuf, char NoLogTimeout);	//������� �������� ��������� ������������������ � ��������� ���������� ��������
	bool SendCommand(char Cmd, unsigned char Len,WORD Addr,char *OutBuf) {return SendCommand(Cmd,Len,Addr,OutBuf,0);}	//������� �������� ��������� ������������������ � ��������� ���������� ��������
	void SetProtocol(TProtocol *aProtocol);
	bool Connected(){return IOHandler? ((TComPortIOHandler*)IOHandler)->Connected():false;}	
public:
//	unsigned long COMRXCounter;      //RXTotalCounter       � ������� ������
//	unsigned long COMTXCounter;      //TXTotalCounter

 //	virtual char NeedToFlushRXBeforeTX() {return false;}
	int GetRX_Total(){return RXTotalCounter;}
	int GetTX_Total(){return TXTotalCounter;}
	void ResetRX_Total(){RXTotalCounter = 0;}
	void ResetTX_Total(){TXTotalCounter = 0;}
	void Log(AnsiString s){	if(LogQueue) LogQueue->AddList(s);}
public:
   bool		ReadWord(U16 Addr, short *pV);
   bool		WriteWord(U16 Addr, short V);
   bool		ReadWArray(U16 Addr, short *pVs,int n);
   bool		WriteWArray(U16 Addr, short *pVs,int n);
   bool		ReadBiArray(U16 Addr, BYTE *pVs,int n);
   bool		WriteBit(U16 Addr, bool V);
/*
   bool		ReadBit(U16 Addr, bool *pV){};
   bool		ReadWord(U16 Addr, short *pV){};
   bool		ReadWArray(U16 Addr, short *pVs){};
   bool		WriteWord(U16 Addr, short V){};
   bool		WriteWArray(U16 Addr, short *pVs){};
*/
};
//---------------------------------------------------------------------------
#endif
