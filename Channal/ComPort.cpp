//---------------------------------------------------------------------------
// ���� �������� ������ COM-�����
//---------------------------------------------------------------------------

#pragma hdrstop

#include "ComPort.h"
#include "strconv.h"
#include "stdio.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//=============================================================================================================
// TComPort - ����� ���������� COM-�����
//=============================================================================================================

//-------------
// �����������
//-------------
__fastcall TComPort::TComPort() {

	CS = new TCriticalSection;

	FRXThread = NULL;

	SetFormat("8N1");

	FRXBufferSize = 1024;
	FTXBufferSize = 1024;

	OnRXChar = NULL;

	SetConnected(false);
}

//------------
// ����������
//------------
__fastcall TComPort::~TComPort() {
	delete CS;
}

//------------------------------
// ��������� �������� ���-�����
//------------------------------
bool __fastcall TComPort::Connect() {

	if (Connected()) return false;

	//�������� ������� ����
	HANDLE AHandle = CreateFile(("\\\\.\\" + FPortName).c_str(),
								   GENERIC_READ | GENERIC_WRITE,
								   0,
								   NULL,
								   OPEN_EXISTING,
								   FILE_FLAG_OVERLAPPED,
								   0);

	if (AHandle==INVALID_HANDLE_VALUE) {
		return false;
	}

	FHandle = AHandle;

	if (!SetupComPort()) {
		CloseHandle(FHandle);
		return false;
	}

	//��������� �������� �����
	FRXThread = new TComPortRXThread(FHandle,this);

	SetConnected(true);
    return true;
}

//------------------------------
// ��������� �������� ���-�����
//------------------------------
bool __fastcall TComPort::Disconnect() {
	if (!Connected()) return true;

	//���� ������� �� ������� � ���������� ������
	CS->Enter();
	try {FRXThread->Stop();}
	__finally {CS->Leave();}

	//������� ��������������� ������
	bool terminated=false;
	DWORD start=GetTickCount();
	while ((GetTickCount()-start)<2000) {	//����-��� 2 �������
		CS->Enter();
		try {if (FRXThread==NULL) {terminated = true; break;}}
		__finally {CS->Leave();}
		Sleep(50);
		Application->ProcessMessages();		//����� ��� ���������� ���������� ����� DoEvents()
	}
	if (!terminated) Application->MessageBox(AnsiString("Can't terminate RX thread of " + FPortName).c_str(),"Message",MB_OK);

	//��������� �������� � �����
	PurgeComm(FHandle, PURGE_TXABORT | PURGE_RXABORT);

	//��������� ���-����
	if (CloseHandle(FHandle)) {
		SetConnected(false);
		return true;
	}
	else return false;
}

//-----------------------------------
// ��������� ������������� ���-�����
//-----------------------------------
bool __fastcall TComPort::SetupComPort() {
	if (!ApplyBuffers())	return false;
	if (!ApplyDCB())		return false;
	if (!ApplyTimeouts())	return false;
	return true;
}

//-----------------------------------------
// ��������� ���������� ���������� �������
//-----------------------------------------
bool __fastcall TComPort::ApplyBuffers() {
	return SetupComm(FHandle, FRXBufferSize, FTXBufferSize);
}

//-----------------------------------
// ��������� ���������� �������� DCB
//-----------------------------------
bool __fastcall TComPort::ApplyDCB() {
	DCB dcb;

	dcb.DCBlength			= sizeof(DCB);
	dcb.BaudRate			= FBaudRate;
	dcb.fBinary				= 1;
	dcb.fParity				= FParity!=NOPARITY;
	dcb.fOutxCtsFlow		= 0;
	dcb.fOutxDsrFlow		= 0;
	dcb.fDtrControl			= DTR_CONTROL_DISABLE;
	dcb.fDsrSensitivity		= 0;
	dcb.fTXContinueOnXoff	= 0;
	dcb.fOutX				= 0;
	dcb.fInX				= 0;
	dcb.fErrorChar			= 0;
	dcb.fNull				= 0;
	dcb.fRtsControl			= RTS_CONTROL_TOGGLE;	//RTS_CONTROL_DISABLE;
	dcb.fAbortOnError		= 0;
	dcb.wReserved			= 0;
	dcb.XonLim				= 1;
	dcb.XoffLim				= 1;
	dcb.ByteSize			= FByteSize;
	dcb.Parity				= FParity;
	dcb.StopBits			= FStopBits;
	dcb.XonChar				= 0;
	dcb.XoffChar			= 0;
	dcb.ErrorChar			= 0;
	dcb.EofChar				= 0;
	dcb.EvtChar				= 0;

	return SetCommState(FHandle, &dcb);
}

//--------------------------------------------
// ��������� ���������� ���������� ����-�����
//--------------------------------------------
bool __fastcall TComPort::ApplyTimeouts() {
	COMMTIMEOUTS timeouts;

	timeouts.ReadIntervalTimeout			= -1;
	timeouts.ReadTotalTimeoutMultiplier		= 0;
	timeouts.ReadTotalTimeoutConstant	  	= 0;
	timeouts.WriteTotalTimeoutMultiplier	= 100;
	timeouts.WriteTotalTimeoutConstant		= 1000;

	return SetCommTimeouts(FHandle, &timeouts);
}

//--------------------------------
// ��������� ������� ������ �����
//--------------------------------
bool __fastcall TComPort::SetPortName(AnsiString APortName) {
	if (Connected()) return false;
	FPortName = APortName;
	return true;
}

//-----------------------------------
// ��������� ������� �������� ������
//-----------------------------------
bool __fastcall TComPort::SetBaudRate(int ABaudRate) {
	if (Connected()) return false;
	FBaudRate = ABaudRate;
	return true;
}

//-------------------------------------------
// ��������� ������� ������� ������/��������
//-------------------------------------------
bool __fastcall TComPort::SetFormat(AnsiString Format) {
	if (Connected()) return false;

	if (Format.Length()!=3) Format = "8N1";

	switch (Format[1]) {
		case '5':
		case '6':
		case '7':	FByteSize = Format[1]-'0'; break;
		default:	FByteSize = 8;
	}

	switch (Format[2]) {
		case 'E':	FParity = EVENPARITY;	break;
		case 'O':	FParity = ODDPARITY;	break;
		case 'M':	FParity = MARKPARITY;	break;
		case 'S':	FParity = SPACEPARITY;	break;
		default:	FParity = NOPARITY;
	}

	char StopBits;
	switch (Format[3]) {
		case 2:		FStopBits = TWOSTOPBITS; break;
		default:	FStopBits = ONESTOPBIT;
	}

	return true;
}

//-----------------------------------------
// ��������� ������� ������� ������ ������
//-----------------------------------------
bool __fastcall TComPort::SetRXBufferSize(int ASize) {
	if (Connected()) return false;
	FRXBufferSize = ASize;
	return true;
}

//-------------------------------------------
// ��������� ������� ������� ������ ��������
//-------------------------------------------
bool __fastcall TComPort::SetTXBufferSize(int ASize) {
	if (Connected()) return false;
	FTXBufferSize = ASize;
	return true;
}

//-----------------------------------------------
// ��������� ������� ��������� ������� ���-�����
//-----------------------------------------------
bool __fastcall TComPort::SetEvents(TComEvents AEvents) {
	if (Connected()) return false;
	FEvents = AEvents;
	return true;
}





//--------------------------------------------------
// ������� �������������� ����� � ��������� �������
//--------------------------------------------------
TComEvents TComPort::IntToEvents(int AMask) {
	TComEvents Result;

	if (AMask & EV_RXCHAR  ) Result << evRxChar;
	if (AMask & EV_TXEMPTY ) Result << evTxEmpty;
	if (AMask & EV_CTS     ) Result << evCTS;
	if (AMask & EV_DSR     ) Result << evDSR;
	if (AMask & EV_RLSD    ) Result << evRLSD;
	if (AMask & EV_BREAK   ) Result << evBreak;
	if (AMask & EV_ERR     ) Result << evError;
	if (AMask & EV_RING    ) Result << evRing;
	if (AMask & EV_PERR    ) Result << evPErr;
	if (AMask & EV_RX80FULL) Result << evRx80Full;

	return Result;
}

//--------------------------------------------------
// ������� �������������� ��������� ������� � �����
//--------------------------------------------------
int TComPort::EventsToInt(TComEvents AEvents) {
	int Result = 0;

	if (AEvents.Contains(evRxChar  )) Result |= EV_RXCHAR;
	if (AEvents.Contains(evRxFlag  )) Result |= EV_RXFLAG;
	if (AEvents.Contains(evTxEmpty )) Result |= EV_TXEMPTY;
	if (AEvents.Contains(evCTS     )) Result |= EV_CTS;
	if (AEvents.Contains(evDSR     )) Result |= EV_DSR;
	if (AEvents.Contains(evRLSD    )) Result |= EV_RLSD;
	if (AEvents.Contains(evBreak   )) Result |= EV_BREAK;
	if (AEvents.Contains(evError   )) Result |= EV_ERR;
	if (AEvents.Contains(evRing    )) Result |= EV_RING;
	if (AEvents.Contains(evPErr    )) Result |= EV_PERR;
	if (AEvents.Contains(evRx80Full)) Result |= EV_RX80FULL;

	return Result;
}

//----------------------------------
// ���������� ������� ������ ������
//----------------------------------
void __fastcall TComPort::CallOnRXChar() {

	if (OnRXChar) {

		char *Buffer=NULL;
		unsigned long Count;

		try {
			if (ReadBuffer(&Buffer,&Count)&&(Count>0)) {
				OnRXChar(this, Buffer, Count);

/*				//�������
				TFileStream *Stream = new TFileStream("D:\\comm.log",fmOpenWrite);
				Stream->Seek(0,soFromEnd);
				Stream->Write("\r\n",2);
				Stream->Write(Buffer,Count);
				delete Stream;*/
			}
		}
		__finally {
			if (Buffer) delete Buffer;
		}
	}
}

void __fastcall TComPort::CallOnRXFlag() {}

void __fastcall TComPort::CallOnTXEmpty() {}

void __fastcall TComPort::CallOnCTS() {}

void __fastcall TComPort::CallOnDSR() {}

void __fastcall TComPort::CallOnRLSD() {}

void __fastcall TComPort::CallOnBreak() {}

void __fastcall TComPort::CallOnError() {}

void __fastcall TComPort::CallOnRing() {}

void __fastcall TComPort::CallOnPErr() {}

void __fastcall TComPort::CallOnRx80Full() {}

//-------------------------------------------------------------------------------
// ������� ��������� ���������� ���� ������ � �������� ������ �������� ���-�����
//-------------------------------------------------------------------------------
bool __fastcall TComPort::InputCount(unsigned long *ACount) {
	DWORD	Errors=0;
	COMSTAT	Stat;

	bool result = ClearCommError(FHandle, &Errors, &Stat);

	if (result) *ACount = Stat.cbInQue;

	return result;
}

//--------------------------------------------------------------
// ������� ������ ������ �� ��������� ������ �������� ���-�����
//--------------------------------------------------------------
// ����������:
//   ABuffer - ��������� �� ����� ��� NULL
//   ACount  - ���������� ���� � ������
//   true/false - ��������� ���������� ������
//--------------------------------------------------------------
bool __fastcall TComPort::ReadBuffer(char **ABuffer, unsigned long *ACount) {

	unsigned long CountInQueue;
	if (!InputCount(&CountInQueue)) return false;

	if (CountInQueue==0) {
		*ACount = 0;
		*ABuffer = NULL;		
		return true;
	}

	OVERLAPPED Reading;
	ZeroMemory(&Reading,sizeof(Reading));
	Reading.hEvent = CreateEvent(NULL, true, false, NULL);

	char *Buffer = new char [FRXBufferSize];
	DWORD Count, BytesTransferred;
	bool result = ReadFile(FHandle, Buffer, CountInQueue, &Count, &Reading);
	if (result) {

		if (WaitForSingleObject(Reading.hEvent, 200/*INFINITE*/) == WAIT_OBJECT_0) {

			result = GetOverlappedResult(FHandle, &Reading, &BytesTransferred, false);

			if (result) {
				*ABuffer = Buffer;
				*ACount = Count;
			}
		}
		else result = false;
	}

	if (!result) {
		delete [] Buffer;
	}

	CloseHandle(Reading.hEvent);
	return result;
}

//------------------------------------
// ������� �������� ������ � ���-����
//------------------------------------
bool TComPort::WriteBuffer(char *ABuffer, int ASize) {
	if (!Connected()) return false;

	OVERLAPPED Writting;
	ZeroMemory(&Writting,sizeof(Writting));
	bool result = false;

	try {
		Writting.hEvent = CreateEvent(NULL, true, false, NULL);
		DWORD Count=0;

		result = WriteFile(FHandle, ABuffer, ASize, &Count, &Writting) || (GetLastError()==ERROR_IO_PENDING);

		if (result) {
			if (WaitForSingleObject(Writting.hEvent, 200/*INFINITE*/) != WAIT_OBJECT_0) result = false;
		}

	}
	__finally {
		if (Writting.hEvent) CloseHandle(Writting.hEvent);
		return result;
	}
}

//-----------------------------------
// ������� ��������� ��������� �����
//-----------------------------------
void __fastcall TComPort::SetConnected(bool AConnected) {
	CS->Enter();
	try {FConnected = AConnected;}
	__finally {CS->Leave();}
}

//-----------------------------------------------------
// ������� ������ ��������� ����� (���������/��������)
//-----------------------------------------------------
bool __fastcall TComPort::Connected() {
	bool result;
	CS->Enter();
	try {result = FConnected;}
	__finally {CS->Leave();}
	return result;
}

//--------------------------------------------------
// ������� ������ �������������� � FHandle ����� CS
//--------------------------------------------------
void __fastcall TComPort::SetHandle(HANDLE AHandle) {
	CS->Enter();
	try {FHandle = AHandle;}
	__finally {CS->Leave();}
}

//---------------------------------------------------
// ������� ������ �������������� �� FHandle ����� CS
//---------------------------------------------------
HANDLE __fastcall TComPort::GetHandle() {
	HANDLE result;
	CS->Enter();
	try {result = FHandle;}
	__finally {CS->Leave();}
	return result;
}

//=============================================================================================================
// TComPortRXThread - ����� ��������� ������
//=============================================================================================================

//-------------
// �����������
//-------------
__fastcall TComPortRXThread::TComPortRXThread(HANDLE AHandle, TComPort *AComPort) : TThread(true) {		//������� ����� � ������������ ��������� - Suspended

	FHandle = AHandle;
	FComPort = AComPort;

	FStopEvent = CreateEvent(NULL, true, false, NULL);				//������� ������������� ������� �������� ������

	SetCommMask(FHandle, FComPort->EventsToInt(FComPort->FEvents));	//������ ����� ������� ���-�����

	Priority = tpNormal;											//��������� ������������ ��������� ������
	FreeOnTerminate = true;											//��������������� ��� ����������

	Resume();														//��������� �����

}

//------------
// ����������
//------------
__fastcall TComPortRXThread::~TComPortRXThread() {

	//������ � ����������� ������ � �������� � ���-����� ��������� �� ����� - ��� ������� ����, ��� ����� ����� �������
	FComPort->CS->Enter();
	try {
		if(FComPort->FRXThread != NULL)
		{
 //			 delete FComPort->FRXThread;
			 FComPort->FRXThread = NULL;
		}
	}
	__finally {FComPort->CS->Leave();}
	
}

//-----------------------------------------------
// ������� ��������� ���������� ��������� ������
//-----------------------------------------------
void __fastcall TComPortRXThread::Execute() {

	OVERLAPPED Overlapped;
	ZeroMemory(&Overlapped,sizeof(Overlapped));
	Overlapped.hEvent = CreateEvent(NULL, true, true, NULL);

	HANDLE EventHandles[2];
	EventHandles[0] = FStopEvent;
	EventHandles[1] = Overlapped.hEvent;

	while (1) {
		DWORD Mask, Signaled, BytesTransferred;

		WaitCommEvent(FHandle, &Mask, &Overlapped);							 	//���������������� ����� �� ��������� ������� ���-�����
		Signaled = WaitForMultipleObjects(2, EventHandles, false, INFINITE); 	//���������������� ����� �� ��������� ������� ���-����� ��� �������� ������

		if (Signaled==(WAIT_OBJECT_0 + 1)) {									//���� �������� ������� ���-�����...
			if (GetOverlappedResult(FHandle, &Overlapped, &BytesTransferred, false)) {	//���� ������� �������� ����� ������� ���-�����...
				FEvents = FComPort->IntToEvents(Mask);										//����������� ����� � ��������� �������
				if (FComPort->InterceptEvents(FEvents)) {}									//�������� ����������� ������� (�� ����� ���� ��������� � ����������)
				else Synchronize(&DoEvents);												//����� - �������� ���������� ������� ���-����� � �������� ������
			}
		}
		else break;																//� ������ ������� ������� ��� ������ - ��������� �����
	};

	SetCommMask(FHandle, 0);													//������ ������� ����� ����������� �������
	PurgeComm(FHandle, PURGE_TXCLEAR | PURGE_RXCLEAR);							//���������� ������ ������ � ��������

	CloseHandle(Overlapped.hEvent);												//����������� ������������� ������� ���-�����
	CloseHandle(FStopEvent);													//����������� ������������� ������� �������� ������
}

//-------------------------------------------------------------------
// ��������� ������ ������� �� ������� � ���������� ��������� ������
//-------------------------------------------------------------------
void __fastcall TComPortRXThread::Stop() {
	SetEvent(FStopEvent);
	Sleep(0);
}

//-------------------------------------------------
// ��������� ������ ������������ ������� ���-�����
//-------------------------------------------------
void __fastcall TComPortRXThread::DoEvents() {
	if (FEvents.Contains(evRxChar  )) FComPort->CallOnRXChar();
	if (FEvents.Contains(evRxFlag  )) FComPort->CallOnRXFlag();
   	if (FEvents.Contains(evTxEmpty )) FComPort->CallOnTXEmpty();
	if (FEvents.Contains(evCTS     )) FComPort->CallOnCTS();
	if (FEvents.Contains(evDSR     )) FComPort->CallOnDSR();
	if (FEvents.Contains(evRLSD    )) FComPort->CallOnRLSD();
	if (FEvents.Contains(evBreak   )) FComPort->CallOnBreak();
	if (FEvents.Contains(evError   )) FComPort->CallOnError();
	if (FEvents.Contains(evRing    )) FComPort->CallOnRing();
	if (FEvents.Contains(evPErr    )) FComPort->CallOnPErr();
	if (FEvents.Contains(evRx80Full)) FComPort->CallOnRx80Full();
}






