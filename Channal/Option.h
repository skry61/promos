//---------------------------------------------------------------------------

#ifndef OptionH
#define OptionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Sockets.hpp>
#include <ExtCtrls.hpp>

#include "ThreadSafe.h"
#include "ComPort.h"
#include "com.h"
#include "PLC_Drv.h"
//#include "..\First\BASINTX11.h"
#include <XPMan.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TOptionForm : public TForm
{
__published:	// IDE-managed Components
	TTcpClient *TCPClient;
	TButton *Button1;
	TButton *Button2;
	TTimer *RefreshTimer;
	TGroupBox *GroupBox1;
	TPanel *TCPStateLabel;
	TLabeledEdit *AddrEdit;
	TLabeledEdit *PortEdit;
	TGroupBox *GroupBox2;
	TComboBox *PortComboBox;
	TLabel *Label1;
	TLabel *Label2;
	TComboBox *BaudrateComboBox;
	TTimer *PortScanTimer;
	TButton *ConnectButton;
	TXPManifest *XPManifest1;
	TButton *ResetTCPButton;
	TPanel *COMStateLabel;
	TComboBox *BitCountComboBox;
	TLabel *Label3;
	TComboBox *ParityComboBox;
	TLabel *Label4;
	TLabel *TCPRXLabel;
	TLabel *TCPTXLabel;
	TButton *btLog;
	TCheckBox *cbSimulTCP;
	TLabel *Label7;
	TLabel *Label8;
	TCheckBox *cbSimulCOM;
	TLabel *Label9;
	TLabel *COMRXLabel;
	TLabel *COMTXLabel;
	TLabel *Label12;
	TButton *ResetCOMButton;
	TLabeledEdit *NodeEditTCP;
	TLabeledEdit *NodeEditMB;
	TComboBox *COM_ProtocolComboBox;
	TLabel *Label5;
	TLabel *Label6;
	TComboBox *TCP_ProtocolComboBox;
	TCheckBox *cbEcho;
	TLabeledEdit *LErrCount;
	TCheckBox *cbPLC_to_Simul;
	TGroupBox *GroupBox4;
	TLabel *Label16;
	TBevel *Bevel1;
	TLabel *Label17;
	TBevel *Bevel2;
	TLabel *Label20;
	TLabel *Label21;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall RefreshTimerTimer(TObject *Sender);
	void __fastcall PortScanTimerTimer(TObject *Sender);
	void __fastcall PortComboBoxKeyPress(TObject *Sender, char &Key);
	void __fastcall ConnectButtonClick(TObject *Sender);
	void __fastcall ResetTCPButtonClick(TObject *Sender);
	void __fastcall btLogClick(TObject *Sender);
	void __fastcall ResetCOMButtonClick(TObject *Sender);
	void __fastcall cbSimulCOMClic(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TOptionForm(TComponent* Owner);
//	__fastcall ~TOptionForm();

	TComPortIOHandler	*ComPort;
	TPLCDriver 			*PLCDriver;  
/*
	double Get_Cur_I(){return PLCDriver? PLCDriver->Get_Cur_I():10;};            // �������� ������� ���
	double Get_Cur_B(){return PLCDriver? PLCDriver->Get_Cur_B():2;};             // �������� ������� ����������
	double Get_Cur_V(){return PLCDriver? PLCDriver->Get_Cur_V():1;};             // �������� ������� �������� ���������
	double Get_Cur_F(){return PLCDriver? PLCDriver->Get_Cur_F():10;};             // �������� ������� ������ ����
*/
	int ConnectPLC();			// ������������� � �����������

	bool isChanal1FrontStabilErrorOn(){return PLCDriver->isFrontStabilErrorOn();}
	bool isChanal1FrontStabilErrorOff(){return PLCDriver->isFrontStabilErrorOff();}
	bool isChanal1StabilError(){return PLCDriver->isStabilError();}
};

extern TThreadSafeBool ConnectCommand;
extern TThreadSafeBool DisconnectCommand;

//---------------------------------------------------------------------------
extern PACKAGE TOptionForm *OptionForm;
//---------------------------------------------------------------------------
#endif    //   OptionForm-> PLCDriver->
