object ULogs: TULogs
  Left = 0
  Top = 0
  Caption = #1051#1086#1075#1080' '#1057#1054#1052' '#1080' TCP'
  ClientHeight = 433
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 201
    Width = 513
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 233
    ExplicitWidth = 181
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 414
    Width = 513
    Height = 19
    Panels = <
      item
        Text = 'Offline'
        Width = 50
      end
      item
        Text = #1047#1072#1087#1088#1086#1089#1086#1074' :'
        Width = 100
      end
      item
        Text = ' '#1055#1077#1088#1077#1076#1072#1085#1086' :'
        Width = 100
      end
      item
        Text = #1055#1088#1080#1085#1103#1090#1086' :'
        Width = 100
      end
      item
        Text = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1081' :'
        Width = 147
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 513
    Height = 33
    Align = alTop
    TabOrder = 1
    object CheckBoxLogProtocol: TCheckBox
      Left = 352
      Top = 4
      Width = 129
      Height = 17
      Caption = #1057#1090#1086#1087' '#1051#1086#1075
      TabOrder = 0
      OnClick = CheckBoxLogProtocolClick
    end
    object Button1: TButton
      Left = 8
      Top = 4
      Width = 65
      Height = 25
      Caption = 'Clear Log'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 33
    Width = 513
    Height = 168
    Align = alTop
    DragKind = dkDock
    HideSelection = False
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object Memo2: TMemo
    Left = 0
    Top = 204
    Width = 513
    Height = 210
    Align = alClient
    Lines.Strings = (
      '')
    TabOrder = 3
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 264
  end
end
