//---------------------------------------------------------------------------

#ifndef ComDrvH
#define ComDrvH
//---------------------------------------------------------------------------

#include "serialdrv.h"

class TCOMDriver : public TSerialDriver {

public:

	TCOMDriver() : TSerialDriver(4096,4096) {}

	bool IsConnectionClosed();

	virtual void Handler();

public:
	unsigned long COMRXCounter;
	unsigned long COMTXCounter;

	virtual char NeedToFlushRXBeforeTX() {return false;}

};


#endif
