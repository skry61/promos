//---------------------------------------------------------------------------

#ifndef GraferH
#define GraferH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "Fron_Grapher.h"
#include "MP4_Unit.h"
//---------------------------------------------------------------------------
class TGraferForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TEdit *eDrawFriq;
	TColorBox *ColorBox1;
	TEdit *EditdZ;
	TEdit *EditdX;
	TTrackBar *TrackBar1;
	TCheckBox *cbDirRotation;
	TCheckBox *cbWCS_MCS;
	TTimer *Timer1;
	TPanel *Panel2;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall eDrawFriqKeyPress(TObject *Sender, char &Key);
	void __fastcall EditdXKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall EditdZKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall ColorBox1Change(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
		int ZoomCount;      		// ����������� ���������� � 2 ����.
		int ShowCount;				// ����� ������� ������ ���������� �����������
		int ShowCountLimit;			// ����� ������� ������ ���������� ����������� (������������ ��������)
		TFron_Grapher *Grapher1;    // ������ ��������
		TMP4_Unit *MP4_Unit;        // ���������

		bool FlShow;		// ���� ���������� ���������

public:		// User declarations
	__fastcall TGraferForm(TComponent* Owner);
	   void	__fastcall ZoomIn(void);
	   void	__fastcall ZoomOut(void);
	   void	__fastcall Add(int X,int Y);
	   void	__fastcall AddData(int X,int Y);
	   void	__fastcall EraseGraph(void);
//	void __fastcall ShowData(void);

};
//---------------------------------------------------------------------------
extern PACKAGE TGraferForm *GraferForm;
//---------------------------------------------------------------------------
#endif
