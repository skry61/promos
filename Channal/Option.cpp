//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Option.h"
#include "Math.hpp"
#include "IniFiles.hpp"
#include "Logs.h"
#include "..\Src\Simulate_PLC.h"
//#include "..\Src\Simulate_BUSENTX11.h"
#include "..\Src\MainScreen.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOptionForm *OptionForm;

TThreadSafeBool ConnectCommand;
TThreadSafeBool DisconnectCommand;

//---------------------------------------------------------------------------
__fastcall TOptionForm::TOptionForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
//__fastcall TOptionForm::~TOptionForm()
//{
//}
//---------------------------------------------------------------------------
void __fastcall TOptionForm::Button1Click(TObject *Sender)
{
	ConnectCommand = true;
}
//---------------------------------------------------------------------------
void __fastcall TOptionForm::Button2Click(TObject *Sender)
{
	DisconnectCommand = true;
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::FormCreate(TObject *Sender)
{
//	int Tag = ((TButton *)Sender)->Tag;
	PortScanTimerTimer(0);												//�������� ������������ ��������� � ������� ������
	if (PortComboBox->Items->Count) PortComboBox->ItemIndex = 0;		//�������� ������ ���-����

	//��������� ������ ��������� COM-������
	BaudrateComboBox->Clear();
	BaudrateComboBox->AddItem("9600",NULL);
	BaudrateComboBox->AddItem("19200",NULL);
	BaudrateComboBox->AddItem("38400",NULL);
	BaudrateComboBox->AddItem("57600",NULL);
	BaudrateComboBox->AddItem("115200",NULL);
	BaudrateComboBox->ItemIndex = 2;
	//�������� �������� �� INI-�����
	TMemIniFile *ini = new TMemIniFile(ChangeFileExt(Application->ExeName,".ini"));

	AddrEdit->Text				= ini->ReadString("Server","Addr","127.0.0.1");
	PortEdit->Text				= ini->ReadString("Server","Port","20332");

	AnsiString str				= ini->ReadString("ComPort","Name","").UpperCase();
	int n = PortComboBox->Items->IndexOf(str);
	if (n>=0) PortComboBox->ItemIndex = n;

	str							= ini->ReadString("ComPort","Baudrate","");
	n = BaudrateComboBox->Items->IndexOf(str);
	if (n>=0) BaudrateComboBox->ItemIndex = n;

	str							= ini->ReadString("ComPort","ByteSize","7");
	n = BitCountComboBox->Items->IndexOf(str);
	if (n>=0) BitCountComboBox->ItemIndex = n;

	ParityComboBox->ItemIndex	= ini->ReadInteger("ComPort","Parity",2);
	NodeEditMB->Text =  ini->ReadInteger("ComPort","Node",1);
	str							= ini->ReadString("ComPort","Protocol","ModBus ASCII");
	n = COM_ProtocolComboBox->Items->IndexOf(str);
	if (n>=0) COM_ProtocolComboBox->ItemIndex = n;
	else COM_ProtocolComboBox->ItemIndex = 0;

	delete ini;

	ComPort 		 = new TComPortIOHandler();      			// ������������ �������

	PLCDriver = new TPLCDriver(TULogs::GetLogCOM());	// ���������� �������
	PLCDriver->SetIOHandler(ComPort);
    // ��� ��� ��������� ������
#ifndef EASY
	PLCDriver->Run(tpNormal);
#endif

}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::FormDestroy(TObject *Sender)
{
	RefreshTimer->Enabled = false;
	PortScanTimer->Enabled = false;

#ifndef EASY
	PLCDriver->Stop();
#endif
	ComPort->Disconnect();
	delete PLCDriver;
	delete ComPort;
	PLCDriver = NULL;
	ComPort = NULL;

	//��������� ��������� � INI-����
	TStringList *list = new TStringList;

	list->Add("[Server]");
	list->Values["Addr"]	= AddrEdit->Text;
	list->Values["Port"]	= PortEdit->Text;

	list->Add("");

	list->Add("[ComPort]");
	list->Values["Name"]		= PortComboBox->Text;
	list->Values["Baudrate"]	= BaudrateComboBox->Text;
	list->Values["ByteSize"]	= BitCountComboBox->Text;
	list->Values["Parity"]		= ParityComboBox->ItemIndex;
	list->Values["Node"]		= NodeEditMB->Text;
	list->Values["Protocol"] 	= COM_ProtocolComboBox->Text;

	list->SaveToFile(ChangeFileExt(Application->ExeName,".ini"));
	list->Clear();
	delete list;
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::RefreshTimerTimer(TObject *Sender)
{
	AnsiString str;
	TColor color;
	if (ConnectCommand==true)	{str = "�����������"; color = clYellow;}
	else
	if (TCPClient->Connected)	{str = "���������"; color = clLime;}
	else						{str = "��������"; color = clSilver;}

	TCPStateLabel->Caption = str;
	TCPStateLabel->Color = color;

	if (ComPort && ComPort->Connected())	{str = "���������"; color = clLime;}
	else						{str = "��������"; color = clSilver;}

	COMStateLabel->Caption = str;
	COMStateLabel->Color = color;
	if(PLCDriver)
	{
		COMRXLabel->Caption = PLCDriver->GetRX_Total();
		COMTXLabel->Caption = PLCDriver->GetTX_Total();
		LErrCount->Text = PLCDriver->GetError();
	}
	Label17->Caption = PLCDriver->GetSliceCount();
	Label16->Caption = MainForm->TimeTickCount;

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------
// callback-������� ��� ���������� ������ ���-������
//---------------------------------------------------------------
int __fastcall PortListSort(TStringList* AList, int Index1, int Index2)
{
	return CompareValue((int)AList->Objects[Index1],(int)AList->Objects[Index2]);
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::PortScanTimerTimer(TObject *Sender)
{
	HANDLE hDlg;
	HKEY hKey;
	bool flag;

	//��������� ��� ������������ � ������� �������� �� �������
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), 0, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS) return;

	DWORD NumVal = 0;
	char * pValue0 = new char[40];
	char * pValue1 = new char[40];
	DWORD cValue1 = 40, cValue0 = 40, Type;

	TStringList *TempList = new TStringList;

	flag = false;

	while (!(RegEnumValue(hKey, NumVal++, pValue0, &cValue0, NULL, &Type, (LPBYTE)pValue1, &cValue1))) {

		//���� � ������ ��� ������ �� �����, �� ��������� �����
		if (TempList->IndexOf(pValue1)<0) TempList->AddObject(pValue1,(TObject *)atoi(&pValue1[3]));

        if (AnsiString(pValue1) == PortComboBox->Text) flag = true;
		cValue1 = cValue0 = 40;
	}
//	delete pValue1;
//	delete pValue0;

	//�������� ��������, ���� �� �������� � ��������� (���������/���������, ���������� ������)
	flag = false;
	if (TempList->Count != PortComboBox->Items->Count) flag = true;
	else {
		for (int i = 0; i < TempList->Count; i++) {
			if (PortComboBox->Items->IndexOf(TempList->Strings[i])<0) {
				flag = true;
				break;
			}
		}
	}

	//���� ���� ��������� � ��������� - ������ ������� �� � PortComboBox
	if (flag) {
		TempList->CustomSort(PortListSort);									//�������� ���������� ���������� ������
		AnsiString prev = PortComboBox->Text;								//���������� ����� � �����-�����
		PortComboBox->Clear();												//������� ������ � �����-�����
		for (int j = 0; j < TempList->Count; j++) {							//��������� ������ �����-�����
			if (PortComboBox->Items->IndexOf(TempList->Strings[j])<0)
				PortComboBox->Items->Add(TempList->Strings[j]);
		}
		PortComboBox->ItemIndex = PortComboBox->Items->IndexOf(prev);		//������������� �����-���� �� ���������� �����
	}
	delete TempList;
	delete [] pValue1;
	delete [] pValue0;
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::PortComboBoxKeyPress(TObject *Sender, char &Key)
{
	Key = 0; 																//���������� ������� ����� �������
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::ConnectButtonClick(TObject *Sender)
{
	ComPort->SetPortName(PortComboBox->Text);
	ComPort->SetBaudRate(StrToIntDef(BaudrateComboBox->Text,115200));

	AnsiString Format;
	switch (ParityComboBox->ItemIndex) {
		case 0:		Format = "E"; break;
		case 1:		Format = "O"; break;
		case 3:		Format = "M"; break;
		case 4:		Format = "S"; break;
		default:	Format = "N"; ParityComboBox->ItemIndex = 2;
	}

	switch (BitCountComboBox->Text.ToIntDef(8)) {
		case 5:
		case 6:
		case 7: break;
		default: BitCountComboBox->ItemIndex = 3;
	}

	Format = AnsiString(BitCountComboBox->Text.ToIntDef(8)) + Format + "1";
	ComPort->SetFormat(Format);

	ComPort->SetRXBufferSize(1024);
	ComPort->SetTXBufferSize(1024);
	ComPort->SetEvents(TComEvents() << evRxChar);

	PLCDriver->Node = NodeEditMB->Text.ToIntDef(1);
	PLCDriver->Echo = cbEcho->Checked;
	if (!ComPort->Connected()) {
		if(COM_ProtocolComboBox->ItemIndex == 0)
		{
			PLCDriver->SetProtocol(new TModBusProtocolASCII());
		}else
		if(COM_ProtocolComboBox->ItemIndex == 1)
		{
			PLCDriver->SetProtocol(new TModBusProtocolRTU());
		}
/*!!!		ComPort->ClrRTS();
		ComPort->SetRTS();
		ComPort->ClrRTS();  */
		PLCDriver->SetLogQueue(TULogs::GetLogCOM());

		if (ComPort->Connect()) {
			ConnectButton->Caption = "&���������";
		}
	}
	else {
		if (ComPort->Disconnect()) {
			ConnectButton->Caption = "&����������";
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::ResetTCPButtonClick(TObject *Sender)
{
//	BridgeDriver->COMRXCounter = BridgeDriver->COMTXCounter = 0;
}
//---------------------------------------------------------------------------



void __fastcall TOptionForm::btLogClick(TObject *Sender)
{
	if(ULogs)
		ULogs->Show();
	else
	{
		ULogs = new TULogs(Application);
		ULogs->Init();
		ULogs->Show();
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::ResetCOMButtonClick(TObject *Sender)
{
	 PLCDriver->ResetRX_Total(); PLCDriver->ResetTX_Total();
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::cbSimulCOMClic(TObject *Sender)
{
	if(cbSimulCOM->Checked)
	{	if(SimulPLC_Form == NULL)
		{
			SimulPLC_Form = new TSimulPLC_Form (Application);
			SimulPLC_Form->Show();
		}
	}
	else
	if(SimulPLC_Form != NULL)
	{
		delete SimulPLC_Form;
		SimulPLC_Form = NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::FormActivate(TObject *Sender)
{
	PortScanTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TOptionForm::FormDeactivate(TObject *Sender)
{
	PortScanTimer->Enabled = false;
}
//---------------------------------------------------------------------------
int TOptionForm::ConnectPLC()			// ������������� � �����������
{
	ConnectButtonClick(NULL);
	return ComPort->Connected()?0:1;
}


