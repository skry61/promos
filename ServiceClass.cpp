//---------------------------------------------------------------------------


#pragma hdrstop

//#include "ServiceClass.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ServiceClass.h"
//TLogMemoQueue *LogQueue;

//---------------------------------------------------------------------------

#pragma package(smart_init)
	//������a �������
void TCustomQueue::Clear() {
	TList *List = LockList();
	while (List->Count) {
		DeleteConnector(List);
		List->Delete(0);
	}
	UnlockList();
};

//---------------------------------------------------------------------------
void TLogQueue::LogList()
{
	TList *List = LockList();
	while (List->Count) {
		Str = (AnsiString *)List->Items[0];
		show();
		delete Str;
		List->Delete(0);
	}
	UnlockList();
}

//---------------------------------------------------------------------------
void TLogQueue::LogList(TObject *Acceptor)
{
	memAcceptor = lAcceptor;
	lAcceptor = Acceptor;
	LogList();
	lAcceptor = memAcceptor;
}

//---------------------------------------------------------------------------
void TLogQueue::AddList(AnsiString s)
{
	AnsiString *str = new AnsiString(s);
	TList *List = LockList();
	List->Add(str);
	UnlockList();
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void TLogMemoQueue ::show()
{
	((TMemo*)lAcceptor)->Lines->Add(*Str);
}
//---------------------------------------------------------------------------
void TLogMemoQueue ::Init(TObject *Memo)
{
	SetAcceptor(Memo);
	AddList("Start!");
}
//---------------------------------------------------------------------------

//==============================================================================
//����������� / �������������� �������
//------------------------------------------------------------------------------
// ������������ �� HEX � Bin �����
unsigned short  GetByte(char c){
	if (c == ' ') return 0x2000;
	if((c  <= 'f')&&(c  >= 'a'))
		return (byte)((byte)c - 0x57);
	if((c  <= 'F')&&(c  >= 'A'))
		return (byte)((byte)c - 0x37);
	if((c  <= '9')&&(c  >= '0'))
		return (byte)((byte)c - 0x30);
	return	c<<8;
}

//------------------------------------------------------------------------------
//������������� ������
unsigned short  __fastcall DecodeTextToBin(char * Dest,char * Src, int Len)
{
	unsigned short  c;
	int Faza = 0,Index = 0;
	char Sum = 0;
	for(int i = 0; i < Len; i++)
	{
		c = GetByte(*Src++); Faza++;
		if(Faza == 3)
		{
			Dest[Index++] = Sum;
			if(c != 0x2000)
//			 || (a != 0x1000))
				return ((c & 0xff00)+Index);
			Sum = 0;
			Faza = 0;
		}else
		{
			if((c>>8) == 0)
				Sum = (Sum << 4) + (c & 0x0f);
			else
				return ((c & 0xff00)+Index);
		}
	}
	return ((c & 0xff00)+Index);
}

//------------------------------------------------------------------------------
//����������� ������
int __fastcall CodeBinToText(char * Dest,char * Src,int Len)
{
	int Count = 0;

	for(int i = 0; i < Len; i++)
	{
		byte c = Src[i];
		Dest[Count++] = (c >> 4) + (c > 0x9f? 0x37:0x30);
		Dest[Count++] = (c & 0x0f) + ((c & 0x0f) > 0x9? 0x37:0x30);
		Dest[Count++] = 0x20;
	}
	Dest[--Count] = 0;
	return Count;
}
//------------------------------------------------------------------------------
//����������� ������ ModBus
int __fastcall CodeBinToTextModBus(char * Dest,char * Src,int Len)
{
	int Count = 1;
	Dest[0] = ':';
	for(int i = 0; i < Len-1; i++)
	{
		byte c = *Src++;
		Dest[Count++] = (c >> 4) + (c > 0x9f? 0x37:0x30);
		Dest[Count++] = (c & 0x0f) + ((c & 0x0f) > 0x9? 0x37:0x30);
	}
	Dest[Count++] = 0x0d;
	Dest[Count++] = 0x0a;
	Dest[Count] = 0;
	return Count;
}
//------------------------------------------------------------------------------
//������������� ������
bool  __fastcall DecodeTextToBinModBus(char * Dest,char * Src, int Len)
{
	unsigned short  c1,c2;
	for(int i = 0; i < Len/2; i++)
	{
		c1 = GetByte(*Src++);
		c2 = GetByte(*Src++);
		if((c1>=0x100)||(c2>=0x100)) return false;
		*Dest++ = (c1<<4) + c2;
	}
	return true;
}

//==============================================================================
int __fastcall FloToInt10(double v)
{
	return ((int)(v*10));
}

//==============================================================================
void __fastcall ColorButtonTop(TBitBtn *BitBtn,TColor Color)
{
static TColor oldColor = (TColor)0;
	if(Color == oldColor) return;
	oldColor = Color;
 HDC DC;
 TCanvas* Canv;
	DC = GetDC(BitBtn->Handle);
	Canv = new TCanvas();
	Canv->Handle = DC;
/*	if(Canv->Brush->Color == Color)
	{
		delete Canv;
		return;
	}
*/
	Canv->Brush->Color = Color;
	Canv->Pen->Color = clBlack;
//	TPoint
	TRect R = BitBtn->ClientRect;
	TRect Rect = TRect(R.Left+2,R.Top+2,R.Right-2,R.Bottom-2);
	Canv->Rectangle(Rect);
	int W = Canv->TextWidth(BitBtn->Caption);
	int H = Canv->TextHeight("H");
	Canv->TextOut((R.Width()-W)/2,(R.Height()-H)/2,BitBtn->Caption);
	delete Canv;

}
