//---------------------------------------------------------------------------

#ifndef TWeldBeadH
#define TWeldBeadH
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>

#define POINTS 10		/*������� ����� �������� �������*/
extern double _GradToRad(double GrMin);

//����� �������� ������ � �������� � ������� �� ����������
class TWeldBead
{                                            // ��� 8 �����
	static double x_criticalArr[POINTS*2];// = { -1.0 ,-0.99,-0.98,-0.97,-0.95,-0.9,-0.8, -0.6, -0.3,  0.0,   0.3,0.6,0.8,0.9,0.95,0.97,0.98,0.99,    1.0};
	double y_criticalArr[POINTS*2];
	double r_criticalArr[POINTS*2];

	double x_ArrAngle0[POINTS*2];
	double y_ArrAngle0[POINTS*2];
	double x_ArrAngleA[POINTS*2];
	double y_ArrAngleA[POINTS*2];

	TPoint xy_PointsArrAngle0[POINTS*2];
	TPoint xy_PointsArrAngleA[POINTS*2];
	int DisplacementPixel_X;			// �������� ��������� ������� ��������� ������ �� �
	int	DisplacementPixel_Y;			// �������� ��������� ������� ��������� ������ �� Y
	double	KoeffPixMm;					// ����������� �������� ��-> Pixel
public:
	__fastcall TWeldBead();
	void SetDisplacementPixel(int DispPix_X,int DispPix_Y){DisplacementPixel_X = DispPix_X;DisplacementPixel_Y = DispPix_Y;};
	void SetSimpleBeadWeld(int pcF);
	void SetBeadWeld(double dirAngle,double	aKoeffPixMm, double BeadLen);
	void Draw(TCanvas * cnv,double xs,double ys, bool Fl_Angle);  // ������ ����� � ���������� ������


};

//---------------------------------------------------------------------------
#endif
