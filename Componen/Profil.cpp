//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>

//������� ��������������� ������ �� ���������
#define H1mm  	100.0
#define H2mm  	100.0
#define H3mm  	100.0
#define PixSize 0.20	//200 ������


#pragma hdrstop
#include <Math.hpp>
#include <Math.h>

//#include <SysUtils.hpp>
//#include <Classes.hpp>
//#include <Controls.hpp>
//#include <ExtCtrls.hpp>

#include "..\Service.h"
#include "TWeldBead.h"
#include "Profil.h"
#pragma package(smart_init)
//------------------------------------------------------------------------------
// ����������� (��������� �������� �����)
__fastcall TProfil::TProfil()
{
	W1 = 0;		  // ������� �������������� ������� ���������

	H1 = 100.0;		  // ������ ������������ ������� ���������
	H2 = 100.0;		  // ������� ��������� ������� ���������
	H3 = 100.0;		  // ������� ������������ ������� ���������
	L1 = 10.0;        // ���������� �� �������� (�� ����� ������)
	A  = 45.00;       // ���� �������
	D  = 300.0;       // ������� ���������� ���������
	//-----------
	Vcb   = 100.0;    // �������� ������							   ( �/�   )
	S     = 8.0;      // ������ ����								   ( ��    )
	d     = 2.5;      // ������� ���������							 ( ��    )
	Vw    = 10;       // �������� ������ ���������				  	( �/��� )
	wType = 1;  	  // ��� ��������� 0 - ����������, 1 - �������
	CalcPhisicGabarit();			// �������� �������� � ��������
}

//==============================================================================
TProfil& TProfil::operator =(TProfil& classFrom)
{
	W1 = classFrom.W1;		  // ������� �������������� ������� ���������

	H1 = classFrom.H1;		  // ������ ������������ ������� ���������
	H2 = classFrom.H2;		  // ������� ��������� ������� ���������
	H3 = classFrom.H3;		  // ������� ������������ ������� ���������
	L1 = classFrom.L1;        // ���������� �� �������� (�� ����� ������)
	A  = classFrom.A;         // ���� �������
	D  = classFrom.D;         // ������� ���������� ���������
	//-----------
	Vcb   = classFrom.Vcb;    // �������� ������							   ( �/�   )
	S     = classFrom.S;      // ������ ����								   ( ��    )
	d     = classFrom.d;      // ������� ���������							 ( ��    )
	Vw    = classFrom.Vw;     // �������� ������ ���������				  	( �/��� )
	wType = classFrom.wType;  // ��� ��������� 0 - ����������, 1 - �������

	CalcPhisicGabarit();	  // ������ ���������� ���������
//	ReCalc();  ��� ������ �����				  // �������� ������ �������
//	CalcPixelPoints();		  // ������ ������� ����� ������� ������� � ��������

	return *this;
}

//------------------------------------------------------------------------------
// �������� ��������� ����� ���������� �� ������� ������������� �����������
bool TProfil::operator == (TProfil& classFrom)
{
	if(W1 == classFrom.W1)		  // ������� �������������� ������� ���������
	 if(H1 == classFrom.H1)		  // ������ ������������ ������� ���������
	  if(H2 == classFrom.H2)		  // ������� ��������� ������� ���������
	   if(H3 == classFrom.H3)		  // ������� ������������ ������� ���������
//		if(L1 == classFrom.L1)        // ���������� �� �������� (�� ����� ������)
		 if(A  == classFrom.A)         // ���� �������
		  if(wType == classFrom.wType)  // ��� ��������� 0 - ����������, 1 - �������
		  {
//			BeadWeldWidth = Vw/Vcb * 60 * d;					// ������ (������� ���)
//			BeadWeldWidth *= 2;  //��� �������������� �������� � 2 ���� (������ b/a = 1/2) �� �������� �������
			return true;
		  }
		   //-----------
/*		  if(D  == classFrom.D)         // ������� ���������� ���������
			if(Vcb   == classFrom.Vcb)    // �������� ������							   ( �/�   )
			 if(S     == classFrom.S)      // ������ ����								   ( ��    )
			   if(d     == classFrom.d)      // ������� ���������							 ( ��    )
				if(Vw    == classFrom.Vw)     // �������� ������ ���������				  	( �/��� )
				 if(wType == classFrom.wType)  // ��� ��������� 0 - ����������, 1 - �������
*/
	return false;

}
//------------------------------------------------------------------------------
// �������� ��������� ����� ���������� �� ������� ������������� �����������
bool TProfil::operator != (TProfil& classFrom)
{
/*	if(W1 == classFrom.W1)		  // ������� �������������� ������� ���������
	 if(H1 == classFrom.H1)		  // ������ ������������ ������� ���������
	  if(H2 == classFrom.H2)		  // ������� ��������� ������� ���������
	   if(H3 == classFrom.H3)		  // ������� ������������ ������� ���������
		if(L1 == classFrom.L1)        // ���������� �� �������� (�� ����� ������)
		 if(A  == classFrom.A)         // ���� �������
				 if(wType == classFrom.wType)  // ��� ��������� 0 - ����������, 1 - �������
		   //-----------
*/		  if(D  == classFrom.D)         // ������� ���������� ���������
			if(Vcb   == classFrom.Vcb)    // �������� ������							   ( �/�   )
			 if(S     == classFrom.S)      // ������ ����								   ( ��    )
			   if(d     == classFrom.d)      // ������� ���������							 ( ��    )
				if(Vw    == classFrom.Vw)     // �������� ������ ���������				  	( �/��� )
					return false;
	//-----------
	Vcb   = classFrom.Vcb;    // �������� ������							   ( �/�   )
	S     = classFrom.S;      // ������ ����								   ( ��    )
	d     = classFrom.d;      // ������� ���������							 ( ��    )
	Vw    = classFrom.Vw;     // �������� ������ ���������				  	( �/��� )
	return true;

}

//------------------------------------------------------------------------------
//
bool __fastcall TProfilPixel::ReCalcBead()
{
	if(Vcb != 0)
	{
//		BeadWeldWidth = Vw/Vcb * 60 * d;					// ������ (������� ���)
		BeadWeldWidth = sqrt(Vw/Vcb * 30 * d*d);					// ������ (������� ���)
		BeadWeldWidth *= 2;  //��� �������������� �������� � 2 ���� (������ b/a = 1/2) �� �������� �������
		S_sin = sin(AngleRad) * S;	 // ����� ����
		S_cos = cos(AngleRad) * S;	 // ������� ����
		return true;
	}
	return false;
}

//------------------------------------------------------------------------------
//������ ��������� � ��.���
// �����: ��� �������� ��������� False - �������� ������� ������� ���
bool __fastcall TProfil::CalcPhisicGabarit()			// �������� �������� � ��������
{
//	tgA = tan(_GradToRad(A));
	AngleRad = _GradToRad(A);
	tgA = tan(AngleRad);
	cosA= cos(AngleRad); 
	sinA= sin(AngleRad); 
	S_sin = sin(AngleRad) * S;	 // ����� ����
	S_cos = cos(AngleRad) * S;	 // ������� ����

	if (H2 == 0) W2 = 0;
	else
	if (A == 90) {W2 = H2; H2 = 0;}
	else W2 = tgA*H2;

	// ��������� �������� ������� �����
	Ygab = H1+H2+H3;

	if(W1 == 0)			// ���� �������������� ��������� ���
		if(W2 < (Ygab/10)) GAP_W1 = (Ygab/5);								// ���� ��� �������������� -�������������� - 20% �� ������������
		else if(((W2 > (Ygab/5))&&(W2 < (Ygab/2)))) GAP_W1 = (Ygab/10);		// ���� ���� � �� 20 �� 50 % �������������� - 10% �� ������������
		else GAP_W1 = (W2/10);												// ���� ������ 50%, �������������� - 10% �� ��������� �� �����������
	else                // ���� �������������� ��������� ����,
		GAP_W1 = 0;		// �� ��� ������������� �������
	// ������������� ������ ��������� �����������
	Xgab = GAP_W1 + W1+W2;

	// ������������� ������ ��������� ���������
	if(Ygab == 0) {Ygab = GAP_H = Xgab/10;}									// ���� ��� ������������ -������������ - 10% �� ��������������
	return wType? D>W2*2:true;
}

//------------------------------------------------------------------------------
// ������ ����� A-F
void 	__fastcall TProfil::CalcMmPoints()
{
	Amm = TDPoint(-Xgab,Ygab);
	Bmm = TDPoint(-Xgab,0);
	Cmm = TDPoint(0,0);
	Dmm = TDPoint(0,H1);
	Emm = TDPoint(-W2,H2+H1);
	Fmm = TDPoint(-W2,Ygab);//H3+H2+H1);
	ShapePointArray[0] = Amm;
	ShapePointArray[1] = Bmm;
	ShapePointArray[2] = Cmm;
	ShapePointArray[3] = Dmm;
	ShapePointArray[4] = Emm;
	ShapePointArray[5] = Fmm;
	PointCount		   = 6;	
}

//------------------------------------------------------------------------------
/*
//������ ��������� � ��.���
void __fastcall TProfil::FillShapePointArray()
{
	int PointCount = 0;

	ShapePointArray[PointCount++] = TDPoint(0.0,0.0);		// ��������� ����� ������ �������
	ShapePointArray[PointCount++] = TDPoint(0.0,Ygab);		// ���, ����� ����
	ShapePointArray[PointCount++] = TDPoint(Xgab,Ygab);		// ���, ������ ����
	if(H1 > 0)
		ShapePointArray[PointCount++] = TDPoint(Xgab,Ygab);		// ���, ������ ����
}
*/
//------------------------------------------------------------------------------
//------------------------       TProfilPixel            -----------------------
//------------------------------------------------------------------------------

__fastcall TProfilPixel::TProfilPixel(int aBorder,int aWinWidth,int aWinHeight,
									  int aWidthMax,int aHeightMax,int arrowSize,double aPixSize):TProfil()
{
	Border    = aBorder;
	WinWidth  = aWinWidth;
	WinHeight = aWinHeight;
	WidthMax  = aWidthMax;
	HeightMax = aHeightMax;
	ArrowSize = arrowSize;
	PixelSize = aPixSize;

	// �������� ������� ���������� TBitmap
	Height 	  = aHeightMax;
	Width  	  = aWidthMax;

	FOColor = clBlack;
	FIColor = clWhite;
	FAColor = clBlue;

	H_Div_W = ((double)(WinHeight - Border*4))/(WinWidth - Border*2 - ArrowSize);

	ReCalc();			// �������� ������ �������
	CalcPixelPoints();	// ������ ������� ����� ������� ������� � ��������
//	DrawProfil();	
};
//------------------------------------------------------------------------------
// �������� ������ ��� ����������
int __fastcall TProfilPixel::ReCalc()
{
	if(H_Div_W < (Ygab/Xgab))	// ����������� ������� ���� �� �������� ������ ����������� �������� ������
	{							// (����������� ���������, � ����������� ��� ������)
		Koeff_PixToMm = Ygab/(WinHeight - Border*4);
		Koeff_MmToPix = PixOnMm = (WinHeight - Border*4)/Ygab;
	}
	else // (����������� �����������, � ��������� ��� ������)
	{
		Koeff_PixToMm = Xgab/(WinWidth - Border*2 - ArrowSize);
		Koeff_MmToPix = PixOnMm = (WinWidth - Border*2 - ArrowSize)/Xgab;
	}
/*
	double  Vcb;    // �������� ������							   ( �/�   )
	double  S;      // ������ ����								   ( ��    )
	double  d;      // ������� ���������							 ( ��    )
	double  Vw;     // �������� ������ ���������						( �/��� )
					// ������� ������� S = �*a*b (a/b=2),  ������� ����� S=�*r*r
	double  Sw = MPar.Vw/MPar.Vcb * 60 * MPar.d*MPar.d*Pi/4;      // ������� �����
			Dn = sqrt(Sw*2/Pi)*2 = sgrt(Vw/Vcb * 30 * d*d)*2
	double  Sw1 = Sw * 2;      // ������� �����
	double  a = Sw * 2;        // ������� �������
	double  Sw1 = Sw * 2;      // ������� �������     �*d*d/4 = �*a*a/2
*/
	BeadWeldWidth = sqrt(Vw/Vcb * 30 * d*d);					// ������ (������� ���)
	BeadWeldWidth *= 2;  //��� �������������� �������� � 2 ���� (������ b/a = 1/2) �� �������� �������
//	BeadWeldWidth *= 2;  //��� ������ ������� ������ ��������� �������


	MinMashtab = FMashtab = PixOnMm * PixSize;
	MaxMashtab = ((double)(HeightMax - Border*4))/(WinHeight - Border*4);
	// �����: ��� �������� ��������� �������� ������� ������� ���
	if( wType && (D<=W2*2))  return 1;
	return 0;
}

//------------------------------------------------------------------------------
// ������ ������� ����� ������� ������� � ��������
void __fastcall TProfilPixel::CalcPixelPoints()
{
	CalcMmPoints();
	for (int i = 0; i< PointCount;i++)
	{
		ShapePixPointArray[i].x = (Xgab + ShapePointArray[i].x) * Koeff_MmToPix + Border;
		ShapePixPointArray[i].y = (Ygab - ShapePointArray[i].y) * Koeff_MmToPix + Border;
	}
	DisplacementPixel_X = Xgab * Koeff_MmToPix + Border;
	DisplacementPixel_Y = Ygab * Koeff_MmToPix + Border;
	
}

//------------------------------------------------------------------------------
void __fastcall TProfilPixel::SetMashtab(double aMashtab)
{
	if((aMashtab <= MinMashtab)&&(FMashtab != MinMashtab))
		aMashtab = MinMashtab;
	if((aMashtab >= MaxMashtab)&&(FMashtab != MaxMashtab))
		aMashtab = MaxMashtab;
	{
		FMashtab = aMashtab;
		Koeff_MmToPix = aMashtab/PixSize;
		CalcPixelPoints();
		DrawProfil();
		// ����� �������� � ��������� �������

	}
};

//------------------------------------------------------------------------------
// ����� ��� ����� 0 ��� 90��
void TProfilPixel::SetFontDir(bool Dir)
{
	LOGFONT n;
	Canvas->Brush->Style = bsClear;
	ZeroMemory(&n, sizeof(LOGFONT));
	n.lfHeight = 20;
	if(!Dir)
		n.lfEscapement = 10 * 90; 	//------------------- ������� ������� 90 ��
	else
		n.lfEscapement = 0;       	//------------------- ������� ������� 0 ��

//	n.lfEscapement = 10 * 90; 		//------------------- A zdes gradusi stavit
	n.lfOrientation = 10 * 45;
	n.lfCharSet = DEFAULT_CHARSET;
	strcpy(n.lfFaceName, "Tahoma");
	Canvas->Font->Handle = CreateFontIndirect(&n);
}

//------------------------------------------------------------------------------
//���������� ��������� �������. Dir = true - �� �����������
//������� ��������  ������ ��� �����
void TProfilPixel::DrawArrow(int x,int y,int len,bool dir,AnsiString Text)
{
	SetFontDir(dir);
	Canvas->MoveTo(x,y);
	if(dir)    //�������������
	{   //����� � ������ ������
		Canvas->LineTo(x+len,y);
		Canvas->LineTo(x+len-8,y+2);
		Canvas->MoveTo(x+len-8,y-2);
		Canvas->LineTo(x+len,y);
		//����� ������
		Canvas->MoveTo(x+8,y-2);
		Canvas->LineTo(x,y);
		Canvas->LineTo(x+8,y+2);

		Canvas->TextOut(x + len/2 - Canvas->TextWidth(Text)/2, y-Canvas->TextHeight("H")-2, Text);

	}else    // �����������
	{   //����� � ������� ������
		Canvas->LineTo(x,y-len);
		Canvas->LineTo(x+2,y-len+8);
		Canvas->MoveTo(x-2,y-len+8);
		Canvas->LineTo(x,y-len);
		//������ ������
		Canvas->MoveTo(x-2,y-8);
		Canvas->LineTo(x,y);
		Canvas->LineTo(x+2,y-8);

		Canvas->TextOut( x-Canvas->TextHeight("H")- 2, y - len/2 + Canvas->TextWidth(Text)/2,  Text);
	}
}

//------------------------------------------------------------------------------
//���������� ��������� �����. Dir = true - �� �����������
//����� ��������  ������ ��� �����
void TProfilPixel::DrawLine(TPoint P,int ArrowPos, bool dir)
{
	Canvas->PenPos = P;
	if(dir)		//���� �� ����������� �� ������ ������ ���� ������������
	{
		Canvas->LineTo(P.x,ArrowPos);
	}else{
		Canvas->LineTo(ArrowPos,P.y);
	}
}

//------------------------------------------------------------------------------
//���������� ������ �� ��������. Dir = true - �� �����������
//������� ��������  ������ ��� �����
void TProfilPixel::DrawSizeArrow(TPoint LeftBottom,bool isLB,TPoint RightTop,bool isRT,int ArrowPos,bool dir,AnsiString Text)
{
	TPoint StartArrowPos;	// ������� ������ ��������� �������
	int len;
	if(isLB)
		DrawLine(LeftBottom,ArrowPos,dir);
	if(isRT)
		DrawLine(RightTop,ArrowPos,dir);

	if(dir)			//���� �� ����������� �� ������ ������ ���� ������������
	{
		if(LeftBottom.y < ArrowPos)  // ���� ������ ���� �� �������
			StartArrowPos = TPoint(LeftBottom.x,ArrowPos - 5);
		else                         // ���� ������ ���� �� �������
			StartArrowPos = TPoint(LeftBottom.x,ArrowPos + 5);
		len = RightTop.x - LeftBottom.x;
	}else {
		if(LeftBottom.x < ArrowPos)  // ���� ������ ������ �� �������
			StartArrowPos = TPoint(ArrowPos - 5,LeftBottom.y);
		else                         // ���� ������ ����� �� �������
			StartArrowPos = TPoint(ArrowPos + 5,LeftBottom.y);
		len = LeftBottom.y - RightTop.y;
	}
	DrawArrow(StartArrowPos.x,StartArrowPos.y,len,dir,Text);

}

//------------------------------------------------------------------------------
//  ���� ������� �� ���������
void TProfilPixel::DrawAngle()
{
	int x = ShapePixPointArray[3].x;
	int y = ShapePixPointArray[3].y;
	int x4 = ShapePixPointArray[4].x;
	int y4 = ShapePixPointArray[4].y;
	Canvas->PenPos = ShapePixPointArray[3];
	Canvas->LineTo(x,y>Border+108?y-108:Border);


	Canvas->Arc(x-100, y-100, x+100, y+100,	x + 16, y-100, x4, y4+80);
//	Canvas->Arc(x-100, y-100, x+100, y+100,	x + 16, y-100, x-100, y-30);
	//������ ������ ������� ����
	Canvas->MoveTo(x +12,y-100-3);	Canvas->LineTo(x  ,y - 100  );	Canvas->LineTo(x +12,y- 100+6);
//	Canvas->MoveTo(x - 100-2,y-12);	Canvas->LineTo(x - 100  ,y   );	Canvas->LineTo(x - 100+5,y-12);
	//������� ������ ������� ����
	// ������ �� 2 / 2
//	double Root = 0.7071*100;
	double RootX = sin(AngleRad)*100;	 // ����� ����
	double RootY = cos(AngleRad)*100;	 // ������� ����

	Canvas->MoveTo(x-RootX-10, y-RootY+6);Canvas->LineTo(x-RootX,y-RootY);Canvas->LineTo(x-RootX-5, y-RootY+12);
	SetFontDir(true);
	Canvas->TextOut(x-25,y-120," A ");
//	Canvas->TextOut(x-RootX-25,y-RootY+20," A ");
}


//------------------------------------------------------------------------------
//���������� ��������� �����
// pcF - ������� ����� ���������� (�������������). 100% - ������ ����������
void TProfilPixel::DrawBeadWeld(int pcF,double xs,double ys,double dirAngle)
{
	TWeldBead * WeldBead = new TWeldBead();							// ������ ������
	WeldBead->SetDisplacementPixel(DisplacementPixel_X + Displacement_X*Koeff_MmToPix,
								   DisplacementPixel_Y-Displacement_Y*Koeff_MmToPix);

	WeldBead->SetBeadWeld(dirAngle,Koeff_MmToPix, BeadWeldWidth);	// �� ������ - ������� � ������������
	WeldBead->Draw(Canvas,xs,ys,dirAngle != 0);  						// ������ ����� � ���������� ������
	delete WeldBead;
}
//------------------------------------------------------------------------------
//���������� ������ (�����������) �������
// double xStart,double yStart, - ��������� �������
// double len,					- ������ ���
// int Count,					- ����������
// double dirAngle)				- ���� �������
TDPoint TProfilPixel::DrawWeldFace(double xStart,double yStart,double len,int Count,double dirAngle)
{

	TWeldBead * WeldBead = new TWeldBead();							// ������ ������
//	WeldBead->SetSimpleBeadWeld(int pcF); 							// �� ������ - ������� � ������������
	WeldBead->SetBeadWeld(dirAngle,Koeff_MmToPix, BeadWeldWidth);

//	WeldBead->SetDisplacementPixel(DisplacementPixel_X + Displacement_X*Koeff_MmToPix,
//								   DisplacementPixel_Y-Displacement_Y*Koeff_MmToPix);
	for(int i = 0; i < Count; i++)
	{
		WeldBead->Draw(Canvas,xStart,yStart,dirAngle != 0);  			// ������ ����� � ���������� ������
		xStart += sin(_GradToRad(dirAngle))*len;
		yStart += cos(_GradToRad(dirAngle))*len;
	}
	delete WeldBead;
	TDPoint p(xStart,yStart);
	return p;
}
//---------------------------------------------------------------------------
// ���������� ��������� �����
bool __fastcall TProfilPixel::DrawNewWeld(long WeldCount,TStringGrid * StringGrid)
{
	static double D_Sum = 0;
	static double D_Old = 0;
	static double S_Sum = 0;
	static int    Dir = 0;				//�����������
	static bool   FlLast = false;
	static double Angle = 0;
	double dX;
	if(WeldCount == 1)
	{
		Canvas->Pen->Color = clRed;
		Canvas->Pen->Width = 1;
		D_Sum = D_Old = StringGrid->Cells[2][1].ToDouble();
		S_Sum = 0;
		FlLast = false;
	}
	if(WeldCount < StringGrid->RowCount)
	{
		if(WeldCount < StringGrid->RowCount-1)			//����������� �������
		{
			double D = StringGrid->Cells[2][WeldCount+1].ToDouble();
//			if((StringGrid->Cells[2][WeldCount+1].ToDouble()!=D_Old)&&
			if((D != D_Old) &&
			   (StringGrid->Cells[3][WeldCount].ToDouble()!=0))
				Dir = 1;
			else
				Dir = 0;
		}else
			FlLast = true;

//		StringGrid->Cells[2][Line] = D;
//		StringGrid->Cells[3][Line] = S;

		if(StringGrid->Cells[4][WeldCount] == "��")
		{
			if(Dir == 0)		//�����������
			{
				dX = D_Sum -StringGrid->Cells[2][WeldCount].ToDouble();
				if(wType)
					dX = -dX;
				DrawBeadWeld(50,dX/2,S_Sum,0);
			}
			else
			{
				if(!FlLast)
				{
					dX = StringGrid->Cells[2][WeldCount+1].ToDouble() - D_Old;
					if(wType)
						dX = -dX;
					Angle = atan(dX/StringGrid->Cells[3][WeldCount].ToDouble()/2);
				}
				dX = D_Sum -StringGrid->Cells[2][WeldCount].ToDouble();
				if(wType)
					dX = -dX;
				DrawBeadWeld(50,dX/2,S_Sum,Angle);
			}
		}
		S_Sum += StringGrid->Cells[3][WeldCount].ToDouble();
		if(!FlLast)
			D_Old = StringGrid->Cells[2][WeldCount+1].ToDouble();

	}
/*
StringGrid->RowCount
		DrawBeadWeld(50,ShapePointArray[2].x,ShapePointArray[2].y,0);
	}else
	if(WeldCount < 10)
		DrawBeadWeld(50,ShapePointArray[2].x,ShapePointArray[2].y+S * (WeldCount-1),0);
*/
	return WeldCount < StringGrid->RowCount;
}

//------------------------------------------------------------------------------
//���������� �������
void TProfilPixel::DrawProfil()
{
//	Canvas->Font->Size = Border;
//reeImage();
//anvas->F
//	Assign(NULL);
	Canvas->Brush->Style = bsSolid;//bsClear;
	Canvas->FillRect(TRect(0,0,Width,Height));
	if(Canvas->Font->Size < 8 ) Canvas->Font->Size = 8;
	Canvas->Pen->Color = FOColor;

	//������
	Canvas->Brush->Color = FOColor;
	Canvas->Brush->Style = bsBDiagonal;
	Canvas->Pen->Width = 3;
	Canvas->Polygon(ShapePixPointArray,PointCount-1);
	Canvas->Brush->Style = bsClear;

	// �������
	Canvas->Pen->Width = 1;
	// ��� �1
	DrawSizeArrow(ShapePixPointArray[2],true,ShapePixPointArray[3],true,
				  ShapePixPointArray[2].x + ArrowSize,false,"H1");
	// ��� �2
	DrawSizeArrow(ShapePixPointArray[3],false,ShapePixPointArray[4],true,
				  ShapePixPointArray[2].x + ArrowSize,false,"H2");
	// ��� �3
	DrawSizeArrow(ShapePixPointArray[4],false,ShapePixPointArray[5],true,
				  ShapePixPointArray[2].x + ArrowSize,false,"H3");
	DrawAngle();
/*
	Canvas->Pen->Color = clRed;
	Canvas->Pen->Width = 1;
	DrawBeadWeld(50,ShapePointArray[2].x,ShapePointArray[2].y,0);
	DrawBeadWeld(50,ShapePointArray[2].x,ShapePointArray[2].y+S,0);
*/
/*
	{
		cnv->Pen->Color = clRed;
		cnv->Pen->Width = 1;
//			DrawBeadWeld(cnv,50,180,120,MPar.S,0);
		SetDisplacementP(Parrow[ParrowBotRightM].x,Parrow[ParrowBotRightM].y);
		int BeadCount = ProcessSimulator->GetCount();
		int Limit1 = MPar->H1 / MPar->S+1;
		int Limit2 = MPar->H2 / cos(_GradToRad(MPar->A)) / MPar->S;
		int Limit3 = MPar->H3 / MPar->S+1;
		int Cnt    = BeadCount;
		int BeadCount0  = BeadCount>Limit1?Limit1:BeadCount;
		Cnt -= Limit1;
		int BeadCountA  = BeadCount>Limit1?Cnt>Limit2?Limit2:Cnt:0;
		Cnt -= Limit2;

		int BeadCount01 = (BeadCount>(Limit1+Limit2))?Cnt>Limit3?Limit3:Cnt:0;
		Cnt -= Limit2;
//!!				KoeffPixMm = (double)(Parrow[ParrowBotRightM].y - Parrow[ParrowMiddleBotM].y) / MPar.H1;
		TDPoint p =DrawWeldFace(cnv,0,0,MPar->S,BeadCount0,0);
		p.x = -1;
//!!				p.x = 0;
		p.y = MPar->H1;

		double xm,ym,c =  MPar->H2*tan(_GradToRad(MPar->A));	// ���������
		xm = (Parrow[ParrowMiddleTopM].x - Parrow[ ParrowMiddleBotM].x);
		ym = (Parrow[ParrowMiddleBotM].y - Parrow[ParrowMiddleTopM ].y);
//!!				KoeffPixMm = sqrt(xm*xm+ym*ym) / c;
		DrawWeldFace(cnv,p.x,p.y,MPar->S,BeadCountA,MPar->A);
//!!				p.x =  MPar.H2*tan(_GradToRad(MPar.A));
		p.x =  MPar->H2*tan(_GradToRad(MPar->A))-4;
		p.y = MPar->H1+MPar->H2;
//!!				KoeffPixMm = (double)(Parrow[ParrowMiddleTopM ].y - Parrow[ParrowTopRightM].y) / MPar.H3;
		DrawWeldFace(cnv,p.x,p.y,MPar->S,BeadCount01,0);
		if(Cnt >= 0)
		{
			StopEmulation();
		}
	}
*/
}

//---------------------------------------------------------------------------
// !!!!  ���������� ���������(� �������� ���������)
//������� ��������� � StringGrid
void TProfilPixel::MakeProgramm0(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid)
{
	int Line = 1;
	int Limit1 = H1 / S+1;
	int Limit2 = H2 / S_cos+1;
	int Limit3 = H3 / S+1;

	if(Limit1 == 1)	Limit1 = 0;
	StringGrid->RowCount = Limit1+Limit2+Limit3+1;
//	if(Limit1>1)
	for (int i=0; i< Limit1; i++)
	{
		if(Line == 1)
			StringGrid->Cells[1][Line] = MacroNameStart;
		else
			StringGrid->Cells[1][Line] = MacroNameWork;
		StringGrid->Cells[2][Line] = D;
		StringGrid->Cells[3][Line] = S;
		StringGrid->Cells[4][Line] = "��";
		Line++;
	}
	if(Line>2)
		StringGrid->Cells[3][Line-1] = (double)((int)(H1 - S*(Limit1-1))*10)/10;

	double Diametr = 0;
	double Step = 0;
	double dx = S_sin;// MPar->S * sin(_GradToRad(MPar->A));		//
	double dy = S_cos;// MPar->S * cos(_GradToRad(MPar->A));		//
 //	double DeltaX = dx - (double)((int)dx*10)/10; dx = (double)((int)dx*10)/10;
	double RemY = dy - (double)((int)(dy*10))/10; dy = (double)((int)(dy*10))/10;
	double DeltaY = 0;// = dy - (double)((int)(dy*10))/10; //dy = (double)((int)(dy*10))/10;
	if(wType)
		dx = -dx;

	for (int i=0; i<Limit2; i++)
	{
		if(Line == 1)
			StringGrid->Cells[1][Line] = MacroNameStart;
		else
			StringGrid->Cells[1][Line] = MacroNameWork;
//		StringGrid->Cells[1][Line] = MacroName;

		StringGrid->Cells[2][Line] = D + (double)((int)(Diametr*10))/10;	// �������
		Diametr += dx*2;// + RemX;

		DeltaY = DeltaY + RemY;
		Step = dy + DeltaY;
		DeltaY = Step - (double)((int)(Step*10))/10;
//		DeltaY = (double)((int)(DeltaY*10))/10;
		StringGrid->Cells[3][Line] = (double)((int)(Step*10))/10;
		StringGrid->Cells[4][Line] = "��";
		Line++;
	}
	if(Limit2>2)
		StringGrid->Cells[3][Line-1] = (double)((int)(H2 - Step*(Limit2-1))*10)/10;

	Diametr = W2*2+0.05;
	if(wType)
    	Diametr = -Diametr;

	for (int i=0; i< Limit3; i++)
	{
		if(Line == 1)
			StringGrid->Cells[1][Line] = MacroNameStart;
		else
			StringGrid->Cells[1][Line] = MacroNameWork;
//		StringGrid->Cells[1][Line] = MacroName;
		StringGrid->Cells[2][Line] = D + (double)((int)(Diametr*10))/10;	// �������
		StringGrid->Cells[3][Line] = S;
		StringGrid->Cells[4][Line] = "��";
		Line++;
	}

}
//---------------------------------------------------------------------------
// !!!!  ���������� ���������(� �������� ���������)
//������� ��������� � StringGrid
void TProfilPixel::MakeProgramm(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid)
{
	int Line = 1;
	double H2_= H2/cosA;
	int Lines = (H1 + H2_ + H3) / S+1;
	double Step = S;
	double StepSum = 0;
	double Diametr = 0;
	double D_tmp = 0;
	double Height  = 0;
//	double HeightOld  = 0;
	double Diag  = 0;
	bool FlIsAngle1 = false;
	bool FlIsAngle2 = false;
	double dX = 0;
	double dY = 0;

//	int Limit2 = H2 / S_cos+1;
//	int Limit3 = H3 / S+1;

//	if(Limit1 == 1)	Limit1 = 0;
	StringGrid->RowCount = Lines+1;
//	if(Limit1>1)
	for (int i=0; i< Lines; i++)
	{
		if(Line == 1)
		{
			StringGrid->Cells[1][Line] = MacroNameStart;
//**			StringGrid->Cells[2][Line] = Diametr;
//			StringGrid->Cells[3][Line] = Step;
//			StringGrid->Cells[4][Line] = "��";
//			Line++;
		}
		else
			StringGrid->Cells[1][Line] = MacroNameWork;

		Height += S;
		if((Height<H1)||(Height>=(H2_+H1)))
		{
			Step = S;
			if(Height>=(H2_+H1))
			{
				if(!FlIsAngle2)
				{
					Diag = (Height - H2_ - H1);
					Step = Diag + (S - Diag)*cosA;
					FlIsAngle2 = true;
//					StepSum = Diag-S;
				}
				D_tmp = H2 * tgA*2;
			}
		}
		else
		{
			Diag = (Height - H1);
			if(!FlIsAngle1)
			{
				FlIsAngle1 = true;
				StepSum = Diag-S;
			}
			Step = cosA*Diag - StepSum;
			D_tmp = sinA*Diag*2;
			StepSum +=Step;
		}
//		Height += S;
//		Height += Step;

		if(wType)
			Diametr = -Diametr;

		double v = D + Diametr + dX;
		StringGrid->Cells[2][Line] = (double)((int)(v*10))/10;
		dX = v - (double)((int)(v*10))/10;

		v = Step + dY;
		StringGrid->Cells[3][Line] = (double)((int)(v*10))/10;;
		dY = v - (double)((int)(v*10))/10;

		StringGrid->Cells[4][Line] = "��";
		Line++;
		Diametr = D_tmp;
	}
	StringGrid->Cells[3][Line-1] = 0;
/*
	if(Line>2)
		StringGrid->Cells[3][Line-1] = (double)((int)(H1 - S*(Limit1-1))*10)/10;

	double Diametr = 0;
	double Step = 0;
	double dx = S_sin;// MPar->S * sin(_GradToRad(MPar->A));		//
	double dy = S_cos;// MPar->S * cos(_GradToRad(MPar->A));		//
 //	double DeltaX = dx - (double)((int)dx*10)/10; dx = (double)((int)dx*10)/10;
	double RemY = dy - (double)((int)(dy*10))/10; dy = (double)((int)(dy*10))/10;
	double DeltaY = 0;// = dy - (double)((int)(dy*10))/10; //dy = (double)((int)(dy*10))/10;
	if(wType)
		dx = -dx;

	for (int i=0; i<Limit2; i++)
	{
		if(Line == 1)
			StringGrid->Cells[1][Line] = MacroNameStart;
		else
			StringGrid->Cells[1][Line] = MacroNameWork;
//		StringGrid->Cells[1][Line] = MacroName;

		StringGrid->Cells[2][Line] = D + (double)((int)(Diametr*10))/10;	// �������
		Diametr += dx*2;// + RemX;

		DeltaY = DeltaY + RemY;
		Step = dy + DeltaY;
		DeltaY = Step - (double)((int)(Step*10))/10;
//		DeltaY = (double)((int)(DeltaY*10))/10;
		StringGrid->Cells[3][Line] = (double)((int)(Step*10))/10;
		StringGrid->Cells[4][Line] = "��";
		Line++;
	}
	if(Limit2>2)
		StringGrid->Cells[3][Line-1] = (double)((int)(H2 - Step*(Limit2-1))*10)/10;

	Diametr = W2*2+0.05;
	if(wType)
		Diametr = -Diametr;

	for (int i=0; i< Limit3; i++)
	{
		if(Line == 1)
			StringGrid->Cells[1][Line] = MacroNameStart;
		else
			StringGrid->Cells[1][Line] = MacroNameWork;
//		StringGrid->Cells[1][Line] = MacroName;
		StringGrid->Cells[2][Line] = D + (double)((int)(Diametr*10))/10;	// �������
		StringGrid->Cells[3][Line] = S;
		StringGrid->Cells[4][Line] = "��";
		Line++;
	}
*/
}

#pragma package(smart_init)

/*  1.������������� �������� ������ ���� �� ���������. � ��� ���. ���������� �������� ��������
	2.� ���� ������ ��������
	3.������������ ������� ������ ���� �������������
	4.���� ������ ���� �����������
	5.� ������� �������� ���� � ���
*/
