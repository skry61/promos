//---------------------------------------------------------------------------


#pragma hdrstop

#include "TWeldBead.h"
#include <Math.hpp>
#include <Math.h>

#define Pi    M_PI
#define RdToGr(Rd)    (180.0/M_PI * (Rd)) /*������� ������ � �������  */
#define GrToRd(Gr)    (M_PI/180.0 * (Gr)) /*������� �������� � �������*/


double TWeldBead::x_criticalArr[POINTS*2] = { -1.0 ,-0.99,-0.98,-0.97,-0.95,-0.9,-0.8, -0.6, -0.3,  0.0,   0.3,0.6,0.8,0.9,0.95,0.97,0.98,0.99,    1.0};

__fastcall TWeldBead::TWeldBead()
{
	SetSimpleBeadWeld(50);		//50% �������������� �������
	SetBeadWeld(45.0,2.5,10);	// �� ��������� ���� 45 ��, 2.5 ������� �� ��, ����� ������ 10 ��
	DisplacementPixel_X = 0;
	DisplacementPixel_Y = 0;
}

//------------------------------------------------------------------------------
//������ ����� ������  20 ����� ���������� �������
// pcF - ������� ����� ���������� (�������������). 100% - ������ ����������
void TWeldBead::SetSimpleBeadWeld(int pcF)
{
	double b = (double)pcF/100;
	int j,i;
	for(i = POINTS-1,j = POINTS-1; i < POINTS*2-1; i++,j--)
	{
		y_criticalArr[i] = y_criticalArr[j] = sqrt( ( 1.0 - x_criticalArr[i] * x_criticalArr[i]) * b * b );
		r_criticalArr[j] = r_criticalArr[i] = sqrt((y_criticalArr[i]*y_criticalArr[i]+x_criticalArr[i]*x_criticalArr[i]));

	}
}
//   y = sqrt( ( 1.0 - ((x*x)/(a*a)) ) * b * b );
//------------------------------------------------------------------------------
//����������� ������������� ���������� ���������� ������
//  pcF - ������� ����� ���������� (�������������). 100% - ������ ����������
//	double	KoeffPixMm  - ����������� �������/��
//  double BeadLen      - ����� ��� (��� �������)
void TWeldBead::SetBeadWeld(double dirAngle,double	aKoeffPixMm, double BeadLen)
{
	KoeffPixMm = aKoeffPixMm;										// ����������� �������� ��-> Pixel
	double Koeff = KoeffPixMm * BeadLen/2;
	double dirAngleRad = Pi/2 - dirAngle;							// ���� ��� �������� � �������
	for(int i = 0; i< POINTS*2-1; i++)
	{
		double A;
		if(x_criticalArr[i] == 0)
			A = Pi/2;
		else
			A = atan(y_criticalArr[i]/x_criticalArr[i]);
		if((A == 0.0)&&(x_criticalArr[i] < 0))
			A = Pi;
		else if(x_criticalArr[i]<0)
			A += Pi;

		A = dirAngleRad + A + Pi;// + Pi/2;

		x_ArrAngleA[i] = cos(A);
		y_ArrAngleA[i] = sin(A);

		xy_PointsArrAngle0[i]  = TPoint(y_criticalArr[i]*Koeff,x_criticalArr[i]*Koeff);   //��� ��� 0 - ��� �����������, �� � ���������� �
//		xy_PointsArrAngleA[i] = TPoint(y_criticalArr[i]*Koeff,x_criticalArr[i]*Koeff);
		xy_PointsArrAngleA[i] = TPoint(r_criticalArr[i]*cos(A)*Koeff,r_criticalArr[i]*sin(A)*Koeff);
	}
}
//---------------------------------------------------------------------------
void TWeldBead::Draw(TCanvas * cnv,double xs,double ys, bool Fl_Angle)  // ������ ����� � ���������� ������
{
	TPoint * Arr = Fl_Angle?xy_PointsArrAngleA:xy_PointsArrAngle0;
	int  pX =  DisplacementPixel_X + xs*KoeffPixMm;
	int  pY = DisplacementPixel_Y - ys*KoeffPixMm;
	cnv->MoveTo(pX + Arr[0].x, pY + Arr[0].y);
	for(int i = 1; i< POINTS*2-1; i++)
	{
		cnv->LineTo(pX + Arr[i].x, pY + Arr[i].y);
	}
	cnv->LineTo(pX + Arr[0].x, pY + Arr[0].y);

}

//---------------------------------------------------------------------------

#pragma package(smart_init)
