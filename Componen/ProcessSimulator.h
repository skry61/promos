//---------------------------------------------------------------------------

#ifndef ProcessSimulatorH
#define ProcessSimulatorH
#include "drvbase.h"

#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>


//------------------------------------------------
// ������� ����� ��������
//------------------------------------------------
class TProcessSimulator : public TDriver {
int DelayInterval;							   	// �������� ������ ����������
bool InTheGo;									// � ������
int  CycleCount;								// ������� ������
//HWND__ Handle;							 	// ����� ����� ��� �������� �� ���������
//HANDLE Handle;							 	// ����� ����� ��� �������� �� ���������
HWND Handle;									// ����� ����� ��� �������� �� ���������

public:
//	TSerialIOHandler *IOHandler;			 	//��������� ���������� �����-������

	TProcessSimulator(HWND aHandle);			//�����������
	~TProcessSimulator(){Stop();}				//����������
	void SetInterval(int Interval){if (Interval >= 10) DelayInterval = Interval; }

	virtual void Handler();					   	//����������� ������� ����������� ��������
	void SetGo(bool Fl);                        // ���������� �����������
	void SetGo();                               // ���� ����������
	int  GetCount(){return CycleCount;}		   	// ������� ������
};

//---------------------------------------------------------------------------
#endif
