//---------------------------------------------------------------------------

#include <vcl.h>
#include <Math.hpp>
#include <Math.h>
#pragma hdrstop

#include "TWorkShape.h"
#include "TWeldBead.h"
#pragma package(smart_init)
/*
	|W1|W2|
	---| _______
	   |
	   |  |    H3
	   |  ______
	   \ A
		\ |    H2        ��� � == 90, H2 == �������������� �������
		 \|_____
		  |
		  |    H1
		  |_____

*/
//---------------------------------------------------------------------------

#define Pi    M_PI

//==============================================================================
static inline void ValidCtrCheck(TWorkShape *)
{
	new TWorkShape(NULL,0,0,NULL,NULL);
}
//---------------------------------------------------------------------------
__fastcall TWorkShape::TWorkShape(TComponent* Owner,int aL,int aT, TForm * aForm,TStringGrid * aStringGrid)
	: TPaintBox(Owner)
{
	Border = 20;
	FValue = 0;
	FOColor = clBlack;
	FIColor = clWhite;
	FAColor = clBlue;
	FInterExt = true;
	FTitle  = "��������� ������";
	Left = aL;
	Top = aT;
	Width = 500;
	Height = 600;
	Displacement_X = 0;					// �������� ��������� ������� ��������� �� �
	Displacement_Y = 0;					// �������� ��������� ������� ��������� �� Y
	DisplacementPixel_X = 0;			// �������� ��������� ������� ��������� ������ �� �
	DisplacementPixel_Y = 0;			// �������� ��������� ������� ��������� ������ �� Y

	StringGrid_Process = aStringGrid;
	ProcessSimulator = new TProcessSimulator(aForm->Handle);	// ��������� ��������
	OnMouseDown = MyMouseDown;
	OnMouseMove = MyMouseMove;
	OnMouseUp = MyMouseUp;
	MouseShiftX = 0;
	MouseShiftY = 0;
	dMouseShiftX = 0;
	dMouseShiftY = 0;
	fl_MoveOn   = false;
	Fl_Emul		= false;
	MPar = new TProfilPixel(Border,Width,Height,2048,3072,
							60/*arrowSize*/,0.3);
	StringGrid = new TStringGrid(Owner);
}

//---------------------------------------------------------------------------
int Round(double v) {return v;}

//---------------------------------------------------------------------------
__fastcall TWorkShape::~TWorkShape()
{
//	delete StringGrid;
	delete MPar;
//	ProcessSimulator->Stop();
	delete ProcessSimulator;
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::MyMouseDown(TObject *Sender,    TMouseButton Button, TShiftState Shift, int X, int Y)
{
	fl_MoveOn = true;
	MouseStartX = X;
	MouseStartY = Y;
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::MyMouseMove(TObject *Sender,
	  TShiftState Shift, int X, int Y)
{
	if(fl_MoveOn)
	{

/*		if((MouseShiftX+MouseStartX - X) < 0)
			dMouseShiftX = MouseStartX - X - MouseShiftX;
		else
*/
			dMouseShiftX = MouseStartX - X;
/*		if((MouseShiftY+MouseStartY - Y) < 0)
			dMouseShiftY = MouseStartY - Y - MouseShiftY;
		else
*/
			dMouseShiftY = MouseStartY - Y;

		Invalidate();
//		Update ();
//		Refresh ();
//		Repaint();
	}
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::MyMouseUp(TObject *Sender,    TMouseButton Button, TShiftState Shift, int X, int Y)
{
	fl_MoveOn = false;
	MyMouseMove(Sender,Shift, X, Y);
	MouseShiftX+=dMouseShiftX;MouseShiftY+=dMouseShiftY;
	dMouseShiftX = 0;
	dMouseShiftY = 0;
}
//---------------------------------------------------------------------------
void TWorkShape::SetMashtab(TPoint MousePos,int Dir,TEdit * eMashtab)
{
	if((MousePos.x>10)&&(MousePos.x<(10+Width))  &&  (MousePos.y>50)&&(MousePos.x<(50+Height)))
	{
		if (Dir == 0) {
			MPar->SetMashtab(MPar->GetMashtab() * 1.02);
//			KoeffPixMm *= 1.3;
		}else
		{
			MPar->SetMashtab(MPar->GetMashtab() * 0.97);
//			KoeffPixMm *= 0.7 ;
    	}
		Mashtab = (double)((int )(MPar->GetMashtab()*100))/100;
		eMashtab->Text = Mashtab;
		Repaint();
	}
}


//------------------------------------------------------------------------------
// ���������� ����������� Paint
void __fastcall TWorkShape::FinishPaint(){
	Canvas->Pen->Color = FAColor;							   // ���������� ������� ���������������
	Canvas->Rectangle(0, 0, Width, Height);
	Canvas->Pen->Color = FOColor;

	BitBlt(Canvas->Handle,                                 // ����������� ��� �� ������� �����
		1,1,
		Width-3,Height-3,
		MPar->Canvas->Handle,MouseShiftX+dMouseShiftX,MouseShiftY+dMouseShiftY,
		SRCCOPY
		);
}

//------------------------------------------------------------------------------
void __fastcall TWorkShape::Paint()
{

	TPaintBox::Paint();
	SetRoundMode(rmNearest);
//	MPar->DrawProfil();
	FinishPaint();
}
/*
//---------------------------------------------------------------------------
void TWorkShape::SetDisplacementM(double mmX,double mmY)
{
	Displacement_X = mmX;					// �������� ��������� ������� ��������� �� �
	Displacement_Y = mmY;					// �������� ��������� ������� ��������� �� Y
}

//---------------------------------------------------------------------------
void TWorkShape::SetDisplacementP(double pX,double pY)
{
	DisplacementPixel_X = pX;			// �������� ��������� ������� ��������� ������ �� �
	DisplacementPixel_Y = pY;			// �������� ��������� ������� ��������� ������ �� Y
}
*/
//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetOColor(TColor Value )
{
	FOColor = Value;
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetIColor(TColor Value )
{
	FIColor = Value;
	Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetAColor(TColor Value )
{
	FAColor = Value;
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetMax(double Value)
{
	FMax    = Value;
//    Calculate(0);
//    CalculateValue(FValue);
	Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetMin(double Value)
{
	FMin    = Value;
//    Calculate(0);
//    CalculateValue(FValue);
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetInterExt(bool Value)
{
	FInterExt = Value;
//	Calculate(0);
//	CalculateValue(FValue);
	Invalidate();

}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetValue(double Value)
{
	if(FValue != Value)
	{
//		CalculateValue(Value);
		Invalidate();
	}

}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::SetTitle(AnsiString Value)
{
/*
	FTitle  = Value;
	TitlePos= Point(Ox - Canvas->TextWidth(FTitle) /2,Oy - Canvas->TextHeight("H")/ 2);
	Invalidate();
*/
}

//---------------------------------------------------------------------------
//���������� ��� ������ �� ������ ������
int __fastcall TWorkShape::SetParam(TProfil *aMasterParam,TEdit * eMashtab)
{
	int Res;
	if(*(TProfil *)MPar == *aMasterParam)
	{
		if(*(TProfil *)MPar != *aMasterParam)
			MPar->ReCalcBead();

		MPar->DrawProfil();
		return 0;
	}else
	{
		*(TProfil *)MPar = *aMasterParam;
		Res = MPar->ReCalc();			// ����������� ������
		MPar->CalcPixelPoints();			// ������ ������� ����� ������� ������� � ��������
		MPar->DrawProfil();

		Mashtab = (double)((int )(MPar->GetMashtab()*10))/10;
		eMashtab->Text = Mashtab;
	}
	return Res;
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::ShowEmulation()
{
	MPar->MakeProgramm(" "," ",StringGrid);
	StringGrid_Run = StringGrid;
	ProcessSimulator->SetGo();
	Fl_Emul	= true;
	Invalidate();

}
//---------------------------------------------------------------------------
// ���������� ��������� �����
bool __fastcall TWorkShape::DrawNewWeld(long WeldCount)
{
	if(isEmulationGo() && MPar->DrawNewWeld(WeldCount,StringGrid_Run))
		Invalidate();
	else
		Fl_Emul	= false;
	return Fl_Emul;
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::StopEmulation()
{
	Fl_Emul	= false;
	ProcessSimulator->SetGo(false);
}

//---------------------------------------------------------------------------
void __fastcall TWorkShape::ShowRun()
{
	MPar->DrawProfil();
	StringGrid_Run = StringGrid_Process;
	ProcessSimulator->SetGo();
	Fl_Emul	= true;
	Invalidate();

}
//---------------------------------------------------------------------------
void __fastcall TWorkShape::StopRun()
{
	Fl_Emul	= false;
	ProcessSimulator->SetGo(false);
}

//---------------------------------------------------------------------------
void TWorkShape::SetInterval(int Interval)
{
	 ProcessSimulator->SetInterval(Interval);
}

//---------------------------------------------------------------------------

void TWorkShape::MakeProgramm(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid)
{
	MPar->MakeProgramm(MacroNameStart,MacroNameWork,StringGrid);
}
//---------------------------------------------------------------------------
namespace Tworkshape
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TWorkShape)};
		RegisterComponents("Samples", classes, 0);
	}
}
//---------------------------------------------------------------------------
