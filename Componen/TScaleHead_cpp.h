//---------------------------------------------------------------------------

#ifndef TScaleHead_cppH
#define TScaleHead_cppH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
enum Pnt{
	ParrowTop,
	ParrowBotRight,
	ParrowBotLeft,
	ParrowMAX
};
struct TParamVisio;
//---------------------------------------------------------------------------
class PACKAGE TScaleHead : public TPaintBox
{
private:
	TColor	FOColor,FIColor,FAColor;
	double	FMin,FMax,FValue;
	int 	FInterv,
			FSlice;		// ����� ������� ����� �������� ��������
	AnsiString FTitle;
	int 	Border,Footer,ArrowLen;
	int 	tmpL,tmpH,H,W,tmp;
	int 	Rarc,Ox,Oy,Xs,Ys,Xe,Ye;  //�����
	double 	Angle,FullAngl,ValueAngl,A,Koeff,IntervAngl;
//    Slice:integer;                  	  // ����� ������� ����� ��������
	double 	StepValue;                	  // ��� �������� �����
	int 	RiskLen;      		       	  // ����� �����
	TPoint 	TitlePos,Parrow[ParrowMAX];
	int     OldH,OldW;

protected:
	void __fastcall SetOColor(TColor Value );
	void __fastcall SetIColor(TColor Value );
	void __fastcall SetAColor(TColor Value );
	void __fastcall SetMax(double Value);
	void __fastcall SetMin(double Value);
	void __fastcall SetInterv(int Value);
	void __fastcall SetValue(double Value);
	void __fastcall SetTitle(AnsiString Value);
	void __fastcall SetSlice(int Value);

	void Calculate(int Value);
	void CalculateValue(double Value );
	TPoint CalculatePoint(double Value );
	TPoint DrawLimitZone(TCanvas * cnv,TColor clr, TPoint Pstart,double Value);	

public:
	TParamVisio *ParamVisio;

	__fastcall TScaleHead(TComponent* Owner);
	__fastcall TScaleHead(TComponent* Owner,int L,int T);
	__fastcall TScaleHead(TComponent* Owner,int L,int T, TParamVisio *aParamVisio);
//	virtual __fastcall ~TScaleHead();
	virtual void __fastcall Paint(void);
	void __fastcall ScaleHeadClick(TObject *Sender);
	void __fastcall SetLimit(double Lred,double Lyel,double Ryel,double Rred);


__published:
	__property TColor OutSideColor 	={ read=FOColor, write=SetOColor};// default clBlack;
	__property TColor InSideColor 	={ read=FIColor, write=SetIColor};// default clWhite;
	__property TColor ArrowColor  	={ read=FAColor, write=SetAColor};// default clRed;
	__property double MaxValue    	={ read=FMax,    write=SetMax};// default 100;
	__property double MinValue    	={ read=FMin,    write=SetMin};// default 100;
	__property int Intervals   		={ read=FInterv, write=SetInterv};// default 10;
	__property double Value       	={ read=FValue,  write=SetValue};// default 0;
	__property AnsiString Title 	={ read=FTitle,  write=SetTitle};
	__property int Slice       		={ read=FSlice,  write=SetSlice};// default 2;  // ����� ������� ����� �������� ��������

};
//---------------------------------------------------------------------------
//class TParamVisio
struct TParamVisio
{
//private:	// User declarations
//public:		// User declarations
	AnsiString Title;		// �������� ���������
	AnsiString Name;		// ��� ���������
	int Min_ScaleValue;		// ����� �������� �� �����
	int Min_RedValue;		// ����� �������� �� ����� ���������
	int Min_YellowValue; 	// ����� �������� �� ����� ��������������

	int Max_YellowValue; 	// ������ �������� �� ����� ��������������
	int Max_RedValue;		// ������ �������� �� ����� ���������
	int Max_ScaleValue;		// ������ �������� �� �����


	TScaleHead *ScaleHead; 					// �������
//public:		// User declarations
//	__fastcall TParamVisio(){};
};
//---------------------------------------------------------------------------


#endif

