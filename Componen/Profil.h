//---------------------------------------------------------------------------

#ifndef ProfilH
#define ProfilH

#include <Controls.hpp>
#include <system.hpp>
#include <Grids.hpp>
/*
	|W1|W2|
	---| _______
	   |
	   |  |    H3
	   |  ______
	   \ A
		\ |    H2        ��� � == 90, H2 == �������������� �������
		 \|_____
		  |
		  |    H1
		  |_____

*/


//��������� ����� � ������� ���������
struct TDPoint
{
	double  x;
	double  y;
	TDPoint(){x=0;y=0;}
	TDPoint(double ax,double ay){x=ax;y=ay;}
//	TDPoint & operator =(TDPoint& classFrom){}; ���� �����


} ;

//---------------------------------------------------------------------------
// ������� �����������
class TWorkSurface
{

};

//---------------------------------------------------------------------------
class TProfil : public TBitmap
{
protected:
TDPoint ShapePointArray[20];
TDPoint Amm,Bmm,Cmm,Dmm,Emm,Fmm;

public:
	double  GAP_W1;	// ������� �������������� ������� (����� �����,������ ����������)
	double  W1;		// ������� �������������� ������� ���������
	double  W2;		// ������� �������������� ������� ���������

	double  GAP_H;	// ������������ ������� (���� ��� �� ������ �������������)
	double  H1;		// ������ ������������ ������� ���������
	double  H2;		// ������� ��������� ������� ���������
	double  H3;		// ������� ������������ ������� ���������
	double  L1;     // ���������� �� �������� (�� ����� ������)
	double  A;      // ���� �������
	double  D;      // ������� ���������� ���������
	//-----------
	double  Vcb;    // �������� ������							   ( �/�   )
	double  S;      // ������ ����								   ( ��    )
	double  d;      // ������� ���������							 ( ��    )
	double  Vw;     // �������� ������ ���������						( �/��� )
	bool    wType;	// ��� ��������� 0 - ����������, 1 - �������
	//------------------ ������������� -------------
	double Ygab,Xgab;// ���������� �������
	double  AngleRad;// ������������� ���� � ��������
	double  S_sin;	 // ����� ����
	double  S_cos;	 // ������� ����
	double  sinA;	 // C����
	double  cosA;	 // �������

	double  tgA;	// ������������� ����
	int PointCount; // ���������� �����

public:
	__fastcall TProfil();
	bool    __fastcall CalcPhisicGabarit();	//������ ���������� ���������
	void 	__fastcall CalcMmPoints();

//	void 	__fastcall FillShapePointArray();
	TProfil & operator =(TProfil& classFrom);
	bool operator == (TProfil& classFrom);
	bool operator != (TProfil& classFrom);
};

//---------------------------------------------------------------------------
class TProfilPixel : public TProfil
{
	double  FMashtab;	// �������
//	double  PredMashtab;// ������� �� ���������� ����
	int Border; 	 	// ���������� �� ������������
	int WinWidth;       // ������ ���� ������� � ��������
	int WinHeight;      // ������ ���� ������� � ��������
	int WidthMax;       // ������ ���� ��� ���������������
	int HeightMax;      // ������ ���� ��� ���������������
	int ArrowSize;      // ������ � �������� �������
	double PixelSize;   // ������ ������� � �� (�������� 0.3)
	double MinMashtab;	// ������� �����������
	double MaxMashtab;  // ������� ������������
	double H_Div_W;		// ����������� ��� ��������� ��������
	double	Displacement_X;					// �������� ��������� ������� ��������� �� �
	double	Displacement_Y;					// �������� ��������� ������� ��������� �� Y

	// ���������
	TColor	FOColor,FIColor,FAColor;
	double	BeadWeldWidth;					// ������ (������� ���)
	int		DisplacementPixel_X;			// �������� ��������� ������� ��������� ������ �� �
	int		DisplacementPixel_Y;			// �������� ��������� ������� ��������� ������ �� Y


	//------------------ ������������� � ������� � ������ �������� -------------
	TPoint ShapePixPointArray[20];
public:
	double Koeff_PixToMm;// ����������� �������� ������� � ��
	double Koeff_MmToPix,PixOnMm;// ����������� �������� �� � �������

 	__fastcall TProfilPixel(int aBorder,int aWinWidth,int aWinHeight,
									  int aWidthMax,int aHeightMax,int arrowSize,double aPixSize);

	int __fastcall ReCalc();
	bool __fastcall ReCalcBead();

	// ������ ������� ����� ������� � ��������
	void __fastcall CalcPixelPoints();
	void __fastcall SetMashtab(double aMashtab);
	double __fastcall GetMashtab(){return FMashtab;}

	//------------  ������  ---------------
	// ����� ��� ����� 0 ��� 90��
	void SetFontDir(bool Dir);
	// ��������� �������
	void DrawArrow(int x,int y,int len,bool dir,AnsiString Text);
	//��������� �����
	void DrawLine(TPoint P,int ArrowPos, bool dir);
	// �������
	void DrawSizeArrow(TPoint LeftBottom,bool isLB,TPoint RightTop,bool isRT,int ArrowPos,bool dir,AnsiString Text);
	//����
	void DrawAngle();
//	void DrawBeadWeld(int pcF,double xs,double ys,double len,double dirAngle);
	void DrawBeadWeld(int pcF,double xs,double ys,double dirAngle);
	TDPoint DrawWeldFace(double xStart,double yStart,double len,int Count,double dirAngle);
	// ���������� ��������� �����
	bool __fastcall DrawNewWeld(long WeldCount,TStringGrid * StringGrid);

	// �������� �������
	void DrawProfil();
    // �������� ��������
	void MakeProgramm0(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid);
	void MakeProgramm(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid);
__published:
//#warning dfgdfg4
//	__property double Mashtab = {read=FMashtab, write=SetMashtab, default=1.0};
//#warning dfgdfg5


};
//---------------------------------------------------------------------------
#endif
