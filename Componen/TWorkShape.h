//---------------------------------------------------------------------------

#ifndef TWorkShapeH
#define TWorkShapeH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//#include <Graphics.hpp>
#include <Grids.hpp>

#include "ProcessSimulator.h"
#include "Profil.h"
#include <system.hpp>

// ������������ ���������� ���� �� ����� �����������
#define MAXBEADS 300
/*
	|W1|W2|
	---| _______
	   |
	   |  |    H3
	   |  ______
	   \ A
		\ |    H2        ��� � == 90, H2 == �������������� �������
		 \|_____
		  |
		  |    H1
		  |_____

*/

//---------------------------------------------------------------------------
//��������� �������� ������ � �������� � ������� �� ����������
typedef struct sWeldBead
{
	static int Count;	// ��� ������������ �����
	int Number;     // ����� �����

	double  X;		// ��������� ����� ���������� ������ �
	double  Y;		// ��������� ����� ���������� ������ Y
	double  stepX;	// �������� ����� ���������� ������ �
	double  stepY;  // �������� ����� ���������� ������ Y
	//���������� �����������

	int		x1,y1,x2,y2,  xs,ys,xe,ye; 	// ���������� ����
	int		xline,yline; 				// ���������� �������
} TWeldBeadPoint;

//---------------------------------------------------------------------------
//��������� �����������
class TWeldFase
{
//	TPoint 	Parrow[
	TWeldBeadPoint wbArrH1[MAXBEADS];	// ��� �� ����������� H1
	TWeldBeadPoint wbArrH2[MAXBEADS];	// ��� �� ����������� H2
	TWeldBeadPoint wbArrH3[MAXBEADS];	// ��� �� ����������� H3

};
//---------------------------------------------------------------------------
enum PntMaster{
	ParrowTopLeftM,
	ParrowBotLeftM,
	ParrowBotRightM,
	ParrowMiddleBotM,
	ParrowMiddleTopM,
	ParrowTopRightM,
	ParrowMAX_M
};


//---------------------------------------------------------------------------
class PACKAGE TWorkShape : public TPaintBox
{
private:
	TStringGrid * StringGrid;			// ������� ���������
	TStringGrid * StringGrid_Process;  	// ������� ��������
	TStringGrid * StringGrid_Run;		// ������� ��������� �� �������
	bool    fl_MoveOn;
	int 	MouseStartX,MouseShiftX,dMouseShiftX;
	int 	MouseStartY,MouseShiftY,dMouseShiftY;
	TColor	FOColor,FIColor,FAColor;
	double	FValue;
	bool 	FInterExt;
	double 	FMin,FMax;
	AnsiString FTitle;
	int 	Border;//,Footer,ArrowLen;
	TPoint 	TitlePos;

	double	Mashtab;						// �������
	double	Displacement_X;					// �������� ��������� ������� ��������� �� �
	double	Displacement_Y;					// �������� ��������� ������� ��������� �� Y
	int		DisplacementPixel_X;			// �������� ��������� ������� ��������� ������ �� �
	int		DisplacementPixel_Y;			// �������� ��������� ������� ��������� ������ �� Y


	TProfilPixel   *MPar;			 		// ������� ��������� ����������
	bool 	Fl_Emul;	

	TProcessSimulator *ProcessSimulator;	// ��������� ��������
	void __fastcall SetOColor(TColor Value );
	void __fastcall SetIColor(TColor Value );
	void __fastcall SetAColor(TColor Value );
	void __fastcall SetMax(double Value);
	void __fastcall SetMin(double Value);
	void __fastcall FinishPaint();
	// ���������� �����
	void __fastcall MyMouseDown(TObject *Sender,    TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall MyMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall MyMouseUp(TObject *Sender,TMouseButton Button, TShiftState Shift, int X, int Y);

protected:
	void __fastcall SetInterExt(bool Value);
	void __fastcall SetValue(double Value);
	void __fastcall SetTitle(AnsiString Value);

public:

	__fastcall TWorkShape(TComponent* Owner,int L,int T, TForm * aForm,TStringGrid * aStringGrid);
	virtual __fastcall ~TWorkShape();
	virtual void __fastcall Paint(void);
	int __fastcall SetParam(TProfil *aMasterParam,TEdit * eMashtab);
	void __fastcall ShowEmulation();
	void __fastcall StopEmulation();
	bool __fastcall isEmulationGo(){return Fl_Emul;}
	void __fastcall ShowRun();
	void __fastcall StopRun();

	bool __fastcall DrawNewWeld(long WeldCount);

	void SetInterval(int Interval);
	void MakeProgramm(AnsiString MacroNameStart,AnsiString MacroNameWork,TStringGrid * StringGrid);
	void SetMashtab(TPoint MousePos,int Dir,TEdit * eMashtab);

__published:
	__property TColor OutSideColor 	={ read=FOColor, write=SetOColor};// default clBlack;
	__property TColor InSideColor 	={ read=FIColor, write=SetIColor};// default clWhite;
	__property TColor ArrowColor  	={ read=FAColor, write=SetAColor};// default clRed;
	__property bool InterExter 		={ read=FInterExt, write=SetInterExt};
	__property double MaxValue    	={ read=FMax,    write=SetMax};// default 100;
	__property double MinValue    	={ read=FMin,    write=SetMin};// default 100;
	__property double Value       	={ read=FValue,  write=SetValue};
	__property AnsiString Title 	={ read=FTitle,  write=SetTitle};
};
//---------------------------------------------------------------------------

#endif



