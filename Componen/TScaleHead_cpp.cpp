//---------------------------------------------------------------------------

#include <vcl.h>
#include <Math.hpp>
#include <Math.h>
#pragma hdrstop

#include "TScaleHead_cpp.h"
#include "EditScHead.h"
#include "MainScreen.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
const double DeltaAngle = M_PI/180*10;
#define Pi    M_PI
static inline void ValidCtrCheck(TScaleHead *)
{
	new TScaleHead(NULL);
}
 void __fastcall TScaleHead::ScaleHeadClick(TObject *Sender)
{
	int Bevel = (MainForm->Width - MainForm->ClientWidth)/2;
	int HeadHeight = MainForm->Height - MainForm->ClientHeight - Bevel;

	EditScaleHead->Left = MainForm->Left + Left + Bevel*2;
	EditScaleHead->Top = MainForm->Top + Top - EditScaleHead->Height + HeadHeight+MainForm->PanelProsessStatus->Height;
	EditScaleHead->SetSender(Sender);
	EditScaleHead->Show();
}

//---------------------------------------------------------------------------
__fastcall TScaleHead::TScaleHead(TComponent* Owner)
	: TPaintBox(Owner)
{
   FMax = 100;
   FValue = 0;
   FOColor = clBlack;
   FIColor = clWhite;
   FAColor = clBlue;
//   FAColor = clRed;
   FInterv = 10;
   FTitle  = "���(A)";
   FSlice  = 2;
   OldH = 0;
   OldW = 0;

	Left = 360;
	Top = 200;
	Width = 160;
	Height = 160;

	OnClick = ScaleHeadClick;
   //������ ������������
//   ControlStyle = ControlStyle + csOpaque;// SET
//   System::Include(ControlStyle,csOpaque);
}
//---------------------------------------------------------------------------
__fastcall TScaleHead::TScaleHead(TComponent* Owner,int L,int T)
	: TPaintBox(Owner)
{
   FMax = 100;
   FValue = 0;
   FOColor = clBlack;
   FIColor = clWhite;
   FAColor = clBlue;
//   FAColor = clRed;
   FInterv = 10;
   FTitle  = "���(A)";
   FSlice  = 2;
   OldH = 0;
   OldW = 0;

	Left = L;
	Top = T;
	Width = 160;
	Height = 160;

	OnClick = ScaleHeadClick;
   //������ ������������
//   ControlStyle = ControlStyle + csOpaque;// SET
//   System::Include(ControlStyle,csOpaque);
}
//---------------------------------------------------------------------------
__fastcall TScaleHead::TScaleHead(TComponent* Owner,int L,int T,TParamVisio *aParamVisio)
	: TPaintBox(Owner)
{
   FMax = aParamVisio->Max_ScaleValue;
   FMin = aParamVisio->Min_ScaleValue;
   FValue = aParamVisio->Min_ScaleValue;
   FOColor = clBlack;
   FIColor = clWhite;
   FAColor = clBlue;
//   FAColor = clRed;
//   FInterv = (FMax-FMin)/10;
   FInterv = 10;
   FTitle  = aParamVisio->Name;
   FSlice  = 2;
   OldH = 0;
   OldW = 0;

	Left = L;
	Top = T;
	Width = 320;
	Height = 260;

	OnClick = ScaleHeadClick;
	ParamVisio = aParamVisio;
   //������ ������������
//   ControlStyle = ControlStyle + csOpaque;// SET
//   System::Include(ControlStyle,csOpaque);
}

int Round(double v) {return v;}
//------------------------------------------------------------------------------
void __fastcall TScaleHead::Paint()
{

/*
	Border,Footer,ArrowLen,IntervSize:integer;
	tmpL,tmpH,H,W,tmp : integer;
	Rarc,Ox,Oy,Xs,Ys,Xe,Ye : integer;  //�����
	Angle,FullAngl,ValueAngl,A,Koeff,IntervAngl : double;
	Slice:integer;                     // ����� ������� ����� ��������
	StepValue:double;                  // ��� �������� �����
	RiskLen : integer;                 // ����� �����
	TitlePos,ParrowTop,ParrowBotRight,ParrowBotLeft : TPoint;
*/
	double CurAngle;
	int CurSlice,X,Y,i;
	AnsiString S;

	Graphics::TBitmap *TheImage;
//  Canvas.Brush.Color := FOColor;
  TPaintBox::Paint();
  if((OldH != Height) ||(OldW != Width))
  {
	 Calculate(0);
	 CalculateValue(FValue);
  }
  SetRoundMode(rmNearest);
/*
  if(Width < Height) then tmp := Width else tmp := Height;
  // ������������ ���������� � ����
  Border := tmp div 20; if Border < 2 then Border := 2;
  Footer := tmp div 4;  if Footer < 5 then Footer := 5;
  H := (Height - Border - Footer);  // ������ ����
  W := Width  - Border * 2;         // ������ ����
  //�������������� ��������� �������
  tmpH := H div 3;                  // ������ ������������
  tmpL := Width div 2 - tmpH;       // ����� ������� �������������

  // �����     sin
  Rarc :=  H - H div 5;             // ������
  Xe := Border*3;
  Xs := Width - Xe;
  A := (Width / 2 - Xe) / Rarc;
  Angle := ArcTan (sqrt (1-sqr (A)) / A);
//  FullAngl := Pi - Angle * 2;
  Koeff := (Pi - Angle * 2)/FMax;
  Ye :=  H - Round(Rarc * sin(Angle)) + Border;
  Ys := Ye;
  // �����
  FullAngl := Pi - Angle;

  IntervAngl := (Pi - Angle *2) / FInterv;
  StepValue:= FMax / FInterv;
  RiskLen := Rarc + 6;

  //�������
  Ox :=  Width div 2;
  Oy :=  Height - Footer;
  ArrowLen := Rarc + 3;
  TitlePos:= Point(Ox - Canvas.TextWidth(FTitle) div 2,Oy - Canvas.TextHeight('H') div 2);

//  IntervSize

  ValueAngl := Pi - Angle  - Koeff * FValue;
  ParrowTop      := Point(Ox + Round(ArrowLen*cos(ValueAngl)),Oy - Round(ArrowLen*sin(ValueAngl)));
  ParrowBotRight := Point(Ox + Round(tmpH*cos(ValueAngl-DeltaAngle)),Oy - Round(tmpH*sin(ValueAngl-DeltaAngle)));
  ParrowBotLeft  := Point(Ox + Round(tmpH*cos(ValueAngl+DeltaAngle)),Oy - Round(tmpH*sin(ValueAngl+DeltaAngle)));
*/

  TheImage = new Graphics::TBitmap;
  try
  {
	  TheImage->Height = Height;
	  TheImage->Width = Width;
	  TCanvas * cnv = TheImage->Canvas;
	  {
		  cnv->Font->Size = Border;
		  if(cnv->Font->Size < 8 ) cnv->Font->Size = 8;
		  cnv->Brush->Color = FOColor;
		  cnv->Rectangle(0, 0, Width, Height);
		  cnv->Brush->Color = FIColor;
		  //����
		  cnv->FillRect(Rect(Border,Border,Width - Border,Height - Footer));
		  // �����
		  cnv->Arc(Ox - Rarc, Oy - Rarc, Ox + Rarc, Oy + Rarc,Xs, Ys, Xe, Ye);
		  // ���������� ��������������� ���
			
		  TPoint p = DrawLimitZone(cnv,   clRed, TPoint(Xs,Ys),ParamVisio->Max_RedValue   );   // ������� ������
				 p = DrawLimitZone(cnv,clYellow,             p,ParamVisio->Max_YellowValue);   // ������ ������
//				 p = CalculatePoint(ParamVisio->Min_YellowValue);
				 p = DrawLimitZone(cnv,clLime,               p,ParamVisio->Min_YellowValue);   // ������� ������

				 p = DrawLimitZone(cnv,clYellow,             p,ParamVisio->Min_RedValue   );
				 p = DrawLimitZone(cnv,   clRed,             p,ParamVisio->Min_ScaleValue );

 /*
		  TColor c = cnv->Pen->Color;

		  int  w = cnv->Pen->Width;
		  cnv->Pen->Width = 3;
		  // �������
		  cnv->Pen->Color = clRed;
		  int X_LeftStart = Xs;
		  int Y_LeftStart = Ys;
//		  int TmpAngle = (Pi - Angle *2) / ParamVisio->Max_RedValue;
		  TPoint p = CalculatePoint(ParamVisio->Max_RedValue);
		  int X_LeftEnd = p.x;
		  int Y_LeftEnd = p.y;
		  cnv->Arc(Ox - Rarc-3, Oy - Rarc-3, Ox + Rarc+3, Oy + Rarc+3, X_LeftStart, Y_LeftStart,X_LeftEnd, Y_LeftEnd);
//		  cnv->LineTo(Ox + Round(Rarc * Cos(CurAngle)),Oy - Round(Rarc * Sin(CurAngle)));

//		  cnv->Arc(Ox - Rarc-3, Oy - Rarc-3, Ox + Rarc+3, Oy + Rarc+3,Xs, Ys, Xe, Ye);

		  // ������
		  cnv->Pen->Color = clYellow;
		  X_LeftStart = X_LeftEnd;
		  Y_LeftStart = Y_LeftEnd;
//		  int TmpAngle = (Pi - Angle *2) / ParamVisio->Max_RedValue;
		  p = CalculatePoint(ParamVisio->Max_YellowValue);
		  X_LeftEnd = p.x;
		  Y_LeftEnd = p.y;
		  cnv->Arc(Ox - Rarc-3, Oy - Rarc-3, Ox + Rarc+3, Oy + Rarc+3, X_LeftStart, Y_LeftStart,X_LeftEnd, Y_LeftEnd);
//		  cnv->Arc(Ox - Rarc+3, Oy - Rarc+3, Ox + Rarc-3, Oy + Rarc-3,Xs, Ys, Xe, Ye);


		  cnv->Pen->Color = c;
		  cnv->Pen->Width = w;
*/
		  CurAngle = FullAngl;
		  CurSlice = FSlice;
		  for (i = 0; i< FInterv+1; i++)
		  {
			  X = Ox + Round(RiskLen * Cos(CurAngle));
			  Y = Oy - Round(RiskLen * Sin(CurAngle));
			  cnv->MoveTo(X,Y);
			  cnv->LineTo(Ox + Round(Rarc * Cos(CurAngle)),Oy - Round(Rarc * Sin(CurAngle)));
			  CurAngle = CurAngle - IntervAngl;
			  CurSlice++;
			  if( CurSlice >= FSlice)
			  {
				 CurSlice = 0;
				 S = IntToStr(Round(i * (FMax-FMin) / FInterv));
				 cnv->TextOut(X - cnv->TextWidth(S)/2, Y-cnv->TextHeight("H")-4, S);
			  }
		  }

		  // �������
		  cnv->Brush->Color = Color;
		  cnv->Polygon(Parrow,ParrowMAX-1);
		  //��� �������
		  cnv->Brush->Color = FOColor;
		  cnv->Pie(Ox - tmpH, Oy - tmpH, Ox + tmpH, Oy + tmpH,Ox + tmpH, Oy, Ox - tmpH, Oy);
		  // �����
		  cnv->Font->Color = clRed;
		  cnv->TextOut(TitlePos.x, TitlePos.y, FTitle);
		  cnv->Brush->Color = FIColor;

		  cnv->FillRect(Rect(Ox - tmpH,TitlePos.y+cnv->TextHeight("H")+4,Ox + tmpH,TitlePos.y+cnv->TextHeight("H")*2+8));
//		  int v = FValue*10;
		  AnsiString s;// = IntToStr(v);
		  s.sprintf("%2.1f",FValue);
		  cnv->TextOut(Ox - tmpH + 4, TitlePos.y+cnv->TextHeight("H")+6, s);

	  }
	  Canvas->CopyMode = cmSrcCopy;
	  Canvas->Draw(0, 0, TheImage);
  }
  __finally
  {
	  delete TheImage;
  }
//  if Assigned(FOnPaint) then FOnPaint(Self);

}
//------------------------------------------------------------------------------
// ���������� ��������������� ���
TPoint TScaleHead::DrawLimitZone(TCanvas * cnv,TColor clr, TPoint Pstart,double Value)
{
	TColor c = cnv->Pen->Color;
	int  w = cnv->Pen->Width;
	cnv->Pen->Width = 3;
	cnv->Pen->Color = clr;
	TPoint p = CalculatePoint(Value);
	cnv->Arc(Ox - Rarc-3, Oy - Rarc-3, Ox + Rarc+3, Oy + Rarc+3, Pstart.x, Pstart.y,p.x, p.y);
	cnv->Pen->Color = c;
	cnv->Pen->Width = w;
	return p;
}


//------------------------------------------------------------------------------
void TScaleHead::Calculate(int Value)
{
	OldH = Height;
	OldW = Width;
	if(Width < Height)
		tmp = Width;
	else
		tmp = Height;
	// ������������ ���������� � ����
	Border = tmp / 20;
	if (Border < 2)
		Border = 2;
	Footer = tmp/4;
	if (Footer < 5)
		Footer = 5;
	H = (Height - Border - Footer);  // ������ ����
	W = Width  - Border * 2;         // ������ ����
	//�������������� ��������� �������
	tmpH = H/3;                  // ������ ������������
	tmpL = Width/2 - tmpH;       // ����� ������� �������������

	// �����     sin
	Rarc =  H - H/4;             // ������ 3/4 ����   //    Pi-2a
	Xe = Border*3;                                    //  \      /
	Xs = Width - Xe;                                  //   \    /
	A = (double)(Width / 2 - Xe) / Rarc;  //A=Sin(a)  // a  \  /  a
	Angle = ArcTan (sqrt (1-(A*A)) / A);  //a         // ____\/____
	//  FullAngl = Pi - Angle * 2;
	Koeff = (Pi - Angle * 2)/(FMax-FMin);
	Ye =  H - Round(Rarc * Sin(Angle)) + Border;
	Ys = Ye;
	// �����
	FullAngl = Pi - Angle;

	IntervAngl = (Pi - Angle *2) / FInterv;
	StepValue= (FMax-FMin) / FInterv;
	RiskLen = Rarc + 4;

	//�������
	Ox =  Width/2;
	Oy =  Height - Footer;
	ArrowLen = Rarc + 3;
	TitlePos= Point(Ox - Canvas->TextWidth(FTitle)/2,Oy - Canvas->TextHeight("H"));
 /*
	ValueAngl 		= Pi - Angle  - Koeff * FValue;
	ParrowTop      	= Point(Ox + Round(ArrowLen * Cos(ValueAngl)),Oy - Round(ArrowLen*sin(ValueAngl)));
	ParrowBotRight 	= Point(Ox + Round(tmpH * Cos(ValueAngl-DeltaAngle)),Oy - Round(tmpH * Sin(ValueAngl-DeltaAngle)));
	ParrowBotLeft  	= Point(Ox + Round(tmpH * Cos(ValueAngl+DeltaAngle)),Oy - Round(tmpH * Sin(ValueAngl+DeltaAngle)));
*/
}

//---------------------------------------------------------------------------
void TScaleHead::CalculateValue(double Value )
{
  FValue 		 = Value;
  ValueAngl 	 = Pi - Angle  - Koeff * (FValue-FMin);
  Parrow[ParrowTop]      = Point(Ox + Round(ArrowLen * Cos(ValueAngl)),Oy - Round(ArrowLen * Sin(ValueAngl)));
  Parrow[ParrowBotRight] = Point(Ox + Round(tmpH * Cos(ValueAngl-DeltaAngle)),Oy - Round(tmpH * Sin(ValueAngl-DeltaAngle)));
  Parrow[ParrowBotLeft]  = Point(Ox + Round(tmpH * Cos(ValueAngl+DeltaAngle)),Oy - Round(tmpH * Sin(ValueAngl+DeltaAngle)));
}
//---------------------------------------------------------------------------
TPoint TScaleHead::CalculatePoint(double Value )
{
  double Angl 	 = Pi - Angle  - Koeff * (Value-FMin);
  TPoint P = TPoint(Ox + Round(Rarc * Cos(Angl)),Oy - Round(Rarc * Sin(Angl)));
  return P;	
}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetOColor(TColor Value )
{
	FOColor = Value;
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetIColor(TColor Value )
{
	FIColor = Value;
	Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetAColor(TColor Value )
{
	FAColor = Value;
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetMax(double Value)
{
	FMax    = Value;
    Calculate(0);
    CalculateValue(FValue);
	Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetMin(double Value)
{
	FMin    = Value;
    Calculate(0);
    CalculateValue(FValue);
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetInterv(int Value)
{
	FInterv = Value;
    Calculate(0);
    CalculateValue(FValue);
	Invalidate();

}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetValue(double Value)
{
	if(FValue != Value)
	{
		CalculateValue(Value);
		Invalidate();
	}

}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetTitle(AnsiString Value)
{
	FTitle  = Value;
	TitlePos= Point(Ox - Canvas->TextWidth(FTitle) /2,Oy - Canvas->TextHeight("H")/ 2);
	Invalidate();
}

//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetSlice(int Value)
{
	FSlice = Value;
	Calculate(0);
	CalculateValue(FValue);
	Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TScaleHead::SetLimit(double Lred,double Lyel,double Ryel,double Rred)
{
	ParamVisio->Min_RedValue    = Lred;
	ParamVisio->Min_YellowValue = Lyel;
	ParamVisio->Max_YellowValue = Ryel;
	ParamVisio->Max_RedValue    = Rred;
	Invalidate();
}

//---------------------------------------------------------------------------
namespace Tscalehead_cpp
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TScaleHead)};
		RegisterComponents("Samples", classes, 0);
	}
}
//---------------------------------------------------------------------------
