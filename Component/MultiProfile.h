//---------------------------------------------------------------------------

#ifndef MultiProfileH
#define MultiProfileH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Grapher.h"
#include "MultiGrapher.h"
#include "XGrapher.h"
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "Report.h"

//#include "DbUnit.h"
//#include <stdio.h>
//#include <math.h>
#include "Params.h"
//#include "Config.h"
//#include "FlameMath.h"
//#include "Link.h"

extern HWND  hWndMultiProfile;
//---------------------------------------------------------------------------
class TFormMuliProfile : public TForm
{
__published:	// IDE-managed Components
    TMultiGrapher *MultiGrapher1;
    TPanel *Panel11;
    TBitBtn *BitBtn_CgPrint;
    TStatusBar *StatusBar1;
    void __fastcall BitBtn_CgPrintClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TFormMuliProfile(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMuliProfile *FormMuliProfile;
//---------------------------------------------------------------------------
#endif
