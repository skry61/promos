//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include <winuser.h>

#pragma hdrstop

#include "FlameUnitLite.h"
#include "FlameList.H"
#pragma link "DbUnit_ev"
#include "FMath.h"


#pragma package(smart_init)

extern char * ArrayRegText[];

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


int TFlameUnit::FObjectIdValue = 0;   // �������� �����


//---------------------------------------------------------------------------
static inline void ValidCtrCheck(TFlameUnitLite *)
{
    new TFlameUnitLite(NULL);
}

//---------------------------------------------------------------------------
__fastcall TFlameUnitLite::TFlameUnitLite(TComponent* Owner)
    : TFlameUnit(Owner)
{


    Width = xWight;
    Font->Style = TFontStyles()<< fsBold;
//    FullRepaint = true;
    BorderWidth = 1;
    Type = uOVEN;
    Ac->FontSize = 12;
    Cg->FontSize = 12;
    Cg->FontSize = 12;
    EdsValue->FontSize = 12;
    Cg->Visible = true;
    TpValue->NewName = "�� =";
    TdValue->NewName = "�� =";
    EdsValue->NewName = "��� =";
    int Bot = TypeUnitPanel->Top + TypeUnitPanel->Height;
    ForEach(pSetNormalDisp,&Bot);
    Bvl->Top = 96;
    TBevel*Bvl_ = new TBevel(this);         // ����� �����������
    Bvl_->Parent = this;
    Bvl_->Top = Bot+4;
    Bvl_->Width = Bvl->Width;
    Bvl_->Left= Bvl->Left;
    Bvl_->Height = 2;


    //---------------------------------------

/*
    Height = FUnit_Type == uOVEN? HeightP : HeightE;
    //---------------------------------------


    //������ "����"
    TypeUnitPanel = new TPanel(this);  // ������ ���� (���� / �������������) �����.
    CompPL(TypeUnitPanel,taCenter,false,alTop,this);
    ConstrPL(TypeUnitPanel,CommonX,NamePanelY,CommonWight,NamePanelHeight,Caption);
    TypeUnitPanel->BevelInner = bvLowered;
    TypeUnitPanel->BorderWidth = 2;
    CompPANEL(TypeUnitPanel,12);

    //---------------------------------------
    Bvl = new TBevel(this);         // ����� �����������
    Bvl->Parent = this;
    Bvl->Top = FUnit_Type == uOVEN?BevelYp:BevelYe; Bvl->Left = CommonX;
    Bvl->Width = CommonWight; Bvl->Height = 2;
    //---------------------------------------

    // Label ���  Tp Td
//    ParamList->Add (TpValue = NULL);
    ParamList->Add ( TpValue =  new TTpParamUnit(this,"����������� ����:",L_TpY+Bvl->Top ,L_TpX,Width - L_TpX*2,L_TpH, 14, eTp,this));
    ParamList->Add ( TdValue =  new TTdParamUnit(this,"����������� �������:",L_TdY+Bvl->Top ,L_TdX,Width - L_TdX*2,L_TdH, 14, eTd,this));
    ParamList->Add ( EdsValue = new TEParamUnit (this,"��� �������: ",L_EdsY+Bvl->Top ,L_EdsX,Width - L_EdsX*2,L_EdsH, 14, eEDS,this));
    // ��2
    ParamList->Add (CoValue = new TEParamUnit (this,"CO2 � ����: ",L_EdsY+3+Bvl->Top ,L_EdsX+3,Width - L_EdsX*2,L_EdsH, 14, eEDS,this));
    CoValue->Visible = false;
//    NULL);
    // Label Ac                        -
    ParamList->Add ( Ac = new TAcParamUnit(this,"Ac = ",FUnit_Type == uOVEN?L_2Y:L_1Y ,AcX,CommonWight,L_12H, 23, eAc,this));
    // Label ��
//    if(FUnit_Type == uOVEN){
    ParamList->Add ( Cg = new TCgParamUnit(this,"�� = ",L_2Y ,CgX,CommonWight,L_12H, 23, eCg,this));
    Cg->Visible = false;
//    }else {
//       Cg = NULL;
//       ParamList->Add (NULL);
//    }

    //---------------------------------------
    TPanel *RegimPanel = new TPanel(this);     // ������ ������(����/������)
    CompPL(RegimPanel,taCenter,false,alBottom,this);
    ConstrPL(RegimPanel,CommonX,BotPanelY+Bvl->Top,CommonWight,NamePanelHeight,"");
    RegimPanel->BevelInner = bvLowered;
    RegimPanel->BevelOuter = bvNone;
    RegimPanel->BevelWidth = 1;
    RegimPanel->BorderStyle = bsNone;
    RegimPanel->BorderWidth = 2;
    CompPANEL(RegimPanel,12);

    //---------------------------------------
    TLabel *TmpLabel = new TLabel(this);
    CompL(TmpLabel,taLeftJustify,true,alNone);
    ConstrPL(TmpLabel,8,8,10,19,"�����:");

    TmpLabel = new TLabel(this);
    CompL(TmpLabel,taLeftJustify,true,alNone);
    ConstrPL(TmpLabel,80,8,100,19,"������");

    TImage *Image = new TImage(this);
    TImage *Image1 = new TImage(this);
//    ConstrPI(Image,184,2,32,16,"Close.bmp");
//    ConstrPI(Image1,184,16,32,16,"Open.bmp");
    ConstrPI(Image,184,2,32,16);
    ConstrPI(Image1,184,16,32,16);

//    Image->Picture->Bitmap->Handle = LoadBitmap(NULL,OBM_OLD_CLOSE);
    Image->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16000));
    Image1->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16001));

    Image->Visible = false;
    Image1->Visible = false;
    Image->Parent  = RegimPanel;
    Image1->Parent = RegimPanel;

    Regim = new TRegimUnit(TmpLabel,Image,Image1,rPULT);

    FClap_Pipe = new TClap_Pipe(10,1,0.1,true,
                                FObjectId,            //- �� ���������
                                IsOVEN,    //- ������ "��" ��� "���"
                                Regim->Regim_Value,
                                Regim->More_InMain,
                                Regim->Less_InMain
                                );

    FIs_profile = false;                               // ������� �� �����������

    FCtrlTp       = &vFCtrlTp;
    FCtrlTd       = &vFCtrlTd;
    FCtrlCO       = &vFCtrlCO;
    FCtrlEDS      = &vFCtrlEDS;
    FTpAutoCtrl   = &vFTpAutoCtrl;

    FTdAutoCtrl   = &vFTdAutoCtrl;
    FEDSAutoCtrl  = &vFEDSAutoCtrl;
    FBurnAutoCtrl = &vFBurnAutoCtrl;

    vFCtrlTp         = 0;
    vFCtrlTd         = 0;
    vFCtrlCO         = 0;
    vFCtrlEDS        = 0;
    vFTpAutoCtrl     = 0;
    vFTdAutoCtrl     = 0;
    vFEDSAutoCtrl    = 0;
    vFBurnAutoCtrl   = 0;

    Cg->AutoCtrl = 0;             // ����� ������ ��
    Ac->AutoCtrl = 0;             //              ��
    EdsValue->AutoCtrl = 0;
    TdValue->AutoCtrl = 0;        //
    TpValue->AutoCtrl = 0;


//    Profile = new sProfileParam();
//    Prog_profile = new sProgParam();
    Profile      = NULL;
    Prog_profile = NULL;

    Visible = true;
*/
}
//---------------------------------------------------------------------------
/*
__fastcall TFlameUnitLite:~TFlameUnitLite()
{
    //TODO: Add your source code here
    if (Assigned(FlameList))
      FlameList->DeleteReference(FIndexInFlameList);
    if (Assigned(ParamList)) {
    // ��������� ��� �������� ������� ������  ???
        for (int i = 0; i< ParamList->Count;i++){
           TParamUnit *ParamUnit = (TParamUnit*)ParamList->Items[i];
           if(ParamUnit != NULL)
              delete ParamUnit;
              ParamUnit = NULL;
        }
        ParamList->Clear();
        delete ParamList;
   }
   delete Regim;
   delete FClap_Pipe;
   if(FProfile){
      delete FProfile;
      FProfile = NULL;
   }
   if(Fprog_profile){
      delete Fprog_profile;
      Fprog_profile = NULL;
   }
}
*/
//---------------------------------------------------------------------------
namespace Flameunitlite
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TFlameUnitLite)};
         RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


