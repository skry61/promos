//---------------------------------------------------------------------------

#ifndef RBPanelH
#define RBPanelH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TRBPanel : public TPanel
{
private:
    TLabel*         Label;              //-- ���� �������
    TRadioButton*   RButton[2];         //-- ��� ����� ������
    bool            FChecked;
    AnsiString      FText;
    int             FInterval;
    void __fastcall SetText(AnsiString aText);
    void __fastcall SetInterval(int aInt);
    void __fastcall SetChecked(bool aCheck);
    bool __fastcall GetChecked(void);
    void __fastcall MyResize(TObject *Sender);


protected:
public:
    __fastcall TRBPanel(TComponent* Owner);
    __fastcall ~TRBPanel();
    
    void __fastcall SetOnTag(int Value);

__published:
    __property AnsiString   Text        = {read=FText, write=SetText, default=0};
    __property int          Interval    = {read=FInterval, write=SetInterval, default=24};
    __property bool         Checked     = {read=GetChecked, write=SetChecked, default=0};

};
//---------------------------------------------------------------------------
#endif
