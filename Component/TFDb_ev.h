//---------------------------------------------------------------------------
#ifndef TFDbH_ev
#define TFDbH_ev
#include <system.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "Params.h"


//---------------------------------------------------------------------------

//-- ��������� ��������� �������� ���� ������ -------------------------------
struct TFDbHeader
{
    char    ID[8];                 //-- ���������
    int     RecordCount;           //-- ������ ������ � ������ 4
    int     RecordSize;            //-- ������ ������ � ������ 4
    TDateTime DateLastDB_Updating; //-- ���� ��������� �������� ������
    TDateTime DateLastMagazining;  //-- ���� ���������� ��������� � �����
};

// ��������� ������� ���������� ������� UNITa
struct TParamUNITPresent
{
    int     UnitID;                //-- ������������� �����, ������ ��� � FlameUnit 0 - �� ������������
    int     UnitPosInParamRecord;  //-- ������� ����� � ������ ����������
};


//-- ��������� ��������� �������� ���� ������ -------------------------------
struct TFDbDescriptor
{
    char    *FileName;          //-- ��� ����� ���� ������
    int     RecordSize;         //-- ������ ������ � ������

//    bool    CreateIfEmpty;    //-- ������� �������� ��� ������ �����
    bool    CreateIfNoexist;    //-- ������� �������� ��� �������������� �����
    bool    CreateIfExist;      //-- ������� �������� ��� ������������ �����
    int     CacheRecord;        //-- ���-�� ���������� �������
    char   *ErrOpen;            // ��������� ��� ������ ��������
};


enum TFDbError
{
    erNone = 0,
    erFileNotOpen,
    erReadError,
    erNotEnoughMemory,
    erWriteError,
    erNotCreate,
    erRecCount,
    erTmpNotCreate,
    erBadHeader,

    erMaxError
};

class TFDataBase : public /*TComponent*/ TListView
{
private:	// User declarations
    int hFile,hTmpFile;
    AnsiString DbFileName,DbFileNameCopy,TmpDbFileName;
    int TmpUni;
    int ObjectLength;
    bool IsOpen;

    TFDbHeader      Header;                 //-- ��������� ��
    TFDbHeader      rHeader;                //-- ��������� ����������

    char *Ptr;
    int Length;
    int Error;
    int Cursor;
    int iRealSeek;

    bool __fastcall OpenDbFile(int Mode);     //-- ������� ���� �� � ������
    bool __fastcall CreateTmpFile(void);      //-- ������� ��������� ���� ��
    bool __fastcall DeleteTmpFile(void);      //-- ������� ��������� ����
    bool __fastcall OpenTmpFile(void);        //-- ������� ��������� ���� ��

    bool __fastcall UpdateHeader (void);       //-- �������� ���������

    TDateTime __fastcall GetDateLastDB_Updating();
    void __fastcall SetDateLastDB_Updating(TDateTime aDateTime);
    TDateTime __fastcall GetDateLastMagazining();
    void __fastcall SetDateLastMagazining(TDateTime aDateTime);
    int __fastcall StoryPortionToTmpFile(int Len, TDateTime aDateTime);     // ���� �� ����
    bool __fastcall SearchDate(TDateTime aDateTime);



  //  int __fastcall FileToMem(void);     // ���� � ������
  //  int __fastcall MemToFile(void);     // ���� �� ����

protected:
    // ������� ����������� ����� � ������ ������� ����� ����������
    TParamUNITPresent ParamUNITPresent[MAX_OBJECT];
    int UNITCount;
    virtual bool __fastcall GetUnitCount() {return true;};
    bool __fastcall GetUnitCountOpig();


public:		// User declarations


    TFDbDescriptor *Descriptor;             //-- ��������� ��
    __fastcall TFDataBase(Classes::TComponent* AOwner,TFDbDescriptor *tfdbDescript);
    __fastcall ~TFDataBase();
//    TList *ID_List;                     // ������ ������������ ���������� ()

    bool Eof;                           //-- ������� EOF
    bool Bof;                           //-- ������� BOF

    bool __fastcall Open  (void);        //-- ������� ����
    bool __fastcall Close (void);        //-- ������� ����

    int __fastcall RecCount(void);       //-- ���������� �������

    bool __fastcall First (void);        //-- ������ �� ������ �������
    bool __fastcall FirstQuick (void);   //-- ������ �� ������ ������� � ������� ����
    bool __fastcall Next (void);         //-- ������ �� ��������� �������
    bool __fastcall Last (void);         //-- ������ �� ��������� �������
    bool __fastcall MoveBy(int Dist);    //-- ����������� ������

    int __fastcall DeleteRec (int Index);//-- ������� ������ � ��������
    int __fastcall UpdateDB (void);      //-- �������� ����

    int __fastcall GetCursorPos (void);         //-- �������� ��������� �������
    bool __fastcall SetCursorPos (int Pos);     //-- ���������� ��������� �������
    bool __fastcall SetCursorPosFile (int Pos); //-- ���������� ��������� ������� � ������� � �����


    char* __fastcall GetError (int ErrorCode);  //-- �������� ������

    int __fastcall SetSorting (int SortingType);

    bool __fastcall Add (void *NewRec);      //-- ���������� ������
    bool __fastcall Append (void *NewRec);   //-- ���������� ������ � �����

    bool __fastcall GetRecord(void* RecPtr); //-- ��������� ������ �� �������
    bool __fastcall GetRecordQuick(void* RecPtr); //-- ��������� ������ �� �������

    __property TDateTime DateLastDB_Updating= {read=GetDateLastDB_Updating, write=SetDateLastDB_Updating};
    __property TDateTime DateLastMagazining = {read=GetDateLastMagazining,  write=SetDateLastMagazining};
};


#endif
