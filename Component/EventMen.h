//---------------------------------------------------------------------------

#ifndef EventMenH
#define EventMenH
#include "DbUnit_ev.h"

//-- �������������� ���������� �������/���������� -----------------------------
enum eParamId
{
    eTp = 0,            //-- ����������� ����
    eTd,                //-- ����������� �������
    eEDS,               //-- ��� �������
    eCO2,               //-- �������� ����.-� ��2
    eAc,                //-- ���������� ��������
    eCg                 //-- ���������� ���������
};

//-- �������������� ������� � ��� -------------------------------------------
enum eEventId
{
    eEmptyEvent = 0,
    eRunArm,
    eStopArm,
    eExcaptionOnWork,
    eAccessChange,
    eAlarmPSW,
    eLostConnect,
    eSetInterval,
    eSetCO,
    eSetTp,
    eCalcError,

    eTpHighLimit,
    eTpLowLimit,
    eTdHighLimit,
    eTdLowLimit,
    eEHighLimit,
    eELowLimit,

    eAc2HighLimit,
    eAcHighLimit,
    eAcLowLimit,

    eCgHighLimit,
    eCgLowLimit,

    eStartProfile,
    eStopProfile,
    eExtrStopProfile,       //-- ��������� ���������� ������������ ����������� �������
    eSetTd,
    eSetEDS,

    eTpNormal,
    eTdNormal,
    eENormal,
    eAcNormal,
    eCgNormal,
    //-- �������� eSetEDSAdjusting �������
    eSetEDSAdjusting,       // ������ ��� ��� �������������
    eSetCg,
    eSetCgAdjusting,         // ������ �� ��� ������������� (��� �����)

    eBurnSensor,            //-- ������ ������� ����� ������ �������

    eLostConnectALT,        //-- ������ ����� � ��� �����������(��������)
    eErrALT,                //-- ��� ����������(��������)������������� �� ������

    eSetTemp,               //-- ������ �������� ����� ��� ��������� ����

    eMaxEventId
};





//-- ��������� ������� ------------------------------------------------------
struct sArmEvent
{
    TDateTime   Time;       //-- ����/����� ��������� �������
    int         Id;         //-- ������������� �������
    int         Source;     //-- �������� �������������
    char        Param[32];  //-- �������� ������
    __fastcall  sArmEvent(TDateTime aTime,int aId,int aSource, char  *aParam)
    {
      Time     =  aTime;
      Id       =  aId;
      Source   =  aSource;
      if (aParam) 
         strncpy(Param, aParam,31);
      else Param[0] = 0;
    }
    __fastcall  sArmEvent()
    {
         memset(Param, 0,32);
    }
};

class PACKAGE TEventMen : public TxDataBase
{
    sArmEvent * pEvent;

public:		// User declarations
    __fastcall TEventMen(Classes::TComponent* AOwner);
    __fastcall virtual ~TEventMen();

    //-- ��������� ������ � ����� ������-----------------------------------------
    void *__fastcall CreateNew(void);
    int  __fastcall  GetStructSize(void); //-- ���������� ������ ���������
    void __fastcall  ClearAll(void);      //-- ��������� �������� ������������� ������, ���� � ����������
    virtual void __fastcall  AddToListComp(void); //-- ��������� ���������� ����������� ���������� ������
    void __fastcall  ClearList(void);
    bool __fastcall  Store (void);
    void __fastcall  LoadAll(TDateTime Time);       //-- ��������� �������� ������������� ������, ���� � ���������� �� �����
    
    bool __fastcall FillListComp(void);
    bool __fastcall QuickFillListComp(int * TypeArray, int MaxIndex, TDateTime StartTime);

//    void __fastcall AddEventList(sArmEvent *event);
//    bool __fastcall RecovertTable (void);
    bool __fastcall  Generate(int EventId,int Source,char* Comment);
    // ���������� �������� �����������
    void __fastcall SetInterval(int intervalIntex);
//    bool __fastcall  InitTable (void);
//    bool __fastcall  OpenTable (void);
//    bool __fastcall  CloseTable (void);

    
private:
//    void __fastcall SetEventListView(TListView * value);
//    TListView * __fastcall GetEventListView();
__published:
//    __property TListView * EventListView  = { read=GetEventListView, write=SetEventListView };
    __property VisibleInterval;
//      = { read=FVisibleInt, write=FVisibleInt };
};


//char* __fastcall DoubleToComment(double Value);

//---------------------------------------------------------------------------
//-- ��������� ������ � ��������� -------------------------------------------
//---------------------------------------------------------------------------

//-- ������������ ������� ---------------------------------------------------
bool __fastcall GenerateEvent(int EventId,int Source,char* Comment);

//-- ��������� ������� � ���� ������ ----------------------------------------
bool __fastcall StoreEvent(sArmEvent &event);

//-- ��������� �������� ������������� ������ ������� �� ��
bool __fastcall LoadEventList(TList* pList, TDateTime StartTime);

bool __fastcall FindEventRecord (TDateTime Time);



extern PACKAGE TEventMen *EventMenagerGlobal;
//---------------------------------------------------------------------------
#endif
