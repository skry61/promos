//---------------------------------------------------------------------------
#ifndef FMathH
#define FMathH
//---------------------------------------------------------------------------

//#include "Profile.h"
//#include "Params.h"

//-- ������� �������� ������� � ������� ��������
double CelcToKelvin_l(double C);

//-- ���������� ����������� ���������� �� �� �������� ����������
//-- �� - ���������� ��������
//-- Tp - ����������� ���� (�)

double Cg_func_l(double Ac, double Tp);

//-- ���������� ���������� �������� �� �� �������� ����������
//-- CO - �������� ������������ �� (%)
//-- Tp - ����������� ���� (�)
//-- E  - ������ � �������, ��
//-- Td - ����������� ������� (�)

double Ac_func_l(double CO, double Tp, double E, double Td);


//int _matherr(struct _exception *e);

//-- ��������� ���������� ������� �����
bool order (double value, int &order);

//-- ��������� ���������� �������������
//bool PrepareKoeff (sProfileParam* profile);

//-- ������� erfc
//double erfc (double X, double dX);
//double erfc (double X, double dX, bool real_dx);

//-- ������� ����������� ������� �(x,t)
double C_func_l(double Xi, int i, double C0,double Cf, double dX);

#endif
