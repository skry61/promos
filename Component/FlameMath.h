//---------------------------------------------------------------------------
#ifndef FlameMathH
#define FlameMathH
//---------------------------------------------------------------------------

//#include "Params.h"
//#include "Profile.h"
#include "ParamMen.h"


//-- ������� �������� ������� � ������� ��������
double CelcToKelvin (double C);

//-- ���������� ����������� ���������� �� �� �������� ����������
//-- �� - ���������� ��������
//-- Tp - ����������� ���� (�)

double Cg_func (double Ac, double Tp);

//-- ���������� ���������� �������� �� �� �������� ����������
//-- CO - �������� ������������ �� (%)
//-- Tp - ����������� ���� (�)
//-- E  - ������ � �������, ��
//-- Td - ����������� ������� (�)

double Ac_func (double CO, double Tp, double E, double Td);

//-- ������ �������������� ��������� ���������� ��� ������ �������

//bool MathHandle (sArmParam* param, bool IsCalcCg,bool IsCalcCgOld);

int _matherr(struct _exception *e);

//-- ��������� ���������� ������� �����
bool order (double value, int &order);

//-- ��������� ���������� �������������
//bool PrepareKoeff (sProfileParam* profile);

//-- ������� erfc
//double erfc (double X, double dX);
//double erfc (double X, double dX, bool real_dx);

//-- ������� ����������� ������� �(x,t)
double C_func (double Xi, int i, double C0,double Cf, double dX);

#endif
