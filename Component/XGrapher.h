//---------------------------------------------------------------------------
#ifndef XGrapherH
#define XGrapherH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include "Grapher.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TXGrapher : public TGrapher
{
private:
    int FLimitLineWidth;

    void __fastcall SetVDivisor (int iDivisor);
    void __fastcall SetHDivisor (int iDivisor);
    void __fastcall SetEndV (int iEnd);
    void __fastcall SetEndH (int iEnd);
    void __fastcall SetLimitLow (int iLow);
    void __fastcall SetF_Value (int Value);
    void __fastcall SetW_Value (int Value);
    void __fastcall SetLimitLineWidth(int value);

protected:
    int             FVDivisor;
    int             FHDivisor;
    int             FEndV;
    int             FEndH;

    TColor          FLimitColor;         //-- ���� ����� �0 �������
    int             FLimitLow;
    int             FF_Value;
    void __fastcall DrawLabel(TCanvas *cnv, TRect GraphRect1);
//    void __fastcall DrawMarker(TCanvas *cnv);
    int __fastcall YFunconGrapher (int y_real);
public:
    AnsiString  aXDimension;
    AnsiString  aYDimension;
    bool        IsProfile;

    __fastcall TXGrapher(TComponent* Owner);
    void __fastcall SetMarkerX (int X)
    {
        FH_Marker = X + FHMin;
    }
    void __fastcall SetMarkerY (int Y)
    {
        MarkerY = Y;
    }

//    void __fastcall DrawFunctionData(TColor tColor,int* FuncData, int Idx, int Count);
//    void __fastcall TXGrapher::DrawFunctionData(TCanvas *cnv);
//    void __fastcall TXGrapher::DrawFunctionData(TGraphicControl *GC);
   // void __fastcall TXGrapher::DrawFunctionData(TImage *GC);

    void __fastcall DrawStatMarker(void)
    {
        DrawMarker((AlterPaint)?AlterCanvas:Canvas);
    }
    void __fastcall PrepareMarker();       //���������� ������� (�����������)

    //-- ��������� ��������� � � ��������� �������� �������
    void __fastcall DrawLimitsLabels(TCanvas *cnv);//void);
    //-- ��������� ��������� � � ��������� �������� �������
    void __fastcall DrawLimitsLines(TCanvas *cnv);//void);
    virtual void __fastcall /*TXGrapher::*/DrawTime(TCanvas *cnv);//void);
    
    virtual void __fastcall Paint(void);


__published:

    __property int VDivisor = {read=FVDivisor, write=SetVDivisor, stored = true};
    __property int HDivisor = {read=FHDivisor, write=SetHDivisor, stored = true};

    __property int EndV = {read=FEndV, write=SetEndV, stored = true};
    __property int EndH = {read=FEndH, write=SetEndH, stored = true};

    __property int W_Value = {read=FW_Value, write=SetW_Value, stored = true};
    __property int F_Value = {read=FF_Value, write=SetF_Value, stored = true};

    __property int LimitLow = {read=FLimitLow, write=SetLimitLow, stored = true};
    __property TColor LimitColor = {read=FLimitColor, write=FLimitColor, default=clBlue};
    __property int LimitLineWidth  = { read=FLimitLineWidth, write=SetLimitLineWidth };

//    __property TColor MarkColor = {read=FMarkColor, write=FMarkColor, default=clAqua};
//    __property TColor MarkerColor = {read=FMarkerColor, write=FMarkerColor, default=clWhite};
//    __property bool Marker = {read=FMarker, write=FMarker, stored = true};
//    __property TDateTime  MarkerTime = {read=FMarkerTime, write=FMarkerTime};
};
//---------------------------------------------------------------------------
#endif
