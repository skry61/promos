//---------------------------------------------------------------------------
#ifndef TimeGrapherH
#define TimeGrapherH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include "Grapher.h"
#include <ExtCtrls.hpp>
//#include "FlameList.h"

//---------------------------------------------------------------------------
class PACKAGE TTimeGrapher : public TGrapher
{
private:
    Classes::TNotifyEvent FOnMarker;
    AnsiString      FTimeFormat;
    TDateTime       FTimeStep;       // ��� ����� �� �������
    TDateTime       FTimeStart;      // ����� ������ �����

    int             FDivisor;
    double          FAdjusting_Value;
    bool            FEnable_Adjusting;
    TColor          FAdjustColor;

    void __fastcall SetDivisor (int iDivisor);


    void __fastcall SetTimeFormat (AnsiString aFormat);
    void __fastcall SetTimeStep (TDateTime aStep);
    void __fastcall SetTimeStart (TDateTime aStart);

    void __fastcall DrawLabel(TCanvas *cnv, TRect GraphRect1);

    void __fastcall SetAdjusting_Value(double value);
    void __fastcall SetEnable_Adjusting(bool value)
    {
         FEnable_Adjusting = value;
    }
    bool __fastcall GetEnable_Adjusting()
    {
         return FEnable_Adjusting;
    }

    void __fastcall DrawOneFunction(TCanvas *cnv,int Start,int Count,TColor tColor, int* FuncData, int Idx);
    
	  TDateTime __fastcall  GetVisioTimeStart();
	  TDateTime __fastcall  GetVisioTimeEnd();
    
protected:
    virtual void __fastcall CalcMarkerParam (){
                FMarkerTime = FTimeStart + TDateTime((double)MarkerPoint*(double)FTimeStep);
            }

public:
    __fastcall TTimeGrapher(TComponent* Owner);

    void __fastcall DrawMoreFunctionData(TColor tColor, int* FuncData,  int Idx,  int Count,
                                         TColor tColor2,int* FuncData2, int Idx2, int Count2,int StartIdx2);

    void __fastcall DrawFunctionData(TColor tColor,int* FuncData, int Idx, int Count);
    int __fastcall YFunconGrapher (int y_real);

    // ������������� ����� ������ ����
    void __fastcall SetStartArmTime(TDateTime  aTimeStart);

__published:


    __property int Divisor = {read=FDivisor, write=SetDivisor, stored = true};

	__property AnsiString TimeFormat = {read=FTimeFormat, write=SetTimeFormat};
	__property TDateTime  TimeStep = {read=FTimeStep, write=SetTimeStep};
	__property TDateTime  TimeStart = {read=FTimeStart, write=SetTimeStart};
	__property TDateTime  VisioTimeStart = {read=GetVisioTimeStart};
	__property TDateTime  VisioTimeEnd = {read=GetVisioTimeEnd};


    __property double Adjusting_Value  = { read=FAdjusting_Value, write=SetAdjusting_Value, stored = true};
    __property bool Enable_Adjusting  = { read=GetEnable_Adjusting, write=SetEnable_Adjusting, stored = true, default = true };
    __property TColor AdjustColor  = { read=FAdjustColor, write=FAdjustColor, stored = true, default=clBlue};

};
//---------------------------------------------------------------------------

#endif

