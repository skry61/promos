unit Pipe;

interface

uses SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, StdCtrls;

type

  TPipeKind = (gkHorizontalBar, gkVerticalBar);
  TPipeDirect= (Forwart, Back);

  TPipe = class(TGraphicControl)
  private
    FQ : Longint;
    FSteps: Longint;
    FNStep: Longint;
    FKind: TpipeKind;
    FBorderStyle: TBorderStyle;
    FBackColor: TColor;
    FForeColor: TColor;
    FDirect: TpipeDirect;
    procedure PaintBackground(AnImage: TBitmap);
    procedure PaintAsBar(AnImage: TBitmap; PaintRect: TRect);
    procedure SetPipeKind(Value: TPipeKind);
    procedure SetPipeDirect(Value: TPipeDirect);
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetBackColor(Value: TColor);
    procedure SetForeColor(Value: TColor);
    procedure SetFSteps(Value: Longint);
    procedure SetFQ(Value: Longint);
    procedure SetNStep(Value: Longint);
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure MakeStep;
  published
    property Enabled;
    property Kind: TPipeKind read FKind write SetPipeKind default gkHorizontalBar;
    property Direct: TPipeDirect read FDirect write SetPipeDirect;{ default Back;}
    property Font;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property BackColor: TColor read FBackColor write SetBackColor default clWhite;
    property ForeColor: TColor read FForeColor write SetForeColor default clBlack;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property Visible;
    property Steps: Longint read FSteps write SetFSteps default 4;
    property Q: Longint read FQ write SetFQ default 2;
    property NStep: Longint read FNStep write SetNStep default 5;
  end;


procedure Register;

implementation

uses  Consts;

type
  TBltBitmap = class(TBitmap)
    procedure MakeLike(ATemplate: TBitmap);
  end;

procedure Register;
begin
  RegisterComponents('Fx', [TPipe]);
end;



procedure TBltBitmap.MakeLike(ATemplate: TBitmap);
begin
  Width := ATemplate.Width;
  Height := ATemplate.Height;
  Canvas.Brush.Color := clWindowFrame;
  Canvas.Brush.Style := bsSolid;
  Canvas.FillRect(Rect(0, 0, Width, Height));
end;


constructor TPipe.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle + [csFramed, csOpaque];
  FKind := gkHorizontalBar;
  FDirect := Back;
  FBorderStyle := bsSingle;
  FBackColor := clWhite;
  FSteps := 4;
  FQ := 2;
  FNStep := 0;
  Width := 100;
  Height := 100;
end;


procedure TPipe.Paint;
var
  TheImage: TBitmap;
  PaintRect: TRect;
   OverlayImage: TBltBitmap;
begin
  with Canvas do
  begin
    TheImage := TBitmap.Create;
    try
      TheImage.Height := Height;
      TheImage.Width := Width;
      PaintBackground(TheImage);     {������� ������ ��� TheImage}
      PaintRect := ClientRect;       {��� ��������� �������}
      {���������� ���������}
      if FBorderStyle = bsSingle then InflateRect(PaintRect, -1, -1);
      OverlayImage := TBltBitmap.Create;
      try
        OverlayImage.MakeLike(TheImage);
        PaintBackground(OverlayImage); {������� ������ ��� OverlayImage}
        PaintAsBar(OverlayImage,PaintRect);
        TheImage.Canvas.CopyMode := cmSrcInvert;
        TheImage.Canvas.Draw(0, 0, OverlayImage);
        TheImage.Canvas.CopyMode := cmSrcCopy;
      finally
        OverlayImage.Free;
      end;
      Canvas.CopyMode := cmSrcCopy;
      Canvas.Draw(0, 0, TheImage);
    finally
      TheImage.Destroy;
    end;
  end;
end;

procedure TPipe.PaintBackground(AnImage: TBitmap);
var
  ARect: TRect;
begin
  with AnImage.Canvas do
  begin
    CopyMode := cmBlackness;
    ARect := Rect(0, 0, Width, Height);
    CopyRect(ARect, Animage.Canvas, ARect);
    CopyMode := cmSrcCopy;
  end;
end;


procedure TPipe.PaintAsBar(AnImage: TBitmap; PaintRect: TRect);
var
  FillSize: Longint;
  W, H, X, step: Integer;
begin
  W := PaintRect.Right - PaintRect.Left + 1;
  H := PaintRect.Bottom - PaintRect.Top + 1;
  with AnImage.Canvas do
  begin
    Brush.Color := BackColor;
    FillRect(PaintRect);
    Pen.Color := ForeColor;
    Pen.Width := 1;
    Brush.Color := ForeColor;
    case FKind of
gkHorizontalBar:  begin
        FillSize := Height*2;
        if FillSize >= W then begin
           FillSize := W div 2;
           FQ := 2;
        end;
        X := FillSize*FQ;
        step := NStep*round(X/FSteps);
        case FDirect of
   Forwart: begin
            if step > X-FillSize then
              FillRect(Rect(PaintRect.Left,PaintRect.Top,step-(X-FillSize),H));
            FillRect(Rect(PaintRect.Left+step, PaintRect.Top,FillSize+step,H));
            if X < W then begin
               while X < W do begin
                 if X+FillSize <= W then begin
                    FillRect(Rect(PaintRect.Left+X+step, PaintRect.Top,
                                                   X+FillSize+step, H));
                    X := X + FillSize*FQ div 2;
                 end  {if}
                 else
                    FillRect(Rect(PaintRect.Left+ X+step,PaintRect.Top,W, H));
                    X := X + FillSize*FQ div 2;
               end;  {while}
             end;   {if}
           end;
   Back: begin
            if abs(step) >= X-FillSize then
                 FillRect(Rect(W,PaintRect.Top,W-abs(step)+(X-FillSize),H));
            FillRect(Rect(W-abs(step), PaintRect.Top,W-FillSize-abs(step), H));
            if X < W then begin
               while X < W do begin
                 if X+FillSize <= W then begin
                    FillRect(Rect(W-X-abs(step), PaintRect.Top,W-X-FillSize-abs(step), H));
                    X := X + FillSize*FQ div 2;
                 end  {if}
                 else
                    FillRect(Rect(W-X-abs(step),PaintRect.Top,PaintRect.Left, H));
                    X := X + FillSize*FQ div 2;
               end;   {while}
             end;   {if}
           end;   {Back}
         end;   {Case}
       end;   {gkHorizontalBar}
gkVerticalBar:
        begin
          FillSize := Width*2;
          if FillSize >= H then
            begin
              FillSize := H div 2;
              FQ := 2;
            end;
          X := FillSize*FQ;
          step := NStep*round(X/FSteps);
          case FDirect of
   Forwart: begin
            if step >= X-FillSize then
              FillRect(Rect(PaintRect.Left, H, W, H-(step-X+FillSize)));
            FillRect(Rect(PaintRect.Left, H - FillSize-step, W, H-step));
            if X < H then begin
              while X < H do begin
                if X+FillSize <= H then begin
                  FillRect(Rect(PaintRect.Left, H-X-FillSize-step,W,H-X-step));
                  X := X + FillSize*FQ div 2;
                end
                else
                  FillRect(Rect(PaintRect.Left, H-X-step,W, PaintRect.Top));
                  X := X + FillSize*FQ div 2;
             end;
           end;
          end;
   Back: begin
            if abs(step) >= X-FillSize then
                 FillRect(Rect(PaintRect.Left,0,W,abs(step)-(X-FillSize)));
            FillRect(Rect(PaintRect.Left,abs(step),W,FillSize+abs(step)));
            if X < H then begin
               while X < H do begin
                 if X+FillSize <= H then begin
                    FillRect(Rect(PaintRect.Left,X+abs(step),W,X+FillSize+abs(step)));
                    X := X + FillSize*FQ div 2;
                 end  {if}
                 else
                    FillRect(Rect(PaintRect.Left,X+abs(step),W,H));
                    X := X + FillSize*FQ div 2;
               end;   {while}
            end;    {if}
          end;    {Back}
        end;    {Case}
      end;    {gkVerticalBar}
    end;    {Case}
  end;    {With}
end;   {procedure}


procedure TPipe.SetPipeKind(Value: TPipeKind);
begin
  if Value <> FKind then
  begin
    FKind := Value;
    Refresh;
  end;
end;

procedure TPipe.SetPipeDirect(Value: TPipeDirect);
begin
  if Value <> FDirect then
  begin
    FDirect := Value;
    Refresh;
  end;
end;


procedure TPipe.SetBorderStyle(Value: TBorderStyle);
begin
  if Value <> FBorderStyle then
  begin
    FBorderStyle := Value;
    Refresh;
  end;
end;


procedure TPipe.SetBackColor(Value: TColor);
begin
  if Value <> FBackColor then
  begin
    FBackColor := Value;
    Refresh;
  end;
end;

procedure TPipe.SetForeColor(Value: TColor);
begin
  if Value <> FForeColor then
  begin
    FForeColor := Value;
    Refresh;
  end;
end;


procedure TPipe.SetFQ(Value: Longint);
begin
  if Value <> FQ then
  begin
    if Value >=2 then  FQ := Value;
    Refresh;
  end;
end;


procedure TPipe.SetFSteps(Value: Longint);
begin
  if Value <> FSteps then
  begin
    if (Value >=2) and (Value < Height*2*FQ) then  FSteps := Value;
    Refresh;
  end;
end;


procedure TPipe.SetNStep(Value: Longint);
begin
  if Value <> FNStep then
  begin
    FNStep := Value mod FSteps;
    Refresh;
  end;
end;

procedure TPipe.MakeStep;
begin
    NStep := NStep + 1;
end;


end.
