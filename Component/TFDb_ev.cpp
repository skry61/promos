//---------------------------------------------------------------------------
#include <vcl.h>
#include <dir.h>
#pragma hdrstop

#include "TFDb_ev.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#define min(a, b)  (((a) < (b)) ? (a) : (b))  

char* cTFDbError[] =
{
    "��� ������",
    "���������� ������� ����",
    "������ ������ �����",
    "������ ��������� ������",
    "������ ������ �����",
    "������ �������� ����� ��",
    "�������������� ���-�� ������� ����� ����� ��",
    "������ �������� ���������� �����",
    "������������ ��������� ����� ��",

    "����������� ������ ���� ������"
};

//---------------------------------------------------------------------------
__fastcall TFDataBase::TFDataBase(Classes::TComponent* AOwner,TFDbDescriptor *tfdbDescript):TListView(AOwner)
//TComponent(AOwner)
{
    char NewFN[256];
    AnsiString s = GetCurrentDir()+(AnsiString)"\\";
    Descriptor = tfdbDescript;
    DbFileName = s + tfdbDescript->FileName;
    fnsplit(DbFileName.c_str(), 0, 0, NewFN, 0);
    TmpDbFileName = s+NewFN ;
    TmpDbFileName += ".bak";
    DbFileNameCopy = s+NewFN;
    DbFileNameCopy += ".cpy";

    ObjectLength = tfdbDescript->RecordSize;
    Header.RecordSize = tfdbDescript->RecordSize;
    strcpy(Header.ID, "TFDB FL");       //-- ���������� ��������������
    strcpy(rHeader.ID,"TFDB FL");       //-- ���������� ��������������
    Error = erNone;
    Length = 0;
    Ptr = NULL;
    Cursor = 0;
    TmpUni = rand()%0xFFFF;
    IsOpen = false;
    Eof = Bof = false;
}

//------------------------------------------------------------------------------
__fastcall TFDataBase::~TFDataBase()
{
    if (Ptr)
    {
       //	MemToFile();
	   delete Ptr;
//	   delete[]Ptr;
	}
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::OpenDbFile(int Mode)     // ���� � ������
{
    //-- �������� ����
    hFile = FileOpen (DbFileName,Mode);
    if (hFile == -1)
    {
        //-- ���� ���������� ���� �������� ��� ���������� ����� - �������
        if (Descriptor->CreateIfNoexist)
        {
            hFile = FileCreate (DbFileName);
            if (hFile == -1)
            {
                Error = erNotCreate;
                return false;
            }
            Length = 0;
            Error = erNone;
            Header.RecordCount = 0;
            Header.DateLastDB_Updating = TDateTime::CurrentDateTime();
            Header.DateLastMagazining  = TDateTime::CurrentDateTime();

            return UpdateHeader();
            //return true;
        }

        Error = erFileNotOpen;
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::OpenTmpFile(void)
{
/*
    AnsiString TmpName = DbFileName + IntToStr(TmpUni);
    hTmpFile = FileOpen (TmpName,Mode);
    if (hTmpFile == -1)
    {
        //-- ���� ���������� ���� �������� ��� ���������� ����� - �������
        if (Descriptor->CreateIfNoexist)
        {
            hTmpFile = FileCreate (DbFileName);
            if (hTmpFile == -1)
            {
                Error = erTmpNotCreate;
                return false;
            }
            Error = erNone;
            return true;
        }

        Error = erFileNotOpen;
        return false;
    }
    */
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::CreateTmpFile(void)
{
    AnsiString TmpName = DbFileName + IntToStr(TmpUni);
    DeleteTmpFile();
    return RenameFile(DbFileName,TmpName);
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::DeleteTmpFile(void)
{
    AnsiString TmpName = DbFileName + IntToStr(TmpUni);
    return DeleteFile(TmpName);
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Open  (void)
{
    if (!OpenDbFile(fmOpenReadWrite)) return false;

    //-- ������ ���������
        FileSeek(hFile,0,0);
        int iReaded = FileRead(hFile,&rHeader,sizeof(TFDbHeader));
        if (iReaded != sizeof(TFDbHeader))
        {
            Error = erReadError;
            FileClose(hFile);
            return false;
        }
        if (strcmp(rHeader.ID,Header.ID) ||
            rHeader.RecordSize != Header.RecordSize)
        {
            Error = erBadHeader;
            FileClose(hFile);
            return false;
        }
        Header.RecordCount = rHeader.RecordCount;
        Header.DateLastDB_Updating = rHeader.DateLastDB_Updating;
        Header.DateLastMagazining = rHeader.DateLastMagazining;

//-------------------------------
    int iFileLength = FileSeek(hFile,0,2) - sizeof(TFDbHeader);
    Length = iFileLength/ObjectLength;      //-- ��������� ���-�� �������
    if ( (Length*ObjectLength)!=iFileLength)
    {
        FileClose(hFile);
        Error = erRecCount;
        return false;
    }

    if ( Header.RecordCount != Length)
    {
        FileClose(hFile);
        Error = erRecCount;
        return false;
    }
    // ���������� ������, ������������ �� ����� ����������.
    if (!GetUnitCount())
         return false;
    Cursor = 0;
    Bof = true;
    Eof = false;
    FileClose(hFile);
    IsOpen = true;
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Close (void)
{
    IsOpen = false;
    Bof = false;
    Eof = false;
    return true;
}

bool __fastcall TFDataBase::GetUnitCountOpig()
{
    // ���������� ������, ������������ �� ����� ����������.
    UNITCount = 0;

    if(Length != 0)                 //-- ���� ���� ���� ���� ������
    {
        // ������� ���� ������ ��� ��������� ���������� ������ � �� ID
        sOptimalParam   optim_param;        //-- ���������������� ���������
        // ������������� �� ������ ������ (����� ����� ���������)
        FileSeek(hFile,(int )sizeof(TFDbHeader),0);

        int iReaded = FileRead(hFile,&optim_param,sizeof(sOptimalParam));
        if (iReaded != sizeof(sOptimalParam) || iReaded != ObjectLength)
        {
            Error = erReadError;
            FileClose(hFile);
            return false;
        }
        // �������� ������� ����������� ����� � ������ ������� ����� ����������

        for (int i = 0; i < MAX_OBJECT; i++)
        {
           // ���� ����� �������������� �� 0 - ������ ������ ������������
           if(optim_param.Params[i].ObjectId)
           {
               ParamUNITPresent[UNITCount].UnitID = optim_param.Params[i].ObjectId;
               ParamUNITPresent[UNITCount].UnitPosInParamRecord = i;
               ++UNITCount;
           }
        }
    }
    return true;
}



/*
int __fastcall TFDataBase::FileToMem(void)     // ���� � ������
{
    int Readed;
    if (Ptr)        //-- ���� ���� � ������ - ������
    {
        delete []Ptr;
        Ptr = NULL;
    }
    hFile = FileOpen (DbFileName,fmOpenRead);
    if (hFile == -1)
    {
        Length = 0;
        ObjectLength = 0;
        Error = erFileNotOpen;
        return -1;
    }
    int FileRec;
    int FileObjectLength;

    Readed = FileRead(hFile,&FileRec,sizeof(int));
    if (Readed != sizeof (int))
    {
        FileClose(hFile);
        Error = erReadError;
        return -1;
    }
    Readed = FileRead(hFile,&FileObjectLength,sizeof(int));
    if (Readed != sizeof (int))
    {
        FileClose(hFile);
        Error = erReadError;
        return -1;
    }

    Ptr = new char [FileRec * ObjectLength + 1];
    if (!Ptr)
    {
        FileClose(hFile);
        Error = erNotEnoughMemory;
        return -1;
    }
    for (int i = 0; i < FileRec; i++)
    {
        Readed = FileRead(hFile,(Ptr + ObjectLength*i),FileObjectLength);
        if (Readed != FileObjectLength)
        {
            FileClose(hFile);
            Length = i+1;
            Error = erReadError;
            return -1;
        }
    }
    Length = FileRec;
    Error = erNone;     //-- ��� ������
    FileClose(hFile);
    return 0;
}
int __fastcall TFDataBase::MemToFile(void)     // ���� �� ����
{
    int Writed;

    hFile = FileCreate (DbFileName);
    if (hFile == -1)
    {
        Error = erFileNotOpen;
        return -1;
    }
    if (!Ptr)
    {
        Length = 0;
        ObjectLength = 0;
    }

    Writed = FileWrite(hFile,&Length,sizeof(int));
    if (Writed != sizeof (int))
    {
        FileClose(hFile);
        Error = erWriteError;
        return -1;
    }
    Writed = FileWrite(hFile,&ObjectLength,sizeof(int));
    if (Writed != sizeof (int))
    {
        FileClose(hFile);
        Error = erWriteError;
        return -1;
    }

    if (Ptr)
    {
        Writed = FileWrite(hFile,Ptr,ObjectLength*Length);  // ��������� ����
        if (Writed != ObjectLength*Length)
        {
            FileClose(hFile);
            Error = erWriteError;
            return -1;
        }
    }
    Error = erNone;     //-- ��� ������
    FileClose(hFile);
    return 0;
}
*/

//------------------------------------------------------------------------------
int __fastcall TFDataBase::RecCount(void)     // ���������� �������
{
//    if (!ObjectLength) return 0;
    return Length;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::First (void)        //-- ������ �� ������ �������
{
    Cursor = 0;
    Bof = true;
    Eof = false;
	return true;
}

//------------------------------------------------------------------------------
int __fastcall TFDataBase::GetCursorPos (void)     //-- �������� ��������� �������
{
    return Cursor;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::SetCursorPos (int Pos)     //-- ���������� ��������� �������
{
    Cursor = Pos;
    Eof = false;
    Bof = false;

    if (Cursor < 0)
	{
    	Cursor=0;
        if (!Cursor) Bof = true;
    	return true;
	}
    if (Cursor > (Length-1))
    {
        Cursor = Length-1;
        Eof = true;
    }
	return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::SetCursorPosFile (int Pos)     //-- ���������� ��������� ������� � ������� � �����
{
   SetCursorPos (Pos);
   // ������������� � �����
   int iFileSeek = Cursor*ObjectLength + sizeof(TFDbHeader);
   iRealSeek = FileSeek(hFile,iFileSeek,0);
   if (iRealSeek != iFileSeek)
   {
       Error = erRecCount;
       FileClose(hFile);
       return false;
   }
   return true;

}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Next (void)         //-- ������ �� ��������� �������
{
    if (Cursor < (Length-1))
	{
    	Cursor ++;
        if (Cursor) Bof = false;
        Eof = false;
    	return true;
	}
    else
    {
        Bof = false;
        Eof = true;
        Cursor = Length-1;
    }
	return false;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Last (void)         //-- ������ �� ��������� �������
{
    Bof = false;
    Eof = true;
    Cursor = Length-1;
	return false;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::MoveBy(int Dist)    //-- ����������� ������
{
    Bof = false;
    Eof = false;
    Cursor += Dist;
    //-- ������� �����������
    if (Cursor > (Length-1))
    {
        Eof = true;
        Cursor = Length-1;
    }
    //-- ������ �����������
    if (Cursor < 0)
    {
        Bof = true;
        Cursor = 0;
    }
    return true;
}

//------------------------------------------------------------------------------
int __fastcall TFDataBase::DeleteRec (int Index)  //-- ������� ������ � ��������
{
// ��� ������ �  Ptr = new char[ReadRec*ObjectLength + 1]; (�� ������������ � �� ���������)
    char NewRec[64];
    Cursor = Index+1;

    //-- ������� ������� ��������� ����
    if (!CreateTmpFile()) return false;

    if (!OpenDbFile(fmOpenReadWrite)) return false;



    int iRealSeek = FileSeek(hFile,(Cursor*ObjectLength),0);
    if ((Cursor*ObjectLength) != iRealSeek)
    {
        Error = erRecCount;
        FileClose(hFile);
        return false;
    }
    memset (NewRec,255,ObjectLength);
    bool work = true;
    while (work)
    {
        //-- ���������� �� ������ ����������� �����
        FileSeek(hFile,(Cursor*ObjectLength),0);
        int ReadRec = min(Descriptor->CacheRecord,Length-Cursor);

        Ptr = new char[ReadRec*ObjectLength + 1];
        if (!Ptr)
        {
            Error = erNotEnoughMemory;
            FileClose(hFile);
            return false;
        }
        int iReaded = FileRead(hFile,Ptr,(ReadRec*ObjectLength));
        if (iReaded != (ReadRec*ObjectLength))
        {
            Error = erReadError;
            FileClose(hFile);
            return false;
        }
        int iWrited = FileWrite(hFile,NewRec,ObjectLength);
        if (iWrited != ObjectLength)
        {
            Error = erWriteError;
            FileClose(hFile);
            return false;
        }
    }

    FileClose(hFile);
    return true;

/*
	if  (FileToMem() == -1) return -1;
	if  (Index >= Length) return 0;		//-- �������� ������

	if  (Index < (Length-1))
	{
		//-- ������� ���������� ����� �� ������ �����
		memmove ((Ptr + Index*ObjectLength),
			   (Ptr + (Index+1)*ObjectLength),
			   (Length-Index-1)*ObjectLength);
	}
	Length--;
    	return MemToFile();
        */
}

//------------------------------------------------------------------------------
int __fastcall TFDataBase::UpdateDB (void)       //-- �������� ����
{
//    return MemToFile();
    return 0;
}

//------------------------------------------------------------------------------
char* __fastcall TFDataBase::GetError (int ErrorCode)  //-- �������� ������
{
    if (Error > 0 && Error < erMaxError) return cTFDbError[Error];
    else return "";
}

//------------------------------------------------------------------------------
int __fastcall TFDataBase::SetSorting (int SortingType)
{
    return 0;
}

//------------------------------------------------------------------------------
int __fastcall TFDataBase::StoryPortionToTmpFile(int Len, TDateTime aDateTime)     // ���� �� ����
{
    int iWrited;
    int ReadRecCnt;
    AnsiString NewFileName;
    // ���� ��� �����, �� ���� ����� (>0)
    if ((double)aDateTime > 0){
        NewFileName = TmpDbFileName;
        rHeader.DateLastMagazining = aDateTime;
    }else
    {
        char NewFN[256];
        fnsplit(Descriptor->FileName, 0, 0, NewFN, 0);
        NewFileName = NewFN + Header.DateLastMagazining.FormatString (" hh-nn dd-mm-yyyy") + ".bak";
        // ���� ������
        rHeader.DateLastMagazining = Header.DateLastMagazining;
    }
    // ������� ��������� ����
    hTmpFile = FileCreate (NewFileName);

    if (hTmpFile == -1)
    {
        Error = erFileNotOpen;
        return -1;
    }

    // ���������� ���������
    rHeader.RecordCount = Len;
    strcpy(rHeader.ID,"TFDB FL");       //-- ���������� ��������������
    // ������� ���������
    iWrited = FileWrite(hTmpFile,&rHeader,sizeof(TFDbHeader));
    if (iWrited != sizeof(TFDbHeader))
    {
        Error = erWriteError;
        FileClose(hTmpFile);
        FileClose(hFile);
        return -1;
    }

    // �������� ��������� �����
    char *Ptr;
    Ptr = new char[Descriptor->CacheRecord*ObjectLength + 1];
    if (!Ptr)
    {
        Error = erNotEnoughMemory;
        FileClose(hTmpFile);
        FileClose(hFile);
        return -2;
    }

    try {
        int ReadRec = min(Descriptor->CacheRecord,Len);
        bool work = true;
        ReadRecCnt= 0;
        //-- ���������� �� ������ ����������� �����
        FileSeek(hFile,(Cursor*ObjectLength) + (int)sizeof(TFDbHeader),0);
        // ����������
        if(Len > 0)
        {
        while (work)
        {
    //            FileSeek(hFile,(Cursor*ObjectLength),0);
            // ������� ��������� ����
            int iReaded = FileRead(hFile,Ptr,(ReadRec*ObjectLength));
                ReadRecCnt += ReadRec;
                //���� �������� ��� ��� ���� ��������
                if((ReadRecCnt >= Len)||(iReaded != (ReadRec*ObjectLength)))
                work = false;
            // ���� ���-�� ������� - ��� �������
            if (iReaded > 0)
            {
                iWrited = FileWrite(hTmpFile,Ptr,iReaded);
                if (iWrited != iReaded)
                {
                    Error = erWriteError;
                    FileClose(hFile);
                    return -2;
                }
            }
        }
        }
        return 0;
    }
    __finally
    {
        FileClose(hTmpFile);
        delete []Ptr;
    }
    return 0;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Append (void *NewRec)
{
    if (!IsOpen) return false;

    if (!OpenDbFile(fmOpenReadWrite)) return false;
    Cursor = Length;
    Eof = true;

    int iFileSeek = Cursor*ObjectLength + sizeof(TFDbHeader);

    int iRealSeek = FileSeek(hFile,iFileSeek,0);
    if (iRealSeek != iFileSeek)
    {
        Error = erRecCount;
        FileClose(hFile);
        return false;
    }
    int iWrited = FileWrite(hFile,NewRec,ObjectLength);
    if (iWrited != ObjectLength)
    {
        Error = erWriteError;
        FileClose(hFile);
        return false;
    }
    Length++;
    Header.RecordCount = Length;

    if (!UpdateHeader()) return false;
    FileClose(hFile);
    Error = erNone;
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::UpdateHeader (void)       //-- �������� ���������
{
    int iRealSeek = FileSeek(hFile,0,0);
    if (iRealSeek != 0)
    {
        Error = erRecCount;
        FileClose(hFile);
        return false;
    }
    int iWrited = FileWrite(hFile,&Header,sizeof(TFDbHeader));
    if (iWrited != sizeof(TFDbHeader))
    {
        Error = erWriteError;
        FileClose(hFile);
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::Add (void *NewRec)
{
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::GetRecord(void* RecPtr)
{
    char *Ptr;
    if (!OpenDbFile(fmOpenReadWrite)) return false;

    int iFileSeek = Cursor*ObjectLength + sizeof(TFDbHeader);
    int iRealSeek = FileSeek(hFile,iFileSeek,0);

    if (iRealSeek != iFileSeek)
    {
        Error = erRecCount;
        FileClose(hFile);
        return false;
    }
    Ptr = new char[ObjectLength + 1];
    if (!Ptr)
    {
        Error = erNotEnoughMemory;
        FileClose(hFile);
        return false;
    }
    int iReaded = FileRead(hFile,Ptr,ObjectLength);
    if (iReaded != ObjectLength)
    {
        Error = erReadError;
        FileClose(hFile);
        return false;
    }
    memcpy (RecPtr,Ptr,ObjectLength);
    delete []Ptr;
    Error = erNone;
    FileClose(hFile);
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::FirstQuick (void)        //-- ������ �� ������ �������
{
    Cursor = 0;
    Bof = true;
    Eof = false;
    char *Ptr;
    if (!OpenDbFile(fmOpenReadWrite)) return false;

    int iFileSeek = sizeof(TFDbHeader);
    int iRealSeek = FileSeek(hFile,iFileSeek,0);
    if (iRealSeek != iFileSeek)
    {
        Error = erRecCount;
        FileClose(hFile);
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::GetRecordQuick(void* RecPtr)
{
    char *Ptr;
    Ptr = new char[ObjectLength + 1];
    if (!Ptr)
    {
        Error = erNotEnoughMemory;
        FileClose(hFile);
        return false;
    }
    int iReaded = FileRead(hFile,Ptr,ObjectLength);
    if (iReaded != ObjectLength)
    {
        Error = erReadError;
        FileClose(hFile);
        return false;
    }
    memcpy (RecPtr,Ptr,ObjectLength);
    delete []Ptr;
    Error = erNone;

    if (Cursor < (Length-1))
	{
    	Cursor ++;
        if (Cursor) Bof = false;
        Eof = false;
    	return true;
	}
    else
    {
        Bof = false;
        Eof = true;

        FileClose(hFile);
        Cursor = Length-1;
	return false;
    }
}

//------------------------------------------------------------------------------
bool __fastcall TFDataBase::SearchDate(TDateTime aDateTime)
{
    int TopCursor,BotCursor;
    // ���������� ����� ��� ������
    char *Ptr;
    if(Error)
        return false;
    Ptr = new char[ObjectLength + 1];
    if (!Ptr)
    {
        Error = erNotEnoughMemory;
        FileClose(hFile);
        return false;
    }

    try {
        // ������ �������
        TopCursor = 0;
        BotCursor = Length-1;

        while (true){

            if ((BotCursor - TopCursor) == 1){
                 if(Cursor != TopCursor){
                    Cursor = TopCursor;
                    if (!SetCursorPosFile (Cursor))
                        return false;
                 }
                 Error = erNone;
                 return true;
            }
            Cursor = (BotCursor+TopCursor)/2;

            // ������������� � �����
            int iFileSeek = Cursor*ObjectLength + sizeof(TFDbHeader);
            iRealSeek = FileSeek(hFile,iFileSeek,0);
            if (iRealSeek != iFileSeek)
            {
                Error = erRecCount;
                FileClose(hFile);
                return false;
            }

            // ������� ������� ������
            int iReaded = FileRead(hFile,Ptr,ObjectLength);
            if (iReaded != ObjectLength)
            {
                Error = erReadError;
                FileClose(hFile);
                return false;
            }
            // ������ ����� �������
                     //-- ���� ��������� �������
//            Cursor = (((sArmEvent*)Ptr)->Time > aDateTime)? BotCursor:TopCursor;
            if(*(TDateTime*)Ptr > aDateTime)
                BotCursor = Cursor;
            else    
                TopCursor = Cursor;
//            Cursor = (*(TDateTime*)Ptr > aDateTime)? BotCursor:TopCursor;
        }
    }
    __finally
    {
        delete []Ptr;
    }
    return true;
}

//------------------------------------------------------------------------------
TDateTime __fastcall TFDataBase::GetDateLastDB_Updating()
{
   return Header.DateLastDB_Updating;
}

//------------------------------------------------------------------------------
void __fastcall TFDataBase::SetDateLastDB_Updating(TDateTime aDateTime)
{
    if (!IsOpen) return;
    // ���� ���� � ����� ����� ������� - �� ���������.
    if(Header.DateLastDB_Updating >= aDateTime) return;

    // ������� ���� (���� �� �������� - ������)
    if (!OpenDbFile(fmOpenReadWrite)) return;

    Header.RecordCount = Length;
    Header.DateLastDB_Updating = aDateTime;
    if (!UpdateHeader()) return;
    FileClose(hFile);
    Error = erNone;
}
//------------------------------------------------------------------------------
TDateTime __fastcall TFDataBase::GetDateLastMagazining()
{
   return Header.DateLastMagazining;
}

//------------------------------------------------------------------------------
void __fastcall TFDataBase::SetDateLastMagazining(TDateTime aDateTime)
{
    if (!IsOpen) return;
    // ���� ���� � ����� ����� ������� - �� ���������.
    if(Header.DateLastMagazining >= aDateTime) return;

    // ������� ���� (���� �� �������� - ������)
    if (!OpenDbFile(fmOpenReadWrite)) return;
    // ������ ����������� ����� �����
    int iFileLength = FileSeek(hFile,0,2);
    // ����� ��������� ������ �� �������  (��������� ����� �� ���� ������)
    if(!SearchDate(aDateTime))
        return;                              // ��������� ������

    // ��������� ����� ����� � �������
    Length = (iFileLength - iRealSeek)/ObjectLength;
    // ����� � ����� ����
    if(StoryPortionToTmpFile(Length,aDateTime))
        return;                              // ��������� ������
    // �������� ������ �� ������ ������
    FileSeek(hFile,(int )sizeof(TFDbHeader),0);
//    FileSeek(hFile,sizeof(TFDbHeader),0);
    // ������ � ���� �� ������ �����
    if(StoryPortionToTmpFile(iRealSeek/ObjectLength,0))
        return;                              // ��������� ������

    Header.RecordCount = Length;
    Header.DateLastMagazining = aDateTime;
    FileClose(hFile);
    DeleteFile(DbFileNameCopy);
    RenameFile(DbFileName,DbFileNameCopy);
    DeleteFile(DbFileName);
    RenameFile(TmpDbFileName,DbFileName);
    Cursor = 0;
    Error = erNone;
}

