//---------------------------------------------------------------------------
#ifndef LinkH
#define LinkH
//---------------------------------------------------------------------------
#include "commands.h"


//-- ��������� ���������� �����
bool LoadChannelDll (AnsiString aDllName);

//-- ��������� ���������� �����
bool FreeChannelDll (void);

//-- ������� ��������� �� �����������
bool ExchangeDll (short *V,char *PSW, int PSW_Length);
bool ExchangeDllPar ();
bool ExchangeDllInfo (short *V,char *PSW, int PSW_Length);
bool ExchangeDllPSW (char *PSW, int PSW_Length);
bool ExchangeDllControl (short *V);



//-- �������� ��������� � ����������
bool WriteDataDll (int Address, void *Ptr, int Count);

//-- �������� ����� ���������� ���������� ���������� �������� �� � ��
bool WriteCcwDll (unsigned char* ccw);

bool WriteBcwDll (unsigned char* bcw);
bool WriteAutoControlDll (byte *aACW,int Index);
bool WriteTermDll (short* term);
bool WriteTermCurDll(short* termCur);

bool WriteProbe_Dll(unsigned short ccw1);
bool WriteREG_Dll (unsigned short ccw);
bool WriteMORE_LESS_Dll (unsigned short ccw);
bool ReadMORE_LESS_Dll (unsigned short *ccw);


#endif
