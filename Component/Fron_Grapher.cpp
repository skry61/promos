//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#pragma link "Grapher"
#include "Fron_Grapher.h"

#include <stdio.h>
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
static inline void ValidCtrCheck(TFron_Grapher *)
{
	new TFron_Grapher(NULL);
}

//---------------------------------------------------------------------------
__fastcall TFron_Grapher::TFron_Grapher(TComponent* Owner)
	: TGrapher(Owner)
{
	FHDivisor = 1;
	FVDivisor = 1;
	LimitColor = clRed;
	DataList = new TList;
	NewPenIndex=0;
	CS = new TCriticalSection;
	AxisName1 = 'X'; AxisName2 = 'Y';					// ����� ����


}
void __fastcall TFron_Grapher::ClearData(void) {           //������� ��� ������
    TPoint *p;
    for (int i = 0; i < DataList->Count; i++)
    {
        p = (TPoint *)DataList->Items[i];
        delete p;
//        delete []p;
    }
    DataList->Clear();
    NewPenIndex=0;
}
//---------------------------------------------------------------------------
__fastcall TFron_Grapher::~TFron_Grapher(void){
    ClearData();                           
	delete DataList;
	delete CS;

}

//---------------------------------------------------------------------------
bool __fastcall TFron_Grapher::Add(int x, int y) { //TPoint *Point){       //�������� ����� �����
	CS->Enter();
	try {
		if(DataList->Count == 0)
			DataList->Add(new TPoint(x,y)/*Point*/);
		else {
			TPoint *Point_Last = (TPoint *)DataList->Last();
			if((Point_Last->x != x) || (Point_Last->y != y))
				DataList->Add(new TPoint(x,y)/*Point*/);
			else return false;
		}
		return true;
	}
	__finally {CS->Leave();}
	return true;
}

//---------------------------------------------------------------------------
bool __fastcall TFron_Grapher::Add(TPoint *P) { //TPoint *Point){       //�������� ����� �����
	if(DataList->Count == 0)
		DataList->Add(P);
	else {
		TPoint *Point_Last = (TPoint *)DataList->Last();
		if((Point_Last->x != P->x) || (Point_Last->y != P->y))
		   DataList->Add(P);
		else return false;
	}
	return true;
}

//---------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetVDivisor (int iDivisor)
{
	if (iDivisor <= 0) iDivisor = 1;
	FVDivisor = iDivisor;
	Repaint();
}

//---------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetHDivisor (int iDivisor)
{
	if (iDivisor <= 0) iDivisor = 1;
	FHDivisor = iDivisor;
	Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetV_Disp (int Value)
{
	if(FV_Disp != Value)
	{
		VMax += FV_Disp - Value;
		VMin += FV_Disp - Value;
		FV_Disp = Value;
		Repaint();
	}
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetH_Disp (int Value)
{
	if(FH_Disp != Value)
	{
		HMax += FH_Disp - Value;
		HMin += FH_Disp - Value;
		FH_Disp = Value;
		Repaint();
	}
//	FH_Disp = Value;
//	Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetEndV (int iEnd)
{
	FEndV = iEnd;
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetEndH (int iEnd)
{
	FEndH = iEnd;
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetLimitLow (int iEnd)
{
	FLimitLow = iEnd;
}
//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetF_Value (int Value)
{
	FF_Value = Value;
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::SetW_Value (int Value)
{
	FW_Value = Value;
}

//------------------------------------------------------------------------------
int __fastcall TFron_Grapher::YFunconGrapher (int y_real)
{
	return GraphRect.Bottom - (int)(kY*(float)( (float)y_real  - (float)VMin));
//	return GraphRect.Bottom - (int)(kY*(float)( (float)y_real/1000  - (float)VMin));
}

//------------------------------------------------------------------------------
int __fastcall TFron_Grapher::XFunconGrapher (int x_real)
{
	return GraphRect.Left + (int)(kX*(float)((float)x_real - (float)HMin));
//	return GraphRect.Left + (int)(kX*(float)((float)x_real/1000 - (float)HMin));
}

//------------------------------------------------------------------------------
void __fastcall TFron_Grapher::Paint(void)
{
	TPoint *p;
	TCanvas *cnv = StartPaint();
	int PointCount;

	int intCount = DataList->Count;
	if(intCount != 0)
	{
		PrepareCalc();

		cnv->Pen->Style  = psSolid;
		cnv->Pen->Width = FLineWidth;
		cnv->Pen->Color = DFColor;                     //���� �����

		int X,Y;
		TPoint *p = (TPoint *)DataList->Items[0];
		//��������� �����
		X = XFunconGrapher (p->x/*+FH_Disp*/); Y = YFunconGrapher (p->y/*+FV_Disp*/);
		cnv->MoveTo (X,Y);
		int i=1;
		for(int j=0;j<=NewPenIndex;j++)
		{
			if(NewPenIndex == 0)
			{
			   PointCount = intCount;
			}else
			{
			   if(NewPenIndex != j)
				  PointCount = PenColorFIX[j];
			   else
				  PointCount = intCount;
			}
			for (;i<PointCount;i++){
				p = (TPoint *)DataList->Items[i];
				X = XFunconGrapher (p->x/*+FH_Disp*/); Y = YFunconGrapher (p->y/*+FV_Disp*/);
				//����������
				cnv->LineTo (X,Y);
			}
			if(NewPenIndex != 0)
			{
			   cnv->Pen->Color = PenColorArray[j];
			}
		}
		PrepareMarker();                  //���������� ������ - �� �����������
		DrawLabel (cnv, GraphRect);
	}
	FinishPaint();
	if (FOnPaint) FOnPaint(this);
}

void __fastcall TFron_Grapher::ShowData(void)
{
	CS->Enter();
	try {
		Repaint();
	}
	__finally {CS->Leave();}

}


//---------------------------------------------------------------------------

//TColor cResultMarkerColor[3]={clWhite,clYellow,clLime};

//---------------------------------------------------------------------------
void __fastcall TFron_Grapher::DrawLabel(TCanvas *cnv, TRect GraphRect1)
{
	int X1,Y1;
//	int _HMax,_HMin,_VMax,_VMin;

	PrepareCalc();
	int divx = (FHMax-FHMin)/FHStep;
	int divy = (FVMax-FVMin)/FVStep;

	bool osy = (FHMin <= 0 && FHMax >=0)?true:false;
	bool osx = (FVMin <= 0 && FVMax >=0)?true:false;

    cnv->Pen->Width = 1;
	cnv->Pen->Color = FLineColor;
    cnv->Pen->Style  = psDot;
    cnv->Brush->Style = bsClear;

    double dt;
    Extended Value;
    int i;
    for (i = 0; i<divx; i++)
    {
		X1 = XonGrapher (FHMin + FHStep*i);
		cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);

//		dt = (double)(FHMin + i*FHStep - FH_Disp) / (double)FHDivisor;
		dt = (double)(FHMin + i*FHStep) / (double)FHDivisor;
		cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
    }
    //��� ��� �� �
    X1 = XonGrapher (FHMin + FHStep*i);
	cnv->TextOut (X1-4,GraphRect1.Bottom+10,AnsiString(AxisName1)+AnsiString(" ��"));

    for (i = 0; i<divy; i++)
    {
		Y1 = YonGrapher (FVMin + FVStep*i);
        cnv->MoveTo (GraphRect1.Left,Y1);
		cnv->LineTo (GraphRect1.Right,Y1);

//		Value = (FVMin + FVStep*i - FV_Disp)/(double)FVDivisor;
		Value = (FVMin + FVStep*i)/(double)FVDivisor;
		cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));
    }
    //��� ��� �� Y
//    i++;
	Y1 = YonGrapher (FVMin + FVStep*i);
	cnv->TextOut (ClientRectEx.Left + 5,Y1-5,AnsiString(AxisName2)+AnsiString(" ��"));

	cnv->Pen->Style  = psSolid;

	if (!osy)   //-- ������� ��� ��� - �������� ��� Y
	{
		cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Left,GraphRect.Top);
        DrawArrow(cnv,eToTop,GraphRect.Left,GraphRect.Top);
    }
    else        //-- ������� ��� ���� - �������� ��� Y
    {
        X1 = XonGrapher (0);
        //X1 = GraphRect1.Left + (int)(kX*(float)(-FHMin));
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        DrawArrow(cnv,eToTop,X1,GraphRect.Top);

		dt = 0;//(double)(FHMin) / (double)FHDivisor;
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
	}
    if (!osx)   //-- ������� ��� ��� - �������� ��� X
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Right,GraphRect.Bottom);
        DrawArrow(cnv,eToRight,GraphRect.Right,GraphRect.Bottom);
    }
    else
    {
        Y1 = YonGrapher (0);
		//Y1 = GraphRect1.Bottom - (int)(kY*(float)(-FVMin));
		cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);

		Value = (0);
 //		Value = (double)(FHMin) / (double)FHDivisor;
		cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));

        DrawArrow(cnv,eToRight,GraphRect.Right,Y1);
    }
    //-- ���� ������ ������� ��������� ������� � ������ ����
    if (IsMarker && MarkerLinesCount) DrawMarker(cnv);  //-- SPACK
}

//---------------------------------------------------------------------------
void TFron_Grapher::AddColor(TColor NewPenColor)
{
    if(NewPenIndex<PEN_COLOR_MAX)
    {
       PenColorArray[NewPenIndex] = NewPenColor;
       PenColorFIX[NewPenIndex] = DataList->Count;
       NewPenIndex++;
    }
}
void TFron_Grapher::AddColor(void)
{
}

//---------------------------------------------------------------------------
void TFron_Grapher::ChangePenColor(TColor NewPenColor)
{
  //TODO: Add your source code here
  DFColor = NewPenColor;
}
namespace Fron_grapher
{
    void __fastcall PACKAGE Register()
    {
		TComponentClass classes[1] = {__classid(TFron_Grapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

