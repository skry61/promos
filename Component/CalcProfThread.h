//---------------------------------------------------------------------------

#ifndef CalcProfThreadH
#define CalcProfThreadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//#pragma link "Profile"
//#pragma link "XGrapher"
#include "MultiGrapher.h"
//---------------------------------------------------------------------------
class TCalcProfThread : public TThread
{
private:
        TMultiGrapher *MultiGrapher;
protected:
        void __fastcall Execute();
public:
       bool Finished;
        __fastcall TCalcProfThread(bool CreateSuspended,TMultiGrapher *aMultiGrapher);
};

extern TCalcProfThread * CalcProfThread;
//---------------------------------------------------------------------------
#endif
