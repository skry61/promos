//---------------------------------------------------------------------------
// ������ ��������� ������ � ������ ������
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include <vcl.h>
#include <dir.h>
#pragma hdrstop

#include "DbUnit_ev.h"
#include "EventMen.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
//char  cProfileDbFileName[100];

//---------------------------------------------------------------------------
//�����������
__fastcall TxDataBase::TxDataBase(Classes::TComponent* AOwner,TFDbDescriptor *tfdbDescript/*,TListView *aListView*//*,HWND ahWnd*/)
    : TFDataBase(AOwner,tfdbDescript)
{
    List = new TList();
//!!    OpenTable();
}    

//---------------------------------------------------------------------------
__fastcall TxDataBase:: ~TxDataBase()
{
    ClearAll();
    Close();                     // ������� ���� (����)
    delete List;
}

//---------------------------------------------------------------------------
void __fastcall TxDataBase::SetVisibleInterval(TDateTime value)
{
        FVisibleInt = value;
}

//---------------------------------------------------------------------------
TDateTime __fastcall TxDataBase::GetVisibleInterval()
{
        return FVisibleInt;
}

//---------------------------------------------------------------------------
bool __fastcall TxDataBase::RecoverTable (void)
{
    AnsiString NewFileName;// = Descriptor->FileName;
    char NewFN[256];

    fnsplit(Descriptor->FileName, 0, 0, NewFN, 0);
    NewFileName = NewFN;
    NewFileName += TDateTime::CurrentDateTime().FormatString (" hh-nn dd-mm-yyyy");
    NewFileName += ".bak";

    //-- ��������� ���������� ������ ���� ������
    if (!RenameFile(AnsiString (Descriptor->FileName),NewFileName))
    {
        MessageBox (GetActiveWindow(),
        "���������� �������� ������ � ���� ������ ��� ���\n"
        "���������� ��������� ��������� ����������� ��",
        "��������� ������!",MB_OK|MB_ICONERROR);
        return false;
    }
    return Open();
}

//-- �������� ���� ������ ---------------------------------------------------
bool __fastcall TxDataBase::OpenTable (void)
{
    if (!Open())
    {
        if (MessageBox (GetActiveWindow(),Descriptor->ErrOpen,
        "��������� ������!",MB_YESNO|MB_ICONERROR) == ID_NO) return false;
        else return RecoverTable();
    }
    return true;
}

/*
//-- ��������� ��������� � ���� ������ --------------------------------------
bool __fastcall StoreParam (sOptimalParam &optim_param)
{
    if (!ParamTable) return false;
    return ParamTable->Append(&optim_param);
}

//-- ��������� ��������� � ���� ������ --------------------------------------
bool __fastcall StoreParam (sArmParam* params)
{
    sOptimalParam   optim_param;        //-- ���������������� ���������
    PackParam(params,&optim_param);     //-- �������� ����������
    return StoreParam(optim_param);     //-- ���������� � �� ����� ����������
}

*/
//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������, ���� � ���������� �� �����
void __fastcall TxDataBase::LoadAll(TDateTime Time)
{
    LoadList(Time);
    FillListComp();
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ ������� �� ��
//---------------------------------------------------------------------------
bool __fastcall TxDataBase::LoadList(TDateTime StartTime)
{
    sArmEvent e;
    void *p;
    int Size = GetStructSize();
    ClearList();
    //-- ����� ����� ����� �����
    if (!FindRecord (StartTime)) return false;

    //-- �������� ��������� �� ���� ������ ����������
    while (!Eof)
    {
        GetRecord(&e);                                  //-- �������� ������� / ��������
        if (((double)StartTime > 0 &&
              e.Time >= StartTime)||         //--  ����������� �� ������� / ��������?
            (double)StartTime == 0)
        {
            p = CreateNew();                            //-- ������� ���������
            memcpy (p,&e,Size);                         //-- ��������� ���������
            List->Add (p);
        }
        Next();
    }
    return true;
}

//---------------------------------------------------------------------------
//-- ����� �����
bool __fastcall TxDataBase::FindRecord (TDateTime Time)
{
    double fTime,firstTime,lastTime;
    double dTime = (double)Time;
    int Locate;
    int Cnt;
    sArmEvent   event;

    //-- ��������� ����� ������ �� ����� ������
    First();
    GetRecord(&event);

    if (((double)event.Time) >= dTime) return true;

    //-- ��������� ����� ��������� �� ����� ������
    Last();

    GetRecord(&event);

    lastTime = event.Time;
    if (lastTime < dTime) return false;

    Cnt = RecCount();
    First();
    GetRecord(&event);

    firstTime = event.Time;

    while (1)
    {
        First();
        Locate = (int)(dTime * (double)Cnt) /(lastTime-firstTime);
        MoveBy(Locate);

        GetRecord(&event);
        fTime = event.Time;
        if (fTime == dTime) return true;

        if (fTime < dTime)
        {
            //-- ��������� ����� ���������
            while (!Eof)
            {
                GetRecord(&event);
                fTime = event.Time;
                if (fTime >= dTime) return true;
                Next();
            }
        }
        lastTime = fTime;
        Cnt = Locate;
    }
}

/*
//-- �������� ���� ������ ---------------------------------------------------
bool __fastcall OpenTables (void)
{

    if (!EventTable || !ParamTable)
    {
        MessageBox (GetActiveWindow(),
        "���������� ������� ������� ������ ���� ������ ��� ���\n"
        "�������: ��������� ������",
        "��������� ������!",MB_OK|MB_ICONERROR);
        return false;
    }
    if(!EventTable->OpenTable() || !ParamTable->OpenTable())
        return false;
    else    
        return true;
}


//-- �������� ���� ������ ---------------------------------------------------
bool __fastcall CloseTables (void)
{
    if (EventTable) {  EventTable->Close(); }
    if (ParamTable) {  ParamTable->Close(); }
    return true;
}

*/

