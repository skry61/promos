//---------------------------------------------------------------------------

#ifndef TimeGrapher_updH
#define TimeGrapher_updH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include "Grapher.h"
#include "TimeGrapher.h"
#include <Controls.hpp>
#include <ExtCtrls.hpp>
struct sAxisData {
   short AxFeed;
   short AxOffset;
   short Ax3;
   short Ax4;
};

struct sInputData {
   sAxisData AxisData[4];
   short Par1;
   short Par2;
   short Par3;
   short Par4;
};
// ����������� ���������� ���������� ����
#define MAX_AXIS_COUNT 4

//---------------------------------------------------------------------------
class PACKAGE TTimeGrapher_upd : public TTimeGrapher
{
private:
        int Finterval;
        TTimer* FTimer;
        AnsiString FFileName;
        int FKadrNumber;
        int ResultFile;
        int FStartViewOffset;
        float FUnitWeight;
        int FAxisNum;

        // ���������� ������ ��� ������������ ��������� � ����������� �����������
        DecodeKadr(int index = 0);
        void __fastcall SetTimer(TTimer* value);
        TTimer* __fastcall GetTimer();
        void __fastcall SetFileName(AnsiString value);
        AnsiString __fastcall GetFileName();
        void __fastcall SetKadrNumber(int value);
        int __fastcall GetKadrNumber();
        void __fastcall SetStartViewOffset(int value);
        int __fastcall GetStartViewOffset();
        void __fastcall SetUnitWeight(float value);
        float __fastcall GetUnitWeight();
        void __fastcall SetAxisNum(int value);
        int __fastcall GetAxisNum();
protected:
public:
       TList* Data;    //������ ���������� �� ���� ��������� � DLL
       void __fastcall LoadCanvas();
       void __fastcall FreeCanvas();
       void __fastcall Add(short *Buf);       //�������� ����� ����
       void __fastcall ClearData(void);       //������� ��� ������


        __fastcall TTimeGrapher_upd(TComponent* Owner);
        __fastcall ~TTimeGrapher_upd();
        virtual void __fastcall Paint(void);

        DrawData(int Axis = 0);
       __published:
        __property TTimer* Timer  = { read=GetTimer, write=SetTimer };
        __property AnsiString FileName  = { read=GetFileName, write=SetFileName };
        __property int KadrNumber  = { read=GetKadrNumber, write=SetKadrNumber };
        __property int StartViewOffset  = { read=GetStartViewOffset, write=SetStartViewOffset };
        __property float UnitWeight  = { read=GetUnitWeight, write=SetUnitWeight };
        __property int AxisNum  = { read=GetAxisNum, write=SetAxisNum };
};
//---------------------------------------------------------------------------
#endif
