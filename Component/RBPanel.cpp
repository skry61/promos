//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "RBPanel.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TRBPanel *)
{
    new TRBPanel(NULL);
}
//---------------------------------------------------------------------------
__fastcall TRBPanel::TRBPanel(TComponent* Owner)
    : TPanel(Owner)
{
    BevelOuter = bvRaised;
    BevelInner = bvNone;
    BorderWidth = 1;
    Width = 160;
    Height = 25;
    FInterval = 24;
    FChecked = true;

    Label = new TLabel (this);
    Label->Parent = this;
    Label->Top = 4;
    Label->Left = FInterval/3 + FInterval*2;
    Caption = "";
    //OnResize = MyResize;

    for (int i=0;i<2;i++)
    {
        RButton[i] = new TRadioButton (this);
        RButton[i]->Top = 4;
        RButton[i]->Left = FInterval/3 + FInterval*i;
        RButton[i]->Parent = this;
        RButton[i]->Caption = "";
        RButton[i]->Width = 24;
    }
    SetChecked(true);
    SetInterval(24);
    SetText("TwoRBPanel");
}
__fastcall TRBPanel::~TRBPanel()
{
    delete RButton[0];
    delete RButton[1];
    delete Label;
}


//---------------------------------------------------------------------------
void __fastcall TRBPanel::MyResize(TObject *Sender)
{
/*    for (int i=0;i<8;i++)
    if (TLb[i])
    {
        //-- ����� ��������� ��������� ��������
        TLb[i]->Top = (int)(((double)iTopLeft[i][0] * (double)Height ) / (double)56);
        TLb[i]->Left = (int)(((double)iTopLeft[i][1] * (double)Width ) / (double)230);// iTopLeft[i][1];
    }
    */
}

void __fastcall TRBPanel::SetText(AnsiString aText)
{
    FText = aText;
    Label->Caption = aText;
    Repaint();
}

void __fastcall TRBPanel::SetInterval(int aInt)
{
    FInterval = aInt;
}

void __fastcall TRBPanel::SetChecked(bool aCheck)
{
    FChecked = aCheck;
    RButton[0]->Checked = !FChecked;
    RButton[1]->Checked = FChecked;
}

bool __fastcall TRBPanel::GetChecked(void)
{
    return RButton[1]->Checked;
}


void __fastcall TRBPanel::SetOnTag(int Value)
{
    SetChecked((Tag & Value)?true:false);
}
//---------------------------------------------------------------------------
namespace Rbpanel
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TRBPanel)};
         RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------
 