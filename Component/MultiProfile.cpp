//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MultiProfile.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Grapher"
#pragma link "MultiGrapher"
#pragma link "XGrapher"
#pragma resource "*.dfm"
//#include "Params.h"
#include "ParamMen.h"
#include "FlameUnit.h"
#include "MainUnit.h"




TFormMuliProfile *FormMuliProfile;
HWND  hWndMultiProfile;
//---------------------------------------------------------------------------
__fastcall TFormMuliProfile::TFormMuliProfile(TComponent* Owner)
    : TForm(Owner)
{
    hWndMultiProfile = TWinControl::Handle;

}
//---------------------------------------------------------------------------

void __fastcall TFormMuliProfile::BitBtn_CgPrintClick(TObject *Sender)
{
    if(Form_Profil->Busy) return;
    // ����� ������ ������� ��� ����������� ����������
    TFlameUnit *FlameUnit = MainForm->FlameList->FlameUnits[2];

    Form_Profil->QRLabel_Header->Caption = "���������� �������";
    Form_Profil->PrepareDataForPrint(2,MultiGrapher1);
    Form_Profil->SetControlParams(FlameUnit->GetParam());
    Form_Profil->SetProfParams(FlameUnit->Profile);
    Form_Profil->SetExec(Sender);
    Form_Profil->Prof_QuickRep->PreviewModal();
}
//---------------------------------------------------------------------------
