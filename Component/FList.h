//---------------------------------------------------------------------------
#ifndef FListH
#define FListH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
enum FListState
{
	lsNone=1,
	lsYes,
	lsNo,
    lsActive,
	lsWarning,
    lsHeader,
};

struct TFListItem
{
    FListState State;
    char Name[50];
    AnsiString Text;
};

//---------------------------------------------------------------------------
class PACKAGE TFList : public TGraphicControl
{

private:
    TStrings* FHeaderLines;
    TStrings* FLines;
    TStrings* FWarningLines;
    TStrings* FErrorLines;

    int FItemHeight;
    int FActive;
    int FItemSpace;

    void __fastcall SetItemSpace(int Value)
        {
            FItemSpace = Value;
            //Repaint();
        }
    void __fastcall SetActive(int Value)
        {
            FActive = Value;
            //Repaint();
        }
    int __fastcall GetCount(void)
        {   return FLines->Count;       }
    void __fastcall SetItemHeight(int NewHeight)
        {
            FItemHeight=NewHeight;
            //Repaint();
        }

    void __fastcall DoDrawText(AnsiString Text,RECT* dRect, bool IsHeader);

	void __fastcall SetLines (TStrings* Value)
        {
            FLines->Assign(Value);
            //Repaint();
        }
	void __fastcall SetWarningLines (TStrings* Value)
        {
            FWarningLines->Assign(Value);
            //Repaint();
        }
	void __fastcall SetErrorLines (TStrings* Value)
        {
            FErrorLines->Assign(Value);
        }
	void __fastcall SetHeaderLines (TStrings* Value)
        {
            FHeaderLines->Assign(Value);
        }


    void __fastcall Clear(void);
    FListState __fastcall DefineState(int Index);

public:

	__fastcall TFList(TComponent* AOwner);
	__fastcall ~TFList()
    {
        FLines->Clear();
        FWarningLines->Clear();
        FErrorLines->Clear();
        FHeaderLines->Clear();
        delete FLines;
        delete FWarningLines;
        delete FErrorLines;
        delete FHeaderLines;
    }

    //virtual void __fastcall Repaint(void);
    virtual void __fastcall Paint(void);
    //virtual void __fastcall Show(void);

__published:

    __property TStrings* Lines ={read=FLines, write=SetLines};

    __property TStrings* WarningLines ={read=FWarningLines, write=SetWarningLines};
    __property TStrings* ErrorLines ={read=FErrorLines, write=SetErrorLines};
    __property TStrings* HeaderLines ={read=FHeaderLines, write=SetHeaderLines};
	__property int ActiveItem = {read=FActive, write=SetActive};
	__property int ItemSpace = {read=FItemSpace, write=SetItemSpace};
	__property int Count = {read=GetCount, nodefault};
    __property int ItemHeight = {read=FItemHeight, write=SetItemHeight};
	__property Font;

};

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
