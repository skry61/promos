// Borland C++ Builder
// Copyright (c) 1995, 2002 by Borland Software Corporation
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Pipe.pas' rev: 6.00

#ifndef PipeHPP
#define PipeHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <StdCtrls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <SysUtils.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Pipe
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TPipeKind { gkHorizontalBar, gkVerticalBar };
#pragma option pop

#pragma option push -b-
enum TPipeDirect { Forwart, Back };
#pragma option pop

class DELPHICLASS TPipe;
class PASCALIMPLEMENTATION TPipe : public Controls::TGraphicControl 
{
	typedef Controls::TGraphicControl inherited;
	
private:
	int FQ;
	int FSteps;
	int FNStep;
	TPipeKind FKind;
	Forms::TFormBorderStyle FBorderStyle;
	Graphics::TColor FBackColor;
	Graphics::TColor FForeColor;
	TPipeDirect FDirect;
	void __fastcall PaintBackground(Graphics::TBitmap* AnImage);
	void __fastcall PaintAsBar(Graphics::TBitmap* AnImage, const Types::TRect &PaintRect);
	void __fastcall SetPipeKind(TPipeKind Value);
	void __fastcall SetPipeDirect(TPipeDirect Value);
	void __fastcall SetBorderStyle(Forms::TBorderStyle Value);
	void __fastcall SetBackColor(Graphics::TColor Value);
	void __fastcall SetForeColor(Graphics::TColor Value);
	void __fastcall SetFSteps(int Value);
	void __fastcall SetFQ(int Value);
	void __fastcall SetNStep(int Value);
	
protected:
	virtual void __fastcall Paint(void);
	
public:
	__fastcall virtual TPipe(Classes::TComponent* AOwner);
	void __fastcall MakeStep(void);
	
__published:
	__property Enabled  = {default=1};
	__property TPipeKind Kind = {read=FKind, write=SetPipeKind, default=0};
	__property TPipeDirect Direct = {read=FDirect, write=SetPipeDirect, nodefault};
	__property Font ;
	__property Forms::TBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, default=1};
	__property Graphics::TColor BackColor = {read=FBackColor, write=SetBackColor, default=16777215};
	__property Graphics::TColor ForeColor = {read=FForeColor, write=SetForeColor, default=0};
	__property ParentColor  = {default=1};
	__property ParentFont  = {default=1};
	__property ParentShowHint  = {default=1};
	__property ShowHint ;
	__property Visible  = {default=1};
	__property int Steps = {read=FSteps, write=SetFSteps, default=4};
	__property int Q = {read=FQ, write=SetFQ, default=2};
	__property int NStep = {read=FNStep, write=SetNStep, default=5};
public:
	#pragma option push -w-inl
	/* TGraphicControl.Destroy */ inline __fastcall virtual ~TPipe(void) { }
	#pragma option pop
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Pipe */
using namespace Pipe;
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Pipe
