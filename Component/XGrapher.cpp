//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#pragma link "Grapher"
#include <math.h>
#include "XGrapher.h"
#include <stdio.h>
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.     
static inline void ValidCtrCheck(TXGrapher *)
{
    new TXGrapher(NULL);
}
//---------------------------------------------------------------------------
__fastcall TXGrapher::TXGrapher(TComponent* Owner)
    : TGrapher(Owner)
{
    FHDivisor = 1;
    FVDivisor = 1;
    LimitColor = clRed;
    aXDimension = "��.";
    aYDimension = "� %";
    IsProfile = true;
    FLimitLineWidth = FLineWidth;
}
void __fastcall TXGrapher::SetVDivisor (int iDivisor)
{
    if (iDivisor <= 0) iDivisor = 1;
    FVDivisor = iDivisor;
//!    Repaint();
}
void __fastcall TXGrapher::SetHDivisor (int iDivisor)
{
    if (iDivisor <= 0) iDivisor = 1;
    FHDivisor = iDivisor;
//!    Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TXGrapher::SetEndV (int iEnd)
{
    FEndV = iEnd;
//!    Repaint();
}
//------------------------------------------------------------------------------
void __fastcall TXGrapher::SetEndH (int iEnd)
{
    FEndH = iEnd;
//!    Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TXGrapher::SetLimitLow (int iEnd)
{
    FLimitLow = iEnd;
//!    Repaint();
}
//------------------------------------------------------------------------------
void __fastcall TXGrapher::SetF_Value (int Value)
{
    FF_Value = Value;
//!    Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TXGrapher::SetW_Value (int Value)
{
    FW_Value = Value;
//!    Repaint();
}

//------------------------------------------------------------------------------
int __fastcall TXGrapher::YFunconGrapher (int y_real)
{
    return GraphRect.Bottom - (int)(kY*(double)( ((float)y_real /* /(float)FVDivisor*/) - (double)VMin));
}

//------------------------------------------------------------------------------
void __fastcall TXGrapher::DrawLimitsLines(TCanvas *cnv)//void)
{
    int X = XonGrapher (FEndH);
    int Y = YonGrapher (FEndV);
    int Yl = YonGrapher (FLimitLow);
    int Yf = YonGrapher (FF_Value);
    int Yw = YonGrapher (W_Value);

    cnv->Pen->Color = FLimitColor;
    cnv->Pen->Width = FLimitLineWidth;

    //-- ������ ��� �����
    cnv->Pen->Color = FLimitColor;
    cnv->MoveTo (GraphRect.Left,Y);
    cnv->LineTo (X,Y);
    cnv->LineTo (X,GraphRect.Bottom);

//~~ 
    TColor color = cnv->Pen->Color;

    //������ ��������� � ����� ����������� ������� ���������
    cnv->Pen->Color = clBlue;
    cnv->Ellipse(X-4, Y-4, X+4, Y+4);
    cnv->Pen->Color = color;

//~~    
    
    //-- ������ ����� Co
    cnv->MoveTo (GraphRect.Left,Yl);
    cnv->LineTo (GraphRect.Right,Yl);

    //-- ������ ����� Cf
    cnv->MoveTo (GraphRect.Left,Yf);
    cnv->LineTo (GraphRect.Left+5,Yf);

    //-- ������ ����� Cw
    cnv->MoveTo (GraphRect.Left,Yw);
    cnv->LineTo (GraphRect.Left+5,Yw);

    /*
    double dt = (double)(FEndH) / (double)FHDivisor;
    cnv->TextOut (X-4,GraphRect.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
    dt = (FEndV)/(double)FVDivisor;
    cnv->TextOut (ClientRect.Left + 5,Y-5,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,FValuePrecision,FValueDigits));
    */
}


//------------------------------------------------------------------------------

void __fastcall TXGrapher::DrawTime(TCanvas *cnv)
{

    AnsiString STime = FMarkerTime.FormatString("hh:nn");
    int tw = cnv->TextWidth(STime);
    int th = cnv->TextHeight(STime);
    cnv->TextOut (GraphRect.Right - tw,(GraphRect.Top - BorderWidth)- th ,STime);
}
//------------------------------------------------------------------------------
void __fastcall TXGrapher::DrawLimitsLabels(TCanvas *cnv)
{
    int X = XonGrapher (FEndH);
    int Y = YonGrapher (FEndV);
    int Yl = YonGrapher (FLimitLow);
    int Yf = YonGrapher (FF_Value);
    int Yw = YonGrapher (W_Value);

    //-- ��� ������� � ���������
    double dt = (double)(FEndH) / (double)FHDivisor;
    AnsiString aStr = AnsiString("X�=") +  AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2);
    DrawSText(cnv, FMarkColor,X-4,GraphRect.Bottom+10,aStr);
    

    dt = (FEndV)/(double)FVDivisor;
    aStr = AnsiString("C�=") + AnsiString::FloatToStrF (dt,AnsiString::sffFixed,FValuePrecision,FValueDigits);
    DrawSText(cnv, FMarkColor,ClientRectEx.Left + 3,Y-5,aStr);
    dt = (FLimitLow)/(double)FVDivisor;
    aStr = AnsiString("Co=") +  AnsiString::FloatToStrF (dt,AnsiString::sffFixed,FValuePrecision,FValueDigits);
    DrawSText(cnv, FMarkColor,ClientRectEx.Left + 3,Yl-5,aStr);

    dt = (FF_Value)/(double)FVDivisor;
    aStr = AnsiString("C�=") + AnsiString::FloatToStrF (dt,AnsiString::sffFixed,FValuePrecision,FValueDigits);
    DrawSText(cnv, FMarkColor,ClientRectEx.Left + 3,Yf-5,aStr);

    dt = (W_Value)/(double)FVDivisor;
    aStr = AnsiString("C�=") + AnsiString::FloatToStrF (dt,AnsiString::sffFixed,FValuePrecision,FValueDigits);
    DrawSText(cnv, FMarkColor,ClientRectEx.Left + 3,Yw-5,aStr);

}
//---------------------------------------------------------------------------
void __fastcall TXGrapher::Paint(void)
{
    TCanvas *cnv = StartPaint();

    if(p_int)
    {
        int * FuncData = p_int;
        PrepareCalc();

        DrawLimitsLines(cnv);
        cnv->Pen->Style  = psSolid;
        cnv->Pen->Width = FLineWidth;
        double StepX = (double)(FHMax - FHMin)/intCount;

        int X1,X2,Y1,Y2;
        bool AllUnknown = true;

        for (int i=0;i < intCount;i++)
        {
            if ( *FuncData++ != UNKNOWN_DATA )
            {
                AllUnknown = false;
                break;
            }
        }
        if (AllUnknown)
        {
            cnv->Brush->Color = clGray;
            cnv->Brush->Style = bsSolid;
            cnv->FillRect(GraphRect);
            DrawLabel (cnv, GraphRect);
        }else{
            // ������� ��������� �������
            FuncData = p_int;
            int *FuncDataNext = p_int+1;
            double CurX = FHMin;
            for (int i=0;i<intCount-1;i++)
            {   //�������� ���������� ����� �� X
                X1 = XonGrapher ((int)CurX);  CurX += StepX; X2 = XonGrapher ((int)CurX);

                if ( *FuncData != UNKNOWN_DATA &&
                     *FuncDataNext != UNKNOWN_DATA )
                {
                    cnv->Pen->Color = DFColor;                     //���� �����
                    //�������� ���������� ����� �� Y
                    Y1 = YFunconGrapher ( *FuncData++ ); Y2 = YFunconGrapher ( *FuncDataNext++ );
                    //����������
                    cnv->MoveTo (X1,Y1);
                    cnv->LineTo (X2,Y2);
                }
                else
                {
                    TRect NoRect;
                    NoRect.Top = GraphRect.Top + 1;
                    NoRect.Bottom = GraphRect.Bottom - 1;
                    NoRect.Left = X1;
                    NoRect.Right = X2;

                    cnv->Brush->Color = clGray;
                    cnv->Brush->Style = bsSolid;
                    cnv->FillRect(NoRect);
                    // �� ����� �����
                    FuncDataNext++; FuncData++;
                }
            }
            PrepareMarker();                  //���������� ������ - �� �����������
            DrawLabel (cnv, GraphRect);
            if (IsProfile) DrawLimitsLabels(cnv);
            DrawTime(cnv);
        }
    }
    FinishPaint();
    if (FOnPaint) FOnPaint(this);
}

//---------------------------------------------------------------------------
/*
void __fastcall TXGrapher::DrawMarker(TCanvas *cnv)
{
    int X1 = XonGrapher (FH_Marker);
    // ������ �������� ����� ����� �������
    cnv->Pen->Color = clBlue;
    cnv->MoveTo (X1,GraphRect.Bottom);
    cnv->LineTo (X1,GraphRect.Top);
    cnv->Ellipse(X1-3, GraphRect.Bottom-3, X1+3, GraphRect.Bottom+3);

    cnv->Pen->Color = clBlack;

    int height = (cnv->TextHeight("A") + 3)*MarkerLinesCount;
    int width = 0;
    int t_w;
    if (MarkerLines){
        for (int i=0;i<MarkerLinesCount;i++)
        {
            t_w = cnv->TextWidth(AnsiString(MarkerLines[i]));
            if (t_w > width) width = t_w;
        }
        width +=10;

        cnv->Brush->Color = FMarkerColor;
        cnv->Brush->Style = bsSolid;
        cnv->Rectangle (X1,MarkerY - height - 4,X1 + width,MarkerY );

        int l_h = height/MarkerLinesCount;
        for (int i=0;i<MarkerLinesCount;i++)
           cnv->TextOut (X1+5,MarkerY+(l_h*i)-2 - height,AnsiString(MarkerLines[i]));
    }      
}
*/

TColor cResultMarkerColor[4]={clWhite,clYellow,clLime,clRed};

void __fastcall TXGrapher::PrepareMarker(){       //���������� �������

//-------------------------------------------------------------------------------
//   �������� �������� ��������� ������������ �������
//-------------------------------------------------------------------------------

    int Result = 0;
    int i;
        //-- ����� ����������� ������� � ������ ��������� �������� �
    if(p_int && !FMarker)
    {
        int * FuncData = p_int;
        double k = (double)intCount/(FHMax - FHMin);
        int End = FEndH * k;
        for (i=0; i< intCount-1;i++,FuncData++)
        if ((*FuncData >= FEndV)&&(*(FuncData+1) <= FEndV))
        {
            if (i >= (End * 1.1)){
                Result = 4;
            } else if (i >= End) {
                Result = 3;
            }else if (i >= (End * 0.9)){
                Result = 2;
            }else
                Result = 1;
            break;
        }
        //-- ����� ����������� ������� � ������ ��������� �������� �
        if(Result)
        {
            int hv = ceil( (double)i/k);
            SetMarkerX(hv);
            SetMarkerY(Height/2 - 50);
            sprintf (Msg1,"Xt=%5.3f ��", (double)hv/FHDivisor);
            sprintf (Msg2,"t= %s �:�", FMarkerTime.FormatString("hh:nn"));
            SetMarkerLines(2,FMarkerLines);
            if (Result < 5) MarkerColor = cResultMarkerColor[Result - 1];
            IsMarker = true;
        }else{
          MarkerLines = NULL;
          IsMarker = false;
        }  
    }
}

//---------------------------------------------------------------------------
void __fastcall TXGrapher::DrawLabel(TCanvas *cnv, TRect GraphRect1)
{
    int X1,Y1;

    PrepareCalc();
    int divx = (FHMax-FHMin)/FHStep;
    int divy = (FVMax-FVMin)/FVStep;

    bool osy = (FHMin <= 0 && FHMax >=0)?true:false;
    bool osx = (FVMin <= 0 && FVMax >=0)?true:false;

    cnv->Pen->Width = 1;
    cnv->Pen->Color = FLineColor;
    cnv->Pen->Style  = psDot;
    cnv->Brush->Style = bsClear;

    double dt;
    Extended Value;
    int i;
    for (i = 0; i<divx; i++)
    {
        X1 = XonGrapher (FHMin + FHStep*i);
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);

        dt = (double)(FHMin + i*FHStep) / (double)FHDivisor;
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));

        //-- � ������ ������� ������� ����
        if (i==0 || i == (divx-1))                               {
            //=cnv->TextOut (X1-4,GraphRect1.Bottom+24,dtTime.FormatString("dd.mm.yyy �"));
        }
    }
    //��� ��� �� �
//    i++;
    X1 = XonGrapher (FHMin + FHStep*i);
    cnv->TextOut (X1-4,GraphRect1.Bottom+10,aXDimension);

    for (i = 0; i<divy; i++)
    {
        Y1 = YonGrapher (VMin + FVStep*i);
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        Value = (FVMin + FVStep*i)/(double)FVDivisor;
        cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));
    }
    //��� ��� �� Y
//    i++;
    Y1 = YonGrapher (VMin + FVStep*i);
    cnv->TextOut (ClientRectEx.Left + 5,Y1-5,aYDimension);

    cnv->Pen->Style  = psSolid;

    if (!osy)   //-- ������� ��� ��� - �������� ��� Y
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Left,GraphRect.Top);
        DrawArrow(cnv,eToTop,GraphRect.Left,GraphRect.Top);
    }
    else        //-- ������� ��� ���� - �������� ��� Y
    {
        X1 = XonGrapher (0);
        //X1 = GraphRect1.Left + (int)(kX*(float)(-FHMin));
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        DrawArrow(cnv,eToTop,X1,GraphRect.Top);

        dt = (double)(FHMin) / (double)FHDivisor;
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
        //=dtTime = TDateTime(FTimeStart);
        //=cnv->TextOut (X1-4,GraphRect1.Bottom+10,dtTime.FormatString(FTimeFormat));
    }
    if (!osx)   //-- ������� ��� ��� - �������� ��� X
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Right,GraphRect.Bottom);
        DrawArrow(cnv,eToRight,GraphRect.Right,GraphRect.Bottom);
    }
    else
    {
        Y1 = YonGrapher (0);
        //Y1 = GraphRect1.Bottom - (int)(kY*(float)(-FVMin));
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);

        Value = (0);
        cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));
        //cnv->TextOut (ClientRectEx.Left + 5,Y1-5,IntToStr(0));

        DrawArrow(cnv,eToRight,GraphRect.Right,Y1);
    }
    //-- ���� ������ ������� ��������� ������� � ������ ����
    if (/*FMarker && */IsMarker && MarkerLinesCount) DrawMarker(cnv);  //-- SPACK
}
/*
void __fastcall TXGrapher::MyMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    if (!FMarker) return;
    if((X > GraphRect.Right)||(X < GraphRect.Left)||(Y < GraphRect.Top)||(Y > GraphRect.Bottom))
                  return;

    //-- ����� ������ ���� - ����� �����
    if (Button == mbRight || Button == mbMiddle)
      if (IsMarker)
         IsMarker = false;
      else
         return; //������ ����� ��� ��������
    //-- ������ ������ ���� - ���������� ������
    if (Button == mbLeft)
    {
        MarkerY = Y;
        MarkerPoint = (int)( (float) (X - GraphRect.Left)/kX);
        //=FMarkerTime = FTimeStart + TDateTime((double)i*(double)FTimeStep);

        FH_Marker = FHMin +  MarkerPoint;
        IsMarker = true;
        if (FOnMarker) FOnMarker(this);

    }
    Repaint();
}
*/
namespace Xgrapher
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TXGrapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------





void __fastcall TXGrapher::SetLimitLineWidth(int value)
{
    if(FLimitLineWidth != value) {
        FLimitLineWidth = value;
//!        Repaint();
   }
}
