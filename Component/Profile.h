//---------------------------------------------------------------------------
// ������ ���������� ��� ������������ ����������� �������
//---------------------------------------------------------------------------
#ifndef ProfileH
#define ProfileH
//---------------------------------------------------------------------------
#include<alloc.h>
#include<system.hpp>
#include "FMath.h"

typedef long double  PProc(double);
// ��� ��������� �� ������� ��� ������ ������� ������� �������
struct sProfileParam;

typedef double*  TCalc_ProfFunc (sProfileParam *);

//���������� ��������� ����� �������
#define POINTS 50
#define MAXPOINTS 1000
// ���������� ������
#define STADYS 6
struct TStadyInf {
 double StadyTime;         // ����� ������ � �����
 double PokExp;            // ��������� ������� ���������� ����������double ProfileArray[MAXPOINTS];     // ������ ������� �������
	    		           // ��� ������ ������, ���� ������� ����.�����double TmpDouble;                   // ������� ������ ������� ������� ������
		   	               // ��������� Cgdouble StadyTimesArray[STADYS];     // ���������� ������
 double  StartCg;          // ������� ��������� �������� Cgdouble PokExp[STADYS];             // 
 double  EndCg;            // ������� �������� �������� Cg//-------------------   ������� ���������  -----------------------
 double  Bt;               // ������� �������� Bettadouble Const6      = 6.0;
 double  BoundCond;        // ������� ����� ���������� �������double Const_T_abs = 273;
 double  CgLow;            // ������� ����� ������ ��������� Cgdouble Const_1t98  = 1.98;
				           // 1-��������; 2-����double Const1t5    = 1.5;
 double  TStart;           // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;
 double  TEnd;             // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;				        // ��� ������� ������double Const_min1  = -1.0;
 __fastcall  TStadyInf();  // ������������
};
     


   
//-- ��������� ������ ����� ������������ ������� -----------------------------
struct sOneEtapProfile
{
    int         Time;       //-- ����� �����, �
    int         T;          //-- �������� �����������, ����
    double      Cg;         //-- ���������� ���������, %

    sOneEtapProfile()
    {
        Time = 3600;    //-- ���
        T = 900;        //-- 900 ����
        Cg = 0.2;       //-- 0,2 ���������� ���������
    }

   sOneEtapProfile & operator =(const sOneEtapProfile &sP) //�ASSIGNMENT�OPERATOR.
   {
        Time = sP.Time;
        T = T;
        Cg = Cg;
        return *this;
   }
   AnsiString ToString(int idx)
   {
        AnsiString aStr;
        aStr.sprintf("%-6d %-6d %-6.3f %-5d",idx+1,Time/60,Cg,T);
        return aStr;
   }
};

#define MAX_ETAP_PROFILE    10
//-- ��������� ������ ������������ ������� -----------------------------
struct sEtapsProfile
{
    char            Sign[4];    //-- ��������� �����
    int             Number;     //-- � ��������� ���������
    int             Used;       //-- ���-�� ������������ ������
    int             AllTime;    //-- ����� ����� ������������ ������� � �

    sOneEtapProfile
    Etap[MAX_ETAP_PROFILE];     //-- ��������� ���� ������


    sEtapsProfile ()
    {
        strcpy(Sign,"TxPR");
        Number = 0;
        Used = 1;
    }

    void ComputeTime(void)
    {
        AllTime = 0;
        for (int i=0;i<Used;i++) AllTime += Etap[i].Time;
    }

    bool  __fastcall  Add(int idx);
    bool  __fastcall  Sub(int idx);

    bool  __fastcall  Save(AnsiString aFile);
    bool  __fastcall  Load(AnsiString aFile);

};


//-- ������������ ����� ������������ ������� --------------------------------
enum eTypeCreate
{
    etpUnknown = 0,
    etpC0const,
    etpC0func,
};

//-- ������������ �������� �������� ���������� ���� ------------------------
enum eGrafStatus
{
    egsStart = 0,        //-- 
    egsGasClapanOff,     //-- �� ��������� �������������� ������� ������ ���� ��������
    egsEndPeriod_0_1,    //-- ��������� ������� ������� �����
    egsGasClapanOn,      //-- �� ��������� �������������� ������� ������ ���� �������
    egsEndPeriod_1_2,    //-- ��������� ������� �������������� ������ ���������
    egsRizingCg_Prog     //-- ������ �� �� ���������
    
};

//-- ��������� ���������� ������� -------------------------------------------
struct sProfileParam
{
    char        Name[30];   //-- �������� �������
    int         Num;        //-- � �������
    int         SadkaNum;   //-- � �����
    int         Source;     //-- �������� ������������ (ID ����)
    eTypeCreate Create;     //-- ��� ������������

    double      Cg;         //-- �������� �f;
    double      Tp;         //-- ����������� ����
    double      Td;         //-- ����������� �������
    double      EDS;        //-- ���
    double      C0;         //-- ��������� �������� �� %
    double      D;          //-- ����������� �����������
    double      B;          //-- ����������� B
    double      X_Wid;      //-- ������� �������������� ����
    double      dX;         //-- ��� ����������
    double      EndC;       //-- �������� �������� ������������ ��������
    double      EndX;       //-- �� ��������� �������
    int         Points;     //-- ��������� ����� �������

//    int         Stage;      //-- ���-�� ������ ������������
    TDateTime   Setup;      //-- ����/����� ������� ������������ ����������� �������
    TDateTime   Start;      //-- ����� ������ ������������ ����������� ������� ����������
    int         AllTime;    //-- ����� ����� ������������ ����������� ������� � �
    int         Time;       //-- ����� ����� ������������ ����������� ������� � �
    
    bool        Done;       //-- ������������ ����������� ������� ���������
    bool        Fl_Start;   //-- ���� ������� �������


    double      Cf_Array[8];
    double      *Ptr;
    double      Sum;
    double      FMul;       // ��������� ��� �������������� double  � int
    double      LastCf;     //-- �������� �f c���� ������

//-------- ����� �������� ��������� ------------------    
    double      *Data;       //-- ������ �������
    double      *DataBase;   //-- ������ ������� ��� �������    
    int         *DataInt;    //-- ������ ������� ������� �����
//    double      FHDivisor;
                                              

//-- ������ ������ ----------------------------------------
    bool          UseEtaps;        //-- ������������ �� �����
    sEtapsProfile Etaps;           //-- ��������� ������
//    sProgParam   * prog_profile;   //-- ��������� ������������ ��������
//-- ������ ���������� ----------------------------------------
    bool          UsePrepare;        //-- ������������ �� ����������
    int           GrafStatus;        //-- ���� ��������� �������� ����������
    int           Period_0_1;        //-- ������ ������� �����
    int           Period_1_2;        //-- ������ �������������� ������ ���������
    int           ClapanOpenTime;    //-- ������ �������� �������
    TDateTime     EndGasClapanOff;   //-- ����� ��������� ���������� ������� ����
    TDateTime     EndPeriod_0_1;     //-- ����� ��������� ������� �����
    TDateTime     EndPeriod_1_2;     //-- ����� ��������� �������������� ���������� ����
    TDateTime     EndClapanOpenTime; //-- ����� ��������� �������� �������

    sEtapsProfile PrepareEtaps;      //-- ��������� ������ ����������
    
//---------------------------------------------------------

    double *Block1,*Block2,*Block3;
    double* ProfileArray;
    int  PredArraySize;      // ������ ������� ����������������� �� ���������� ����

//    __fastcall  sProfileParam(sProfileParam *  aProf);
   sProfileParam & operator =(const sProfileParam &sP); //�ASSIGNMENT�OPERATOR.
   sProfileParam & operator << (const sProfileParam &sP); //�ASSIGNMENT�OPERATOR.
   
   //-- ��������� ������� � ���� ������ --------------------------------------
   bool  __fastcall  SaveProf();
   bool  __fastcall  SaveFile(AnsiString aFile);

   bool  __fastcall FindProfile();

   void  __fastcall  InitData(double V);
   void  __fastcall  SetFrontData(){ Cg = LastCf = *DataBase; }
   void  __fastcall  InitData(double Width,double dX,double V);
   
   //-----------------------------------------------
   void  __fastcall  SetMul(double aMul){
       if(FMul != aMul){
           FMul = aMul;
           SetDataInt();
       }
   }
   //-----------------------------------------------
   int  __fastcall  SetDataInt(){
       if(DataInt!= NULL){
          int *p = DataInt;
          double *p1 = Data;
          for (int i=0;i<Points;i++) *p++ = (int)((*p1++) * FMul + 0.5);
          return *DataInt;
       }
       return 0;   
   }
   //-----------------------------------------------
   
   __fastcall  sProfileParam(double Width,double dX,double V);
   __fastcall  sProfileParam(AnsiString aFile);
   __fastcall  ~sProfileParam();
   double* __fastcall  LoadFromFile(AnsiString aFile);
   //��������� ��� ������ -  ����� � ������
   void __fastcall  EvenData(){
     if((DataBase!= NULL)&&(Data!=NULL))
         memcpy(DataBase,Data,Points * sizeof(double));
   }

   //��������� ��� ������ -  ������ � �����  
   void __fastcall  ReEvenData(){
     if((DataBase!= NULL)&&(Data!=NULL)){
         memcpy(Data,DataBase,Points * sizeof(double));
         SetDataInt();
     }    
         
   }
   // �� CalcProfile
   void __fastcall CalculateProc();
   int __fastcall CalculateProcData();
   long double	Calc_ExpForD(double aSumSteps);
   void  CulcData(PProc P7,PProc P6,PProc P5,PProc P4,PProc P3,PProc P2,PProc P1,
   double*PSumSteps, double* ProfData, int NPoints, double Xl, double lStep, int NSteps, int equ2);
   
   
   
   //   �������� �������� ��������� ������������ �������
   int  __fastcall  sProfileParam::TestFinish(int &Index);
   
   
//-----------------------------------------------------
    //����������� �� ���������
    __fastcall  sProfileParam(){
             LastCf = 0;
             InitCf();
             Data = NULL;
             DataBase = NULL;
             DataInt = NULL;
             Points   = 0;     //-- ��������� ����� �������
             AllTime  = 0;     //-- ����� ����� ������������ ����������� ������� � �
             Time     = 0;     //-- ����� ����� ������������ ����������� ������� � �
             Done     = false; //-- ������������ ����������� ������� ���������
             Fl_Start = false; //-- ���� ������� �������
             Create = etpUnknown;
             UseEtaps  = false; //-- � ������� ��� ������
             FMul = 1000;
    }
    
    void __fastcall InitCf(){
             for(int i=0;i<8;i++) {
                 Cf_Array[i] = LastCf;
             }
             Sum = LastCf *8;
             Ptr = Cf_Array;
    };

    double __fastcall GetCf(){
             return Sum/8;
    };

    void __fastcall SetCf(double aCf){
             Sum -= *Ptr;
             *Ptr = aCf;
             Sum += aCf;
             Ptr++;
             if(Ptr == (double *)&Ptr) Ptr = Cf_Array;
             Cg = LastCf = aCf;
    };
    void __fastcall CalculateCg()
    {
       double Tpk = CelcToKelvin_l(Tp);
       Cf = Cg_func_l(Ac_func_l(C0, Tpk, EDS, CelcToKelvin_l(Td)), Tpk);
    }


    __property double Cf = {read=GetCf, write=SetCf};
    __property double Mul = {read=FMul, write=SetMul};
private:
    void __fastcall SetX_width(double value);
    void __fastcall SetX_Step(double value);
        void __fastcall SetProfileFileName(AnsiString value);
        AnsiString __fastcall GetProfileFileName();
public:    
    __property double X_width  = { write=SetX_width };
    __property double X_Step  = { write=SetX_Step };
        __property AnsiString ProfileFileName  = { read=GetProfileFileName, write=SetProfileFileName };

};


typedef struct sProfileParam  TProfileParam;

//-- ��������� ����� ���� ������ --------------------------------------------
/*
    2 ����� - ������������� ����� ������ ������� PR
    2 ����� - ����� ��������� sProfileParam
    ... ���������� ������ ��������� sProfileParam

    4 ����� - ����� ������ ������� = (X_Wid/dX) * sizeof (double)
*/

  /*
//---------------------------------------------------------------------------
//-- ������� ��� ������� � ����� ������� ������� ----------------------------
enum eThrdCmd
{
    eEmpty = 0,     //-- ������ �������
    eStartCalc,     //-- ������ ������ �������
    eAbortCalc,     //-- �������� ������ �������
    eExitThread,    //-- ��������� ����
    eSetIdle,       //-- ���������� �������� �������
    eWaitTimeTail   //-- ���������� �������� ��������� ������� ������� ���������
};

//-- ��������� ���������� ����� ---------------------------------------------
struct sThreadControl
{
    eThrdCmd        Cmd;        //-- ������� ��� ����
    sProfileParam   *param;     //-- ��������� �������
    int             Idx;        //-- ������ �������
    double Cf;                  //-- Cf
    TDateTime dTime;            //--  �������� �����������
    TDateTime StartTime;        //--  ������ ��������� �����������
};

//-- ������ ���� ------------------------------------------------------------
enum eThrdStat
{
    eUnknow = 0,    //-- ����������
    eStart,         //-- ������ ����� �������
    eIdle,          //-- ��������
    eCalculate,     //-- ������ ������������
    eEndOfCalc,     //-- ���������� ���������
    eDead           //-- ���� ������
};

//-- ��������� ������� ���� -------------------------------------------------
struct sThreadStatus
{
    eThrdStat   Stat;       //-- ������

    int         Count;      //--

    int         Max;        //-- �������� ����������
    int         Current;    //-- ������� ��������� ����������
};
*/

//��������
//double*  NewCXT_Func (sProfileParam* profile);

//---------------------------------------------------------------------------
//-- ��������� ������ � ��������� -------------------------------------------
//---------------------------------------------------------------------------

//-- ��������� ������� � ���� ������ ----------------------------------------
bool __fastcall StoreProfile (sProfileParam *profile, double *data);

//-- ����� ������� � ���� ������ --------------------------------------------
//bool __fastcall FindProfile (sProfileParam *profile);

//-- ������� ������� �� ���� ������ -----------------------------------------
double* __fastcall ReadProfile (sProfileParam *profile);

//-- ��������� ������������� ������� � ���� ������ --------------------------
bool __fastcall ProfileExists (sProfileParam *profile);

//-- ��������� ����� ����� ������� ------------------------------------------
AnsiString __fastcall ProfileFileName(sProfileParam *profile);



#endif
