//---------------------------------------------------------------------------
//      ������ �������������� � ������������ ���-��
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Link.h"
//#include "Unit1.h"
//#include "MainUnit.h"

#include "Config.h"
#include "Params.h"
#include "ParamMen.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#define min(a, b)  (((a) < (b)) ? (a) : (b))

extern double EDS_K;
//-- ���������� ��������� ���������� -----------------------------------
HINSTANCE hDLL;

//-- ����� ����� � ��������� ���������� --------------------------------
int  (__cdecl *pExchange)(int CMD, int addr = 0, int size=0,void *Buff = NULL);
int  (__cdecl *pPLC_Interface) (char Cmd_XXX, short Count, void *Buf, short adresPLC );
void (__cdecl *pSetNode)(short);
void (__cdecl *pSetChanal)(char Com, short Speed, char ParityMode, char ParityType, char NumberBits, char StopBitLength);
void (__cdecl *pSetInterface)(short);
void (__cdecl *pSetSerialPort)(char COMPort);

extern unsigned char psw[MAX_OBJECT + 2];       //-- ����� ��������� �����������
//-- ��������� ��������� ���������� ------------------------------
//-- (���������� ����� ����� � ��������� �������������)-----------
bool LoadChannelDll (AnsiString aDllFileName)
{
    AnsiString aCom;
    AnsiString aInterface;
    int  Interface;
    // hChannelDll - ���������� ����������� ����������
    hDLL = LoadLibrary(aDllFileName.c_str());
    if (!hDLL)
    {

        MessageBox (GetActiveWindow(),"���������� ��������� ��������� ����������","������!",MB_OK|MB_ICONERROR);
        LastErrorCode = eErrDllNotFound;
        return false;
    }
//    pExchange =         (int  (__cdecl *)(int, int, int, void *))
//                                GetProcAddress(hDLL,"_Exchange");
    pPLC_Interface =    (int  (__cdecl *)(char, short, void *, short ))
                                GetProcAddress(hDLL,"_PLC_Interface");
    pSetNode =          (void (__cdecl *)(short))
                                GetProcAddress(hDLL,"_SetNode");
    pSetChanal =        (void (__cdecl *)(char,short,char,char,char,char))
                                GetProcAddress(hDLL,"_SetChanal");
    pSetInterface =     (void (__cdecl *)(short))
                                GetProcAddress(hDLL,"_SetInterface");
    pSetSerialPort =    (void (__cdecl *)(char))
                                GetProcAddress(hDLL,"_SetSerialPort");

    if (/*!pExchange||*/!pPLC_Interface||!pSetNode|| !pSetChanal||!pSetInterface||!pSetSerialPort)
    {
        MessageBox (GetActiveWindow(),"���������� �������� ������ � ���������� ����������","������!",MB_OK|MB_ICONERROR);
        hDLL = NULL;
        LastErrorCode = eErrDllIncorrect;
        return false;
    }

    int Com;
    //-- ������ �� ������� �������� ������ ��� ����� �����
    if (!GetRegParam(cFlameCtrlRegKey,"COM",aCom))
    {
        aCom = IntToStr(COM_PORT_DEFAULT);
        PutRegParam(cFlameCtrlRegKey,"COM",IntToStr(COM_PORT_DEFAULT));
    }
    else Com = aCom.ToInt();

    //-- ������ �� ������� ���� ���������� (RS 232/RS 485)
    if (!GetRegParam(cFlameCtrlRegKey,"Interface",aInterface))
    {
        Interface = 0;
        PutRegParam(cFlameCtrlRegKey,"Interface","0");
    }
    else Interface = aInterface.ToInt();

    short int IO_Value;

    pSetSerialPort((char)(Com));                //-- ���������� ����� ����� �����
    pSetInterface(Interface);                   //-- ���������� ��������� 485
    try {
       pPLC_Interface(Cmd_TEST,2,&IO_Value,0);     //-- ����������� ���������� ������ ���������
    }
    catch ( ... )
    {
       // handler for any C++ exception
       LastErrorCode = eErrCOM_PordBusy;
       return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool ExchangeDll (short *V,char *PSW, int PSW_Length)
{
    sLinkParam IO_buf[8];
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_READDATA,sizeof(IO_buf),&IO_buf,LINK_DATA_ADDRESS) != 0) return false;
    Application->ProcessMessages();
    if (pPLC_Interface (Cmd_READIO,PSW_Length,PSW,LINK_PSW_ADDRESS) != 0) return false;
    Application->ProcessMessages();
//    short V;
    if (pPLC_Interface (Cmd_READIO,2, // - ��� ����� ���������� ��� �������
                        V,CONTROL_MORE_LESS_CCW_ADDRESS) != 0) return false;
    Application->ProcessMessages();
//    TClap_Pipe::ControlBits = V;

    for (int i = 0; i<MAX_OBJECT; i++)
    {
        all_param[i].Param[0] = min(IO_buf[i].Tp,2000);
        all_param[i].Param[1] = min(IO_buf[i].Td,2000);
        if(i<3) all_param[i].Param[2] = (double)(IO_buf[i].E);
        else    all_param[i].Param[2] = min(EDS_K * (double)(IO_buf[i].E),2500);
    }
    return true;
}

//------------------------------------------------------------------------------
bool ExchangeDllPar ()
{
    sLinkParam IO_buf[8];
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_READDATA,sizeof(IO_buf),&IO_buf,LINK_DATA_ADDRESS) != 0) return false;
    Application->ProcessMessages();
    for (int i = 0; i<MAX_OBJECT; i++)
    {
        all_param[i].Param[0] = min(IO_buf[i].Tp,2000);
        all_param[i].Param[1] = min(IO_buf[i].Td,2000);
        if(i<3) all_param[i].Param[2] = (double)(IO_buf[i].E);
        else    all_param[i].Param[2] = min(EDS_K * (double)(IO_buf[i].E),2500);
    }
    return true;
}

//------------------------------------------------------------------------------
bool ExchangeDllInfo (short *V,char *PSW, int PSW_Length)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_READIO,PSW_Length,PSW,LINK_PSW_ADDRESS) != 0) return false;
    Application->ProcessMessages();
//    short V;
    if (pPLC_Interface (Cmd_READIO,2, // - ��� ����� ���������� ��� �������
                        V,CONTROL_MORE_LESS_CCW_ADDRESS) != 0) return false;
    Application->ProcessMessages();
    return true;
}

//------------------------------------------------------------------------------
bool ExchangeDllPSW (char *PSW, int PSW_Length)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_READIO,PSW_Length,PSW,LINK_PSW_ADDRESS) != 0) return false;
    Application->ProcessMessages();
    return true;
}

//------------------------------------------------------------------------------
bool ExchangeDllControl (short *V)
{
    if (pPLC_Interface (Cmd_READIO,2, // - ��� ����� ���������� ��� �������
                        V,CONTROL_MORE_LESS_CCW_ADDRESS) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool WriteDataDll (int Address, void *Ptr, int Count)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEDATA,Count,Ptr,Address) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool WriteCcwDll (unsigned char* ccw)
{
    if (!hDLL) return false;                                          //$$
    if (pPLC_Interface (Cmd_WRITEIO,MAX_OBJECT/*+2 + 2*MAX_OBJECT*/, // +2 - ��� ����� ���������� �� C�
                        ccw,LIMITS_CCW_ADDRESS) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool WriteREG_Dll (unsigned short ccw)  // - ��� ����� ���������� ��� �������
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEIO,1,
                        &ccw,CONTROL_REGIM_CCW_ADDRESS /** 8 + Chanal*/) != 0) return false;
    return true;
}
// ����� ��� ����������
//bool WriteMORE_LESS_Dll (unsigned char ccw){

//}

//------------------------------------------------------------------------------
bool WriteProbe_Dll(unsigned short ccw1)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEIO,1,
                        &ccw1,CONTROL_PROBE_ADDRESS) != 0) return false;
    return true;
}


//------------------------------------------------------------------------------
bool WriteMORE_LESS_Dll (unsigned short ccw)
{
    if (!hDLL) return false;
//    char V = ccw & 1;                                          //$$
    if (pPLC_Interface (Cmd_WRITEIO,2, // - ��� ����� ���������� ��� �������
                        &ccw,CONTROL_MORE_LESS_CCW_ADDRESS /** 8 + Chanal*2*/) != 0) return false;
//    V =(ccw & 2) >> 1;                                          //$$
//    if (pPLC_Interface (Cmd_WRITEBIT,1, // - ��� ����� ���������� ��� �������
//                        &V,CONTROL_MORE_LESS_CCW_ADDRESS * 8 + Chanal * 2+1) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool ReadMORE_LESS_Dll (unsigned short *ccw)
{
    if (!hDLL) return false;
    short V;                                          //$$
    if (pPLC_Interface (Cmd_READIO,2, // - ��� ����� ���������� ��� �������
                        &V,CONTROL_MORE_LESS_CCW_ADDRESS) != 0) return false;
    *ccw = V;
    Application->ProcessMessages();
    return true;
}

//------------------------------------------------------------------------------
bool WriteBcwDll (unsigned char* bcw)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEIO,1,
                        bcw,BURN_SENSOR_CCW_ADDRESS) != 0) return false;
    return true;
}


//------------------------------------------------------------------------------
bool WriteAutoControlDll (byte *aACW,int Index)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEIO,1,
                        aACW,FLAG_PARAM_FROM_PLC_ADDRESS + Index/2) != 0) return false;
    return true;
}


//------------------------------------------------------------------------------
bool WriteTermDll (short* term)
{
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEDATA,18,
                        (unsigned char *)term,TERM_CONTROL_ADDRESS) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool WriteTermCurDll(short* termCur){
    if (!hDLL) return false;
    if (pPLC_Interface (Cmd_WRITEDATA,18,
                        (unsigned char *)termCur,TERM_CURRENT_ADDRESS) != 0) return false;
    return true;
}

//------------------------------------------------------------------------------
bool FreeChannelDll ()
{
    return FreeLibrary(hDLL);
}
