//---------------------------------------------------------------------------


#pragma hdrstop

#include "ParamMen.h"

#pragma package(smart_init)

TFDbDescriptor tfdParam=
{
    "param.db",
    sizeof (sOptimalParam),
    true,
    false,
    100,
// ��������� ��� ������ ��������
    "���������� �������� ������ � ���� ������ ���������� ��� ���\n"
    "�������: ����������� �� ���������� \n\n"
    "���������� ������������ �� ���������� ?\n"
};

TParamMen  *ParamMen;
sArmParam  all_param[MAX_OBJECT];      //-- ��������� ���� �������
sArmParam  back_param[MAX_OBJECT];     //-- ����������� ���������


//---------------------------------------------------------------------------
__fastcall TParamMen::TParamMen(Classes::TComponent* AOwner)
  : TxDataBase(AOwner, &tfdParam)
{
   Visible = false;
   pParam = all_param;
   ParamMen = this;
}
//---------------------------------------------------------------------------
__fastcall TParamMen::~TParamMen()
{
  ParamMen = NULL;
}

//---------------------------------------------------------------------------
//-- ���������� ������ ���������
int  __fastcall TParamMen::GetStructSize(void)
{
   return sizeof(sArmParam);
}

//---------------------------------------------------------------------------
//-- ������� ����� �������
void * __fastcall TParamMen::CreateNew(void)
{
   return (void*)new sArmParam;                          //-- ������� ���������
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ ����������
//---------------------------------------------------------------------------
void __fastcall TParamMen::ClearList(void)
{
    sArmParam *p;
    for (int i = 0; i < List->Count; i++)
    {
        p = (sArmParam *)List->Items[i];
        if (p)
        delete []p;
    }
    List->Clear();
}

//-- ��������� ��������� � ���� ������ --------------------------------------
//bool __fastcall TParamMen::Store (sArmParam* params)
bool __fastcall TParamMen::Store(void)
{
    sOptimalParam   optim_param;        //-- ���������������� ���������
    PackParam(&optim_param);            //-- �������� ����������
    return  Append(&optim_param);       //-- ���������� � �� ����� ����������
}


bool __fastcall  TParamMen::Generate(int EventId,int Source,char* Comment)
{
 return false;
}

//---------------------------------------------------------------------------
//-- ��������� �������� ���� ���������� �������� � ���������������� ���������
//---------------------------------------------------------------------------
bool __fastcall TParamMen::PackParam(sOptimalParam* optim_param)
{
    //-- ����� �������� �� �������
    sArmParam* p = pParam;
    optim_param->Time = p->Time;

    for (int i=0;i<MAX_OBJECT;i++,p++)
    {
        if(p->Fl_UNKNOWN_DATA){
            optim_param->Params[i].Tp = (short)UNKNOWN_DATA;
            optim_param->Params[i].Td = (short)UNKNOWN_DATA;
            optim_param->Params[i].E  = (short)UNKNOWN_DATA;
            optim_param->Params[i].CO = (short)UNKNOWN_DATA;
            optim_param->Params[i].Ac = (short)UNKNOWN_DATA;
            optim_param->Params[i].Cg = (short)UNKNOWN_DATA;
        }else{
            optim_param->Params[i].Tp = (short)p->Param[0];
            optim_param->Params[i].Td = (short)p->Param[1];
            optim_param->Params[i].E  = (short)p->Param[2];
            optim_param->Params[i].CO = (short)(p->Param[3]*100);
            optim_param->Params[i].Ac = (short)(p->Param[4]*1000);
            optim_param->Params[i].Cg = (short)(p->Param[5]*1000);
        }
        optim_param->Params[i].ObjectId = p->Source;
    }

    //-- ������������ ��������� �����
/*
    for (int i=0;i<3;i++)
    {
        optim_param->Param1[i].Tp = (pParam+i)->Param[0];
        optim_param->Param1[i].Td = (pParam+i)->Param[1];
        optim_param->Param1[i].E  = (pParam+i)->Param[2];
        optim_param->Param1[i].CO = (short int)((pParam+i)->Param[3]*100);
        optim_param->Param1[i].Ac = (pParam+i)->Param[4];
        optim_param->Param1[i].Cg = (pParam+i)->Param[5];
    }

    //-- ������������ ��������� ���������������
    for (int i=0;i<3;i++)
    {
        optim_param->Param2[i].Tp = (pParam+3+i)->Param[0];
        optim_param->Param2[i].Td = (pParam+3+i)->Param[1];
        optim_param->Param2[i].E  = (pParam+3+i)->Param[2];
        optim_param->Param2[i].CO = (short int)((pParam+3+i)->Param[3]*100);
        optim_param->Param2[i].Ac = (pParam+3+i)->Param[4];
    }
*/
    return true;
}

//---------------------------------------------------------------------------
//-- ��������� ���������� �������� � ������������� ������
//---------------------------------------------------------------------------
void __fastcall TParamMen::AddParamList()
{
    sArmParam *p = new sArmParam[MAX_OBJECT];
    memcpy(p,pParam,sizeof(sArmParam)*MAX_OBJECT);
    List->Add (p);

    //-- ���� �� ������ ��� ������ ������ �� ������
    if (List->Count > 1)
    while (1)
    {
        p = (sArmParam *)List->Items[0];
        if (p->Time < (pParam->Time - VisibleInterval) )
        {
            delete []p;
            List->Delete(0);
        }
        else break;
    }
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� �� ������������� ������
//---------------------------------------------------------------------------
int* __fastcall TParamMen::MakeFuncData(int ObjectIdx, int ParamIdx, int Mul, int &Cnt)
{
    Cnt = List->Count;
    int *p_int = new int[Cnt];
//    p_int = new int[Count];
    sArmParam v_all_param[MAX_OBJECT];

    for (int i=0;i<Cnt;i++)
    {
        memcpy(v_all_param,List->Items[i],sizeof(sArmParam)*MAX_OBJECT);

        //-- ���� � ���� Source ������� "��� ������" - ��������� ��� � ��������
//AA 17.05.07
//        if (v_all_param[ObjectIdx].Source == UNKNOWN_DATA) *(p_int+i) = UNKNOWN_DATA;
        if (v_all_param[ObjectIdx].Fl_UNKNOWN_DATA == UNKNOWN_DATA)
           *(p_int+i) = UNKNOWN_DATA;
        else
           *(p_int+i) = (int)(v_all_param[ObjectIdx].Param[ParamIdx]*(double)Mul);
    }
    return p_int;
}

//---------------------------------------------------------------------------
//-- ���������� ����� �� ����� ������ ���������
TDateTime  __fastcall  TParamMen::GetFirstTime (void)
{
   return ((sArmParam*)List->Items[0])->Time;
}


//---------------------------------------------------------------------------
//-- ������������ ��������� --------------------------------------------------
//---------------------------------------------------------------------------
bool __fastcall TParamMen::GenerateParam(sArmParam *param,int Source)
{
    param->Time = TDateTime::CurrentDateTime();
    param->Source = Source;
    for (int i=0;i<MAX_PARAM;i++) param->Param[i] = 0.0;
    return true;
}

void __fastcall TParamMen::ClearAll(void)
{
//    RecoverTable();                //-- ���������� ���� ���� ������
//    Items->Clear();                //-- ���������� ������ �� ������
    ClearList();                   //-- �������� ������
}

//---------------------------------------------------------------------------
void __fastcall  TParamMen::SetComboBox(TComboBox *aComboBox, int Type = 0)
{
    // �� ������� ����������� ����� � ������ ������� ����� ����������
    aComboBox->Items->Clear();
    for (int i=0; i < UNITCount; i++)
        aComboBox->Items->Add(pSourceName->Strings[ParamUNITPresent[i].UnitID].c_str());
    aComboBox->ItemIndex = 0;
    aComboBox->Text = aComboBox->Items[0].GetText();
}

//---------------------------------------------------------------------------
int __fastcall  TParamMen::GetID(int Ind)
{
    if (Ind < UNITCount)
       return ParamUNITPresent[Ind].UnitID;
    else
       return 0;
}

int __fastcall  TParamMen::GetUnitPosInParamRecord(int Ind)
{
    if (Ind < UNITCount)
       return ParamUNITPresent[Ind].UnitPosInParamRecord;
    else
       return 0;
}

//---------------------------------------------------------------------------
bool __fastcall TParamMen::GetUnitCount()
{
    return GetUnitCountOpig();
}

bool __fastcall TParamMen::ParamIs_Ovens(int Inx)
{
    int  ID = GetID(Inx);
    return ((ID >= eZone1)&&(ID <= eZone5));
//    return false;
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//-- ��������� �� �������
bool sControlParam::LoadFromRegistry(bool SetDefaultOnError)
{
    char* cKey = "ControlParam";
    //-- ������ �� ������� ��������� ������������ ���������� ����
    if (!GetRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam)))
    {
        if (SetDefaultOnError)
        {
            SetDefault();
            return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam));
        }
        else return false;
    }
    return true;
}

//-- ��������� � ������
bool sControlParam::StoreToRegistry()
{
    char* cKey = "ControlParam";
    return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam));
}

//-- ��������� �� �������
bool sOptions::LoadFromRegistry(bool SetDefaultOnError)
{
    char* cKey = "Options";
    //-- ������ �� ������� ��������� ������������ ���������� ����
    if (!GetRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions)))
    {
        if (SetDefaultOnError)
        {
            SetDefault();
            return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions));
        }
        else return false;
    }
    return true;
}

//-- ��������� � ������
bool sOptions::StoreToRegistry()
{
    char* cKey = "Options";
    return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions));
}



//---------------------------------------------------------------------------
//      ������ ���������� ����������
//---------------------------------------------------------------------------

//------------------------------------------------------------------------------
/*
char *ParamName[] =
{
    "����������� �������� ����, ����.C",
    "����������� �������, ����.C",
    "��� �������, ��",
    "�������� ������������ CO",
    "���������� ��������",
    "���������� ���������, %"
};
*/
/*
int iObjectId[MAX_OBJECT]=
{
    eZone1,
    eZone2,
    eZone3,
    eEndoGen1,
    eEndoGen2,
    eEndoGen3
};
*/

//---------------------------------------------------------------------------
//      ������ ��������
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� CXT �� ������ double
//---------------------------------------------------------------------------
/*
int* __fastcall CXTFuncData(double* Ptr, int Mul, int Count)
{
    static int *p_int;
    p_int = new int[Count];

    for (int i=0;i<Count;i++)
        *(p_int+i) = (int)((*(Ptr+i))*(double)Mul);

    return p_int;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� ���������� �������� ������� �� ������� � ���������
//---------------------------------------------------------------------------
//-- Interval - ����� ������ ����������� � ��

int __fastcall SamplesOfPeriod (TDateTime StartTime, TDateTime EndTime, int Interval)
{
    double delta = (double)(EndTime - StartTime);
    if (delta < 0) return 0;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    if ((double)TimeStep == 0) return 0;
    return (int) (delta/(double)TimeStep);
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������ ��������� ����� � ����������� ��������� "��� ������"
//---------------------------------------------------------------------------
/*
bool __fastcall CreateUnknowParamList(TList *ParamList, int Count)
{
    for (int i = 0;i<Count; i++)
    {
        sArmParam *p = new sArmParam[MAX_OBJECT];
        if (!p) return false;
        memset(p,0,sizeof(sArmParam) * MAX_OBJECT);
        for (int j=0;j<MAX_OBJECT; j++) (p + j)->Source = UNKNOWN_DATA;
        ParamList->Add (p);
    }
    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������/���������� ���������� ������� ������ ��� ������� (������)
//---------------------------------------------------------------------------
/*
bool __fastcall SetDataParamList(TList *ParamList, int Idx, sArmParam* one_param, int ObjectIdx)
{
    sArmParam* list_param;

    //-- ������� ����� ����� ���������� (��� ���� ��������)
    list_param = (sArmParam*)ParamList->Items[Idx];
    if (!list_param) return false;

    memcpy((list_param + ObjectIdx),one_param,sizeof(sArmParam));

    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� ������� ��� ���������� �� ������� � ���������
//---------------------------------------------------------------------------
bool __fastcall GetIndexFromTime(sArmParam* one_param,TDateTime StartTime,int Interval, int &Idx)
{
    if (Interval == 0) return false;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    double dIdx = (double)(one_param->Time - StartTime) / (double)TimeStep;
    if (dIdx < 0) return false;
    else Idx = (int)(dIdx+0.4999);
    return true;
}

//---------------------------------------------------------------------------
//-- ��������� ���������� ������� ��� ����� ���������� �� ������� � ���������
//---------------------------------------------------------------------------
bool __fastcall GetIndexFromOptimTime(sOptimalParam* optim_param,TDateTime StartTime,int Interval, int &Idx)
{
    if (Interval == 0) return false;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    double dIdx = (double)(optim_param->Time - StartTime) / (double)TimeStep;
    if (dIdx < 0) return false;
    else Idx = (int)dIdx;
    return true;
}

namespace Parammen
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TParamMen)};
         RegisterComponents("Fx", classes, 0);
    }
}


