//---------------------------------------------------------------------------
#ifndef TFGraphH
#define TFGraphH
//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>

//---------------------------------------------------------------------------
//-- ������������ ����������� ��������� ������� -----------------------------
enum eArrowDirect
{
    eToTop,
    eToBottom,
    eToLeft,
    eToRight
};

//---------------------------------------------------------------------------
//-- ��������� ������� ------------------------------------------------------
class PACKAGE TFGraph :  public Controls::TGraphicControl
{
private:
    int     FBorderWidth;       //-- ������ �������
/*
    TColor  FBgColor;           //-- ��� �������
    TColor  FAreaColor;         //-- ���� ������� �������
    TColor  FBorderColor;       //-- ���� ������� �������
    TColor  FLineColor;         //-- ���� ����� �������

    int     FArrowWidth;        //-- ������ �������
    int     FArrowLength;       //-- ����� �������
    int     FLineWidth;         //-- ������ ����� ��������� � �����

    int     FAreaSpace;         //-- ������ �� ������� �������

    AnsiString FCaption;        //-- ��������� �������

    TFont*   FCaptionFont;       //-- ����� ��������� �������
    TFont*   FLabelFont;         //-- ����� ����� �������
    bool    FArrowDraw;         //-- ������� ��������� �������

    //void __fastcall DrawArrow(eArrowDirect Dir, int X, int Y);
    //void __fastcall MyPaint(System::TObject* Sender);

protected:
  */
public:

	__fastcall TFGraph(Classes::TComponent* AOwner);
	//__property Font ;

	__fastcall ~TFGraph(void) { };
    virtual void __fastcall Paint(void);

__published:
    __property int BorderWidth = {read=FBorderWidth, write=FBorderWidth, default=2};
};

//---------------------------------------------------------------------------
#endif
