//---------------------------------------------------------------------------


#pragma hdrstop

#include "Clap_Pipe.h"
#include "Params.h"
#include "Link.h"

//������ ������� ����������� ��� ������� ���
short Addr_Array[6] = {
   SET_E_REGUL_ADDRESS_1,
   SET_E_REGUL_ADDRESS_2,
   SET_E_REGUL_ADDRESS_3,
   SET_E_REGUL_ADDRESS_4,
   SET_E_REGUL_ADDRESS_5,
   SET_E_REGUL_ADDRESS_6
};

char * ArrayRegText[] = {
  "�����",
  "������",
  "����"
};


//@@TClap_Pipe Clap_Pipe[MAX_OBJECT]; // ��������� �� �����/�������

TSpeedButton *   TClap_Pipe::More_InControl        = NULL;
TSpeedButton *   TClap_Pipe::Less_InControl        = NULL;
TLabel *         TClap_Pipe::ReginHandleInControl  = NULL;//ControlForm->Label_H;
TLabel *         TClap_Pipe::ReginAutoInControl    = NULL;//ControlForm->Label_A;
TPanel *         TClap_Pipe::PanelControl          = NULL;//ControlForm->Panel_Control;
TPanel *         TClap_Pipe::PanelControlParamName = NULL;
TrxSwitch_  *    TClap_Pipe::rxSwitch_REGIM        = NULL;//ControlForm->rxSwitch_REG;
TEdit    *       TClap_Pipe::Edit_EDS_Value        = NULL;//ControlForm->Edit_EDS_Value;
TClap_Pipe     * TClap_Pipe::ActiveClap_Pipe       = NULL;
unsigned short   TClap_Pipe::ControlBits           = 0;        //-- ������������� ��� ���������� ��� �����������
unsigned short   TClap_Pipe::FRegimCommon          = 0;
unsigned short   TClap_Pipe::GasProbeClapanStatus  = 0xffff; //��� �������

//------------------------------------------------------------------------------

//-- ���������
int  __fastcall TClap_Pipe::Regulator(float aV_Zad,float V_Tek){
   if(Regim == rAUTO){
      if(Options.RegulTYPE == 1){
          float  Delta = aV_Zad - V_Tek;
          float  A = V_Tek - V_Tek_Old;
          float Delta_A = A - A_Old;
          A_Old = A;
          V_Tek_Old = V_Tek;
          Sum +=Delta;
          int Res =  Delta * FKp + Sum * FKi - Delta_A*FKd;
          if(abs(Res) > 255){
             Sum -=Delta;
             if(Res>0) Res = 255;
             else Res = -255;
          }

          if(abs(Res) < EPS)
              Neitral_On_Off = sToNone;
          else if(Res > 0) Neitral_On_Off = sToMore;
          else Neitral_On_Off = sToLess;
  //        TClap_Pipe::ControlBits =  Neitral_On_Off
          ControlBits = (ControlBits & (~(0x3 << (Index*2)))) | ((Neitral_On_Off  & 0x3) << (Index*2));
          return Res;
      }else{
          short V = ControlBits >> (Index*2) & 0x3; // ��������� ��� ��������� ����������
          if(V == 0)
              Neitral_On_Off = sToNone;
          else if(V == 1) Neitral_On_Off = sToMore;
          else Neitral_On_Off = sToLess;
//           ReadMORE_LESS_Dll (&ControlBits);
      }
   }
   return 0;
}

//------------------------------------------------------------------------------
__fastcall TClap_Pipe::TClap_Pipe( float aKp,float aKi,float aKd,bool aFl_Reg,
                                  int aIndex,
                                  bool aIsOVEN,
                                  TLabel *Regim_Value,
                                  TImage *aMore_InMain,
                                  TImage *aLess_InMain

){
   FKp = aKp;  FKi = aKi;  FKd = aKd;
   FFl_Reg = aFl_Reg;
   Index = aIndex;
   IsOVEN = aIsOVEN;
   AdjustingParam = IsOVEN?5:2;
   V_Tek_Old = A_Old = Sum = 0;
//   FlameUnit = aFlameUnit;
   ReginInMain = Regim_Value;
   More_InMain = aMore_InMain;
   Less_InMain = aLess_InMain;
//   More_InMain->Width = 32;
//   Less_InMain->Width = 32;
   FRegim = rHANDLE;
   More_InMain->Visible = false;
   Less_InMain->Visible = false;
   ReginInMain->Caption = ArrayRegText[FRegim];
   Caption =  "���������� " + AnsiString(IsOVEN?"��":"���");
//   ShowRegul(sToNone);
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetUnitType(bool aIsOVEN)
{
   IsOVEN = aIsOVEN;
   AdjustingParam = IsOVEN?5:2;
   Caption =  "���������� " + AnsiString(IsOVEN?"��":"���");
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::RegimStory(short value)
{
  if(FRegim != value)
  {
      FRegimCommon &= ~(1 << Index );
      FEnable = value != rPULT;
      FRegim = value;
//      More_InMain->Visible = false;
//      Less_InMain->Visible = false;
//      ReginInMain->Caption = ArrayRegText[FRegim];
  }
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetRegim(short value)
{
  if(FRegim != value){
//    if(Index == MainUnit->FlameList->CurrentUnitIndex )
      if (value == rPULT){
//         if(Is_Current)
            PanelControl->Visible = false;
         FRegimCommon &= ~(1 << Index );
//         WriteREG_Dll (0, Index);
      }else if (value == rHANDLE){
//         if(Is_Current)
         {
             PanelControl->Visible = true;
             PanelControlParamName->Caption = Caption;
             ReginHandleInControl->Visible = true;
             ReginAutoInControl->Visible = false;
             More_InControl->Enabled = true;
             Less_InControl->Enabled = true;
             More_InControl->Visible = true;
             Less_InControl->Visible = true;
         }
         FRegimCommon &=  ~(1 << Index );
      }else if (value == rAUTO){
//         if(Is_Current)
         {
             PanelControl->Visible = true;
             More_InControl->Enabled = false;
             Less_InControl->Enabled = false;
             ReginHandleInControl-> Visible = false;
             ReginAutoInControl-> Visible = true;
         }
         FRegimCommon |= (1 << Index );
//         WriteREG_Dll (1, Index);
      }
      WriteREG_Dll (FRegimCommon);

      FRegim = value;
      More_InMain->Visible = false;
      Less_InMain->Visible = false;
      ReginInMain->Caption = ArrayRegText[FRegim];
  }
}

//-- ��������/��������� ������ �� ������� �����
void __fastcall  TClap_Pipe::ProbeClapan(bool On_Off)
{
      if(On_Off == true)
         GasProbeClapanStatus |= (1 << Index );
      else
         GasProbeClapanStatus &=  ~(1 << Index );
      WriteProbe_Dll(GasProbeClapanStatus);

}

//------------------------------------------------------------------------------
short __fastcall TClap_Pipe::GetRegim()
{
        return FRegim;
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetEnable(bool value)
{
  if(FEnable!= value){
     if(value == true){
        SetRegim(rHANDLE);
     }else{
        SetRegim(rPULT);
     }
     FEnable  = value;
     rxSwitch_REGIM->StateOn = false;
  }
//   WriteREG_Dll (0, Index);
}

//------------------------------------------------------------------------------
bool __fastcall TClap_Pipe::GetEnable()
{
        return FEnable;
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetAbove()
{
   ShowRegul(sToMore);
   if(FRegim != rAUTO){
     TClap_Pipe::ControlBits = (TClap_Pipe::ControlBits & ((~3) << (Index * 2))) |(((1) << (Index * 2)));
     if(WriteMORE_LESS_Dll (TClap_Pipe::ControlBits));
   }
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetBelow()
{
   ShowRegul(sToLess);
   if(FRegim != rAUTO){
     TClap_Pipe::ControlBits = (TClap_Pipe::ControlBits & ((~3) << (Index * 2))) |(((2) << (Index * 2)));
     if(WriteMORE_LESS_Dll (TClap_Pipe::ControlBits));
   }
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetNone()
{
   ShowRegul(sToNone);
   if(FRegim != rAUTO){
     TClap_Pipe::ControlBits = TClap_Pipe::ControlBits & ((~3) << (Index * 2));
     if(WriteMORE_LESS_Dll ( 0));//TClap_Pipe::ControlBits));
   }
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetValueToController()
{
   WriteDataDll (Addr_Array[Index], &FEDS, 2);
}

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::ShowRegul(short aReg)
{
   if(FRegim == rAUTO)
//!!       if(Index == MainUnit->FlameList->CurrentUnitIndex )
       if(aReg == sToLess){
          More_InControl->Visible = false;
          Less_InControl->Visible = true;
       }else if(aReg == sToMore){
          More_InControl->Visible = true;
          Less_InControl->Visible = false;
       }else {
          More_InControl->Visible = false;
          Less_InControl->Visible = false;
       }

   //�� ������� � ����� ������
   if(aReg == sToLess){
      More_InMain->Visible = false;
      Less_InMain->Visible = true;
   }else if(aReg == sToMore){
      More_InMain->Visible = true;
      Less_InMain->Visible = false;
   }else{
      More_InMain->Visible = false;
      Less_InMain->Visible = false;
   }
}

//------------------------------------------------------------------------------
int __fastcall TClap_Pipe::GetStat(){
             return FClapanStat;
};

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::SetStat(int aSt){
       FClapanStat = aSt;
       if(aSt == sToNone){               //-- ����������� ���������  = 0
           SetNone();
       }else if(aSt == sToMore){              //-- ������� �� ��������    = 1
           SetAbove();
       }else if(aSt == sToLess){             //-- ������� �� ��������    = 2
           SetBelow();
       }else{
       }
};

//------------------------------------------------------------------------------
void __fastcall TClap_Pipe::ActivateControl(){
      if (FRegim == rPULT){
         PanelControl->Visible = false;
      }else if (FRegim == rHANDLE){
         PanelControl->Visible = true;
         PanelControlParamName->Caption = Caption;
//~~         PanelControlParamNameArr[Index];

         ReginHandleInControl->Visible = true;
         ReginAutoInControl->Visible = false;
         More_InControl->Visible = true;
         Less_InControl->Visible = true;

         More_InControl->Enabled = true;
         Less_InControl->Enabled = true;
         rxSwitch_REGIM->StateOn = false;

      }else if (FRegim == rAUTO){
         PanelControlParamName->Caption = Caption;
         PanelControl->Visible = true;
         More_InControl->Visible = false;
         Less_InControl->Visible = false;
         More_InControl->Enabled = false;
         Less_InControl->Enabled = false;
         ReginHandleInControl-> Visible = false;
         ReginAutoInControl-> Visible = true;
         rxSwitch_REGIM->StateOn = true;
      }
      if(IsOVEN)   // ��� ����� ��
         Edit_EDS_Value->Text = AnsiString::FloatToStrF((double)EDS/1000,AnsiString::sffFixed,4,3);
      else
         Edit_EDS_Value->Text = AnsiString::FloatToStrF(EDS,AnsiString::sffFixed,4,0);
      ActiveClap_Pipe = this;

}
//------------------------------------------------------------------------------
bool TClap_Pipe::Clap_PipeIsActive()
{
  return ActiveClap_Pipe == this;
//  return ControlForm->Visible && (FlameList->CurentFlameUnit->Clap_Pipe == this);
}
//------------------------------------------------------------------------------

void __fastcall TClap_Pipe::SetEDS(unsigned short value)
{
    if(FEDS != value)
    {
        FEDS = value;
        if(Clap_PipeIsActive())
        {
            if(IsOVEN)   // ��� ����� ��
               Edit_EDS_Value->Text = AnsiString::FloatToStrF((double)EDS/1000,AnsiString::sffFixed,4,3);
            else
               Edit_EDS_Value->Text = AnsiString::FloatToStrF(EDS,AnsiString::sffFixed,4,0);
        }
        Options.All_Limits[Index].EReg = FEDS;
                //-- �������� � ����������
//                SetValueToController();
    }
    SetValueToController();
}

void __fastcall TClap_Pipe::EDS_Init(WORD aEDS)
{
    FEDS = aEDS;
    SetValueToController();
}    

//---------------------------------------------------------------------------
unsigned short __fastcall TClap_Pipe::GetEDS()
{   return FEDS;    }
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#pragma package(smart_init)
