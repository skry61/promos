//---------------------------------------------------------------------------
#ifndef GrapherH
#define GrapherH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>

//-- �������� �������� "��� ������"
#define UNKNOWN_DATA        (int)0xFFFFFFFF
#define EMPTY_DATA          (int)0x8FFFFFFF
#define UNKNOWN_DATA_FLOAT  (float)-1.0

class TScrollerPanel;
//---------------------------------------------------------------------------
//-- ������������ ����������� ��������� ������� -----------------------------
enum eArrowDirect
{    eToTop,    eToBottom,   eToLeft,    eToRight   };
//---------------------------------------------------------------------------
class PACKAGE TGrapher : public TPaintBox 
{

private:
    TColor  FBgColor;           //-- ��� �������
    TColor  FAreaColor;         //-- ���� ������� �������
    TColor  FBorderColor;       //-- ���� ������� �������
    int     FBorderWidth;       //-- ������ �������
    int     FArrowWidth;        //-- ������ �������
    int     FArrowLength;       //-- ����� �������
    int     FAreaSpace;         //-- ������ �� ������� �������

    AnsiString FCaptionFont;    //-- ����� ���������
    int     FCaptionSize;       //-- ������ ���������
    bool    FArrowDraw;         //-- ������� ��������� �������

    void __fastcall SetValueDigits (int Digits);
    void __fastcall SetValuePrecision (int Precision);
    void __fastcall SetValueFormat (TFloatFormat aFormat);

protected:
    int             FValuePrecision;
    int             FValueDigits;
    TFloatFormat    FValueFormat;

    bool  AlterPaint;       //-- ������� ��������� ������� �� �������������� �����
    TRect AlterRect;        //-- ���������� ������������� �����
    TCanvas* AlterCanvas;   //-- ��������������� ���� �����
    Graphics::TBitmap *Bitmap;//������������� ����� ���������
    TCanvas *WorkCnv;       //-- ������� �����
    TRect    WorkRect;      //-- ����������  ����� �������


//-- ��� ������ �� TXGrapher � TTimeGrapher
protected:

Classes::TNotifyEvent FOnMarker;

    bool            FMarker;
    int             FH_Marker;
    TDateTime       FMarkerTime;
    int             MarkerY;
    int             MarkerLinesCount;
    char**          MarkerLines;
    TColor          FMarkColor;
    TColor          FMarkerColor;
// ���������� �������
    char  Msg1[50],Msg2[50];
    char* FMarkerLines[4];

private:
    void __fastcall SetMarker(bool aFM){FMarker = aFM;IsMarker = false;};
    

public:
    bool            IsMarker;
    int             MarkerPoint;
    void __fastcall SetMarkerLines (int Count, char** Lines);
    void __fastcall SetAlterPaint(TCanvas *cnv,TRect R);
    void __fastcall ResetAlterPaint(void);


protected:
    // ������ ������� � ���� �����
    int *p_int;                 //-- ������ ������� ��������� ������� �� ������ double
    int intCount;               //-- ����� ������� ��������� ������� �� ������ double
    TColor DFColor;             //-- ���� �����
    int    FW_Value;            //-- �������� ������ �������


   Classes::TNotifyEvent FOnPaint;

    void __fastcall SetCaption(AnsiString aCap);
    virtual void __fastcall LoadCanvas();   // ��������� ������� �����
    virtual void __fastcall FreeCanvas();   // ���������� ������� �����
    virtual void __fastcall FinishPaint(void);
    TCanvas * __fastcall StartPaint(void);
    
    

protected:
    virtual void __fastcall MyPaint(TCanvas *cnv, TRect R);
    virtual void __fastcall DrawLabel(TCanvas *cnv, TRect Rect);
    void __fastcall DrawMarker(TCanvas *cnv);
   
    virtual void __fastcall DrawArrow(TCanvas *cnv, eArrowDirect Dir, int X, int Y);

    virtual int __fastcall XonGrapher (int x_real);
    virtual int __fastcall YonGrapher (int y_real);
    int __fastcall XonGrapher (double x_real);
    int __fastcall YonGrapher (double y_real);
    virtual void __fastcall CalcMarkerParam (){};  //������
    virtual void __fastcall PrepareMarker(){};       //���������� �������
    
    

    TRect GraphRect;            //-- ������� �������
    TRect ClientRectEx;         //-- ������� �������� �������
    double kX,kY;

    int     FVMax,FVMin,FVStep;
    int     FHMax,FHMin,FHStep; // ����� ��������� �����, ����� ������, ��� �����
    int     FLineWidth;         //-- ������ ����� ��������� � �����
    TColor  FLineColor;         //-- ���� ����� �������

    AnsiString FCaption;        //-- ��������� �������
    TScrollerPanel  *SP;
    void __fastcall PrepareCalc(void);
    double GetkX(){return kX;}
    double GetkY(){return kY/1000;}
    TRect GetCR(){return GraphRect;}

    void __fastcall MyMouseDown(TObject *Sender,    TMouseButton Button, TShiftState Shift, int X, int Y);
    virtual void __fastcall SetHMax(int value){FHMax = value;};

public:
    __fastcall TGrapher(TComponent* Owner);
    __fastcall ~TGrapher();

    void __fastcall DrawSText(TCanvas *cnv, TColor tColor,int X, int Y, AnsiString aStr);
    void __fastcall DrawFunctionData(TColor tColor,int* FuncData, int Idx, int Count);

    void __fastcall SetFunctionData(TColor tColor,double* FuncData, int Idx, int Count);//���� ������
    int* __fastcall FuncDataToInt(double* Ptr, int Mul, int Count);
    void __fastcall SetScroolPanel(TScrollerPanel  *aSP){SP = aSP;};

    virtual void __fastcall Paint(void);
    __property double KoeffX = {read=GetkX};
    __property double KoeffY = {read=GetkY};
    __property TRect CoordRect = {read=GetCR};            //-- ������� �������

__published:


	__property Classes::TNotifyEvent OnPaint = {read=FOnPaint, write=FOnPaint};
	__property int CaptionSize = {read=FCaptionSize, write=FCaptionSize};
	__property AnsiString CaptionFont = {read=FCaptionFont, write=FCaptionFont};
	__property AnsiString Caption = {read=FCaption, write=SetCaption};

    __property TColor LineColor = {read=FLineColor, write=FLineColor, default=clBlack};
    __property TColor BgColor = {read=FBgColor, write=FBgColor, default=clWhite};
    __property TColor AreaColor = {read=FAreaColor, write=FAreaColor, default=clSilver};
    __property TColor BorderColor = {read=FBorderColor, write=FBorderColor, default=clBlack};

    __property int BorderWidth = {read=FBorderWidth, write=FBorderWidth, default=2};

	__property int ArrowWidth = {read=FArrowWidth, write=FArrowWidth, default=6};
	__property int ArrowLength = {read=FArrowLength, write=FArrowLength, default=10};
	__property int LineWidth = {read=FLineWidth, write=FLineWidth, default=1};
	__property int AreaSpace = {read=FAreaSpace, write=FAreaSpace, default=30};

	__property bool ArrowDraw = {read=FArrowDraw, write=FArrowDraw, default=true};

	__property int VMax = {read=FVMax, write=FVMax};
	__property int VMin = {read=FVMin, write=FVMin};
	__property int VStep = {read=FVStep, write=FVStep};

	__property int HMax = {read=FHMax, write=SetHMax};
	__property int HMin = {read=FHMin, write=FHMin};
	__property int HStep = {read=FHStep, write=FHStep};


//--------------------------
    __property Classes::TNotifyEvent OnMarker = {read=FOnMarker, write=FOnMarker};

    __property bool Marker = {read=FMarker, write=SetMarker, stored = true};
    __property TDateTime  MarkerTime = {read=FMarkerTime, write=FMarkerTime};
    __property TColor MarkColor = {read=FMarkColor, write=FMarkColor, default=clAqua};
    __property TColor MarkerColor = {read=FMarkerColor, write=FMarkerColor, default=clWhite};

    __property TFloatFormat ValueFormat = {read=FValueFormat, write=SetValueFormat, stored = true};
    __property int ValuePrecision = {read=FValuePrecision, write=SetValuePrecision, stored = true, default = 4};
    __property int ValueDigits = {read=FValueDigits, write=SetValueDigits, stored = true, default = 2};

};

//---------------------------------------------------------------------------
#endif
