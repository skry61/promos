//---------------------------------------------------------------------------
#ifndef AuthH
#define AuthH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include "Params.h"
#pragma link "Params"



//-- �������� ������ �������
AnsiString __fastcall AccessModeName ();

//---------------------------------------------------------------------------
class TLoginForm : public TForm
{
__published:	// IDE-managed Components
    TEdit *Edit2;
    void __fastcall BitBtn1Click(TObject *Sender);
    void __fastcall Edit2KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
    AnsiString Login;
    AnsiString Password;


public:		// User declarations
    __fastcall TLoginForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLoginForm *LoginForm;
extern eAccessMode     AccessMode;         //-- ����� �������

//---------------------------------------------------------------------------
#endif
