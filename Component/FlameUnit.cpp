//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include <winuser.h>

#pragma hdrstop

#include "FlameUnit.h"
#include "FlameList.H"
#pragma link "DbUnit_ev"
#include "FMath.h"
#include <math.h>


#pragma package(smart_init)

extern char * ArrayRegText[];

/*
char * ArrayRegText[] = {
  "�����",
  "������",
  "����"
};
*/
//------------------------------------------------------------------------------
void sControlUParam::SetDefault(void)
{
    CO  = CO_DEFAULT;
    Tp  = TP_DEFAULT;
    Td  = TD_DEFAULT;
    EDS = EDS_DEFAULT;
    TpAutoCtrl   = TP_AUTOCONTROL_DEFAULT;
    TdAutoCtrl   = TD_AUTOCONTROL_DEFAULT;
    EDSAutoCtrl  = EDS_AUTOCONTROL_DEFAULT;
    CgAutoCtrl   = CG_AUTOCONTROL_DEFAULT;
    BurnAutoCtrl = BURN_AUTOCONTROL_DEF;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void __fastcall TParamUnit::SetParamValue(double value)
{
        *FpValue = value;
}

//------------------------------------------------------------------------------
void __fastcall TParamUnit::SetValue(double value)
{
//    AnsiString  TmpV;
    if((FValue != value)||(*FpValue != value)||(FValue != *FpValue)) {
        *FpValue = value;
        FValue = value;
        Unit_Value->Caption = GetTextValue();//TmpV;
        //-- �������� ���������� ��������
        CheckLimits();
    }
}

//-- ��������� �������� ���������� ��������
bool __fastcall TParamUnit::CheckLimits(void)
{
    bool IsAutoCtrl = (AutoCtrl!= acHandl);
    if (IsAutoCtrl && Fl_LOW || !IsAutoCtrl && (Value < LimitLow))
    {
        Status = 1;
    }
    else if (IsAutoCtrl && Fl_HIGH || !IsAutoCtrl && (Value > LimitHigh))
    {
        Status = 2;
    }
    else{
       Status = 0;
    }
    return Status != 0;
}

//------------------------------------------------------------------------------
void __fastcall TParamUnit::Repaint(){
       double OldValue = *FpValue;
       *FpValue = - *FpValue;
       SetValue(OldValue);
}

//------------------------------------------------------------------------------
inline double __fastcall TParamUnit::GetValue()
{
    return *FpValue;
}

//------------------------------------------------------------------------------
void __fastcall TParamUnit::SetStatus(int value)
{
    if(FStatus != value) {
        FStatus = value;
        Owner->SetEvent(Limits[value],Owner->ObjectId,DoubleToComment(Value));
        if(FStatus == 0) FStatus = 4;
        TriggerPhasa();
    }
}
//------------------------------------------------------------------------------
int __fastcall TParamUnit::GetStatus()
{
    return FStatus;
}

//------------------------------------------------------------------------------
int TParamUnit::TriggerPhasa()
{
    //TODO: Add your source code here
  int Color1;
  int Color2;
    switch (FStatus)
    {
      case 0:
          return 0;
      case 1:
          Color1 = clBlue;
          Color2 = clWhite;
          break;

      case 2:
          Color1 = clRed;
          Color2 = clWhite;
          break;

      case 3:
          Color1 = clRed;
          Color2 = clBlack;
          break;
          
      case 4:
          if(TypeLabels >= eAc){
             Unit_Name->Color = ((TPanel*)Unit_Name->Parent)->Color;// clBtnFace;
             Unit_Name->Font->Color = clWindowText;
          }
          Unit_Value->Color = ((TPanel*)Unit_Name->Parent)->Color;// clBtnFace;
          Unit_Value->Font->Color = clWindowText;
          FStatus = 0;
          return 0;
      }
      if(TypeLabels >= eAc){
          Unit_Name->Color =(TColor) (Phasa?Color1:Color2);
          Unit_Name->Font->Color = (TColor) (Phasa?Color2:Color1);
      }
      Unit_Value->Color = (TColor) (Phasa?Color1:Color2);
      Unit_Value->Font->Color = (TColor) (Phasa?Color2:Color1);
      Phasa = !Phasa;
   return 1;
}

//------------------------------------------------------------------------------
TParamUnit::TParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId/*TypeLab */aTypeLabels,TFlameUnit * aOwner)
{
    //TODO: Add your source code here
    TypeLabels = aTypeLabels;
    Owner      = aOwner;
    FValue = 0;
    FVisible = true;
    FLimitHigh = NULL;   // �������� ������ ��� ������ ����������
    FLimitLow = NULL;    // ������  -----//-------
    FReg = NULL;         // �������� ������������� ���������
    FpValue = &FValue;
    FSize = FontSize;

    Unit_Name = new TLabel(MainPanel);        //
    CompPL(Unit_Name,taLeftJustify,true,alNone,MainPanel);
    ConstrPL(Unit_Name,Left,Top,Widht,Heigth,Name);
    switch(TypeLabels){
    case eCg:
       Limits[0] = eCgNormal;
       Limits[1] = eCgLowLimit;
       Limits[2] = eCgHighLimit;
       CompPANEL(Unit_Name,FontSize);
       break;

    case eAc:
       Limits[0] = eAcNormal;
       Limits[1] = eAcLowLimit;
       Limits[2] = eAcHighLimit;
       Limits[3] = eAc2HighLimit;
       CompPANEL(Unit_Name,FontSize);
       break;
    case eEDS:
       Limits[0] = eENormal;
       Limits[1] = eELowLimit;
       Limits[2] = eEHighLimit;
       CompLABEL(Unit_Name,FontSize);
       break;
    case eTp:
       Limits[0] = eTpNormal;
       Limits[1] = eTpLowLimit;
       Limits[2] = eTpHighLimit;
       break;
    case eTd:
       Limits[0] = eTdNormal;
       Limits[1] = eTdLowLimit;
       Limits[2] = eTdHighLimit;
       break;
    }

    Unit_Value = new TLabel(MainPanel);        //
    CompPL(Unit_Value,taLeftJustify,true,alNone,MainPanel);
//    int MainForm->Canvas->TextWidth(Unit_Name->Caption);
    ConstrPL(Unit_Value,Left + Unit_Name->Width-1,Top,Widht,20,"???");
    switch(TypeLabels){
    case eCg:
    case eAc:
       CompPANEL(Unit_Value,FontSize);
       break;
    case eEDS:
       CompLABEL(Unit_Value,FontSize);
       break;
    case eTp:
    case eTd:
       break;
    }

}

//------------------------------------------------------------------------------
TParamUnit::~TParamUnit()
{
  // ��������� ��� �������� �����
}

//------------------------------------------------------------------------------
int TParamUnit::SetTop(int aTopDisp)
{
      Unit_Name->Top   = aTopDisp;              //
      Unit_Value->Top  = aTopDisp;             //
      Unit_Name->Left  = 10;              //
      Unit_Value->Left = 60;             //

//      int value  = (Unit_Name->Font->Height > 0)?
//         Font.Height = -Font.Size * Font.PixelsPerInch / 72
      return abs(Unit_Name->Font->Height) + abs(Unit_Name->Font->Height)/4;
}


//------------------------------------------------------------------------------
int TParamUnit::DispTop(int aTopDisp)
{
      Unit_Name->Top += aTopDisp;              //
      Unit_Value->Top += aTopDisp;             //
      return abs(Unit_Name->Font->Height);

}


//------------------------------------------------------------------------------
void __fastcall TParamUnit::SetVisible(bool value)
{
    if(FVisible != value) {
        FVisible = value;
        Unit_Value->Visible = value;
        Unit_Name->Visible = value;
    }
}

//------------------------------------------------------------------------------
void __fastcall TParamUnit::AcceptPSW(unsigned char psw)
{
    Fl_LOW  = psw & 2;// ���������� ������� � ���������� ������ ������� ���������
    Fl_HIGH = psw & 1;// ���������� ������� � ���������� ������� ������� ���������
}

//------------------------------------------------------------------------------
int  __fastcall TParamUnit::GetFontSize()
{
    return FSize;
}

//------------------------------------------------------------------------------
void __fastcall TParamUnit::SetFontSize(int fs) // ������ ������
{
     FSize = fs;

    switch(TypeLabels){
    case eCg:
    case eAc:
    case eEDS:
       Unit_Name->Font->Size = fs;
       break;
    }

//    Unit_Value = new TLabel(MainPanel);        //
//    CompPL(Unit_Value,taLeftJustify,true,alNone,MainPanel);
//    ConstrPL(Unit_Value,Left + Unit_Name->Width-1,Top,Widht,20,"???");
    switch(TypeLabels){
    case eCg:
    case eAc:
    case eEDS:
       Unit_Value->Font->Size = fs;
       break;
    }

/*
    switch(TypeLabels){
    case eCg:
    case eAc:
       CompPANEL(Unit_Value,FontSize);
       break;
    case eEDS:
       CompLABEL(Unit_Value,FontSize);
       break;
    case eTp:
    case eTd:
       break;
    }
*/
}

//---------------------------------------------------------------------------
AnsiString __fastcall TParamUnit::GetNewName()
{
   return Unit_Name->Caption;
}

//---------------------------------------------------------------------------
void  __fastcall TParamUnit::SetNewName(AnsiString n)
{
   Unit_Name->Caption = n;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TTpParamUnit::SetLimitAddr(sLimits *aLimits)
{
    FLimitHigh = &aLimits->TpHigh;
    FLimitLow  = &aLimits->TpLow;
    FReg       = &aLimits->TpReg;
}

//---------------------------------------------------------------------------
void   __fastcall TTpParamUnit::SetLimLValue(double value)
{
    if(FLimitLow)
      *FLimitLow = value;
}

//---------------------------------------------------------------------------
double __fastcall TTpParamUnit::GetLimLValue()
{
    if(FLimitLow)
       return *FLimitLow;
    else
       return 0;
}

//---------------------------------------------------------------------------
void   __fastcall TTpParamUnit::SetLimHValue(double value)
{
    if(FLimitHigh)
       *FLimitHigh = value;
}

//---------------------------------------------------------------------------
double __fastcall TTpParamUnit::GetLimHValue()
{
    if(FLimitHigh)
       return *FLimitHigh;
    else
       return 0;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TTdParamUnit::SetLimitAddr(sLimits *aLimits)
{
    FLimitHigh = &aLimits->TdHigh;
    FLimitLow  = &aLimits->TdLow;
    FReg       = NULL;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TEParamUnit::SetLimitAddr(sLimits *aLimits)
{
    FLimitHigh = &aLimits->EHigh;
    FLimitLow  = &aLimits->ELow;
    FReg       = &aLimits->EReg;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TCgParamUnit::SetLimitAddr(sLimits *aLimits)
{
    FLimitHighF = &aLimits->CgHigh;
    FLimitLowF  = &aLimits->CgLow;
    FRegF       = &aLimits->Cg_AcReg;

    float   Ac2High;                    //-- ������ ������� ������ ��� ��
    float   AcHigh;                     //-- ������� ������ ��� ��
    float   AcLow;                      //-- ������ ������ ��� ��

}

//---------------------------------------------------------------------------
void   __fastcall TCgParamUnit::SetLimLValue(double value)
{
    if(FLimitLowF)
       *FLimitLowF = value;
}

//---------------------------------------------------------------------------
double __fastcall TCgParamUnit::GetLimLValue()
{
    if(FLimitLowF)
       return *FLimitLowF;
    else
       return 0;
}

//---------------------------------------------------------------------------
void   __fastcall TCgParamUnit::SetLimHValue(double value)
{
    if(FLimitHighF)
       *FLimitHighF = value;
}

//---------------------------------------------------------------------------
double __fastcall TCgParamUnit::GetLimHValue()
{
    if(FLimitHighF)
       return *FLimitHighF;
    else
       return 0;
}

//---------------------------------------------------------------------------
//-- ��������� �������� ���������� ��������
//-- �������� ��
bool __fastcall TCgParamUnit::CheckLimits(void)
{
    // ����� ����� ���������� ��� ������ ����
    unsigned char ccw = Owner->CCW;
    ccw &= ~(CG_LOW_CCW | CG_HIGH_CCW);

    if (Value < LimitLow)
    {
        ccw |= CG_LOW_CCW;
        Status = 1;
    }
    else if (Value > LimitHigh)
    {
        ccw |= CG_HIGH_CCW;
        Status = 2;
    }
    else
       Status = 0;
    Owner->CCW_ = ccw;
    return Status != 0;
    
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TAcParamUnit::SetLimitAddr(sLimits *aLimits)
{
    FLimitHighF2 = &aLimits->Ac2High;
    FLimitHighF  = &aLimits->AcHigh;
    FLimitLowF   = &aLimits->AcLow;
}

//---------------------------------------------------------------------------
//-- ��������� �������� ���������� ��������
//-- �������� Ac
bool __fastcall TAcParamUnit::CheckLimits(void)
{
    // ����� ����� ���������� ��� ������ ����
    unsigned char ccw = Owner->CCW;
    ccw &= ~(AC_LOW_CCW | AC_HIGH_CCW | AC2_HIGH_CCW);

    if (Value < LimitLow)
    {
        // ���������� �������� ��������������
        ccw |= AC_LOW_CCW;
        Status = 1;
    }
    else if (FLimitHighF2 && (Value > *FLimitHighF2))
    {
        // ���������� �������� ��������������
        if(AutoCtrl == 0)
            Owner->DoBurnSensor = true;
        ccw |= AC2_HIGH_CCW;
        Status = 3;
    }
    else
    {
        if((AutoCtrl == 0) &&(Owner->DoBurnSensor == true))
            Owner->DoBurnSensor = false;
        if (Value > LimitHigh)
        {
            ccw |= AC_HIGH_CCW;
            Status = 2;
        }
        else
            Status = 0;
    }
    Owner->CCW_ = ccw;
    return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
TRegimUnit::TRegimUnit(TLabel *Name,TImage *Image,TImage *Image1,int Reg)
{
    FRegim = 99;
    Regim_Value = Name;
    More_InMain = Image;
    Less_InMain  = Image1;
    SetRegim(Reg);
}

//------------------------------------------------------------------------------
void __fastcall TRegimUnit::SetRegim(int value)
{
    if((FRegim != value)&&(value <= rAUTO)) {
        FRegim = value;
        Regim_Value->Caption = ArrayRegText[FRegim];
    }
}
//------------------------------------------------------------------------------
int __fastcall TRegimUnit::GetRegim()
{
    return FRegim;
}

void __fastcall TRegimUnit::SetNewImage(int ImgIndex)
{
     More_InMain->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16000 + ImgIndex));
     Less_InMain->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16001 + ImgIndex));
}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


int TFlameUnit::FObjectIdValue = 0;   // �������� �����

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
bool Assigned(void *p)
{
  return p != NULL;
}

//---------------------------------------------------------------------------
static inline void ValidCtrCheck(TFlameUnit *)
{
    new TFlameUnit(NULL);
}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::RepaintUnit(){
    for (int i = 0; i< ParamList->Count;i++){
       TParamUnit *ParamUnit = (TParamUnit*)ParamList->Items[i];
       if(ParamUnit != NULL)
          ParamUnit->Repaint();
    }
}

//------------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TFlameUnit::TFlameUnit(TComponent* Owner)
    : TPanel(Owner)//TPaintBox(Owner)
{
    Caption = ' ';
    FObjectId = FObjectIdValue;
    FObjectIdValue++;                               // ����� ������� �� ���������
    FlameList = NULL;                               //������� ��������
    ParamList = new TList;

    Width = xWight;
    Font->Style = TFontStyles()<< fsBold;
    FullRepaint = true;
    BorderWidth = 3;
    //---------------------------------------


    Height = FUnit_Type == uOVEN? HeightP : HeightE;
    //---------------------------------------


    //������ "����"
    TypeUnitPanel = new TPanel(this);  // ������ ���� (���� / �������������) �����.
    CompPL(TypeUnitPanel,taCenter,false,alTop,this);
    ConstrPL(TypeUnitPanel,CommonX,NamePanelY,CommonWight,NamePanelHeight,Caption);
    TypeUnitPanel->BevelInner = bvLowered;
    TypeUnitPanel->BorderWidth = 2;
    CompPANEL(TypeUnitPanel,12);

    //---------------------------------------
    Bvl = new TBevel(this);         // ����� �����������
    Bvl->Parent = this;
    Bvl->Top = FUnit_Type == uOVEN?BevelYp:BevelYe; Bvl->Left = CommonX;
    Bvl->Width = CommonWight; Bvl->Height = 2;
    //---------------------------------------

    // Label ���  Tp Td
//    ParamList->Add (TpValue = NULL);
//..    ParamList->Add ( TpValue =  new TTpParamUnit(this,"����������� ����:",L_TpY+Bvl->Top ,L_TpX,Width - L_TpX*2,L_TpH, 14, eTp,this));
//..    ParamList->Add ( TdValue =  new TTdParamUnit(this,"����������� �������:",L_TdY+Bvl->Top ,L_TdX,Width - L_TdX*2,L_TdH, 14, eTd,this));
    ParamList->Add ( TpValue =  new TTpParamUnit(this,"� ����: ",L_TpY+Bvl->Top ,L_TpX,Width - L_TpX*2,L_TpH, 14, eTp,this));
    ParamList->Add ( TdValue =  new TTdParamUnit(this,"� �������: ",L_TdY+Bvl->Top ,L_TdX,Width - L_TdX*2,L_TdH, 14, eTd,this));
    ParamList->Add ( EdsValue = new TEParamUnit (this,"��� �������: ",L_EdsY+Bvl->Top ,L_EdsX,Width - L_EdsX*2,L_EdsH, 14, eEDS,this));
    // ��2
    ParamList->Add (CoValue = new TEParamUnit (this,"CO2 � ����: ",L_EdsY+3+Bvl->Top ,L_EdsX+3,Width - L_EdsX*2,L_EdsH, 14, eEDS,this));
    CoValue->Visible = false;
//    NULL);
    // Label Ac                        -
    ParamList->Add ( Ac = new TAcParamUnit(this,"Ac = ",FUnit_Type == uOVEN?L_2Y:L_1Y ,AcX,CommonWight,L_12H, 23, eAc,this));
    // Label ��
//    if(FUnit_Type == uOVEN){
    ParamList->Add ( Cg = new TCgParamUnit(this,"�� = ",L_2Y ,CgX,CommonWight,L_12H, 23, eCg,this));
    Cg->Visible = false;
//    }else {
//       Cg = NULL;
//       ParamList->Add (NULL);
//    }

    //---------------------------------------
    TPanel *RegimPanel = new TPanel(this);     // ������ ������(����/������)
    CompPL(RegimPanel,taCenter,false,alBottom,this);
    ConstrPL(RegimPanel,CommonX,BotPanelY+Bvl->Top,CommonWight,NamePanelHeight,"");
    RegimPanel->BevelInner = bvLowered;
    RegimPanel->BevelOuter = bvNone;
    RegimPanel->BevelWidth = 1;
    RegimPanel->BorderStyle = bsNone;
    RegimPanel->BorderWidth = 2;
    CompPANEL(RegimPanel,12);

    //---------------------------------------
    TLabel *TmpLabel = new TLabel(this);
    CompL(TmpLabel,taLeftJustify,true,alNone);
    ConstrPL(TmpLabel,8,8,10,19,"�����:");

    TmpLabel = new TLabel(this);
    CompL(TmpLabel,taLeftJustify,true,alNone);
    ConstrPL(TmpLabel,80,8,100,19,"������");

    TImage *Image = new TImage(this);
    TImage *Image1 = new TImage(this);
//    ConstrPI(Image,184,2,32,16,"Close.bmp");
//    ConstrPI(Image1,184,16,32,16,"Open.bmp");
    ConstrPI(Image,184,2,32,16);
    ConstrPI(Image1,184,16,32,16);

//    Image->Picture->Bitmap->Handle = LoadBitmap(NULL,OBM_OLD_CLOSE);
    Image->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16000));
    Image1->Picture->Bitmap->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16001));

    Image->Visible = false;
    Image1->Visible = false;
    Image->Parent  = RegimPanel;
    Image1->Parent = RegimPanel;

    Regim = new TRegimUnit(TmpLabel,Image,Image1,rPULT);

    FClap_Pipe = new TClap_Pipe(10,1,0.1,true,
                                FObjectId,            //- �� ���������
                                IsOVEN,    //- ������ "��" ��� "���"
                                Regim->Regim_Value,
                                Regim->More_InMain,
                                Regim->Less_InMain
                                );

    FIs_profile = false;                               // ������� �� �����������

    FCtrlTp       = &vFCtrlTp;
    FCtrlTd       = &vFCtrlTd;
    FCtrlCO       = &vFCtrlCO;
    FCtrlEDS      = &vFCtrlEDS;
    FTpAutoCtrl   = &vFTpAutoCtrl;

    FTdAutoCtrl   = &vFTdAutoCtrl;
    FEDSAutoCtrl  = &vFEDSAutoCtrl;
    FBurnAutoCtrl = &vFBurnAutoCtrl;
    FTurnON_Ctrl  = &vTurnON_Ctrl;

    vFCtrlTp         = 0;
    vFCtrlTd         = 0;
    vFCtrlCO         = 0;
    vFCtrlEDS        = 0;
    vFTpAutoCtrl     = 0;
    vFTdAutoCtrl     = 0;
    vFEDSAutoCtrl    = 0;
    vFBurnAutoCtrl   = 0;
    vTurnON_Ctrl     = 1;

    Cg->AutoCtrl = 0;             // ����� ������ ��
    Ac->AutoCtrl = 0;             //              ��
    EdsValue->AutoCtrl = 0;
    TdValue->AutoCtrl = 0;        //
    TpValue->AutoCtrl = 0;


//    Profile = new sProfileParam();
//    Prog_profile = new sProgParam();
    Profile      = NULL;
    Prog_profile = NULL;

    Visible = true;

}
//---------------------------------------------------------------------------
__fastcall TFlameUnit::~TFlameUnit()
{
    //TODO: Add your source code here
    if (Assigned(FlameList))
      FlameList->DeleteReference(FIndexInFlameList);
    if (Assigned(ParamList)) {
    // ��������� ��� �������� ������� ������  ???
        for (int i = 0; i< ParamList->Count;i++){
           TParamUnit *ParamUnit = (TParamUnit*)ParamList->Items[i];
           if(ParamUnit != NULL)
              delete ParamUnit;
              ParamUnit = NULL;
        }
        ParamList->Clear();
        delete ParamList;
   }
   delete Regim;
   delete FClap_Pipe;
   if(FProfile){
      delete FProfile;
      FProfile = NULL;
   }
   if(Fprog_profile){
      delete Fprog_profile;
      Fprog_profile = NULL;
   }
}
//---------------------------------------------------------------------------
namespace Flameunit
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TFlameUnit)};
         RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

//------------------------------------------------------------------------------
char * Oven_StatusName[3]={
     "",
     " -> �������",
     " -> �����"
};

//��������� : ������ , ������������ �������, ��� ������������
void __fastcall TFlameUnit::SetStatus(int value)
{
  if (Profile == NULL) return;
  if((FCarburizer_Oven_Status != value)&&(IsOVEN /*FUnit_Type == uOVEN*/)) {
     int Old = FCarburizer_Oven_Status;
     TypeUnitPanel->Caption = FUCaption + Oven_StatusName[value>2?2:value];
     FIs_profile = value != sstNone;

     FCarburizer_Oven_Status = value;

     //-- ������� "���������� ������������" ��������� � ���� �������
     int EventId = -1;
     if ((Old == sstNone) && (value == sstInProc))                              EventId = eStartProfile;
     else if ((Old == sstInProc) && (value == sstReady) || (value== sstNone))   EventId = eStopProfile;
     else if ((Old == sstInProc) && (value == ssExtrStopProfile))               EventId = eExtrStopProfile;
     if(EventId != -1){
        char Comment[50];
        sprintf (Comment,"[%s %d]",Profile->Name,Profile->Num);
        GenerateEvent(EventId,/*eProgram*/ FObjectId,Comment);
     }
     // �����
     if(value > sstInProc){
        Profile->Done = true;
        // ��� ���������
        unsigned char ccw = CCW;
        //-- ��������� ����������
        //-- ������ � ���������� �������� ���������� ������������ �������
        ccw |= STOP_PROFILE_CCW;
        ccw |= START_PROFILE_CCW;
        ccw &= ~EXTR_PROFILE_CCW;
        //-- ��� �� ��������� ������ ����� ���������� � ����������
        CCW = ccw;
        Profile->SaveProf();
/*
        //-- ��������� ������ ����� ���������� � ����������
        if (!WriteCcwDll(ccw))
        {   //-- ���������� ������ �����
            //-- � ��������� ��� ������� � ���� �������
            GenerateEvent(eLostConnect,eController1,NULL);
        }
*/
     } else {
        Profile->Done = false;
        // �����
        if(value == sstInProc){
            //-- ������ � ���������� �������� ������ ������������ �������
            unsigned char ccw = CCW;
            ccw &= ~STOP_PROFILE_CCW;
            ccw &= ~EXTR_PROFILE_CCW;
            ccw |= START_PROFILE_CCW;
            //-- ��� �� ��������� ������ ����� ���������� � ����������
            CCW = ccw;
            Profile->SaveProf();
        }
     }
  }
}

//------------------------------------------------------------------------------
bool __fastcall TFlameUnit::GenerateEvent(int EventId,char* Comment)
{
   if(FlameList){
     FlameList->GenerateEvent(EventId,FObjectId,Comment);
     return true;
  }else
     return false;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameUnit::GenerateEvent(int EventId,int Source,char* Comment)
{
   if(FlameList){
     FlameList->GenerateEvent(EventId,Source,Comment);
     return true;
  }else
     return false;

}

//------------------------------------------------------------------------------
//int __fastcall TFlameUnit::GetStatus()
//{
//    return FCarburizer_Oven_Status;
//}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::SetIs_profile(bool value)
{
    if(FIs_profile != value) {
        SetStatus(value?1:0);
        if (Profile)
           Profile->Fl_Start   = value;
    }
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetUCaption(AnsiString value)
{
    if(TypeUnitPanel->Caption != value) {
        TypeUnitPanel->Caption = value;
        FUCaption = value;
    }
}

//---------------------------------------------------------------------------
AnsiString __fastcall TFlameUnit::GetUCaption()
{
//    return TypeUnitPanel->Caption;
    return FUCaption;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetEvent(int EventId,int Source,char* Comment)
{
  if(FOnSetStat)
     FOnSetStat(EventId,Source,Comment);
  else
     GenerateEvent(EventId,Comment);
}

void __fastcall TFlameUnit::SetType(Units_Type value)    
{
    Units_Type Old;
    bool Old_IsOVEN = IsOVEN;
    int iCgDisp;
    if(FUnit_Type != value) {
        Old = FUnit_Type;
        FUnit_Type = value;
        Status = sstNone;
        FCarburizer_Oven_Status = 0;
        FUnit_Type = value;
        /*  NULL_UNIT, uOVEN, uENDO,uOVEN_Lite  */
        switch(FUnit_Type){
        case uOVEN:
           Height = HeightP;
           Bvl->Top = BevelYp;   // Y �����������
           if(!Old_IsOVEN)
           {
               Cg->Visible = true;
               iCgDisp = CgDisp;
               ForEach( pSetDisp ,&iCgDisp);
               Regim->SetNewImage(2);
           }
           break;
        case uENDO:
           Height = HeightE;
           Bvl->Top = BevelYe;   // Y �����������
           if(Old_IsOVEN)
           {
               Cg->Visible = false;
               iCgDisp = -CgDisp;
               ForEach( pSetDisp ,&iCgDisp);
               Regim->SetNewImage(0);
           }
           break;
        case uOVEN_Lite:
           Height = HeightPL;
           Bvl->Top = BevelYpl;   // Y �����������
           if(!Old_IsOVEN)
           {
               Cg->Visible = true;
               iCgDisp = CgDisp;
               ForEach( pSetDisp ,&iCgDisp);
               Regim->SetNewImage(2);
           }
        };

        if (Assigned(FlameList))
           FlameList->ChangeType(FIndexInFlameList, Old);
        if(FClap_Pipe)
           FClap_Pipe->SetUnitType(IsOVEN /*FUnit_Type == uOVEN*/);
        Invalidate();
    }
}

//------------------------------------------------------------------------------
void TFlameUnit::SetValueAll(double * Values)
{
    FParam = Values;
    ForEach(pSetValue, Values);
}

//------------------------------------------------------------------------------
void TFlameUnit::SetValueAll()
{
    FParam = ArmParam->Param;
    ForEach(pSetValue, ArmParam->Param);
}

//------------------------------------------------------------------------------
// ���������� ��������� �������� ���������
void __fastcall TFlameUnit::SetLimits(sLimits *aLimits)
{
  Limits = aLimits;
  ForEach(pSetLimits, aLimits);
}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::SetTp(double value)
{
//    if(FTp != value) {
//        FTp = value;
        TpValue->Value = value;
//    }
}

double __fastcall TFlameUnit::GetTp()
{
        return TpValue->Value;
}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::SetTd(double value)
{
//    if(FTd != value) {
//        FTd = value;
        TdValue->Value = value;
//    }
}
double __fastcall TFlameUnit::GetTd()
{
        return TdValue->Value;
}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::SetCo(double value)
{
    if(FCo != value) {
        FCo = value;
//        CoValue = value;
    }
}
double __fastcall TFlameUnit::GetCo()
{
        return FCo;
}

//------------------------------------------------------------------------------
void __fastcall TFlameUnit::Set_Ac(double value)
{
//    if(F_Ac != value) {
//        F_Ac = value;
        Ac->Value = value;
//    }
}
double __fastcall TFlameUnit::Get_Ac()
{
        return Ac->Value;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetEDS(double value)
{
//    if(FEDS != value) {
//        FEDS = value;
        EdsValue->Value = value;
//    }
}

double __fastcall TFlameUnit::GetEDS()
{
        return EdsValue->Value;
}
//------------------------------------------------------------------------------
void __fastcall TFlameUnit::Set_Cg(double value)
{
//    if(F_Cg != value) {
//        F_Cg = value;
        Cg->Value = value;
//    }
}
double __fastcall TFlameUnit::Get_Cg()
{
        return Cg->Value;
}

//------------------------------------------------------------------------------
bool __fastcall TFlameUnit::ForEach(TProcType Type, void *AInfo)
{
   int StartTop;
   for (int i = 0; i < ParamList->Count; i++)
      if (ParamList->Items[i])
          switch (Type){
          case pSetDisp:           // �������� � ���������� �� ������ ���� ��� ������������ ����/����
              if(i < 4)            // �� ��� Cg and Ac
                if(Visible)
                  ((TParamUnit*)ParamList->Items[i])->DispTop(*(int *)AInfo);
              break;
          case pSetNormalDisp:
                if(i == 0)            // �� ��� Cg and Ac
                   StartTop = *(int *)AInfo;
                if((TParamUnit*)ParamList->Items[ParamList->Count - i -1])
                   if(((TParamUnit*)ParamList->Items[ParamList->Count - i -1])->Visible)
                       StartTop += ((TParamUnit*)ParamList->Items[ParamList->Count - i -1])->SetTop(StartTop);
                if(i == (ParamList->Count - 1))
                      *(int *)AInfo = StartTop;
              break;
          case pSetValue:          // ������ �������� ���������
              ((TParamUnit*)ParamList->Items[i])->Value = (*(double *)AInfo);
              ((double *)AInfo)++;
              break;

          case pSetArmParam:       // ���������� ��������� ��������
              ((TParamUnit*)ParamList->Items[i])->SetArmParam((double *)AInfo);
              ((double *)AInfo)++;
              break;


          case pShowAlarm:         // ������� ��������� ��� ���������� �������
              if(Visible)
                 ((TParamUnit*)ParamList->Items[i])->TriggerPhasa();
              break;

          case pSetLimits:         // ���������� ��������� �������� ���������
              if(Visible)
                 ((TTpParamUnit*)ParamList->Items[i])->SetLimitAddr((sLimits *)AInfo);
              break;

          case pAcceptPSW:         // ���������� �������� � ��������
              if(i < 4)            // �� ��� Cg and Ac
                 if(Visible)
                    ((TTpParamUnit*)ParamList->Items[i])->AcceptPSW(*(unsigned char *)AInfo >>(i*2));
              break;
              
//          case pLoadMultiData:     // ��������� ������ ��� �������������         
          
//              break;
          }
    return true;
}

//---------------------------------------------------------------------------
// ������� ��������� ��� ���������� �������
void  __fastcall TFlameUnit::TriggerAlarmPhasa()
{
   ForEach(pShowAlarm);
}

//---------------------------------------------------------------------------
// ��� ���� ?
bool __fastcall TFlameUnit::GetIsOVEN()
{
    return (Type == uOVEN)||(Type == uOVEN_Lite);
}

//---------------------------------------------------------------------------
int __fastcall TFlameUnit::GetIndexInFlameList()
{
    return FIndexInFlameList;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetCtrlTp(double value)
{
  if(FCtrlTp)
    if(*FCtrlTp != value) {
        *FCtrlTp = value;
    }
}

void __fastcall TFlameUnit::SetCtrlTd(double value)
{
  if(FCtrlTd)
    if(*FCtrlTd != value) {
        *FCtrlTd = value;
    }
}

void __fastcall TFlameUnit::SetCtrlCO(double value)
{
   if(FCtrlCO)
    if(*FCtrlCO != value) {
        *FCtrlCO = value;
    }
}

void __fastcall TFlameUnit::SetCtrlEDS(double value)
{
  if(FCtrlEDS)
    if(*FCtrlEDS != value) {
        *FCtrlEDS = value;
    }
}

void __fastcall TFlameUnit::SetTpAutoCtrl(int value)
{
  if(FTpAutoCtrl)
    if(*FTpAutoCtrl != value) {
        *FTpAutoCtrl = value;
        TpValue->AutoCtrl = value;        //
    }
}

void __fastcall TFlameUnit::SetTdAutoCtrl(int value)
{
  if(FTdAutoCtrl)
    if(*FTdAutoCtrl != value) {
        *FTdAutoCtrl = value;
//    Cg->AutoCtrl = 0;             // ����� ������ ��
//    Ac->AutoCtrl = 0;             //              ��
      TdValue->AutoCtrl = value;
    }
}

void __fastcall TFlameUnit::SetEDSAutoCtrl(int value)
{
  if(FEDSAutoCtrl)
    if(*FEDSAutoCtrl != value) {
        *FEDSAutoCtrl = value;
        EdsValue->AutoCtrl = value;
    }
}
void __fastcall TFlameUnit::SetCgAutoCtrl(int value)
{
  if(FCgAutoCtrl)
    if(*FCgAutoCtrl != value) {
        *FCgAutoCtrl = value;
        Cg->AutoCtrl = value;
    }
}

void __fastcall TFlameUnit::SetBurnAutoCtrl(int value)
{
  if(FBurnAutoCtrl)
    if(*FBurnAutoCtrl != value) {
        *FBurnAutoCtrl = value;
        Ac->AutoCtrl = value;             //              ��
    }
}
void __fastcall TFlameUnit::SetTurnON_Ctrl(int value)
{
  if(FTurnON_Ctrl)
    if(*FTurnON_Ctrl != value) {
        *FTurnON_Ctrl = value;
        if(value)
           Visible = true;
        else
           Visible = false;
    }
}

//---------------------------------------------------------------------------
double __fastcall TFlameUnit::GetCtrlTp(void)
{
   if(FCtrlTp)
        return *FCtrlTp;
   else
        return 0;
}

double __fastcall TFlameUnit::GetCtrlTd(void)
{
   if(FCtrlTd)
        return *FCtrlTd;
   else
        return 0;
}

double __fastcall TFlameUnit::GetCtrlCO(void)
{
   if(FCtrlCO)
        return *FCtrlCO;
   else
        return 0;
}

double __fastcall TFlameUnit::GetCtrlEDS(void)
{
   if(FCtrlEDS)
        return *FCtrlEDS;
   else
        return 0;
}

int    __fastcall TFlameUnit::GetTpAutoCtrl(void)
{
   if(FTpAutoCtrl)
        return *FTpAutoCtrl;
   else
        return 0;
}

int    __fastcall TFlameUnit::GetTdAutoCtrl(void)
{
   if(FTdAutoCtrl)
        return *FTdAutoCtrl;
   else
        return 0;
}

int    __fastcall TFlameUnit::GetEDSAutoCtrl(void)
{
   if(FEDSAutoCtrl)
        return *FEDSAutoCtrl;
   else
        return 0;
}

int    __fastcall TFlameUnit::GetCgAutoCtrl(void)
{
   if(FCgAutoCtrl)
        return *FCgAutoCtrl;
   else
        return 0;
}

int   __fastcall TFlameUnit::GetBurnAutoCtrl(void)
{
   if(FBurnAutoCtrl)
        return *FBurnAutoCtrl;
   else
        return 0;
}

int   __fastcall TFlameUnit::GetTurnON_Ctrl(void)
{
   if(FTurnON_Ctrl)
        return *FTurnON_Ctrl;
   else
        return 0;
}

//---------------------------------------------------------------------------

void __fastcall TFlameUnit::SetProfile(sProfileParam* value)
{
    if(FProfile != value) {
        FProfile = value;
        FProfile->Source = FObjectId;
    }
}

sProfileParam* __fastcall TFlameUnit::GetProfile()
{
    return FProfile;
}

//---------------------------------------------------------------------------
sProgParam * __fastcall TFlameUnit::GetProg_profile()
{
    return Fprog_profile;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetProg_profile(sProgParam* PrgPar)
{
    Fprog_profile = PrgPar;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameUnit::GetUseEtaps()
{
        //TODO: Add your source code here
//    if (CurFlameUnit->IsOVEN && CurFlameUnit->Is_profile &&
    if(Profile == NULL) return false;
    if (Profile->UseEtaps) return true;
    return false;
}

// --  ����������� ���� ��� ����������  -----------------------------
int  __fastcall TFlameUnit::GetTpReg(void)
{
    return (int)TpValue->RegValue;
}


void __fastcall TFlameUnit::SetTpReg(int value)
{
    TpValue->RegValue = value;
}

// --  ������������� ������ ����������� ���������  -----------------------------
int * __fastcall TFlameUnit::FillProgData(int Interval,int TpMul,int CgMul, int Tag)
{
    if((Tag == 0) || (Tag == 5)){
       if(Prog_profile == NULL)
            Prog_profile = new sProgParam(&Profile->Etaps,Interval,TpMul,CgMul);
//       else
//            Prog_profile->FillData(&Profile->Etaps,Interval,TpMul,CgMul);
       return  Tag ? Prog_profile->pCgData :Prog_profile->pTpData;

    }
    return NULL;
}

//---------------------------------------------------------------------------
// ����� ����� ����� ������ � ��������
sProfileParam* __fastcall TFlameUnit::GetFirstProfile()
{
  if(Is_profile)
     return Profile;
  if(FlameList == NULL)
     return NULL;
  else
     return  FlameList->FirstProfile;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetArmParam(sArmParam * aPar){
    ArmParam = aPar;
    ForEach( pSetArmParam,aPar->Param);
}


//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetFirstProfile(sProfileParam* value){};

//---------------------------------------------------------------------------

void __fastcall TFlameUnit::SetObjectId(int value)
{
    if(FObjectId != value) {
        FObjectId = value;
        if (FProfile)
           FProfile->Source = value;
    }
}

//---------------------------------------------------------------------------
// ������ �������
bool  __fastcall TFlameUnit::CalculateProfile()
{
    FProfile->CalculateProc();
    return true;
}

//---------------------------------------------------------------------------
// ������ �������
bool  __fastcall TFlameUnit::ExecuteProfile(unsigned char  *ccw)
{
    unsigned short int hour,min,sec,msec;
    if (Profile->Fl_Start) Profile->Fl_Start = false;

    TDateTime dt = TDateTime::CurrentDateTime() - Profile->Start;
    dt.DecodeTime (&hour,&min,&sec,&msec);
    Profile->Time = sec + min*60 + hour*3600 - Profile->AllTime;
    Profile->AllTime = sec + min*60 + hour*3600;

    FProfile->CalculateProc();
    //��������� ��� ������ -  ����� � ������
    Profile->EvenData();
    int Ind;
    if ( !Profile->Done){
        //   �������� �������� ��������� ������������ �������
        if (Profile->TestFinish(Ind) == 3){
            // ���������������� � ����������� ���� ����������
            // ������������ ����������� �������
            Status = sstReady;  // ��� � Profile->SaveProf();
            return true;
        }
        Profile->SaveProf();
    }
//    Profile->SaveProf();
    return Profile->Done;
}

//---------------------------------------------------------------------------
// �������� ����� ������� �� ����������� ����������
bool  __fastcall TFlameUnit::TestSameProfile(sProfileParam *p)
{
    if (AnsiString(Profile->Name) == AnsiString(p->Name) 
        && Profile->Num == p->Num
        && Profile->SadkaNum == p->SadkaNum                    
        )
    {
        return true;
    }
   return false;

}

//---------------------------------------------------------------------------
// ������� �������
bool __fastcall TFlameUnit::RecognizeProfile(sProfileParam *p)
{
    if(Profile == NULL)
        Profile = new sProfileParam();

    *Profile = *p;                   // ������ ����� ����������
//    Profile->Fl_Start   = true;    �������� � Is_profile = true
    Is_profile = true;      //-- ���������� �������
    //-- ������ �f ������������ ����� �� � ��������� ������ �������
    Profile->Cf = _Cg;
    Profile->Tp = Tp;
    Profile->InitCf();
//    Status = sstInProc;   �������� � Is_profile = true
    return true; //!!
}

// -- ������������ ��������� �������  ------------------------------------------
bool  __fastcall TFlameUnit::ReleaseProgProfile()
{
//    Profile->Fl_Start   = false;     �������� � Is_profile = false
    Is_profile = false;            //-- ������� �������
    FlameList-> ReleaseProfile(Profile);
//    Profile->UseEtaps = false;
    unsigned char ccw = CCW;
    //-- ������ � ���������� �������� ����������� ���������� ������������ �������
    ccw &= ~STOP_PROFILE_CCW;
    ccw &= ~START_PROFILE_CCW;
    ccw |= EXTR_PROFILE_CCW;
    //-- ��� �� ��������� ������ ����� ���������� � ����������
    CCW = ccw;
 //   Status = sstNone;
    Profile->SaveProf();
    return true;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetCtrls(
            double      *aCO,                     //-- ������������ ��
            double      *aTp,                     //-- �������� ����������� �����
            double      *aTd,                     //-- �������� ����������� �������
            double      *aEDS,                    //-- �������� ���
            int         *aTpAutoCtrl,             //-- ������� ��������� ��
            int         *aTdAutoCtrl,             //-- ������� ��������� �d
            int         *aEDSAutoCtrl,            //-- ������� ��������� ���
            int         *aCgAutoCtrl,             //-- ������� ��������� Cg
            int         *aBurnAutoCtrl            //-- �������  ������ "������ �������"
){
   FCtrlTp      =      aTp;                     //-- ������������ ��
   FCtrlTd      =      aTd;                     //-- �������� ����������� �����
   FCtrlCO      =      aCO;                     //-- �������� ����������� �������
   FCtrlEDS     =      aEDS;                    //-- �������� ���

   FTpAutoCtrl  =      aTpAutoCtrl;             //-- ������� ��������� ��
   FTdAutoCtrl  =      aTdAutoCtrl;             //-- ������� ��������� �d
   FEDSAutoCtrl =      aEDSAutoCtrl;            //-- ������� ��������� ���
   FCgAutoCtrl =       aCgAutoCtrl;             //-- ������� ��������� Cg
   FBurnAutoCtrl=      aBurnAutoCtrl;           //-- �������  ������ "������ �������"
}

//------------------------------------------------------------------------------
byte __fastcall TFlameUnit::UpLoadCtrls()
{
   byte Result = 0;
   if(vTurnON_Ctrl)
   {
       Result        =      *FTpAutoCtrl>>1;
       Result       |=      *FTdAutoCtrl & 2;
       Result       |=      (*FEDSAutoCtrl & 2)<<1;

       if(Result) 
       {
          Result     = (Result | 0x08)<<4;             //-- ������� - ����� ������������
       }   
   }    
   return Result;

}

//------------------------------------------------------------------------------
bool __fastcall TFlameUnit::InitCtrlParam()
{
        //-- � �������� ������������� ������������� CO � ������
        GenerateEvent(eSetCO,AnsiString(CtrlCO).c_str());
        //-- � �������� ������������� ����������� ���� � ������
        if (!TpAutoCtrl)
        {
            GenerateEvent(eSetTp,AnsiString(CtrlTp).c_str());
        }
        //-- � �������� ������������� ����������� ������� � ������
        if (!TdAutoCtrl)
        {
            GenerateEvent(eSetTd,AnsiString(CtrlTd).c_str());
        }

        //-- � �������� ������������� ��� � ������
        //-- ��� ����������� �� ������ ����������
        Clap_Pipe->EDS_Init(Options.All_Limits[IndexInFlameList].EReg);
        if (!EDSAutoCtrl)
        {
            GenerateEvent(eSetEDS,AnsiString(CtrlEDS).c_str());
        }
        if(TpAutoCtrl == acALT_Unit)
            return true;
        else
            return false;
}

//---------------------------------------------------------------------------
// ������ � ���������� �� �����������
void __fastcall TFlameUnit::SetCCW_(unsigned char value)
{

  if(FlameList != NULL){
     FlameList->SetCCW_(value,IndexInFlameList);
  }

}

void __fastcall TFlameUnit::SetCCW(unsigned char value)
{

  if(FlameList != NULL){
     FlameList->SetCCW(value,IndexInFlameList);
  }

}
unsigned char __fastcall TFlameUnit::GetCCW()
{
  if(FlameList != NULL){
     return FlameList->GetCCW(IndexInFlameList);
  }else
     return 0;
}

//---------------------------------------------------------------------------
//-- ��������/������ ���� ��������� ������� ��������
bool __fastcall TFlameUnit::GetDoBurnSensor()
{
  if(FlameList)
       return (FlameList->BurnSensorBits & (1 << IndexInFlameList)) != 0;
  else
       return false;
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetDoBurnSensor(bool Abcw)
{
  if(FlameList)
      if(Abcw)
        FlameList->BurnSensorBits |= (1 << IndexInFlameList);
      else
        FlameList->BurnSensorBits &= ~(1 << IndexInFlameList);
}

//---------------------------------------------------------------------------
bool __fastcall TFlameUnit::GenerateParam(void)
{
    return FParamMenager->GenerateParam(ArmParam,ObjectId);
}

//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetReference(TFlameList *aFlameList,int Index)
{
    FIndexInFlameList = Index;
    FlameList = aFlameList;
    FParamMenager = FlameList->ParamMenager;
    FClap_Pipe ->SetIndex(Index);   
}

//---------------------------------------------------------------------------
int* __fastcall TFlameUnit::MakeFuncData(int ParamIdx, int Mul, int &Count)
{
   return FParamMenager->MakeFuncData(FIndexInFlameList,ParamIdx, Mul, Count);
}
//---------------------------------------------------------------------------
void __fastcall TFlameUnit::SetClap_Pipe(TClap_Pipe  * value)
{
        if(FClap_Pipe != value) {
//                FClap_Pipe = value;
        }
}

//---------------------------------------------------------------------------
TClap_Pipe  * __fastcall TFlameUnit::GetClap_Pipe()
{
        return FClap_Pipe;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameUnit::MathHandle(bool IsCalcCg , bool IsCalcCgOld)
{
    double Td_dbl = CelcToKelvin_l(Td);
    double Tp_dbl = CelcToKelvin_l(Tp);
    if (Tp ==0) return false;
    if (Td ==0) return false;

//    if (!IsCalcCg || IsCalcCg &&IsCalcCgOld){ // ��� ���������������
       Ac->SetParamValue(Ac_func_l(  Co,Tp_dbl,EDS,Td_dbl));

      //-- ����������� ��������� �������� �� 0 �� 100%
      if (_Ac < 0)   Ac->SetParamValue(0);
      if (_Ac > 100) Ac->SetParamValue(101);
//    }

    if (IsCalcCg)
    {
        if(IsCalcCgOld)
//��������� (���� ���������� �� ��)
            Cg->SetParamValue(Cg_func_l( _Ac,Tp_dbl));
        else
            Cg->SetParamValue(EDS/1000); // �� � ���� ���

        //-- ����������� ��������� �������� �� 0 �� 100%
        if (_Cg < 0)   Cg->SetParamValue(0);
        if (_Cg > 100) Cg->SetParamValue(101);
    }
    return true;
}

//--  ������ �������� ����������   ---------------------------------------------
bool __fastcall TFlameUnit::PersInputInfo(TTermoDat *TermoDat)
{
   int Ind;
   Co   = CtrlCO;
   // �������� �������
   if( TpAutoCtrl == acHandl){
      Tp = CtrlTp;
   // �� ���������
   }else if( TpAutoCtrl == acALT_Unit){        // ����� �� �����������
      Tp = TermoDat->GetParam(FIndexInFlameList*3,&Ind);
      AnsiString aStr = IntToStr(Ind);

      if(Tp == 0)
         //-- ������ ����� � ��� �����������(��������)
         GenerateEvent(eLostConnectALT,eTermodat,aStr.c_str());
      else if(Tp < 0){
         Tp = 0;
         //-- ��� ����������(��������)������������� �� ������(�����)
         GenerateEvent(eErrALT,eTermodat,aStr.c_str() );
       }

   }

   // �������� �������
   if( TdAutoCtrl == acHandl){                 // ����� �� �����������
      Td = CtrlTd;
   }
   // �������� �������
   if( EDSAutoCtrl == acHandl){                // ����� �� �����������
      EDS = CtrlEDS;
   }
   //-- �������������� ��������� �������� ����������
   try
   {
       if (!MathHandle (IsOVEN,CgAutoCtrl == acHandl))
       {
           //-- ����������� ������������ � ������� ������

           // ���� - ��������� � ��������� ������� ����������
           //  (��� ��������� �������� ������ ������ � ������)
           if(!FlIn_MathExeption)
           {
               GenerateEvent(eExcaptionOnWork,eProgram,"Math Excaption1!");
               _Ac  = 0;
               _Cg  = 0;
               FlIn_MathExeption = true;
           }
       }
       //-- ������� � ��������� ������ - ���������� �f ��� ��������
       //-- ���������� �����
       else
       {
           if(IsOVEN){
             if(Profile != NULL)
               Profile->Cf = _Cg;
           }
           FlIn_MathExeption = false;
       }
       return false;

   }
   catch (...) //-- � ������ ���������� ����� � ������������
   {
       //-- ����������� ������������ (���� ��� �� �������������)
       if(!FlIn_MathExeption)
       {
           GenerateEvent(eExcaptionOnWork,eProgram,"Math Excaption!");
           _Ac  = 0;
           _Cg  = 0;
           FlIn_MathExeption = true;
       }
       return true;
   }
}

//--  ���������� ���� ��������� ����� --------------------------------------
bool __fastcall TFlameUnit::MakeCommonProg(sTermAssignment * TermAssignment, sEtapsProfile *pEtaps)
{
    bool Result = true;
    //== ������������ ��������� ���������
    //-- ���������� ������ ������� �������
    unsigned short hour,min,sec,msec;
    TDateTime dtDelta = (TDateTime::CurrentDateTime() - Profile->Start);
    dtDelta.DecodeTime (&hour,&min,&sec,&msec);
    int Sec = (sec + min*60 + hour*3600);

    //-- ����� ������ ����� �� ������� ��������� ������� Sec
    int EtapIdx = -1;
    int AllSec=0;
    for (int j=0;j<pEtaps->Used;j++)
    {
        if ((Sec >= AllSec) && (Sec < (AllSec + pEtaps->Etap[j].Time)))
        {
            EtapIdx = j;
            break;
        }
        else AllSec+= pEtaps->Etap[j].Time;
    }

    //-- �������� ������ ����� ������� ����� ������ ������� ����������� ���������
    //-- � �������� ����������� ������������� ����� ���������
    //-- �� ���� ������� ����������� ��������� �����������
    if (EtapIdx == -1 && Sec >= AllSec) 
    {
        EtapIdx = pEtaps->Used-1;
        Result = false;
    }
    if (EtapIdx != -1){
        TermAssignment->Cg_Assignment = pEtaps->Etap[EtapIdx].Cg*1000;
        FClap_Pipe->EDS = TermAssignment->Cg_Assignment;
        TermAssignment->Tp_Assignment = pEtaps->Etap[EtapIdx].T;
    }else{
        Result = false;
    }
    return Result;
}

//--  ���������� ���� ��������� ���������� --------------------------------
bool __fastcall TFlameUnit::MakePrepareProg(sTermAssignment * TermAssignment)
{
   switch(Profile->GrafStatus)
   {
   case egsStart:
         Clap_Pipe->Regim = rHANDLE;     //-- ����������� �� ������
         Clap_Pipe->SetStat(sToLess);    //-- ������ ����������
         Clap_Pipe->ProbeClapan(OFF);    //-- ��������� ������ �� ������� �����
         DoBurnSensor = true;            //-- �������� ������
         Profile->GrafStatus = egsGasClapanOff;
   case egsGasClapanOff:     //-- �� ��������� �������������� ������� ������ ���� ��������
         if(TDateTime::CurrentDateTime() > Profile->EndGasClapanOff)
         {
             Clap_Pipe->SetStat(sToNone);//-- ������ "�� ����������"
             Profile->GrafStatus = egsEndPeriod_0_1;
         }
         break;
   case egsEndPeriod_0_1:    //-- ��������� ������� ������� �����
         if(TDateTime::CurrentDateTime() > Profile->EndPeriod_0_1)
         {
             Clap_Pipe->SetStat(sToMore);    //-- ������ ����������
             Profile->GrafStatus = egsGasClapanOn;
         }
         break;

   case egsGasClapanOn:      //-- �� ��������� �������������� ������� ������ ���� �������
         if(TDateTime::CurrentDateTime() >Profile->EndClapanOpenTime)
         {
             Profile->GrafStatus = egsEndPeriod_1_2;
             Clap_Pipe->SetStat(sToNone);//-- ������ "�� ����������"
         }
         break;

   case egsEndPeriod_1_2:     //-- ��������� ������� �������������� ������ ���������
         if(TDateTime::CurrentDateTime() > Profile->EndPeriod_1_2)
         {
             Clap_Pipe->Regim = rAUTO;
             DoBurnSensor = false;                         //-- ��������� ������
             Clap_Pipe->ProbeClapan(ON);                   //-- �������� ������ �� ������� �����
             Profile->Start = TDateTime::CurrentDateTime();
             Profile->GrafStatus = egsRizingCg_Prog;
         }
         break;
   case egsRizingCg_Prog:     //-- ������ �� �� ���������
          //== ������������ ��������� ����������� ���������
         if(!MakeCommonProg(TermAssignment,&Profile->PrepareEtaps/*Etaps*/)) //���� ��������� ���������
         {
            Profile->Start = TDateTime::CurrentDateTime();
            Profile->Setup = Profile->Start;
            Profile->UsePrepare = false;
         }
         break;
   }
   return true;
}

//--  ���������� ���� ���������� ���������  --------------------------------
bool __fastcall TFlameUnit::MakeTermalProg(sTermAssignment * TermAssignment, TTermoDat * pTermoDat)
{
    //== ������������ ��������� ����������� ���������
    bool Result = MakeCommonProg(TermAssignment,&Profile->Etaps);
    
    if( TpAutoCtrl == acALT_Unit){        // ������� � ��������
       if (Clap_Pipe->Enable)
          pTermoDat->SetUst(TermAssignment->Tp_Assignment,FIndexInFlameList*3);

    }
    return Result;
}

//--  C���� ��������� �����������  -----------------------------------------
void __fastcall TFlameUnit::SetPSW(unsigned char value)
{
    if(Fpsw != value) {
        Fpsw = value;
        ForEach( pAcceptPSW ,&value);
    }

}

//--  ���������� �� �����. � ��������� �������� "���� �������"  -------------
void __fastcall TFlameUnit::SetPultStatus(bool st)
{
    if(FPultStatus != st) {
        FPultStatus = st;
        Regim->Regim = st? rPULT:rHANDLE;
        if(Is_Current)
        {
            FClap_Pipe->Regim = Regim->Regim;
            FClap_Pipe->Enable = st != rHANDLE;
            if(TClap_Pipe::PanelControl)
                 TClap_Pipe::PanelControl->Visible = FClap_Pipe->Enable;
        }
        else
        {
            FClap_Pipe->RegimStory(Regim->Regim);
        }
    }

}

//--  ���������� ������ ���� �������                            -------------
void __fastcall TFlameUnit::SetIs_Current(bool value)
{
    if(FlameList){
        FlameList->CurentFlameUnit = this;
        if (IsOVEN){
           FClap_Pipe->More_InControl->Glyph->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16002));
           FClap_Pipe->Less_InControl->Glyph->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16003));
        }else{
           FClap_Pipe->More_InControl->Glyph->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16000));
           FClap_Pipe->Less_InControl->Glyph->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(16001));
        }

    }

}

//---------------------------------------------------------------------------

bool __fastcall TFlameUnit::GetIs_Current()
{
   if(FlameList)
      return FlameList->CurentFlameUnit == this;
   else
      return false;
}

//---------------------------------------------------------------------------
// ��������� ������ ��� �������������
bool  __fastcall TFlameUnit::LoadMultiData(double ** Data)
{
        **Data = Tp; (*Data)++;
        **Data = Td; (*Data)++;
        **Data = EDS;(*Data)++;
        return true;
}

//---------------------------------------------------------------------------

