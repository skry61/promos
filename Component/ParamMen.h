//---------------------------------------------------------------------------

#ifndef ParamMenH
#define ParamMenH
#include "Profile.h"
#include "DbUnit_ev.h"
#include "Params.h"
#include "Grapher.h"

//---------------------------------------------------------------------------
//-- ��������� ����� ���������� ---------------------------------------------
struct sArmParam
{
    TDateTime   Time;               //-- ����/����� ��������� ����������
    double      Param[MAX_PARAM];   //-- ������ ����������
    int         Source;             //-- �������� �������������
    int         Fl_UNKNOWN_DATA;    //-- ������ �������� ��� ��������
};

extern sArmParam       all_param[MAX_OBJECT];      //-- ��������� ���� �������
extern sArmParam       back_param[MAX_OBJECT];     //-- ����������� ���������


class PACKAGE TParamMen : public TxDataBase
{
    sArmParam * pParam;

public:		// User declarations
    __fastcall TParamMen(Classes::TComponent* AOwner);
    __fastcall virtual ~TParamMen();

    //-- ��������� ������ � ����� ������-----------------------------------------
    virtual void *__fastcall CreateNew(void);
    virtual int  __fastcall  GetStructSize(void); //-- ���������� ������ ���������
    virtual void __fastcall  ClearAll(void);      //-- ��������� �������� ������������� ������, ���� � ����������
    virtual void __fastcall  AddToListComp(void){}; //-- ��������� ���������� ����������� ���������� ������
    virtual void __fastcall  ClearList(void);
    virtual bool __fastcall  Store (void);
    virtual bool __fastcall  FillListComp(void){return true;};
    virtual bool __fastcall  Generate(int EventId,int Source,char* Comment);
    virtual bool __fastcall GetUnitCount();

    //-- ������������ ��������� --------------------------------------------------
    bool __fastcall GenerateParam(sArmParam *param,int Source);
    bool __fastcall PackParam(sOptimalParam *optim_param);
    void __fastcall AddParamList();

    void __fastcall  SetComboBox(TComboBox *aComboBox, int Type);
    int __fastcall  GetID(int Ind);
    int  __fastcall GetUnitPosInParamRecord(int Ind);
    bool __fastcall ParamIs_Ovens(int Inx);

    int* __fastcall MakeFuncData(int ObjectIdx, int ParamIdx, int Mul, int &Cnt);
    TDateTime  __fastcall  GetFirstTime (void); //-- ���������� ����� �� ����� ������ ���������


    /*
    //-- ��������� ��������� � ���� ������ --------------------------------------
    bool __fastcall StoreParam (sOptimalParam &optim_param);

    //-- ��������� ��������� � ���� ������ --------------------------------------
    bool __fastcall StoreParam (sArmParam* params);
*/

};


//---------------------------------------------------------------------------
//-- ��������� ������ � ����������� -----------------------------------------
//---------------------------------------------------------------------------
bool __fastcall GetIndexFromTime(sArmParam* one_param,TDateTime StartTime,int Interval, int &Idx);
bool __fastcall GetIndexFromOptimTime(sOptimalParam* optim_param,TDateTime StartTime,int Interval, int &Idx);


extern TParamMen *ParamMen;
//---------------------------------------------------------------------------
#endif
