//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "TFGraph.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

static inline void ValidCtrCheck(TFGraph *)
{
    new TFGraph(NULL);
}
//---------------------------------------------------------------------------

__fastcall TFGraph::TFGraph(TComponent* AOwner): TGraphicControl(Owner) //Extctrls::TPaintBox(Owner)
{
    Height = 100;
    Width = 300;
}

void __fastcall TFGraph::Paint(void)
{
}
//---------------------------------------------------------------------------
namespace Tfgraph
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TFGraph)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

