//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "TimeGrapher_upd.h"

//#pragma link "Grapher"
//#pragma link "TimeGrapher"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TTimeGrapher_upd *)
{
        new TTimeGrapher_upd(NULL);
}
//---------------------------------------------------------------------------
__fastcall TTimeGrapher_upd::TTimeGrapher_upd(TComponent* Owner)
        : TTimeGrapher(Owner)
{
  Data = new TList();
  ResultFile = FileOpen(FFileName, fmOpenReadWrite);
}
//---------------------------------------------------------------------------

void __fastcall TTimeGrapher_upd::ClearData(void) {           //������� ��� ������
    short *p;
    for (int i = 0; i < Data->Count; i++)
    {
        p = (short *)Data->Items[i];
        delete []p;
    }
    Data->Clear();
}
//---------------------------------------------------------------------------

__fastcall TTimeGrapher_upd::~TTimeGrapher_upd(){
    ClearData();
    delete Data;
}

//---------------------------------------------------------------------------
       void __fastcall TTimeGrapher_upd::LoadCanvas(){};
       void __fastcall TTimeGrapher_upd::FreeCanvas(){};

//---------------------------------------------------------------------------

namespace Timegrapher_upd
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TTimeGrapher_upd)};
                 RegisterComponents("Fx", classes, 0);
        }
}
//---------------------------------------------------------------------------



void __fastcall TTimeGrapher_upd::SetTimer(TTimer* value)
{
        if(FTimer != value) {
                FTimer = value;
        }
}
TTimer* __fastcall TTimeGrapher_upd::GetTimer()
{
        return FTimer;
}



void __fastcall TTimeGrapher_upd::SetFileName(AnsiString value)
{
        if(FFileName != value) {
                  FFileName = value;
        }
}
AnsiString __fastcall TTimeGrapher_upd::GetFileName()
{
        return FFileName;
}

void __fastcall TTimeGrapher_upd::SetKadrNumber(int value)
{
        if(FKadrNumber != value) {
                FKadrNumber = value;
        }
}
int __fastcall TTimeGrapher_upd::GetKadrNumber()
{
        return FKadrNumber;
}

void __fastcall TTimeGrapher_upd::Add(short *Buf){       //�������� ����� ����
 try{
 //    if(Buf[0] > 0){
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;
         if(Buf[0] > 60) Buf[0] = 60;

         if  (Buf[0] < 0)
               Buf[0] = -1*Buf[0];      // ������ ������ ������ ������ 0
         short *BbufPtr = new short[(Buf[0]+1)];
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
         memcpy(BbufPtr,Buf,((Buf[0]+1))*sizeof (short));
//         memcpy
         Data->Add(BbufPtr);
       //}
 }catch(...){
    ShowMessage("����������� ������");
  }
}


TTimeGrapher_upd::DrawData(int Axis) // Axis - ����� ���
{
 short  *Temp = new short[60];      //��� ���������� �������� ������
 double *AllData = new double[60]; // �� ����������� ������
 double *SelectedData = new double[10]; // �������� �� ����� �� ����
 int di;
 int j = 0;
 memcpy(Temp,(short*)Data->Items[FKadrNumber],120);
 for(int i=0;i<60;i++){
   AllData[i] = Temp[i];
 }
//����� ������ ���� ������ ������
   switch (Axis){
     case 0: di = 0;
            break;
     case 1: di = 4;
            break;
     case 2: di = 8;
            break;
     case 3: di = 12;
            break;
   }
   for(int i = di;i<60;i=i+20){
    SelectedData[j] = AllData[i];
    j++;
   }
 //-----------------------------------------------------------------------------
 SetFunctionData(LineColor,SelectedData,Divisor,10);
 DrawFunctionData(LineColor,FuncDataToInt(SelectedData,Divisor,10),Divisor,10);
 delete []AllData;
 delete []Temp;
 delete []SelectedData;
 return 0;
}

 //-----------------------------------------------------------------------------
void __fastcall TTimeGrapher_upd::Paint(void)
{
    sInputData *PBlocks;
    TCanvas *cnv = StartPaint();
    bool FlFirst = false;

    int BufCount = Data->Count;     // ������� �������
    if(BufCount != 0)
    {
        PrepareCalc();

        cnv->Pen->Style  = psSolid;
        cnv->Pen->Width = FLineWidth;
        int X,Y;
        for (int i=0;i<BufCount;i++){
            short *p = (short *)Data->Items[i];
            int ElCount = p[0];                                   // ����� ������� ������
            PBlocks = (sInputData *)&p[1];                        // ������ ���������� �����
            for (int i=0;i<ElCount;i++){
                 // �������� �� ����������� ������ ����� (������������� ���, ���, Min, StartOffs )
                 //            ...
                  Y = YFunconGrapher (PBlocks->AxisData[FAxisNum].AxFeed);
                 if(FlFirst)
                    cnv->LineTo (X,Y);
                 else{
                    FlFirst = true;
                    cnv->MoveTo (X,Y);
                 }
            }
            //����������
            cnv->LineTo (X,Y);
        }

        PrepareMarker();                  //���������� ������ - �� �����������
    }else{
        cnv->Brush->Color = clGray;
        cnv->Brush->Style = bsSolid;
        cnv->FillRect(GraphRect);
    }
    //DrawLabel (cnv, GraphRect);

    FinishPaint();
    if (FOnPaint) FOnPaint(this);
}



//-----------------------------------------------------------------------------
void __fastcall TTimeGrapher_upd::SetStartViewOffset(int value)
{
        if(FStartViewOffset != value) {
                FStartViewOffset = value;
        }
}
//-----------------------------------------------------------------------------
int __fastcall TTimeGrapher_upd::GetStartViewOffset()
{
        return FStartViewOffset;
}

//-----------------------------------------------------------------------------
void __fastcall TTimeGrapher_upd::SetUnitWeight(float value)
{
        if(FUnitWeight != value) {
                FUnitWeight = value;
        }
}
//-----------------------------------------------------------------------------
float __fastcall TTimeGrapher_upd::GetUnitWeight()
{
        return FUnitWeight;
}
//-----------------------------------------------------------------------------


void __fastcall TTimeGrapher_upd::SetAxisNum(int value)
{
        if((FAxisNum != value)&&(FAxisNum < MAX_AXIS_COUNT)) {

                FAxisNum = value;
        }
}
int __fastcall TTimeGrapher_upd::GetAxisNum()
{
        return FAxisNum;
}
