//---------------------------------------------------------------------------
// ������ ���������� ��� ���
//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#pragma hdrstop

#include "Params.h"

eAccessMode     AccessMode;         //-- ����� �������

eErrorCode LastErrorCode;           // ��� ������


char* cAccessMode[]=
{
    "�������",
    "��������",
    "�������������",
    "��������",
    "������ ����",
    "������ ����"
};


//---------------------------------------------------------------------------
#pragma package(smart_init)

int iObjectId[MAX_OBJECT]=
{
    eZone1,
    eZone2,
    eZone3,
    eEndoGen1,
    eEndoGen2,
    eEndoGen3
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//-- ��������� ����� ���������� ����������� ------------------------------------
//struct sControlParam
// ������������
sControlParam::sControlParam()
{
     SetDefault();
}

//------------------------------------------------------------------------------
//-- ���������� �������� �� ���������
void sControlParam::SetDefault(void)
{
     for (int i=0;i<MAX_OBJECT;i++)
     {
        CO [i] = CO_DEFAULT;
        Tp [i] = TP_DEFAULT;
        Td [i] = TD_DEFAULT;
        EDS[i] = EDS_DEFAULT;
        TpAutoCtrl  [i] = TP_AUTOCONTROL_DEFAULT;
        TdAutoCtrl  [i] = TD_AUTOCONTROL_DEFAULT;
        EDSAutoCtrl [i] = EDS_AUTOCONTROL_DEFAULT;
        CgAutoCtrl  [i] = CG_AUTOCONTROL_DEFAULT;
        BurnAutoCtrl[i] = BURN_AUTOCONTROL_DEFAULT;
     }
 }

//------------------------------------------------------------------------------
//-- ��������� �� �������
bool sControlParam::LoadFromRegistry(bool SetDefaultOnError)
{
    char* cKey = "ControlParam";
    //-- ������ �� ������� ��������� ������������ ���������� ����
    if (!GetRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam)))
    {
        if (SetDefaultOnError)
        {
            SetDefault();
            return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam));
        }
        else return false;
    }
    return true;
}

//-- ��������� � ������
bool sControlParam::StoreToRegistry()
{
    char* cKey = "ControlParam";
    return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sControlParam));
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//-- ��������� ���������� ����������� ���������
//struct sProgParam
sProgParam::sProgParam()
{
        pTpData = pCgData = NULL;
        Count = 0;
}

sProgParam::sProgParam(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul)
{
    SetObjects(Etaps,Interval,TpMul,CgMul);
}
//------------------------------------------------------------------------------
sProgParam::~sProgParam()
{
    ReleaseObjects();
}

void sProgParam::SetObjects(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul)
{
    Etaps->ComputeTime();
    Count = Etaps->AllTime/(Interval/1000);
    pTpData = new int [Count];
    pCgData = new int [Count];
    int cnt = 0;
    for (int i=0;i<Etaps->Used;i++)
    for (int j=0;j<(Etaps->Etap[i].Time/(Interval/1000));j++,cnt++)
    {
        pTpData[cnt] = Etaps->Etap[i].T * TpMul;
        pCgData[cnt] = Etaps->Etap[i].Cg * CgMul;
    }
}

//------------------------------------------------------------------------------
//-- ���������� ������ �� �������
void sProgParam::ReleaseObjects(void )
{
    if (pTpData) delete []pTpData;
    if (pCgData) delete []pCgData;
    pTpData = pCgData = NULL;           //-- ������� ������
    Count = 0;
}

//------------------------------------------------------------------------------
//-- ��������� �������
void sProgParam::FillData(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul)
{
    ReleaseObjects();
    SetObjects(Etaps,Interval,TpMul,CgMul);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//-- ��������� ����� ��������� ----------------------------------------------
//�����������
sOptions::sOptions()
{
    SetDefault();
}
//------------------------------------------------------------------------------
//-- ��������� �� ���������
void sOptions::SetDefault(void)
{
    Interval = INTERVAL_DEFAULT;
    VisibleInterval = VISIBLE_INTERVAL_DEFAULT;
    EventView = EVENT_VIEW_DEFAULT;
    RegulTYPE = REGUL_DEFAULT;
    FlSetUstTermodat= FlSetUstTermodat_DEFAULT;
    Magazining      = 30;                 //-- ������ ���������� ���������� (���)
    DB_Updating     = 60;                 //-- ������ ���������� ���� (�������)


    GraphicLimits[0].Max =  TP_HIGH_DEFAULT;
    GraphicLimits[0].Min =  TP_LOW_DEFAULT;
    GraphicLimits[0].Step = TP_STEP_DEFAULT;

    GraphicLimits[1].Max =  TD_HIGH_DEFAULT;
    GraphicLimits[1].Min =  TD_LOW_DEFAULT;
    GraphicLimits[1].Step = TD_STEP_DEFAULT;

    GraphicLimits[2].Max =  E_HIGH_DEFAULT;
    GraphicLimits[2].Min =  E_LOW_DEFAULT;
    GraphicLimits[2].Step = E_STEP_DEFAULT;

    GraphicLimits[4].Max =  AC_HIGH_DEFAULT;
    GraphicLimits[4].Min =  AC_LOW_DEFAULT;
    GraphicLimits[4].Step = AC_STEP_DEFAULT;

    GraphicLimits[5].Max =  CG_HIGH_DEFAULT;
    GraphicLimits[5].Min =  CG_LOW_DEFAULT;
    GraphicLimits[5].Step = CG_STEP_DEFAULT;

    ProfileLimits.Max = PROFILE_HIGH_DEFAULT;
    ProfileLimits.Min = PROFILE_LOW_DEFAULT;
    ProfileLimits.Step = PROFILE_STEP_DEFAULT;

    for (int i=0;i<MAX_OBJECT;i++)
    {
        All_Limits[i].TpHigh = TP_HIGH_DEFAULT;
        All_Limits[i].TpLow =  TP_LOW_DEFAULT;
        All_Limits[i].TdHigh = TD_HIGH_DEFAULT;
        All_Limits[i].TdLow =  TD_LOW_DEFAULT;
        All_Limits[i].EHigh =  E_HIGH_DEFAULT;
        All_Limits[i].ELow =   E_LOW_DEFAULT;

        All_Limits[i].Ac2High = AC2_HIGH_DEFAULT;
        All_Limits[i].AcHigh = AC_HIGH_DEFAULT;
        All_Limits[i].AcLow =  AC_LOW_DEFAULT;
        All_Limits[i].CgHigh = CG_HIGH_DEFAULT;
        All_Limits[i].CgLow =  CG_LOW_DEFAULT;

        All_Limits[i].EReg =  (i < 3)?Cg_REG_DEFAULT:E_REG_DEFAULT;
        All_Limits[i].TpReg =  TP_REG_DEFAULT;

        All_Limits[i].Cg_AcReg =  CG_AC_DEFAULT;


    }
}


//------------------------------------------------------------------------------
//-- ��������� �� �������
bool sOptions::LoadFromRegistry(bool SetDefaultOnError)
{
    char* cKey = "Options";
    //-- ������ �� ������� ��������� ������������ ���������� ����
    if (!GetRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions)))
    {
        if (SetDefaultOnError)
        {
            SetDefault();
            return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions));
        }
        else return false;
    }
    return true;
}

//------------------------------------------------------------------------------
//-- ��������� � ������
bool sOptions::StoreToRegistry()
{
    char* cKey = "Options";
    return PutRegBinData(cFlameCtrlRegKey,cKey,this,sizeof(sOptions));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---------------------------------------------------------------------------
//      ������ ���������� ����������
//---------------------------------------------------------------------------

#ifdef  EXTEND_SYSTEMS
sConfItem LinkParam[]=
{
    {"����� ��������",0000,0,1000},
    {"COM ����",1,1,2},
    {"�������� ������",9600,300,115000}
};

sConfItem CtrlParam[]=
{
    {"����� �����������",0000,0,1000},
    {"����������� �������� ��� � ������� �����",0000,0,1000},
    {"��������� �������� ��� ��� ���������� ������",0000,0,1000},

    {"����������� �������� ����������� � ������� �����",0000,0,1000},
    {"������� �������� ����������� ��������� � ������� �����",0000,0,1000},
    {"����������� �������� ��������� ���������",0000,0,1000},
    {"����������� �������� ��������� ���",0000,0,1000},
    {"��������� �������� ��� ��������� ��� ������� �����������(��)",0000,0,1000},
    {"������ ���, ��������������� �������� �������� �����������",0000,0,1000},
    {"������ ������ ������������ ���, ��",0000,0,1000},

    {"���������� ���������� �����������, ��� ������� ��� ����� ���������� ������",0000,0,1000},
    {"����������� ��������� ����������",0000,0,1000},
    {"�������� �� ���������",0000,0,1000}
};

sBlockConf Config[]=
{
    {"��������� �����",&LinkParam[0],3},
    {"��������� �����������",&CtrlParam[0],13}
};
#endif

sConfItem OpControlParam[]=
{
    {"����������� ����, ����. �",0,00,1500},
    {"����������� �������, ����. �",0,00,1500},
    {"���, ��",0000,0,1000},
    {"������������ CO, %",25,0,2},
    {"���������� �������� Ac",0,0,1500},
    {"���������� ��������� ��, %",0,0,100},

    {"� ���� ������� ������, ����. �",510,1,1500},
    {"� ���� ������ ������, ����. �",510,1,1500},
    {"� ������� ������� ������, ����. �",510,1,1500},
    {"� ������� ������ ������, ����. �",510,1,1500},
    {"��� ������� ������, ��",510,1,1500},
    {"��� ������ ������, ��",510,1,1500},
    {"Ac ������ ������� ������",510,1,1500},
    {"Ac ������� ������",510,1,1500},
    {"Ac ������ ������",510,1,1500},
    {"�� ������� ������, %",510,1,1500},
    {"�� ������ ������, %",510,1,1500},
};

sBlockConf OpControl[]=
{
    {"����������� ��������",&OpControlParam[0],17}
};

sConfItem OpOxidFuelParam[]=
{
    {"�������",1,0,10},
    {"�������",4,0,10},
    {"��������",0,0,10},
    {"����",0,0,10},
};

sBlockConf OpOxidFuel[]=
{
    {"����������� ��������",&OpOxidFuelParam[0],4}
};

sConfItem OpTpParam[]=
{
    {"������� ������",  1000, 100, 10000},
    {"������ ������",   0,    0,   1000},
};
sBlockConf OpTp[]=
{
    {"����������� ��������",&OpTpParam[0],2}
};


TSourceName *pSourceName;
TID_value   *pID_value;

//---------------------------------------------------------------------------
char *SourceName[]=
{
    "",
    "���������� 1",
    "���������� 2",
    "���������� 3",
    "���������� 4",
    "���������� 5",
    "���� 1",
    "���� 2",
    "���� 3",
    "���� 4",
    "���� 5",
    "������������� 1",
    "������������� 2",
    "������������� 3",
    "������������� 4",
    "������������� 5",
    "����������� 1",
    "����������� 2",
    "����������� 3",
    "���������� ����",
    "��������",
    "��������� ����",
    "����������� ��������"
};

//---------------------------------------------------------------------------
__fastcall TSourceName::TSourceName(bool aFlStore){
    FlStore = aFlStore;
    LoadFromRegistry();
}
//---------------------------------------------------------------------------
__fastcall TSourceName::~TSourceName(){
    StoreToRegistry();
}

//---------------------------------------------------------------------------
void __fastcall TSourceName::ReNameSource(AnsiString as, int Index)
{
    if(Count > Index)
//       SourceName
       Strings[Index] = as;
}

//---------------------------------------------------------------------------
bool __fastcall TSourceName::StoreToRegistry(){
    AnsiString aCount;
    AnsiString aSTR;
    if(FlStore){
        AnsiString SNKey = cFlameCtrlRegKey + AnsiString("\\SourceName");
        //-- ������ � ������ �������� ���������� �����
        aCount = IntToStr(Count);
        PutRegParam(SNKey,"Count",aCount);

        for(int i=0; i<Count;i++){
            aSTR = Strings[i];
            PutRegParam(SNKey,"SN"+IntToStr(i),aSTR);
        }
    }
    return true;
}

//---------------------------------------------------------------------------
bool __fastcall TSourceName::LoadFromRegistry(){
    AnsiString aCount;
    AnsiString aSTR;
    int iCount;
    AnsiString SNKey = cFlameCtrlRegKey + AnsiString("\\SourceName");
        //-- ������ �� ������� �������� ���������� �����
    if (!GetRegParam(SNKey,"Count",aCount))
    {
        aCount = IntToStr(eUnknown);
        iCount = eUnknown;
        PutRegParam(SNKey,"Count",aCount);
    }
    else iCount = aCount.ToInt();

    if(iCount < eUnknown)
        iCount = eUnknown;

    for(int i=0; i<iCount;i++){
        if (!GetRegParam(SNKey,"SN"+IntToStr(i),aSTR))
        {
            aSTR = SourceName[i];
            PutRegParam(SNKey,"SN"+IntToStr(i),aSTR);
        }
        Add(aSTR);
    }
    return true;
}

//------------------------------------------------------------------------------
__fastcall TID_value::TID_value(bool aFlStore){
    FlStore = aFlStore;
    LoadFromRegistry();
}
__fastcall TID_value::~TID_value(){
    StoreToRegistry();
}

bool __fastcall TID_value::StoreToRegistry(){
    AnsiString aCount;
    AnsiString aSTR;
    if(FlStore){
//        int Count = UnitManager->UnitList->Count;
        AnsiString SNKey = cFlameCtrlRegKey + AnsiString("\\ID_Value");
        //-- ������ � ������ �������� ���������� �����
        aCount = IntToStr(Count);
        PutRegParam(SNKey,"Count",aCount);

        for(int i=0; i<Count;i++){
            aSTR = IntToStr(iObjectId[i]);
            PutRegParam(SNKey,"ID"+IntToStr(i),aSTR);
        }    
    }    
    return true;
}

bool __fastcall TID_value::LoadFromRegistry(){
    AnsiString aCount;
    AnsiString aSTR;
//    int iCount;                            
    AnsiString SNKey = cFlameCtrlRegKey + AnsiString("\\ID_Value");
        //-- ������ �� ������� �������� ���������� �����
    if (!GetRegParam(SNKey,"Count",aCount))
    {
        aCount = IntToStr(6);
        Count = 6;
        PutRegParam(SNKey,"Count",aCount);
    }
    else Count = aCount.ToInt();

    for(int i=0; i<Count;i++){
        if (!GetRegParam(SNKey,"ID"+IntToStr(i),aSTR))
        {
            aSTR = iObjectId[i];
            PutRegParam(SNKey,"ID"+IntToStr(i),aSTR);
        }
        iObjectId[i] = aSTR.ToInt();
    }    
    return true;
}
//------------------------------------------------------------------------------
char *ParamName[] =
{
    "����������� �������� ����, ����.C",
    "����������� �������, ����.C",
    "��� �������, ��",
    "�������� ������������ CO",
    "���������� ��������",
    "���������� ���������, %"
};

//-- ���� � ������� ��� �������� ���������� ������������
char*           cFlameCtrlRegKey = "Software\\AutoPlus\\FlameCtrl";
//sArmParam       all_param[MAX_OBJECT];      //-- ��������� ���� �������
//sArmParam       back_param[MAX_OBJECT];     //-- ����������� ���������
sControlParam   ctrl_param;                 //-- ��������� ����������
//sProfileParam   profile[MAX_PROFILE];       //-- ������ ��������
//bool            is_profile[MAX_PROFILE];    //-- ������� ������������ �������
//sProgParam      prog_profile[MAX_PROFILE];  //-- ��������� ������������ ��������
//TDateTime       ParamVisibleInt;            //-- ��������� �������� ���������� ����������
TDateTime       EventVisibleInt;            //-- ��������� �������� ���������� �������
sOptions        Options;                    //-- ����� ���������
TList*          ParamList;                  //-- ������������ ������ ����������
//TList*          EventList;                  //-- ������������ ������ �������



//---------------------------------------------------------------------------
//      ������ ��������
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ ����������
//---------------------------------------------------------------------------
/*
void __fastcall ClearParamList(TList* ParamList)
{
    sArmParam *p;
    for (int i = 0; i < ParamList->Count; i++)
    {
        p = (sArmParam *)ParamList->Items[i];
        delete []p;
    }
    ParamList->Clear();
}
*/
/*
//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ �������
//---------------------------------------------------------------------------
void __fastcall ClearEventList(TList* pList)
{
    sArmEvent *p;
    for (int i = 0; i < pList->Count; i++)
    {
        p = (sArmEvent *)pList->Items[i];
        delete p;
    }
    pList->Clear();
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� �������� � ������������� ������
//---------------------------------------------------------------------------
/*
void __fastcall AddParamList(TList* ParamList, sArmParam* all_param)
{
    sArmParam *p = new sArmParam[MAX_OBJECT];
    memcpy(p,all_param,sizeof(sArmParam)*MAX_OBJECT);
    ParamList->Add (p);

    //-- ���� �� ������ ��� ������ ������ �� ������
    if (ParamList->Count > 1)
    while (1)
    {
        p = (sArmParam *)ParamList->Items[0];
        if (p->Time < (all_param->Time - ParamVisibleInt) )
        {
            delete []p;
            ParamList->Delete(0);
        }
        else break;
    }
}
*/
//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� �� ������������� ������
//---------------------------------------------------------------------------
/*
int* __fastcall FuncData(TList* ParamList, int ObjectIdx, int ParamIdx, int Mul, int &Count)
{
    Count = ParamList->Count;
    static int *p_int;
    p_int = new int[Count];
    sArmParam v_all_param[MAX_OBJECT];

    for (int i=0;i<Count;i++)
    {
        memcpy(v_all_param,ParamList->Items[i],sizeof(sArmParam)*MAX_OBJECT);

        //-- ���� � ���� Source ������� "��� ������" - ��������� ��� � ��������
        if (v_all_param[ObjectIdx].Source == UNKNOWN_DATA) *(p_int+i) = UNKNOWN_DATA;
        else
        *(p_int+i) = (int)(v_all_param[ObjectIdx].Param[ParamIdx]*(double)Mul);
    }
    return p_int;
}

*/
//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� CXT �� ������ double
//---------------------------------------------------------------------------
/*
int* __fastcall CXTFuncData(double* Ptr, int Mul, int Count)
{
    static int *p_int;
    p_int = new int[Count];

    for (int i=0;i<Count;i++)
        *(p_int+i) = (int)((*(Ptr+i))*(double)Mul);

    return p_int;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� ���������� �������� ������� �� ������� � ���������
//---------------------------------------------------------------------------
//-- Interval - ����� ������ ����������� � ��
/*
int __fastcall SamplesOfPeriod (TDateTime StartTime, TDateTime EndTime, int Interval)
{
    double delta = (double)(EndTime - StartTime);
    if (delta < 0) return 0;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    if ((double)TimeStep == 0) return 0;
    return (int) (delta/(double)TimeStep);
}
*/
//---------------------------------------------------------------------------
//-- ��������� �������� ������ ��������� ����� � ����������� ��������� "��� ������"
//---------------------------------------------------------------------------
/*
bool __fastcall CreateUnknowParamList(TList *ParamList, int Count)
{
    for (int i = 0;i<Count; i++)
    {
        sArmParam *p = new sArmParam[MAX_OBJECT];
        if (!p) return false;
        memset(p,0,sizeof(sArmParam) * MAX_OBJECT);
        for (int j=0;j<MAX_OBJECT; j++) (p + j)->Source = UNKNOWN_DATA;
        ParamList->Add (p);
    }
    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������/���������� ���������� ������� ������ ��� ������� (������)
//---------------------------------------------------------------------------
/*
bool __fastcall SetDataParamList(TList *ParamList, int Idx, sArmParam* one_param, int ObjectIdx)
{
    sArmParam* list_param;

    //-- ������� ����� ����� ���������� (��� ���� ��������)
    list_param = (sArmParam*)ParamList->Items[Idx];
    if (!list_param) return false;

    memcpy((list_param + ObjectIdx),one_param,sizeof(sArmParam));

    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� ������� ��� ���������� �� ������� � ���������
//---------------------------------------------------------------------------
/*
bool __fastcall GetIndexFromTime(sArmParam* one_param,TDateTime StartTime,int Interval, int &Idx)
{
    if (Interval == 0) return false;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    double dIdx = (double)(one_param->Time - StartTime) / (double)TimeStep;
    if (dIdx < 0) return false;
    else Idx = (int)dIdx;
    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ���������� ������� ��� ����� ���������� �� ������� � ���������
//---------------------------------------------------------------------------
bool __fastcall GetIndexFromOptimTime(sOptimalParam* optim_param,TDateTime StartTime,int Interval, int &Idx)
{
    if (Interval == 0) return false;

    TDateTime TimeStep (0,0,
        (unsigned short)(Interval/1000),
        (unsigned short)(Interval%1000));

    double dIdx = (double)(optim_param->Time - StartTime) / (double)TimeStep;
    if (dIdx < 0) return false;
    else Idx = (int)dIdx;
    return true;
}

//---------------------------------------------------------------------------
//-- ������������ ��������� --------------------------------------------------
//---------------------------------------------------------------------------
/*
bool __fastcall GenerateParam(sArmParam &param,int Source)
{
    param.Time = TDateTime::CurrentDateTime();
    param.Source = Source;
    for (int i=0;i<MAX_PARAM;i++) param.Param[i] = 0.0;
    return true;
}
*/
//---------------------------------------------------------------------------
//-- ��������� �������� ���� ���������� �������� � ���������������� ���������
//---------------------------------------------------------------------------
/*
bool __fastcall PackParam(sArmParam* all_param,sOptimalParam* optim_param)
{
    //-- ����� �������� �� �������
    optim_param->Time = (all_param)->Time;

    //-- ������������ ��������� �����
    for (int i=0;i<3;i++)
    {
        optim_param->Param1[i].Tp = (all_param+i)->Param[0];
        optim_param->Param1[i].Td = (all_param+i)->Param[1];
        optim_param->Param1[i].E  = (all_param+i)->Param[2];
        optim_param->Param1[i].CO = (short int)((all_param+i)->Param[3]*100);
        optim_param->Param1[i].Ac = (all_param+i)->Param[4];
        optim_param->Param1[i].Cg = (all_param+i)->Param[5];
    }

    //-- ������������ ��������� ���������������
    for (int i=0;i<3;i++)
    {
        optim_param->Param2[i].Tp = (all_param+3+i)->Param[0];
        optim_param->Param2[i].Td = (all_param+3+i)->Param[1];
        optim_param->Param2[i].E  = (all_param+3+i)->Param[2];
        optim_param->Param2[i].CO = (short int)((all_param+3+i)->Param[3]*100);
        optim_param->Param2[i].Ac = (all_param+3+i)->Param[4];
    }

    return true;
}
*/

//��������������� ������ ��������� �������� � ������ ���������� ����������� �����������
double __fastcall CorrectDigits(TEdit *Edit)
{
    static AnsiString aRes;
    char ch[2];
    AnsiString aStr = Edit->Text;
    ch[1] = 0;
    aRes = "";

    for (int i=1;i< (aStr.Length()+1);i++)
    {
        ch[0] = aStr[i];
        if (ch[0] >= '0' && ch[0] <= '9') aRes += AnsiString (ch);
        if (ch[0] == '.' || ch[0] == ',') aRes += DecimalSeparator;	
    }
    return aRes.ToDouble(); 
}

//-- ������������ ����������� ������� �� �������� ��������� -----------------
char* __fastcall DoubleToComment(double Value)
{
    static char Msg[100];
    sprintf (Msg,"%6.3f",Value);
    return Msg;
}

// �������� ��� ������ (��� ���� ������� ������ ��������)
//------------------------------------------------------------------------------
int  __fastcall GetLastErrorCode()
{
    int Res = LastErrorCode;
    LastErrorCode = eErrNoError;
    return Res;
}


