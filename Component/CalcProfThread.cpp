//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CalcProfThread.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall CalcProfThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
TCalcProfThread * CalcProfThread;
//---------------------------------------------------------------------------
__fastcall TCalcProfThread::TCalcProfThread(bool CreateSuspended,TMultiGrapher *aMultiGrapher)
        : TThread(CreateSuspended)
{
   MultiGrapher = aMultiGrapher;
   Finished = false;
}
//---------------------------------------------------------------------------
void __fastcall TCalcProfThread::Execute()
{
    do
    {
        Finished = false;
        MultiGrapher->FirstThreadPart();
        if(Terminated)
            return;
        MultiGrapher->SecondThreadPart();
        if(Terminated)
            return;
        MultiGrapher->ThirdThreadPart();
        if(Terminated)
            return;

        Synchronize(MultiGrapher->Repaint);
        Finished = true;
        Suspend();
    }while(!Terminated);
}

//---------------------------------------------------------------------------

