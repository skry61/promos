//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstcop

//#include "resource.h"
#include "FList.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
static inline void ValidCtrCheck(TFList *)
{
    new TFList(NULL);
}
//---------------------------------------------------------------------------
__fastcall TFList::TFList(TComponent* Owner): TGraphicControl(Owner)
{
        // ��������� ������� ���������� �� ���������
    Width = 64;
    Height = 48;
    FItemHeight = 26;
    FLines = new TStringList();
    FWarningLines = new TStringList();
    FErrorLines = new TStringList();
    FHeaderLines = new TStringList();

    ItemSpace = 26;

    FLines->Add("��������� ����������");
    ActiveItem = -1;
}

// ��������� ��������� ��������� �������
void __fastcall TFList::DoDrawText(AnsiString Text,RECT* dRect,bool IsHeader)
{
    Canvas->Font = Font;

    dRect->top += 4;
    if (!IsHeader)
    {
        OffsetRect(dRect, 1, 1);
        Canvas->Font->Color = clWhite;
        Canvas->TextOut(dRect->left,dRect->top,Text);
        OffsetRect(dRect, -1, -1);

    Canvas->Font->Color = clBlack;
    Canvas->TextOut(dRect->left,dRect->top,Text);

    }
    else
    {
    Canvas->Font->Color = clWhite;
    Canvas->TextOut(dRect->left,dRect->top,Text);
    }
    dRect->top -= 4;
}

void __fastcall TFList::Paint(void)
{
    TRect rct;
    TColor stdColor;
    TBrushStyle stdStyle;
    Canvas->Brush->Style = bsDiagCross;
    for (int i=0;i<FLines->Count;i++)
    {
        RECT R;
        R.right = Width;
        R.top = 5 + i*FItemHeight;
        R.bottom = R.top + FItemHeight - 1;

        if (DefineState(i) == lsHeader)
        {
            R.left = 0;
           // R.top -=6;
           // R.bottom -= 7;
            // ������ ����������
            rct = R;
            stdColor = Canvas->Brush->Color;
            stdStyle = Canvas->Brush->Style;
    	    Canvas->Brush->Color = clNavy;
            Canvas->Brush->Style = bsSolid;
                Canvas->FillRect(rct);
            Canvas->Brush->Color = stdColor;
            Canvas->Brush->Style = stdStyle;
            R.left = FItemSpace;
            // � ���������� �����
            DoDrawText(FLines->Strings[i],&R,true);
        }
        else
        {
            R.left = FItemSpace;
            DoDrawText(FLines->Strings[i],&R,false);
            Canvas->Font->Color = clBlack;
            //R.top-=5;
            R.left = 0;
        }
    }
}
FListState __fastcall TFList::DefineState(int Index)
{
    int i;
    for (i=0;i<FHeaderLines->Count;i++)
        if (FHeaderLines->Strings[i].ToInt()==Index) return lsHeader;

    if (Index == ActiveItem) return lsActive;
    if (Index > ActiveItem) return lsNone;

    for (i=0;i<FErrorLines->Count;i++)
        if (FErrorLines->Strings[i].ToInt()==Index) return lsNo;

    for (i=0;i<FWarningLines->Count;i++)
        if (FWarningLines->Strings[i].ToInt()==Index) return lsWarning;
    return lsYes;
}

void __fastcall TFList::Clear(void)
{
    FLines->Clear();
    WarningLines->Clear();
    ErrorLines->Clear();
//    Repaint();
}
// ......... ����������� ���������� � ������� ...................
//---------------------------------------------------------------------------
namespace Flist
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TFList)};
        RegisterComponents("Fx", classes, 0);
    }
}


