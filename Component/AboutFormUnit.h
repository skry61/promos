//---------------------------------------------------------------------------
#ifndef AboutFormUnitH
#define AboutFormUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TAboutForm : public TForm
{
__published:	// IDE-managed Components
    TImage *Image1;
    TBevel *Bevel1;
    TBitBtn *BitBtn1;
    TStaticText *StaticText1;
    TStaticText *StaticText2;
    TStaticText *StaticText3;
private:	// User declarations
public:		// User declarations
    __fastcall TAboutForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAboutForm *AboutForm;
//---------------------------------------------------------------------------
#endif
