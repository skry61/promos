//---------------------------------------------------------------------------
//      ������ �������������� � ������������ ���-��
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Link_ALT.h"
#include "Config.h"
#include "Params.h"
#include "DbUnit_EV.h"
//#include "MainUnit.h"


//------------------------------------------------------------------------------
#pragma package(smart_init)
#define min(a, b)  (((a) < (b)) ? (a) : (b))

//-- ��������� ��������� ���������� ------------------------------
//-- ��������� ����������
bool __fastcall ALT_Unit::LoadDll (AnsiString aDllName){
    // hChannelDll - ���������� ����������� ����������
    if(Fl_CommonLib) return true;
    hDLL = LoadLibrary(aDllName.c_str());
    if (!hDLL)
    {
        MessageBox (GetActiveWindow(),"���������� ��������� ��������� ����������","������!",MB_OK|MB_ICONERROR);
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall ALT_Unit::LoadDllFunction (void){

    pSetNode =          (void (__cdecl *)(short))
                                GetProcAddress(hDLL,"_SetNode");

    pSetSerialPort =    (void (__cdecl *)(char))
                                GetProcAddress(hDLL,"_SetSerialPort");

    if (!pSetNode  || !pSetSerialPort) return false;
    else return true;
}

//------------------------------------------------------------------------------
bool __fastcall ALT_Unit::LoadRegisterCOM(){
    AnsiString aCom;
    int Com;
    if(Fl_CommonLib) return true;

    //-- ������ �� ������� �������� ������ ��� ����� �����
    if (!GetRegParam(cFlameCtrlRegKey,"COM-ALT",aCom))
    {
        Com = COM_PORT_ALT_DEFAULT;
        PutRegParam(cFlameCtrlRegKey,"COM-ALT",IntToStr(COM_PORT_ALT_DEFAULT));
    }
    else Com = aCom.ToInt();

    pSetSerialPort((char)(Com));                //-- ���������� ����� ����� �����
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall ALT_Unit::LoadRegisterNodes (AnsiString aKeyName){
    AnsiString aStr;
    int Tmp;
    //-- ������ �� ������� �������� �������� �������
    if (!GetRegParam(cFlameCtrlRegKey,aKeyName + "Count",aStr))
    {
        Tmp = 1;
        PutRegParam(cFlameCtrlRegKey,aKeyName + "Count","1");
    }
    else Tmp = aStr.ToInt();
    if((Tmp > 0) && (Tmp < 80)) for(int i=0; i < Tmp;i++){

        //-- ������ �� ������� �������� ������ �������
        if (!GetRegParam(cFlameCtrlRegKey,aKeyName+IntToStr(i),aStr))
        {
            Nodes[i] = 80+i;
            PutRegParam(cFlameCtrlRegKey,aKeyName+IntToStr(i),IntToStr(80+i));
        }
        else Nodes[i] = aStr.ToInt();
    }

    //-- ������ �� ������� �������� ������ �������
    if (!GetRegParam(cFlameCtrlRegKey,aKeyName,aStr))
    {
        Tmp = 89;
        PutRegParam(cFlameCtrlRegKey,aKeyName,"89");
    }
    else Tmp = aStr.ToInt();

    pSetNode(Tmp);
    return true;
}

//------------------------------------------------------------------------------
//-- (���������� ����� ����� � ��������� �������������)-----------
bool __fastcall ALT_Unit::LoadChannelDll (AnsiString aDllFileName,AnsiString aKeyName)
{
    AnsiString aCom;
    if (!LoadDll (aDllFileName)) return false;
    if (!LoadDllFunction()){
        MessageBox (GetActiveWindow(),"���������� �������� ������ � ���������� ����������","������!",MB_OK|MB_ICONERROR);
        hDLL = NULL;
        return false;
    }
    LoadRegisterCOM();           // ������ �� ������� ����� COM �����
    LoadRegisterNodes(aKeyName); // ������ �� ������� ������ �������
    return true;
}

//------------------------------------------------------------------------------
int __fastcall ALT_Unit::GetParam(int Index,int *Ind){
    pSetNode(Nodes[Index]);
    if (Ind == NULL) return 0;
    return 0;//pGet_T();
}

//------------------------------------------------------------------------------
bool __fastcall ALT_Unit::FreeChannelDll (void)
{
    return FreeDll();
}

//------------------------------------------------------------------------------
bool __fastcall ALT_Unit::FreeDll (void)
{
    if(!Fl_CommonLib)
       return FreeLibrary(hDLL);
    else
       return true;
}

//------------------------------------------------------------------------------
__fastcall ALT_Unit::ALT_Unit(AnsiString aDllFileName,AnsiString aDllFileName1,
                                 AnsiString aKeyName,HINSTANCE ahDLL){
   AnsiString aStr;
   hDLL = ahDLL;
   aStr = aDllFileName1.UpperCase();
   if(aStr.AnsiCompare("EMULATOR.DLL") == 0){
       hDLL = NULL;
       Fl_CommonLib = true;
       Result = false;
       return;
   }
   if(aStr.AnsiCompare("PLC_TD_XCHG.DLL") == 0){
      Fl_CommonLib = true;
   }else{
      Fl_CommonLib = false;
   }
   Result = LoadChannelDll (aDllFileName,aKeyName);
}

//------------------------------------------------------------------------------
__fastcall ALT_Unit::~ALT_Unit(){
//    if(!Fl_CommonLib)
//       FreeLibrary(hDLL);
    FreeChannelDll();
}


//------------------------------------------------------------------------------
//==============================================================================
//------------------------------------------------------------------------------
bool __fastcall TTermoDat::LoadDllFunction (void){
     if (!ALT_Unit::LoadDllFunction()) return false;
     pGet_T    =         (int  (__cdecl *)())
                                  GetProcAddress(hDLL,"_Get_T");

     pSetUst =           (int (__cdecl *)(short))
                                  GetProcAddress(hDLL,"_SetUst");

     pExchangeTD =       (int  (__cdecl *)(int, int&, void *))
                                  GetProcAddress(hDLL,"_ExchangeTD");

    if (!pGet_T || !pSetUst || !pExchangeTD) return false;
    else return true;
}

//bool __fastcall TermoDat::LoadRegisterNodes (AnsiString aKeyName){

//}

__fastcall  TTermoDat::TTermoDat(AnsiString aDllFileName,HINSTANCE ahDLL,tSetMessage aSetMessage):
            ALT_Unit("TD_Exchange.DLL",aDllFileName,"TermoDat",ahDLL){
  SetMessage =  aSetMessage;
  SetMessage (false,"�������� ���������� ��������...");
  if(Result)
      Result = LoadDllFunction();
  for(int i = 0; i < 80; i++){
     ExchangeErrorFixed[i] = false;
     FDisabled[i] = false;
     FUst[i] = 0;
  }
}
//__fastcall  TermoDat::~TermoDat(){
//}

void __fastcall TTermoDat::FixErr(int Index,int Res){
    AnsiString aStr = IntToStr(Nodes[Index]);
    if(Res <= 0){
       if(Res == 0)
           SetMessage (true,"����� ����� � �������� ����� " + aStr );
       else
           SetMessage (true,"����� ��������� �� �������� ����� "+ aStr );

           //-- � ���� ������ �� ����������� - ������ �� 1 ��� � ������
       if (!ExchangeErrorFixed[Index]){
                        //-- � ��������� ��� ������� � ���� �������
/*
                if(Res == 0)
                   //-- ������ ����� � ��� �����������(��������)
                   GenerateEvent(eLostConnectALT,eTermodat,aStr.c_str());
                else
                   //-- ��� ����������(��������)������������� �� ������(�����)
                   GenerateEvent(eErrALT,eTermodat,aStr.c_str() );
*/
                ExchangeErrorFixed[Index] = true; //-- ��������
       }
    }else if(ExchangeErrorFixed[Index]){
       SetMessage (false,"����� � �������� ����� "+ aStr +" �������������");
       ExchangeErrorFixed[Index] = false;         //-- ������ �� �����������
    }
}



int __fastcall  TTermoDat::GetParam(int Index, int *Ind){
    int Res;
    *Ind = Nodes[Index];
    if(hDLL == NULL) return 888;
    if(FDisabled[Index])  return FUst[Index];
    pSetNode(Nodes[Index]);
    Res = pGet_T();
    FixErr(Index,Res);
//    Application->ProcessMessages();

/*
    if(Res <= 0){
       if(Res == 0)
           MainForm->SetMessage (true,"����� ����� � �������� ����� " + aStr );
       else
           MainForm->SetMessage (true,"����� ��������� �� �������� ����� "+ aStr );

           //-- � ���� ������ �� ����������� - ������ �� 1 ��� � ������
           if (!ExchangeErrorFixed[Index]){
                        //-- � ��������� ��� ������� � ���� �������
                if(Res == 0)
                   //-- ������ ����� � ��� �����������(��������)
                   GenerateEvent(event,eLostConnectALT,eTermodat,aStr.c_str());
                else
                   //-- ��� ����������(��������)������������� �� ������(�����)
                   GenerateEvent(event,eErrALT,eTermodat,aStr.c_str() );
                StoreEvent(event);
                ExchangeErrorFixed[Index] = true; //-- ��������
           }
    }else if(ExchangeErrorFixed[Index]){
       MainForm->SetMessage (false,"����� � �������� ����� "+ aStr +" �������������");
       ExchangeErrorFixed[Index] = false;         //-- ������ �� �����������
    }
*/
    return Res;
}


int __fastcall TTermoDat::SetUst(int V,int Index){
    int Res;
    if(hDLL == NULL) return 0;
    if(FDisabled[Index]){
        return FUst[Index] = V;
    }    
    if(FUst[Index] != V){
       pSetNode(Nodes[Index]);
       Res = pSetUst(V);
       FixErr(Index,Res);
       // ���� ��� ������
       if(Res)
          FUst[Index] = V;
    }
    Application->ProcessMessages();
    return Res;
}

 int __fastcall TTermoDat::Disable(int Index){
     FDisabled[Index]  = true;
     return 0;
}
