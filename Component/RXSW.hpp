// Borland C++ Builder
// Copyright (c) 1995, 2002 by Borland Software Corporation
// All rights reserved

// (DO NOT EDIT: machine generated header) 'RXSw.pas' rev: 6.00

#ifndef RXSwHPP
#define RXSwHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <Menus.hpp>	// Pascal unit
#include <ExtCtrls.hpp>	// Pascal unit
#include <StdCtrls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <SysUtils.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Rxsw
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TTextPos { tpRight, tpLeft, tpAbove, tpBelow, tpNone };
#pragma option pop

typedef Set<bool, 0, 1>  TSwitchBitmaps;

class DELPHICLASS TrxSwitch_;
class PASCALIMPLEMENTATION TrxSwitch_ : public Controls::TCustomControl 
{
	typedef Controls::TCustomControl inherited;
	
private:
	bool FActive;
	Graphics::TBitmap* FBitmaps[2];
	Classes::TNotifyEvent FOnOn;
	Classes::TNotifyEvent FOnOff;
	bool FStateOn;
	TTextPos FTextPosition;
	Forms::TFormBorderStyle FBorderStyle;
	Classes::TShortCut FToggleKey;
	bool FShowFocus;
	TSwitchBitmaps FUserBitmaps;
	HIDESBASE MESSAGE void __fastcall CMDialogChar(Messages::TWMKey &Message);
	HIDESBASE MESSAGE void __fastcall CMFocusChanged(Controls::TCMFocusChanged &Message);
	MESSAGE void __fastcall CMTextChanged(Messages::TMessage &Message);
	void __fastcall SetStateOn(bool Value);
	void __fastcall SetTextPosition(TTextPos Value);
	void __fastcall SetBorderStyle(Forms::TBorderStyle Value);
	Graphics::TBitmap* __fastcall GetSwitchGlyph(int Index);
	void __fastcall SetSwitchGlyph(int Index, Graphics::TBitmap* Value);
	bool __fastcall StoreBitmap(int Index);
	void __fastcall SetShowFocus(bool Value);
	void __fastcall ReadBinaryData(Classes::TStream* Stream);
	void __fastcall WriteBinaryData(Classes::TStream* Stream);
	
protected:
	virtual void __fastcall CreateParams(Controls::TCreateParams &Params);
	virtual void __fastcall DefineProperties(Classes::TFiler* Filer);
	DYNAMIC void __fastcall MouseDown(Controls::TMouseButton Button, Classes::TShiftState Shift, int X, int Y);
	DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
	virtual void __fastcall Paint(void);
	DYNAMIC void __fastcall DoOn(void);
	DYNAMIC void __fastcall DoOff(void);
	
public:
	__fastcall virtual TrxSwitch_(Classes::TComponent* AOwner);
	__fastcall virtual ~TrxSwitch_(void);
	void __fastcall ToggleSwitch(void);
	
__published:
	__property Align  = {default=0};
	__property Forms::TBorderStyle BorderStyle = {read=FBorderStyle, write=SetBorderStyle, default=0};
	__property Caption ;
	__property Color  = {default=-2147483643};
	__property Cursor  = {default=0};
	__property DragKind  = {default=0};
	__property DragMode  = {default=0};
	__property DragCursor  = {default=-12};
	__property Font ;
	__property Graphics::TBitmap* GlyphOff = {read=GetSwitchGlyph, write=SetSwitchGlyph, stored=StoreBitmap, index=0};
	__property Graphics::TBitmap* GlyphOn = {read=GetSwitchGlyph, write=SetSwitchGlyph, stored=StoreBitmap, index=1};
	__property ParentColor  = {default=1};
	__property ParentFont  = {default=1};
	__property ParentShowHint  = {default=1};
	__property PopupMenu ;
	__property bool ShowFocus = {read=FShowFocus, write=SetShowFocus, default=1};
	__property Classes::TShortCut ToggleKey = {read=FToggleKey, write=FToggleKey, default=32};
	__property ShowHint ;
	__property bool StateOn = {read=FStateOn, write=SetStateOn, default=0};
	__property TabOrder  = {default=-1};
	__property TabStop  = {default=1};
	__property TTextPos TextPosition = {read=FTextPosition, write=SetTextPosition, default=4};
	__property OnClick ;
	__property OnDblClick ;
	__property OnMouseMove ;
	__property OnMouseDown ;
	__property OnMouseUp ;
	__property OnKeyDown ;
	__property OnKeyUp ;
	__property OnKeyPress ;
	__property OnDragOver ;
	__property OnDragDrop ;
	__property OnEndDrag ;
	__property Classes::TNotifyEvent OnOn = {read=FOnOn, write=FOnOn};
	__property Classes::TNotifyEvent OnOff = {read=FOnOff, write=FOnOff};
public:
	#pragma option push -w-inl
	/* TWinControl.CreateParented */ inline __fastcall TrxSwitch_(HWND ParentWindow) : Controls::TCustomControl(ParentWindow) { }
	#pragma option pop
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Rxsw */
using namespace Rxsw;
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// RXSw
