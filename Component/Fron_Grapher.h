//---------------------------------------------------------------------------
#ifndef Fron_GrapherH
#define Fron_GrapherH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include "Grapher.h"
#include <SyncObjs.hpp>

#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#define PEN_COLOR_MAX  100
//---------------------------------------------------------------------------
class PACKAGE TFron_Grapher : public TGrapher {
private:
    int             FVDivisor;
    int             FHDivisor;
    int             FEndV;
    int             FEndH;
    int             FV_Disp;
    int             FH_Disp;
    int             NewPenIndex;
    TColor          PenColorArray[PEN_COLOR_MAX]; // �����
    int             PenColorFIX  [PEN_COLOR_MAX]; // ����� ������ ������ �����

    TColor          FLimitColor;         //-- ���� ����� �0 �������
    int             FLimitLow;
    int             FF_Value;
	TList           *DataList;

	void __fastcall SetVDivisor (int iDivisor);
	void __fastcall SetHDivisor (int iDivisor);
	void __fastcall SetEndV (int iEnd);
	void __fastcall SetEndH (int iEnd);
	void __fastcall SetV_Disp (int Value);
	void __fastcall SetH_Disp (int Value);
	void __fastcall SetLimitLow (int iLow);
	void __fastcall SetF_Value (int Value);
	void __fastcall SetW_Value (int Value);
	void __fastcall DrawLabel(TCanvas *cnv, TRect GraphRect1);
	int  __fastcall XFunconGrapher (int x_real);
	int  __fastcall YFunconGrapher (int y_real);
//    int __fastcall  YonGrapher (int y_real);
//    int __fastcall  XonGrapher (int y_real);

	TCriticalSection	*CS;					// ����������� ������ ��� �������
	char AxisName1,AxisName2;					// ����� ����


protected:
public:

	__fastcall TFron_Grapher(TComponent* Owner);
    __fastcall ~TFron_Grapher();
    void __fastcall SetMarkerX (int X)  {    FH_Marker = X + FHMin;  }
    void __fastcall SetMarkerY (int Y)  {   MarkerY = Y;   }
    void __fastcall DrawStatMarker(void){ DrawMarker((AlterPaint)?AlterCanvas:Canvas);  }
    bool __fastcall Add(TPoint *Point);       //�������� ����� �����
	bool __fastcall Add(int x, int y);        //�������� ����� �����

    virtual void __fastcall Paint(void);
    void __fastcall ClearData(void);
    void AddColor(TColor NewPenColor);
    void AddColor(void);
    void ChangePenColor(TColor NewPenColor);  //������� ��� ������
	void __fastcall ShowData(void);

	void __fastcall ReNameAxis(char Ax1,char Ax2){AxisName1 = Ax1;AxisName2=Ax2;};


__published:

    __property int VDivisor = {read=FVDivisor, write=SetVDivisor, stored = true};
    __property int HDivisor = {read=FHDivisor, write=SetHDivisor, stored = true};

    __property int EndV = {read=FEndV, write=SetEndV, stored = true};
    __property int EndH = {read=FEndH, write=SetEndH, stored = true};

    __property int W_Value = {read=FW_Value, write=SetW_Value, stored = true};
    __property int F_Value = {read=FF_Value, write=SetF_Value, stored = true};
    __property int V_Disp = {read=FV_Disp, write=SetV_Disp, stored = true};
    __property int H_Disp = {read=FH_Disp, write=SetH_Disp, stored = true};

    __property int LimitLow = {read=FLimitLow, write=SetLimitLow, stored = true};
    __property TColor LimitColor = {read=FLimitColor, write=FLimitColor, default=clBlue};
    __property TColor PenColor = {read=DFColor, write=DFColor, default=clBlack};

};
//---------------------------------------------------------------------------
#endif
