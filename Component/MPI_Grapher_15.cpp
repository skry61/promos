//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#pragma link "Grapher"
#include "MPI_Grapher.h"
#include <stdio.h>
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
static inline void ValidCtrCheck(TMPIGrapher *)
{
    new TMPIGrapher(NULL);
}
//---------------------------------------------------------------------------
__fastcall TMPIGrapher::TMPIGrapher(TComponent* Owner)
    : TGrapher(Owner)
{
    FHDivisor = 1;
    FVDivisor = 1;
    LimitColor = clRed;
    DataList = new TList;
    NewPenIndex=0;

}
void __fastcall TMPIGrapher::ClearData(void) {           //������� ��� ������
    TPoint *p;
    for (int i = 0; i < DataList->Count; i++)
    {
        p = (TPoint *)DataList->Items[i];
        delete p;
//        delete []p;
    }
    DataList->Clear();
    NewPenIndex=0;
}
//---------------------------------------------------------------------------
__fastcall TMPIGrapher::~TMPIGrapher(void){
    ClearData();                           
    delete DataList;         
}


//---------------------------------------------------------------------------
void __fastcall TMPIGrapher::Add(int x, int y) { //TPoint *Point){       //�������� ����� �����
    if(DataList->Count == 0)
        DataList->Add(new TPoint(x,y)/*Point*/);
    else {
        TPoint *Point_Last = (TPoint *)DataList->Last();
    //    TPoint *Point = new TPoint(x,y);
    //    new TPoint(x,y);
    //    (PAList) DataList->Last()
        if((Point_Last->x != x) || (Point_Last->y != y))
            DataList->Add(new TPoint(x,y)/*Point*/);
    }
}

//---------------------------------------------------------------------------
void __fastcall TMPIGrapher::Add(TPoint *P) { //TPoint *Point){       //�������� ����� �����
	if(DataList->Count == 0)
		DataList->Add(P);
	else {
		TPoint *Point_Last = (TPoint *)DataList->Last();
		if((Point_Last->x != P->x) || (Point_Last->y != P->y))
		   DataList->Add(P);
	}
}

//---------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetVDivisor (int iDivisor)
{
	if (iDivisor <= 0) iDivisor = 1;
	FVDivisor = iDivisor;
	Repaint();
}

//---------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetHDivisor (int iDivisor)
{
	if (iDivisor <= 0) iDivisor = 1;
	FHDivisor = iDivisor;
	Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetV_Disp (int Value)
{
	FV_Disp = Value;
	Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetH_Disp (int Value)
{
	FH_Disp = Value;
	Repaint();
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetEndV (int iEnd)
{
	FEndV = iEnd;
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetEndH (int iEnd)
{
	FEndH = iEnd;
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetLimitLow (int iEnd)
{
	FLimitLow = iEnd;
}
//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetF_Value (int Value)
{
	FF_Value = Value;
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::SetW_Value (int Value)
{
	FW_Value = Value;
}

//------------------------------------------------------------------------------
int __fastcall TMPIGrapher::YFunconGrapher (int y_real)
{
	return GraphRect.Bottom - (int)(kY*(float)( (float)y_real/1000  - (float)VMin));
}

//------------------------------------------------------------------------------
int __fastcall TMPIGrapher::XFunconGrapher (int x_real)
{
	return GraphRect.Left + (int)(kX*(float)((float)x_real/1000 - (float)HMin));
}

//------------------------------------------------------------------------------
void __fastcall TMPIGrapher::Paint(void)
{
	TPoint *p;
	TCanvas *cnv = StartPaint();
	int PointCount;

	int intCount = DataList->Count;
	if(intCount != 0)
	{
		PrepareCalc();

		cnv->Pen->Style  = psSolid;
		cnv->Pen->Width = FLineWidth;
		cnv->Pen->Color = DFColor;                     //���� �����

		int X,Y;
		TPoint *p = (TPoint *)DataList->Items[0];
		//��������� �����
		X = XFunconGrapher (p->x+FH_Disp); Y = YFunconGrapher (p->y+FV_Disp);
		cnv->MoveTo (X,Y);
		int i=1;
		for(int j=0;j<=NewPenIndex;j++)
		{
			if(NewPenIndex == 0)
			{
			   PointCount = intCount;
			}else
			{
			   if(NewPenIndex != j)
				  PointCount = PenColorFIX[j];
			   else
				  PointCount = intCount;
			}
			for (;i<PointCount;i++){
				p = (TPoint *)DataList->Items[i];
				X = XFunconGrapher (p->x+FH_Disp); Y = YFunconGrapher (p->y+FV_Disp);
				//����������
				cnv->LineTo (X,Y);
			}
			if(NewPenIndex != 0)
			{
			   cnv->Pen->Color = PenColorArray[j];
			}
		}
		PrepareMarker();                  //���������� ������ - �� �����������
		DrawLabel (cnv, GraphRect);
	}
	FinishPaint();
	if (FOnPaint) FOnPaint(this);
}

//---------------------------------------------------------------------------

//TColor cResultMarkerColor[3]={clWhite,clYellow,clLime};

//---------------------------------------------------------------------------
void __fastcall TMPIGrapher::DrawLabel(TCanvas *cnv, TRect GraphRect1)
{
	int X1,Y1;

	PrepareCalc();
	int divx = (FHMax-FHMin)/FHStep;
	int divy = (FVMax-FVMin)/FVStep;

	bool osy = (FHMin <= 0 && FHMax >=0)?true:false;
	bool osx = (FVMin <= 0 && FVMax >=0)?true:false;

    cnv->Pen->Width = 1;
    cnv->Pen->Color = FLineColor;
    cnv->Pen->Style  = psDot;
    cnv->Brush->Style = bsClear;

    double dt;
    Extended Value;
    int i;
    for (i = 0; i<divx; i++)
    {
        X1 = XonGrapher (FHMin + FHStep*i);
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);

        dt = (double)(FHMin + i*FHStep) / (double)FHDivisor;
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
    }
    //��� ��� �� �
    X1 = XonGrapher (FHMin + FHStep*i);
    cnv->TextOut (X1-4,GraphRect1.Bottom+10,AnsiString("� ��"));

    for (i = 0; i<divy; i++)
    {
        Y1 = YonGrapher (VMin + FVStep*i);
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        Value = (FVMin + FVStep*i)/(double)FVDivisor;
        cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));
    }
    //��� ��� �� Y
//    i++;
    Y1 = YonGrapher (VMin + FVStep*i);
    cnv->TextOut (ClientRectEx.Left + 5,Y1-5,AnsiString("Y ��"));

    cnv->Pen->Style  = psSolid;

    if (!osy)   //-- ������� ��� ��� - �������� ��� Y
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Left,GraphRect.Top);
        DrawArrow(cnv,eToTop,GraphRect.Left,GraphRect.Top);
    }
    else        //-- ������� ��� ���� - �������� ��� Y
    {
        X1 = XonGrapher (0);
        //X1 = GraphRect1.Left + (int)(kX*(float)(-FHMin));
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        DrawArrow(cnv,eToTop,X1,GraphRect.Top);

        dt = (double)(FHMin) / (double)FHDivisor;
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,
            AnsiString::FloatToStrF (dt,AnsiString::sffFixed,5,2));
        //=dtTime = TDateTime(FTimeStart);
        //=cnv->TextOut (X1-4,GraphRect1.Bottom+10,dtTime.FormatString(FTimeFormat));
    }
    if (!osx)   //-- ������� ��� ��� - �������� ��� X
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Right,GraphRect.Bottom);
        DrawArrow(cnv,eToRight,GraphRect.Right,GraphRect.Bottom);
    }
    else
    {
        Y1 = YonGrapher (0);
        //Y1 = GraphRect1.Bottom - (int)(kY*(float)(-FVMin));
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);

        Value = (0);
        cnv->TextOut (ClientRectEx.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed,FValuePrecision,FValueDigits));

        DrawArrow(cnv,eToRight,GraphRect.Right,Y1);
    }
    //-- ���� ������ ������� ��������� ������� � ������ ����
    if (IsMarker && MarkerLinesCount) DrawMarker(cnv);  //-- SPACK
}

//---------------------------------------------------------------------------
void TMPIGrapher::AddColor(TColor NewPenColor)
{
    if(NewPenIndex<PEN_COLOR_MAX)
    {
       PenColorArray[NewPenIndex] = NewPenColor;
       PenColorFIX[NewPenIndex] = DataList->Count;
       NewPenIndex++;
    }
}
void TMPIGrapher::AddColor(void)
{
}

//---------------------------------------------------------------------------
void TMPIGrapher::ChangePenColor(TColor NewPenColor)
{
  //TODO: Add your source code here
  DFColor = NewPenColor;
}
namespace Mpi_grapher
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TMPIGrapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

