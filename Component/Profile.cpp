//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <math.h>


//#pragma link "CalcProfile"
#pragma link "FMath"
#include "Profile.h"
//#include "ParamMen.h"

//==============================================================================




//typedef struct sStadyInf TStadyInf;     

TStadyInf  StadyInfArray[STADYS];
TCalc_ProfFunc *Calc_ProfFunc; // ��������� �� ������� ��� ������ ������� ������� �������

//double *ProfArrayForAllStady;// ������ ������� ������� �� ��� ������

//-------------------   ������� ���������  -----------------------
double Const6      = 6.0;
double Const_T_abs = 273;
double Const_1t98  = 1.98;
double Const1t5    = 1.5;
double Const_0t5   = 0.5;
double Const_min1  = -1.0;
double Const_min2  = -2.0;
double Const_1     = 1.0;
double Const_0     = 0.0;
double Const_0t25  = 0.25;
double Const_2     = 2.0;
double Const1_E    = 31887.0;  // ������� ���������  (���/����)
double Const1_G    = 0.857;    // �����  G           (1/%)
//---------------   ������� ���������� ������  -------------------
double dTemper;              // �������� ��������� ����������� ��� ������� ������
			                 // ��/���

double CurPokExp;            //������� ���������� ����������
			                 // ��� ������ ������, ���� ������� ����.�����
			                 // ��������� Cg
double CurStartCg;           // ������� ��������� �������� Cg
double CurEndCg;             // ������� �������� �������� Cg
double CurBt;                // ������� �������� Betta
int    CurBoundCond;         // ������� ����� ���������� �������
int    CurCgLow;             // ������� ����� ������ ��������� Cg
			                 // 1-��������; 2-����
double CurT_Start;           // ������� �������� Betta
double CurStadyTime;         // ����� ������� ������

//---------------   ������� ���������� �������  ------------------
double ExpForD;              // ������� �������� ���������� ��� ������� �������
                             // D0*(1.0+G*D)*exp(-E/RTp)
double D0;                   // D0
                             


//-------------------------------------------------------------------------------------
// ����������� ��� ���������� ������     
__fastcall TStadyInf::TStadyInf(){
     PokExp = 1;        //��������� ������� ���������� ����������double ProfileArray[MAXPOINTS];     // ������ ������� �������
			            // ��� ������ ������, ���� ������� ����.�����double TmpDouble;                   // ������� ������ ������� ������� ������}
			            // ��������� Cgdouble StadyTimesArray[STADYS];     // ���������� ������
     StartCg = 1;       // ������� ��������� �������� Cgdouble PokExp[STADYS];             // //-------------------------------------------------------------------------------------
     EndCg = 1;         // ������� �������� �������� Cg//-------------------   ������� ���������  -----------------------//�������� ��������
     Bt = 1;            // ������� �������� Bettadouble Const6      = 6.0;double	Load_1(double aSumSteps){
     BoundCond = 3;     // ������� ����� ���������� �������double Const_T_abs = 273;      return  Const_1;
     CgLow = 1;         // ������� ����� ������ ��������� Cgdouble Const_1t98  = 1.98;}
				        // 1-��������; 2-����double Const1t5    = 1.5;
     TStart = 900;      // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;double	Load_0(double aSumSteps){
     TEnd = 900;        // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;				        // ��� ������� ������double Const_min1  = -1.0;      return  Const_0;
}

//-------------------------------------------------------------------------------------
//�������� ��������
long double	Load_1(double aSumSteps){
      return  Const_1;
}

long double	Load_0(double aSumSteps){
      return  Const_0;
}
long double	Load_0_(double aSumSteps){
      return  Const_0;
}

//--------------------------------------------------------------------------------------
//������ ������������ ��������
long double	sProfileParam::Calc_ExpForD(double aSumSteps){
      return  exp(-Const1_E/ (Const_1t98 * (CurT_Start + aSumSteps * dTemper)))*D0;
}
long double Calk_D (double Cg){
      return (1.0 + Const1_G * Cg)*ExpForD;
}

//--------------------------------------------------------------------------------------
//������ �����. Betta
long double Calc_B (double aSumSteps){
     // ��������� �� ���������� �������
     switch(CurBoundCond){
     case 1:                         // ������� ����
	     return (0.0);

     case 2:                         // 2-�� ����
	     return (-1.0);

     case 3:                         // 3-�� ����
	     return (-1.0 / CurBt);
     }    
     return (0.0);
}

//--------------------------------------------------------------------------------------
//����� ��������� 1/0
long double Choice_1_or_0(double aSumSteps){
     // ��������� �� ���������� �������
     if(CurBoundCond == 2)
	     return 0;
     else
	     return 1;
}

//--------------------------------------------------------------------------------------
//������ �������� �������� ����������� ����������
long double Calc_Cg (double aSumSteps){
     // ��������� �� ������ ��������� �g � ������� ������
     if(CurCgLow == 1)              // �������� �����
	      return CurStartCg + (CurEndCg - CurStartCg) * aSumSteps / CurStadyTime;
                                    // ���������������
//2b4c
	      long double Tmp = (exp(CurPokExp * aSumSteps) - Const_1)*
                              (CurEndCg - CurStartCg);
//2b7e
	      return Tmp / (exp(CurPokExp * CurStadyTime) - Const_1);
}

//--------------------------------------------------------------------------------------

void  sProfileParam::CulcData(PProc P7,PProc P6,PProc P5,PProc P4,PProc P3,PProc P2,PProc P1,
     double*PSumSteps, double* ProfData, int NPoints, double Xl, double lStep, int NSteps, int equ2)
{
     double Local_dX,Local_NPoints,Step_dXdX;
     double * DataPtr;
     double  Tmp,Tmp1,Tmp2,Tmp_e;
     double  Tmp_1e,Tmp_16;
     double  Tmp_2e,Tmp_26;
     double  Tmp_3e,Tmp_36;
     double  Tmp_4e,Tmp_46;
     double  Tmp_5e,Tmp_56;
     double  D_div_B;
     int i;
     
  Local_NPoints = NPoints;               //� �������� ���������� 50
  Local_dX = Xl / Local_NPoints;         //��������� ��� (2.5/50)  = 0.05
  Step_dXdX = lStep /Local_dX/Local_dX;  // (0.05/6)/(2.5/50)/(2.5/50) = 3.33333
  memcpy(Block3, ProfData,(NPoints+1) * sizeof(double));
  
  while(NSteps--){                       // ��� ������� ������ = 12, ��� �������  Tst/Step ~ 20
    *PSumSteps += lStep;                    // ���������� 0.05/6 - 0.008333
	DataPtr = ProfData;
	ExpForD = Calc_ExpForD(*PSumSteps); // ������  ���������� * D0 -  ����� �������
	for(i = 0; i < equ2; i++){              // ��� ����
	   long double _B = P5(*PSumSteps);     // Calc_B   - ������ Betta
	   long double  D = P7(*Block3);        // Calk_D() - ������ �������� ������������ ��������
	   long double  D_div_B = D * _B;
	   long double  Tmp0 = Const1t5 * D_div_B / Local_dX; //-0.172
	   Tmp =  P6(*PSumSteps)/*Choice 0  ��� 1*/ - Tmp0;
	   Tmp1 =  Const_2 * D_div_B / Local_dX;
	   Tmp2 = -Tmp1*Const_0t25;
//	   long double Tmp3 = P2(*PSumSteps);   // Load_1
	   //         Load_1             Calk_D   ?????  -NPoints �� ��������
	   D_div_B = P2(*PSumSteps) * P7(Block3[NPoints-1]);     // Calk_D() - ������ �������� ������������ ��������
//26c4
	   Tmp0 = D_div_B * Const1t5 / Local_dX;
	   Tmp_4e = P1(*PSumSteps) +Tmp0;    // Load_0_
//2703
	   Tmp_56 = Const_min2 * D_div_B / Local_dX;
	   Tmp_5e = -Tmp_56 * Const_0t25;
//2721
	   ProfData = DataPtr;    //� ������ 
//275c               D
	   Tmp_3e = P7((*Block3++ + *Block3)*Const_0t5) * Step_dXdX;
//279a
	   Tmp_36 = P7((*Block3++ + *Block3)*Const_0t5) * Step_dXdX;
//27ad
	   Tmp_2e = Tmp_3e + Tmp_36 + 1;
//27c3     ���������
	   D_div_B = Tmp - Tmp_3e * Tmp2 / Tmp_36;
//27ea
	   Tmp_26 = *Block1 = -(Tmp_2e * Tmp2 / Tmp_36 + Tmp1)/D_div_B;
	   ProfData++;
//                              Calc_Cg
	   Tmp_1e = *Block2 = (P4(*PSumSteps) + *ProfData * Tmp2/Tmp_36)/D_div_B;
//2842
	   D_div_B = Tmp_2e - Tmp_26 * Tmp_3e;
	   *++Block1 = Tmp_36 / D_div_B;
	   *++Block2 = (Tmp_3e * Tmp_1e + *ProfData) / D_div_B;
	   // C ����, �� 2 ����� ��� ����������
	   for (int j = 2; j< NPoints;j++){
//2886
	       Tmp_26 = *Block1++;
	       Tmp_1e = *Block2++;
	       Tmp_3e = Tmp_36;
//28f4                  Calc_D
               try {
                  Tmp_36 = P7((Block3[0] + Block3[1])*Const_0t5) * Step_dXdX;
               }
               catch (...)
               {
                  Tmp_36 = P7(Block3[0]) * Step_dXdX;
               }
	       Block3++;
//2907
	       Tmp_2e = Tmp_3e + Tmp_36 + 1;
//2919         ���������
	       D_div_B = Tmp_2e - Tmp_26 * Tmp_3e;
	       *Block1  = Tmp_36 / D_div_B;
//2948
	       *Block2  = (Tmp_3e * Tmp_1e+ *++ProfData) / D_div_B;

	   }
//2966     ���������
	   D_div_B = Tmp_4e - Tmp_36 * Tmp_5e / Tmp_3e;
//2983
	   Tmp_16  = - (Tmp_2e * Tmp_5e / Tmp_3e + Tmp_56)/D_div_B;
//29b6               0
	   Tmp_e = P1(*PSumSteps) + *ProfData * Tmp_5e/Tmp_3e/D_div_B;
	   *Block3 =  (*Block2++ * Tmp_16 + Tmp_e) / (Const_1 - *Block1++ * Tmp_16);
	   for(int j=NPoints; j; j--){
	       D_div_B = *Block3--;
//2a2e
	       *Block3 = *--Block1 * D_div_B + *--Block2;
	   }

	}
	ProfData = Block3;
	Block3 = DataPtr;
  }
}

//==============================================================================
//---------------------------------------------------------------------------

#pragma package(smart_init)

//-------------------------------------------------------------------------------

sProfileParam & sProfileParam::operator =(const sProfileParam &sP){ //�ASSIGNMENT�OPERATOR.
    if(this==&sP) return *this;
    double * DataBaseCopy = DataBase;
    double * DataCopy = Data;
    int    * DataIntCopy = DataInt;
    memcpy(this,&sP,sizeof(sProfileParam));
    //���� ������ ������������
    if(sP.Data){
        if(DataCopy == NULL)
          Data = new double [Points];
        else  
          Data = DataCopy;
        if(DataBaseCopy == NULL)
          DataBase = new double [Points];
        else  
          DataBase = DataBaseCopy;
        if(DataIntCopy == NULL)
             DataInt = new int [Points];
          
        memcpy(Data,sP.Data,Points * sizeof(double));
        memcpy(DataBase,Data,Points * sizeof(double));
        memcpy(DataInt,sP.DataInt,Points * sizeof(int));
    }else {
        Data = DataCopy;
        DataBase = DataBaseCopy;
        DataInt = DataIntCopy;
    }
    Cg = LastCf = *DataBase;   
    InitCf();
    return *this;
}
//-------------------------------------------------------------------------------

sProfileParam & sProfileParam::operator << (const sProfileParam &sP) //�ASSIGNMENT�OPERATOR.
{
    if((this==&sP)||(sP.Data==NULL)) return *this;
    if(DataBase == NULL)
      DataBase = new double [Points];
    memcpy(DataBase,sP.Data,Points * sizeof(double));
    Cg = LastCf = *DataBase;   
    InitCf();
    return *this;
}

//-------------------------------------------------------------------------------
__fastcall  sProfileParam::sProfileParam(AnsiString aFile){
    Data = NULL;
    DataBase = NULL;
    DataInt  = NULL;
    PredArraySize = 0;
    if(LoadFromFile(aFile) == NULL)
        Create = etpUnknown;
}    
//-------------------------------------------------------------------------------
void  __fastcall sProfileParam::CalculateProc()
{
 int Ndt,TotalCount;
 double Step,SummaSteps,Xl;
     if((Time > 3600 * 24) || (Time <=5 )) return;
     Xl = X_Wid;                       // ������� ���������� ����
     Step = dX;                        //��� = 0.05 �� (50 ����� - 2.5 ��)
     D0 = D*10000;
    
     CurStadyTime = (double)Time/3600;  //StadyInfArray[i].StadyTime;
     Ndt = 2 * (int)(CurStadyTime / (2 * Step) + 0.1); //���������� ����� ��������� �������
     //����������� ���������� ����� 20
     if(Ndt < 20)  Ndt  = 20;
//!!!2.02.07     
     if(Ndt > 500)  Ndt  = 500;
     //������ ������ ����
     Step = CurStadyTime / Ndt;
     SummaSteps = 0;
     CurPokExp = 1;                               //��������� ������� ���������� ����������
     CurStartCg = Cg;                    // ������� ��������� �������� Cg
     CurEndCg   = Cg;                    // ������� �������� �������� Cg
     CurBt   = B;                        // ������� �������� Betta
     if(CurStartCg >*DataBase)
         CurBoundCond = 3;                            // ������� ����� ���������� �������
     else  
         CurBoundCond = 1;                            // ������� ����� ���������� �������
     CurCgLow   = 1;                              // ������� ����� ������ ��������� Cg
				                                  // 1-��������; 2-����
     CurT_Start   = Tp;                  // �� ������� ������������� ����������� ������� ���������
                                            	  // ��� ������� ������
     // �� �������� ����������� ���������� ��������� � ������� �� ����� ������
     dTemper = (Tp - CurT_Start) / CurStadyTime;// �������� ��������� �����������
     CurT_Start = CurT_Start + Const_T_abs; //���������� ��������� �����������


     //������ ������ �� ������ ����� ������
     Block1 = new double[Points];
     Block2 = new double[Points];
     Block3 = new double[Points];
     ProfileArray = new double[Points];
     PredArraySize = Points;
     
     memcpy(ProfileArray, DataBase,Points * sizeof(double));
//              7       6           5       4       3      2      1
     CulcData(Calk_D,Choice_1_or_0,Calc_B,Calc_Cg,Load_0,Load_1,Load_0_,
	  &SummaSteps,ProfileArray,Points-1/*50*/, Xl,Step/6.0, 12, 2);
//              7      6             5       4       3      2      1
     CulcData(Calk_D,Choice_1_or_0,Calc_B,Calc_Cg,Load_0,Load_1,Load_0_,
	  &SummaSteps,ProfileArray,Points-1/*50*/, Xl, Step, Ndt-2, 2);

     memcpy(Data,ProfileArray,Points * sizeof(double));
     delete [] Block3;
     delete [] Block2;
     delete [] Block1;
     delete [] ProfileArray;
}

//-------------------------------------------------------------------------------
//���������� � �����������
int __fastcall sProfileParam::CalculateProcData()
{
    double Tpk = CelcToKelvin_l(Tp);
    Cf = Cg_func_l(Ac_func_l(C0, Tpk, EDS, CelcToKelvin_l(Td)), Tpk);
    CalculateProc();
    return SetDataInt();
}

//-------------------------------------------------------------------------------
bool  __fastcall  sProfileParam::SaveProf()
{
    AnsiString FileName;
    FileName = Name;
    FileName += "_";
    FileName += Num;
    FileName += "_";
    FileName += SadkaNum;
    FileName += ".prf";
    return SaveFile(FileName);
}

bool  __fastcall  sProfileParam::SaveFile(AnsiString aFile)
{
    int hFile = FileOpen(aFile,fmOpenRead); //-- �������� �������
    FileClose(hFile);
//    if ((hFile != -1)){
//       if (MessageBox(GetActiveWindow(),"���� � ����� ������ ��� ����������. ��������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return false;
//    }
    hFile = FileCreate(aFile);          //-- ������� ����
    if (hFile == -1) return false;

    FileWrite (hFile,this,sizeof(sProfileParam));    //-- ���������� ��������� �������

    if (!Data)  //-- ���� ������ ��� ���������� ����
    {
        Data = new double[Points];
        memset (Data,0,Points * sizeof(double));
    }
    if (!DataInt)  //-- ���� ������ ��� ���������� ����
    {
        DataInt = new int[Points];
        memset (DataInt,0,Points * sizeof(double));
    }
    if(DataBase == NULL)
       DataBase = new double [Points];
    memcpy(DataBase,Data,Points * sizeof(int));
    
    FileWrite (hFile,Data,Points * sizeof(double)); //-- ���������� ������ �������
    FileClose (hFile);
    return true;

}
//-------------------------------------------------------------------------------
void  __fastcall  sProfileParam::InitData(double V){

    if (!Data)  //-- ���� ������ ��� ���������� ����
        Data = new double[Points];
    if(DataBase == NULL)
        DataBase = new double [Points];
    if (!DataInt)  //-- ���� ������ ��� ���������� ����
        DataInt = new int[Points];

    for(int i=0; i<Points;i++){
       DataBase[i]=Data[i] = V;
//       DataInt[i] = (int)V;
       DataInt[i] = (int)(V*1000);
    }   
    Cg = LastCf = *DataBase;   
    InitCf();
    
}

void  __fastcall  sProfileParam::InitData(double Width,double adX,double V){
    LastCf = V;
//    InitCf();
    X_Wid = Width;      //-- ������� �������������� ����
    dX = adX;           //-- ��� ����������
    Points = X_Wid/dX+2;//-- ��������� ����� �������
    InitData(V);
}

//-------------------------------------------------------------------------------

__fastcall  sProfileParam::sProfileParam(double Width,double adX,double V){
    LastCf = V;
    Data = NULL;
    DataBase = NULL;
    DataInt = NULL;
    
    InitCf();
    X_Wid = Width;      //-- ������� �������������� ����
    dX = adX;           //-- ��� ����������
    Points = X_Wid/dX+2;//-- ��������� ����� �������
    InitData(V);
    Create = etpUnknown;
    PredArraySize = 0;
}

//-------------------------------------------------------------------------------
__fastcall  sProfileParam::~sProfileParam(){
    if (Data){  //-- ���� ������ ��� ���������� ����
        delete []Data;
        Data = NULL;
    }    
    if (DataInt){  //-- ���� ������ ��� ���������� ����
        delete []DataInt;
        DataInt = NULL;
    }    
    if (DataBase){  //-- ���� ������ ��� ���������� ����
        delete []DataBase;
        DataBase = NULL;
    }    

}

//-------------------------------------------------------------------------------
double*  __fastcall  sProfileParam::LoadFromFile(AnsiString aFile){
    bool    err = false;
    int hFile = FileOpen(aFile,fmOpenRead);
    if (hFile == -1) return NULL;
    double * DataBaseCopy = DataBase;
    double * DataCopy = Data;
    int    * DataIntCopy = DataInt;

    if (FileRead (hFile,this,sizeof (sProfileParam)) != sizeof (sProfileParam)) err = true;
    if (!err)
    {
        if(DataCopy == NULL)
          Data = new double [Points];
        else
          Data = DataCopy;
        if(DataIntCopy == NULL)
          DataInt = new int [Points];
        else  
          DataInt = DataIntCopy;
          
        if (!Data) err = true;
        if(DataBaseCopy == NULL)
          DataBase = new double [Points];
        else  
          DataBase = DataBaseCopy;
        if (!DataBase) err = true;
    }else {
        Data = DataCopy;
        DataBase = DataBaseCopy;
        DataInt = DataIntCopy;
    }
    
    if (!err) if (FileRead (hFile,Data,Points * sizeof(double)) != (int)(Points * sizeof(double))) err = true;
    FileClose(hFile);
    if (!err) 
       memcpy(DataBase,Data,Points * sizeof(double));
    else 
        return NULL;
    Cg = LastCf = *DataBase;
    InitCf();

    return Data;

}
//-------------------------------------------------------------------------------
//   �������� �������� ��������� ������������ �������
//-------------------------------------------------------------------------------
int  __fastcall  sProfileParam::TestFinish(int &Index){
        int Result = 0;
        int i;
        //-- ����� ����������� ������� � ������ ��������� �������� �
        for (i=0; i< Points-1;i++)
        if ((Data[i] >= EndC)&&(Data[i+1] <= EndC))
        {
            if ((i * dX) >= EndX) {
                Result = 3;
            }else if ( (( i * dX) >= (EndX * (double)0.9) )){
                Result = 2;
            }else
                Result = 1;
            break;
        }
        Index = i;
        return Result;
}

//-- ����� ������� � ���� ������ ------------------------------------------
//-- ��������� ������� � ������ ������ ��������� �������
bool __fastcall sProfileParam::FindProfile ()
{
//    AnsiString aFile = ProfileFileName;
    int hFile = FileOpen(ProfileFileName/*aFile*/,fmOpenRead);
    if (hFile == -1) return false;
    if(FileSeek(hFile,0,2) != (int)(sizeof (sProfileParam)+ Points * sizeof (double)))
    {
        FileClose(hFile);
        return false;
    }
    FileClose(hFile);
    return true;
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
bool  __fastcall  sEtapsProfile::Save(AnsiString aFile)
{
    //-- ��������
    int hFile = FileCreate(aFile);
    if (hFile == -1) return false;
    int iWrite = FileWrite(hFile,this,sizeof(sEtapsProfile));
    FileClose(hFile);
    if (iWrite!= sizeof(sEtapsProfile)) return false;
    return true;
}
//------------------------------------------------------------------------------------
bool  __fastcall  sEtapsProfile::Load(AnsiString aFile)
{
    sEtapsProfile EEmpty,ELoad;
    //-- ��������
    int hFile = FileOpen(aFile,fmOpenRead);
    if (hFile == -1) return false;
    int iRead = FileRead(hFile,&ELoad,sizeof(sEtapsProfile));
    FileClose(hFile);
    //-- ��������
    if (iRead!= sizeof(sEtapsProfile)) return false;
    //-- ��������
    if (strncmp(ELoad.Sign,EEmpty.Sign,4)) return false;
    //-- �����������
    memcpy(this,&ELoad,sizeof(sEtapsProfile));
    return true;
}
//------------------------------------------------------------------------------------
bool  __fastcall  sEtapsProfile::Add(int idx)
{
    //-- ��������
    if (Used == MAX_ETAP_PROFILE) return false;
    if (idx<0 || idx >= MAX_ETAP_PROFILE) return false;
    for (int i = MAX_ETAP_PROFILE-1;i>=idx;i--)
    {
        Etap[i]=Etap[i-1];
    }
    sOneEtapProfile NewEtap;
    Etap[idx] = NewEtap;
    Used++;

    ComputeTime();
    return true;
}
//------------------------------------------------------------------------------------
bool  __fastcall  sEtapsProfile::Sub(int idx)
{
    //-- ��������
    if (Used == 1) return false;
    if (idx >= MAX_ETAP_PROFILE) return false;
    for (int i=idx;i<MAX_ETAP_PROFILE-1;i++)
    {
        Etap[i] = Etap[i+1];
    }
    Used--;
    ComputeTime();
    return true;
}

/*
//-- ����� ������� � ���� ������ ------------------------------------------
//-- ��������� ������� � ������ ������ ��������� �������
bool __fastcall FindProfile (sProfileParam *profile)
{
//    sProfileParam local_profile;
//    bool    err = false;

    AnsiString aFile = ProfileFileName(profile);

    int hFile = FileOpen(aFile,fmOpenRead);
    if (hFile == -1) return false;
    if(FileSeek(hFile,0,2) != (int)(sizeof (sProfileParam)+ profile->Points * sizeof (double)))
    {
        FileClose(hFile);
        return false;
    }
    FileClose(hFile);
    return true;
}

*/
//-- ��������� ������ � ��������� ������� �������������� �� ��������� ��������:
//   1. ������ ������� - ��� ���� � ������: <��� �������> <_> <�����> . PRF
//      ��������� ��������� ������� + ������ �������


//-- ��������� ������������� ����� �������-----------------------------------
bool __fastcall ProfileExists (sProfileParam *profile)
{
    AnsiString aFile = ProfileFileName(profile);
    int hFile = FileOpen(aFile,fmOpenRead); //-- �������� �������
    if (hFile == -1) return false;
    FileClose(hFile);
    return true;
}

//-- ������� ������� �� ���� ������ ----------------------------------------
double* __fastcall ReadProfile (sProfileParam *profile)
{
    AnsiString aFile = ProfileFileName(profile);
    profile->LoadFromFile(aFile);
    return profile->Data;
}

//-- ��������� ����� ����� ������� ------------------------------------------
AnsiString __fastcall ProfileFileName (sProfileParam *profile)
{
    AnsiString aFile;
//!!
    aFile = profile->Name;
    aFile += "_";
    aFile += profile->Num;
    aFile += "_";
    aFile += profile->SadkaNum;
    aFile += ".prf";
    return aFile;
}



void __fastcall sProfileParam::SetX_width(double value)
{
    if(X_Wid != value) {
        X_Wid = value;           //-- ������� �������������� ����
        dX = X_Wid/(Points-2);   //-- ��� ����������
    }
}

void __fastcall sProfileParam::SetX_Step(double value)
{
    if(dX != value) {
        dX = value;
        X_Wid = dX * (Points-2); //-- ��� ����������
    }
}

void __fastcall sProfileParam::SetProfileFileName(AnsiString value)
{
    AnsiString aFile = value;
    //!! ���� �� ������� ������� �� ����� � ��������� �������
}

//-- ��������� ����� ����� ������� ------------------------------------------
AnsiString __fastcall sProfileParam::GetProfileFileName()
{
    AnsiString aFile;
    aFile = Name;
    aFile += "_";
    aFile += Num;
    aFile += "_";
    aFile += SadkaNum;
    aFile += ".prf";
    return aFile;
}
