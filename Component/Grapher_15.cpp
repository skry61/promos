//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "Grapher.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TGrapher *)
{
    new TGrapher(NULL);
}
//---------------------------------------------------------------------------
__fastcall TGrapher::TGrapher(TComponent* Owner) :TPaintBox(Owner)
{

    Height = 100;
    Width = 300;

    FLineColor = clBlack;
    FBgColor = clWhite;
    FAreaColor = clSilver;
    FBorderColor = clNavy;
    FMarkColor = clGreen;
    
    FAreaSpace = 30;
    FArrowWidth = 6;
    FArrowLength = 10;
    FBorderWidth = 1;
    FCaptionFont = "Arial";
    FCaptionSize = 14;
    FCaption = "������ 1";
    FHMin = FVMin = 0;
    FHMax = FVMax = 100;
    FHStep = FVStep = 10;
    p_int = NULL;
    intCount  = 0;

    FValueDigits = 2;
    FValuePrecision = 4;
    FValueFormat = (TFloatFormat)AnsiString::sffFixed;

    FMarker = false;
    IsMarker = false;
    MarkerLinesCount = 0;//iCount;
    MarkerLines = NULL;//cLines;
    FMarkerLines[0] = Msg1;
    FMarkerLines[1] = Msg2;
    //������ ������������
    ControlStyle = ControlStyle << csOpaque;// SET
    OnMouseDown = MyMouseDown;
}
//---------------------------------------------------------------------------
__fastcall TGrapher::~TGrapher()
{
   if (p_int)
        //-- �������  ������ �����
        delete [] p_int;
}
//---------------------------------------------------------------------------
void __fastcall TGrapher::SetValueDigits (int Digits)
{
    FValueDigits = Digits;
    Repaint();
}
//------------------------------------------------------------------------------
void __fastcall TGrapher::SetValuePrecision (int Precision)
{
    FValuePrecision = Precision;
    Repaint();
}
//------------------------------------------------------------------------------
void __fastcall TGrapher::SetValueFormat (TFloatFormat aFormat)
{
    FValueFormat = aFormat;
    Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TGrapher::SetMarkerLines (int iCount, char** cLines)
{
    MarkerLinesCount = iCount;
    MarkerLines = cLines;
}

//---------------------------------------------------------------------------
void __fastcall TGrapher::DrawSText(TCanvas *cnv,TColor tColor,int X, int Y, AnsiString aStr)
{
    cnv->Brush->Color = tColor;
    cnv->FillRect(Rect(X-1,Y-1,X+cnv->TextWidth(aStr)+1,Y+1+cnv->TextHeight(aStr)));
    cnv->Pen->Width = 1;
    cnv->Pen->Color = FLineColor;
    cnv->TextOut (X,Y,aStr);
}

//---------------------------------------------------------------------------
// ��������� ������� �����
void __fastcall TGrapher::LoadCanvas(){
//    if(AlterPaint)
}   
//---------------------------------------------------------------------------
// ���������� ������� �����
void __fastcall TGrapher::FreeCanvas(){   
}

TCanvas * __fastcall TGrapher::StartPaint(void){
    Bitmap = new Graphics::TBitmap;
    if(AlterPaint){
       WorkRect = AlterRect;
       WorkCnv = AlterCanvas;
    }else{
       WorkRect = ClientRect;
       WorkCnv = Canvas;
    } 
    Bitmap->Width  = WorkRect.Right;
    Bitmap->Height = WorkRect.Bottom;
    MyPaint(Bitmap->Canvas,WorkRect);
    return Bitmap->Canvas;
}

void __fastcall TGrapher::FinishPaint(void){
//    Res = 
    BitBlt(WorkCnv->Handle,
        0,0,
        WorkRect.Right,WorkRect.Bottom,
        Bitmap->Canvas->Handle,0,0,
        SRCCOPY
        );
    delete Bitmap;
}

//---------------------------------------------------------------------------
void __fastcall TGrapher::Paint(void)
{
    if (FOnPaint) FOnPaint(this);
    else MyPaint((AlterPaint)?AlterCanvas:Canvas,ClientRect);
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� CXT �� ������ double
//---------------------------------------------------------------------------
int* __fastcall TGrapher::FuncDataToInt(double* Ptr, int Mul, int Count)
{
    // ���� ������ ��� ���������� � ��� ����� ���������� �� �������������
    if ((p_int) && (intCount != Count))
    {
        //-- �������  ������ �����
        delete [] p_int;
        p_int = NULL;
    }
    if(p_int == NULL) p_int = new int[Count];
    int *p  = p_int;
    intCount = Count;
    for (int i=0;i<Count;i++) *p++ = (int)((*Ptr++) * Mul + 0.5);
    return  p_int;
}

//---------------------------------------------------------------------------
//���� ������ 
void __fastcall TGrapher::SetFunctionData(TColor tColor,double * FuncData, int Mul, int Count)
{
   DFColor = tColor;
   FuncDataToInt(FuncData, Mul, Count);
   // �������� �� ����������� - ������ �������� �������
   FW_Value = *p_int;
}

//---------------------------------------------------------------------------
void __fastcall TGrapher::DrawFunctionData(TColor tColor,int * FuncData, int Idx, int Count)
{
    // ���� ������ ��� ���������� � ��� ����� ���������� �� �������������
    if ((p_int) && (intCount != Count))
    {
        //-- �������  ������ �����
        delete [] p_int;
        p_int = NULL;
    }
    if(p_int == NULL) p_int = new int[Count];
    intCount = Count;
    memcpy(p_int,FuncData + Idx,Count*sizeof(int));

  
    PrepareCalc();

    TCanvas *cnv = (AlterPaint)?AlterCanvas:Canvas;

    cnv->Pen->Color = tColor;
    cnv->Pen->Width = FLineWidth;
    cnv->Pen->Style  = psSolid;
    intCount = Count;
    int X1,X2,Y1,Y2;
    
    int aMin = FHMin;
    int *p  = p_int;
    for (int i=0;i<intCount-1;i++)
    {
        X1 = XonGrapher (aMin);
        X2 = XonGrapher (++aMin);
        Y1 = YonGrapher ( *p++);
        Y2 = YonGrapher ( *p);
        cnv->MoveTo (X1,Y1);
        cnv->LineTo (X2,Y2);
    }
}

void __fastcall TGrapher::SetCaption(AnsiString aCap)
{
    FCaption = aCap;
    Repaint();
}

void __fastcall TGrapher::SetAlterPaint(TCanvas *cnv,TRect R)
{
    AlterPaint = true;
    AlterRect = R;
    AlterCanvas = cnv;
}

void __fastcall TGrapher::ResetAlterPaint(void)
{
    AlterPaint = false;
}

void __fastcall TGrapher::MyPaint(TCanvas *cnv,TRect R)
{
    cnv->Pen->Style  = psSolid;
    cnv->Brush->Style = bsSolid;
    cnv->Pen->Color = FBorderColor;
    cnv->Pen->Width = FBorderWidth;
    cnv->Brush->Color = BgColor;
    cnv->Rectangle(R.Left,R.Top,R.Right,R.Bottom);

    cnv->Pen->Color = FLineColor;
    cnv->Pen->Width = 1;//FLineWidth;
    cnv->Pen->Style  = psSolid;

    GraphRect = R;
    ClientRectEx = R;
    GraphRect.Left  +=FAreaSpace;
    GraphRect.Top   +=FAreaSpace;
    GraphRect.Right -=FAreaSpace;
    GraphRect.Bottom -=FAreaSpace;

    cnv->Brush->Color = AreaColor;
    //-- ������� �������
    cnv->FillRect(GraphRect);

    //DrawLabel (cnv, GraphRect);

    AnsiString aFontName = cnv->Font->Name;
    int FontSize = cnv->Font->Size;

    cnv->Brush->Color = FBgColor;
    cnv->Font->Name = FCaptionFont;
    cnv->Font->Size = FCaptionSize;

    int tw = cnv->TextWidth(FCaption);
    int th = cnv->TextHeight(FCaption);
    cnv->TextOut (ClientRectEx.Right/2 - tw/2,(GraphRect.Top - FBorderWidth)/2 - th/2 ,FCaption);

    cnv->Font->Name = aFontName;
    cnv->Font->Size = FontSize;
}
void __fastcall TGrapher::PrepareCalc(void)
{
    if (FHStep == 0) FHStep = 1;
    if (FVStep == 0) FVStep = 1;
    if ( (FHMax-FHMin) == 0) FHMax = FHMin + 10;
    if ( (FVMax-FVMin) == 0) FVMax = FVMin + 10;
    kX =  (double)(GraphRect.Right - GraphRect.Left)/(double)(FHMax - FHMin);
    kY =  (double)(GraphRect.Bottom - GraphRect.Top)/(double)(FVMax - FVMin);

}
int __fastcall TGrapher::XonGrapher (int x_real)
{
    return GraphRect.Left + (int)(kX*(double)(x_real - HMin));
}
int __fastcall TGrapher::YonGrapher (int y_real)
{
    return GraphRect.Bottom - (int)(kY*(double)(y_real - VMin));
}

int __fastcall TGrapher::XonGrapher (double x_real)
{
    return GraphRect.Left + (int)(kX* (x_real - HMin));
}
int __fastcall TGrapher::YonGrapher (double y_real)
{
    return GraphRect.Bottom - (int)(kY*(y_real - VMin));
}
//---------------------------------------------------------------------------

void __fastcall TGrapher::DrawMarker(TCanvas *cnv)
{
    int X1 = XonGrapher (FH_Marker);
    // ������ �������� ����� ����� �������
    cnv->Pen->Color = clBlue;
    cnv->MoveTo (X1,GraphRect.Bottom);
    cnv->LineTo (X1,GraphRect.Top);
    cnv->Ellipse(X1-3, GraphRect.Bottom-3, X1+3, GraphRect.Bottom+3);

    cnv->Pen->Color = clBlack;

    int height = (cnv->TextHeight("A") + 3)*MarkerLinesCount;
    int width = 0;
    int t_w;
    if (MarkerLines){
        for (int i=0;i<MarkerLinesCount;i++)
        {
            t_w = cnv->TextWidth(AnsiString(MarkerLines[i]));
            if (t_w > width) width = t_w;
        }
        width +=10;

        cnv->Brush->Color = FMarkerColor;
        cnv->Brush->Style = bsSolid;
        cnv->Rectangle (X1,MarkerY - height - 4,X1 + width,MarkerY );

        int l_h = height/MarkerLinesCount;
        for (int i=0;i<MarkerLinesCount;i++)
           cnv->TextOut (X1+5,MarkerY+(l_h*i)-2 - height,AnsiString(MarkerLines[i]));
    }      
}

//---------------------------------------------------------------------------

void __fastcall TGrapher::DrawLabel(TCanvas *cnv, TRect GraphRect1)
{
    PrepareCalc();
    int divx = (FHMax-FHMin)/FHStep;
    int divy = (FVMax-FVMin)/FVStep;

    bool osy = (FHMin <= 0 && FHMax >=0)?true:false;
    bool osx = (FVMin <= 0 && FVMax >=0)?true:false;
    int X1,Y1;

    cnv->Pen->Width = 1;//FLineWidth;
    cnv->Pen->Color = FLineColor;
    cnv->Pen->Style  = psDot;
    cnv->Brush->Style = bsClear;

    for (int i = 0; i<divx; i++)
    {
        //X1 = GraphRect1.Left + (int)(kX*(float)(FHStep*i));
        X1 = XonGrapher (FHMin + FHStep*i);

        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,IntToStr(FHMin + FHStep*i));
    }

    for (int i = 0; i<divy; i++)
    {
        Y1 = YonGrapher (VMin + FVStep*i);
        //Y1 = GraphRect1.Bottom - (int)(kY*(float)(FVStep*i));
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        cnv->TextOut (ClientRectEx.Left + 5,Y1-5,IntToStr(FVMin + FVStep*i));
    }

    cnv->Pen->Style  = psSolid;

    if (!osy)   //-- ������� ��� ��� - �������� ��� Y
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Left,GraphRect.Top);
        DrawArrow(cnv,eToTop,GraphRect.Left,GraphRect.Top);
    }
    else        //-- ������� ��� ���� - �������� ��� Y
    {
        X1 = XonGrapher (0);
        //X1 = GraphRect1.Left + (int)(kX*(float)(-FHMin));
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        DrawArrow(cnv,eToTop,X1,GraphRect.Top);
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,IntToStr(0));
    }
    if (!osx)   //-- ������� ��� ��� - �������� ��� X
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Right,GraphRect.Bottom);
        DrawArrow(cnv,eToRight,GraphRect.Right,GraphRect.Bottom);
    }
    else
    {
        Y1 = YonGrapher (0);
        //Y1 = GraphRect1.Bottom - (int)(kY*(float)(-FVMin));
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        cnv->TextOut (ClientRect.Left + 5,Y1-5,IntToStr(0));
        DrawArrow(cnv,eToRight,GraphRect.Right,Y1);
    }
}

void __fastcall TGrapher::DrawArrow(TCanvas *cnv, eArrowDirect Dir, int X, int Y)
{
    int X1,Y1,X2,Y2,AddW;
    AddW = FArrowWidth/2;
    switch (Dir)
    {
        case eToTop:
            X1 = X - AddW;
            X2 = X + AddW;
            Y1 = Y + FArrowLength;
            Y2 = Y1;
            break;

        case eToBottom:
            X1 = X - AddW;
            X2 = X + AddW;
            Y1 = Y - FArrowLength;
            Y2 = Y1;
            break;

        case eToLeft:
            X1 = X + FArrowLength;
            X2 = X1;
            Y1 = Y - AddW;
            Y2 = Y + AddW;

            break;

        case eToRight:
            X1 = X - FArrowLength;
            X2 = X1;
            Y1 = Y - AddW;
            Y2 = Y + AddW;
            break;
    }
    cnv->MoveTo (X,Y);
    cnv->LineTo (X1,Y1);

    cnv->MoveTo (X,Y);
    cnv->LineTo (X2,Y2);

}
void __fastcall TGrapher::MyMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    if (!FMarker) return;
    if((X > GraphRect.Right)||(X < GraphRect.Left)||(Y < GraphRect.Top)||(Y > GraphRect.Bottom))
                  return;

    //-- ����� ������ ���� - ����� �����
    if (Button == mbRight || Button == mbMiddle)
    {
      if (FOnMarker)
         FOnMarker(this);
      if (IsMarker){
         IsMarker = false;
      }
      else
         return; //������ ����� ��� ��������
    }     
    //-- ������ ������ ���� - ���������� ������
    if (Button == mbLeft)
    {
        MarkerY = Y;
        MarkerPoint = (int)( (double) (X - GraphRect.Left)/kX);
        CalcMarkerParam();
//        FMarkerTime = FTimeStart + TDateTime((double)MarkerPoint*(double)FTimeStep);

        FH_Marker = FHMin +  MarkerPoint;
        IsMarker = true;
        if (FOnMarker) FOnMarker(this);

    }
    Repaint();
}

namespace Grapher
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TGrapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------
