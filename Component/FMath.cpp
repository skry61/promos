//---------------------------------------------------------------------------
// Модуль математической обработки данных
//---------------------------------------------------------------------------
#include <vcl.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#pragma hdrstop

#include "FMath.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

int matherr (struct exception *a)
{   return 0;   }



double CelcToKelvin_l(double C)
{    return C + 273.3;  }

double Cg_func_l(double Ac, double Tp)
{   return (Ac * 100 )/ (1.07 * exp(4798.6/Tp) + 19.5*Ac );     }


double Ac_func_l(double CO, double Tp, double E, double Td)
{
    if(E > 2500) E = 2500;
    double step = (6995/Tp) - 6.1367 + (E - 1290)/(0.0992*Td);
    double res_pow = pow (10,step);
    return CO*res_pow;
}

bool order (double value, int &order)
{
    if (value == 0) return false;
    int result = 0;
    while (1)
    {
        if (value <= 1 && value > 0.1)
        {
            order = result;
            return true;
        }

        if (value > 1)
        {
            value = value/10;
            result++;
        }
        if (value <= 0.1)
        {
            value = value*10;
            result--;
        }
    }
}


