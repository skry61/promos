//---------------------------------------------------------------------------
// ������ ���������� ��� ���
//---------------------------------------------------------------------------
#ifndef ArmParamsH
#define ArmParamsH

#include <stdio.h>
#include "Config.h"

//---------------------------------------------------------------------------
//      ������ ����������� � ��������
//---------------------------------------------------------------------------

#define NEW_AREA_COLOR 0xE0E0E0

//#define is_Oven(ID)  (((ID)>=eZone1)&&((ID)<=eZone5))
//#define is_Oven(Ind)  ((iObjectId[(Ind)]>=eZone1)&&(iObjectId[(Ind)]<=eZone5))
#define is_EndoGen(Ind)  ((iObjectId[(Ind)]>=eEndoGen1)&&(iObjectId[(Ind)]<=eEndoGen5))


#define EPS  5
#define ON   true
#define OFF  false

//��� ���������� ������ 2 ���
#define TAY  120

//-- ��� ���������� 0- ����� ���������� 1-����� ��������
#define REGUL_DEFAULT       0
// ���� ������� �� ������ � ��������
#define FlSetUstTermodat_DEFAULT  0
//-- ��� ���������� 0- ����� ���������� 1-����� ��������
#define EVENT_VIEW_DEFAULT  0


//-- �������� ��������/���-�� �������� �� ���������
#define VISIBLE_INTERVAL_DEFAULT    3600
#define INTERVAL_DEFAULT    10000
#define MAX_PROFILE         3
#define MAX_OBJECT          6
//-- ������� ����� ��������� 1 Wire � �������� ����� ����� ���������
#define _1_WIRE_PSW         (MAX_OBJECT + 1)
//-- ������� :
#define _1W_DETECTED        0x1
#define TERMODATS_DETECTED  0x2
#define REQUEST_OK          0x4
#define ERR_DETECTED        0x10
#define LOST_CONNECT        0x40
#define TERMODATsDetected() (psw[_1_WIRE_PSW] & TERMODATS_DETECTED)
#define _1WireDetected()    (psw[_1_WIRE_PSW] & _1W_DETECTED)
#define _1WireLostConnection()    (psw[_1_WIRE_PSW] & LOST_CONNECT)
#define _1WireError()    (psw[_1_WIRE_PSW] & ERR_DETECTED)


#define MAX_PARAM           6
#define MAX_JOURNAL_EVENT   4

#define MAX_EVENT_VIEW      3000        //-- ������. ����� ������� �������

//-- ���������� ������������ ������ ����� � ������������
#define MAX_IGNORE_EXCHANGE_ERROR   2

//-- �������� ��� ����� �� ���������
#define COM_PORT_DEFAULT    2
#define COM_PORT_ALT_DEFAULT    1

//-- �������� �������� ������������
#define TP_AUTOCONTROL_DEFAULT   0x3F
#define TD_AUTOCONTROL_DEFAULT   0x3F
#define EDS_AUTOCONTROL_DEFAULT  0x3F
#define CG_AUTOCONTROL_DEFAULT   0x0
#define BURN_AUTOCONTROL_DEFAULT 0x3F
#define BURN_AUTOCONTROL_DEF     0x3F

//-- �������� ���������� ���������� ������� �� ���������
#define CO_DEFAULT          25
#define TP_DEFAULT          910
#define TD_DEFAULT          895
#define EDS_DEFAULT         1250

//-- �������� ��� ��� ����� ���������� � ����� ���������
#define TP_HIGH_PSW         0x01
#define TP_LOW_PSW          0x02
#define TD_HIGH_PSW         0x04
#define TD_LOW_PSW          0x08
#define E_HIGH_PSW          0x10
#define E_LOW_PSW           0x20

#define AC2_HIGH_CCW        0x01
#define AC_HIGH_CCW         0x02
#define AC_LOW_CCW          0x04
#define CG_HIGH_CCW         0x08
#define CG_LOW_CCW          0x10
#define STOP_PROFILE_CCW    0x20
#define START_PROFILE_CCW   0x40
#define EXTR_PROFILE_CCW    0x80

//-- ������� ���������� �� ��������� -------
//------------------------------------------
#define TP_HIGH_DEFAULT      1000
#define TP_LOW_DEFAULT       700
#define TP_REG_DEFAULT       840

#define TD_HIGH_DEFAULT      1000
#define TD_LOW_DEFAULT       700

#define E_HIGH_DEFAULT       1500
#define E_LOW_DEFAULT        500

#define E_REG_DEFAULT        1130
#define Cg_REG_DEFAULT       1000

#define AC2_HIGH_DEFAULT     50
#define AC_HIGH_DEFAULT      20
#define AC_LOW_DEFAULT       0

#define CG_HIGH_DEFAULT      100
#define CG_LOW_DEFAULT       0.000
#define CG_AC_DEFAULT        1.0

//-- ��� ����� �� �������� �� ��������� ----
//------------------------------------------
#define TP_STEP_DEFAULT      100
#define TD_STEP_DEFAULT      100
#define E_STEP_DEFAULT       250
#define CO_STEP_DEFAULT      50
#define AC_STEP_DEFAULT      10
#define CG_STEP_DEFAULT      10


#define PROFILE_HIGH_DEFAULT      100
#define PROFILE_LOW_DEFAULT       0
#define PROFILE_STEP_DEFAULT      10


//---������ �������� ������ ����������� --------
//-- ������ ���������� -------------------------
//-- 4100 (8) -> 100 (8) =   64 (10)
#define LINK_DATA_ADDRESS    64

//-- ������ ���������� - ������� ��, ��, � -----
//-- 4200 (8) -> 200 (8) =   128 (10)
#define LIMITS_DATA_ADDRESS  128

//------------------------------------------------------------------------------
//-- ������ ���������� - ������������� ��� �1  -----
//-- 4214 (8) -> 214 (8) =   140 (10)
#define SET_E_REGUL_ADDRESS_1     140

//-- ������ ���������� - ������������� ��� �2  -----
//-- 4234 (8) -> 234 (8) =   156 (10)
#define SET_E_REGUL_ADDRESS_2     156
//-- ������ ���������� - ������������� ��� �3  -----
//-- 4254 (8) -> 254 (8) =   172 (10)
#define SET_E_REGUL_ADDRESS_3     172

//-- ������ ���������� - ������������� ��� �4  -----
//-- 4274 (8) -> 274 (8) =   188 (10)
#define SET_E_REGUL_ADDRESS_4     188
//-- ������ ���������� - ������������� ��� �5  -----
//-- 4314 (8) -> 314 (8) =   204 (10)
#define SET_E_REGUL_ADDRESS_5     204

//-- ������ ���������� - ������������� ��� �6  -----
//-- 4334 (8) -> 334 (8) =   220 (10)
#define SET_E_REGUL_ADDRESS_6     220
//------------------------------------------------------------------------------
//-- ������ ����� ��������� ---------------------
//-- 310 (8) -> 310 (8) =    200 (10)
#define LINK_PSW_ADDRESS     200

//-- ������ ����� ����������
//-- �������� ������ �� ������� �� � �� --
//-- 320 (8) -> 320 (8) =    208 (10)
#define LIMITS_CCW_ADDRESS   208

//-- ������ ����� ���������� ����������./����������� ������� �������/������������
//-- 330 (8) -> 330 (8) =    216 (10)
#define CONTROL_REGIM_CCW_ADDRESS   216

//-- ������ ����� ���������� ����������./����������� �������� � ������/������������
//-- 331 (8) -> 332 (8) =    217 (10)
#define CONTROL_MORE_LESS_CCW_ADDRESS   217

//-- ������ ����� ���������� ������� "������ �������"
//-- 333 (8) -> 333 (8) =    219 (10)
#define BURN_SENSOR_CCW_ADDRESS   219
/* ���� ����� �����
  7   6    5     4     3     2    1   0
|   |   | ��3 | ��2 | ��1 | �3 | �2 | �1 |
*/

//-- ������ ����� ���������� ������� "���������� ������� �� ������� �����"
//-- 334 (8) -> 334 (8) =    220 (10)
#define CONTROL_PROBE_ADDRESS   220
/* ���� ����� �����
  7   6    5     4     3     2    1   0
|   |   | ��3 | ��2 | ��1 | �3 | �2 | �1 |
*/

//-- ������ ������������ ����� ������� ����������
//   ������� �������� - ���������� ������������� �� �����������
//   ������������� ������ ���� ���������� ���������� ��� ����� � ����������
//   YES - ��������, ��� �� ������ ����� ���� �������, ������� ���� ��������� 
//   �� ����������� (��, ��, ��� �� ���)
//-- 335 (8) -> 337 (8) =    221-223 (10)
#define FLAG_PARAM_FROM_PLC_ADDRESS   221
/* ���� ����� �����
  7      6      5     4     3      2      1     0
|YES1 | ���1 | ��1 | ��1 | YES0 | ���0 | ��0 | ��0 |
|YES3 | ���3 | ��3 | ��3 | YES2 | ���2 | ��2 | ��2 |
|YES5 | ���5 | ��5 | ��5 | YES4 | ���4 | ��4 | ��4 |
*/

//-- ������ �������� ����������� ������������� ����������� ���������
//-- 4400 (8) -> 400 (8) =   256 (10)
#define TERM_CONTROL_ADDRESS     256

//-- ������ �������� ����������� � �� �������
//-- 4440 (8) -> 440 (8) =   288 (10)
#define TERM_CURRENT_ADDRESS     288

//---------------------------------------------------------------------------
//      ������ ����� ����� � ��������
//---------------------------------------------------------------------------
//-- ��������� ����� ���������� ---------------------------------------------
/*
struct sArmParam
{
    TDateTime   Time;       //-- ����/����� ��������� ����������
    double      Param[MAX_PARAM];  //-- ������ ����������
    int         Source;     //-- �������� �������������
};
*/
//-- ���������������� ��������� ��� ���� -----------------------------------
struct sOptimalParam1
{
    int         Tp;         //-- ����������� ����/��������������
    int         Td;         //-- ����������� �������
    int         E;          //-- �������� ���
    short int   CO;         //-- ������������ ��
    double      Ac;         //-- ���������� ��������
    double      Cg;         //-- ���������� ���������
};
//-- ���������������� ��������� ��� �������������� -------------------------
struct sOptimalParam2
{
    int         Tp;         //-- ����������� ����/��������������
    int         Td;         //-- ����������� �������
    int         E;          //-- �������� ���
    short int   CO;         //-- ������������ ��
    double      Ac;         //-- ���������� ��������
};

//-- ��������� ����� ���������� �� ��� ������� ------------------------------
//-- 3 ���� � 3 �������������� ----------------------------------------------
struct sOptimalParam_
{
    double Time;                    //-- ����/����� ��������� ����������
    sOptimalParam1 Param1[3];       //-- ��������� 3-� �����
    sOptimalParam2 Param2[3];       //-- ��������� 3-� ���������������
};
//--
struct sOptimalShortParam
{
    short       Tp;         //-- ����������� ����/��������������
    short       Td;         //-- ����������� �������
    short       E;          //-- �������� ���
    short       CO;         //-- ������������ ��
    short       Ac;         //-- ���������� ��������
    short       Cg;         //-- ���������� ���������
    short  ObjectId;        //-- ������
};

struct sOptimalParam //sShortParam
{
    double Time;                    //-- ����/����� ��������� ����������
    sOptimalShortParam Params[MAX_OBJECT];       //-- ��������� ����� � ���������������
};

//-- c�������� ��������� �� ������ �� �����������
struct sLinkParam
{
    unsigned short int Tp;
    unsigned short int Td;
    unsigned short int E;
    unsigned short int CO;
};

//---------------------------------------------------------------------------
//-- ��������� �������� �� ���� ������
struct sLimits
{
    unsigned short int TpHigh;         //-- ������� ������ ��� ��
    unsigned short int TpLow;          //-- ������ ������ ��� ��
    unsigned short int TdHigh;         //-- ������� ������ ��� ��
    unsigned short int TdLow;          //-- ������ ������ ��� ��
    unsigned short int EHigh;          //-- ������� ������ ��� ��� � ���� ���
    unsigned short int ELow;           //-- ������ ������ ��� ��� � ���� ���
    unsigned short int EReg;           //-- ��� ������������� � ���� ���    
    unsigned short int TpReg;          //-- ����������� �������������

    float   Cg_AcReg;                  //-- C�/�c ��� �������������

    float   Ac2High;                    //-- ������ ������� ������ ��� ��
    float   AcHigh;                     //-- ������� ������ ��� ��
    float   AcLow;                      //-- ������ ������ ��� ��
    float   CgHigh;                     //-- ������� ������ ��� ��
    float   CgLow;                      //-- ������ ������ ��� ��

};


//-- c�������� �������� �� ������ � ����������
struct sLimitsParam
{
    unsigned short int TpHigh;
    unsigned short int TpLow;
    unsigned short int TdHigh;
    unsigned short int TdLow;
    unsigned short int EHigh;
    unsigned short int ELow;    
    unsigned short int EReg;
    unsigned short int TpReg;
};

enum uAutoCtrl{
   acHandl,                  // ���������� �������� �������
   acALT_Unit,                // ���������� ������� � ��������������� ����������
   acKontroller             // ���������� ������� �� �����������
};
//-- ��������� ����� ���������� ����������� ---------------------------------------------
struct sControlParam
{
    double      CO[MAX_OBJECT];     //-- ������������ ��
    double      Tp[MAX_OBJECT];     //-- �������� ����������� �����
    double      Td[MAX_OBJECT];     //-- �������� ����������� �������
    double      EDS[MAX_OBJECT];    //-- �������� ���
    int         TpAutoCtrl[MAX_OBJECT];             //-- ������� ��������� ��
    int         TdAutoCtrl[MAX_OBJECT];             //-- ������� ��������� �d
    int         EDSAutoCtrl[MAX_OBJECT];            //-- ������� ��������� ���
    int         CgAutoCtrl[MAX_OBJECT];             //-- ������� ��������� Cg (������ � ������� = 2)
    int         BurnAutoCtrl[MAX_OBJECT];           //-- �������  ������ "������ �������"
    int         TurnON_Ctrl[MAX_OBJECT];            //-- �������  "���� ��������"
    //-----------------------
    sControlParam();
    //-- ���������� �������� �� ���������
    void SetDefault(void);
    //-- ��������� �� �������
    bool LoadFromRegistry(bool SetDefaultOnError);
    //-- ��������� � ������
    bool StoreToRegistry();
};

//-- ��������� ���������� ����������� ���������
struct sProgParam
{
    int *pTpData;           //-- ������ �����������
    int *pCgData;           //-- ������ ����������� ����������
    int  Count;             //-- ����� ���������� ��������
    //-----------------------
    //-- �����������
    sProgParam();
    sProgParam(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul);

    ~sProgParam();
    //-- ���������� ������ �� �������
    void ReleaseObjects(void );
    void SetObjects(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul);

    //-- ��������� �������
    void FillData(sEtapsProfile* Etaps,int Interval,int TpMul,int CgMul);
};


//-- ��������� �������� ������� �� ���� ��������
//-- ��� ��������� � ����������� �������� � ��� ��������� � ������
struct sGraphicLimit
{
    double Min;
    double Max;
    double Step;
};

//-- �������������� ���������� �������/���������� -----------------------------
enum eSourceId
{
    eProgram = 0,       //-- ���������
    eController1,       //-- ���������� 1
    eController2,
    eController3,
    eController4,
    eController5,
    eZone1,             //-- ���� 1
    eZone2,
    eZone3,
    eZone4,
    eZone5,
    eEndoGen1,          //-- ������������� 1
    eEndoGen2,
    eEndoGen3,
    eEndoGen4,
    eEndoGen5,
    eHeater1,           //-- ���� 1
    eHeater2,
    eHeater3,
    eSpreadHeater,      //-- ���������� ����
    eTermodat,          //-- ��������           20
    ePusherFurnace,     //-- ��������� ����
    eUnknown            //-- ����������� ��������
};
/*
//-- �������������� ���������� �������/���������� -----------------------------
enum eParamId
{
    eTp = 0,            //-- ����������� ����
    eTd,                //-- ����������� �������
    eEDS,               //-- ��� �������
    eCO2,               //-- �������� ����.-� ��2
    eAc,                //-- ���������� ��������
    eCg                 //-- ���������� ���������
};

//-- �������������� ������� � ��� -------------------------------------------
enum eEventId
{
    eEmptyEvent = 0,
    eRunArm,
    eStopArm,
    eExcaptionOnWork,
    eAccessChange,
    eAlarmPSW,
    eLostConnect,
    eSetInterval,
    eSetCO,
    eSetTp,
    eCalcError,

    eTpHighLimit,
    eTpLowLimit,
    eTdHighLimit,
    eTdLowLimit,
    eEHighLimit,
    eELowLimit,
    eAc2HighLimit,
    eAcHighLimit,
    eAcLowLimit,
    eCgHighLimit,
    eCgLowLimit,

    eStartProfile,
    eStopProfile,
    eExtrStopProfile,
    eSetTd,
    eSetEDS,

    eTpNormal,
    eTdNormal,
    eENormal,
    eAcNormal,
    eCgNormal,

    //-- �������� eSetEDSAdjusting �������
    eSetEDSAdjusting,       // ������ ��� ��� �������������
    eSetCg,
    eSetCgAdjusting,         // ������ �� ��� ������������� (��� �����)

    eBurnSensor,            //-- ������ ������� ����� ������ �������
    eLostConnectALT,        //-- ������ ����� � ��� �����������(��������)
    eErrALT,                //-- ��� ����������(��������)������������� �� ������

    eMaxEventId
};
*/
//-- ��������� ����� ��������� ----------------------------------------------
struct sOptions
{
    int             Interval;                   //-- �������� ������ �����������
    int             VisibleInterval;            //-- �������� ���������� ����������
    int             EventView;                  //-- ������ ���������� �������
    int             RegulTYPE;                  //-- ��� ���������� 0- ������ ���������� 1-����� ��������
    int             FlSetUstTermodat;           //-- ���� - ��� ������ ������ ������� � ��������
    int             Magazining;                 //-- ������ ���������� ����������
    int             DB_Updating;                //-- ������ ���������� ����

    sLimits         All_Limits[MAX_OBJECT];     //-- ������� ���� ����������
    sGraphicLimit   GraphicLimits[MAX_PARAM];   //-- ����������� ������� ����������
    sGraphicLimit   ProfileLimits;              //-- ����������� ������� �������
    //------------------------------------------
    sOptions();
    //-- ��������� �� ���������
    void SetDefault(void);
    //-- ��������� �� �������
    bool LoadFromRegistry(bool SetDefaultOnError);
    //-- ��������� � ������
    bool StoreToRegistry();
};

class TSourceName : public TStringList{
bool FlStore;
public:
   bool __fastcall StoreToRegistry();
   bool __fastcall LoadFromRegistry();
   void __fastcall ReNameSource(AnsiString as, int Index);

   __fastcall TSourceName(bool aFlStore);
   __fastcall ~TSourceName();
};   

class TID_value {
bool FlStore;
int Count;
public:
   bool __fastcall StoreToRegistry();
   bool __fastcall LoadFromRegistry();
   __fastcall TID_value(bool aFlStore);
   __fastcall ~TID_value();
   void __fastcall SetCount(int aCount){Count = aCount;};
   int __fastcall GetCount(){return Count;};
};   




//---------------------------------------------------------------------------
//      ������ ���������� ����������
//---------------------------------------------------------------------------

extern char* cAccessMode[];
//-- ������������ ������� �������
//---------------------------------------------------------------------------
enum eAccessMode
{
    eUser   = 0,        //-- ������������
    eTechnolog,         //-- ��������
    eAdmin,             //-- ������������� ���
    eDebugger,          //-- �������� ���
    eNethost,
    eNetServer
};

// ���� ��������� ������
enum eErrorCode
{
   eErrNoError,
   eErrDllNotFound,
   eErrDllIncorrect,
   eErrCOM_PordBusy
};


//extern eAccessMode     AccessMode;         //-- ����� �������

extern eErrorCode LastErrorCode;                      // ��� ������

extern char*           cFlameCtrlRegKey;           //-- ���� � ������� ��� �������� ���������� ������������
//extern sArmParam       all_param[MAX_OBJECT];      //-- ��������� ���� �������
//extern sArmParam       back_param[MAX_OBJECT];     //-- ����������� ���������
extern sControlParam   ctrl_param;                 //-- ��������� ����������
//extern sProfileParam   profile[MAX_PROFILE];       //-- ������ ��������
//extern bool            is_profile[MAX_PROFILE];    //-- �������� ������������ �������
//extern sProgParam      prog_profile[MAX_PROFILE];  //-- ��������� ������������ ��������
//extern TDateTime       ParamVisibleInt;            //-- ��������� �������� ���������� ����������
extern TDateTime       EventVisibleInt;            //-- ��������� �������� ���������� �������
extern sOptions        Options;                    //-- ����� ���������
extern TList*          ParamList;                  //-- ������������ ������ ����������
extern TList*          EventList;                  //-- ������������ ������ �������
extern char*           EventName[];                //-- �������� �������
extern TSourceName *   pSourceName;                //-- ����� ��������
extern TID_value   *   pID_value;                  //-- ����� ��������������
extern char*           SourceName[];               //-- �������� ���������� �������
extern char*           ParamName[];                //-- �������� ����������
extern sBlockConf      OpControl[];                //-- ��������� ������������ ��������
extern sBlockConf      OpOxidFuel[];               //-- ��������� ��������: ��������� ����������/�������
//extern int             iObjectId[MAX_OBJECT];      //-- �������������� ��������
//extern BYTE            BurnSensorBits;             //-- ���� ��������� ������� ��������


extern sConfItem LinkParam[];
extern sConfItem CtrlParam[];
extern sBlockConf Config[];






//---------------------------------------------------------------------------
//      ������ ������������ ��������
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//-- ��������� ��� ������ � ������������ ������� ���������� �������
//---------------------------------------------------------------------------
//void __fastcall AddParamList(TList* ParamList, sArmParam* all_param);
//void __fastcall ClearParamList(TList* ParamList);
//void __fastcall ClearEventList(TList* pList);
//int* __fastcall FuncData(TList* ParamList, int ObjectIdx, int ParamIdx, int Mul, int &Count);
int  __fastcall SamplesOfPeriod (TDateTime StartTime, TDateTime EndTime, int Interval);
//bool __fastcall CreateUnknowParamList(TList *ParamList, int Count);
//bool __fastcall SetDataParamList(TList *ParamList, int Idx, sArmParam* one_param,int ObjectIdx);
//bool __fastcall GetIndexFromTime(sArmParam* one_param,TDateTime StartTime,int Interval, int &Idx);
bool __fastcall GetIndexFromOptimTime(sOptimalParam* optim_param,TDateTime StartTime,int Interval, int &Idx);
//bool __fastcall GenerateParam(sArmParam &param,int Source);
bool __fastcall GenerateEvent(int EventId,int Source,char* Comment);
//bool __fastcall PackParam(sArmParam* all_param,sOptimalParam* optim_param);
double __fastcall CorrectDigits(TEdit *edt);

//---------------------------------------------------------------------------
//-- ��������� �������� ������� ��������� ������� CXT �� ������ double
//---------------------------------------------------------------------------
int* __fastcall CXTFuncData(double* Ptr, int Mul, int Count);

//-- ������������ ����������� ������� �� �������� ��������� -----------------
char* __fastcall DoubleToComment(double Value);

int  __fastcall GetLastErrorCode();

#endif
