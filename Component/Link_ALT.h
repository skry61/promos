//---------------------------------------------------------------------------
#ifndef Link_ALTH
#define Link_ALTH
//---------------------------------------------------------------------------
//#define Link_TDH
#include "Params.h"


//-- ��������� ���������� �����
//bool LoadChannelDll_T (AnsiString aDllName);

//-- ��������� ���������� �����
//bool FreeChannelDll_T (void);

//int GetTerm(void);
//int GetParam(int Index);
typedef void __fastcall (__closure * tSetMessage)(bool aAlarm, AnsiString aMsg);


// ����������� ����� ��������������� ����������
class ALT_Unit{
   //-- ����� ����� � ��������� ���������� --------------------------------
   int  (__cdecl *pExchange)(int CMD, int addr, int *size,void *Buff);
   void (__cdecl *pSetChanal)(char Com, short Speed, char ParityMode, char ParityType, char NumberBits, char StopBitLength);
   void (__cdecl *pSetInterface)(short);
   void (__cdecl *pSetSerialPort)(char COMPort);

   //-- ��������� ����������
   virtual bool __fastcall LoadDll (AnsiString aDllName);
   virtual bool __fastcall LoadRegisterCOM(void);
   virtual bool __fastcall LoadRegisterNodes (AnsiString aKeyName);

//   virtual bool __fastcall LoadRegister (void);

   //-- ��������� ���������� �����
   virtual bool __fastcall FreeDll (void);

   //-- ��������� ���������� �����
//   bool __fastcall LoadChannelDll (AnsiString aDllName);
   bool __fastcall LoadChannelDll (AnsiString aDllFileName,AnsiString aKeyName);
   //-- ��������� ���������� �����
   bool __fastcall FreeChannelDll (void);
protected:
   virtual bool __fastcall LoadDllFunction (void);
   //-- ���������� ��������� ���������� -----------------------------------
   HINSTANCE hDLL;
   bool Fl_CommonLib;
   //-- ������ ���������
   BYTE Nodes[80];
   void (__cdecl *pSetNode)(short);

public:
   bool Result;
   __fastcall ALT_Unit::ALT_Unit(AnsiString aDllFileName,AnsiString aDllFileName1,
                                 AnsiString aKeyName,HINSTANCE ahDLL);
//   __fastcall ALT_Unit(void);
   __fastcall  ~ALT_Unit();
   virtual int __fastcall GetParam(int Index,int *Ind);
};

class TTermoDat : public  ALT_Unit{

   bool ExchangeErrorFixed[80];
   // ��������� ��������� ���������
   tSetMessage SetMessage;
   //-- ����� ����� � ��������� ���������� --------------------------------
   int  (__cdecl *pGet_T)();
   int  (__cdecl *pSetUst)(short);
   int  (__cdecl *pExchangeTD)(int CMD,int &size,void *Buff);

   void __fastcall FixErr(int Index,int Res);
   bool __fastcall LoadDllFunction (void);

   int FUst[80];
   int FDisabled[80];
public:
   __fastcall TTermoDat(AnsiString aDllFileName,HINSTANCE hDLL,tSetMessage aSetMessage);
//   __fastcall  ~TTermoDat();
   virtual int __fastcall GetParam(int Index, int *Ind);
   int __fastcall SetUst(int V,int Index);
   int __fastcall Disable(int Index);
};

#endif

