//---------------------------------------------------------------------------
#ifndef MultiGrapherH
#define MultiGrapherH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include "XGrapher.h"
#include <ExtCtrls.hpp>
#include "Profile.h"

class TCalcProfThread;
//---------------------------------------------------------------------------
class PACKAGE TMultiGrapher : public TXGrapher
{
private:
  // ��� ����� �������
    sProfileParam *profile1,*profile2,*profile3;

    int FTick;
    int FTimeProf1;
    int FTimeProf2;
    int FTimeProf3;
    TColor FColorProf1;
    TColor FColorProf2;
    TColor FColorProf3;
    double FStartConst;
    double FKoeffCO;
    double FKoeff_D;
    double FKoeff_B;
    double FEndC;
    double FEndX;
    double FTp;
    double FTd;
    double FEDS;
    double FTp1;
    double FTd1;
    double FEDS1;
    double FTp2;
    double FTd2;
    double FEDS2;
    AnsiString FLegentName1;
    AnsiString FLegentName2;
    AnsiString FLegentName3;
    void __fastcall SetTimeProf1(int value);
    void __fastcall SetTimeProf2(int value);
    void __fastcall SetTimeProf3(int value);
    void __fastcall SetColorProf1(TColor value);
    void __fastcall SetColorProf2(TColor value);
    void __fastcall SetColorProf3(TColor value);
    void __fastcall SetStartConst(double value);
    void __fastcall SetKoeffCO(double value);
    void __fastcall SetKoeff_D(double value);
    void __fastcall SetKoeff_B(double value);
    void __fastcall SetEndC(double value);
    void __fastcall SetEndX(double value);
    void __fastcall SetTp(double value);
    void __fastcall SetTd(double value);
    void __fastcall SetEDS(double value);

    void __fastcall InitWithConst (double V, bool Empty, int Time, double aDivisor);
    void __fastcall SetTp1(double value);
    void __fastcall SetTd1(double value);
    void __fastcall SetEDS1(double value);
    void __fastcall SetTp2(double value);
    void __fastcall SetTd2(double value);
    void __fastcall SetEDS2(double value);
    void __fastcall SetLegentName1(AnsiString value);
    void __fastcall SetLegentName2(AnsiString value);
    void __fastcall SetLegentName3(AnsiString value);

    
protected:
//    virtual void __fastcall FinishPaint(void){}; // ��������� �����������
    virtual void __fastcall SetHMax(int value);

public:
    void __fastcall FirstThreadPart();
    void __fastcall SecondThreadPart();
    void __fastcall ThirdThreadPart();

    __fastcall TMultiGrapher(TComponent* Owner);
    __fastcall ~TMultiGrapher();

    //-- ��������� ��������� � � ��������� �������� �������
//    void __fastcall DrawLimitsLabels(TCanvas *cnv);//void);
    //-- ��������� ��������� � � ��������� �������� �������
//    void __fastcall DrawLimitsLines(TCanvas *cnv);//void);
    virtual void __fastcall DrawTime(TCanvas *cnv){};
    
    virtual void __fastcall Paint(void);
    bool SetStartProfile(sProfileParam *);
    double* LoadFromFile(AnsiString);
    LoadConstValue(double);
    void __fastcall SetTick(int value);
    // ���������� �������� �������������
    void DrawLegende(TCanvas *cnv);


__published:
    __property int Tick  = { read=FTick, write=SetTick };
    __property int TimeProf1  = { read=FTimeProf1, write=SetTimeProf1 };
    __property int TimeProf2  = { read=FTimeProf2, write=SetTimeProf2 };
    __property int TimeProf3  = { read=FTimeProf3, write=SetTimeProf3 };
    __property TColor ColorProf1  = { read=FColorProf1, write=SetColorProf1 };
    __property TColor ColorProf2  = { read=FColorProf2, write=SetColorProf2 };
    __property TColor ColorProf3  = { read=FColorProf3, write=SetColorProf3 };
    __property double StartConst  = { read=FStartConst, write=SetStartConst };
    __property double KoeffCO  = { read=FKoeffCO, write=SetKoeffCO };
    __property double Koeff_D  = { read=FKoeff_D, write=SetKoeff_D };
    __property double Koeff_B  = { read=FKoeff_B, write=SetKoeff_B };
    __property double EndC  = { read=FEndC, write=SetEndC };
    __property double EndX  = { read=FEndX, write=SetEndX };
    __property double Tp  = { read=FTp, write=SetTp };
    __property double Td  = { read=FTd, write=SetTd };
    __property double EDS  = { read=FEDS, write=SetEDS };
    __property double Tp1  = { read=FTp1, write=SetTp1 };
    __property double Td1  = { read=FTd1, write=SetTd1 };
    __property double EDS1  = { read=FEDS1, write=SetEDS1 };
    __property double Tp2  = { read=FTp2, write=SetTp2 };
    __property double Td2  = { read=FTd2, write=SetTd2 };
    __property double EDS2  = { read=FEDS2, write=SetEDS2 };
    __property AnsiString LegentName1  = { read=FLegentName1, write=SetLegentName1 };
    __property AnsiString LegentName2  = { read=FLegentName2, write=SetLegentName2 };
    __property AnsiString LegentName3  = { read=FLegentName3, write=SetLegentName3 };

};
//---------------------------------------------------------------------------
#endif



