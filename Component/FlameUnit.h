//---------------------------------------------------------------------------

#ifndef FlameUnitH
#define FlameUnitH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>

#include "Params.h"
#include "Profile.h"
#include "EventMen.h"
#include "ParamMen.h"
#include "Clap_Pipe.h"
#include "Link_ALT.h"


//#include "FlameList.H"
#pragma option push -b-
enum Units_Type { NULL_UNIT, uOVEN, uENDO,uOVEN_Lite };
#pragma option pop

enum eStatusType{
   sstNone,
   sstInProc,
   sstReady,
   ssExtrStopProfile
};


// ��� ��������� �� �������
typedef enum eProcType{
   pSetDisp,          // �������� � ���������� �� ������ ���� ��� ������������ ����/����
   pSetNormalDisp,    // �������� � ���������� �� ������ ����������
   pSetValue,         // ������ �������� ���������
   pFU1,              // ������ ��� Unit
   pFU2,
   pFU3,
   pFU4,
   pShowStat,         // ��� FlameList -�������� ������
   pShowAlarm,        // ����������� ���������� ��������
   pSetComboBoxAll,   // ��������� ComboBox ������� ���� ���������
   pSetComboBoxOven,  // ��������� ComboBox ������� ������ �����
   pSetComboBoxProfile,// ��������� ComboBox ������� ������ ����� c ��������
   pSetCtrlParam,     // ���������� ��������� �������� ��� ����������
   pUpLoadCtrlParToPLC,     // ���������� ��������� �������� ��� ����������
   pSetArmParam,      // ���������� ��������� ��������
   pSetLimits,        // ���������� ��������� �������� ���������
   pPersInputInfo,    // ������ �������� ����������
   pTestAltEnable,    // ��������� �� ������ ����� ���������� � ����������
//   pSetClap_Pipe,   // ���������� �������
   pSetIDValue,       // ������ �������� �������������� �����
   pExecuteProf,      // ������ ��������
   pMakeTermalProg,   // ���������� ���� ���������� ���������
   pGetFirstProf,     // ����� ������ ������� � �����
   pClearParamArray,  // ���������� ����� ���������� ��� ������ �����
   pInitCtrlParam,    // ���������������� ��������� ��������
   pAcceptPSW,        // ���������� �������� � ��������
   pSetParMen,        // ������ ������ �������� ����������

   pTestSameProf,     // �������� �� ������� ���� �� ������� � ����� �� �����
   pLoadMultiData     // ��������� ������ ��� �������������
   
} TProcType;   

/* */
/*
//-- �������������� ���������� �������/���������� -----------------------------
enum eParamId
{
    eTp = 0,            //-- ����������� ����
    eTd,                //-- ����������� �������
    eEDS,               //-- ��� �������
    eCO2,               //-- �������� ����.-� ��2
    eAc,                //-- ���������� ��������
    eCg                 //-- ���������� ���������
};

*/
/**/
//-- �������������� ������� � ��� -------------------------------------------
/*
enum eEventId
{
    eEmptyEvent = 0,
    eRunArm,
    eStopArm,
    eExcaptionOnWork,
    eAccessChange,
    eAlarmPSW,
    eLostConnect,
    eSetInterval,
    eSetCO,
    eSetTp,
    eCalcError,

    eTpHighLimit,
    eTpLowLimit,
    eTdHighLimit,
    eTdLowLimit,
    eEHighLimit,
    eELowLimit,
    eAc2HighLimit,
    eAcHighLimit,
    eAcLowLimit,
    eCgHighLimit,
    eCgLowLimit,

    eStartProfile,
    eStopProfile,
    eExtrStopProfile,
    eSetTd,
    eSetEDS,

    eTpNormal,
    eTdNormal,
    eENormal,
    eAcNormal,
    eCgNormal,
    
    //-- �������� eSetEDSAdjusting �������
    eSetEDSAdjusting,       // ������ ��� ��� �������������
    eSetCg,
    eSetCgAdjusting,         // ������ �� ��� ������������� (��� �����)

    eBurnSensor,            //-- ������ ������� ����� ������ �������
    eLostConnectALT,        //-- ������ ����� � ��� �����������(��������)
    eErrALT,                //-- ��� ����������(��������)������������� �� ������

    eMaxEventId
};
*/
/**/

//#include "DbUnit.h"



const HeightP = 260;
const HeightE = 200;
const HeightPL = 100;

const xWight =  230;

const CommonX = 4;
const CgX   = 20;
const AcX   = 20;
const NamePanelY = 4;
const NamePanelHeight = 33;
const CommonWight = 230 - CommonX*2;
const L_1Y = 37;
const L_2Y = 85;
const L_12H = 50;
const CgDisp =50;
//------------------------------------------------------------------------------

const BevelYp = 133; // �� 134
const BevelYe = 88;  // ��89
const BevelYpl = 55;

const L_EdsY = 11;   //������������ �����
const L_EdsX = 20;   //
const L_EdsH = 22;   //

const L_VEdsY = 13;   //������������ �����
const L_VEdsX = 130;   //
const L_VEdsH = 18;   //

const L_TpY = 35;    //������������ �����
//..const L_TpX = 48;    //
const L_TpX = 94;    //
const L_TpH = 13;   //

const L_VTpY = 35;    //������������ �����
const L_VTpX = 168;    //
const L_VTpH = 13;   //

const L_TdY = 55;    //������������ �����
//..const L_TdX = 28;    //
const L_TdX = 74;    //
const L_TdH = 13;   //

const L_VTdY = 55;    //������������ �����
const L_VTdX = 168;    //
const L_VTdH = 13;   //

const BotPanelY = 60;//������������ �����

#define ConstrPL(Type,x,y,w,h,text) { \
   Type->Left = (x); Type->Width = (w); Type->Height = (h);\
   Type-> Caption = (text);\
   Type->Top = (y);\
   }
   
#define ConstrPI(Type,x,y,w,h/*,text*/) { \
   Type->Left = (x); Type->Width = (w); Type->Height = (h);\
/*   Type->Picture->LoadFromFile((text));*/\
   Type->Top = (y);\
   }

#define CompPL(Comp,aAlignment,aAutoSize,aAlign,aParent){\
    Comp->Alignment = aAlignment;Comp->AutoSize = aAutoSize;\
    Comp->Align = aAlign;\
    Comp->Parent  = aParent;\
    Comp->ParentColor  = true;}
    
#define CompL(Comp,aAlignment,aAutoSize,aAlign){\
    Comp->Alignment = aAlignment;Comp->AutoSize = aAutoSize;\
    Comp->Align = aAlign;\
    Comp->Parent  = RegimPanel;}
    
#define CompPANEL(Comp,Sz){\
    Comp->Font->Name = "Arial";\
    Comp->Font->Size = Sz;\
    }
    
#define CompLABEL(Comp,Sz){\
    Comp->Font->Name = "Arial";\
    Comp->Font->Size = Sz;\
    Comp->Font->Style = TFontStyles();\
    }
//---------------------------------------------------------------------------
//typedef void __fastcall (__closure *TBucketProc)(void *AInfo, void *AItem, void *AData, bool &AContinue);
//viod __fastcall SetDisp(TWinControl * wc, void *AInfo = NULL);
//bool __fastcall ForEach(TBucketProc AProc, void *AInfo = NULL);



//-- ��������� ����� ���������� ����������� ---------------------------------------------
typedef struct sControlUParam
{
    double      CO;     //-- ������������ ��
    double      Tp;     //-- �������� ����������� �����
    double      Td;     //-- �������� ����������� �������
    double      EDS;    //-- �������� ���

    double       TpAutoCtrl;             //-- ������� ��������� ��
    double       TdAutoCtrl;             //-- ������� ��������� �d
    double      EDSAutoCtrl;            //-- ������� ��������� ���
    double      CgAutoCtrl;            //-- ������� ��������� ���
    double      BurnAutoCtrl;           //-- �������  ������ "������ �������"

    sControlUParam()
    {
        SetDefault();
    }
    //-- ���������� �������� �� ���������
    void SetDefault(void);
/*
    //-- ���������� �������� �� ���������
    void SetDefault(void)
    {
            CO  = CO_DEFAULT;
            Tp  = TP_DEFAULT;
            Td  = TD_DEFAULT;
            EDS = EDS_DEFAULT;
            TpAutoCtrl   = TP_AUTOCONTROL_DEFAULT;
            TdAutoCtrl   = TD_AUTOCONTROL_DEFAULT;
            EDSAutoCtrl  = EDS_AUTOCONTROL_DEFAULT;
            BurnAutoCtrl = BURN_AUTOCONTROL_DEF;

    }
*/
} ControlUParam;


class TFlameList;
class TFlameUnit;

//---------------------------------------------------------------------------
// ����� ��������� �� �����
class TParamUnit
{
private:
    TLabel *Unit_Name;              // ����� �����
    TLabel *Unit_Value;             // ����� ��������
    int FAutoCtrl;
    int FStatus;
    int FSize;
    bool    Phasa;                  // ���� ��������� ������
    eParamId TypeLabels;            // ������� : ��������� ���������� �����������, ��, ��, ���
    int Limits[4];
//    double LimitsValue[4];
    bool FVisible;
    void __fastcall SetValue(double value);

    double __fastcall GetValue();
    void __fastcall SetStatus(int value);
    int __fastcall GetStatus();
    void __fastcall SetVisible(bool value);

    virtual void __fastcall SetLimLValue(double value) = 0;
    virtual double __fastcall GetLimLValue() = 0;
    virtual void __fastcall SetLimHValue(double value) = 0;
    virtual double __fastcall GetLimHValue() = 0;
    virtual void __fastcall SetRegValue(double value){ *FReg = value;};
    virtual double __fastcall GetRegValue(){return *FReg;};

    int  __fastcall GetFontSize();
    void __fastcall SetFontSize(int fs); // ������ ������
    AnsiString __fastcall GetNewName();
    void  __fastcall SetNewName(AnsiString n);

protected:                            // ��� �������� ������� ������������ (�������������)
    bool FFl_LOW;                     // ���������� ������� � ���������� ������ ������� ���������
    bool FFl_HIGH;                    // ���������� ������� � ���������� ������� ������� ���������
    unsigned short int *FLimitHigh;   // �������� ������ ��� ������ ����������
    unsigned short int *FLimitLow;    // ������  -----//-------
    unsigned short int *FReg;         // �������� ������������� ���������
    double FValue;
    double *FpValue;
    TFlameUnit * Owner;


public:
    int TriggerPhasa();

    void __fastcall Repaint();

//    TParamUnit(AnsiString Name, int FontSize);
    TParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner);
    int DispTop(int);
    int SetTop(int);
    ~TParamUnit();
    virtual void __fastcall SetLimitAddr(sLimits *aLimits)=0;
    virtual AnsiString __fastcall GetTextValue()=0;
    //-- ��������� �������� ���������� ��������
    virtual bool __fastcall CheckLimits(void);

    void __fastcall AcceptPSW(unsigned char psw);
    void __fastcall SetArmParam(double *pValue){ FpValue = pValue;};
    void __fastcall SetParamValue(double value);

    __property double Value      = { read=GetValue, write=SetValue };
    __property int    Status     = { read=GetStatus, write=SetStatus }; // ����� ��������� ������
    __property bool   Visible    = { read=FVisible, write=SetVisible };
    __property int    AutoCtrl   = { read=FAutoCtrl, write=FAutoCtrl };

    __property double LimitLow   = { read=GetLimLValue, write=SetLimLValue };
    __property double LimitHigh  = { read=GetLimHValue, write=SetLimHValue };
    __property double RegValue   = { read=GetRegValue, write=SetRegValue };
    __property bool   Fl_LOW     = { read=FFl_LOW, write=FFl_LOW };// ���������� ������� � ���������� ������ ������� ���������
    __property bool   Fl_HIGH    = { read=FFl_HIGH,write=FFl_HIGH};// ���������� ������� � ���������� ������� ������� ���������

    __property int    FontSize   = { read=GetFontSize, write=SetFontSize }; // ������ ������
    __property AnsiString NewName= { read=GetNewName, write=SetNewName }; // ����� ���

};

//---------------------------------------------------------------------------
//---  ����� �������������� ������� ����������    ----
//  ������������� ���������
class TTpParamUnit: public TParamUnit
{
    virtual void __fastcall SetLimLValue(double value);
    virtual double __fastcall GetLimLValue();
    virtual void __fastcall SetLimHValue(double value);
    virtual double __fastcall GetLimHValue();
    virtual void __fastcall SetRegValue(double value){ *FReg = value;};
    virtual double __fastcall GetRegValue(){return *FReg;};
public:
    TTpParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner):
       TParamUnit(MainPanel,Name,Top ,Left,Widht,Heigth, FontSize, aTypeLabels,aOwner){};


    virtual void __fastcall SetLimitAddr(sLimits *aLimits);
    virtual AnsiString __fastcall GetTextValue(){
      return AnsiString::FloatToStrF( Value,AnsiString::sffFixed,5,0)+ " C�";;
    };
/*
    __property double LimitLow   = { read=GetLimLValue, write=SetLimLValue };
    __property double LimitHigh  = { read=GetLimHValue, write=SetLimHValue };
    __property double RegValue   = { read=GetRegValue, write=SetRegValue };
*/
};

//---------------------------------------------------------------------------
class TTdParamUnit: public TTpParamUnit
{
public:
    virtual void __fastcall SetLimitAddr(sLimits *aLimits);
    TTdParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner):
       TTpParamUnit(MainPanel,Name,Top ,Left,Widht,Heigth, FontSize, aTypeLabels,aOwner){};

};


//---------------------------------------------------------------------------
class TEParamUnit: public TTpParamUnit
{
public:
    virtual void __fastcall SetLimitAddr(sLimits *aLimits);
    TEParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner):
       TTpParamUnit(MainPanel,Name,Top ,Left,Widht,Heigth, FontSize, aTypeLabels,aOwner){};
    virtual AnsiString __fastcall GetTextValue(){
      return AnsiString::FloatToStrF( Value,AnsiString::sffFixed,5,0)+ " mB";
    };

};

//---------------------------------------------------------------------------
class TCgParamUnit: public TTpParamUnit
{
    virtual void __fastcall SetLimLValue(double value);
    virtual double __fastcall GetLimLValue();
    virtual void __fastcall SetLimHValue(double value);
    virtual double __fastcall GetLimHValue();
    virtual void __fastcall SetRegValue(double value){ *FRegF = value;};
    virtual double __fastcall GetRegValue(){return *FRegF;};
protected:
    float *FLimitHighF;
    float *FLimitLowF;
    float *FRegF;
public:
    TCgParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner):
       TTpParamUnit(MainPanel,Name,Top ,Left,Widht,Heigth, FontSize, aTypeLabels,aOwner){
          FLimitHighF = NULL;
          FLimitLowF = NULL;
          FRegF = NULL;

       };
    virtual void __fastcall SetLimitAddr(sLimits *aLimits);
    virtual AnsiString __fastcall GetTextValue(){
      return AnsiString::FloatToStrF( Value,AnsiString::sffFixed,4,3) +" %";
    };
    //-- ��������� �������� ���������� ��������
    virtual bool __fastcall CheckLimits(void);


};

//---------------------------------------------------------------------------
class TAcParamUnit: public TCgParamUnit
{
    float *FLimitHighF2;
public:
    TAcParamUnit(TPanel *MainPanel,AnsiString Name,int Top ,int Left,int Widht,int Heigth, int FontSize, eParamId aTypeLabels,TFlameUnit * aOwner):
       TCgParamUnit(MainPanel,Name,Top ,Left,Widht,Heigth, FontSize, aTypeLabels,aOwner){
           FLimitHighF2 = NULL;
       };
    virtual void __fastcall SetLimitAddr(sLimits *aLimits);
    //-- ��������� �������� ���������� ��������
    virtual bool __fastcall CheckLimits(void);

};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//����� ������ ������
class TRegimUnit
{
//    TTypeREGIM TypeREGIM;
public:
    TLabel         *Regim_Value;
    TImage         *More_InMain;
    TImage         *Less_InMain;
//    TLabel         *ReginInMain;

private:
//    TRegims  FRegim;
    int  FRegim;
    void __fastcall SetRegim(int value);
    int __fastcall GetRegim();
    TImage * __fastcall GetImage1(){return More_InMain;};
    TImage * __fastcall GetImage2(){return Less_InMain;};
public:
    void __fastcall SetNewImage(int ImgIndex);
    TRegimUnit(TLabel *Name,TImage *Image,TImage *Image1,int Reg);
    __property int Regim  = { read=GetRegim, write=SetRegim };
    
};

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

typedef Set<Units_Type, NULL_UNIT, uENDO>  TUnitTypes;

typedef bool __fastcall (__closure * TGenerateEvent)(int EventId,int Source,char* Comment);

struct sTermAssignment
{
  short Cg_Assignment;   // ������� ��
  short Tp_Assignment;   // ������� ����������� ����

};

//---------------------------------------------------------------------------
class PACKAGE TFlameUnit : public TPanel//TPaintBox
{
protected:
    int             FIndexInFlameList; // ������ ����. � ������-��������
    TFlameList     *FlameList;         // ������-��������
    sArmParam      *ArmParam;          // ��������� �� ��������� ��������
    sLimits        *Limits;            // ���� ��������� ��� �������� (�������������)
    TParamMen      *FParamMenager;     // ����� ������� �� FlameList
    TClap_Pipe     *FClap_Pipe;        // ��������� �� �������
    unsigned char   Fpsw;              // ����� ��������� �����������


    TList          *ParamList;         // ����� ����������� ����������

    TPanel         *TypeUnitPanel;     // ������ ���� (���� / �������������) �����.
    TBevel         *Bvl;               // ����� �����������

    AnsiString      FUCaption;         // ������������ ���������
    int        FCarburizer_Oven_Status;// ��������� ���� ������� �����������=1/�����=2/ ������� ��� =0

    int FObjectId;                     // ������������� ������� (��� ��� ��������� ��� ������������ ������� � � ������ �������)
    void __fastcall RepaintUnit();
    static int FObjectIdValue;
    bool FIs_profile;
    bool FlIn_MathExeption;            // ���� - ��������� � ��������� ������� ����������
                                       //  (��� ��������� �������� ������ ������ � ������)
    double FTp;
    double FTd;
    double FCo;
    double F_Ac;
    double F_Cg;
    double FEDS;
    //-----------------------------
    double vFCtrlTp;
    double vFCtrlTd;
    double vFCtrlCO;
    double vFCtrlEDS;
    int vFTpAutoCtrl;
    int vFTdAutoCtrl;
    int vFEDSAutoCtrl;
    int vCgAutoCtrl;
    int vFBurnAutoCtrl;
    int vTurnON_Ctrl;
    double *FCtrlTp;
    double *FCtrlTd;
    double *FCtrlCO;
    double *FCtrlEDS;
    int *FTpAutoCtrl;
    int *FTdAutoCtrl;
    int *FEDSAutoCtrl;
    int *FCgAutoCtrl;
    int *FBurnAutoCtrl;
    int *FTurnON_Ctrl;
    bool FPultStatus;   //���������� �� �����. � ��������� �������� "���� �������"

    sProfileParam* FProfile;
    sProgParam   * Fprog_profile;  //-- ��������� ������������ ��������

    //-----------------------------
    void __fastcall SetIs_profile(bool value);
    void __fastcall SetUCaption(AnsiString value);
    AnsiString __fastcall GetUCaption();
    void __fastcall SetType(Units_Type value);
    void __fastcall SetTp(double value);
    void __fastcall SetTd(double value);
    void __fastcall SetCo(double value);
    void __fastcall Set_Ac(double value);
    void __fastcall Set_Cg(double value);
    void __fastcall SetEDS(double value);
    void __fastcall SetStatus(int value);
    double __fastcall GetTp();
    double __fastcall GetTd();
    double __fastcall GetCo();
    double __fastcall Get_Ac();
    double __fastcall GetEDS();
    double __fastcall Get_Cg();

    bool __fastcall GetIsOVEN();
    int  __fastcall GetIndexInFlameList();
    void __fastcall SetCtrlTp(double value);
    void __fastcall SetCtrlTd(double value);
    void __fastcall SetCtrlCO(double value);
    void __fastcall SetCtrlEDS(double value);
    void __fastcall SetTpAutoCtrl(int value);
    void __fastcall SetTdAutoCtrl(int value);
    void __fastcall SetEDSAutoCtrl(int value);
    void __fastcall SetCgAutoCtrl(int value);
    void __fastcall SetBurnAutoCtrl(int value);
    void __fastcall SetTurnON_Ctrl(int value);

    double __fastcall GetCtrlTp(void);
    double __fastcall GetCtrlTd(void);
    double __fastcall GetCtrlCO(void);
    double __fastcall GetCtrlEDS(void);
    int    __fastcall GetTpAutoCtrl(void);
    int    __fastcall GetTdAutoCtrl(void);
    int    __fastcall GetEDSAutoCtrl(void);
    int    __fastcall GetCgAutoCtrl(void);
    int    __fastcall GetBurnAutoCtrl(void);
    int    __fastcall GetTurnON_Ctrl(void);
    
    void   __fastcall SetProfile(sProfileParam* value);
    sProfileParam* __fastcall GetProfile();
    void __fastcall SetFirstProfile(sProfileParam* value);
    sProfileParam* __fastcall GetFirstProfile();
    void __fastcall SetObjectId(int value);
    void __fastcall SetCCW(unsigned char value);
    void __fastcall SetCCW_(unsigned char value);    // ������ � ���������� �� �����������
    unsigned char __fastcall GetCCW();

    int    __fastcall GetTpReg(void);
    void __fastcall SetTpReg(int value);

    void __fastcall SetPSW(unsigned char value);

    sProgParam * __fastcall GetProg_profile();
    void __fastcall SetProg_profile(sProgParam*);
    void __fastcall SetClap_Pipe(TClap_Pipe  * value);
    TClap_Pipe  * __fastcall GetClap_Pipe();
    bool __fastcall MathHandle(bool IsCalcCg , bool IsCalcCgOld);
    bool __fastcall GetUseEtaps();

    //-- ��������/������ ���� ��������� ������� ��������
    bool __fastcall GetDoBurnSensor();
    void __fastcall SetDoBurnSensor(bool Abcw);

public:
    TParamUnit  *Cg;             // ����� ������ ��
    TParamUnit  *Ac;             //              ��
    TParamUnit  *EdsValue;
    TParamUnit  *TpValue;        //
    TParamUnit  *TdValue;
    TParamUnit  *CoValue;

    TRegimUnit  *Regim;
    double      *FParam;

    bool __fastcall ForEach(TProcType Type, void *AInfo = NULL);

    __fastcall TFlameUnit(TComponent* Owner);
    virtual __fastcall ~TFlameUnit();
    
    // ��� ��������� ������ ���������
    bool __fastcall GenerateEvent(int EventId,int Source,char* Comment);
    // ��� ��������� ��������� �� �������� �������
    bool __fastcall GenerateEvent(int EventId,char* Comment);
    //���������� �� �����. � ��������� �������� "���� �������"
    void __fastcall SetPultStatus(bool st);
    void __fastcall SetIs_Current(bool value);
    bool __fastcall GetIs_Current();


    void __fastcall SetArmParam(sArmParam * aPar);
    double * __fastcall GetParam(){ return ArmParam->Param;}
    void __fastcall SetLimits(sLimits *aLimits);
    void __fastcall SetParMen(TParamMen *aParamMenager){
        FParamMenager = aParamMenager;
    };


//    void __fastcall SetClap_Pipe(TClap_Pipe *aClap_Pipe){ FClap_Pipe = aClap_Pipe; }

    void __fastcall SetReference(TFlameList *aFlameList,int Index);
    void __fastcall SetEvent(int EventId,int Source,char* Comment);
    int* __fastcall MakeFuncData(int ParamIdx, int Mul, int &Count);
    // ������������� ������ ����������� ���������
//    void __fastcall FillProgData(int Interval,int TpMul,int CgMul);
    int * __fastcall FillProgData(int Interval,int TpMul,int CgMul, int Tag);
    //--  ���������� ���� ��������� ���������� ---------------------------------
    bool __fastcall MakePrepareProg(sTermAssignment * TermAssignment);
    //--  ���������� ���� ���������� ���������  --------------------------------
    bool __fastcall MakeTermalProg(sTermAssignment * TermAssignment, TTermoDat * pTermoDat);  
    //--  ���������� ���� ��������� ����� --------------------------------------
    bool __fastcall MakeCommonProg(sTermAssignment * TermAssignment, sEtapsProfile *pEtaps);

    void __fastcall SetCtrls(
        double      *aCO,                     //-- ������������ ��
        double      *aTp,                     //-- �������� ����������� �����
        double      *aTd,                     //-- �������� ����������� �������
        double      *aEDS,                    //-- �������� ���
        int         *aTpAutoCtrl,             //-- ������� ��������� ��
        int         *aTdAutoCtrl,             //-- ������� ��������� �d
        int         *aEDSAutoCtrl,            //-- ������� ��������� ���
        int         *aCgAutoCtrl,             //-- ������� ��������� Cg
        int         *aBurnAutoCtrl            //-- �������  ������ "������ �������"
    );
    byte __fastcall UpLoadCtrls();
    // �������� � ����� ���������� �������� � ������
    bool __fastcall InitCtrlParam();

    void __fastcall ClearReference()
    {
      FlameList = NULL;
    }
    void SetValueAll(double *);
    void SetValueAll();
    // ������� ���������� ��� ������ �����
    bool __fastcall GenerateParam(void);
    //--  ������ �������� ����������   ---------------------------------------------
    bool __fastcall PersInputInfo(TTermoDat *TermoDat);


   void  __fastcall TriggerAlarmPhasa();
   // ������ �������
   bool  __fastcall CalculateProfile();


   // ��������� �������
   bool  __fastcall ExecuteProfile(unsigned char  *ccw);
   bool  __fastcall TestSameProfile(sProfileParam *p);
   // ������������ ��������� �������
   bool  __fastcall ReleaseProgProfile();
   bool  __fastcall LoadMultiData(double ** Data);
   // ������� �������
   bool  __fastcall RecognizeProfile(sProfileParam *p);
   __property sProgParam * Prog_profile = { read=GetProg_profile, write=SetProg_profile };  //-- ��������� ������������ ��������

   __property sProfileParam* Profile  = { read=GetProfile, write=SetProfile };
   //������ ������� � ������ �����
   __property sProfileParam* FirstProfile  = { read=GetFirstProfile, write=SetFirstProfile };
   __property double * Param               = { read=FParam, write=FParam };
   __property unsigned char CCW            = { read=GetCCW, write=SetCCW };
   // ������ � ���������� �� �����������
   __property unsigned char CCW_           = { read=GetCCW, write=SetCCW_ };
   __property TClap_Pipe  * Clap_Pipe      = { read=GetClap_Pipe, write=SetClap_Pipe };
   __property int TpReg                    = { read=GetTpReg, write=SetTpReg };
   __property bool        DoBurnSensor     = { read=GetDoBurnSensor, write=SetDoBurnSensor};      //-- ���� ��������� ������� ��������


protected:
   TGenerateEvent FOnSetStat;         // C���� ��������� - �������
   Units_Type      FUnit_Type;        // ��� ���������� (����/�������������)


__published:
//	__property Units_Type Unit_Type = {read=FUnit_Type, write=FUnit_Type, default=0};
    __property int ObjectId  = { read=FObjectId, write=SetObjectId };
    __property bool Is_profile  = { read=FIs_profile, write=SetIs_profile };
    __property bool Is_Current = { read=GetIs_Current, write=SetIs_Current };
    __property AnsiString UCaption  = { read=GetUCaption, write=SetUCaption };
    __property TGenerateEvent OnSetStat = {read=FOnSetStat, write=FOnSetStat};
    __property Units_Type Type  = {read=FUnit_Type,  write=SetType , default=0};
    __property double Tp         = { read=GetTp, write=SetTp };
    __property double Td         = { read=GetTd, write=SetTd };
    __property double Co         = { read=GetCo, write=SetCo };
    __property double _Ac        = { read=Get_Ac, write=Set_Ac };
    __property double _Cg        = { read=Get_Cg, write=Set_Cg };
    __property double EDS        = { read=GetEDS, write=SetEDS };
    __property int Status        = { read=FCarburizer_Oven_Status, write=SetStatus };
    __property bool IsOVEN       = { read=GetIsOVEN };
    __property int IndexInFlameList  = { read=GetIndexInFlameList };
    __property double CtrlTp     = { read=GetCtrlTp, write=SetCtrlTp };
    __property double CtrlTd     = { read=GetCtrlTd, write=SetCtrlTd };
    __property double CtrlCO     = { read=GetCtrlCO, write=SetCtrlCO };
    __property double CtrlEDS    = { read=GetCtrlEDS, write=SetCtrlEDS };
    __property int TpAutoCtrl    = { read=GetTpAutoCtrl, write=SetTpAutoCtrl };
    __property int TdAutoCtrl    = { read=GetTdAutoCtrl, write=SetTdAutoCtrl };
    __property int EDSAutoCtrl   = { read=GetEDSAutoCtrl, write=SetEDSAutoCtrl };
    __property int CgAutoCtrl    = { read=GetCgAutoCtrl, write=SetCgAutoCtrl };
    __property int BurnAutoCtrl  = { read=GetBurnAutoCtrl, write=SetBurnAutoCtrl };
    __property int TurnON_Ctrl   = { read=GetTurnON_Ctrl, write=SetTurnON_Ctrl };
    __property bool UseEtaps     = { read=GetUseEtaps };
    __property unsigned char psw = { read=Fpsw, write=SetPSW };//C���� ��������� �����������
    __property bool PultStatus =   { read=FPultStatus, write=SetPultStatus };//���������� �� �����. � ��������� �������� "���� �������"


};

bool Assigned(void *);
//-- ������������ ����������� ������� �� �������� ��������� -----------------
//char* __fastcall DoubleToComment(double Value);

//---------------------------------------------------------------------------
#endif


