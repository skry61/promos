//---------------------------------------------------------------------------
// ������ ����������������
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Config.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//-- ���������� ������� StringGrid � �������� ���������� --------------------
int __fastcall ConfPrepareSG (TStringGrid *SG,AnsiString aStr1, AnsiString aStr2)
{
    SG->ColCount = 2;
    SG->RowCount = 2;
    SG->DefaultRowHeight = 18;

    SG->ScrollBars = ssVertical;
    SG->ColWidths[0] = ((SG->Width*80)/100)-24;
    SG->ColWidths[1] = ((SG->Width*20)/100)-2;
    SG->Cells[0][0] = aStr1;
    SG->Cells[1][0] = aStr2;
    return 0;
}

//-- ���������� ������� StringGrid �� ����� ������������ --------------------
int __fastcall ConfIntoSG (TStringGrid *SG,sBlockConf* pBlock,int Idx)
{
    SG->RowCount = (pBlock + Idx)->Count + 1;
    for (int i=0; i < (pBlock + Idx)->Count; i++)
    {
        SG->Cells[0][i+1] = AnsiString((pBlock + Idx)->Items[i].Name);

        SG->Cells[1][i+1] = (pBlock + Idx)->Items[i].Value;
    }
    return 0;
}

//-- ���������� ����� ������������ �� ��������� ������ ������� StringGrid ---
int __fastcall SGIntoConf (TStringGrid *SG,sBlockConf* pBlock,int Idx)
{

    return 0;
};

//-- �������� ����� ������������ �� ������� ������� ------------------------
int __fastcall REGIntoConf (AnsiString RegKey,sBlockConf* pBlock,int Idx)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    RegKey += AnsiString("\\");
    RegKey += AnsiString((pBlock + Idx)->Name);
    if (!Reg->OpenKey(RegKey,false))
    {
        delete Reg;
        return -1;
    }
    for (int i=0; i < (pBlock + Idx)->Count; i++)
    {
        try
        {
            (pBlock + Idx)->Items[i].Value = Reg->ReadInteger (AnsiString((pBlock + Idx)->Items[i].Name));
        }
        catch (Exception &exception)
        {
            ShowMessage (AnsiString("� ������� ��� ��������� ")
                        + AnsiString((pBlock + Idx)->Items[i].Name));
        }
    }
    Reg->CloseKey();
    delete Reg;
    return 0;
}
//-- ���������� ����� ������������ � ������� ������� ------------------------
int __fastcall ConfIntoREG (AnsiString RegKey,sBlockConf* pBlock,int Idx)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    RegKey += AnsiString("\\");
    RegKey += AnsiString((pBlock + Idx)->Name);
    Reg->OpenKey(RegKey,true);

    for (int i=0; i < (pBlock + Idx)->Count; i++)
    {
        Reg->WriteInteger (AnsiString((pBlock + Idx)->Items[i].Name),
                            (pBlock + Idx)->Items[i].Value);
    }
    Reg->CloseKey();
    delete Reg;
    return 0;
};

//-- �������� ��������� �� ������� ������� �� ����� ------------------------
bool __fastcall GetRegParam (AnsiString RegKey,AnsiString Name,AnsiString &Value)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    if (!Reg->OpenKey(RegKey,false)) return false;

    Value = Reg->ReadString (Name);
    if (Value.IsEmpty()) return false;
    Reg->CloseKey();
    delete Reg;
    return true;
}
//-- �������� ��������� � ������� ������� �� ����� -------------------------
bool __fastcall PutRegParam (AnsiString RegKey,AnsiString Name,AnsiString Value)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    if (!Reg->OpenKey(RegKey,true)) return false;

    Reg->WriteString (Name,Value);
    Reg->CloseKey();
    delete Reg;
    return true;
}

//-- �������� �������� ������ �� ������� ������� �� ����� ------------------------
bool __fastcall GetRegBinData (AnsiString RegKey,AnsiString Name,void* Ptr, int Count)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    if (!Reg->OpenKey(RegKey,true)) return false;

    int writed = Reg->ReadBinaryData (Name,Ptr,Count);
    if (writed != Count)
    {
        Reg->CloseKey();
        delete Reg;
        return false;
    }

    Reg->CloseKey();
    delete Reg;
    return true;
}

//-- �������� �������� ������ � ������ ������� �� ����� --------------------------
bool __fastcall PutRegBinData (AnsiString RegKey,AnsiString Name,void* Ptr, int Count)
{
    TRegistry *Reg = new TRegistry;
    Reg->RootKey = HKEY_LOCAL_MACHINE;
    if (!Reg->OpenKey(RegKey,true)) return false;

    Reg->WriteBinaryData (Name,Ptr,Count);
    Reg->CloseKey();
    delete Reg;

    return true;
}

//-- ���������� ���������� �� ������ -----------------------------------------
bool __fastcall FillComboBox(TComboBox *CB,char** Str,int Count)
{
    CB->Items->Clear();
    for (int i=0;i<Count;i++)
    {
        CB->Items->Add(Str[i]);
    }
    CB->ItemIndex = 0;
    return true;
}

//-- �������� ������������� �������� � ��������� ������ ----------------------
bool __fastcall DirExists (AnsiString Name)
{
    int fCode = GetFileAttributes(Name.c_str());
    bool hf = ((fCode != -1) && (FILE_ATTRIBUTE_DIRECTORY & fCode) )?true:false;
    return hf;
}

//--  ����������� ������� ���������� ������������ �������  -----------------
void __fastcall InitGrid1(TStringGrid *SG)
{
    ConfPrepareSG(SG,"��������","��������");
    SG->RowCount = 13;
    SG->ColWidths[0] = 260;
    SG->ColWidths[1] = 140;     
    SG->DefaultRowHeight = 20;     
    SG->Height = SG->RowCount *  (SG->DefaultRowHeight + 1) + 4;// 276;     
    SG->Cells[0][1] = "�������� �������/������";

    SG->Cells[0][2] = "����� �������/������";
    
    SG->Cells[0][3] = "����� �����";

    SG->Cells[0][4] = "��������� �������� ��, %";

    SG->Cells[0][5] = "������� ���������� ����, ��";

    SG->Cells[0][6] = "��� ���������� ����, ��";

    SG->Cells[0][7] = "����������� ����.��������, ��2/�";

    SG->Cells[0][8] = "����������� �����������, ��/�";

    SG->Cells[0][9] = "��������� ����������� �, %";

    SG->Cells[0][10] = "             �� �������, ��";

    SG->Cells[0][11] = "����� ������������ �������";

    SG->Cells[0][12] = "����� ������������ �������";
}

//--  �������� ������� ���������� �������  -----------------
void __fastcall ShowGrid1(TStringGrid *SG,sProfileParam* prof)
{
    SG->Cells[1][1] = AnsiString(prof->Name);

    SG->Cells[1][2] = IntToStr(prof->Num);
    
    SG->Cells[1][3] = IntToStr(prof->SadkaNum);

    SG->Cells[1][4] = AnsiString(prof->C0);

    SG->Cells[1][5] = AnsiString(prof->X_Wid);

    SG->Cells[1][6] = AnsiString(prof->dX);

    SG->Cells[1][7] = AnsiString(prof->D);

    SG->Cells[1][8] = AnsiString(prof->B);

    SG->Cells[1][9] = AnsiString(prof->EndC);

    SG->Cells[1][10] = AnsiString(prof->EndX);

    SG->Cells[1][11] = prof->Start.FormatString ("yyyy\\mm\\dd hh:nn.ss");

    AnsiString aTime =
        AnsiString(prof->AllTime/3600) +
        AnsiString(":") +
        AnsiString((prof->AllTime/60)%60) +
        AnsiString(".") +
        AnsiString(prof->AllTime%60);
    SG->Cells[1][12] = aTime;

}



//--  ����������� ������� ������� ���������� ��������  -----------------
void __fastcall InitGrid2(TStringGrid *SG)
{
    ConfPrepareSG(SG,"��������","��������");
    SG->RowCount = 11;
    SG->ColWidths[0] = 60;
    SG->ColWidths[1] = 54;     
//    SG->DefaultRowHeight = 20;     
    SG->Height = SG->RowCount *  (SG->DefaultRowHeight + 1) + 4;// 276;     
    SG->Cells[0][1] = "�o, %";
    SG->Cells[0][2] = "��, %";
    SG->Cells[0][3] = "��, %";
    SG->Cells[0][4] = "��, %";
    SG->Cells[0][5] = "X�,��";
    
    SG->Cells[0][6] = "-- ��� --";
    
    SG->Cells[0][7] = "��,C�";
    SG->Cells[0][8] = "��,C�";
    SG->Cells[0][9] = "���,��";
    SG->Cells[0][10] = "��,%";

}


//--  �������� ������� ���������� �������  -----------------
void __fastcall ShowGrid2(TStringGrid *SG,sProfileParam* prof,double* DPTR)
{
    SG->Cells[1][1] = AnsiString(prof->C0);
    SG->Cells[1][2] = AnsiString::FloatToStrF(prof->Cf,AnsiString::sffFixed,5,3);
    SG->Cells[1][3] = (prof->Data)?AnsiString::FloatToStrF(prof->Data[0],
                                AnsiString::sffFixed,5,3):AnsiString(" - ");
    SG->Cells[1][4] = AnsiString(prof->EndC);
    SG->Cells[1][5] = AnsiString(prof->EndX);
  
    SG->Cells[1][6] = "-- ��� --";
    SG->Cells[1][7] = AnsiString::FloatToStrF(DPTR[0],AnsiString::sffFixed,5,1);
    SG->Cells[1][8] = AnsiString::FloatToStrF(DPTR[1],AnsiString::sffFixed,5,1);
    SG->Cells[1][9] = AnsiString::FloatToStrF(DPTR[2],AnsiString::sffFixed,5,1);
    SG->Cells[1][10]= AnsiString::FloatToStrF(DPTR[4],AnsiString::sffFixed,5,3);;
    
}
