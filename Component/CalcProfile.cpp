#include <math.h>
#include <vcl.h>
#include "CalcProfile.h"

TCalc_ProfFunc *Calc_ProfFunc; // ��������� �� ������� ��� ������ ������� ������� �������

TStadyInf  StadyInfArray[STADYS];

//double *ProfArrayForAllStady;// ������ ������� ������� �� ��� ������

//-------------------   ������� ���������  -----------------------
double Const6      = 6.0;
double Const_T_abs = 273;
double Const_1t98  = 1.98;
double Const1t5    = 1.5;
double Const_0t5   = 0.5;
double Const_min1  = -1.0;
double Const_min2  = -2.0;
double Const_1     = 1.0;
double Const_0     = 0.0;
double Const_0t25  = 0.25;
double Const_2     = 2.0;
double Const1_E    = 31887.0;  // ������� ���������  (���/����)
double Const1_G    = 0.857;    // �����  G           (1/%)
//---------------   ������� ���������� ������  -------------------
                             
//-------------------------------------------------------------------------------------
// ����������� ��� ���������� ������     
__fastcall TStadyInf::TStadyInf(){
     PokExp = 1;        //��������� ������� ���������� ����������double ProfileArray[MAXPOINTS];     // ������ ������� �������
			            // ��� ������ ������, ���� ������� ����.�����double TmpDouble;                   // ������� ������ ������� ������� ������}
			            // ��������� Cgdouble StadyTimesArray[STADYS];     // ���������� ������
     StartCg = 1;       // ������� ��������� �������� Cgdouble PokExp[STADYS];             // //-------------------------------------------------------------------------------------
     EndCg = 1;         // ������� �������� �������� Cg//-------------------   ������� ���������  -----------------------//�������� ��������
     Bt = 1;            // ������� �������� Bettadouble Const6      = 6.0;double	Load_1(double aSumSteps){
     BoundCond = 3;     // ������� ����� ���������� �������double Const_T_abs = 273;      return  Const_1;
     CgLow = 1;         // ������� ����� ������ ��������� Cgdouble Const_1t98  = 1.98;}
				        // 1-��������; 2-����double Const1t5    = 1.5;
     TStart = 900;      // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;double	Load_0(double aSumSteps){
     TEnd = 900;        // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;				        // ��� ������� ������double Const_min1  = -1.0;      return  Const_0;
}

//-------------------------------------------------------------------------------------
//�������� ��������
long double	Load_1(double aSumSteps){
      return  Const_1;
}

long double	Load_0(double aSumSteps){
      return  Const_0;
}
long double	Load_0_(double aSumSteps){
      return  Const_0;
}

//--------------------------------------------------------------------------------------
//������ ������������ ��������
long double	Calc_ExpForD(double aSumSteps){
      return  exp(-Const1_E/ (Const_1t98 * (CurT_Start + aSumSteps * dTemper)))*D0;
}
long double Calk_D (double Cg){
      return (1.0 + Const1_G * Cg)*ExpForD;
}

//--------------------------------------------------------------------------------------
//������ �����. Betta
long double Calc_B (double aSumSteps){
     // ��������� �� ���������� �������
     switch(CurBoundCond){
     case 1:                         // ������� ����
	     return (0.0);

     case 2:                         // 2-�� ����
	     return (-1.0);

     case 3:                         // 3-�� ����
	     return (-1.0 / CurBt);
     }    
     return (0.0);
}

//--------------------------------------------------------------------------------------
//����� ��������� 1/0
long double Choice_1_or_0(double aSumSteps){
     // ��������� �� ���������� �������
     if(CurBoundCond == 2)
	     return 0;
     else
	     return 1;
}

//--------------------------------------------------------------------------------------
//������ �������� �������� ����������� ����������
long double Calc_Cg (double aSumSteps){
     // ��������� �� ������ ��������� �g � ������� ������
     if(CurCgLow == 1)              // �������� �����
	      return CurStartCg + (CurEndCg - CurStartCg) * aSumSteps / CurStadyTime;
                                    // ���������������
//2b4c
	      long double Tmp = (exp(CurPokExp * aSumSteps) - Const_1)*
                              (CurEndCg - CurStartCg);
//2b7e
	      return Tmp / (exp(CurPokExp * CurStadyTime) - Const_1);
}

//--------------------------------------------------------------------------------------

void  TCalcProf::CulcData(PProc P7,PProc P6,PProc P5,PProc P4,PProc P3,PProc P2,PProc P1,
     double*PSumSteps, double* ProfData, int NPoints, double Xl, double lStep, int NSteps, int equ2)
{
     double Local_dX,Local_NPoints,Step_dXdX;
     double * DataPtr;
     double  Tmp,Tmp1,Tmp2,Tmp_e;
     double  Tmp_1e,Tmp_16;
     double  Tmp_2e,Tmp_26;
     double  Tmp_3e,Tmp_36;
     double  Tmp_4e,Tmp_46;
     double  Tmp_5e,Tmp_56;
     double  D_div_B;
     int i;
     
  Local_NPoints = NPoints;               //� �������� ���������� 50
  Local_dX = Xl / Local_NPoints;         //��������� ��� (2.5/50)  = 0.05
  Step_dXdX = lStep /Local_dX/Local_dX;  // (0.05/6)/(2.5/50)/(2.5/50) = 3.33333
  memcpy(Block3, ProfData,(NPoints+1) * sizeof(double));
  
  while(NSteps--){                       // ��� ������� ������ = 12, ��� �������  Tst/Step ~ 20
    *PSumSteps += lStep;                    // ���������� 0.05/6 - 0.008333
	DataPtr = ProfData;
	ExpForD = Calc_ExpForD(*PSumSteps); // ������  ���������� * D0 -  ����� �������
	for(i = 0; i < equ2; i++){              // ��� ����
	   long double _B = P5(*PSumSteps);     // Calc_B   - ������ Betta
	   long double  D = P7(*Block3);        // Calk_D() - ������ �������� ������������ ��������
	   long double  D_div_B = D * _B;
	   long double  Tmp0 = Const1t5 * D_div_B / Local_dX; //-0.172
	   Tmp =  P6(*PSumSteps)/*Choice 0  ��� 1*/ - Tmp0;
	   Tmp1 =  Const_2 * D_div_B / Local_dX;
	   Tmp2 = -Tmp1*Const_0t25;
//	   long double Tmp3 = P2(*PSumSteps);   // Load_1
	   //         Load_1             Calk_D   ?????  -NPoints �� ��������
	   D_div_B = P2(*PSumSteps) * P7(Block3[NPoints-1]);     // Calk_D() - ������ �������� ������������ ��������
//26c4
	   Tmp0 = D_div_B * Const1t5 / Local_dX;
	   Tmp_4e = P1(*PSumSteps) +Tmp0;    // Load_0_
//2703
	   Tmp_56 = Const_min2 * D_div_B / Local_dX;
	   Tmp_5e = -Tmp_56 * Const_0t25;
//2721
	   ProfData = DataPtr;    //� ������ 
//275c               D
	   Tmp_3e = P7((*Block3++ + *Block3)*Const_0t5) * Step_dXdX;
//279a
	   Tmp_36 = P7((*Block3++ + *Block3)*Const_0t5) * Step_dXdX;
//27ad
	   Tmp_2e = Tmp_3e + Tmp_36 + 1;
//27c3     ���������
	   D_div_B = Tmp - Tmp_3e * Tmp2 / Tmp_36;
//27ea
	   Tmp_26 = *Block1 = -(Tmp_2e * Tmp2 / Tmp_36 + Tmp1)/D_div_B;
	   ProfData++;
//                              Calc_Cg
	   Tmp_1e = *Block2 = (P4(*PSumSteps) + *ProfData * Tmp2/Tmp_36)/D_div_B;
//2842
	   D_div_B = Tmp_2e - Tmp_26 * Tmp_3e;
	   *++Block1 = Tmp_36 / D_div_B;
	   *++Block2 = (Tmp_3e * Tmp_1e + *ProfData) / D_div_B;
	   // C ����, �� 2 ����� ��� ����������
	   for (int j = 2; j< NPoints;j++){
//2886
	       Tmp_26 = *Block1++;
	       Tmp_1e = *Block2++;
	       Tmp_3e = Tmp_36;
//28f4                  Calc_D
               try {
                  Tmp_36 = P7((Block3[0] + Block3[1])*Const_0t5) * Step_dXdX;
               }
               catch (...)
               {
                  Tmp_36 = P7(Block3[0]) * Step_dXdX;
               }
	       Block3++;
//2907
	       Tmp_2e = Tmp_3e + Tmp_36 + 1;
//2919         ���������
	       D_div_B = Tmp_2e - Tmp_26 * Tmp_3e;
	       *Block1  = Tmp_36 / D_div_B;
//2948
	       *Block2  = (Tmp_3e * Tmp_1e+ *++ProfData) / D_div_B;

	   }
//2966     ���������
	   D_div_B = Tmp_4e - Tmp_36 * Tmp_5e / Tmp_3e;
//2983
	   Tmp_16  = - (Tmp_2e * Tmp_5e / Tmp_3e + Tmp_56)/D_div_B;
//29b6               0
	   Tmp_e = P1(*PSumSteps) + *ProfData * Tmp_5e/Tmp_3e/D_div_B;
	   *Block3 =  (*Block2++ * Tmp_16 + Tmp_e) / (Const_1 - *Block1++ * Tmp_16);
	   for(int j=NPoints; j; j--){
	       D_div_B = *Block3--;
//2a2e
	       *Block3 = *--Block1 * D_div_B + *--Block2;
	   }

	}
	ProfData = Block3;
	Block3 = DataPtr;
  }
}
//-------------------------------------------------------------------------------
double*  TCalcProf::NewCXT_Func(sProfileParam* profile){
 int Ndt,TotalCount;
 double Step,SummaSteps,Xl;
     if((profile->Time > 3600 * 24) || (profile->Time <=5 )) return profile->Data;
     Xl = profile->X_Wid;                       // ������� ���������� ����
     Step = profile->dX;                        //��� = 0.05 �� (50 ����� - 2.5 ��)
     D0 = profile->D*10000;

     
     CurStadyTime = (double)profile->Time/3600;  //StadyInfArray[i].StadyTime;
     Ndt = 2 * (int)(CurStadyTime / (2 * Step) + 0.1); //���������� ����� ��������� �������
     //����������� ���������� ����� 20
     if(Ndt < 20)  Ndt  = 20;

     //������ ������ ����
     Step = CurStadyTime / Ndt;
     SummaSteps = 0;
     CurPokExp = 1;                               //��������� ������� ���������� ����������
     CurStartCg = profile->Cg;                    // ������� ��������� �������� Cg
     CurEndCg   = profile->Cg;                    // ������� �������� �������� Cg
     CurBt   = profile->B;                        // ������� �������� Betta
     if(CurStartCg >*profile->DataBase)
         CurBoundCond = 3;                            // ������� ����� ���������� �������
     else  
         CurBoundCond = 1;                            // ������� ����� ���������� �������
     CurCgLow   = 1;                              // ������� ����� ������ ��������� Cg
				                                  // 1-��������; 2-����
     CurT_Start   = profile->Tp;                  // �� ������� ������������� ����������� ������� ���������
                                            	  // ��� ������� ������
     // �� �������� ����������� ���������� ��������� � ������� �� ����� ������
     dTemper = (profile->Tp - CurT_Start) / CurStadyTime;// �������� ��������� �����������
     CurT_Start = CurT_Start + Const_T_abs; //���������� ��������� �����������


     //���� ������ ��� �� ��������� - �������� 
     if(PredArraySize == 0){                         //������ ������ �� ������ ����� ������
         Block1 = new double[profile->Points];
         Block2 = new double[profile->Points];
         Block3 = new double[profile->Points];
         ProfileArray = new double[profile->Points];
     //���� ������ ������� ��������� - ������������
     } else if(PredArraySize != profile->Points){
         delete [] Block3;
         delete [] Block2;
         delete [] Block1;
         delete [] ProfileArray;
         Block1 = new double[profile->Points];
         Block2 = new double[profile->Points];
         Block3 = new double[profile->Points];
         ProfileArray = new double[profile->Points];
     //����� �������� � ��� �� ��������
     }
     PredArraySize = profile->Points;
     
     memcpy(ProfileArray, profile->DataBase,profile->Points * sizeof(double));
//              7       6           5       4       3      2      1
     CulcData(Calk_D,Choice_1_or_0,Calc_B,Calc_Cg,Load_0,Load_1,Load_0_,
	  &SummaSteps,ProfileArray,profile->Points-1/*50*/, Xl,Step/6.0, 12, 2);
//              7      6             5       4       3      2      1
     CulcData(Calk_D,Choice_1_or_0,Calc_B,Calc_Cg,Load_0,Load_1,Load_0_,
	  &SummaSteps,ProfileArray,profile->Points-1/*50*/, Xl, Step, Ndt-2, 2);

     memcpy(profile->Data,ProfileArray,profile->Points * sizeof(double));
     return ProfileArray;
}

//---------------------------------------------------------------------------

__fastcall TCalcProf::TCalcProf()
{ 
   PredArraySize = 0;      // ������ ������� ����������������� �� ���������� ����
}     
//---------------------------------------------------------------------------
__fastcall TCalcProf::~TCalcProf()
{
   if(PredArraySize != 0){       //������������ ������ �� ������ ����� ������
       delete [] Block3;
       delete [] Block2;
       delete [] Block1;
       delete [] ProfileArray;
   }    
}     


TCalcProf *CalcProf; 
//---------------------------------------------------------------------------



