//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#pragma link "Profile"
#pragma link "XGrapher"
#include "MultiGrapher.h"
#include "FMath.h"
#include "CalcProfThread.h"
#include <stdio.h>
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.     
static inline void ValidCtrCheck(TMultiGrapher *)
{
    new TMultiGrapher(NULL);
}
//---------------------------------------------------------------------------
__fastcall TMultiGrapher::TMultiGrapher(TComponent* Owner)
    : TXGrapher(Owner)
{
    FKoeffCO    = 20;  //% fhhhh   hhh
    FKoeff_D    = 4;
    FKoeff_B    = 0.8;
    FEndC       = 0.35;
    FEndX       = 1.25;
    FTimeProf1  = FTimeProf2 = FTimeProf3 = 30;
    FTd         = 900;
    FTp         = 900;
    FEDS        = 1126;
    FColorProf1 = clGreen;
    FColorProf2 = clNavy;
    FColorProf3 = clRed;
    FHMax = 250;
    FVMax = 1200;
    FVDivisor = 1000;
    FHDivisor = 100;
    FVStep = 100;     
    FHStep = 50;
    FLegentName1 = "���� 1*";
    FLegentName2 = "���� 2*";
    FLegentName3 = "���� 3*";
    
    InitWithConst (0.2, true,FTimeProf1,FVDivisor);       //bbb
    Caption = "���������� �������";
    int i_t = (profile3->AllTime + profile3->Time)/60;
    if(i_t >= 1439) i_t = 1439;
    MarkerTime = TDateTime(i_t/60,i_t%60,0,0);
    EndH = (int)(profile3->EndX*(double)100.001);
    EndV = profile3->EndC*1000;
    LimitLow = profile3->C0*1000;
    F_Value = profile3->Cg * 1000; 
    
}

//------------------------------------------------------------------------------
__fastcall TMultiGrapher::~TMultiGrapher()
{
    if (CalcProfThread)
    {
        CalcProfThread->Resume();
        CalcProfThread->Terminate();
        CalcProfThread->WaitFor();
        delete CalcProfThread;
//        while()
//        CalcProfThread->FreeOnTerminate = true;
    }
    delete profile1;
    delete profile2;
    delete profile3;
    p_int = NULL;
    //    delete CalcProfThread;
}

//------------------------------------------------------------------------------
void __fastcall TMultiGrapher::Paint(void)
{
    TCanvas *cnv = StartPaint();
    intCount = profile3->Points;
    p_int    = profile3->DataInt;

    if(p_int)
    {
        int * FuncData3 = p_int;
        PrepareCalc();

        DrawLimitsLines(cnv);
        cnv->Pen->Style  = psSolid;
        cnv->Pen->Width = FLineWidth;
        double StepX = (double)(FHMax - FHMin)/intCount;

        int X1,X2,Y1,Y2;
        bool AllUnknown = true;

        for (int i=0;i < intCount;i++)
        {
            if ( *FuncData3++ != UNKNOWN_DATA )
            {
                AllUnknown = false;
                break;
            }
        }
        if (AllUnknown)
        {
            cnv->Brush->Color = clGray;
            cnv->Brush->Style = bsSolid;
            cnv->FillRect(GraphRect);
            DrawLabel (cnv, GraphRect);
        }else{
            // ������� ��������� �������
            FuncData3 = p_int;
            int * FuncData2 = profile2->DataInt;
            int * FuncData1 = profile1->DataInt;
            
            int *FuncDataNext3 = p_int+1;
            int *FuncDataNext2 = FuncData2+1;
            int *FuncDataNext1 = FuncData1+1;
            
            double CurX = FHMin;
            for (int i=0;i<intCount-1;i++)
            {   //��������� ���������� ����� �� X
                X1 = XonGrapher (CurX);  CurX += StepX; X2 = XonGrapher (CurX);

                if ( *FuncData3 != UNKNOWN_DATA &&
                     *FuncDataNext3 != UNKNOWN_DATA )
                {
                    //============ ������ �����  ===============================
                    cnv->Pen->Color = FColorProf3;                     //���� �����
                    //�������� ���������� ����� �� Y
                    Y1 = YFunconGrapher ( *FuncData3++ ); Y2 = YFunconGrapher ( *FuncDataNext3++ );
                    //����������
                    cnv->MoveTo (X1,Y1);
                    cnv->LineTo (X2,Y2);
                    //============ ������ �����  ===============================
                    cnv->Pen->Color = FColorProf2;                    //���� �����
                    //�������� ���������� ����� �� Y
                    Y1 = YFunconGrapher ( *FuncData2++ ); Y2 = YFunconGrapher ( *FuncDataNext2++ );
                    //����������
                    cnv->MoveTo (X1,Y1);
                    cnv->LineTo (X2,Y2);
                    //============ ������ �����  ===============================
                    cnv->Pen->Color = FColorProf1;                   //���� �����
                    //�������� ���������� ����� �� Y
                    Y1 = YFunconGrapher ( *FuncData1++ ); Y2 = YFunconGrapher ( *FuncDataNext1++ );
                    //����������
                    cnv->MoveTo (X1,Y1);
                    cnv->LineTo (X2,Y2);
                }
                else
                {
                    TRect NoRect;
                    NoRect.Top = GraphRect.Top + 1;
                    NoRect.Bottom = GraphRect.Bottom - 1;
                    NoRect.Left = X1;
                    NoRect.Right = X2;

                    cnv->Brush->Color = clGray;
                    cnv->Brush->Style = bsSolid;
                    cnv->FillRect(NoRect);
                    // �� ����� �����
                    FuncDataNext3++; FuncData3++;
                    FuncDataNext2++; FuncData2++;
                    FuncDataNext1++; FuncData1++;
                }
            }
            PrepareMarker();                  //���������� ������ - �� �����������
            DrawLabel (cnv, GraphRect);
            if (IsProfile) DrawLimitsLabels(cnv);
            DrawTime(cnv);                     //��������� �� ���� ����������� �������
            DrawLegende(cnv);
            
        }
    }
    FinishPaint();
    if (FOnPaint) FOnPaint(this);
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::InitWithConst (double V, bool Empty, int Time, double aDivisor)
{
    if (Empty){
        profile1 = new sProfileParam(2.5,0.01,V);
        profile1->Source = 1;
        strncpy(profile1->Name,"����� ����",29);
        profile1->Num    = 1;
        profile1->SadkaNum = 1;
        profile1->C0 = FKoeffCO;
//        profile1->InitData(2.5,0.01,V);
        profile1->D =  FKoeff_D;
        profile1->B =  FKoeff_B;
        profile1->Create = etpC0func;
        profile1->EndC = FEndC;
        profile1->EndX = FEndX;
        profile1->Tp   = FTp;         //-- ����������� ����
        profile1->Td   = FTd;         //-- ����������� 
        profile1->EDS  = FEDS;        //-- 
        profile1->Mul = aDivisor;

        profile2 = new sProfileParam();
        profile3 = new sProfileParam();
    }    
    profile1->Done = false;
    profile1->Start = TDateTime::CurrentDateTime();
    profile1->Setup = TDateTime::CurrentDateTime();
    profile1->AllTime = Time;
    profile1->Time = Time;
    profile1->Create = etpC0func;
    profile1->UseEtaps = false;
    
    *profile2 = *profile1;
    *profile3 = *profile1;
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTimeProf1(int value)
{
    if(FTimeProf1 != value) {         
        FTimeProf1 = value;
        profile1->Time = value;
        Tick = 0;
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTimeProf2(int value)
{
    if(FTimeProf2 != value) {
        FTimeProf2 = value;
        profile2->Time = value;
        Tick = 0;
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTimeProf3(int value)
{
    if(FTimeProf3 != value) {
        FTimeProf3 = value;
        profile3->Time = value;
        Tick = 0;
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetColorProf1(TColor value)
{
    if(FColorProf1 != value) {
        FColorProf1 = value;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetColorProf2(TColor value)
{
    if(FColorProf2 != value) {
        FColorProf2 = value;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetColorProf3(TColor value)
{
    if(FColorProf3 != value) {
        FColorProf3 = value;
        Repaint();
    }
}

//---------------------------------------------------------------------------
bool TMultiGrapher::SetStartProfile(sProfileParam *)
{
    //TODO: Add your source code here
    return true;
}

//---------------------------------------------------------------------------
double* TMultiGrapher::LoadFromFile(AnsiString fn)
{
        profile1->LoadFromFile(fn);
        Repaint();
        return 0;
}

//---------------------------------------------------------------------------
TMultiGrapher::LoadConstValue(double v)
{
        profile1->InitData(v);
        Repaint();
        return 0;
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetStartConst(double value)
{
    if(FStartConst != value) {
        FStartConst = value;
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetKoeffCO(double value)
{
    if(FKoeffCO != value) {
        FKoeffCO = value;
        profile1->C0 = FKoeffCO;
        profile2->C0 = FKoeffCO;
        profile3->C0 = FKoeffCO;
        Repaint();
        
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetKoeff_D(double value)
{
    if(FKoeff_D != value) {
        FKoeff_D = value;
        profile1->D =  FKoeff_D;
        profile2->D =  FKoeff_D;
        profile3->D =  FKoeff_D;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetKoeff_B(double value)
{
    if(FKoeff_B != value) {
        FKoeff_B = value;
        profile1->B =  FKoeff_B;
        profile2->B =  FKoeff_B;
        profile3->B =  FKoeff_B;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetEndC(double value)
{
    if(FEndC != value) {
        FEndC = value;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetEndX(double value)
{
    if(FEndX != value) {
        FEndX = value;
        Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTp(double value)
{
    if(FTp != value) {
        FTp = value;
        profile3->Tp = value;
        Tick = 0;                     //Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTd(double value)
{
    if(FTd != value) {
        FTd = value;
        profile3->Td = value;
        Tick = 0;                     //Repaint();
    }
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetEDS(double value)
{
    if(FEDS != value) {
        FEDS = value;
        profile3->EDS = value;
        Tick = 0;                     //Repaint();
    }
}
//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTick(int value)
{
    if(FTick != value){
        // ��� ==1 ��������� 1 ��� � �� ����� �������� ��� ����� ��, ��, � ��� (���� ������� 2 � �����)
        if(value == 1)
        {
           FTick = 0;
           if (CalcProfThread == NULL)
               CalcProfThread = new TCalcProfThread(false,this);
           else
               if(CalcProfThread->Suspended)
               CalcProfThread->Resume();
           return;    
        }

        else
           FTick = value;
        profile1->CalculateProcData();
        *profile2 << *profile1;
        profile2->CalculateProcData();
        *profile3 << *profile2;
        W_Value = profile3->CalculateProcData();
        F_Value = profile3->Cg * 1000;
        int T = profile1->Time + profile2->Time + profile3->Time;
        int Sut = 3600*24;
//        int D = T/Sut;
        T = T % Sut;
        MarkerTime = EncodeTime(T/3600, (T % 3600)/60, T % 60, 0);
        Repaint();
    }
}
//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::FirstThreadPart()
{
        profile1->CalculateProcData();
        *profile2 << *profile1;
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::SecondThreadPart()
{
        profile2->CalculateProcData();
        *profile3 << *profile2;
}

//---------------------------------------------------------------------------
void __fastcall TMultiGrapher::ThirdThreadPart()
{
        W_Value = profile3->CalculateProcData();
        F_Value = profile3->Cg * 1000;
        int T = profile1->Time + profile2->Time + profile3->Time;
        int Sut = 3600*24;
//        int D = T/Sut;
        T = T % Sut;
        MarkerTime = EncodeTime(T/3600, (T % 3600)/60, T % 60, 0);
}
//        Repaint();

//---------------------------------------------------------------------------
namespace Multigrapher
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TMultiGrapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------


void TMultiGrapher::DrawLegende(TCanvas *cnv)
{
//    AnsiString STime = 
//FMarkerTime.FormatString("hh:nn");
    int tw = cnv->TextWidth(FLegentName1);
    int th = cnv->TextHeight(FLegentName1);
    int x = GraphRect.Right - 50;
    int y = GraphRect.Top +10;
    int gap = 10;
    int wd  = 20;

    cnv->Brush->Style = bsSolid;
//    x+=10;
    
    cnv->Brush->Color = clWhite;
    cnv->Rectangle (x- tw -10, y-5 , x+ wd + 20,y+ 2*(th + gap) + th + 5);

    
    cnv->TextOut (x - tw, y ,FLegentName1);
    cnv->TextOut (x - tw, y + th + gap,FLegentName2);
    cnv->TextOut (x - tw, y + 2*(th +gap),FLegentName3);
    x+=10;
    
    cnv->Brush->Color = FColorProf1;
    cnv->Rectangle (x, y , x + wd, y + th);
    cnv->Brush->Color = FColorProf2;
    cnv->Rectangle (x, y + th + gap, x + wd, y + 2*th + gap);
    cnv->Brush->Color = FColorProf3;
    cnv->Rectangle (x, y + 2*(th + gap), x + wd, y + 2*(th + gap) +th);
    

}

void __fastcall TMultiGrapher::SetTp1(double value)
{
    if(FTp1 != value) {
        FTp1 = value;
        profile1->Tp = value;
        Tick = 0;                     //Repaint();
    }
}

void __fastcall TMultiGrapher::SetTd1(double value)
{
    if(FTd1 != value) {
        FTd1 = value;
        profile1->Td = value;
        Tick = 0;                     //Repaint();
    }
}

void __fastcall TMultiGrapher::SetEDS1(double value)
{
    if(FEDS1 != value) {
        FEDS1 = value;
        profile1->EDS = value;
        Tick = 0;                     //Repaint();
    }
}

//------------------------------------------------------------------------------
void __fastcall TMultiGrapher::SetTp2(double value)
{
    if(FTp2 != value) {
        FTp2 = value;
        profile2->Tp = value;
        Tick = 0;                     //Repaint();
    }
}

void __fastcall TMultiGrapher::SetTd2(double value)
{
    if(FTd2 != value) {
        FTd2 = value;
        profile2->Td = value;
        Tick = 0;                     //Repaint();
    }
}

void __fastcall TMultiGrapher::SetEDS2(double value)
{
    if(FEDS2 != value) {
        FEDS2 = value;
        profile2->EDS = value;
        Tick = 0;
   }
}

void __fastcall TMultiGrapher::SetHMax(int value)
{
   FHMax = value;
   profile1->X_width = (double)value/100;
   profile2->X_width = (double)value/100;
   profile3->X_width = (double)value/100;
   Tick = 0;
}


void __fastcall TMultiGrapher::SetLegentName1(AnsiString value)
{
    if(FLegentName1 != value) {
        FLegentName1 = value;
        Repaint();
    }
}

void __fastcall TMultiGrapher::SetLegentName2(AnsiString value)
{
    if(FLegentName2 != value) {
        FLegentName2 = value;
        Repaint();
    }
}

void __fastcall TMultiGrapher::SetLegentName3(AnsiString value)
{
    if(FLegentName3 != value) {
        FLegentName3 = value;
        Repaint();
    }
}
