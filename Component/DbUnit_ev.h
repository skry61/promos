//---------------------------------------------------------------------------
// ������ ��������� ������ � ������ ������
//---------------------------------------------------------------------------
#ifndef DbUnitH
#define DbUnitH
//---------------------------------------------------------------------------

#include "TFDb_ev.h"
#include <ComCtrls.hpp>

typedef void __fastcall (__closure *TTestEventType)(System::TObject* Sender,
     int ID,
     bool &Allowed);
//---------------------------------------------------------------------------


class TxDataBase : public TFDataBase
{

protected:
    char * ErrOpen;
    TList* List;
    TDateTime FVisibleInt;               // �������� ���������
    TTestEventType FOnTestEventType;     // �������� ����� �������

public:		// User declarations
    __fastcall TxDataBase(Classes::TComponent* AOwner,TFDbDescriptor *tfdbDescript/*,TListView *aListView*/);
    __fastcall virtual ~TxDataBase();

    //-- ��������� ������ � ����� ������-----------------------------------------
    bool virtual __fastcall  OpenTable (void);
    bool virtual __fastcall  Store (void)=0;
    virtual void *__fastcall CreateNew(void)=0;
    int  virtual __fastcall  GetStructSize(void)=0; //-- ���������� ������ ���������
    void virtual __fastcall  AddToListComp(void)=0; //-- ��������� ���������� ����������� ���������� ������
    void         __fastcall  LoadAll(TDateTime Time);       //-- ��������� �������� ������������� ������, ���� � ���������� �� �����
    void virtual __fastcall  ClearAll(void)=0;      //-- ��������� �������� ������������� ������, ���� � ����������
    void virtual __fastcall  ClearList(void)=0;     //-- ��������� �������� ������������� ������
    bool virtual __fastcall  LoadList(TDateTime Time);
    bool virtual __fastcall  FillListComp(void)=0;
    bool         __fastcall  FindRecord (TDateTime Time);
    bool         __fastcall  RecoverTable (void);
    bool virtual __fastcall  Generate(int EventId,int Source,char* Comment)= 0;

    int  __fastcall  GetCount(void){if(List) return List->Count; else return 0;};
    __property TDateTime VisibleInterval  = { read=GetVisibleInterval, write=SetVisibleInterval };
//    FParamMenager
private:
        void __fastcall SetVisibleInterval(TDateTime value);
        TDateTime __fastcall GetVisibleInterval();
//    void         __fastcall  SetListView (TListView *aListView);
//    TListView *  __fastcall  GetListView () {return ListView;};
//    bool virtual __fastcall RecovertTable (void);

__published:	// IDE-managed Components
    __property TTestEventType OnTestEventType= {read=FOnTestEventType, write=FOnTestEventType};


};

//---------------------------------------------------------------------------
#endif



