//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#include <sysinit.hpp>
#pragma hdrstop

#include "ScrollerPanel.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TScrollerPanel *)
{
    new TScrollerPanel(NULL);
}
//---------------------------------------------------------------------------
__fastcall TScrollerPanel::TScrollerPanel(TComponent* Owner)
    : TCustomPanel(Owner)
{
    Width = 241;
    Height = 24;
    Caption = "";
    BevelInner = bvNone;
    BevelOuter = bvNone;
    BorderStyle = bsNone;
    BorderWidth = 1;
    FullRepaint = true;
    Zoom = 7;
    FSamples = 127;
    FVisibleWindow = 127;
    FStartWindow = 0;
    FPageSize = 360;

    FButtonWidth = 26;
    FButtonHeight = 24;
    OnResize = MyResize;

    TSpeedButton* Btn;
    //AnsiString ResName;
    int X = 0;
    for (int i=0;i<6;i++)
    {
        Btn = new TSpeedButton(this);
        Buttons[i] = Btn;
        Btn->Flat = true;
        Btn->Tag = i;
        Btn->OnClick = BtnHandler;

        Btn->Glyph->Handle = LoadBitmap(HInstance,MAKEINTRESOURCE(15000+i));
        Btn->Visible = true;
        Btn->Enabled = true;
        Btn->Height = FButtonHeight;
        Btn->Width = FButtonWidth;
        Btn->NumGlyphs = 2;
        Btn->Parent = this;
        X += 26;
    }

    Bar = new TScrollBar(this);
    Bar->Parent = this;
    Bar->Visible = true;
    Bar->Enabled = true;
    Bar->SetBounds (FButtonHeight+1,0,120,FButtonHeight);
    Bar->Position = 0;
    Bar->OnScroll = ScrollBarScroll;

}
void __fastcall TScrollerPanel::SetButtonHeight(int aHeight)
{
    FButtonHeight = aHeight;
    Resize();
}
void __fastcall TScrollerPanel::SetButtonWidth(int aWidth)
{
    FButtonWidth = aWidth;
    Resize();
}
void __fastcall TScrollerPanel::SetVisibleWindow(int aVisible)
{
    FVisibleWindow = aVisible;

    int max = FSamples - aVisible;
    Bar->Max = (max < 0)?0:max;
}
void __fastcall TScrollerPanel::SetStartWindow(int aStart)
{
    FStartWindow = aStart;
    Bar->Position = aStart;
}

void __fastcall TScrollerPanel::SetSamples(int aSamples)
{
    FSamples = aSamples;
    int max = aSamples - FVisibleWindow;
    Bar->Max = (max < 0)?0:max+4;
}

void __fastcall TScrollerPanel::SetGrapher(TGrapher* Gr)
{
   FGrapher = Gr;
   if(FGrapher)
      FGrapher->SetScroolPanel(this);
}
    
void __fastcall TScrollerPanel::BtnHandler(TObject* Sender)
{
    switch (((TSpeedButton *)Sender)->Tag)
    {
        case 0:     //-- ������ |<
            if (FPrevSamples) FPrevSamples(this);
            else return;
            break;
        case 1:     //-- ������ <
            StartWindow -= FPageSize;
            if (StartWindow < 0) StartWindow = 0;
            break;
        case 2:     //-- ������ >
            if (Samples > FPageSize)
            {
                StartWindow += FPageSize;
                if (StartWindow > (Samples - VisibleWindow))
                        StartWindow = Samples - VisibleWindow;
            }
            break;
        case 3:     //-- ������ >|
            if (FNextSamples) FNextSamples(this);
            else return;
            break;
        case 4:     //-- ������ +
            if (Zoom > 1) Zoom --;
            else Zoom = 1;
            break;
        case 5:     //-- ������ -
            if (Zoom < 13) Zoom ++;
            else Zoom = 13;
            break;
    }
    VisibleWindow = pow (2,Zoom);
    Bar->Position = StartWindow;
    UpdateGrapher();
}
/*void __fastcall TScrollerPanel::ScrollBarChange(TObject* Sender)
{
    StartWindow = Bar->Position;
    UpdateGrapher();
} */
void __fastcall TScrollerPanel::ScrollBarScroll(TObject *Sender,
      TScrollCode ScrollCode, int &ScrollPos)
{
    StartWindow = ScrollPos;
    UpdateGrapher();
}
void __fastcall TScrollerPanel::UpdateGrapher(void)
{
    if (FGrapher)   //-- ���� ���� ������
    {               //-- ��������� ��� ���������
        FGrapher->HMin = StartWindow;
        FGrapher->HMax = StartWindow + VisibleWindow;
        FGrapher->HStep = (VisibleWindow)/8;
        if (FGrapher->OnPaint) FGrapher->OnPaint(FGrapher);
        else Repaint();
    }
}

void __fastcall TScrollerPanel::MyResize(TObject* Sender)
{
    TSpeedButton* Btn;
    Btn = Buttons[0];
    Btn->SetBounds (0,0,FButtonWidth,FButtonHeight);

    Btn = Buttons[1];
    Btn->SetBounds (FButtonWidth,0,FButtonWidth,FButtonHeight);


    for (int i=2;i<6;i++)
    {
        Btn = Buttons[i];
        Btn->SetBounds (this->Width - (FButtonWidth*(6-i)),0,FButtonWidth,FButtonHeight);
    }
    Bar->SetBounds (FButtonWidth*2 + 1,0,this->Width - (FButtonWidth*6) - 2 ,FButtonHeight);
    //if (FOnOutResize) FOnOutResize (this);
    //OnKeyDown
}
//---------------------------------------------------------------------------
namespace Scrollerpanel
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TScrollerPanel)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------
