//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ScrollerPanel.h"
#include "TrendGrapher.h"
#pragma link "Grapher"
#pragma package(smart_init)
//---------------------------------------------------------------------------
static inline void ValidCtrCheck(TTrendGrapher *)
{
    new TTrendGrapher(NULL);
}
//---------------------------------------------------------------------------
__fastcall TTrendGrapher::TTrendGrapher(TComponent* Owner)
    : TGrapher(Owner)
{
    FTimeFormat = AnsiString ("hh:nn.ss");
    FDivisor = 1;
    FTimeStep = TDateTime (0,0,10,0);
    FTimeStart = TDateTime::CurrentDateTime();
}
//---------------------------------------------------------------------------
__fastcall TTrendGrapher::~TTrendGrapher()
{
    if (iData)  delete []iData;
}
//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::SetTimeFormat (AnsiString aFormat)
{
    FTimeFormat = aFormat;
    Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::SetDivisor (int iDivisor)
{
    if (iDivisor <= 0) iDivisor = 1;
    FDivisor = iDivisor;
    Repaint();
}

//---------------------------------------------------------------------------
// ������������� ����� ������ ����
void __fastcall TTrendGrapher::SetStartArmTime(TDateTime  aTimeStart)
{
    TimeStart = aTimeStart;
}

//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::SetTimeStep (TDateTime aStep)
{
    FTimeStep = aStep;
    Repaint();
}
//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::SetTimeStart (TDateTime aStart)
{
    FTimeStart = aStart;
    Repaint();
}
//---------------------------------------------------------------------------
int __fastcall TTrendGrapher::YFunconGrapher (int y_real)
{
    return GraphRect.Bottom - (int)(kY*(float)( ((float)y_real/(float)FDivisor) - (float)VMin));
}

//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::DrawFunctionData(TColor tColor,int* FuncData, int Idx, int Count)
{
    TCanvas *cnv = StartPaint();
    intCount = Count;
    PrepareCalc();
    cnv->Pen->Style  = psSolid;
    cnv->Pen->Width = FLineWidth;

    int X1,X2,Y1,Y2;
    bool AllUnknown = true;

    for (int i=0;i < Count;i++)
    if ( *(FuncData + Idx + i) != UNKNOWN_DATA )
    {
        AllUnknown = false;
        break;
    }
    if (AllUnknown)
    {
        cnv->Brush->Color = clGray;
        cnv->Brush->Style = bsSolid;
        cnv->FillRect(GraphRect);
        DrawLabel (cnv, GraphRect);
    }
    else
    {
        //-- ��������� ����� ������� -------------------
        DrawOneFunction(cnv,0,Count,tColor,FuncData,Idx);
        DrawLabel (cnv, GraphRect);
    }
    FinishPaint();

}

void __fastcall TTrendGrapher::DrawOneFunction(TCanvas *cnv,int Start,int Count,TColor tColor, int* FuncData, int Idx)
{
    int X1,X2,Y1,Y2;

        for (int i=0;i<Count-1;i++)
        {
            X1 = XonGrapher (FHMin+i+Start);
            X2 = XonGrapher (FHMin+i+1+Start);
            if ( *(FuncData + Idx + i) != UNKNOWN_DATA &&
                 *(FuncData + Idx + i + 1) != UNKNOWN_DATA )
            {
                cnv->Pen->Color = tColor;
                Y1 = YFunconGrapher ( *(FuncData + Idx + i) /* FDivisor*/);
                Y2 = YFunconGrapher ( *(FuncData + Idx + i + 1) /*FDivisor*/);
                cnv->MoveTo (X1,Y1);
                cnv->LineTo (X2,Y2);
            }
            else
            {
                TRect NoRect;
                NoRect.Top = GraphRect.Top + 1;
                NoRect.Bottom = GraphRect.Bottom - 1;
                NoRect.Left = X1;
                NoRect.Right = X2;

                cnv->Brush->Color = clGray;
                cnv->Brush->Style = bsSolid;
                cnv->FillRect(NoRect);
            }
        }
}

//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::DrawLabel(TCanvas *cnv, TRect GraphRect1)
{
    int X1,Y1;
    double FVStep_,FHMax_,FHMin_;
    int FValuePrecision_,FValueDigits_;

    TGrapher::PrepareCalc();
    int divx = (FHMax-FHMin)/FHStep;    // ��� ������ �����.
    int divy = (FVMax-FVMin)/FVStep;
    if(divy <= 1){
       FVStep_ = (double)(FVMax-FVMin)/5;
       FValuePrecision_= FValuePrecision -1;
       FValueDigits_   = FValueDigits+1;
     } else if(divy <= 2){
       FVStep_ = (double)(FVMax-FVMin)/10;   
       FValuePrecision_= FValuePrecision -1;
       FValueDigits_   = FValueDigits+1;
     } else{
       FVStep_ = FVStep;
       FValuePrecision_= FValuePrecision;
       FValueDigits_   = FValueDigits;

     }

    divy = (FVMax-FVMin)/FVStep_+1;

    bool osy = (FHMin <= 0 && FHMax >=0)?true:false;
    bool osx = (FVMin <= 0 && FVMax >=0)?true:false;

    cnv->Pen->Width = 1;//FLineWidth;


    if(FEnable_Adjusting){
        cnv->Pen->Color = FAdjustColor;
        Y1 = YonGrapher (FAdjusting_Value);
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);

        /*Windows::*/TPoint points[3];
        int h = cnv->TextHeight("A")+1;
        points[0] = Point(GraphRect1.Right + 1,Y1);
        points[1] = Point(GraphRect1.Right + h, Y1+h/2);
        points[2] = Point(GraphRect1.Right + h, Y1-h/2);
        cnv->Brush->Color = FAdjustColor;//White;

        cnv->Polygon(points, 2);
        DrawSText(cnv, clWhite, GraphRect1.Right + 1 + h, Y1-h/2+1 ,AnsiString("���"));
    }

    cnv->Pen->Style  = psDot;
    cnv->Brush->Style = bsClear;
    cnv->Pen->Color = clBlack;//FLineColor;

    TDateTime dtTime;
    Extended Value;
    for (int i = 0; i<divx; i++)
    {
        X1 = XonGrapher (FHMin + FHStep*i);
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);

        dtTime = FTimeStart;
        dtTime += TDateTime( ((double)FTimeStep)* (double)(FHMin + i*FHStep) );
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,dtTime.FormatString(FTimeFormat));
        //-- � ������ � ����� ������� ������� ����
        if (!i || i == (divx-1)) cnv->TextOut (X1-4,GraphRect1.Bottom+24,dtTime.FormatString("dd.mm.yyyy �"));
    }

    for (int i = 0; i<divy; i++)
    {
        Y1 = YonGrapher (VMin + FVStep_*i);
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        Value = (FVMin + FVStep_*i);
        cnv->TextOut (ClientRect.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed/*ValueFormat*/,FValuePrecision_,FValueDigits_));
    }
    cnv->Pen->Style  = psSolid;

    if (!osy)   //-- ������� ��� ��� - �������� ��� Y
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Left,GraphRect.Top);
        DrawArrow(cnv,eToTop,GraphRect.Left,GraphRect.Top);
    }
    else        //-- ������� ��� ���� - �������� ��� Y
    {
        X1 = XonGrapher (0);
        cnv->MoveTo (X1,GraphRect1.Bottom);
        cnv->LineTo (X1,GraphRect1.Top);
        DrawArrow(cnv,eToTop,X1,GraphRect.Top);
        dtTime = TDateTime(FTimeStart);
        cnv->TextOut (X1-4,GraphRect1.Bottom+10,dtTime.FormatString(FTimeFormat));
    }
    if (!osx)   //-- ������� ��� ��� - �������� ��� X
    {
        cnv->MoveTo (GraphRect.Left,GraphRect.Bottom);
        cnv->LineTo (GraphRect.Right,GraphRect.Bottom);
        DrawArrow(cnv,eToRight,GraphRect.Right,GraphRect.Bottom);
    }
    else
    {
        Y1 = YonGrapher (0);
        cnv->MoveTo (GraphRect1.Left,Y1);
        cnv->LineTo (GraphRect1.Right,Y1);
        Value = (0);
        cnv->TextOut (ClientRect.Left + 5,Y1-5,
            AnsiString::FloatToStrF (Value,AnsiString::sffFixed/*ValueFormat*/,FValuePrecision_,FValueDigits_));
        DrawArrow(cnv,eToRight,GraphRect.Right,Y1);
    }
    //-- ���� ������ ������� ��������� ������� � ������ ����
    if (FMarker && IsMarker && MarkerLinesCount) DrawMarker(cnv);   //-- SPACK
}

//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::Paint(void)
{
    int Count,SamplesCount;
    if (iData) {
       delete []iData;
       iData = NULL;
    }   
//    iFuncData = MakeFuncData(i,Divisor,Count);
    TDateTime TimeBegin = FTimeStart + TDateTime(((double)FTimeStep)* (double)FHMin);
    TDateTime TimeEnd   = FTimeStart + TDateTime(((double)FTimeStep)* (double)FHMax);
    
    if(FOnRequestData) 
    {
       FOnRequestData(this,(int *)&iData,&Count,TimeBegin,TimeEnd,&SamplesCount);
       CorrectCount = Count;
//       SP->Samples =  LastTime - TimeBegin   SamplesCount;                              
       //-----------------------------------------------------------------
       //-- ��������� ����� ������� �� �������
       if(iData)
          DrawFunctionData(LineColor,iData,0,Count);
    } else MyPaint((AlterPaint)?AlterCanvas:Canvas,ClientRect);      
    if (FOnPaint) FOnPaint(this);
}

//---------------------------------------------------------------------------
void __fastcall TTrendGrapher::SetAdjusting_Value(double value)
{
    if(FAdjusting_Value != value) FAdjusting_Value = value;
}
//---------------------------------------------------------------------------
TDateTime __fastcall  TTrendGrapher::GetVisioTimeStart()
{
    return FTimeStart + TDateTime(((double)FTimeStep)* (double)(FHMin));
}
//---------------------------------------------------------------------------
TDateTime __fastcall  TTrendGrapher::GetVisioTimeEnd()
{
    return FTimeStart + TDateTime(((double)FTimeStep)* (double)(FHMax));
}
//---------------------------------------------------------------------------
namespace Trendgrapher
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TTrendGrapher)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------




