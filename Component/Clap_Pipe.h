//---------------------------------------------------------------------------

#ifndef Clap_PipeH
#define Clap_PipeH
#include <vcl.h>
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ToolWin.hpp>
#include <Buttons.hpp>
#include "RXSW.hpp"


enum TRegims{
   rPULT=0,
   rHANDLE,
   rAUTO
};

enum {
   sToNone,
   sToLess,
   sToMore
};

//---------------------------------------------------------------------------

class TClap_Pipe  {
public:

    static         TSpeedButton   *More_InControl;
    static         TSpeedButton   *Less_InControl;
    static         TLabel         *ReginHandleInControl;
    static         TLabel         *ReginAutoInControl;
    static         TPanel         *PanelControl;
    static         TPanel         *PanelControlParamName;   // ��� ��������� ����������
                                                           // ��� ��� ���������� ���������
    static         TrxSwitch_     *rxSwitch_REGIM;
    static         TEdit          *Edit_EDS_Value;
private:	// User declarations
                   TImage         *More_InMain;
                   TImage         *Less_InMain;
                   TLabel         *ReginInMain;
    static         TClap_Pipe     *ActiveClap_Pipe;        // ������� ��������


private:	// User declarations
    bool           FStatOn,FStatOff; //-- ��������� �������� �� ���������/����������
    int            FClapanStat;
    float          V_Tek_Old;
    float          A_Old;
    float          Sum;
    float          FKp,FKi,FKd;
    bool           FFl_Reg;          //-- ������ ������������
    int            Index;
    short          FRegim;
    bool           FEnable;
    WORD           FEDS;             // -- �������� ���
    AnsiString     Caption;
    bool           IsOVEN;

//    TFlameUnit     *FlameUnit;
//    static unsigned short ControlBits;

    void           __fastcall SetRegim(short value);
    short          __fastcall GetRegim();
    void           __fastcall SetEnable(bool value);
    bool           __fastcall GetEnable();
    void           __fastcall SetEDS(unsigned short value);
    unsigned short __fastcall GetEDS();
    bool Clap_PipeIsActive();


public:		// User declarations
    void __fastcall RegimStory(short value);
    static unsigned short ControlBits;
    //����� ������� ��� �����������
    static unsigned short  FRegimCommon;
    //����� ���������� �������� ������� �����  ��� �����������
    static unsigned short  GasProbeClapanStatus;
    int AdjustingParam;
    bool           Is_Current;
//    void __fastcall SetMessage(bool aAlarm, AnsiString aMsg);

    __fastcall TClap_Pipe( float aKp, float aKi, float aKd,bool aFl_Reg,
                                      int aIndex,
                                      bool aIsOVEN,
                                      TLabel *Regim_Value,
                                      TImage *aMore_InMain,
                                      TImage *aLess_InMain
                                      );

    __fastcall TClap_Pipe(){};

    void __fastcall SetUnitType(bool aIsOVEN);
    int __fastcall GetStat();
    void __fastcall SetStat(int aSt);
    void __fastcall EDS_Init(WORD aEDS);

    short  __fastcall GetControl(){return ControlBits;};

    int  __fastcall Regulator(float aV_Zad, float V_Tek);
    void __fastcall SetAbove();
    void __fastcall SetBelow();
    void __fastcall SetValueToController();
    void __fastcall ShowRegul(short aReg);
    void __fastcall SetNone();
    void __fastcall ActivateControl();
    void __fastcall  SetIndex(int i) { Index = i;}
    void __fastcall  ProbeClapan(bool);

//    __fastcall ~TClap_Pipe();

    __property int Neitral_On_Off = {read=GetStat, write=SetStat};
    __property short ClpanControl = {read=GetControl};
    __property short Regim  = { read=GetRegim, write=SetRegim};
    __property bool Enable  = { read=GetEnable, write=SetEnable};
    __property unsigned short EDS  = { read=GetEDS, write=SetEDS };
};

#endif
