//---------------------------------------------------------------------------
/*
��� �������:
1)  ������� ������������ FlameList->IsProfile   //���� ���� ���� ���� ���� c ��������

2)  ������� ������������ FlameList->IsOvenEmpty)    //����  ���� ���� ���� ��� �������
3)

*/
#include <basepch.h>
#include <stdio.h>

#pragma hdrstop
#include "Link.h"
#include "Link_ALT.h"
#include "FlameList.h"
#pragma package(smart_init)

//-- ���������� ��������� ���������� -----------------------------------
extern HINSTANCE hDLL;

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TFlameList *)
{
    new TFlameList(NULL);
}
//---------------------------------------------------------------------------
__fastcall TFlameList::TFlameList(TComponent* Owner)
    : TTimer(Owner)
{

  FUnits.Clear();               // �������� ���������
  FIsProfile = false;           // ������� ���� ����� ���
//  FCurentFlameUnit = NULL;
  FCurFlameUnit  = NULL;
  FOnReqestData  = NULL;
  FCurrentUnitIndex = 0;
  AccessMode = eUser;
//  bcw = 0;
  FBurnSensorBits = 0;
  FEnterInTimer = false;
  FFlameUnitCount = 0;
  FCountOven = 0;                                 // ���������� �����    
  FCountEndo = 0;                                 // ���������� ���������������
  FCountProfil = 0;                               // ���� ������ �� �������  - �� +1, ���� ������� , �� -1
  FCountTermProfil = 0;                           // ���� ������ �� ������� �� ���� ��� - �� +1, ���� ������� , �� -1
  FIsProfile = false;                                // ���� �� ������������ ������� ���� � ����� ����


//  FParamMenager = new TParamMen(Owner);
//  FParamMenager->Parent = (TWinControl*)Owner;
//  FParamMenager->Name = "ParamMenager";
}

//-- ��������/������ ���� ��������� ������� ��������
BYTE __fastcall TFlameList::GetBurnSensorBits()
{
  return FBurnSensorBits;

}
void __fastcall TFlameList::SetBurnSensorBits(BYTE Abcw)
{
  FBurnSensorBits = Abcw;
  SetBCW();

}

//---------------------------------------------------------------------------
__fastcall TFlameList::~TFlameList(void)
{
  for (int i = 0; i < MAX_OBJECT /*MAXUNIT*/; i++)
       if (Assigned(FlameUnits[i]))
            FlameUnits[i]->ClearReference();
// ����� � �� ���� ������ "delete FParamMenager" �� ��� ���� � Owner
//   delete FParamMenager;

   delete pTermoDat;

};

//---------------------------------------------------------------------------

void __fastcall TFlameList::SetUnits(TUnitTypes Value)
{
  if (FUnits != Value)
  {
    FUnits = Value;
  }
  FEnterInTimer = false;

}
//---------------------------------------------------------------------------
void __fastcall TFlameList::DeleteReference(int Index)
{
  if (Assigned(FlameUnits[Index]))
  {
    TUnitTypes UnitT;
    UnitT.Clear();
    UnitT << FlameUnits[Index]->Type;
    FFlameUnitCount--;
    if((FlameUnits[Index]->Type == uOVEN)||(FlameUnits[Index]->Type == uOVEN_Lite)){
         if(!--FCountOven)
            FUnits = FUnits - UnitT;

    }else if(FlameUnits[Index]->Type == uENDO){
         if(!--FCountEndo)
            FUnits = FUnits - UnitT;
    }else
         FUnits = FUnits - UnitT;
    TFlameUnit* fu = FlameUnits[Index];
    FlameUnits[Index] = NULL;
    if(fu == FCurFlameUnit){
       CurentFlameUnit = NULL;
       for (int i = 0; i < MAX_OBJECT /*MAXUNIT*/; i++)
          if (FlameUnits[i]){
            CurentFlameUnit = FlameUnits[i];
            break;
          }  
    }

  }
}

//---------------------------------------------------------------------------
// ������ ������ �������� ����������
void __fastcall TFlameList::SetParamMenager(TParamMen *aParamMenager)
{
      FParamMenager = aParamMenager;
      ForEach(pSetParMen);
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::SetUnit(int Index, TFlameUnit* Value)
{
  if (FlameUnits[Index] != Value)
      // ���� ��� ���
      DeleteReference(Index);
      FlameUnits[Index] = Value;
      if  (Assigned(Value)){
          /*FUnits = */FUnits << Value->Type;
          Value->SetReference(this,Index);
          // ������� ���������� �����
          if((Value->Type == uOVEN)||(Value->Type == uOVEN_Lite))
             FCountOven++;
          else if(Value->Type == uENDO)
             FCountEndo++;
          // ������� ���������� ���������   
          FFlameUnitCount++;
          FCurFlameUnit  = Value;
          FCurrentUnitIndex = Index;
          
      // ���� �������    
      }else{
      }    
}
//---------------------------------------------------------------------------

TFlameUnit* __fastcall TFlameList::GetUnit(int Index)
{
   return FlameUnits[Index];
}
//---------------------------------------------------------------------------
namespace Flamelist
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TFlameList)};
         RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

bool __fastcall TFlameList::GetIsOven()
{
    //TODO: Add your source code here
    return (FUnits.Contains(uOVEN)||FUnits.Contains(uOVEN_Lite));
}


//---------------------------------------------------------------------------
 //����  ���� ���� ���� ��� ������� 
bool __fastcall TFlameList::GetIsOvenEmpty(void)
{
      return FCountOven > FCountProfil;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameList::GetIsProfile(void)
{
      return FCountProfil > 0;
}

//---------------------------------------------------------------------------
//���� ���� ���� ���� ���� c ����������� ����������
bool __fastcall TFlameList::GetIsTermProfile(void)
{
      return FCountTermProfil > 0;
}

//---------------------------------------------------------------------------
// ���������� ������� � ����
bool __fastcall TFlameList::RecognizeProfile(sProfileParam *p)
{
     char cMsg[180];

     FCurFlameUnit->RecognizeProfile(p);

//     sprintf (cMsg,"[%s %d]",p->Name,p->Num);
     //-- ������� "����� ������������" ��������� � ���� �������
//     GenerateEvent(eStartProfile,eProgram,cMsg);

     if(FCountProfil < FCountOven)
     {
        FCountProfil++;
        if(p->UseEtaps)
            FCountTermProfil++;
     }
     return 0;
}

// ����� ������� � ����
bool __fastcall TFlameList::ReleaseProfile(sProfileParam *p)
{
//      FCurFlameUnit->RecognizeProfile(p);
      if(FCountProfil > 0)
      {
         FCountProfil--;
         if(p->UseEtaps){
            if(FCountTermProfil)
               FCountTermProfil--;
            p->UseEtaps = false;
         }   
      }
      return 0;

}

//---------------------------------------------------------------------------
void __fastcall  TFlameList::SetComboBox(TComboBox *aComboBox, int Type)
{
   if(Type == pSetComboBoxAll || Type == pSetComboBoxOven)
      ForEach((eProcType)Type,aComboBox);
}

//---------------------------------------------------------------------------
bool __fastcall  TFlameList::SetProfileComboBox(TComboBox *aComboBox)
{
      return ForEach(pSetComboBoxProfile,aComboBox);
}

//void __fastcall  TFlameList::SetExLinks(sArmParam *aArmParam, sControlParam   *aCtrl_param)
void __fastcall  TFlameList::SetExLinks(sArmParam *aArmParam, sControlParam *aCtrl_param, sOptions *aOptions)

{
    ArmParam   = aArmParam;
    Ctrl_param = aCtrl_param;
    Options    = aOptions;
//    Magazining = aOptions->Magazining;          //-- ������ ���������� ����������
//    DB_Updating= aOptions->DB_Updating;         //-- ������ ���������� ����

//    ComboBox = aComboBox;
    ForEach(pSetArmParam, aArmParam);
    ForEach(pSetCtrlParam, aCtrl_param);
    ForEach(pSetLimits, aOptions->All_Limits);
//    ForEach(pSetClap_Pipe,Clap_Pipe);
};


//---------------------------------------------------------------------------
// ������� �������
void __fastcall TFlameList::Timer(void)
{

    if(!FEnterInTimer){                          // �������: ����� � ������ - ��������� ��������� ����
        FEnterInTimer = true;
        TTimer::Timer();
        //==   �������� ����
        //-- ���������� ������ ����������
        if(Options){
           iDateLastDB_Updating = Options->DB_Updating;    //!!
                                                   
           //-- ���������� ������ ����������
           iDateLastMagazining  = Options->Magazining;
        }

        if (FOnReqestData) FOnReqestData(this);
        FEnterInTimer = false;
    }
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::SetCurentFlameUnit(TFlameUnit* value)
{
    //TODO: Add your source code here
    FCurFlameUnit = value;
    if(value != NULL){
        FCurrentUnitIndex = FCurFlameUnit->IndexInFlameList;
        if(TClap_Pipe::PanelControl)
            TClap_Pipe::PanelControl->Visible = FCurFlameUnit->Clap_Pipe->Enable;
    }else
        FCurrentUnitIndex = -1;
}

//---------------------------------------------------------------------------
TFlameUnit* __fastcall TFlameList::GetCurentFlameUnit()
{
    //TODO: Add your source code here
    return FCurFlameUnit;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameList::GenerateEvent(int EventId,int Source,char* Comment)
{
   if(FEventMeneger)
      FEventMeneger->Generate(EventId,Source,Comment);
   return true;
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::ChangeType(int Index, Units_Type Old)
{
  if (Assigned(FlameUnits[Index])) 
  {
    TUnitTypes UnitT;
    UnitT.Clear();
    UnitT << Old;
    if((Old == uOVEN) || (Old == uOVEN_Lite)){
         /*if(!*/--FCountOven;/*)*/
//            FUnits = FUnits - UnitT;
             
    }else if(Old == uENDO){
         /*if(!*/--FCountEndo;/*)*/
//            FUnits = FUnits - UnitT;
    }     
    FUnits = FUnits - UnitT;

    Units_Type New = FlameUnits[Index]->Type;
    /*FUnits = */FUnits << FlameUnits[Index]->Type;
    // ������� ���������� �����
//    if(New == uOVEN)
    if((New == uOVEN) || (New == uOVEN_Lite))
       FCountOven++;
    else if(New == uENDO)
       FCountEndo++;
  }
}

//---------------------------------------------------------------------------
int  __fastcall TFlameList::GetParamCount()
{
   if(FParamMenager != NULL)
      return FParamMenager->GetCount();
   else
      return 0;
}

//---------------------------------------------------------------------------
//-- ������ ������ ���������
TDateTime __fastcall TFlameList::GetStartArmTime()
{
    if (ParamCount >1)
    {
        return FParamMenager->GetFirstTime();
    }
    else
        return FStartArmTime;
}

//---------------------------------------------------------------------------
bool __fastcall TFlameList::ForEach(TProcType Type, void *AInfo)
{
    byte TmpAutoControl = 0;
    bool Result = false;
    sControlParam   *sCP;
    AnsiString SNKey;
    AnsiString aSTR;
    double *p = (double*)AInfo;

    switch (Type){
    case pSetComboBoxProfile:     // ��������� ComboBox ������� ������ ����� c ��������
    case pSetComboBoxAll:         // ��������� ComboBox ������� ���� ���������
    case pSetComboBoxOven:        // ��������� ComboBox ������� ������ �����
        ((TComboBox *)AInfo)->Clear();
    };
    for (int i = 0; i< MAX_OBJECT/*MAXUNIT*/;i++){
       TFlameUnit *FlameUnit = FlameUnits[i];

       if(FlameUnit != NULL) {          // ���� ������������
          int j = FlameUnit->IndexInFlameList;          //������
          switch (Type){
          case pShowStat:               // �������� ��������� ���� : 2-������� �����; 1 - ���� ������������; 0 - ���� ��������
//              if(FlameUnit->IsOVEN) ;    //
//                 FlameUnit->Status =  2;//FlameUnit->Carburizer_Oven_Status;
//                 *(int*)AInfo;//is_profile[Index]?profile[Index].Done?2:1:0;
              break;
          case pShowAlarm:
              FlameUnit->TriggerAlarmPhasa();
              break;
          case pSetValue:
              FlameUnit->SetValueAll();
              break;
          case pSetComboBoxAll:         // ��������� ComboBox ������� ���� ���������
              ((TComboBox *)AInfo)->Items->AddObject(FlameUnit->UCaption, (TObject *)FlameUnit);
              break;

          case pGetFirstProf:           // ����� ������ �������
              if(FlameUnit->Is_profile) {
                  ((TFlameUnit *)AInfo) = FlameUnit;
                  return true;
              }
              break;
          case pSetComboBoxProfile:     // ��������� ComboBox ������� ������ ����� c ��������
              if(FlameUnit->Is_profile) //

          case pSetComboBoxOven:        // ��������� ComboBox ������� ������ �����
              if(FlameUnit->IsOVEN)     //
                   ((TComboBox *)AInfo)->Items->AddObject(FlameUnit->UCaption, (TObject *)FlameUnit);
                    Result = true;
              break;

          case pSetCtrlParam:           // ���������� ��������� �������� ��� ����������
              sCP = (sControlParam *)AInfo;
              FlameUnit->SetCtrls(&sCP->CO[j],
                                  &sCP->Tp[j],
                                  &sCP->Td[j],
                                  &sCP->EDS[j],
                                  &sCP->TpAutoCtrl[j],
                                  &sCP->TdAutoCtrl[j],
                                  &sCP->EDSAutoCtrl[j],            //-- ������� ��������� ���
                                  &sCP->CgAutoCtrl[j],             //-- ������� ��������� Cg
                                  &sCP->BurnAutoCtrl[j]
                                  );
              break;
              
          case pUpLoadCtrlParToPLC:
              TmpAutoControl = (TmpAutoControl >> 4 ) | FlameUnit->UpLoadCtrls();
              // ������ � ���������� � 3350-3370 ��� ������������                   
              if (j&1)       // ���� �� ����       
              {
                  if (!WriteAutoControlDll(&TmpAutoControl,j))
                  {   //-- ���������� ������ �����
                      //-- ����������� ������������
                      if (AccessMode == eDebugger)
                          SetMessage (true,"������ ������ � ������������");
                      //-- � ��������� ��� ������� � ���� �������
                      if(EventMeneger)
                          EventMeneger -> Generate(eLostConnect,eController1,NULL);
                      return  false;
                  }
                  Result = true;
              }      
              break;
          
/*          case pSetClap_Pipe:
              FlameUnit->SetClap_Pipe(&((TClap_Pipe *)AInfo)[j]);
              break;
*/
          case pSetArmParam:             // ���������� ��������� ��������
              FlameUnit-> SetArmParam(&((sArmParam *)AInfo)[j]);
              break;

          case pSetLimits:               // ���������� ��������� �������� ���������
              FlameUnit->SetLimits(&((sLimits *)AInfo)[j]);
              break;

          case pSetIDValue:              // ������ �������� �������������� �����
              SNKey = cFlameCtrlRegKey + AnsiString("\\ID_Value");
              if (!GetRegParam(SNKey,"ID"+IntToStr(i),aSTR))
              {
                  aSTR = FlameUnit->ObjectId;
                  PutRegParam(SNKey,"ID"+IntToStr(i),aSTR);
 //                 break;
              }else
                  FlameUnit->ObjectId = aSTR.ToInt();
              // ������ ��� �����
              SNKey = cFlameCtrlRegKey + AnsiString("\\SourceName");
              if (!GetRegParam(SNKey,"SN"+IntToStr(FlameUnit->ObjectId),aSTR))
              {
                  aSTR = FlameUnit->UCaption;
                  PutRegParam(SNKey,"SN"+IntToStr(FlameUnit->ObjectId),aSTR);
 //                 break;
              }else
                  FlameUnit->UCaption = aSTR;
              break;

          case pExecuteProf:            // ������ ��������
              if((FlameUnit->IsOVEN)&&(FlameUnit->Is_profile)&&(FlameUnit->Status != sstReady))     //
                  Result |= FlameUnit->ExecuteProfile(&ccw[j]);
              break;

          case pTestSameProf:           // �������� �� ������� ���� �� ������� � ����� �� �����
              if(/*FlameUnit->IsOVEN && */FlameUnit->Is_profile)
                  Result |= FlameUnit->TestSameProfile((sProfileParam *)AInfo);
              break;

          case pClearParamArray:
              FlameUnit->GenerateParam();
              break;

          case pInitCtrlParam:          // ���������������� ��������� ��������
              Result |= FlameUnit->InitCtrlParam();
              break;

          case pPersInputInfo:          // ������ �������� ����������
              Result |= FlameUnit->PersInputInfo(pTermoDat);
              break;
          case pTestAltEnable:          // ��������� �� ������ ����� ���������� � ����������
              if(FlameUnit->TpAutoCtrl != acALT_Unit)
                  pTermoDat->Disable(j*3);
              else
                  if(Options->FlSetUstTermodat) //-- ���� - ��� ������ ������ ������� � ��������
                       pTermoDat->SetUst(FlameUnit->TpReg,j*3);
//              Result |=
              break;
          case pMakeTermalProg:         // ���������� ���� ���������� ���������
              if(FlameUnit->Is_profile)              
                  //-- ���� ���� ���� ����������
                  if(FlameUnit->Profile->UsePrepare)
                  {
                       FlameUnit->MakePrepareProg((sTermAssignment *)&term[j*2]);
                  }
                  //-- ���� ����������� ��������� �������
                  else if (FlameUnit->Profile->UseEtaps)
                  {
                       FlameUnit->MakeTermalProg((sTermAssignment *)&term[j*2],pTermoDat);
                  //-- ���� ����������� ��������� ���
                  }else{
                       term[j*2]   = Options->All_Limits[j].EReg;//Clap_Pipe[j].EDS;//��� ��
                       term[j*2+1] = Options->All_Limits[j].TpReg;
                  }
              break;

          case pAcceptPSW:              // ���������� �������� � ��������
                  FlameUnit->psw = psw[j];
                  FlameUnit->PultStatus = (psw[MAX_OBJECT]>>j) & 1;
              break;
          case pSetParMen:              // ������ ������ �������� ����������
                  FlameUnit->SetParMen(FParamMenager);
              break;
          case pLoadMultiData:          // ��������� ������ ��� �������������
                 FlameUnit->LoadMultiData(&p);
              break;
          }//case
       }//if !=NULL
    } //for
    return Result;
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::ShowProfileStatus(void)
{
      ForEach(pShowStat);
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::ShowAlarm(void)
{
      ForEach(pShowAlarm);
}

//-- �������� ��������� -----------------------------------------------------
void __fastcall TFlameList::RepareValue()
{
      ForEach(pSetValue);  // ������ �������� ���������
}

//-- ���������� �������� � ���a�����  ----------------------------------------
void __fastcall TFlameList::AcceptPSW()
{
      ForEach(pAcceptPSW);
      if (!Fl_NO_1Wire){
          //-- ��������� ��������� 1 Wire
          if(!_1WireDetected())
          {
                Fl_NO_1Wire = true;
                MessageBox (GetActiveWindow(),
                "���������� ����� 1 Wire �� ����������\n"
                "�������: ���� �����������\n"
                "������ ����������: ��������� � �������� ����������.",
                "��������� ������!",MB_OK|MB_ICONERROR);
                Fl_NO_1Wire = false;

            }else
            if(!TERMODATsDetected())
            {
                Fl_NO_1Wire = true;
                MessageBox (GetActiveWindow(),
                "���� �� ��������� �������� �� ����������\n"
                "�������: ���� �����������\n"
                "������ ����������: ��������� � �������� ����������.",
                "��������� ������!",MB_OK|MB_ICONERROR);
                Fl_NO_1Wire = false;
                
            }
          }
}

//---------------------------------------------------------------------------
void __fastcall TFlameList::SetCurrentUnitIndex(int value)
{
   if (FFlameUnitCount > value)
      if(FCurrentUnitIndex != value) {
          FCurrentUnitIndex = value;
          FCurFlameUnit = FlameUnits[value];
      }
}

//---------------------------------------------------------------------------
int __fastcall TFlameList::GetCurrentUnitIndex()
{
    return FCurrentUnitIndex;
}

//---------------------------------------------------------------------------
//�������� �������������� �� �������
bool __fastcall TFlameList::LoadFromRegistryID(){
    AnsiString aCount;
    AnsiString aSTR;
    int Count;
//    int iCount;                            
    AnsiString SNKey = cFlameCtrlRegKey + AnsiString("\\ID_Value");
        //-- ������ �� ������� �������� ���������� �����
    if (!GetRegParam(SNKey,"Count",aCount))
    {
        aCount = IntToStr(FFlameUnitCount);
        Count = FFlameUnitCount;
        PutRegParam(SNKey,"Count",aCount);
    }
    else Count = aCount.ToInt();
    //���� � ������� ������ ��������������
    if (FFlameUnitCount < Count) 
        Count = FFlameUnitCount;
    ForEach(pSetIDValue);  // ������ �������� �������������� �����
    return true;
}
//------------------------------------------------------------------------------
// �������� ������� �������� �������
sProfileParam* __fastcall TFlameList::GetProfile()
{

    if(FCurFlameUnit)
       return FCurFlameUnit->Profile;
    else   
       return NULL;
}

//------------------------------------------------------------------------------
// �������� ������ �������
sProfileParam* __fastcall TFlameList::GetFirstProfile()
{
    sProfileParam* prof;
    bool Result = ForEach(pGetFirstProf,&prof);  // ������ ��������
    if(Result)
       return prof;
    else
       return NULL;
}

//------------------------------------------------------------------------------
// ��������� � �������� �������
double* __fastcall TFlameList::GetParam()
{
    if(FCurFlameUnit)
       return FCurFlameUnit->Param;
    else
       return NULL;
}

//------------------------------------------------------------------------------
// ������ ����� ���������� �������� �������� � ����������
bool __fastcall TFlameList::SetBCW()
{
    //-- ��������� ������ ����� ���������� � ����������
//    bcw |= FBurnSensorBits;
//    if (!WriteBcwDll(&bcw))
    if (!WriteBcwDll(&FBurnSensorBits))
    {   //-- ���������� ������ �����
        //-- ����������� ������������
        if (AccessMode == eDebugger)
            SetMessage (true,"������ ������ � ������������");
        //-- � ��������� ��� ������� � ���� �������
        if(EventMeneger)
            EventMeneger -> Generate(eLostConnect,eController1,NULL);
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFlameList::SetCCW()
{
    //-- ��������� ������ ����� ���������� � ����������
    if (!WriteCcwDll(ccw))
    {   //-- ���������� ������ �����
        //-- ����������� ������������
        //-- � ��������� ��� ������� � ���� �������
        if(EventMeneger)
            EventMeneger -> Generate(eLostConnect,eController1,NULL);
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFlameList::GetPSW()               // ������ ����� ��������� ����������� ��������� � ���������
{
    //-- ��������� ������ ����� ���������� � ����������
    if (!ExchangeDllPSW (GetPSW_Addr(),GetPSW_Length()))
    {   //-- ���������� ������ �����
        //-- ����������� ������������
        //-- � ��������� ��� ������� � ���� �������
        if(EventMeneger)
            EventMeneger -> Generate(eLostConnect,eController1,NULL);
        return false;
    }
    AcceptPSW();            // ���������� �������� � ��������
    return true;
}

//------------------------------------------------------------------------------
bool __fastcall TFlameList::ExecuteProfiles()
{
    bool Result = false;
    if(IsProfile){
      Result = ForEach(pExecuteProf);  // ������ ��������
      //-- ��������� ������ ����� ���������� � ����������
      SetCCW();
/*
      if (!WriteCcwDll(ccw))
      {   //-- ���������� ������ �����
          //-- ����������� ������������
          //-- � ��������� ��� ������� � ���� �������
          if(EventMeneger)
              EventMeneger -> Generate(eLostConnect,eController1,NULL);
      }
*/
    }
    return Result;
}

//---------------------------------------------------------------------------
unsigned char * __fastcall TFlameList::GetPSW_Addr()
{
   return psw;
}

//---------------------------------------------------------------------------
int __fastcall TFlameList::GetPSW_Length()
{
   return sizeof (psw);
}

//---------------------------------------------------------------------------
// �������� �� ������� ���� �� ������� � ����� �� �����
bool __fastcall TFlameList::TestSameProfile(sProfileParam* p)
{
      return ForEach(pTestSameProf, p);
}

//------------------------------------------------------------------------------
unsigned char __fastcall TFlameList::GetCCW(int __index)
{
      return ccw[__index];
}

//------------------------------------------------------------------------------
void __fastcall TFlameList::SetCCW_(unsigned char v ,int __index)
{
      ccw[__index] = v;
}

//------------------------------------------------------------------------------
void __fastcall TFlameList::SetCCW(unsigned char v ,int __index)
{
      ccw[__index] = v;
      //-- ��������� ������ ����� ���������� � ����������
      if (!WriteCcwDll(ccw))
      {   //-- ���������� ������ �����
          //-- ����������� ������������
          //-- � ��������� ��� ������� � ���� �������
          if(EventMeneger)
              EventMeneger -> Generate(eLostConnect,eController1,NULL);
      }
}

//------------------------------------------------------------------------------
void __fastcall TFlameList::SetVisibleInterval(TDateTime value)
{
        if(FVisibleInterval != value) {
                FVisibleInterval = value;
                EventMeneger->VisibleInterval = value;
                ParamMenager->VisibleInterval = value;
        }
}

//------------------------------------------------------------------------------
TDateTime __fastcall TFlameList::GetVisibleInterval()
{
        return FVisibleInterval;
}
//------------------------------------------------------------------------------
bool __fastcall TFlameList::ClearParamArray(void)
{
      for (int i = 0; i< MAX_OBJECT;i++){
         all_param[i].Fl_UNKNOWN_DATA = 0;
         all_param[i].Source = 0;
      }
      return ForEach(pClearParamArray);
}

//------------------------------------------------------------------------------
//!! ��������!!! ������ �� ��, ��� ������� ���������������
bool __fastcall TFlameList::SetCurrentByUnit(int Index)
{
     if (FFlameUnitCount > Index){
       if(FCurrentUnitIndex != Index){
          FCurFlameUnit = FlameUnits[Index];
          FCurrentUnitIndex = Index;
       }
       return true;
     }else
       return false;
}


//-- �������� ���� ������ ---------------------------------------------------
bool __fastcall TFlameList::OpenTables (void)
{
//    TEventMen *FEventMeneger;
//    TParamMen *FParamMenager;
    if (!FEventMeneger || !FParamMenager || !FJurnalWorkMeneger)
    {
        MessageBox (GetActiveWindow(),
        "���������� ������� ������� ������ ���� ������ ��� ���\n"
        "�������: ��������� ������",
        "��������� ������!",MB_OK|MB_ICONERROR);
        return false;
    }
    if(!FEventMeneger->OpenTable() || !FParamMenager->OpenTable() || !FJurnalWorkMeneger->OpenTable())
        return false;
    else
        return true;
}

//-- �������� ���� ������ ------------------------------------------------------
bool __fastcall TFlameList::CloseTables (void)
{
    if (FEventMeneger) {  FEventMeneger->Close(); }
    if (FParamMenager) {  FParamMenager->Close(); }
    if (FJurnalWorkMeneger) { FJurnalWorkMeneger->Close(); }
    return true;
}


//--  ���������������� ��������� ��������   ------------------------------------
bool __fastcall TFlameList::InitCtrlParam(AnsiString aDllName, tSetMessage aSetMessage)
{
    memset(term,0,sizeof(term));
    // true - ���� ���� ��������
    if(ForEach(pInitCtrlParam)){
        if(pTermoDat)
             delete pTermoDat;  //��� �����,�����(hendle)  ���������a 
        pTermoDat = new TTermoDat(aDllName,hDLL,aSetMessage);
        ForEach(pTestAltEnable);    // ������ �������  � ��������� ����  ���������
        return true;
    }
    return false;
}

//--  ������ �������� ����������   ---------------------------------------------
bool __fastcall TFlameList::PersInputInfo()
{
    if(ForEach(pPersInputInfo)){
      if (AccessMode == eDebugger)
            SetMessage (true,"������� ������� ������������ ����������.");
      return true;
    }else
      return false;
}

//--  ���������� ���� ���������� ���������  ------------------------------------
bool __fastcall TFlameList::MakeTermalProgramm()
{
    if(IsProfile){
       ForEach(pMakeTermalProg);
    }
    term[6] = Options->All_Limits[3].EReg;
    term[7] = Options->All_Limits[4].EReg;
    term[8] = Options->All_Limits[5].EReg;
    //-- ��������� ������ ������� ���������� �� � ����������� � ����������
    if (!WriteTermDll(term))
    {   //-- ���������� ������ �����
        //-- ����������� ������������
        if (AccessMode == eDebugger)
           SetMessage (true,"������ ������ � ������������");
        //-- � ��������� ��� ������� � ���� �������
        GenerateEvent(eLostConnect,eController1,NULL);
           return false;
    }
    return true;

}

//--  �������� ������� ���������� ���� ������  ������� -------------------------
void __fastcall TFlameList::iSetDateLastDB_Updating(int value)
{
    if (FParamMenager){
          if(value < 2) value = 1;
          FPeriodLastDB_Updating = value;
          TDateTime Period=  EncodeTime(0 , value / 60, value % 60, 0);
          if((TDateTime::CurrentDateTime() - FParamMenager->DateLastDB_Updating) > Period)
              FParamMenager->DateLastDB_Updating = TDateTime::CurrentDateTime() -  Period;
    }else
          FPeriodLastDB_Updating = 0;
}

//------------------------------------------------------------------------------
int  __fastcall TFlameList::iGetDateLastDB_Updating()
{
    return FPeriodLastDB_Updating;
}


//--  �������� ������� ���������� ���� ������ ��������� ------------------------
void __fastcall TFlameList::iSetDateLastMagazining(int value)
{
    if(value < 30) value = 30;
    TDateTime Period=  value; //EncodeDate(0 , value / 30, value % 30);
    TDateTime CurTm= TDateTime::CurrentDateTime(); //
    
    if (FParamMenager)
        if((CurTm - FParamMenager->DateLastMagazining) > (Period + Period))
            FParamMenager->DateLastMagazining = CurTm -  Period;
    if (FEventMeneger)
        if((CurTm - FEventMeneger->DateLastMagazining) > (Period + Period))
            FEventMeneger->DateLastMagazining = CurTm - Period;
    if ((FParamMenager == NULL)&&(FEventMeneger == NULL))
        FPeriodLastMagazining = 0;
    else
        FPeriodLastMagazining = value;
}

//------------------------------------------------------------------------------
int  __fastcall TFlameList::iGetDateLastMagazining()
{
    return FPeriodLastMagazining;
}


//------------------------------------------------------------------------------
bool __fastcall    TFlameList::InitFlameList(tSetMessage aSetMessage)
{
    AnsiString aDllName;
    // ������������� ������� ������ ���������
    SetMessage = aSetMessage;
    //-- ������������� ����� ������ ��� ���
    StartArmTime = TDateTime::CurrentDateTime();

    Options->LoadFromRegistry(true);
//    Timer1->
    Interval = Options->Interval;   //-- ���������� ��������
    TDateTime viTime (  (unsigned short)((Options->VisibleInterval /3600)%24),
                        (unsigned short)((Options->VisibleInterval/60)%60),
                        (unsigned short)((Options->VisibleInterval)%60),
                        0);
    //--  ���������� �������� ����������
    VisibleInterval = viTime;
    EventMeneger->SetInterval(Options->EventView);
    SetMessage (false,"�������� ���������� �����...");
    //-- ������ �� ������� �������� ���������� �����
    if (!GetRegParam (cFlameCtrlRegKey,"PLC_DLL",aDllName))
    {
        aDllName = "PLC_Exchange.DLL";
        PutRegParam(cFlameCtrlRegKey,"PLC_DLL",aDllName);
    }
    //-- �������� ���������� �����
    if(!LoadChannelDll(aDllName))
       return false;                     // �� ������� ��������� ��������� ����������

    Ctrl_param->LoadFromRegistry(true);
    ForEach(pUpLoadCtrlParToPLC);
    
    //--  ���������������� ��������� ��������
    // ��������� ������������� ���������
    if(InitCtrlParam(aDllName,aSetMessage)){};
    SetMessage (false,"�������� ���� ������...");
    return OpenTables();
}

//------------------------------------------------------------------------------
bool __fastcall    TFlameList::LoadMultiData(double * Data)
{
     return ForEach(pLoadMultiData, Data);

}

//------------------------------------------------------------------------------

