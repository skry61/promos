object Form1: TForm1
  Left = 199
  Top = 193
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object TrendGrapher1: TTrendGrapher
    Left = 16
    Top = 48
    Width = 300
    Height = 313
    CaptionSize = 14
    CaptionFont = 'Arial'
    Caption = #1043#1088#1072#1092#1080#1082' 1'
    BorderColor = clNavy
    BorderWidth = 1
    LineWidth = 0
    ArrowDraw = False
    VMax = 100
    VStep = 10
    HMax = 100
    HStep = 10
    MarkColor = clGreen
    MarkerColor = clBlack
    ValueFormat = ffFixed
    Divisor = 1
    TimeFormat = 'hh:nn.ss'
    TimeStep = 0.000115740740740741
    TimeStart = 40223.6638955787
    Enable_Adjusting = False
    AdjustColor = clBlack
  end
  object TimeGrapher1: TTimeGrapher
    Left = 352
    Top = 48
    Width = 300
    Height = 313
    CaptionSize = 14
    CaptionFont = 'Arial'
    Caption = #1043#1088#1072#1092#1080#1082' 1'
    BorderColor = clNavy
    BorderWidth = 1
    LineWidth = 0
    ArrowDraw = False
    VMax = 100
    VStep = 10
    HMax = 100
    HStep = 10
    MarkColor = clGreen
    MarkerColor = clBlack
    ValueFormat = ffFixed
    Divisor = 1
    TimeFormat = 'hh:nn.ss'
    TimeStep = 0.000115740740740741
    TimeStart = 40223.6640111343
    Enable_Adjusting = False
    AdjustColor = clBlack
  end
end
