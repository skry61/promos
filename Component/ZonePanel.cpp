//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ZonePanel.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TZonePanel *)
{
    new TZonePanel(NULL);
}
//---------------------------------------------------------------------------
__fastcall TZonePanel::TZonePanel(TComponent* Owner)
    : TPanel(Owner)
{
    FOneShape = new TShape (Owner);
    FStringGrid = new TStringGrid (Owner);
    FLabel = new TLabel (Owner);

    FOneShape->Align = alClient;
    FOneShape->Brush->Color = clNavy;
    FStringGrid->Left = 10;
    FStringGrid->Top = 56;
    FStringGrid->Width = this->Width-21;

    FLabel->Left = 80;
    FLabel->Top = 8;
    FLabel->Font->Color = clYellow;
    FLabel->Font->Name = "Arial";
    FLabel->Font->Size = 14;
    FLabel->Caption = "����";
}
//---------------------------------------------------------------------------
__fastcall TZonePanel::~TZonePanel()
{
    delete FStringGrid;
    delete FOneShape;
    delete FLabel;
}
//---------------------------------------------------------------------------
void __fastcall TZonePanel::SetCaption (AnsiString aCap)
{
    FLabel->Caption = aCap;
}

//---------------------------------------------------------------------------
namespace Zonepanel
{
    void __fastcall PACKAGE Register()
    {
        TComponentClass classes[1] = {__classid(TZonePanel)};
        RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------
 