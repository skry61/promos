//---------------------------------------------------------------------------
//      ����� ��������������.
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Auth.h"
#include "Config.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLoginForm *LoginForm;

//---------------------------------------------------------------------------
__fastcall TLoginForm::TLoginForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLoginForm::BitBtn1Click(TObject *Sender)
{
        char cPWLA[80];
        char cPWLT[20];

    //-- ������ �� ������� ������ ��������������
    if (!GetRegBinData(cFlameCtrlRegKey,"PWL1",cPWLA,sizeof(cPWLA)))
    {
        strcpy(cPWLA,"ADMIN");
        PutRegBinData(cFlameCtrlRegKey,"PWL1",cPWLA,sizeof(cPWLA));
    }

    //-- ������ �� ������� ������ ���������
    if (!GetRegBinData(cFlameCtrlRegKey,"PWL2",cPWLT,sizeof(cPWLT)))
    {                                                            
        strcpy(cPWLT,"TEHNOL");
        PutRegBinData(cFlameCtrlRegKey,"PWL2",cPWLT,sizeof(cPWLT));
    }
    AnsiString Admin = AnsiString(cPWLA).UpperCase();    
    AnsiString Tehnol = AnsiString(cPWLT).UpperCase();    


    if (Edit2->Text.IsEmpty())
    {
        AccessMode = eUser;
        Close();
        ModalResult = mrOk;
    }

    Password = Edit2->Text.UpperCase();
    if (Password == AnsiString("������") ||
        Password == Tehnol               ||
        Password == AnsiString("NT[YJK") ||
        Password == AnsiString("NT{YJK") )
    {
        AccessMode = eTechnolog;
        Close();
        ModalResult = mrOk;
    }
    if (Password == AnsiString("�����") ||
        Password == Admin               ||
        Password == AnsiString("FLVBY") )
    {
        AccessMode = eAdmin;
        Close();
        ModalResult = mrOk;
    }
    if (Password == AnsiString("�����") ||
        Password == AnsiString("JNKFL") )
    {
        AccessMode = eDebugger;
        Close();
        ModalResult = mrOk;
    }
    return;
}
//---------------------------------------------------------------------------

void __fastcall TLoginForm::Edit2KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if (Key == VK_RETURN) BitBtn1Click(NULL);
    if (Key == VK_ESCAPE)
    {
        Close();
        ModalResult = mrCancel;
    }

}
//---------------------------------------------------------------------------

AnsiString __fastcall AccessModeName ()
{
    return AnsiString (cAccessMode[AccessMode]);
}
void __fastcall TLoginForm::FormShow(TObject *Sender)
{
    Edit2->Text = "";    
}
//---------------------------------------------------------------------------

