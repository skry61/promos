#include "DTrace.hpp"
#include <stdio.h>

int NumWR,NumWR32;
void DTraceInit(void)
{
    int FH;
    NumWR=0;
    NumWR32=0;
    FH = FileCreate("debug.txt");
    FileClose(FH);
}

void DTraceStr(AnsiString Str)
{
    FILE *f;
    int i;
    char buf[180];
    f = fopen("debug.txt","at+");
    if (f==NULL) return;
    for (i=1;i<Str.Length();i++) buf[i-1]=Str[i];
    buf[i]=0;
    fprintf (f,"%s\n",buf[0]);
    fclose(f);

}
void DTrace(char *Str)
{

    FILE *f;
    f = fopen("debug.txt","at+");
    if (f==NULL) return;
    fprintf (f,"%s\n",Str);
    fclose(f);

}
void DTraceInt(char *Name,int Val)
{

    FILE *f;
    f = fopen("debug.txt","a+");
    if (f==NULL) return;
    fprintf (f,"Int %s =%d\n",Name,Val);
    fclose(f);

}

void DTraceWrite32(char *Name,int len)
{
    FILE *f;
    char fname[20];
    NumWR32++;
    sprintf (fname,"wr32_%+3d",NumWR32);
    f = fopen(fname,"wb");
    if (f==NULL) return;
    fwrite (Name,len,1,f);
    fclose(f);

}

void DTraceWrite(char *Name,int len)
{
    FILE *f;
    char fname[20];
    NumWR++;
    sprintf (fname,"wr_%-3d",NumWR);
    f = fopen(fname,"wb");
    if (f==NULL) return;
    fwrite (Name,len,1,f);
    fclose(f);
  
}

void DTraceRead(char *Name,int len)
{
    FILE *f;
    char fname[20];
    NumWR++;
    sprintf (fname,"rd_%-3d",NumWR);
    f = fopen(fname,"wb");
    if (f==NULL) return;
    fwrite (Name,len,1,f);
    fclose(f);

}
void CmDTraceI (char* Nunit, int Dir,int Cmd, int len, void* VStr)
{
    char str[20];
    sprintf (str,"com=%02X",Cmd);
    CmDTrace (Nunit,Dir,str,len,VStr);
}
void CmDTrace (char* Nunit, int Dir,char *Ncmd, int len, void* VStr)
{
    FILE *f;
    int i;
    char *Str = (char *)VStr;
    f = fopen("debug.txt","a+");
    if (f==NULL) return;
    fprintf (f,"-------------------------------------------\n");
    fprintf (f,"%10s %s %-13s",Nunit,(Dir==0x45)?"READ ":"WRITE",Ncmd);
    for (i=0;i<len;i++) fprintf (f,"%02X ",(unsigned char)*(Str+i));
    fprintf (f,"\n");
    fclose(f);
}

void DTraceDump(char *Name,char *Str,int len)
{
    FILE *f;
    int i,page,a;
    f = fopen("debug.txt","a+");
    if (f==NULL) return;
    fprintf (f,"%-13s",Name);
    int j,cp=0;
    page = 1+ (len / 16);
    for (j=0;cp<page;cp++)
    {
        for (i=0;(i<16)&&(j<len);j++,i++)
        {
            a = (unsigned char)*(Str+j);
            fprintf (f,"%02X ",a);
        }
//        fprintf (f,"%c%c",0xD,0xA);
    }
//    fprintf (f,"%c%c",0xD,0xA);
    fprintf (f,"\n");
    fclose(f);

}

