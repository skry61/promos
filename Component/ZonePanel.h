//---------------------------------------------------------------------------
#ifndef ZonePanelH
#define ZonePanelH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class PACKAGE TZonePanel : public TPanel
{
private:
    TStringGrid *FStringGrid;
    TShape      *FOneShape;
    TLabel      *FLabel;

    AnsiString  FCaption;
    void __fastcall SetCaption (AnsiString aCap);

protected:
public:
    __fastcall TZonePanel(TComponent* Owner);
    __fastcall ~TZonePanel(void);

__published:
    __property TStringGrid* StringGrid = {read=FStringGrid, write=FStringGrid};

    __property AnsiString Caption = {read=FCaption, write=SetCaption};
};
//---------------------------------------------------------------------------
#endif
