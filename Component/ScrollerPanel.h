//---------------------------------------------------------------------------
#ifndef ScrollerPanelH
#define ScrollerPanelH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include "Grapher.h"
//---------------------------------------------------------------------------

enum TScrPanelBtnType
{
    btFirst,
    btPrev,
    btNext,
    btLast,
    btPlus,
    btMinus
};
/*
struct TScrPanelButton
{
    bool Visible;
    bool Enabled;
    bool Auto;
    TBitmap Bitmaps[2];
};
*/

class PACKAGE TScrollerPanel : public TCustomPanel
{
private:
    TSpeedButton*   Buttons[6];
    TScrollBar*     Bar;
    TGrapher*       FGrapher;           //-- ����������� ������ �������
    int             FButtonWidth;       //-- ������ ������ �� ��������
    int             FButtonHeight;      //-- ������ ������ �� ��������
    int             FVisibleWindow;     //-- ������ ������� ������� � ������ �������
    int             FStartWindow;       //-- ������ ������� ������� � ������ �������
    int             FSamples;           //-- ����� ���������� �������
    int             FPageSize;          //-- ������ ������ ����  � ������ �������

    int             Zoom;               //-- ���


    void __fastcall SetButtonHeight(int aHeight);
    void __fastcall SetButtonWidth(int aWidth);

    void __fastcall SetVisibleWindow(int aVisible);
    void __fastcall SetStartWindow(int aStart);
    void __fastcall SetSamples(int aSamples);

	Classes::TNotifyEvent FOnOutResize;
	Classes::TNotifyEvent FPrevSamples;
	Classes::TNotifyEvent FNextSamples;

    void __fastcall MyResize(TObject* Sender);

    void __fastcall MyKeyDown(TObject *Sender, WORD &Key,TShiftState Shift);

    void __fastcall BtnHandler(TObject* Sender);
    void __fastcall ScrollBarScroll(TObject *Sender,
                    TScrollCode ScrollCode, int &ScrollPos);
    void __fastcall ScrollBarChange(TObject* Sender);
    void __fastcall SetGrapher(TGrapher* Gr);

    void __fastcall UpdateGrapher(void);
    int __fastcall  GetBarPosition(void);

protected:
public:

    __fastcall TScrollerPanel(TComponent* Owner);
__published:

	__property Classes::TNotifyEvent NextSamples = {read=FNextSamples, write=FNextSamples};
	__property Classes::TNotifyEvent PrevSamples = {read=FPrevSamples, write=FPrevSamples};
	__property Classes::TNotifyEvent OnOutResize = {read=FOnOutResize, write=FOnOutResize};

    __property TGrapher* Grapher = {read=FGrapher, write=SetGrapher};

    __property int ButtonWidth = {read=FButtonWidth, write=SetButtonWidth, default=0};
    __property int ButtonHeight = {read=FButtonHeight, write=SetButtonHeight, default=0};

    __property int VisibleWindow = {read=FVisibleWindow, write=SetVisibleWindow, default=0};
    __property int StartWindow = {read=FStartWindow, write=SetStartWindow, default=0};
    __property int Samples = {read=FSamples, write=SetSamples, default=0};
    __property int PageSize = {read=FPageSize, write=FPageSize, default=0};

    __property Align;
    __property OnResize;
    __property OnKeyDown;
    //__property OnKeyPress;
    //__property OnKeyUp;

};
//---------------------------------------------------------------------------
#endif
