//---------------------------------------------------------------------------
// ������ �������������� ��������� ������
//---------------------------------------------------------------------------
#include <vcl.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#pragma hdrstop

#include "FlameMath.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

int matherr (struct exception *a)
{   return 0;   }



double CelcToKelvin (double C)
{    return C + (double)273.3;  }

double Cg_func (double Ac, double Tp)
{   return (Ac * 100 )/ (1.07 * exp(4798.6/Tp) + 19.5*Ac );     }


double Ac_func (double CO, double Tp, double E, double Td)
{
    double step = (6995/Tp) - 6.1367 + (E - 1290)/(0.0992*Td);
    double res_pow = pow (10,step);
    return CO*res_pow;
}

bool order (double value, int &order)
{
    if (value == 0) return false;
    int result = 0;
    while (1)
    {
        if (value <= 1 && value > 0.1)
        {
            order = result;
            return true;
        }

        if (value > 1)
        {
            value = value/10;
            result++;
        }
        if (value <= 0.1)
        {
            value = value*10;
            result--;
        }
    }
}
/*
bool MathHandle (sArmParam* param, bool IsCalcCg , bool IsCalcCgOld)
{
    if (CelcToKelvin(param->Param[0])==0) return false;
    if (CelcToKelvin(param->Param[1])==0) return false;

    if (!IsCalcCg || IsCalcCg &&IsCalcCgOld){ // ��� ���������������
       param->Param[4] = Ac_func (  param->Param[3],
                        CelcToKelvin(param->Param[0]),
                        param->Param[2],
                        CelcToKelvin(param->Param[1]));

      //-- ����������� ��������� �������� �� 0 �� 100%
      if (param->Param[4] < 0) param->Param[4] = 0;
      if (param->Param[4] > 100) param->Param[4] = 101;
    }

    if (IsCalcCg)
    {
        if(IsCalcCgOld)
//��������� (���� ���������� �� ��)
            param->Param[5] = Cg_func(   param->Param[4],
                              CelcToKelvin(param->Param[0]));
        else
            param->Param[5] = param->Param[2]/1000; // �� � ���� ���

        //-- ����������� ��������� �������� �� 0 �� 100%
        if (param->Param[5] < 0) param->Param[5] = 0;
        if (param->Param[5] > 100) param->Param[5] = 101;
    }
    return true;
}
*/
