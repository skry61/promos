const unsigned short Cmd_READIO         =  0x42; // ������ �/�
const unsigned short Cmd_WRITEIO        =  0x44; // �������� �/�
const unsigned short Cmd_READPROGRAM    =  0x45; // ������ ��������� � PLC
const unsigned short Cmd_WRITEPROGRAM   =  0x46; // �������� ��������� � PLC
const unsigned short Cmd_READDATA       =  0x47; // ������ �� ������ ����� PLC
const unsigned short Cmd_WRITEDATA      =  0x48; // �������� � ������ ������ PLC
const unsigned short Cmd_STOPUPU        =  0x4a; // ���� PLC
const unsigned short Cmd_STARTUPU       =  0x4b; // ����� PLC
const unsigned short Cmd_GETSTATUSUPU   =  0x4c; // �������� ���������� PLC
const unsigned short Cmd_STARTSTEP      =  0x4f; // ��������� ������
const unsigned short Cmd_NEXTSCAN       =  0x50; // ���-�� ������ � ��������� ������
const unsigned short Cmd_BREAKSTEP      =  0x51; // ������ ��������� ������
const unsigned short Cmd_TEST           =  0x52; // ������������ ������ �����

enum RKSCommands{
cmd_NOP,
cmd_TNA,
cmd_TFA,
cmd_TNO,
cmd_TFO,
cmd_TNE,
cmd_TFE,
cmd_SKP,
cmd_SON,
cmd_SOF,
cmd_YON=10,
cmd_FON=11,
cmd_TIM,
cmd_CTR,
cmd_HLT=15,
cmd_PTS=29,
cmd_NTS
};

enum RKS_JMPCommands{
cmd_JMY=4,
cmd_JMN=5,
cmd_JMP=6
};
 
