//---------------------------------------------------------------------------


#pragma hdrstop

#include "Params.h"
#include "EventMen.h"




char *EventName[]=
{
    "--- empty ---",
    "������ ��������� ��� ���",
    "���������� ��������� ��� ���",
    "��������� ������������ ���������� %s",
    "����� ������ ������� � ���������",
    "���������� ������������ ������. ����� ���������: %s",
    "������ ����� � ������������",
    "���������� ������ ������ ����������� %s ��",
    "����������� �������� ������������ CO %s %",
    "������ ������� �������� ����������� ���� �� %s ����.�",
    "������ ��������������� ������� ����������",

    "� ���� ���� �������� �������! Tn = %s",
    "� ���� ���� ������� �������! Tn = %s",
    "� ������� ���� �������� �������! T� = %s",
    "� ������� ���� ������� �������! T� = %s",
    "��� ������� ���� �������� �������! � = %s",
    "��� ������� ���� ������� �������! � = %s",

    "���������� �������� ���� II �������� �������! Ac = %s",
    "���������� �������� ���� �������� �������! Ac = %s",
    "���������� �������� ���� ������� �������! Ac = %s",

    "���������� ��������� ���� �������� �������! �� = %s",
    "���������� ��������� ���� ������� �������! �� = %s",

    "����� ������������ ����������� ������� %s",
    "���������� ������������ ����������� ������� %s",
    "������� � ����������� ���������� ������������ ������� %s",
    "������ ������� �������� ����������� ������� �d %s ����.�",
    "������ ������� �������� ��� ������� %s ��",

    "� ���� � �������� ����� Tn = %s",
    "� ������� � �������� ����� T� = %s",
    "��� ������� � �������� ����� � = %s",
    "���������� �������� � �������� ����� Ac = %s",
    "���������� ��������� � �������� ����� C� = %s",

    "������ ��� ��� ������������� E = %s",
    "������ C� = %s",
    "������ �� ��� ������������� (��� �����) C� = %s",

    "������� ����� <������ �������> %s",

    "������ ����� � ��� �����������. ����� %s",
    "��� ����������������������� (����� %s) �� ������",

    "������ �������� ����� ��� ��������� ���� %s ���",

    "���������� ������� �������"

};



TFDbDescriptor tfdEvent=
{
    "event.db",
    sizeof (sArmEvent),
    true,
    false,
    100,
// ��������� ��� ������ ��������    
    "���������� �������� ������ � ���� ������ ������� ��� ���\n"
    "�������: ����������� �� ������� \n\n"
    "���������� ������������ �� ������� ?\n"
};

TEventMen *EventMenagerGlobal;

//---------------------------------------------------------------------------
__fastcall TEventMen::TEventMen(Classes::TComponent* AOwner)
  : TxDataBase(AOwner,&tfdEvent/*,NULL*/)
{
}
//---------------------------------------------------------------------------
__fastcall TEventMen::~TEventMen()
{
}
 
//---------------------------------------------------------------------------
//-- ���������� ������ ��������� 
int  __fastcall TEventMen::GetStructSize(void)
{
   return sizeof(sArmEvent);
}

//---------------------------------------------------------------------------
//-- ������� ����� �������
void * __fastcall TEventMen::CreateNew(void)
{
   return (void*)new sArmEvent;                          //-- ������� ���������
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ �������
//---------------------------------------------------------------------------
void __fastcall TEventMen::ClearList(void)
{
    sArmEvent *p;
    for (int i = 0; i < List->Count; i++)
    {
        p = (sArmEvent *)List->Items[i];
        if (p)
          delete p;
    }
    List->Clear();
} 

//---------------------------------------------------------------------------
//-- ��������� ��������  ������������� ������ ������� �� �����
//---------------------------------------------------------------------------
/*
void virtual __fastcall TEventMen::LoadList(TDateTime Time)
{
}    

*/
//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������ �������
//---------------------------------------------------------------------------
void __fastcall TEventMen::ClearAll(void)
{

//??    RecoverTable();                //-- ���������� ���� ���� ������
    Items->Clear();                //-- ���������� ������ �� ������
    ClearList();                   //-- �������� ������
}

// ���������� �������� 
void __fastcall TEventMen::SetInterval(int intervalIntex)
{
    TDateTime dtHour ( 1,0,0,0);
    switch (intervalIntex)
    {
        case 0: FVisibleInt = (720*(double)dtHour); break;
        case 1: FVisibleInt = (168*(double)dtHour); break;
        case 2: FVisibleInt = (24*(double)dtHour);
                break;
        case 3:
        default:
                FVisibleInt = dtHour; break;
    }   
}

//---------------------------------------------------------------------------
//-- ��������� �������� ������������� ������, ���� � ���������� �� �����
void __fastcall TEventMen::LoadAll(TDateTime Time)
{
    LoadList(Time - FVisibleInt);
}

//---------------------------------------------------------------------------
/*
bool __fastcall TEventMen::RecovertTable (void)
{
    AnsiString NewFileName = cEventDbFileName;
    NewFileName += TDateTime::CurrentDateTime().FormatString (" hh-nn dd-mm-yyyy");
    NewFileName += ".bak";

    //-- ��������� ���������� ������ ���� ������
    if (!RenameFile(AnsiString (cEventDbFileName),NewFileName))
    {
        MessageBox (GetActiveWindow(),
        "���������� �������� ������ � ���� ������ ��� ���\n"
        "���������� ��������� ��������� ����������� ��",
        "��������� ������!",MB_OK|MB_ICONERROR);
        return false;
    }
    return Open();

}
*/
//-- ��������� ���������� ������ ������� �� ������ -----------------------------
bool __fastcall TEventMen::FillListComp(void)
{
    int EventCount = List->Count;
    Items->Clear();
    Items->BeginUpdate();
    int cur;
    for (int i = 0; i<EventCount; i++)
    {
        pEvent = (sArmEvent *)List->Items[i];
        AddToListComp();
        //Application->ProcessMessages();
    }
    Items->EndUpdate();

    if (Items->Count > 0)
    {
        Repaint();
        ItemFocused = Items->Item[0];
        Selected = Items->Item[0];
    }
    return true;
}

//-- ��������� ���������� ������ ������� �� ����� ------------------------------
bool __fastcall TEventMen::QuickFillListComp(int * TypeArray, int MaxIndex, TDateTime StartTime)
{
    int EventCount = RecCount();
    Items->Clear();
    FirstQuick();
    Items->BeginUpdate();

    pEvent = new sArmEvent;

    for (int i = 0; i<EventCount; i++)
    {
        if(GetRecordQuick(pEvent))
        {
            //-- ����������� ������ � ����
            if (StartTime < (TDateTime)1.1)         // ���� ����� ������ - �� �� �������
                StartTime = Now();
            if (pEvent->Time >= StartTime)
            {
              //-- ���������� ���� ������� �������: � ������ ��� � �������
              for(int j=0;j<MaxIndex;j++)
              if (pEvent->Id == TypeArray[j])
              {
                  AddToListComp();
                  break;
              }
            }
        }
        else
            break;
    }
    delete pEvent;
    Items->EndUpdate();

    if (Items->Count > 0)
    {
        Repaint();
        ItemFocused = Items->Item[0];
        Selected = Items->Item[0];
    }
    return true;
}

//---------------------------------------------------------------------------
/*
void __fastcall TEventMen::AddEventList(sArmEvent *event)
{
        //-- ���������� ���� ������� �������: � ������ ��� � �������
    if (event->Id >= eRunArm && event->Id <= eAccessChange) return;

    //-- *******
    //-- ���� ��� ������������ ���-�� ������� ������� �� ������
    while(ListView->Items->Count > MAX_EVENT_VIEW)
    {
        ListView->Items->Delete( EventListView->Items->Count - 1 );
    }
    //-- *******

    ListItem = EventListView->Items->Insert(0);//Add();
    ListItem->Caption = event->Time.FormatString ("yyyy\\mm\\dd hh:nn.ss");
    sprintf (str,EventName[event->Id],event->Param);
    ListItem->SubItems->Add(str);
    
    ListItem->SubItems->Add(pSourceName->Strings[event->Source]);
//~~    ListItem->SubItems->Add(SourceName[event->Source]);

    EventListView->Selected = ListItem;
    EventListView->ItemFocused = ListItem;
}
*/
//---------------------------------------------------------------------------
//-- ��������� ������� � ���� ������, � ������ � � ����������  -----------------
bool __fastcall TEventMen::Store()
{
    Append(pEvent);                 //-- ��������� � ����
    List->Add (pEvent);        //-- ��������� � ������������ ������
    AddToListComp();
//    AddEventToListBox(pEvent);    //-- ��������� � ��������� ListBox
//    SendMessage(hWnd, MY_AddEventMESSAGE, 0, pEvent) //-- ��������� � ��������� ListBox

    //-- ���� �� ������ ��� ������ �������� - �� ������
    if (List->Count > 1)
    while (1)
    {
        sArmEvent * p = (sArmEvent *)List->Items[0];

        if (p->Time < (pEvent->Time - FVisibleInt) )
        {
            delete p;
            List->Delete(0);        
//            DeleteEventFromListBox();
//            ListView->
            Items->Delete (/*ListView->*/Items->Count-1);

//            SendMessage(hWnd, MY_RemEventMESSAGE, 0, 0t) //-- ������� ��������� � ����� ListBox
        }
        else break;
    }
    return true;
}

 //-- ��������� ���������� ����������� ���������� ������ -----------------------
void __fastcall TEventMen::AddToListComp(void)
{   //-- ���������� ���� ������� �������: � ������ ��� � �������
//    if (pEvent->Id < eRunArm || pEvent->Id > eAccessChange) return;
    bool Allowed = true;
    if (FOnTestEventType) FOnTestEventType(this,pEvent->Id,Allowed);
    if (Allowed){

        //-- ���� ��� ������������ ���-�� ������� ������� �� ������
        while(/*ListView->*/Items->Count > MAX_EVENT_VIEW)
        {
            /*ListView->*/Items->Delete( /*ListView->*/Items->Count - 1 );
        }
        TListItem *ListItem = /*ListView->*/Items->Insert(0);//Add();
        ListItem->Caption = pEvent->Time.FormatString ("yyyy\\mm\\dd hh:nn.ss");
        char str[80];
        sprintf (str,EventName[pEvent->Id],pEvent->Param);
        ListItem->SubItems->Add(str);

        ListItem->SubItems->Add(pSourceName->Strings[pEvent->Source]);
    //    ListView->
        Selected = ListItem;
    //    ListView->
        ItemFocused = ListItem;
    }
}

//---------------------------------------------------------------------------
//-- ������������ ������� ---------------------------------------------------
//---------------------------------------------------------------------------
bool __fastcall TEventMen::Generate(int EventId,int Source,char* Comment)
{
    pEvent = new sArmEvent(TDateTime::CurrentDateTime(),EventId,Source, Comment);
    Store();
    return true;
}


bool __fastcall GenerateEvent(int EventId,int Source,char* Comment)
{
    EventMenagerGlobal->Generate(EventId,Source,Comment);
    return true;
}


namespace Eventmen
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TEventMen)};
         RegisterComponents("Fx", classes, 0);
    }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#pragma package(smart_init)

/*
void __fastcall TEventMen::SetEventListView(TListView * value)
{
     SetListView (value);
}

TListView * __fastcall TEventMen::GetEventListView()
{
//     return GetListView();
     return ListView;
}
*/
