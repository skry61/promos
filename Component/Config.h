//---------------------------------------------------------------------------
// ������ ����������������
//---------------------------------------------------------------------------
#ifndef ConfigH
#define ConfigH

#include <vcl.h>
#include <Grids.hpp>
#include <Registry.hpp>
#include "Profile.h"


//-- ��������� ���������������� 1 ��������� ---------------------------------
struct sConfItem
{
    char*   Name;       //-- �������� ���������
    int     Value;      //-- ������� ��������
    int     Min;        //-- ����������� ��������
    int     Max;        //-- ������������ ��������
};

//-- ��������� ����� ���������������� ---------------------------------------
struct sBlockConf
{
    char*           Name;   //-- �������� ����� ����������������
    sConfItem*      Items;  //-- ��������� �� ���������
    int             Count;  //-- ���������� ���������� � �����
};

//---------------------------------------------------------------------------
// ��������� ����������������
//---------------------------------------------------------------------------

//-- ���������� ������� StringGrid � �������� ���������� --------------------
int __fastcall ConfPrepareSG (TStringGrid *SG,AnsiString aStr1, AnsiString aStr2);

//-- ���������� ������� StringGrid �� ����� ������������ --------------------
int __fastcall ConfIntoSG (TStringGrid *SG,sBlockConf* pBlock,int Idx);

//-- ���������� ����� ������������ �� ��������� ������ ������� StringGrid ---
int __fastcall SGIntoConf (TStringGrid *SG,sBlockConf* pBlock,int Idx);


//-- ���������� ����� ������������ � ������� ������� ------------------------
int __fastcall ConfIntoREG (AnsiString RegKey,sBlockConf* pBlock,int Idx);

//-- �������� ����� ������������ �� ������� ������� ------------------------
int __fastcall REGIntoConf (AnsiString RegKey,sBlockConf* pBlock,int Idx);

//-- �������� ��������� �� ������� ������� �� ����� ------------------------
bool __fastcall GetRegParam (AnsiString RegKey,AnsiString Name,AnsiString &Value);

//-- �������� ��������� � ������� ������� �� ����� -------------------------
bool __fastcall PutRegParam (AnsiString RegKey,AnsiString Name,AnsiString Value);


//-- �������� �������� ������ �� ������� ������� �� ����� ------------------------
bool __fastcall GetRegBinData (AnsiString RegKey,AnsiString Name,void* Ptr, int Count);

//-- �������� �������� ������ � ������ ������� �� ����� --------------------------
bool __fastcall PutRegBinData (AnsiString RegKey,AnsiString Name,void* Ptr, int Count);

//-- ���������� ���������� �� ������ -----------------------------------------
bool __fastcall FillComboBox(TComboBox *CB,char** Str,int Count);

//-- �������� ������������� �������� � ��������� ������ ----------------------
bool __fastcall DirExists (AnsiString Name);

//--  �������� ������� ���������� �������  -----------------
void __fastcall ShowGrid1(TStringGrid *SG,sProfileParam* prof);
void __fastcall ShowGrid2(TStringGrid *SG,sProfileParam* prof,double* DPTR);

//--  ����������� ������� ���������� �������  -----------------
void __fastcall InitGrid1(TStringGrid *SG);
void __fastcall InitGrid2(TStringGrid *SG);

//---------------------------------------------------------------------------
#endif
