//---------------------------------------------------------------------------

#ifndef FlameListH
#define FlameListH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

// ������������ ���������� ��������
//#define MAXUNIT     6
#include "FlameUnit.H"
#include "ParamMen.h"
#include "Link_ALT.h"


//-- user supplied -----------------------------------------------------------

//-- type declarations -------------------------------------------------------

class PACKAGE TFlameList : public TTimer
{
private:
    bool  FEnterInTimer;              // �������: ����� � ������ - ��������� ��������� ����
    bool  Fl_NO_1Wire;                // �������: ��� 1 ����� 1Wire

    sArmParam       *ArmParam;
    sControlParam   *Ctrl_param;         //-- ��������� ����������
    sOptions        *Options;            //-- ����� ���������
    unsigned char   psw[MAX_OBJECT+2];   //-- ����� ��������� ����������� . 
                                         //   + 1 ���� - ��������� ������ ����.��� 
                                         //   + 1 ���� - ��������� 1 Wire
    unsigned char   ccw[MAX_OBJECT];     //-- ����� ���������� ������������
    TEventMen       *FEventMeneger;
    TEventMen       *FJurnalWorkMeneger;
    TParamMen       *FParamMenager;
    TClap_Pipe      *FCurClap_Pipe;
    TTermoDat       *pTermoDat;
    short           term[MAX_OBJECT*2];  //-- �������� C� � ���������� �������������
//    unsigned char   bcw;                 //-- ����� ���������� ������� "������ �������"
    BYTE            FBurnSensorBits;     //-- ���� ��������� ������� ��������

    TDateTime       FStartArmTime;       //-- ������ ������ ���������
//    int             Magazining;          //-- ������ ���������� ����������
//    int             DB_Updating;         //-- ������ ���������� ����
    int             FPeriodLastDB_Updating; //-- ������ ���������� ����������   � ���
    int             FPeriodLastMagazining;  //-- ������ ���������� ���������� ����������  � ����





private:
    TUnitTypes FUnits;
    int FStat;
    int FFlameUnitCount;
    int FDummy;
    int FCountOven;                                 // ���������� �����    
    int FCountEndo;                                 // ���������� ���������������
    int FCountProfil;                               // ���� ������ �� �������  - �� +1, ���� ������� , �� -1
    int FCountTermProfil;                           // ���� ������ �� ������� �� ���� ��� - �� +1, ���� ������� , �� -1
    bool FIsProfile;                                // ���� �� ������������ ������� ���� � ����� ����
    eAccessMode FAccessMode;                        // ��� ���������

    TFlameUnit* FCurFlameUnit;
    int FCurrentUnitIndex;
    TDateTime FVisibleInterval;

    void __fastcall              SetUnits(TUnitTypes Value);
    void __fastcall              SetUnit(int Index, TFlameUnit* Value);
    TFlameUnit* __fastcall       GetUnit(int Index);
    bool __fastcall              GetIsOven(void);
    bool __fastcall              GetIsOvenEmpty(void);
    bool __fastcall              GetIsProfile(void);
    bool __fastcall              GetIsTermProfile(void); //���� ���� ���� ���� ���� c ����������� ����������

    void __fastcall              SetCurentFlameUnit(TFlameUnit* value);
    TFlameUnit* __fastcall       GetCurentFlameUnit();
    void __fastcall              SetCurrentUnitIndex(int value);
    int  __fastcall              GetCurrentUnitIndex();
    sProfileParam* __fastcall    GetProfile();
    sProfileParam* __fastcall    GetFirstProfile();
    double*   __fastcall         GetParam();
    void      __fastcall         SetVisibleInterval(TDateTime value);
    TDateTime __fastcall         GetVisibleInterval();
    void      __fastcall         SetParamMenager(TParamMen *aParamMenager);
    tSetMessage                  SetMessage;   //  ����� ��������� �� ������ �� ������ ����������


protected:
    __property OnTimer;                    // �������� ������
    Classes::TNotifyEvent FOnReqestData;    // �������� �����
    DYNAMIC void __fastcall Timer(void);
//	void __fastcall Timer(void);   // ����� ������� � ����


public:
//	Updk_unt::UPDK_Unit* Menager;
    TFlameUnit* FlameUnits[MAX_OBJECT/*MAXUNIT*/];

    unsigned char * __fastcall GetPSW_Addr();
    int __fastcall GetPSW_Length();
    void __fastcall Open(void);
    __fastcall virtual TFlameList(Classes::TComponent* AOwner);
    __fastcall virtual ~TFlameList(void);
    void __fastcall    SetComboBox(TComboBox *aComboBox, int Type);
    bool __fastcall    SetProfileComboBox(TComboBox *aComboBox);

    // ��������� ������ � ������� ������������ � �������
    void __fastcall    SetExLinks(sArmParam *aArmParam, sControlParam *aCtrl_param, sOptions *Options);

    bool __fastcall    ClearParamArray(void);
    bool __fastcall    SetCurrentByUnit(int Index);
    bool __fastcall    InitCtrlParam(AnsiString aDllName, tSetMessage aSetMessage);
    bool __fastcall    PersInputInfo();
    int  __fastcall    GetParamCount();
    bool __fastcall    InitFlameList(tSetMessage aSetMessage);
    int  __fastcall    GetLastErrorCode();
    bool __fastcall    LoadMultiData(double * Data);
    //������� �� ����������� - �������� ���������� ������� (����� 3167)
    bool __fastcall    IsLoadingPoddon(void){return psw[MAX_OBJECT] & 0x80;};


    // ������� ������ �� Unit
    void __fastcall DeleteReference(int Index);
    void __fastcall ChangeType(int Index, Units_Type Old);
    void __fastcall ShowProfileStatus(void);// �������� �� ������� ������ ���� ���������: 2-������� �����, 1-���� �������, 0-���� ��������
    void __fastcall ShowAlarm(void);        // �������� �������� ���������� ��������
    void __fastcall RepareValue();          // �������� ���������
    void __fastcall AcceptPSW();            // ���������� �������� � ��������
    TDateTime __fastcall GetStartArmTime(); //-- ������ ������ ���������


    bool __fastcall ForEach(TProcType Type, void *AInfo = NULL);
    bool __fastcall LoadFromRegistryID();
    bool __fastcall ExecuteProfiles();
    //-- ���������� ���� ���������� ���������  ---------------------------------
    bool __fastcall MakeTermalProgramm();
    // ��������� �������
    bool __fastcall RecognizeProfile(sProfileParam *p);

    // ���������� ���� �� �������
    bool __fastcall ReleaseProfile(sProfileParam *p);

    bool __fastcall TestSameProfile(sProfileParam* p);
    bool __fastcall GenerateEvent(int EventId,int Source,char* Comment);
//    unsigned char * __fastcall GetCCW(int __index);
    unsigned char __fastcall GetCCW(int __index);

    bool __fastcall SetCCW();               // ������ ����� ���������� � ����������
    bool __fastcall GetPSW();               // ������ ����� ��������� ����������� ��������� � ���������
    bool __fastcall SetBCW();               // ������ ����� ���������� �������� �������� � ����������
    void __fastcall SetCCW(unsigned char v ,int __index);  // ������ ����� ���������� � ���������� 1�� ����.
    void __fastcall SetCCW_(unsigned char v ,int __index);

    //-- ��������/������ ���� ��������� ������� ��������
    BYTE __fastcall GetBurnSensorBits();
    void __fastcall SetBurnSensorBits(BYTE Abcw);
    void __fastcall iSetDateLastDB_Updating(int value);
    int  __fastcall iGetDateLastDB_Updating();
    void __fastcall iSetDateLastMagazining(int value);
    int  __fastcall iGetDateLastMagazining();


    //-- �������� ���� ������ --------------------------------------------------
    bool __fastcall OpenTables (void);

    //-- �������� ���� ������ --------------------------------------------------
    bool __fastcall CloseTables (void);
    //-- ��������� ����������� ���������� --------------------------------------
//    bool __fastcall ThermalProgrammControl(void);


    __property bool IsOven  = { read=GetIsOven };
    __property bool IsOvenEmpty = { read=GetIsOvenEmpty };                          //���� ��������� ����
    __property bool IsProfile = {read=GetIsProfile};   //���� ���� ���� ���� ���� c ��������
    __property bool IsTermalProfile = {read=GetIsTermProfile};   //���� ���� ���� ���� ���� c ����������� ����������
    __property double * Param = { read=GetParam, default=NULL };
    __property sProfileParam * Profile  = { read=GetProfile };
    __property sProfileParam * FirstProfile  = { read=GetFirstProfile };
    __property TDateTime       VisibleInterval  = { read=GetVisibleInterval, write=SetVisibleInterval };
    __property BYTE            BurnSensorBits   = { read=GetBurnSensorBits, write=SetBurnSensorBits };      //-- ���� ��������� ������� ��������
    __property int             ParamCount       = {read=GetParamCount,write=FDummy};   // ���������� ���������� � ������ ����������
    __property TDateTime       StartArmTime     = {read=GetStartArmTime, write=FStartArmTime};//-- ������ ������ ���������
                               // � ��������
    __property int             iDateLastDB_Updating = {read=iGetDateLastDB_Updating, write=iSetDateLastDB_Updating};
                               // � ����
    __property int             iDateLastMagazining  = {read=iGetDateLastMagazining,  write=iSetDateLastMagazining};  //-- ����� ���������� ���������� ����������
    __property bool            EnterInTimer = {read=FEnterInTimer,write=FEnterInTimer};// �������: ����� � ������ - ��������� ��������� ����


__published:
    // ��� ������� ������
    __property Classes::TNotifyEvent OnReqestData = {read=FOnReqestData, write=FOnReqestData};
    __property TUnitTypes Units = {read=FUnits, write=SetUnits, default=0};
    __property int Stat = {read=FStat, write=FStat, default=0};
    __property int UnitCount = {read=FFlameUnitCount,write=FDummy, default=0};

    __property TFlameUnit* FlameUnit1 = {read=GetUnit, write=SetUnit, index=0};
    __property TFlameUnit* FlameUnit2 = {read=GetUnit, write=SetUnit, index=1};
    __property TFlameUnit* FlameUnit3 = {read=GetUnit, write=SetUnit, index=2};
    __property TFlameUnit* FlameUnit4 = {read=GetUnit, write=SetUnit, index=3};
    __property TFlameUnit* FlameUnit5 = {read=GetUnit, write=SetUnit, index=4};
    __property TFlameUnit* FlameUnit6 = {read=GetUnit, write=SetUnit, index=5};
    __property int         Cnt        = { read=FFlameUnitCount, default=0 };
    __property int  CurrentUnitIndex  = { read=GetCurrentUnitIndex, write=SetCurrentUnitIndex };
    __property TFlameUnit* CurentFlameUnit  = { read=GetCurentFlameUnit, write=SetCurentFlameUnit };
    __property TEventMen * EventMeneger  = { read=FEventMeneger, write=FEventMeneger };
    __property TEventMen * JurnalWorkMeneger  = { read=FJurnalWorkMeneger, write=FJurnalWorkMeneger };
    __property TParamMen * ParamMenager  = { read=FParamMenager, write=SetParamMenager};
    __property eAccessMode AccessMode    = { read=FAccessMode, write=FAccessMode};

// ������������ ���������� �������� ����
//#define MAXUNIT     6
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

//-- end unit ----------------------------------------------------------------
#endif	// FlameListH




























