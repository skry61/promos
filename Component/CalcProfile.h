//---------------------------------------------------------------------------

#ifndef CalcProfH
#define CalcProfH

#include "Profile.h"
#include<alloc.h>
typedef long double  PProc(double);
// ��� ��������� �� ������� ��� ������ ������� ������� �������
typedef double*  TCalc_ProfFunc (sProfileParam *);


//���������� ��������� ����� �������
#define POINTS 50
#define MAXPOINTS 1000
// ���������� ������
#define STADYS 6
struct TStadyInf {
 double StadyTime;         // ����� ������ � �����
 double PokExp;            // ��������� ������� ���������� ����������double ProfileArray[MAXPOINTS];     // ������ ������� �������
	    		           // ��� ������ ������, ���� ������� ����.�����double TmpDouble;                   // ������� ������ ������� ������� ������
		   	               // ��������� Cgdouble StadyTimesArray[STADYS];     // ���������� ������
 double  StartCg;          // ������� ��������� �������� Cgdouble PokExp[STADYS];             // 
 double  EndCg;            // ������� �������� �������� Cg//-------------------   ������� ���������  -----------------------
 double  Bt;               // ������� �������� Bettadouble Const6      = 6.0;
 double  BoundCond;        // ������� ����� ���������� �������double Const_T_abs = 273;
 double  CgLow;            // ������� ����� ������ ��������� Cgdouble Const_1t98  = 1.98;
				           // 1-��������; 2-����double Const1t5    = 1.5;
 double  TStart;           // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;
 double  TEnd;             // �� ������� ������������� ����������� ������� ���������double Const_0t5   = 0.5;				        // ��� ������� ������double Const_min1  = -1.0;
 __fastcall  TStadyInf();  // ������������
};


//---------------------------------------------------------------------------
class TCalcProf 
{


  double dTemper;              // �������� ��������� ����������� ��� ������� ������
                               // ��/���

  double CurPokExp;            //������� ���������� ����������
                               // ��� ������ ������, ���� ������� ����.�����
                               // ��������� Cg
  double CurStartCg;           // ������� ��������� �������� Cg
  double CurEndCg;             // ������� �������� �������� Cg
  double CurBt;                // ������� �������� Betta
  int    CurBoundCond;         // ������� ����� ���������� �������
  int    CurCgLow;             // ������� ����� ������ ��������� Cg
                               // 1-��������; 2-����
  double CurT_Start;           // ������� �������� Betta
  double CurStadyTime;         // ����� ������� ������

  //---------------   ������� ���������� �������  ------------------
  double ExpForD;              // ������� �������� ���������� ��� ������� �������
                               // D0*(1.0+G*D)*exp(-E/RTp)
  double D0;                   // D0


  double *Block1,*Block2,*Block3;
  double* ProfileArray;
  int  PredArraySize = 0;      // ������ ������� ����������������� �� ���������� ����



public:
    void  CulcData(PProc P7,PProc P6,PProc P5,PProc P4,PProc P3,PProc P2,PProc P1,
                   double*PSumSteps, double* ProfData, int NPoints, double Xl, 
                   double lStep, int NSteps, int equ2);
    double*  NewCXT_Func (sProfileParam* profile);
    __fastcall TCalcProf(); 
    __fastcall ~TCalcProf();     
};

extern TCalcProf *CalcProf; 
extern TCalc_ProfFunc *Calc_ProfFunc; // ��������� �� ������� ��� ������ ������� ������� �������

#endif 
