//---------------------------------------------------------------------------

#ifndef MacroH
#define MacroH
#include <ExtCtrls.hpp>
#include "Robot.h"
class  TMacroView
{
		TEdit * evS;
		TComboBox *cbDir;

		//"[AVC]"
		TEdit * eV_AVC;
		TEdit * eJump;
		TCheckBox * cbAVCEnable;
		TRadioGroup *rgAVC_Axis;

		//"[Torch]"
		TEdit * elp;
		TEdit * elg;
		TEdit * etp;
		TEdit * etg;
		TCheckBox * cbImp_Enable;
		TEdit * eJOB;

		//"[Wire]"
		TEdit * evdg;
		TCheckBox * cbWire_enable;

		//"[Oscillation]");                                              // ����������
		TEdit * evpl;
		TEdit * etpl;
		TEdit * etpr;
		TEdit * espl;
		TCheckBox * cbOscEnable;

		AnsiString MacroPath;                               // ���� � �������� ��������

public:
		__fastcall TMacroView(TEdit * vS,TComboBox *Dir,TEdit * V_AVC,TEdit * Jump,TCheckBox * AVCEnable,TRadioGroup *RgAVC_Axis,
							  TEdit * lp,TEdit * lg,TEdit * tp,TEdit * tg,TCheckBox * Imp_Enable,
							  TEdit * vdg,TCheckBox * Wire_enable,
							  TEdit * vpl,TEdit * tpl,TEdit * tpr,TEdit * spl,TCheckBox * OscEnable,
							  TEdit * JOB
		);
		int	 __fastcall LoadViewFromFile(AnsiString FileName);
		int	 __fastcall LoadRobotFromFile(AnsiString FileName,TLineToPLC * pCurLineToPLC);
		int	 __fastcall SaveToFile(AnsiString FileName,AnsiString Comment);
		int	 __fastcall LoadMacroFromRobot(TLineToPLC * pCurLineToPLC);
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
