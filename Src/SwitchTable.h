//---------------------------------------------------------------------------

#ifndef SwitchTableH
#define SwitchTableH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TSwitchTableForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
private:	// User declarations
	bool   toSwitch1_2;
public:		// User declarations
	__fastcall TSwitchTableForm(TComponent* Owner);
	void __fastcall SetSwitch(int NewTableNumber);

};
//---------------------------------------------------------------------------
extern PACKAGE TSwitchTableForm *SwitchTableForm;
//---------------------------------------------------------------------------
#endif
