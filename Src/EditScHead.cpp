//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TScaleHead_cpp.h"
#include "EditScHead.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
/* {
	IPULSE_T,	// ������������ ��������� ���
	IGROUND_T,	// ������� ��������� ���
	U_T,		// ���������� ������
	U_W,		// ���������� ���������
	V WELD,		// �������� ������
	V WIRE,		// wire feed speed
	I MOT,		// ��� ��������� ����� ������ (mA).
	V STEP,		// �������� ���� (��/���).
	IM_T,		// ������� �������� ���������� ����(����� � �������)
	IM_W,		// ������� �������� ���� ���������(����� � �������)
	HEAT_INPUT,	// ������� ����� ����� ���/��
	GAS_FLOW,   // �������� ��������� ����
	TEMPERATUR, // �����������
	WIRE_DEPOS_RATE, // �������� ��������� ��������
	PILOT_GAS	// �� ������������.
};
	AnsiString Title;		// �������� ���������
	AnsiString Name;		// ��� ���������
	int Min_ScaleValue;		// ����� �������� �� �����
	int Min_RedValue;		// ����� �������� �� ����� ���������
	int Min_YellowValue; 	// ����� �������� �� ����� ��������������

	int Max_ScaleValue;		// ������ �������� �� �����
	int Max_RedValue;		// ������ �������� �� ����� ���������
	int Max_YellowValue; 	// ������ �������� �� ����� ��������������

*/


TParamVisio ParamsVisio[5] = {
/*IPULSE_T*/  	{"��� ������","I (A)",0,45, 50, 300, 350,400,NULL},
/*U_T*/  		{"���������� ������", "U (�)"   ,0,5,10,20,25,30,NULL},
/*IGROUND_T*/  	{"�������� ��������� ","V (�/���)"  ,0,1, 2, 8, 9,10,NULL},
/*U_T*/  		{"������ ����", "F(�/���)",0,5,8,20,23,25,NULL},
/*U_T*/  		{"��� ��������� ������ ���������", "I wire(A)",0,8,9,10,11,12,NULL}
};

TEditScaleHead *EditScaleHead;
//---------------------------------------------------------------------------
__fastcall TEditScaleHead::TEditScaleHead(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TEditScaleHead::BitBtn2Click(TObject *Sender)
{
	delete ScaleHead;
	Close();	
}
//---------------------------------------------------------------------------

void __fastcall TEditScaleHead::FormShow(TObject *Sender)
{
 //	Caption = "�������� " +ScaleHead->Title;
}
//---------------------------------------------------------------------------

void __fastcall TEditScaleHead::FormActivate(TObject *Sender)
{
	Caption = "�������� " +ScaleHead->Title;
	Edit1->Text = ScaleHead->ParamVisio->Min_RedValue;
	Edit2->Text = ScaleHead->ParamVisio->Min_YellowValue;
	Edit3->Text = ScaleHead->ParamVisio->Max_YellowValue;
	Edit4->Text = ScaleHead->ParamVisio->Max_RedValue;
}
//---------------------------------------------------------------------------

void __fastcall TEditScaleHead::BitBtn1Click(TObject *Sender)
{
	try{

		ScaleHead->SetLimit(Edit1->Text.ToDouble(),Edit2->Text.ToDouble(),Edit3->Text.ToDouble(),Edit4->Text.ToDouble());
		Close();
	}catch (...)
	{
		MessageBox(  Handle, "���������������� ����", "������ �����", MB_ICONINFORMATION);
	}


}
//---------------------------------------------------------------------------

void __fastcall TEditScaleHead::BitBtn3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

