//---------------------------------------------------------------------------


#pragma hdrstop
#include "Simulate_PLC.h"
#include "Robot.h"
#include "Macro.h"
#include "Option.h"
//#include "IniFiles.hpp"
//---------------------------------------------------------------------------
TErrLeval ErrLevalTable[] = {

	{plcer_ERR1,clBtnFace,NOBLINK,  "������ 1"},
	{plcer_ERR2,clYellow,NOBLINK,	"������ 2"},
	{plcer_ERR3,clYellow,BLINK,		"������ 3"},
	{plcer_ERR4,clRed,NOBLINK,		"������ 4"},
	{plcer_ERR5,clRed,BLINK,		"������ 5"},
	{plcer_ERR6,clBtnFace,NOBLINK,  "������ 6"},
	{LinkERR1,clRed,BLINK,"����� � ������������ ������ ��������"},
	{LinkERR2,clRed,BLINK,"����� � ������������ ���������� �������� ��������"}
};

//---------------------------------------------------------------------------
__fastcall TRobot::TRobot(TMacroView *aMacro)//:dStart(10000,0,10)								// �����������
{
	dD 		=  new TMaxMinDefault(30000,0,200,&CurLineToPLC.D);
	dStep 	=  new TMaxMinDefault(10000,-10000,10,&Step);
	dPred_D =  new TMaxMinDefault(30000,0,1000,&Pred_D);
	dAngle 	=  new TMaxMinDefault(3800,0,3600,&CurLineToPLC.Angle);

	Status = robREADY;
//	MacroPath = ".\\";
	ProgLineList = new TList();
	Macro = aMacro;
}
//---------------------------------------------------------------------------
__fastcall TRobot::~TRobot()//:dStart(10000,0,10)								// �����������
{
	delete dPred_D;
	delete dStep;
	delete dD;
	delete dAngle;

	ClearProg();			//������� ������� ������
	delete ProgLineList;
}

//---------------------------------------------------------------------------
//������� ������� ������
int TRobot::ClearProg()
{
	while (ProgLineList->Count) {
		TFullLine *NewFullLine = (TFullLine *)ProgLineList->Items[0];
		delete NewFullLine;
		ProgLineList->Delete(0);
	}
	ProgLineList->Clear();
	return true;
}

//---------------------------------------------------------------------------
int TRobot::AddLine(bool Fl_LastKadr)
{
	TFullLine *NewFullLine = new TFullLine();
	if(Fl_LastKadr)
		CurLineToPLC.Macro.Feed.LastKadr = 1;
	else
		CurLineToPLC.Macro.Feed.LastKadr = 0;
	memcpy(&NewFullLine->LineToPLC,&CurLineToPLC,sizeof(TLineToPLC));
	NewFullLine->MacroName = MacroName;
	NewFullLine->LineToPLC.Line = ProgLineList->Count+1;
	ProgLineList->Add(NewFullLine);
	return 1;
};
//---------------------------------------------------------------------------
int	TRobot::LoadMacroFromRobot(int Index,TMacroView *MacroView)
{
	if(Index == -1)   				//��������� �� �������� CurLineToPLC
	{
		return MacroView->LoadMacroFromRobot(&CurLineToPLC);
	}
	if(ProgLineList->Count > Index)
	{
		TFullLine *FullLine = (TFullLine *)ProgLineList->Items[Index];
		MacroName = FullLine->MacroName;
		return MacroView->LoadMacroFromRobot(&FullLine->LineToPLC);
	}else
		return 1;
}
//---------------------------------------------------------------------------
//CurLineToPLC
int	TRobot::LoadMacroFromFile(AnsiString FileName)
{
	MacroName = ExtractFileName(FileName);
//	MacroName = FileName;
	return Macro->LoadRobotFromFile(FileName,&CurLineToPLC);
}

//---------------------------------------------------------------------------
bool TRobot::Init(int Ln)
{
	ProsessIndex = Ln;
	return true;
}

//---------------------------------------------------------------------------
int	TRobot::GetLastError()			// ������ ������ � ������������ ��
{
	int Er = ErrorCode; ErrorCode = 0;
	return Er;
}
//---------------------------------------------------------------------------
int	TRobot::LoadMacro(AnsiString FileName)
{
	if (FileExists(/*MacroPath+*/FileName))
	{
//!!!!!!!!!!!!!!!!!!!!!    ���������
		return 0;		// ������� �������� ������
	} else              // ���� ����������
		return 1;

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
int TMaxMinDefault::toValue( AnsiString s)
{
	return s.ToDouble()*10;
}
//---------------------------------------------------------------------------
int TMaxMinDefault::GetValueFromStr( AnsiString s)
{
	try{
		int Val = toValue(s);
		if(Val > MaxValue)
		{
			*ValuePtr = MaxValue;
			return 2;
		}
		else if(Val < MinValue)
		{
			*ValuePtr = MinValue;
			return 3;
		}
		else
			*ValuePtr = Val;
	}catch (...){
		*ValuePtr = DefValue;
		return 1;
	}
	return 0;
}
//---------------------------------------------------------------------------
AnsiString TMaxMinDefault::GetValue()
{
	return  ((float)*ValuePtr)/10;
}
//---------------------------------------------------------------------------
double TMaxMinDefault::GetValueF()
{
	return  ((double)*ValuePtr)/10;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//int TMaxMinDefaultLayer::toValue( AnsiString s)
//{
//		return s.ToInt();
//}
//---------------------------------------------------------------------------
int TRobot::SetDiametr(AnsiString s)
{   int res = dD->GetValueFromStr(s);
	return res;
}

//---------------------------------------------------------------------------
// ������ ���� �������� ��������� � ���������� ��� ����������� �������
int TRobot::SetAngle(AnsiString s)
{   int res = dAngle->GetValueFromStr(s);
	return res;
}

//---------------------------------------------------------------------------
int TRobot::SetStepValue(AnsiString s,double SpindelAngle,AnsiString NextDiametr)
{
	int Result = dStep->GetValueFromStr(s);
	if((SpindelAngle < 0)&&(SpindelAngle > 90))
	{
		SpindelAngle = 90;
		if(Result==0)
			Result = 5;
	}
	double Rad = _GradToRad( SpindelAngle);
	if(dPred_D->GetValueFromStr(NextDiametr))
		if(Result==0)
			Result = 6;
	double D_Display = (dPred_D->GetValueF() - dD->GetValueF())/2;
	double StepF = dStep->GetValueF();
	if(Rad > 0.100)
	{
		// ��� ������, ����� ��������� ���� �� �������    (���������� ���������. ������ � ������)
		CurLineToPLC.StepVe = (cos(Rad) * StepF + sin(Rad) * D_Display)*10;
		CurLineToPLC.StepHe = (sin(Rad) * StepF - cos(Rad) * D_Display)*10;
	}else{
		// ��� ������, ����� ��������� ���� ����� ������� (�������� ���������. ������ �� ������)
		CurLineToPLC.StepVe = (cos(Rad) * StepF - sin(Rad) * D_Display)*10;
		CurLineToPLC.StepHe = (sin(Rad) * StepF + cos(Rad) * D_Display)*10;
	}
	return Result;
}
//---------------------------------------------------------------------------
AnsiString TRobot::GetDiametr()
{
	return dD->GetValue();
}

//---------------------------------------------------------------------------
// ������ ���� �������� ��������� � ���������� ��� ����������� �������
AnsiString TRobot::GetAngle()
{
	return dAngle->GetValue();
}

//---------------------------------------------------------------------------
AnsiString  TRobot::GetStepValue()
{
	return   dStep->GetValue();
}

//---------------------------------------------------------------------------
// ������� ��� ������ �� StringGrid
int TRobot::SetWeldControl(AnsiString s)
{
	if(s == "���")
		CurLineToPLC.WeldControl = 1;
	else if(s == "�����")
		CurLineToPLC.WeldControl = 2;
	else if(s == "��")
		CurLineToPLC.WeldControl = 0;
	else
	{
		CurLineToPLC.WeldControl = 0;
		return 1;                       // ������ - ���������� ������. ����������� �������� �� ��������� "��"
	}
	return 0;							// ������ ���
}


//---------------------------------------------------------------------------
//      �������������� � ������������
//---------------------------------------------------------------------------
bool TRobot::toSimul()
{
	return SimulPLC_Form;
}
//---------------------------------------------------------------------------
bool TRobot::toPLC()
{
	return ((SimulPLC_Form == NULL)||(SimulPLC_Form)&&(OptionForm->cbPLC_to_Simul->Checked));
}
//---------------------------------------------------------------------------
void TRobot::SetBuffer( TLineToPLC *pLoadBuf)
{

//	SetControlBufBusy();
	if(toPLC()) 	OptionForm->PLCDriver->SetBuffer(pLoadBuf);
	if(toSimul()) 	SimulPLC_Form->SetBuffer(pLoadBuf);
	SetControlBufBusy();
//	SetControlBufBusy();
}
//---------------------------------------------------------------------------
void TRobot::SetSysParam( sSys *SysPar)
{
	if(toPLC()) 	OptionForm->PLCDriver->SetSysParam(SysPar);
//	if(toSimul()) 	SimulPLC_Form->SetSysParam(SysPar);
}

//---------------------------------------------------------------------------
int TRobot::SendBufferToPLC(int *pProsIndex)
{
	try
	{
		if(ProgLineList->Count > ProsessIndex)
		{
			TLineToPLC LineToPLC;
			TFullLine *FullLine = (TFullLine *)ProgLineList->Items[ProsessIndex];
			memcpy( &LineToPLC, &FullLine->LineToPLC,sizeof(TLineToPLC));
			SetBuffer(&LineToPLC);
			ProsessIndex++;
			if(ProgLineList->Count == ProsessIndex)
			{
				SetControl_LasrKadr();
				return 1;
			}
			return 0;
		}else
			return 2;
	}
	__finally
	{
	   *pProsIndex = ProsessIndex;
	}
	return 0;
}
//---------------------------------------------------------------------------
int	TRobot::GetWorkBuffer(TLineToPLC *pLoadBuf)
{
	if(toPLC())		   return OptionForm->PLCDriver->GetWorkBuffer(pLoadBuf);
	else if(toSimul()) return SimulPLC_Form->GetWorkBuffer(pLoadBuf);
	return 0;
}

//---------------------------------------------------------------------------
int TRobot::DownLoadBuffer(TMacroView *MacroView)
{
	TLineToPLC LineToPLC;
	if(GetWorkBuffer(&LineToPLC) == 0)
	{
		int ProsessIndex = LineToPLC.Line-1;
		if(ProgLineList->Count > ProsessIndex)
		{
			TFullLine *FullLine = (TFullLine *)ProgLineList->Items[ProsessIndex];
			memcpy( &FullLine->LineToPLC,&LineToPLC, sizeof(TLineToPLC));
			MacroView->LoadMacroFromRobot(&LineToPLC);
			//int Res =
			MacroView->SaveToFile(FullLine->MacroName,NULL);
			ClearControlSaveParam();

			return 0;
		}else
			return 1;
	}
	return 2;

}

//---------------------------------------------------------------------------
TBitControlByte TRobot::GetProsControl()
{
	return SimulPLC_Form->GetProsControl();
}
//---------------------------------------------------------------------------
/*TBitStatusByte  TRobot::GetProsStatusBits()
{
	return SimulPLC_Form->GetProsStatusBits();
} */

//---------------------------------------------------------------------------
short  TRobot::GetProsStatus()
{
//	TBitStatusByte BitStatusByte = SimulPLC_Form->GetProsStatusBits();
	if(toPLC()) return OptionForm->PLCDriver->GetProsStatus();
	if(toSimul()) return SimulPLC_Form->GetProsStatus();
	return 0;
}
//---------------------------------------------------------------------------
void TRobot::SetControlBufBusy()     // 1� ����� �����
{
	if(toPLC()) 	OptionForm->PLCDriver->SetControlBufBusy();
	if(toSimul())	SimulPLC_Form->SetControlBufBusy();
}
//---------------------------------------------------------------------------
TBitStatusByte TRobot::GetProsStatusData()
{
	DWORD V;

	if(toPLC())
	{
		V = OptionForm->PLCDriver->GetProsStatusData();
//		BitStatusByteEx = (TBitStatusByteEx)OptionForm->PLCDriver->GetProsStatusDataEx();
//		if(toSimul())
	}
	else if(toSimul())
	{
		V = SimulPLC_Form->GetProsStatusData();
//		BitStatusByteEx = (TBitStatusByteEx)SimulPLC_Form->GetProsStatusDataEx();
	}
	CurLine = V>>16;
	return *(TBitStatusByte*)&V;
}
//---------------------------------------------------------------------------
// �������� ����������� ������ ��������� ��������
/*
TBitStatusByteEx TRobot::GetProsStatusDataEx()
{
	return BitStatusByteEx;
}
*/
//---------------------------------------------------------------------------
void TRobot::SetControlPC_Ready()     // �� �����
{
	if(toPLC()) 	OptionForm->PLCDriver->SetControlPC_Ready();
	if(toSimul())	SimulPLC_Form->SetControlPC_Ready();
}

//---------------------------------------------------------------------------
void TRobot::ReSetControlPC_Ready()     // �� �� �����
{
	if(toPLC()) 	OptionForm->PLCDriver->ReSetControlPC_Ready();
	if(toSimul())	SimulPLC_Form->ReSetControlPC_Ready();
}

//---------------------------------------------------------------------------
void TRobot::SetControl_LasrKadr()          //
{

	if(toPLC()) 	OptionForm->PLCDriver->SetControl_LasrKadr();
	if(toSimul())	SimulPLC_Form->SetControl_LasrKadr();
}
//---------------------------------------------------------------------------
void TRobot::ClearControlSaveParam()
{
	if(toPLC()) 	OptionForm->PLCDriver->ClearControlSaveParam();
	if(toSimul())	SimulPLC_Form->ClearControlSaveParam();
}

//---------------------------------------------------------------------------
void TRobot::SetControlPusk()          // ����
{
	SimulPLC_Form->SetControlPusk();
}
//---------------------------------------------------------------------------
void TRobot::SetControlStop()          // ����
{
	SimulPLC_Form->SetControlStop();
}
//---------------------------------------------------------------------------
void TRobot::SetControlReset()         // �����
{
	SimulPLC_Form->SetControlReset();
}
//---------------------------------------------------------------------------
void TRobot::SetControlPause()         // ������� �� ����� �� PC
{
	if(toPLC()) 	OptionForm->PLCDriver->SetControlPause();
	if(toSimul())	SimulPLC_Form->SetControlPause();
}
//---------------------------------------------------------------------------
void TRobot::SetControlPC_Err()        // �� �� ������
{
	if(toPLC()) 	OptionForm->PLCDriver->SetControlPC_Err();
	if(toSimul())	SimulPLC_Form->SetControlPC_Err();
}

//---------------------------------------------------------------------------
// �������� ������� �������
TPosStruct TRobot::GetPosition()             //
{
	if(toPLC())
		PosHVT =  OptionForm->PLCDriver->GetPosition();
	else
		PosHVT =  SimulPLC_Form->GetPosition();
	return PosHVT;
}

//---------------------------------------------------------------------------
// �������� ������� ���
double TRobot::Get_Cur_I()
{
	if(toPLC())
		return OptionForm->PLCDriver->Get_Cur_I();
	else if(toSimul())
		return SimulPLC_Form->Get_Cur_I();
	else return  0.1;
}

//---------------------------------------------------------------------------
// �������� ������� ����������
double TRobot::Get_Cur_B()
{
	if(toPLC())
		return OptionForm->PLCDriver->Get_Cur_B();
	else if(toSimul())
		return SimulPLC_Form->Get_Cur_B();
	else return  0.1;
}

//---------------------------------------------------------------------------
// �������� ������� �������� ���������
double TRobot::Get_Cur_V()
{
	if(toPLC())
		return OptionForm->PLCDriver->Get_Cur_V();
	else if(toSimul())
		return SimulPLC_Form->Get_Cur_V();
	else return  0.1;
}

//---------------------------------------------------------------------------
// �������� ������� ������ ����
double TRobot::Get_Cur_F()
{
	if(toPLC())
		return OptionForm->PLCDriver->Get_Cur_F();
	else if(toSimul())
		return SimulPLC_Form->Get_Cur_F();
	else return  0.1;
}

//---------------------------------------------------------------------------
short TRobot::GetError()               //
{
	if(toPLC())
		ErrorCode =  OptionForm->PLCDriver->GetErrorCode();
	else
		ErrorCode = SimulPLC_Form->GetErrorCode();
	return ErrorCode;
}
//---------------------------------------------------------------------------
TErrLeval * TRobot::GetErrorLevelAndText()               //
{
	for(int i=0; i < plcer_MAXERROR; i++)
		if(ErrorCode == ErrLevalTable[i].ErrCode)
			return &ErrLevalTable[i];
	return NULL;
}

//---------------------------------------------------------------------------
TErrLeval * TRobot::GetErrorLevelAndText(int ECode)               //
{
	for(int i=0; i < plcer_MAXERROR; i++)
		if(ECode == ErrLevalTable[i].ErrCode)
			return &ErrLevalTable[i];
	return NULL;
}

//---------------------------------------------------------------------------
int TRobot::GetStatusData()
{
	if(!GetProsStatusBitsDone)
	{
		BitStatusByte = GetProsStatusData();
		GetProsStatusBitsDone = true;
	}
	return 0;
}

//---------------------------------------------------------------------------
bool TRobot::isPusk()              // ����  - �������� ������     �205
{
	GetStatusData();
	return BitStatusByte.Pusk;
}
//---------------------------------------------------------------------------
bool TRobot::isBufBusy()            // PC : 1-����� ������ �����   �200
{
	GetStatusData();
	return BitStatusByte.BufBusy;
}
//---------------------------------------------------------------------------
bool TRobot::isGetControlPC_Err()            // PC : 1-����� ������ �����   �200
{
	GetStatusData();
	return BitStatusByte.PC_Err;
}

//---------------------------------------------------------------------------
bool TRobot::isStopWelding()        // ������ ����                 �201
{
	GetStatusData();
	return BitStatusByte.StopWelding;
}
//---------------------------------------------------------------------------
bool TRobot::isPause()              // ����� ���                   �207
{
	GetStatusData();
	return BitStatusByte.Pause;
}
//---------------------------------------------------------------------------
bool TRobot::isAlarmOn()            // ������ ���������            M208
{
	GetStatusData();
	return BitStatusByte.AlarmOn;
}
//---------------------------------------------------------------------------
bool TRobot::isSaveParam()          // ��������� ���������		 �210
{
	GetStatusData();
	return BitStatusByte.SaveParam;
}
//---------------------------------------------------------------------------
bool TRobot::isDataReady()          // ��������� ���������		 �210
{
	GetStatusData();
	return BitStatusByte.DataReady;
}

//---------------------------------------------------------------------------

/*
	WORD   BufBusy			:1;
	WORD   StopWelding		:1;
	WORD   SetNULL			:1;	// ��������					   �202
	WORD   ReadyForNextStep	:1;	// ����� � ���������� �������  �203
	WORD   LasrKadr			:1;	// PC : ��������� ����         �204
	WORD   Pusk				:1;
	WORD   ToPause			:1;	// ������� �� �����            �206
	WORD   Pause			:1;
	WORD   AlarmOn			:1;
	WORD   ToPausePC		:1;	// PC : ������� �� �����       �209
	WORD   SaveParam		:1;
*/
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

