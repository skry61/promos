//---------------------------------------------------------------------------

#ifndef RobotH
#define RobotH
#include "Service.h"
#include "SysPar.h"
//#include "Macro.h"

//---------------------- ���� ������ �����������-----------------------------
enum {
	plcer_NONE,
	plcer_ERR1,
	plcer_ERR2,
	plcer_ERR3,
	plcer_ERR4,
	plcer_ERR5,
	plcer_ERR6,
	LinkERR1,
	LinkERR2,
	plcer_MAXERROR

};
//---------------------- ������� �� ������ ����������� ----------------------
enum {
	SIMPLE,
	SIMPLE_YELLOW,
	BLINK_YELLOW,
	SIMPLE_RED,
	BLINK_RED
};
#define BLINK  		true
#define NOBLINK  	false
//---------------------------------------------------------------------------
enum {
	robNONE,
	robJOG,
	robNOT_READY,
	robREADY,
	robWAIT_PROGRAMM,		// �������� �������� ���������
	robINIT,
	robSTARTING,
	robSTART,
	robSTOP,
	robPAUSING,
	robPAUSE,
	robFINISHING,
	robFINISH,
//	robToERROR,
	robERROR
};

//---------------------------------------------------------------------------
enum {
	MANUAL,
	WELDING,
	STEP,
	AVC,
	STEP_OSC
};
//---------------------------------------------------------------------------
typedef struct sErrLeval
{
	short ErrCode;
	TColor Color;
	bool Blink;
	char * ErrText;
}TErrLeval;

//---------------------------------------------------------------------------
//--------------------------  ����������� ����  -----------------------------
//---------------------------------------------------------------------------
// ��������� ������
struct sFeed
{
	U16	 vS;			 // �������� ������				     [ 0.1 ��/��� ]
	U16	 DirTable     :1;// ����������� �������� ����� 	  	   [ 0 - �� �������, 1- ������]
	U16	 Enable_AVC   :1;// ���� ���������             	    [ 0 - �� ��������,1- ���.  ]
	U16	 Enable_Wire  :1;// ��������� ���������                [ 0 - �� ��������,1- ���.  ]
	U16  Enable_Oscill:1;// ���������� ���������               [ 0 - �� ��������,1- ���.  ]
	U16  Enable_ImpCur:1;// ���������� ���  ���������
	U16  LastKadr     :1;// ��������� ����
	U16  Dammy        :1;// ProcTiG_MiG  :1;// ��� �������� �iG/MiG
	U16  AVC_H_V	  :1;// AVC �� ��� �������������� (0)/ ������������ (1)
	U16  JOB_Number	  :8;// ����� JOB 0-255
};                       // 2*2 - 4 �����
//---------------------------------------------------------------------------
// ��������� �������
struct sTorch
{
	short lp;			// ������������ ��������� ��� ( ��� TiG)[0.0 �] ���  ���������� ��������� ������ ( ��� MiG) [ 0.0 B   ]
	U16	  lg;			// ������� ��������� ���  ( ��� TiG)    [0.0 �]
	U16	  tp;			// ������������ ������������� ���������� ����   [0.0 ��  ]
	U16	  tg;			// ������������ �������� ���������� ����   [ 0.0 ��  ]
};                      // 4*2 - 8 ����
//---------------------------------------------------------------------------
// ��������� ���������
struct sWire
{
	U16	 vdg;			// �������� ������ ��������� �� ����� ���� �������� ���������� ����		[ 0.1 ��/��� ]						// 8*2 - 16 ����
};                      // 1*2 - 2 ����

//---------------------------------------------------------------------------
// ��������� ����������
struct sOscill
{
	U16  vpl; 			// �������� ����������    (��� TiG)  					    [ 0.1 ��/��� ]
	U16	 tpl;			// ����� �������� ���������� � ����� ������� ���������     [ 0.1 ��� ]
	U16	 tpr;			// ����� �������� ���������� � ������ ������� ���������    [ 0.1 ��� ]
	U16	 spl; 			// ��������� ���������� �� ������� ����� �� ����� ��� ������ ���������� ����� [ 0.1  �� ]
						// 4*2 - 8 ����
};
//---------------------------------------------------------------------------
// ��������� AVC       (��� TiG)
struct sAVC
{
	U16  V_AVC;		 	// ���������� ��������          							 [ 0.1 � ]
	U16	 Jump;			// �������� ��������									   [ 0.1 �� ]
						// 2*2 - 4 ����
};
//---------------------------------------------------------------------------
// �������
struct sMacro
{
	struct sFeed 	Feed;
	struct sTorch 	Torch;
	struct sWire  	Wire;
	struct sOscill 	Oscill;
	struct sAVC		AVC;
						// (2 + 4 + 1 + 4 + 2) * 2 = 26 �����, 13 ����.
};

//---------------------------------------------------------------------------
// ������ ����� � ����������
typedef struct sLineToPLC
{
	U16  			Line;		// ������� ����� ������ ���������
	struct sMacro 	Macro;		// �������� �������  (26�����, 13 ����)
	short  			D;			// ������� (0.1 mm)
	short  			StepVb;		// ��� �� ��������� � ������ �����(0.1 mm)
	short  			StepHb;		// ��� �� ����������� � ������ �����(0.1 mm)
	short  			StepVe;		// ��� �� ��������� � ����� �����(0.1 mm)
	short  			StepHe;		// ��� �� ����������� � ����� �����(0.1 mm)
	short  			Angle;		// ���� �������� ��� ������ ���������
	short  			WeldControl;// 0 - ������ ��������, 1 - ������ ���������, 2 - �����
								// (1+13+7) * 2 = 42 ����
								// 38 ���� + WeldControl = 42 ����� (21 �����)
} TLineToPLC;

//---------------------------------------------------------------------------
// ������ ����� ������
typedef struct sFullLine
{
	TLineToPLC LineToPLC;		// (1+13+6) * 2 = 40 ����
	AnsiString MacroName;		// ��� ����� �������
} TFullLine;

//---------------------------------------------------------------------------
/*
// ������� ��������� ���������� � ����������
typedef struct {
	WORD   BufBusy_0	:1;	// 1� ����� �����
	WORD   BufBusy_1	:1;	// 2� ����� �����
	WORD   BufFill_Ptr	:1;	// ��������� ������ ����������
	WORD   Pusk			:1;	// ����
	WORD   Stop			:1;	// ����
	WORD   Reset		:1;	// �����
	WORD   Dummy_0		:1;	//
	WORD   Dummy_1		:1;	//
}TBitControlByte;
//---------------------------------------------------------------------------
// ������� ��������� ���������� � ����������
typedef struct {
	WORD   KadrEnd		:1;	// ���� ���������
	WORD   LastKadrLoad	:1;	// ��������� ���� �������
	WORD   WeldingEnd	:1;	// ������ ���������
	WORD   KadrInWork	:1;	// ���� ��������������
	WORD   Dummy_0	 	:1;	//
	WORD   MacroEdit	:1;	// �����. ��������.
	WORD   Dummy_1		:1;	//
	WORD   Ready		:1;	// �����/ ������
}TBitStatusByte;
*/
//---------------------------------------------------------------------------
//      ����� ���������� �������
//---------------------------------------------------------------------------
// ������� ��������� ���������� � ����������
typedef struct {
	WORD   BufBusy			:1;	// PC : 1-����� ������ �����   �200
	WORD   StopWelding		:1;	// ������ ����                 �201
	WORD   DriveReady		:1;	// ������� ������  		    �202
	WORD   DataReady		:1;	// ������ ������               �203
	WORD   LasrKadr			:1;	// PC : ��������� ����         �204
	WORD   Pusk				:1;	// ����  - �������� ������     �205
	WORD   ToPause			:1;	// ������� �� �����            �206
	WORD   Pause			:1;	// ����� ���                   �207
	WORD   AlarmOn			:1;	// ������ ���������            M208
	WORD   ToPausePC		:1;	// PC : ������� �� �����       �209
	WORD   SaveParam		:1;	// ��������� ���������		 �210
	WORD   PC_Ready			:1;	// �� �����                    �211
	WORD   WeldReady		:1;	// ������ ������    		   �212
	WORD   I_not_0			:1;	// ��� ������ 0                �213
	WORD   PC_Err			:1;	// �� ���������� ������        �214
	WORD   Weld_Err			:1;	// �� ��������� ���.������     �215
}TBitControlByte;
//---------------------------------------------------------------------------
// ������� ��������� ���������� � ����������
typedef struct {
	WORD   BufBusy			:1;	// PC : 1-����� ������ �����   �200
	WORD   StopWelding		:1;	// ������ ����                 �201
	WORD   DriveReady		:1;	// ������� ������  		    �202
	WORD   DataReady		:1;	// ������ ������               �203
	WORD   LasrKadr			:1;	// PC : ��������� ����         �204
	WORD   Pusk				:1;	// ����  - �������� ������     �205
	WORD   ToPause			:1;	// ������� �� �����            �206
	WORD   Pause			:1;	// ����� ���                   �207
	WORD   AlarmOn			:1;	// ������ ���������            M208
	WORD   ToPausePC		:1;	// PC : ������� �� �����       �209
	WORD   SaveParam		:1;	// ��������� ���������		 �210
	WORD   PC_Ready			:1;	// �� �����                    �211
	WORD   WeldReady		:1;	// ������ ������    		   �212
	WORD   I_not_0			:1;	// ��� ������ 0                �213
	WORD   PC_Err			:1;	// �� ���������� ������        �214
	WORD   Weld_Err			:1;	// �� ��������� ���.������     �215
}TBitStatusByte;

//---------------------------------------------------------------------------
// ��������� ������� � �����������
typedef struct {
	short  Lay;         // ����
	short  Horizontal;
	short  Vertical;
	short  TurnTable;
}TPosStruct;
//---------------------------------------------------------------------------
// �������� ������
class TMaxMinDefault
{
	short   MaxValue;	// ����������� ���������� ��������
	short   MinValue;	// ���������� ���������� ��������
	short   DefValue;	// �������� �� ���������
	short  *ValuePtr;
public:
	TMaxMinDefault(short Ma, short Mi, short De,short *aValuePtr){MaxValue = Ma;MinValue = Mi;DefValue=De;ValuePtr = aValuePtr;*ValuePtr = De;}
	virtual int toValue(AnsiString s);
	AnsiString GetValue();
	double GetValueF();
	int GetValueFromStr(AnsiString s);
};

//---------------------------------------------------------------------------
// �������� ������
//class TMaxMinDefaultLayer : public TMaxMinDefault
//{
//public:
//	TMaxMinDefaultLayer(short Ma, short Mi, short De,short *aValuePtr):TMaxMinDefault( Ma, Mi, De,aValuePtr){};
//	virtual int toValue(AnsiString s);
//};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TMacroView;

// ����� ������
class TRobot {
	int 	Value;  									// ������� ��������
	int 	ErrorCode;                                  // ��� ������
	TList  *ProgLineList;                               // ����� � ������
	TFullLine FullLine;									// ������� ���� � ������ �������
	TLineToPLC CurLineToPLC;                            // ������� ����
	TMaxMinDefault *dD,*dStep,*dPred_D,*dAngle;
	short Step;                                          // �������� ����������������(�� ����������� �� ����)
	short Pred_D;                                        // ���������� �������� ��������
	TPosStruct PosHVT;                                  // ��������� ������� �� ���� ����
	TMacroView *Macro;
	AnsiString MacroName;
	int    	ProsessIndex;								// ������ ����� ��������
	TBitStatusByte BitStatusByte;                       // ����� ��������� �������
public:
	__fastcall TRobot(TMacroView *aMacro); 				// �����������
	__fastcall ~TRobot();								// ����������
	int		Status;
	BYTE    CurLine;									// ������� ������
	bool	GetProsStatusBitsDone;						// ������ �� PLC �������
	bool	Init(int Ln);
	int		GetLastError();
//	TRobot(){Status = robREADY;}
	int 	LoadMacroFromFile(AnsiString FileName);       // ��������� ����� �� �����
	int		LoadMacro(AnsiString FileName);
	int		LoadMacroFromRobot(int Index,TMacroView *MacroView);
	AnsiString GetMacroName(){return MacroName;}

	int 	ClearProg();
	int 	AddLine(bool Fl_LastKadr);
	int     SetDiametr(AnsiString s);
	int     SetAngle(AnsiString s);
	int     SetStepValue(AnsiString s,double SpindelAngle,AnsiString NextDiametr);
	int     SetWeldControl(AnsiString s);
//---------------------------------------------------------------------------
//      �������������� � ������������
//---------------------------------------------------------------------------
	void SetBuffer(TLineToPLC *pLoadBuf);
	int  SendBufferToPLC(int *pProsIndex);
	void SetSysParam(sSys *SysPar);
	int	GetWorkBuffer(TLineToPLC *pLoadBuf);
	int DownLoadBuffer(TMacroView *MacroView);
	TBitControlByte GetProsControl();
//	TBitStatusByte  GetProsStatusBits();
	TBitStatusByte GetProsStatusData();
//	TBitStatusByteEx GetProsStatusDataEx();
	int GetStatusData();
	AnsiString GetDiametr();
	AnsiString GetAngle();
	AnsiString GetStepValue();

	short  GetProsStatus();

	void SetControlBufBusy();     	// 1� ����� �����
//	void SetControlBufBusy_1();     // 2� ����� �����
//	void SetControlBufFill_Ptr();   // ��������� ������ ����������
	void SetControlPusk();          // ����
	void SetControlStop();          // ����
	void SetControlReset();         // �����
	void SetControlPause();         // �����
	void SetControl_LasrKadr();     // ��������� ����
	void SetControlPC_Ready();      // �� �����
	void ReSetControlPC_Ready();    // �� �� �����
	void SetControlPC_Err();        // �� �� �����a
	void ClearControlSaveParam();



	TPosStruct GetPosition();
	double Get_Cur_I();             // �������� ������� ���
	double Get_Cur_B();             // �������� ������� ����������
	double Get_Cur_V();             // �������� ������� �������� ���������
	double Get_Cur_F();             // �������� ������� ������ ����


	short  GetError();
	TErrLeval *GetErrorLevelAndText();               //
	TErrLeval *GetErrorLevelAndText(int ECode);               //
	bool isPusk();               	// ����  - �������� ������     �205
	bool isBufBusy();            	// PC : 1-����� ������ �����   �200
	bool isStopWelding();        	// ������ ����                 �201

	bool isDataReady();          	// ������ ������        	   �203
	bool isPause();              	// ����� ���                   �207
	bool isAlarmOn();            	// ������ ���������            M208
	bool isSaveParam();          	// ��������� ���������		 �210
	bool isGetControlPC_Err();		// �� ���������� ������        �214
	void Clear_GetProsStatusBitsDone(){GetProsStatusBitsDone = false;}
//----------------
	bool toSimul();
	bool toPLC();
};

//---------------------------------------------------------------------------
#endif


