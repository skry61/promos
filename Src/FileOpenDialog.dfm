object OpenFileForm: TOpenFileForm
  Left = 0
  Top = 0
  Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
  ClientHeight = 205
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ValueListEditor1: TValueListEditor
    Left = 5
    Top = -2
    Width = 841
    Height = 153
    KeyOptions = [keyEdit, keyAdd, keyUnique]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    Strings.Strings = (
      '='
      '')
    TabOrder = 0
    TitleCaptions.Strings = (
      ' '#1052#1072#1082#1088#1086
      ' '#1054#1087#1080#1089#1072#1085#1080#1077)
    OnSelectCell = ValueListEditor1SelectCell
    ColWidths = (
      296
      539)
  end
  object Button1: TButton
    Left = 256
    Top = 160
    Width = 128
    Height = 36
    Caption = #1042#1099#1073#1088#1072#1090#1100
    ModalResult = 1
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 400
    Top = 160
    Width = 128
    Height = 36
    Caption = #1054#1090#1082#1072#1079
    ModalResult = 2
    TabOrder = 2
    OnClick = Button1Click
  end
end
