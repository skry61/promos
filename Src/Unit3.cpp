//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}

#ifdef XXXX
//---------------------------------------------------------------------------
void __fastcall TMainForm::Timer1Timer(TObject *Sender)
{
	TDateTime dtTmp = Now();
	TPosStruct PosHVT;
	int sRes;
	int LastLine;

	TimeTickCount++;
	Edit35->Text = OptionForm->PLCDriver->GetSliceCount();
	Edit36->Text = TimeTickCount;

	StatusBar1->Panels->Items[1]->Text = dtTmp.TimeString();


	int RobotStat = Robot->GetProsStatus();
	if(RobotStat != robERROR)
	{
		PanelProsessStatus->Color = clBtnFace;
		switch(SystemStatus)
		{
		case sysINIT          :                  	//������� ��������� �� ������� ���������� ���������

					switch(RobotStat)
					{
					case robNOT_READY     :
						PanelProsessStatus->Caption = "�� �����";
						break;

					case robREADY         :
						PanelProsessStatus->Caption = "�����";
						break;

					case robSTARTING : 		// �������� �������� ���������
						PanelProsessStatus->Caption = "��������� �������� ���������";
						SystemStatus = sysACTIVATION_WAIT;
						bbToLoadProg->Perform(WM_LBUTTONDOWN, 0, 0);
						bbToLoadProg->Perform(WM_LBUTTONUP, 0, 0);
						break;

					case robSTARTING      :
						StartTime = Now();		// �������� ������
						PanelProsessStatus->Caption = "����� �������";
						break;

					case robSTART         :
				//		Fl_START_Acivated = true;
						PosHVT = Robot->GetPosition();
						ProsessStatusLine->SetParam("",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
						break;

					case robPAUSING       :
				//		Fl_START_Acivated = true;
						PosHVT = Robot->GetPosition();
						ProsessStatusLine->SetParam("�� �����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
						break;

					case robPAUSE         :
				//		Fl_START_Acivated = true;
						PosHVT = Robot->GetPosition();
						ProsessStatusLine->SetParam("�����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
						break;

					case robFINISHING     :
				//		Fl_START_Acivated = true;
						PosHVT = Robot->GetPosition();
						ProsessStatusLine->SetParam("���������� ������ ",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
						break;

					case robFINISH        :
						Fl_START_Acivated = false;
						SystemStatus = sysEND_PROG;			// ������� �� ������� �� ������� ���������� ���������
						PanelProsessStatus->Caption = "������ ���������. ������������ �������� : " +  ProsessStatusLine->GetRunTime().TimeString();
						break;

					case robERROR         :
						Fl_START_Acivated = false;
						short Er = Robot->GetError();
						if(PlcError != Er)
						{
							ErrLevalPtr = Robot->GetErrorLevelAndText();
							if(ErrLevalPtr != NULL)
							{
								PlcError = Er;
								PanelProsessStatus->Caption = AnsiString(ErrLevalPtr->ErrText);
								PanelProsessStatus->Color   = ErrLevalPtr->Color;
							}
						}
						if(ErrLevalPtr != NULL)
						if(ErrLevalPtr->Blink == BLINK)
							if(PanelProsessStatus->Color == ErrLevalPtr->Color)
								PanelProsessStatus->Color   = clBtnFace;
							else
								PanelProsessStatus->Color   = ErrLevalPtr->Color;
					}
		case sysACTIVATION_WAIT          :                  	//
			break;

		case sysToACTIVATED              :
			Robot->Init();
			SystemStatus = sysACTIVATED;

		case sysACTIVATED                :                  	//
			if(!Robot->isBufBusy())
			{
				sRes = Robot->SendBufferToPLC(&LastLine);
/*
				if ((LastLine != Line)&& (Line < StringGrid1->RowCount))
				{
					Line = LastLine;
					if(Line > 5)
						StringGrid1->TopRow = 1+ Line-5;
					StringGrid1->Refresh();
					sRes = Robot->LoadMacroFromRobot(Line-1,LookMacroView);
					Caption = "������ 1.00   ���������: " +LabelFileName->Caption +"       �����c:"+ Robot->GetMacroName();

				}
*/
//				if(sRes)						// ��������� ����
//					Robot->SetControl_LasrKadr();
			}else  if(Robot->isSaveParam())
			{
				Robot->DownLoadBuffer(LookMacroView);
			}
			//���� �������� ����� ������ �� ������ - ��������� ��� � USINTX11
			if ((Robot->CurLine != 0)&&(Robot->CurLine != Line)&& (Robot->CurLine < StringGrid1->RowCount))
			{
				Line = Robot->CurLine;
				if(Line > 5)
					StringGrid1->TopRow = 1+ Line-5;
				StringGrid1->Refresh();
				sRes = Robot->LoadMacroFromRobot(Line-1,LookMacroView);
//				sRes = BUSintX11->LoadMacroFromRobot(Line-1,LookMacroView);
				Caption = "������ 1.00   ���������: " +LabelFileName->Caption +"       �����c:"+ Robot->GetMacroName();

			}
 //			if(Robot->isPusk())	;
//			break;

			switch(RobotStat)
			{
			case robSTARTING      :
				StartTime = Now();		// �������� ������
				PanelProsessStatus->Caption = "����� �������";
				break;

			case robSTART         :
				PosHVT = Robot->GetPosition();
				ProsessStatusLine->SetParam("",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
				break;

			case robPAUSING       :
				PosHVT = Robot->GetPosition();
				ProsessStatusLine->SetParam("�� �����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
				break;

			case robPAUSE         :
				PosHVT = Robot->GetPosition();
				ProsessStatusLine->SetParam("�����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
				break;

			case robFINISHING     :
				PosHVT = Robot->GetPosition();
				ProsessStatusLine->SetParam("���������� ������ ",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
				break;

			case robFINISH        :
				Fl_START_Acivated = false;
				SystemStatus = sysEND_PROG;			// ������� �� ������� �� ������� ���������� ���������
				ProgrammRegim = NO_REGIM;                	//
				PanelProsessStatus->Caption = "������ ���������. ������������ �������� : " +  ProsessStatusLine->GetRunTime().TimeString();
				bbEditProgActivate->Visible = true;
				Fl_CanRepeat = true;
				break;
			}
			break;

		case sysEND_PROG                :
//			ProgrammRegim = NO_REGIM;                	//
			break;

		}
	}else  // � ������ ������     	case robERROR         :
	{
		Fl_START_Acivated = false;
		short Er = Robot->GetError();
		if(PlcError != Er)
		{
			ErrLevalPtr = Robot->GetErrorLevelAndText();
			if(ErrLevalPtr != NULL)
			{
				PlcError = Er;
				PanelProsessStatus->Caption = AnsiString(ErrLevalPtr->ErrText);
				PanelProsessStatus->Color   = ErrLevalPtr->Color;
			}
		}
		if(ErrLevalPtr != NULL)
		if(ErrLevalPtr->Blink == BLINK)
			if(PanelProsessStatus->Color == ErrLevalPtr->Color)
				PanelProsessStatus->Color   = clBtnFace;
			else
				PanelProsessStatus->Color   = ErrLevalPtr->Color;
	}
	Robot->Clear_GetProsStatusBitsDone();
/*
	if(Fl_START_Acivated)
	{
		ProsessStatusLine->SetParam(Line,1, 1234,2345, StatrTime);
	}else
		ProsessStatusLine->ReSetActivity(RobotStat, Er, AnsiString Text)

		ProsessStatusLine->ReSetActivity(RobotStat);
*/

}
#endif


