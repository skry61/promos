//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Grids.hpp>
#include <Dialogs.hpp>
//#include "Fron_Grapher.h"
#include "TScaleHead_cpp.h"
#include "EditScHead.h"
#include "TWorkShape.h"
#include "Service.h"
#include "ProsessStatusLine.h"
#include "Robot.h"
#include "Macro.h"
#include "SysPar.h"
#include <jpeg.hpp>
#include <ImgList.hpp>
#include <CheckLst.hpp>
#include <ToolWin.hpp>

//---------------------------------------------------------------------------
// ����� ������� �����  
namespace SG_ComboBoxes
{
	class TStringGrid : public Grids::TStringGrid
    {
	private:
		TStringList *slMacro;		// ������ ������ �����
		__fastcall void GetCbxItems(int ACol, int ARow, TStrings *Items)
		{
			if(ACol == 1)
			{
				Items->Clear();
				for(int i=0;i< slMacro->Count;i++)
					Items->Add(slMacro->Strings[i]);
			}
			if(ACol == 5)
			{
				Items->Clear();
				Items->Add("��");
				Items->Add("���");
				Items->Add("�����");
			}

        }
	protected:
        virtual TInplaceEdit* __fastcall CreateEditor(void)
        {
            TInplaceEditList *inplace = new TInplaceEditList(this);
            inplace->DropDownRows = 5;
            inplace->OnGetPickListitems = GetCbxItems;
            return inplace;
        }
        DYNAMIC TEditStyle __fastcall GetEditStyle(int ACol, int ARow)
		{
 //			FocusCell (0, 0, false);

			if((ACol == 1)||(ACol == 5))
				return esPickList;
			else
				return esSimple;
		}
/*
		virtual bool __fastcall SelectCell(int ACol, int ARow)
		{
			return ((ACol == 1) ||(ACol == 7)); // ��������� ����� ��������� � ������� ����� 1 � 7
		}
*/
	public:
		void MakeNewMacroList(AnsiString wpName){
			if(slMacro) {slMacro->Clear() ;delete slMacro;}
			slMacro = GetFileList(wpName,"mcr");
		}

		__fastcall virtual TStringGrid(Classes::TComponent* AOwner)
			: Grids::TStringGrid(AOwner)
		{
			slMacro = NULL;
			MakeNewMacroList("\\Program");
		}
		__fastcall virtual ~TStringGrid()
		{
//			Grids::~TStringGrid();
			delete slMacro;
		}
	};
}
#define TStringGrid SG_ComboBoxes::TStringGrid

	class TWorkShape;

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *PanelProsessStatus;
	TStatusBar *StatusBar1;
	TPageControl *PageControl1;
	TTabSheet *ts1_Main;
	TTabSheet *ts2_Prog;
	TTabSheet *ts3_Macro;
	TTabSheet *ts4_SysPar;
	TBitBtn *bbToEditProg;
	TSpeedButton *sbToEditMacro;
	TSpeedButton *bbToLoadProg;
	TStaticText *StaticText8;
	TStaticText *StaticText9;
	TStaticText *StaticText10;
	TImage *Image2;
	TStringGrid *StringGrid1;
	TBitBtn *bbEditProgLoad;
	TBitBtn *bbEditProgSave;
	TBitBtn *bbEditProgNew;
	TBitBtn *bbEditLinePl;
	TBitBtn *bbEditLineMin;
	TBitBtn *bbEditPagePl;
	TBitBtn *bbEditPageMin;
	TBitBtn *bbEditPrint;
	TBitBtn *bbEditProgExit;
	TLabel *Label1;
	TBitBtn *bbEditProgActivate;
	TBitBtn *bbEditProgDelLine;
	TBitBtn *bbEditNewLine;
	TBitBtn *bbEditPast;
	TBitBtn *bbEditRenumerate;
	TBitBtn *bbDemonstrateProg;
	TBitBtn *bbEditVisio;
	TOpenDialog *OpenDialog;
	TSaveDialog *SaveDialog;
	TGroupBox *gbFeed;
	TGroupBox *gbTorch;
	TGroupBox *gbWire;
	TGroupBox *GroupBox1;
	TBitBtn *BitBtn1;
	TBitBtn *bbEditMacroLoad;
	TBitBtn *bbEditMacroSave;
	TBitBtn *BitBtn6;
	TBitBtn *bbEditMacroNew;
	TBitBtn *BitBtn22;
	TBitBtn *BitBtn23;
	TEdit *evS;
	TStaticText *StaticText21;
	TStaticText *StaticText22;
	TStaticText *StaticText26;
	TStaticText *StaticText33;
	TEdit *evdg;
	TStaticText *StaticText34;
	TStaticText *StaticText37;
	TEdit *evpl;
	TStaticText *StaticText38;
	TStaticText *StaticText41;
	TEdit *etpl;
	TStaticText *StaticText42;
	TStaticText *StaticText43;
	TEdit *etpr;
	TStaticText *StaticText44;
	TStaticText *StaticText45;
	TEdit *espl;
	TStaticText *StaticText46;
	TStaticText *StaticText47;
	TEdit *elp;
	TStaticText *StaticText48;
	TStaticText *StaticText49;
	TEdit *elg;
	TStaticText *StaticText50;
	TStaticText *StaticText51;
	TEdit *etg;
	TStaticText *StaticText52;
	TStaticText *StaticText54;
	TEdit *etp;
	TComboBox *cbDir;
	TStaticText *StaticText68;
	TGroupBox *GroupBox6;
	TCheckBox *cbAVCEnable;
	TBevel *Bevel3;
	TStaticText *stHints;
	TCheckBox *cbWire_enable;
	TCheckBox *cbOscEnable;
	TTabSheet *ts5_Visio;
	TTabSheet *ts6_MINMAX_Edit;
	TTabSheet *ts7_Massenger;
	TGroupBox *GroupBox8;
	TGroupBox *GroupBox9;
	TGroupBox *GroupBox11;
	TGroupBox *GroupBox13;
	TGroupBox *GroupBox14;
	TBevel *Bevel4;
	TStaticText *StaticText83;
	TStaticText *StaticText84;
	TEdit *Edit6;
	TStaticText *StaticText85;
	TEdit *Edit7;
	TStaticText *StaticText87;
	TStaticText *StaticText107;
	TStaticText *StaticText108;
	TEdit *Edit18;
	TStaticText *StaticText121;
	TEdit *Edit25;
	TStaticText *StaticText122;
	TEdit *Edit28;
	TStaticText *StaticText128;
	TStaticText *StaticText129;
	TStaticText *StaticText131;
	TStaticText *StaticText132;
	TEdit *Edit30;
	TEdit *Edit33;
	TStaticText *StaticText138;
	TBitBtn *BitBtn4;
	TBitBtn *BitBtn13;
	TBitBtn *BitBtn17;
	TGroupBox *GroupBox10;
	TBevel *Bevel5;
	TStaticText *StaticText96;
	TGroupBox *GroupBox15;
	TBevel *Bevel6;
	TStaticText *StaticText97;
	TButton *Button1;
	TLabel *Label3;
	TButton *Button2;
	TStaticText *StaticText98;
	TBevel *Bevel7;
	TLabel *Label4;
	TButton *Button3;
	TStaticText *StaticText99;
	TBevel *Bevel8;
	TLabel *Label5;
	TGroupBox *GroupBox16;
	TBevel *Bevel9;
	TStaticText *StaticText139;
	TBitBtn *BitBtn12;
	TBitBtn *BitBtn18;
	TBitBtn *BitBtn19;
	TBitBtn *BitBtn20;
	TBitBtn *BitBtn21;
	TBitBtn *BitBtn24;
	TBitBtn *BitBtn25;
	TBitBtn *BitBtn29;
	TBitBtn *BitBtn31;
	TBitBtn *bbClearMessenger;
	TBitBtn *BitBtn36;
	TBitBtn *BitBtn37;
	TTimer *Timer1;
	TBitBtn *bbPAUSE;
	TBitBtn *bbToEdit;
	TBitBtn *bbResetRobot;
	TEdit *eV_AVC;
	TLabel *Label6;
	TStaticText *StaticText69;
	TEdit *Edit1;
	TStaticText *StaticText75;
	TLabel *Label7;
	TBitBtn *BitBtn11;
	TEdit *eJump;
	TLabel *Label8;
	TStaticText *StaticText2;
	TStaticText *StaticText3;
	TGroupBox *GroupBox12;
	TStaticText *StaticText53;
	TEdit *Edit16;
	TStaticText *StaticText55;
	TCheckBox *CheckBox4;
	TGroupBox *GroupBox17;
	TStaticText *StaticText56;
	TEdit *Edit17;
	TStaticText *StaticText57;
	TStaticText *StaticText58;
	TEdit *Edit20;
	TStaticText *StaticText59;
	TStaticText *StaticText60;
	TEdit *Edit21;
	TStaticText *StaticText61;
	TStaticText *StaticText62;
	TEdit *Edit22;
	TStaticText *StaticText63;
	TCheckBox *CheckBox5;
	TGroupBox *GroupBox18;
	TEdit *Edit23;
	TStaticText *StaticText64;
	TStaticText *StaticText65;
	TComboBox *ComboBox2;
	TStaticText *StaticText66;
	TGroupBox *GroupBox19;
	TStaticText *StaticText67;
	TStaticText *StaticText70;
	TEdit *Edit24;
	TStaticText *StaticText71;
	TStaticText *StaticText72;
	TEdit *Edit26;
	TStaticText *StaticText73;
	TStaticText *StaticText74;
	TEdit *Edit27;
	TStaticText *StaticText76;
	TStaticText *StaticText77;
	TEdit *Edit29;
	TGroupBox *GroupBox20;
	TLabel *Label11;
	TLabel *Label12;
	TCheckBox *CheckBox6;
	TEdit *Edit31;
	TEdit *Edit32;
	TStaticText *StaticText78;
	TStaticText *StaticText79;
	TEdit *Edit34;
	TBitBtn *BitBtn3;
	TGroupBox *GroupBox21;
	TLabel *LabelFileName;
	TGroupBox *GroupBox22;
	TLabel *LabelMkrFileName;
	TButton *Button4;
	TEdit *Edit37;
	TLabel *Label17;
	TGroupBox *GroupBox23;
	TEdit *eJOB_Number;
	TStaticText *JOB;
	TGroupBox *GroupBox24;
	TLabel *Label18;
	TEdit *emJOB_Number;
	TStringGrid *StringGrid2;
	TLabel *Label9;
	TLabel *Label19;
	TLabel *Label20;
	TLabel *Label21;
	TLabel *Label2;
	TLabel *Label16;
	TLabel *stHint10;
	TEdit *Edit5;
	TStaticText *StaticText17;
	TStaticText *StaticText18;
	TStaticText *StaticText19;
	TEdit *Edit8;
	TStaticText *StaticText20;
	TEdit *Edit9;
	TStaticText *StaticText23;
	TStaticText *StaticText24;
	TEdit *Edit10;
	TStaticText *StaticText25;
	TStaticText *StaticText27;
	TGroupBox *GroupBox2;
	TLabel *Label13;
	TLabel *Label22;
	TLabel *Label23;
	TLabel *Label24;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TEdit *Edit11;
	TLabel *Label25;
	TLabel *Label26;
	TLabel *Label27;
	TLabel *Label28;
	TStaticText *StaticText4;
	TEdit *Edit12;
	TStaticText *StaticText5;
	TCheckBox *cbImpEnable;
	TCheckBox *CheckBox1;
	TLabel *Label29;
	TTabSheet *ts9_Master;
	TEdit *ed_H1;
	TLabel *Label30;
	TLabel *Label33;
	TLabel *Label34;
	TEdit *ed_H2;
	TLabel *Label35;
	TLabel *Label36;
	TEdit *ed_H3;
	TLabel *Label37;
	TLabel *Label38;
	TEdit *ed_L1;
	TLabel *Label39;
	TLabel *Label40;
	TEdit *ed_A;
	TLabel *Label41;
	TRadioGroup *RadioGroup1;
	TLabel *Label42;
	TEdit *ed_Dsp;
	TLabel *Label43;
	TLabel *Label44;
	TEdit *ed_Step;
	TLabel *Label45;
	TLabel *Label46;
	TEdit *ed_Dwire;
	TLabel *Label47;
	TLabel *Label48;
	TEdit *ed_V;
	TLabel *Label49;
	TLabel *Label50;
	TEdit *ed_Vcb;
	TLabel *Label54;
	TLabel *Label55;
	TBitBtn *bbExitToChoiceScreen;
	TSpeedButton *sbConstructor;
	TStaticText *stAutoProg;
	TGroupBox *GroupBox3;
	TLabel *Label56;
	TEdit *Edit14;
	TLabel *Label57;
	TBitBtn *bbShowEmulation;
	TLabel *Label52;
	TGroupBox *gbAutoMakePrg;
	TLabel *Label58;
	TBitBtn *BitBtn9;
	TCheckBox *CheckBox2;
	TCheckBox *CheckBox3;
	TCheckBox *CheckBox7;
	TRadioGroup *rgAVC_Axis;
	TRadioGroup *rgAVC_Axis2;
	TSpeedButton *sbChoiceWeldType;
	TStaticText *StaticText31;
	TTabSheet *ts10_ChoiceType;
	TImage *Image6;
	TShape *Shape5;
	TLabel *Label65;
	TShape *Shape6;
	TImage *Image7;
	TShape *Shape7;
	TImage *Image8;
	TTabSheet *ts11_WeldTypeValik;
	TTabSheet *ts12_WeldTypeVert;
	TTabSheet *ts13_WeldTypeHor;
	TShape *Shape8;
	TImage *Image9;
	TLabel *Label69;
	TLabel *Label70;
	TEdit *Edit15;
	TLabel *Label71;
	TEdit *Edit19;
	TLabel *Label72;
	TEdit *Edit35;
	TRadioGroup *RadioGroup2;
	TButton *Button8;
	TButton *bMakeTipProgramm;
	TBitBtn *BitBtn10;
	TBitBtn *BitBtn14;
	TBitBtn *BitBtn15;
	TEdit *eMacroFileName;
	TLabel *Label73;
	TBitBtn *BitBtn16;
	TLabel *Label74;
	TEdit *Edit36;
	TLabel *Label75;
	TStaticText *stRemainder;
	TLabel *Label76;
	TStaticText *stSteps;
	TShape *Shape9;
	TImage *Image10;
	TLabel *Label77;
	TEdit *Edit38;
	TLabel *Label78;
	TEdit *Edit39;
	TLabel *Label79;
	TEdit *Edit40;
	TRadioGroup *RadioGroup3;
	TButton *Button10;
	TButton *Button11;
	TEdit *eMacroFileName1;
	TLabel *Label80;
	TLabel *Label81;
	TEdit *Edit42;
	TLabel *Label82;
	TStaticText *stRemainder1;
	TLabel *Label83;
	TStaticText *stSteps1;
	TShape *Shape10;
	TImage *Image11;
	TLabel *Label84;
	TLabel *Label85;
	TEdit *Edit43;
	TLabel *Label86;
	TEdit *Edit44;
	TLabel *Label87;
	TEdit *Edit45;
	TRadioGroup *RadioGroup4;
	TButton *Button12;
	TButton *Button13;
	TEdit *eMacroFileName2;
	TLabel *Label88;
	TLabel *Label89;
	TEdit *Edit47;
	TLabel *Label90;
	TStaticText *stRemainder2;
	TLabel *Label91;
	TStaticText *stSteps2;
	TLabel *Label92;
	TLabel *Label93;
	TEdit *Edit48;
	TEdit *Edit49;
	TLabel *Label94;
	TLabel *Label95;
	TEdit *Edit50;
	TLabel *Label96;
	TLabel *Label97;
	TLabel *Label98;
	TLabel *Label99;
	TLabel *Label100;
	TLabel *Label101;
	TLabel *Label102;
	TLabel *Label103;
	TLabel *Label104;
	TLabel *Label105;
	TLabel *Label106;
	TLabel *Label107;
	TLabel *Label108;
	TLabel *Label109;
	TLabel *Label110;
	TGroupBox *GroupBox4;
	TEdit *eAngle;
	TStaticText *StaticText32;
	TBitBtn *bbProgrammRun;
	TEdit *eMacroProgGenStart;
	TBitBtn *BitBtn27;
	TEdit *eMashtab;
	TButton *Button9;
	TButton *Button14;
	TEdit *eMacroProgGenWork;
	TLabel *Label51;
	TBitBtn *BitBtn26;
	TBitBtn *bbSysParam;
	TBitBtn *bbCurrentValue;
	TBitBtn *bbService;
	TStaticText *StaticText6;
	TStaticText *StaticText35;
	TStaticText *StaticText15;
	TBitBtn *BitBtn7;
	TStaticText *StaticText39;
	TStaticText *StaticText40;
	TBitBtn *bbUsers;
	TLabel *Label10;
	TShape *Shape1;
	TImage *Image1;
	TTabSheet *ts12_Constructor;
	TBitBtn *BitBtn5;
	TBitBtn *BitBtn8;
	TBitBtn *BitBtn28;
	TBitBtn *BitBtn2;
	TToolBar *ToolBar1;
	TToolButton *ToolButton1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TImageList *ImageList1;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TToolButton *ToolButton7;
	TPageControl *PageControl2;
	TPanel *PanelConstructor;
	TTabSheet *TabSheet1;
	TLabel *Label14;
	TButton *Button5;
	TButton *Button6;
	TComboBox *ComboBox1;
	TRadioGroup *RadioGroup5;
	TTrackBar *TrackBar1;
	TTrackBar *TrackBar2;
	void __fastcall sbToEditMacroClick(TObject *Sender);
	void __fastcall bbEditProgExitClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State);
	void __fastcall StringGrid1Click(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall bbEditProgSaveClick(TObject *Sender);
	void __fastcall bbEditProgLoadClick(TObject *Sender);
	void __fastcall bbEditProgNewClick(TObject *Sender);
	void __fastcall bbEditProgActivateClick(TObject *Sender);
	void __fastcall StringGrid1SetEditText(TObject *Sender, int ACol, int ARow,
          const AnsiString Value);
	void __fastcall bbEditProgDelLineClick(TObject *Sender);
	void __fastcall bbToEditProgClick(TObject *Sender);
	void __fastcall bbEditMacroLoadClick(TObject *Sender);
	void __fastcall bbEditMacroSaveClick(TObject *Sender);
	void __fastcall bbEditMacroNewClick(TObject *Sender);
	void __fastcall eDEnter(TObject *Sender);
	void __fastcall bbServiceClick(TObject *Sender);
	void __fastcall bbToLoadProgClick(TObject *Sender);
	void __fastcall bbSysParamClick(TObject *Sender);
	void __fastcall bbCurrentValueClick(TObject *Sender);
	void __fastcall bbEditLinePlClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall bbResetRobotClick(TObject *Sender);
	void __fastcall bbEditNewLineClick(TObject *Sender);
	void __fastcall StringGrid1KeyPress(TObject *Sender, char &Key);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall BitBtn4Click(TObject *Sender);
	void __fastcall BitBtn11Click(TObject *Sender);
	void __fastcall ts7_MassengerShow(TObject *Sender);
	void __fastcall bbPAUSEClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall bbUsersClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall bbEditVisioClick(TObject *Sender);
	void __fastcall ts6_MINMAX_EditEnter(TObject *Sender);
	void __fastcall BitBtn29Click(TObject *Sender);
	void __fastcall Button5999Click(TObject *Sender);
	void __fastcall bbEditPastClick(TObject *Sender);
	void __fastcall evSKeyPress(TObject *Sender, char &Key);
	void __fastcall cbImpEnableClick(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall evSExit(TObject *Sender);
	void __fastcall bbToEditClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
//	void __fastcall Image1Click(TObject *Sender);
//	void __fastcall Image3Click(TObject *Sender);
//	void __fastcall BitBtn5Click(TObject *Sender);
	void __fastcall bbShowEmulationClick(TObject *Sender);
	void __fastcall ed_H1KeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ts9_MasterShow(TObject *Sender);
	void __fastcall BitBtn9Click(TObject *Sender);
	void __fastcall BitBtn7Click(TObject *Sender);
	void __fastcall FormMouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
	void __fastcall FormMouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
	void __fastcall sbChoiceWeldTypeClick(TObject *Sender);
	void __fastcall Image6Click(TObject *Sender);
	void __fastcall BitBtn10Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall bMakeTipProgrammClick(TObject *Sender);
	void __fastcall Edit36KeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall bbExitToMainScreenClick(TObject *Sender);
	void __fastcall Image6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall bbProgrammRunClick(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall bbClearMessengerClick(TObject *Sender);
	void __fastcall bbExitToChoiceScreenClick(TObject *Sender);
	void __fastcall sbConstructorClick(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall bbEditRenumerateClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ts12_ConstructorShow(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations

	int   X,Y,Line,     				// ������� ������
		  X_show,Line_show;             // ������ ������ � �������� ������ ��� ��������� �����
	bool  Fl_NewProgramm;			    // ������� -  ����� ��������� - ���� �������� ��� ��� ����������
	bool  Fl_NewMacro;					// ������� -  ����� ������ - ���� �������� ��� ��� ����������
	bool  Fl_CanRepeat;					// ������� -  Ghj��������� ����� ���������

	int   ProgrammRegim;			  	// ����� ������ ��������� - ��������������, ������ ���.
	int   LoadError;                    // ������ ��� ��������


	short PlcError;                     // ������ �� �����������
//	short ErrLevel;
	TErrLeval *ErrLevalPtr;
	TMacroView *EditMacroView, *LookMacroView;
//	int  __fastcall RegimProg;          // ����� ������ � ����������: 0 - �����������, 1 - ��������������
	void __fastcall SetRegimEditProg();
	void __fastcall SetRegimActivateProg();
	void __fastcall RenumerateGrid();	// �������������� ������
	void __fastcall TypeMacroChange();  // ��� ����� ���� ������ �� ���� �������������� ���������(��� �����)
	void __fastcall ShiftType(int aType);  		// ��� ��������
	void __fastcall PrepareMacro(int aL);		// ���������� ������ ��� ������ ������� � ���� ���������� ���������
//���	void ChoiceWP_TG_MG();

public:		// User declarations
	TProsessStatusLine *ProsessStatusLine;// ������ ��������� �������� (������� ������)
	int   TimeTickCount;                // ������� �������� �������

	__fastcall TMainForm(TComponent* Owner);
	__fastcall ~TMainForm();
	void __fastcall SetStatLine(int Index,AnsiString str) {StatusBar1->Panels->Items[Index]->Text = str;}
	void __fastcall EventToFile(AnsiString MassageToFile);
	
private:	// User declarations
	int ZoomCount;      				// ����������� ���������� � 2 ����.
	int ShowCount;						// ����� ������� ������ ���������� �����������
	int ShowCountLimit;					// ����� ������� ������ ���������� ����������� (������������ ��������)
//	TFron_Grapher *Grapher1;    		// ������ ��������

	bool FlShow;						// ���� ���������� ���������
	TScaleHead *ScaleHead[16];			// �� �������� "���������"  - ���������� �������
	TWorkShape *WorkShape;              // �� �������� "������� ���������� ��������"  - ������ ������
	bool 		InTimer;				// �������-� ����������� �������
	bool        SysParChanged;			// ��������� ��������� ��������. ��� ������ ���� ��������� � �������� ������� ���������.
//---------------------   ������ ��������  ------------------------
	TDateTime 	StartTime;			// ����� ������
	TDateTime 	StartGasTime;		// ����� ������ ������ ����
	bool 		Fl_START_Acivated;	// ���� - ������� ���������
	int  		SystemStatus;		// ��������� ���������� ������
	TRobot	* 	Robot;				// ����� - ����������
//---------------------   ������ ���������  ------------------------
	int 		WeldType;       	// ��� �������������� 0 - ������(�����); 1- ������������ �����������; 2- �������������� �����������
	int 		StepSize;       	// ������ ���� * 10 (��)
	int 		StepCount;       	// ���������� �����
	int 		D_Start;     		// ������� ���������
	int 		Pos_Start;     		// �������/������� ���������
	int 		Pos_End;       		// �������/������� ��������
	int 		LayerCount;       	// ���������� �����
	int 		Thickness;          // ������� ����
	bool 		DirOut;

	void  __fastcall MakeProgramm(AnsiString MacroName);	// ������������ ������� ���������
	int   __fastcall Compiler(AnsiString Path);// ���������� ������� 
	
protected:
	BEGIN_MESSAGE_MAP
	VCL_MESSAGE_HANDLER(WM_USER+100, TMessage, OnMyMessage)
	END_MESSAGE_MAP(TForm)
	void __fastcall OnMyMessage(TMessage& Msg);

//����������� ������� ������������
private:	// User declarations
	AnsiString 	StoryCaption;			// ������ ��� ����� ��������� �������� ����
	bool        Line_Step;				// ������������� ����������� ������ - ������(�� ��������� � ��� ��������� ������)
//---------    ���������   ---------------
	int GLenumTp;
	HGLRC ghRC;
	HDC   ghDC;
	void Draw();

};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif


/*
	class TForm1 : public TForm
	{
	...
	protected:
	BEGIN_MESSAGE_MAP
	VCL_MESSAGE_HANDLER(WM_USER+100, TMessage, OnMyMessage)
	END_MESSAGE_MAP(TForm)
	void __fastcall OnMyMessage(TMessage& Msg);
	};
	//��������������, ���������� � ���:
	void __fastcall TForm1::OnMyMessage(TMessage& Msg)
	{
	  Panel1->Visible=true;
	  AnsiString String1=(char*)Msg.WParam;
	  AnsiString String1=(char*)Msg.LParam;
	  ...
	}

	//� �������� ���:
	  AnsiString String1="aaaaaaa", String2="bbbbbbbbb";
	  SendMessage(Form1->Handle,WM_USER+100,(WPARAM)String1.c_str(),(LPARAM)String2.c_str());//����������� Send
*/
