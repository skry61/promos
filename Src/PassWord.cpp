//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainScreen.h"
#include "PassWord.h"
#include "SysPar.h"
//--------------------------------------------------------------------- 
#pragma resource "*.dfm"
TPasswordDlg *PasswordDlg;
//---------------------------------------------------------------------
__fastcall TPasswordDlg::TPasswordDlg(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TPasswordDlg::OKBtnClick(TObject *Sender)
{
	if (Password->Text == "admin")                    // ����������� ������ � ���������� ������������ ������������� ��������� � ������
	{
		MainForm->bbSysParam->Visible  = true;        // ������ "��������� ���������" �����
		MainForm->StaticText6->Visible = true;        // ����� ����� ���
		MainForm->bbService->Visible   = true;        // ������ "������ �������" �����
		MainForm->StaticText15->Visible= true;        // ����� ����� ���

		MainForm->BitBtn4->Visible    = false;
		MainForm->BitBtn11->Visible   = false;
		MainForm->BitBtn3->Visible    = false;
		SysPar->EditEnabled(false);
	}else
	if (Password->Text == "master")					  // ����� ������������� ������ ��������� ���������
	{
		if(MainForm->bbSysParam->Visible)
		{
			MainForm->BitBtn4->Visible = true;
			MainForm->BitBtn11->Visible = true;
			MainForm->BitBtn3->Visible = true;
			SysPar->EditEnabled(true);
		}
	}else
	if (Password->Text == "control2")
	{
		if(MainForm->bbService->Visible)
		{
			MainForm->bbClearMessenger->Visible = true;
		}
	}else
	if (Password->Text == "control")
	{
		if(MainForm->bbService->Visible)
		{
			MainForm->bbClearMessenger->Visible = false;
		}
	}else
	{
		MainForm->BitBtn4->Visible = false;
		MainForm->BitBtn11->Visible = false;
		MainForm->BitBtn3->Visible = false;
		MainForm->bbSysParam->Visible = false;
		MainForm->StaticText6->Visible = false;
		MainForm->bbService->Visible  = false;
		MainForm->StaticText15->Visible = false;
		SysPar->EditEnabled(false);
	}


}
//---------------------------------------------------------------------------

void __fastcall TPasswordDlg::CancelBtnClick(TObject *Sender)
{
	Password->Text = "000";
	OKBtnClick(Sender);

}
//---------------------------------------------------------------------------

void __fastcall TPasswordDlg::Label1Click(TObject *Sender)
{
	Password->Text = "admin";
//	OKBtnClick(Sender);

}
//---------------------------------------------------------------------------
 
