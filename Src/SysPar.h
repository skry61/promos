//---------------------------------------------------------------------------

#ifndef SysParH
#define SysParH
#include <ExtCtrls.hpp>

struct sSys {
//"[Feed]"
	short parfG0_Hor;      		//  ������� ��� ��������������
	short parfG0_Vert;          //  ������� ��� ������������
	short parfvStep;            //  �������� ����
	short parTableCycle;        //  ���� ������ �������������:

//"[AVC]"
	short parAVCsens;           //  ����������������

//"[Torch]"
	short parI_StepCorr;        //  ��� ��������� ����
//"[Feed]"
	short parfG1_Hor;         	//  ������������ �������� �� �or
	short parfG1_Vert;         	//  ������������ �������� �� Vert
	short parfG1_Table;       	//  ������������ �������� ���������

//"[Wire]"
	short parWireV_StepCorr;    //  ��� ��������� ��������
//"[Feed]"
	short parfG0_Table;       	//  �������� �������� ���������

//"[Oscillation]"); ��� ������ ������
	short parosV_StepCorr;      //  ��� ��������� �������� ������
	short parosS_StepCorr;      //  ��� ��������� ���������� ��������
//[Delay]
	short parDelFeedWireStart;  // ����� ������ ���������
	short parDelFeedAxis;       // ������ ����
	short parDelFeedWireStop;   // ���� ������ ���������
	short parDelPowerOff;       // ���������� ���������
	short parDelStart_Up;       // ��������� ������
	short parREADY;  		    //  ������� ��� ����������� - ��������� ���������  = 1
};

//-------------------------------------------------
//������� ������������� ���������
struct sWork {
	float parwireMassaCommon;
	float parwireMassaShift;
	float parwireMassaCurWeld;

	void Add(float v){
		parwireMassaCommon += v;
		parwireMassaShift += v;
		parwireMassaCurWeld += v;
	}

	TDateTime parCommonWorkTime;
	float parVersion;
};
#define ShadowParamCount   19
//---------------------------------------------------------------------------
class TSysPar
{
	TEdit * eI_StepCorr;TEdit * eStart_Up;
	TEdit * efG0_Hor;TEdit * efG1_Hor;TEdit * efG0_Vert;TEdit * efG1_Vert;TEdit * efG0_Table;TEdit * efG1_Table;TEdit * efvStep;TEdit * eTableCycle;
	TEdit * eAVCsens;
	TEdit * eosV_StepCorr;     TEdit * eosS_StepCorr;
	TEdit * eWireV_StepCorr;

	TEdit * ePathText;
	TStaticText * ewireMassaCommon;TStaticText * ewireMassaShift;	TStaticText * ewireMassaCurWeld;
	TStaticText * eCommonWorkTime;
	TStaticText * eVersion;
	TEdit *eDelFeedWireStart, *eDelFeedAxis, *eDelFeedWireStop, *eDelPowerOff;
//    TEdit * eDelFeedWireStart, TEdit * eDelFeedAxis,TEdit * eDelFeedWireStop,TEdit * eDelPowerOff;



	TEdit * EditArray[ShadowParamCount];
	sSys  SysPar;
	sWork WorkPar;
	AnsiString ProjectPath;
	int LoadDefaultEdit();
	int	LoadValue();
	int LoadDefault();
public:
	__fastcall TSysPar::TSysPar(
	TEdit * I_StepCorr,TEdit * Start_Up,
	TEdit * fG0_Hor,TEdit * fG1_Hor,TEdit * fG0_Vert,TEdit * fG1_Vert,TEdit * fG0_Table,TEdit * fG1_Table, TEdit * fvStep,TEdit * TableCycle,
	TEdit * AVCsens,
	TEdit * osV_StepCorr,  TEdit * osS_StepCorr,
	TEdit * WireV_StepCorr,

	TEdit * PathText,
	TStaticText * wireMassaCommon,TStaticText * wireMassaShift,	TStaticText * wireMassaCurWeld,
	TStaticText * CommonWorkTime,
	TStaticText * Version,
	TEdit * DelFeedWireStart, TEdit * DelFeedAxis,TEdit * DelFeedWireStop,TEdit * DelPowerOff
	);

	int	__fastcall SaveToFile();
	int	__fastcall SaveToFileDat();
	int	__fastcall LoadFromFile();
	sSys* __fastcall GetSysData();
	AnsiString GetProjectPath(){return ProjectPath;};
	int	__fastcall SetWorkTime(TDateTime wt);
	void EditEnabled(bool Fl){for(int i=0; i<ShadowParamCount;i++){EditArray[i]->Enabled = Fl;};}
	int	__fastcall SetWorkGas(TDateTime Period,double GasSpeed);
	void __fastcall InitWelding();
	void __fastcall ClearCommonGaz(){WorkPar.parwireMassaCurWeld = WorkPar.parwireMassaCommon = 0;}
	void __fastcall ClearShiftGaz(){WorkPar.parwireMassaCurWeld = WorkPar.parwireMassaShift = 0;}

};
extern PACKAGE TSysPar *SysPar;
//---------------------------------------------------------------------------
#endif





