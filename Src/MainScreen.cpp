//---------------------------------------------------------------------------
/*
1.
*/

#define VER   "3.00"  /* 29.04.18  ���� ������ ��� �������� � ����������� */
//#define VER   "2.07"  /* 25.10.17  ��������� : ���������� ���������� ������� ��������� */
//#define VER   "2.06"  /* 1.09.17  ��������� : ����� ����� ������ ������ ��������� � ��������*/
						/*1.09.17  ��������� : ��� �������� �� ����������� ���������*/
						/*1.09.17  ��������� : ������ ���� ��������� ����� ��������� ��������*/

//#define VER   "2.05"  /* 16.07.17  ��������� : ��������� ��������� �� ������ ������*/
//#define VER   "2.04"  /* ��������� : ��������� ���������*/
/*#define VER   "2.03"   ��������� �� �������:
						1.������������� �������� ������ ���� �� ���������. � ��� ���. ���������� �������� ��������
						2.� ���� ������ ��������
						3.������������ ������� ������ ���� �������������
						4.���� ������ ���� �����������
						5.� ������� �������� ���� � ���
					  */
//#define VER   "2.02"  1 - ���������� ������
//						2 - ����������� �������� ����� �������� � ������� 
//#define VER   "2.01"

#define NO_WIN32_LEAN_AND_MEAN
#include <shlobj.h>

#include <vcl.h>
#pragma hdrstop

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

//#include "EditorForm.h"		// ��������
#include "Option.h"
#include "IniFiles.hpp"
#include "Simulate_PLC.h"
#include "ServiceClass.h"
//#pragma link "Grapher"
//#pragma link "Fron_Grapher"
#include "PassWord.h"
#include "MainScreen.h"
#include "SwitchTable.h"
#include "FileOpenDialog.h"
#include "FileSaveDialog.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

float LineXXX = 0.5;
float DepthXXX = 1;
GLenum gleTp[]={GLU_FILL,GLU_LINE,GLU_POINT,GLU_SILHOUETTE};

//---------------------------------------------------------------------------
#include <fstream.h>
#include <iostream.h>

void __fastcall TMainForm::EventToFile(AnsiString MassageToFile)
{

	AnsiString FileName   = ChangeFileExt(Application->ExeName,".evn");

	ofstream fout;
	fout.open(FileName.c_str(),ios::app); // ��������� ������ � ������
//	AnsiString s = Now().CurrentDateTime().operator AnsiString()+ MassageToFile;
	AnsiString s = Now().CurrentDateTime().DateTimeString()+" -- " + MassageToFile;
	fout << s.c_str()<<"\n";
	fout.close();
}


//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
	: TForm(Owner)
{
	Caption = AnsiString("������ ") + VER +"   ��������� ��������� �������";

	Fl_NewProgramm 	= true;
	Fl_NewMacro		= true;
// �� �������� "���������"  - ���������� �������
	ScaleHead[0] = new TScaleHead(Owner,10 ,90,&ParamsVisio[0]);
	ScaleHead[1] = new TScaleHead(Owner,348,90,&ParamsVisio[1]);
	ScaleHead[2] = new TScaleHead(Owner,686,90,&ParamsVisio[2]);
	ScaleHead[3] = new TScaleHead(Owner,10,400,&ParamsVisio[3]);

	ScaleHead[0]->Parent = ts6_MINMAX_Edit;
	ScaleHead[1]->Parent = ts6_MINMAX_Edit;
	ScaleHead[2]->Parent = ts6_MINMAX_Edit;
	ScaleHead[3]->Parent = ts6_MINMAX_Edit;

// �� �������� "������ ���������� ��������"  - ������ ������
	WorkShape = new TWorkShape(Owner,10 ,50,(TForm*)this,(TStringGrid*)StringGrid1);
	WorkShape->Parent = ts9_Master;
//	WorkShape->Parent = PageControl1;

	ProgrammRegim = NO_REGIM;

	LookMacroView		= new TMacroView(
					  Edit23,ComboBox2,Edit31,Edit32,CheckBox6,rgAVC_Axis2,
					  Edit24,Edit26,Edit29,Edit27,CheckBox1,
					  Edit16,CheckBox4,
					  Edit17,Edit20,Edit21,Edit22,CheckBox5,
					  eJOB_Number
	);
	EditMacroView 	    = new TMacroView(
					  evS,cbDir,eV_AVC,eJump,cbAVCEnable,rgAVC_Axis,
					  elp,elg,etp,etg, cbImpEnable,
					  evdg,cbWire_enable,
					  evpl,etpl,etpr,espl,cbOscEnable,
					  emJOB_Number
	);
	SysPar             = new TSysPar(
/*"[Torch]" */	   		Edit18,Edit12,
/*"[Feed]"*/ 			Edit1,Edit8,Edit28,Edit5,Edit9,Edit10,Edit30,Edit34,
/*"[AVC]"*/             Edit33,
/*"[Oscillation]"*/		Edit6,Edit7,
/*"[Wire]"*/	   		Edit25,
/*�����*/ 				 Edit37,
						StaticText97,StaticText98,StaticText99,
						StaticText96,
						StaticText139,
						Edit2,Edit3,Edit4,Edit11
	);


	//----------------    ��������� ��������    --------------------
	Robot				= new TRobot(EditMacroView);		// ����������� ������ �����������

	ProsessStatusLine 	= new TProsessStatusLine(PanelProsessStatus,Robot);
	Fl_START_Acivated 	= false;			// ������� �� �����������
	SystemStatus = sysINIT;					//������� ��������� �� ������� ���������� ���������
	PlcError = 0;
	SimulPLC_Form = NULL;
	MainForm->DoubleBuffered=true;
// ---------  ��� �������������� ��������  -------------------
	StepCount = 1;       	// ���������� �����
	StepSize  = -100;    		// ������ ���� * 10 (��)
	Pos_Start = 1000;   		// �������/������� ���������
	Pos_End   = 900;       		// �������/������� ��������
	LayerCount= 1;      		// ���������� �����
	Line_Step = false;			// �� ��������� � ��� ��������� ������(�� ����)
//	BUSINTX11_Form = NULL;
}
//---------------------------------------------------------------------------
__fastcall TMainForm::~TMainForm()
{
	delete  ProsessStatusLine;
	delete  Robot;
	delete  SysPar;
	delete  LookMacroView;
	delete  EditMacroView;
/*   ���������� Owner
	delete  ScaleHead[2];
	delete  ScaleHead[1];
	delete  ScaleHead[0];
*/
};

//---------------------------------------------------------------------------
void __fastcall TMainForm::sbToEditMacroClick(TObject *Sender)
{
	PageControl1->ActivePageIndex = 2;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::sbChoiceWeldTypeClick(TObject *Sender)
{
	PageControl1->ActivePageIndex = 8;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditProgExitClick(TObject *Sender)
{
	if(((PageControl1->ActivePageIndex == 5)||(PageControl1->ActivePageIndex == 1))&&(ProgrammRegim == ACT))
	{
		if (MessageBox(GetActiveWindow(),"������� ������ �������!\n"
				"������ ��� ���������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;
		bbResetRobotClick(NULL);
    }
	PageControl1->ActivePageIndex = 0;

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormShow(TObject *Sender)
{
static  AnsiString ColsName[] = {"#���","                                          ������","    �������","    �������� ","   ���� ","  ������"};
static  AnsiString ColsName2[] = {"����� �������"," ������� "};
	Line = Line_show =1;
	for(int i=0;i<StringGrid1->ColCount;i++)
		StringGrid1->Cols[i]->Text = ColsName[i];
	for(int i=1;i<StringGrid1->RowCount;i++)
		StringGrid1->Rows[i]->Text = i;
	for(int i=0;i<StringGrid2->ColCount;i++)
		StringGrid2->Cols[i]->Text = ColsName2[i];

#ifndef  EASY
	bbUsersClick(NULL);     // ����� ������
#else
	BitBtn7->Visible = true;
	stAutoProg->Visible = true;
	bbClearMessenger->Visible = true;
#endif
	bbShowEmulationClick(NULL);	//������ ��������� ��������� �������
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StringGrid1DrawCell(TObject *Sender, int ACol,
	  int ARow, TRect &Rect, TGridDrawState State)
{
	if(ProgrammRegim == ACT)
	{
		StringGrid1->Canvas->Brush->Color=clYellow;
		StringGrid1->Canvas->Pen->Color=clBlack;
	}
	else if((ProgrammRegim == EDIT)||(ProgrammRegim == RESET))
		StringGrid1->Canvas->Brush->Color=clSkyBlue;
	if (ACol>=0 && Line>0 && ARow==Line) {
		StringGrid1->Canvas->FillRect(Rect);
		StringGrid1->Canvas->TextOut(Rect.Left, Rect.Top, StringGrid1->Cells[ACol][ARow]);
	}else
	if((ProgrammRegim == ACT)&&(ACol>=0 && Line_show>0 && ARow==Line_show))
	{
		StringGrid1->Canvas->Brush->Color=clSkyBlue;
		StringGrid1->Canvas->FillRect(Rect);
		StringGrid1->Canvas->TextOut(Rect.Left, Rect.Top, StringGrid1->Cells[ACol][ARow]);
	}

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StringGrid1Click(TObject *Sender)
{
	int L;
	X = StringGrid1->Selection.Left;
	if(ProgrammRegim == EDIT)
		L = Line = StringGrid1->Selection.Top;
	else
		L = Line = Line_show = StringGrid1->Selection.Top;
	StringGrid1->Refresh();

	if(ProgrammRegim == EDIT)
	{
		Robot->ClearProg();
		PrepareMacro(L);
    	L=0;
    	if(LoadError!=0){LoadError = 0;return;}
	}
	LoadError = Robot->LoadMacroFromRobot(L-1,LookMacroView);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  //   CanClose = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::bbEditProgSaveClick(TObject *Sender)
{
	SaveForm->InitData(false,LabelFileName->Caption);
	if(SaveForm->ShowModal() == mrCancel) {LoadError = 10; return; }   // ����� ������ �����
	AnsiString FileName = SaveForm->GetFileName();
	AnsiString FullFileName = SaveForm->GetFullFileName();

	TStringList *List = NULL;
	try
	{
		if (FileExists(FullFileName))
			if (MessageBox(GetActiveWindow(),"���� ��������� � ����� ������ ��� ����������!\n"
				"������ ��� ����������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;
		if(StringGrid1->RowCount<2)
		{
			ShowMessage("� ��������� ������ ���� ���� �� ���� ������.");
			return;
		}
		List = new TStringList();
		List->Add(StringGrid1->RowCount);		// ��������� ���������� �����

		for (int i=1; i<StringGrid1->RowCount; i++)
		{
			AnsiString s = "";
			for (int j=1; j<StringGrid1->ColCount; j++)
				s+= StringGrid1->Cells[j][i]+ ";";
			List->Add(s);

		}

		List->Add("[Comments]");
		List->Add("Comment1="+ SaveForm->GetComment());

		List->SaveToFile(/*SaveDialog->FileName*/FullFileName);
		SetStatLine(0,"��������� ��������� � ���� " +FileName+".prg");
		LabelFileName->Caption = FileName;
/*
		SetStatLine(0,"��������� ��������� � ���� " +ExtractFileName(SaveDialog->FileName));
		AnsiString Name = ExtractFileName(SaveDialog->FileName);
		LabelFileName->Caption = Name.SubString(0,Name.Pos(".")-1);
*/
		delete List;
	} catch ( ... ) {
		ShowMessage("������ ��� ���������� ��������� ������.");
		if(List)
			delete List;
//		return;
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::bbEditProgLoadClick(TObject *Sender)
{
	AnsiString wpName = "\\Program";
	StringGrid1->MakeNewMacroList(wpName);

	wpName = SysPar->GetProjectPath() + wpName;
	LoadError = 0;
	OpenFileForm->InitData(false);
	if(OpenFileForm->ShowModal() == mrCancel) {LoadError = 10; return; }   // ����� ������ �����
	AnsiString FullFileName = OpenFileForm->GetFullFileName();
	AnsiString FileName = OpenFileForm->GetFileName();
	SaveForm->SetComment( OpenFileForm->GetComment());


	try
	{
		TStringList *List = new TStringList();
		try
		{
			List->LoadFromFile(FullFileName);
			int RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
			StringGrid1->RowCount = RowCount;
			if((RowCount<2)||(RowCount>500))
			{
				ShowMessage("� ��������� ���������������� ���������� �����.");
				LoadError = 11;
				return;
			}
			for (int i=1; i<RowCount; i++)
			{
				AnsiString s = List->Strings[i];
				for (int j=1; j<StringGrid1->ColCount; j++)
				{
					int Pos = s.Pos(";");
					AnsiString cell =  s.SubString(0,Pos-1);
					StringGrid1->Cells[j][i] = cell;
					s = s.Delete(1,Pos);
				}
			}
			// ���������� ������� 
			int Err = Compiler(wpName);
			if(Err && (Err < 4)) return ;	// ������ ������� - �������
			
			SetStatLine(0,"��������� ��������� �� ����� " +FileName+".prg");
			LabelFileName->Caption = FileName;

		}__finally
		{
			delete List;
		}

	} catch ( ... ) {
		ShowMessage("������ ��� �������� ��������� ������.");
		LoadError = 9;
		return;
	}
	RenumerateGrid();		// �������������� ������
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbEditProgNewClick(TObject *Sender)
{
	Fl_NewProgramm = true;
	LabelFileName->Caption = "NEW";

	StringGrid1->RowCount = 2;
	for (int j=1; j<StringGrid1->ColCount; j++)
		StringGrid1->Cells[j][1] = "";
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::bbEditProgActivateClick(TObject *Sender)
{
	AnsiString wpName = "\\Program";
	wpName = SysPar->GetProjectPath() + wpName;

	if(LabelFileName->Caption != "NEW")
	{
		// ���������� ������� 
		int Err = Compiler(wpName);
		if(Err && (Err < 4)) return ;	// ������ ������� - �������

		Caption = "������ "+AnsiString(VER)+"   ���������: " +LabelFileName->Caption +"       �����c: "+ StringGrid1->Cells[1][1];
		SetRegimActivateProg();
	}
	else
		ShowMessage("������ ��� ������� ��������� �������� ��������� ��.");
}

//---------------------------------------------------------------------------
// ���������� ������� 
int __fastcall TMainForm::Compiler(AnsiString Path)
{
	AnsiString D;
	LoadError = 0;
	Robot->ClearProg();
	for (int i=1; i<StringGrid1->RowCount; i++)
	{
		for (int j=1; j<StringGrid1->ColCount; j++)
		{
			AnsiString cell =  StringGrid1->Cells[j][i];
			switch (j)
			{
			case 1:					// �������� �������
				switch( Robot->LoadMacroFromFile(Path + "\\Macros\\" + cell + ".mcr"))
				{
					case 0:				// ������� ����������
						break;
					case 1:				// ���� ������� �� ������
						ShowMessage("������.\n���� ������� "+ cell + ".mcr �� ������ � ����� ��������\n ������ " + i);
						LoadError = 2;
						return LoadError;
					case 2:				// ���������������� ������ �������
						ShowMessage("������.\n���������������� ������ �������"+ cell + "\n ������ " + i);
						LoadError = 3;
						return LoadError;

				}
				break;
			case 2:					// �������� ���� "�������"
				if(Robot->SetDiametr(cell))
				{
					LoadError = 4;
					ShowMessage("������.\n�������� �������� ���������������.\n��������� ������� �� ���������.");
					StringGrid1->Cells[j][i] = Robot->GetDiametr();
				}
				break;
			case 3:					// �������� ���� "��������"  � ������ ����������� �� ���� H � V
				if(i<StringGrid1->RowCount-1)
					D = StringGrid1->Cells[2][i+1];
				else
					D = StringGrid1->Cells[2][i];

				if(Robot->SetStepValue(cell,eAngle->Text.ToDouble(),D))
				{
					LoadError = 5;
					ShowMessage("������.\n������ ��������, ������� ��� ���� ������� �������� �� ������������.\n��������� ������� �� ���������.");
					StringGrid1->Cells[j][i] = Robot->GetStepValue();

				}
				break;
			case 4:					// �������� ���� "����"
				if(Robot->SetAngle(cell))
				{
					LoadError = 6;
					ShowMessage("������.\n�������� ���� ���������������.\n��������� ������� �� ���������.");
					StringGrid1->Cells[j][i] = Robot->GetAngle();
				}
				break;
			case 5:					// �������� ���� "������"
				if(Robot->SetWeldControl(cell))
				{
					LoadError = 7;
					ShowMessage("������.\n��� ������ ���������������.\n��������� ������� �� ���������(��).");
					StringGrid1->Cells[j][i] = "��";
				}
				break;

			default:  ;
			}
		}
		Robot->AddLine(i==StringGrid1->RowCount-1);
	}
	Fl_NewProgramm = false;
	int Error = Robot->LoadMacroFromRobot(0,LookMacroView);
	if(LoadError == 0)
		LoadError = Error;
	return LoadError;
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::PrepareMacro(int aLine)
{
	AnsiString wpName = "\\Program";
	wpName = SysPar->GetProjectPath() + wpName;

	AnsiString cell =  StringGrid1->Cells[1][aLine];
	if(cell == "") {LoadError = 10;return;} 
	switch( Robot->LoadMacroFromFile(wpName + "\\Macros\\" + cell + ".mcr"))
	{
		case 1:				// ���� ������� �� ������
			ShowMessage("������.\n���� ������� "+ cell + ".mcr �� ������ � ����� ��������\n ������ " + aLine);
			LoadError = 2;
			return;
		case 2:				// ���������������� ������ �������
			ShowMessage("������.\n���������������� ������ �������"+ cell + "\n ������ " + aLine);
			LoadError = 3;
			return;
	}
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::StringGrid1SetEditText(TObject *Sender, int ACol,
	  int ARow, const AnsiString Value)
{
//	SetStatLine(0,"��� SetEditText");
	if(X == 1)
	{
		AnsiString wpName = "\\Program";
		wpName = SysPar->GetProjectPath() + wpName;

		switch( Robot->LoadMacroFromFile(wpName + "\\Macros\\" + Value + ".mcr"))
		{
			case 0:				// ������� ����������
				break;
			case 1:				// ���� ������� �� ������
				ShowMessage("������.\n���� ������� "+ Value + ".mcr �� ������ � ����� ��������\n ������ " + ARow);
				LoadError = 2;
				break;
			case 2:				// ���������������� ������ �������
				ShowMessage("������.\n���������������� ������ �������"+ Value + "\n ������ " + ARow);
				LoadError = 3;
				break;
		}
		LoadError = Robot->LoadMacroFromRobot(-1,LookMacroView);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditProgDelLineClick(TObject *Sender)
{
	if((Line == StringGrid1->RowCount-1)&&(Line>1))
	{
		StringGrid1->RowCount--;
		StringGrid1->Selection.Top--;
		StringGrid1Click(Sender);
	} else if((Line == 1) && (StringGrid1->RowCount == 2))
	{
		StringGrid1->Rows[1]->Clear();
		StringGrid1->Cells[0][1] = 1;
	}
	else{
		for(int i=Line; i < StringGrid1->RowCount-1; i++)
		{
			StringGrid1->Rows[i] = StringGrid1->Rows[i+1];
			StringGrid1->Cells[0][i] = i;
		}
		StringGrid1->RowCount--;
	}
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::SetRegimEditProg()
{
	if(ProgrammRegim != EDIT)          // ����� ������ � ����������: 0 - �����������, 1 - ��������������
	{
		if(ProgrammRegim == NO_REGIM)
			LabelFileName->Caption = "NEW";
//		else
//			LabelFileName->Caption = "";

		ProgrammRegim = EDIT;
//		LabelFileName->Caption = "";
//!!!		StringGrid1->Enabled = true;
		StringGrid1->Options= StringGrid1->Options<<goEditing;

		bbEditProgNew->Visible = true;
		bbEditProgSave->Visible = true;
//		bbEditLinePl->Visible = true;
//		bbEditLineMin->Visible = true;
//		bbEditPagePl->Visible = true;
//		bbEditPageMin->Visible = true;
//		bbEditPrint->Visible = true;
//		bbEditCalkStep->Visible = true;
//		bbEditCopy->Visible = true;
		bbEditPast->Visible = true;
		bbEditNewLine->Visible = true;
		bbEditProgDelLine->Visible = true;
		bbEditProgActivate->Visible = true;
		bbEditVisio->Visible = false;

		bbResetRobot->Visible = false;
		bbPAUSE->Visible = false;
		bbToEdit->Visible = false;
 //		bbSTOP->Visible = false;

		StringGrid1->TopRow = Line = 1;
		StringGrid1->Refresh();
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SetRegimActivateProg()
{
	if(ProgrammRegim != ACT)          // ����� ������ � ����������: 0 - �����������, 1 - ��������������, 2 - ACT
	{
		ProgrammRegim = ACT;
		if(LabelFileName->Caption == "NEW")
			LabelFileName->Caption = "";
		StringGrid1->Options= StringGrid1->Options>>goEditing;

		bbEditProgNew->Visible = false;
		bbEditProgSave->Visible = false;
//		bbEditLinePl->Visible = false;
		bbEditLineMin->Visible = false;
		bbEditPagePl->Visible = false;
		bbEditPageMin->Visible = false;
		bbEditPrint->Visible = false;
		bbDemonstrateProg->Visible = false;
		bbEditRenumerate->Visible = true;
		bbEditPast->Visible = false;
		bbEditNewLine->Visible = false;
		bbEditProgDelLine->Visible = false;
		bbEditVisio->Visible = true;

		bbResetRobot->Visible = true;
		bbPAUSE->Visible = true;
//		bbSTOP->Visible = true;
//		bbSTOP->Enabled = false;
	}
	//StringGrid1->TopRow =
	SystemStatus = sysToACTIVATED;					//
	Line = Line_show;// =1;
	if(Line > 5)
		StringGrid1->TopRow = 1+ Line-5;
	else
		StringGrid1->TopRow = 1;

	StringGrid1->Refresh();

	bbEditProgActivate->Visible = false;
//	bbEditVisio->Visible = false;
	bbToEdit->Visible = false;

	ColorButtonTop(bbPAUSE,clWindow);
//	ColorButtonTop(bbSTOP,clRed);
//	ColorButtonTop(bbResetRobot,clYellow);
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::bbToEditProgClick(TObject *Sender)
{
	AnsiString wpName = "\\Program";
	StringGrid1->MakeNewMacroList(wpName);
	SetRegimEditProg();
	PageControl1->ActivePageIndex = 1;
	TypeMacroChange();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::bbToEditClick(TObject *Sender)
{
	SetRegimEditProg();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditMacroLoadClick(TObject *Sender)
{

	LoadError = 0;
	OpenFileForm->InitData(true);
	if(OpenFileForm->ShowModal() == mrCancel) {LoadError = 10; return; }   // ����� ������ �����
	AnsiString FullFileName = OpenFileForm->GetFullFileName();
	AnsiString FileName = OpenFileForm->GetFileName();
	SaveForm->SetComment( OpenFileForm->GetComment());

	int Res = EditMacroView->LoadViewFromFile(FullFileName);
	if(Res == 0)
	{
		SetStatLine(0,"������ �������� �� ����� " +FileName);
		LabelMkrFileName->Caption = FileName;
	}else
		ShowMessage("������ ��� �������� �������.");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditMacroSaveClick(TObject *Sender)
{
	SaveForm->InitData(true,LabelMkrFileName->Caption);
	if(SaveForm->ShowModal() == mrCancel) {LoadError = 10; return; }   // ����� ������ �����
	AnsiString FileName = SaveForm->GetFileName();
	AnsiString FullFileName = SaveForm->GetFullFileName();

	if(FileName.Pos(".")!= 0)
	{
		ShowMessage("����� � ����� ������� �����������.");
		return;
	}

	try
	{
		if (FileExists(FullFileName))
		{
			if (MessageBox(GetActiveWindow(),"���� ������� � ����� ������ ��� ����������!\n"
				"������ ��� ����������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;
		}
		AnsiString Name = ExtractFileName(FullFileName);
		int Res = EditMacroView->SaveToFile(Name,SaveForm->GetComment());
		if(Res != 0)
		{
			ShowMessage("�� ������� ��������� ���� �������.");
			return;
		}
//		AnsiString Name = ExtractFileName(SaveDialog->FileName);
		SetStatLine(0,"������ �������� � ���� " +Name);
		LabelMkrFileName->Caption = FileName;//.SubString(0,Name.Pos(".")-1);
	} catch ( ... ) {
		ShowMessage("������ ��� ���������� �������.");
	}

//�������� ��� ����� � ��������		StringGrid1->MakeNewMacroList(wpName);                          // �������� ����� ������ - ���� �������� � ������
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::bbEditMacroNewClick(TObject *Sender)
{
	Fl_NewMacro		= true;
	LabelMkrFileName->Caption = "NEW";

	int Res = EditMacroView->LoadViewFromFile("init.mcr");
	if(Res == 0)
	{
		SetStatLine(0,"������ �������� ���������� ����������");
	}else
		ShowMessage("������ ��� �������� �������.");
}
//---------------------------------------------------------------------------
// ����� ��������� ����� ��� ���� Edit ��� �����
void __fastcall TMainForm::eDEnter(TObject *Sender)
{
	TEdit *eEd = (TEdit*)Sender;
	if(eEd->Tag < 10)
	{

/*		if(eEd->Tag==1)
			stHints->Caption = "���������� ��������� � ������ �iG";
		else
*/		
			stHints->Caption = eEd->Hint;
	}
	else if(eEd->Tag == 10)
		stHint10->Caption = eEd->Hint;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbServiceClick(TObject *Sender)
{
	bbUsersClick(NULL);
	if(MainForm->bbService->Visible == true)
		PageControl1->ActivePageIndex = 6;

}
//---------------------------------------------------------------------------
// ��������� ��������� ��� ���������
void __fastcall TMainForm::bbToLoadProgClick(TObject *Sender)
{
	bbEditProgLoadClick(Sender);
	if((LoadError==0)|| (LoadError < 10))
	{
		SetRegimActivateProg();
		PageControl1->ActivePageIndex = 1;
	}
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::bbSysParamClick(TObject *Sender)
{
	bbUsersClick(NULL);
	if(MainForm->BitBtn4->Visible == true)
		PageControl1->ActivePageIndex = 3;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbCurrentValueClick(TObject *Sender)
{
	PageControl1->ActivePageIndex = 5;
}
//---------------------------------------------------------------------------

 // !!!!!!   ��������� ���������
void __fastcall TMainForm::bbEditLinePlClick(TObject *Sender)
{
/*
	if(Fl_START_Acivated)
		if(Line < StringGrid1->RowCount-1)
		{
			Line++;
			if(Line == 2)
			{
				TGridRect Rect;
				Rect.Top = StringGrid1->RowCount;
				Rect.Left = 1; 						// ������ �������
				Rect.Right = StringGrid1->ColCount; // ��������� �������
				Rect.Bottom = StringGrid1->RowCount;
				StringGrid1->Selection = Rect; 		// �������� ������
			}
			StringGrid1->Refresh();
			if(Line > 5)
				StringGrid1->TopRow = 2 + Line-5;
		}else
		{
			Fl_START_Acivated = false;
			SystemStatus = sysEND_PROG;			// ������� �� ������� �� ������� ���������� ���������
		}
*/
}
//===========================================================================
//===========================================================================
// ������� ������� ����
void __fastcall TMainForm::Timer1Timer(TObject *Sender)
{

	static int TimerDelayCnt = 0;
	static int TimerActivity = 0;
	TDateTime DeltaTime;
	TDateTime dtTmp = Now();
	TPosStruct PosHVT;
	TDateTime run_t;
	volatile int sRes;
	int LastLine;

	TimeTickCount++;
	if (InTimer)		return;
	InTimer = true;

	try {


		// ����������� ������� �����
		if(TimeTickCount == 0)
			StartGasTime = DeltaTime = Now();

		// ����������� ������� �����
		if(TimeTickCount == 3)
			OptionForm->ConnectPLC();		// ����������� � �����������
		if(TimeTickCount < 5)
		{
			PanelProsessStatus->Caption = "�� �����";
			return;
		}
		if(TimeTickCount > 10)
		{

			if(OptionForm->isChanal1FrontStabilErrorOn())
				EventToFile("������ ����� � ������������ ������");
			if(OptionForm->isChanal1FrontStabilErrorOff())
				EventToFile("����� � ������������ ������ �������������");

		}

		StatusBar1->Panels->Items[1]->Text = dtTmp.TimeString();


		int RobotStat = Robot->GetProsStatus();
//���		ChoiceWP_TG_MG();

		if((RobotStat != robERROR) && !OptionForm->isChanal1StabilError())
		{
			PanelProsessStatus->Color = clBtnFace;
			if(PageControl1->ActivePageIndex == 5)
			{
				ScaleHead[0]->Value = Robot->Get_Cur_I();
				ScaleHead[1]->Value = Robot->Get_Cur_B();
				ScaleHead[2]->Value = Robot->Get_Cur_V();
				ScaleHead[3]->Value = Robot->Get_Cur_F();//(float)TimeTickCount/10;
			}

			DeltaTime = Now();
			SysPar->SetWorkGas(DeltaTime-StartGasTime,Robot->Get_Cur_F()); 	// ����������� ������� ���� ����������
			StartGasTime = DeltaTime;


			if(SimulPLC_Form != NULL)
				SimulPLC_Form->SetActivity(RobotStat,SystemStatus,TimerActivity++);



			switch(SystemStatus)
			{
			case sysINIT          :                  	//������� ��������� �� ������� ���������� ���������

						switch(RobotStat)
						{
						case robNOT_READY     : PanelProsessStatus->Caption = "�� �����";break;

						case robREADY         : PanelProsessStatus->Caption = "�����";	break;

						case robSTARTING : 		// �������� �������� ���������
							PanelProsessStatus->Caption = "��������� �������� ���������";
							SystemStatus = sysACTIVATION_WAIT;
							bbToLoadProg->Perform(WM_LBUTTONDOWN, 0, 0);
							bbToLoadProg->Perform(WM_LBUTTONUP, 0, 0);
							break;
						default:
							Robot->SetControlPC_Err();
							SystemStatus = sysERROR;

						}
				break;

			case sysERROR          			 :                  	//������ ��
					if(!Robot->isGetControlPC_Err())                // ������ �������� � �����������
							SystemStatus = sysINIT;
				break;

			case sysACTIVATION_WAIT          :                  	//

				break;

			case sysToACTIVATED              :
				Robot->Init(Line-1);
				if(RobotStat == robSTARTING)      	// ��� ������: ���� ���� PC-Ready ���� ���
				{
					Robot->SetControlPC_Ready();
					sRes = Robot->SendBufferToPLC(&LastLine);
					sRes = Robot->LoadMacroFromRobot(Line-1,LookMacroView);
					SystemStatus = sysACTIVATED;
					StartTime = Now();				// �������� ������
					StartGasTime = Now();			// �������� ������ ������ ����
					SysPar->InitWelding();			// ����������� ������� ���� ��������������
					PanelProsessStatus->Caption = "����� ������";
					EventToFile("����� ������");
					TimerDelayCnt = 0;
				}else
				{
					PanelProsessStatus->Caption = "����� � ������";
					break;
				}

			case sysACTIVATED                :                  	//
				switch(RobotStat)
				{
	//			case robSTARTING         :
	//				SystemStatus = sysToACTIVATED;
	//                break;
				case robSTART         :
					if(TimerDelayCnt > 1)		//�������� � 2 ��� �� ������ �������� ������� �����
					{
						if(!Robot->isDataReady()) break;	// ������ ������ ���� �� �������   �203
						TimerDelayCnt = 0;
						if(!Robot->isBufBusy())
							sRes = Robot->SendBufferToPLC(&LastLine);
					}else
						TimerDelayCnt++;
					if(Robot->isSaveParam())
						Robot->DownLoadBuffer(LookMacroView);

					ColorButtonTop(bbPAUSE,clWindow);
					//���� �������� ����� ������ �� ������ - ��������� ��� � USINTX11
					if ((Robot->CurLine != 0)&&(Robot->CurLine != Line)&& (Robot->CurLine < StringGrid1->RowCount))
					{
						Line = Robot->CurLine;
						if(Line > 5)
							StringGrid1->TopRow = 1+ Line-5;
						StringGrid1->Refresh();
						sRes = Robot->LoadMacroFromRobot(Line-1,LookMacroView);
						Caption = "������ "+AnsiString(VER)+"    ���������: " +LabelFileName->Caption +"       �����c:"+ Robot->GetMacroName();

					}

					PosHVT = Robot->GetPosition();
					ProsessStatusLine->SetParam("",Line,PosHVT.Lay/*Vertical*/ , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
					break;

				case robPAUSING       :
					PosHVT = Robot->GetPosition();
					ProsessStatusLine->SetParam("�� �����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
					break;

				case robPAUSE         :
					PosHVT = Robot->GetPosition();
					ColorButtonTop(bbPAUSE,clYellow);
					StartGasTime = Now();		// �������� ������
					ProsessStatusLine->SetParam("�����",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
					break;

				case robFINISHING     :
					PosHVT = Robot->GetPosition();
					run_t = ProsessStatusLine->SetParam("���������� ",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
					SysPar->SetWorkTime(run_t);
					break;

				case robSTOP          :

				case robFINISH        :
					PosHVT = Robot->GetPosition();
					run_t = ProsessStatusLine->SetParam("���������� ",Line,PosHVT.Vertical , PosHVT.TurnTable ,PosHVT.Horizontal , StartTime);
					SysPar->SetWorkTime(run_t);
					SystemStatus = sysFINISHING_PROG;   // ������� �� ������� �� ������� ���������� ���������
					break;
				} //RobotStat
				break;
			case sysFINISHING_PROG:
				Fl_START_Acivated = false;
				SystemStatus = sysEND_PROG;			// ������� �� ������� �� ������� ���������� ���������
	//				ProgrammRegim = NO_REGIM;                	//
				if(RobotStat == robFINISH)
					PanelProsessStatus->Caption = "������ ���������. ������������ �������� : " +  ProsessStatusLine->GetRunTime().TimeString();
				else
					PanelProsessStatus->Caption = "������ �����������. ������������ �������� : " +  ProsessStatusLine->GetRunTime().TimeString();
				bbEditProgActivate->Visible = true;
				Robot->ReSetControlPC_Ready();

				Fl_CanRepeat = true;
				EventToFile(PanelProsessStatus->Caption);
				break;
			case sysEND_PROG                :
	//			ProgrammRegim = NO_REGIM;                	//
				if(RobotStat == robSTARTING)
					SetRegimActivateProg();
				break;

			}  // SystemStatus
		}else  // � ������ ������
		{
			Fl_START_Acivated = false;
			short Er;
			// ���� ������ ������
			if(OptionForm->isChanal1StabilError())
			{
				ErrLevalPtr = Robot->GetErrorLevelAndText(LinkERR1);
			}
			else
			{
				Er = Robot->GetError();
				ErrLevalPtr = Robot->GetErrorLevelAndText(Er);
			}
			if(PlcError != Er)
			{
				if(ErrLevalPtr != NULL)
				{
					PlcError = Er;
					PanelProsessStatus->Caption = AnsiString(ErrLevalPtr->ErrText);
					PanelProsessStatus->Color   = ErrLevalPtr->Color;
				}
			}

			if(ErrLevalPtr != NULL)
			if(ErrLevalPtr->Blink == BLINK)
				if(PanelProsessStatus->Color == ErrLevalPtr->Color)
					PanelProsessStatus->Color   = clBtnFace;
				else
					PanelProsessStatus->Color   = ErrLevalPtr->Color;
		}
		Robot->Clear_GetProsStatusBitsDone();
	}__finally
	{
		InTimer = false;
	}

}

//---------------------------------------------------------------------------


void __fastcall TMainForm::bbResetRobotClick(TObject *Sender)
{
	if(Sender != NULL)
	if (MessageBox(GetActiveWindow(),"������� ������ �������!\n"
		"������ ��� ���������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;

	if(Fl_START_Acivated == false)
	{
		Robot->Init(0);
		SystemStatus = sysLAST;				//������� ���������
		ProsessStatusLine->ReSetStatus();
		bbEditProgActivate->Visible = true;
		bbToEdit->Visible = true;
		Fl_CanRepeat = true;
		Robot->SetControl_LasrKadr();
		bbEditVisio->Visible = false;

	}
	Robot->ReSetControlPC_Ready();

	ProgrammRegim = RESET;//NO_REGIM;                	//
	StringGrid1->TopRow = Line = Line_show = 1;
	StringGrid1->Refresh();

//	ColorButtonTop(bbPAUSE,clYellow);
	ColorButtonTop(bbPAUSE,clWindow);

//	ColorButtonTop(bbSTOP,clRed);
 //	ColorButtonTop(bbResetRobot,clYellow);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditNewLineClick(TObject *Sender)
{
	StringGrid1->RowCount++;
	RenumerateGrid();		// �������������� ������
}

//---------------------------------------------------------------------------
 void __fastcall TMainForm::RenumerateGrid()		// �������������� ������
 {
	for(int i=1; i < StringGrid1->RowCount; i++)
	{
			StringGrid1->Cells[0][i] = i;
	}
 }
 //---------------------------------------------------------------------------



void __fastcall TMainForm::StringGrid1KeyPress(TObject *Sender, char &Key)
{
/*	if(X == 1)
		StringGrid1->Options= StringGrid1->Options>>goEditing;
	else
		StringGrid1->Options= StringGrid1->Options<<goEditing;
*/
//	SetStatLine(0,"��� KeyPress");
	if(X == 1)
		Key = VK_RETURN;
	else
	if((Key < '0') && (Key != VK_TAB)&&(Key != ',')&&(Key != VK_BACK)&&(Key != '-') ||(Key > '9'))
		Key = 0;
/*
	if((Key < '0') && (Key != VK_TAB) ||(Key > '9'))
 		Key = VK_RETURN;
*/
}
//---------------------------------------------------------------------------
#include <fstream.h>
#include <iostream.h>



void __fastcall TMainForm::BitBtn3Click(TObject *Sender)
{
	OptionForm->Show();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button4Click(TObject *Sender)
{
BROWSEINFO bi;
char dir[MAX_PATH];
bi.hwndOwner = this->Handle;
bi.pidlRoot = NULL;
bi.pszDisplayName = dir;
bi.lpszTitle = "����� ��������";
bi.ulFlags = BIF_RETURNONLYFSDIRS;
bi.lpfn = NULL;
LPITEMIDLIST id = SHBrowseForFolder(&bi);
if(id)
  {
  SHGetPathFromIDList(id, dir);
  Edit37->Text = dir;
  }
 
 

}
//---------------------------------------------------------------------------





void __fastcall TMainForm::BitBtn4Click(TObject *Sender)
{
	SysPar->SaveToFile();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn11Click(TObject *Sender)
{
	sSys *pSysPar = SysPar->GetSysData();
	Robot->SetSysParam( pSysPar);
	EventToFile("� ���������� ��������� ����� ��������� ���������");
}
//---------------------------------------------------------------------------
//������ "�������� ������ �������"
void __fastcall TMainForm::bbClearMessengerClick(TObject *Sender)
{
//	StringGrid2->
	AnsiString s = Now().CurrentDateTime().DateTimeString()+" -- �������� ������ �������";

	AnsiString FileName   = ChangeFileExt(Application->ExeName,".evn");

	if (!FileExists(FileName))
	{
		ShowMessage("������ ������� ����������.\n ������ ����� ������ �������!");
		s = Now().CurrentDateTime().DateTimeString()+" -- �������� ������ �������";
	}else
		if(Sender == NULL) return;				// ���� ��� ��� �������� ���� - �� �������

	ofstream fout;
	fout.open(FileName.c_str(),ios::out); // ��������� ������ � ������
//	AnsiString s = Now().CurrentDateTime().DateTimeString()+" -- �������� ������ �������";
	fout << s.c_str()<<"\n";
	fout.close();
	if(Sender != NULL)
		ts7_MassengerShow(NULL);

}
//---------------------------------------------------------------------------
// �������� ������� �������
void __fastcall TMainForm::ts7_MassengerShow(TObject *Sender)
{

//	if(PageControl1->ActivePageIndex == 6)
	{

		bbClearMessengerClick(NULL);

		AnsiString FileName   = ChangeFileExt(Application->ExeName,".evn");

		if (!FileExists(FileName))
		{
			ShowMessage("�� ������� ������� ������ �������!");
			StringGrid2->RowCount = 1;

			return;
		}

		try
		{
			TStringList *List = new TStringList();
	//		Robot->ClearProg();
			try
			{
				List->LoadFromFile(FileName);
				int RowCount = List->Count+1;               // ��������� ���������� �����
				StringGrid2->RowCount = RowCount;
		//		int Ind =0;
				for (int i=1; i<RowCount; i++)
				{
					AnsiString s = List->Strings[i-1];
					for (int j=0; j<StringGrid2->ColCount; j++)
					{
						AnsiString cell;

						int Pos = s.Pos("--");
						if(Pos == 0)
							cell =  s;
						else
							cell =  s.SubString(0,Pos-1);
						StringGrid2->Cells[j][i] = cell;
						if(Pos == 0)
							break;
						s = s.Delete(1,Pos+2);
					}
				}
			}__finally
			{
				delete List;
			}

		} catch ( ... ) {
			ShowMessage("������ ��� �������� ������� �������.");
			LoadError = 11;
			return;
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbPAUSEClick(TObject *Sender)
{

	if(SystemStatus == sysACTIVATED)
		Robot->SetControlPause();
//	ColorButtonTop(bbPAUSE,clYellow);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	Timer1->Enabled = false;
	SysPar->SaveToFileDat();

	if(ghRC)
	{
	  wglMakeCurrent(ghDC,0);
	  wglDeleteContext(ghRC);
	}
	if(ghDC)
	  ReleaseDC(Handle, ghDC);



}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbUsersClick(TObject *Sender)
{
//	PasswordDlg = new TPasswordDlg(Application);
	PasswordDlg = new TPasswordDlg(NULL);
	PasswordDlg->ShowModal();
	delete PasswordDlg;
}
//---------------------------------------------------------------------------
// ��������� �������������� ������ ������� ����
void __fastcall TMainForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_ESCAPE)
	{
		if(PageControl1->ActivePageIndex != 0)
			PageControl1->ActivePageIndex = 0;
	}
	else
	switch(Key)
	{
			case  VK_F1:       // ��������� ��������� ��� ���������
				bbToLoadProgClick(Sender);
				break;
			case  VK_F2:       // �������������� ���������
				bbToEditProgClick(Sender);
				break;
			case  VK_F3:       // �������������� �������
				sbToEditMacroClick(Sender);
				break;
			case  VK_F4:       // ����� ���� ������
				sbChoiceWeldTypeClick(Sender);
				break;
			case  VK_F5:       // �������������������� ������ ������� �����
				sbConstructorClick(Sender);
				break;
			case  VK_F6:       // ������������� ��������� ���������
				if(MainForm->bbSysParam->Visible == true)
					bbSysParamClick(Sender);
				break;
			case  VK_F7:       // �������� ������� ��������
				bbCurrentValueClick(Sender);
				break;
			case  VK_F8:       // �� ������������
				bbServiceClick(Sender);
				break;
			case  VK_F9:       // ����� ������������
				bbUsersClick(Sender);
				break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbEditVisioClick(TObject *Sender)
{
	PageControl1->ActivePageIndex = 5;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ts6_MINMAX_EditEnter(TObject *Sender)
{
	if(ProgrammRegim == ACT)
		BitBtn29->Visible = true;
	else
		BitBtn29->Visible = false;
}
//---------------------------------------------------------------------------
/*
int Connect(AnsiString IP,������ ���������);   //  �����������
	���,
		����� ������� - ��������� �����������: 0 - �������, 1.. ��� ������ (�� ����� ����������� )
		AnsiString IP - ������ � IP ������� � ����: "192.168.0.1"
		������ ����������� ��������  - ������ ����������� ��������� �� ���� ����������(��������� ��������, �������� ������ ���.)

int DisConnect();                              // ����������
	���,
		����� ������� - ��������� ����������: 0 - �������, 1.. ��� ������ (�� ����� ����������� )

int WriteArray(short *pData,int Size);         // �������� ������ � BUSINTX11 (���� �������)
		����� ������� - ��������� ����������: 0 - �������, 1.. ��� ������ (�� ����� ����������� )
		short *pData  - ��������� �� ������ ��� ��������
		int Size	  - ������ �������� � short

int WriteWord(short Data,int Addres);         // �������� ������ � BUSINTX11 (���� �����)
		����� ������� - ��������� ����������: 0 - �������, 1.. ��� ������ (�� ����� ����������� )
		short Data    - ����� ������
		int Addr	  - ����� � ������� (0-������ ����� � �������)

int ReadArray(short *pData,int Size,int *ActualSize);  // �������� ������ �� BUSINTX11 (���� �������)
		����� ������� - ��������� ����������: 0 - �������, 1.. ��� ������ (�� ����� ����������� )
		short *pData  - ��������� �� ����� ������
		int Size	  - ������ �������� � short
		int *ActualSize - ��������� �� �����, ������� �������� ���������� ���������� ����������� ����

int ConnectionStatus();						   // �������� ��������� �����
		����� ������� - ��������� ����� 0 - ����������, ������ ���, 1 - �� ����������, 2.. ��� ������ (�� ����� ����������� )
*/
void __fastcall TMainForm::BitBtn29Click(TObject *Sender)
{
	PageControl1->ActivePageIndex = 1;

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button5999Click(TObject *Sender)
{
  				PanelProsessStatus->Color   = clRed;

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbEditPastClick(TObject *Sender)
{
	StringGrid1->RowCount++;
	int rc = StringGrid1->RowCount-1;
	int trc = rc;
	for(int i=Line; i < trc; i++)
	{
		StringGrid1->Rows[rc] = StringGrid1->Rows[rc-1];
		StringGrid1->Cells[0][rc] = rc;
    	rc--;
	}
	StringGrid1->Rows[Line]->Clear();
	StringGrid1->Cells[4][Line] = "��";
//	StringGrid1->Cells[0][trc] = trc;
	StringGrid1->Cells[0][Line] = Line;

}
//---------------------------------------------------------------------------




//  ������������ ����������� ������
void __fastcall TMainForm::evSKeyPress(TObject *Sender, char &Key)
{
	TEdit *eEd = (TEdit*)Sender;
	bool Dop;
	if(eEd->Tag == 1)      // ��� ���������� � MiG ���� ��������� �����
		Dop = (Key != '-');
	else
		Dop = true;

	if((Key < '0') && (Key != VK_TAB)&&(Key != ',')&&(Key != VK_BACK)&&Dop ||(Key > '9'))
		Key = 0;
	else
    	SysParChanged = true;

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbImpEnableClick(TObject *Sender)
{
/*
	bool bv = cbImpEnable->Checked;
	if(cbProcTypeMacro->ItemIndex == 1)	//������� MiG
		 bv = false;
	StaticText51->Visible = bv; etp->Visible = bv; StaticText49->Visible = bv;
	StaticText54->Visible = bv; etg->Visible = bv; StaticText52->Visible = bv;
*/	
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox1Click(TObject *Sender)
{
//		bool bv = CheckBox1->Checked;
/*
		StaticText74->Visible = bv; Edit29->Visible = bv; StaticText72->Visible = bv;
		StaticText77->Visible = bv; Edit27->Visible = bv; StaticText76->Visible = bv;
*/
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::evSExit(TObject *Sender)
{
	int Tag = ((TEdit *)Sender)->Tag;
	int V;
	bool Mig = true;
	if(Tag == 2) Tag = 1;
	try
	{
		V = ((TEdit *)Sender)->Text.ToDouble() * 10;
	} catch ( ... ) {
		ShowMessage("������� ���������������� ��������.");
		if(Tag == 1)
			if(Mig)	//������� �iG
				((TEdit *)Sender)->Text = "10";
			else
				((TEdit *)Sender)->Text = "45";
		else
			((TEdit *)Sender)->Text = "0,1";
		return;
	}

	switch(Tag)
	{
	case 0:
		if(V > 800)		V = 800;
		break;
	case 1:
		if(Mig)	//������� �iG
		{
			if(V >= 0)
			{
				if(V > 120)		V = 120;
//				else if(V < 2)	V = 2;
			}
			else
			{
				if(V < -120)	V = -120;
//  			else if(V > -40)V = -40;
			}
		}else
			if(V > 3500)	V = 3500;
			else if(V < 450)	V = 450;
		break;
	case 2:
		if(V > 10000)	V = 10000;	//����� ��������/�����
		break;
	case 3:
		if(V > 250)		V = 250;   	//���������� ��������
		break;
	case 4:
		if(V > 1000)	V = 1000;	//�������
		break;
	case 5:
		if(V > 100)		V = 100;	//�������� ������ ���������
		break;
	case 6:
		if(V > 1900) 	V = 1900;	//�������� ����������
		break;
	case 7:
		if(V > 99)		V = 99;		//��������
		break;
	case 8:
		if(V > 1000) 	V = 1000;	//���������
		break;
	case 9:
		break;
	}
	((TEdit *)Sender)->Text = Double(V)/10;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button1Click(TObject *Sender)
{
	SysPar->ClearCommonGaz();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button2Click(TObject *Sender)
{
	SysPar->ClearShiftGaz();
}
//---------------------------------------------------------------------------
/*
void __fastcall TMainForm::BitBtn5Click(TObject *Sender)
{
	PageControl1->ActivePageIndex = 7;

}
*/
//---------------------------------------------------------------------------


/*��� ������
http://www.divinecosmosunion.net/t1133-topic


�, ���� - ����, ���� �����,
  �������� �������� ������,
�� ��� ��������� ��� �������
  ��� ������������ ������!.

� �� �������� ����������,
  �� ������, ��������� ����,
�� ������� � ������������,
  ������������ �����,

�� ���, ������� ��������,
  ������ ��������� ����,
�� �������� ������ �����
  � ������� ������ �� ������ ���!

� ���� - ����, ���� ����,
  ������ ����� � ��� ���.
����� ������� ��� � �����,
  �� �����-����� ������� ���

�� ����� ������� �����
  �������� ������� ������
���� ��� ����� ����� �����
  � ���������� ����� �����.

*/
void __fastcall TMainForm::bbShowEmulationClick(TObject *Sender)
{
	TProfil *Master;
	if(Sender == NULL)                      // �� ��������� ���������� �������
	{

		Master = new TProfil;	//������� ��������� ����������

		Master->H1 = ed_H1->Text.ToDouble(); 	// ������ ������������ ������� ���������
		Master->H2 = ed_H2->Text.ToDouble(); 	// ������� ��������� ������� ���������
		Master->H3 = ed_H3->Text.ToDouble(); 	// ������� ������������ ������� ���������
		Master->L1 = ed_L1->Text.ToDouble(); 	// ���������� �� �������� (�� ����� ������)
		Master->A  = ed_A->Text.ToDouble();		// ���� �������
		Master->D  = ed_Dsp->Text.ToDouble();	// ������� ���������� ���������
		//-----------
		Master->Vcb = ed_Vcb->Text.ToDouble();   // �������� ������
		Master->S  = ed_Step->Text.ToDouble();	// ������ ����
		Master->d  = ed_Dwire->Text.ToDouble();	// ������� ���������
		Master->Vw = ed_V->Text.ToDouble();		// �������� ������ ���������
		Master->wType = RadioGroup1->ItemIndex;	// ��� ��������� 0 - ����������, 1 - �������
		int  Res = WorkShape->SetParam(Master,eMashtab);
		delete Master;
		if(Res)  								// ������ ��������?
		{
			MessageBox( Handle, "���������������� ������� D", "��������� �������", MB_ICONINFORMATION );
			return;
		}
	}
	if(Sender != NULL)                      // �� ��������� ���������� �������
	{
		if(WorkShape->isEmulationGo() == false)
		{
			Master = new TProfil;	//������� ��������� ����������

			Master->H1 = ed_H1->Text.ToDouble(); 	// ������ ������������ ������� ���������
			Master->H2 = ed_H2->Text.ToDouble(); 	// ������� ��������� ������� ���������
			Master->H3 = ed_H3->Text.ToDouble(); 	// ������� ������������ ������� ���������
			Master->L1 = ed_L1->Text.ToDouble(); 	// ���������� �� �������� (�� ����� ������)
			Master->A  = ed_A->Text.ToDouble();		// ���� �������
			Master->D  = ed_Dsp->Text.ToDouble();	// ������� ���������� ���������
			//-----------
			Master->Vcb = ed_Vcb->Text.ToDouble();   // �������� ������
			Master->S  = ed_Step->Text.ToDouble();	// ������ ����
			Master->d  = ed_Dwire->Text.ToDouble();	// ������� ���������
			Master->Vw = ed_V->Text.ToDouble();		// �������� ������ ���������
			Master->wType = RadioGroup1->ItemIndex;	// ��� ��������� 0 - ����������, 1 - �������
			int  Res = WorkShape->SetParam(Master,eMashtab);
			delete Master;
			WORD Key = VK_RETURN;
			ed_H1KeyDown(Edit14, Key,(TShiftState)0);
			if(Res)  								// ������ ��������?
			{
				MessageBox( Handle, "���������������� ������� D", "��������� �������", MB_ICONINFORMATION );
				return;
			}

			WorkShape->ShowEmulation();
			bbShowEmulation->Caption = "������� ����";
		}else
		{
			WorkShape->StopEmulation();
			bbShowEmulation->Caption = "������� �����";
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ed_H1KeyDown(TObject *Sender, WORD &Key,
	  TShiftState Shift)
{
   if ((Key == VK_RETURN)){
		try {
			double V = ((TEdit*)Sender)->Text.ToDouble();
			switch (((TEdit*)Sender)->Tag) {
			case 0:
			case 1:
			case 2:;
			case 10:;
				  WorkShape->SetInterval(V*1000);
			default:
				;
			}
		} catch (...) {
			MessageBox( Handle, "���������������� ����", "��������� �������", MB_ICONINFORMATION );
		}
   }

}
//---------------------------------------------------------------------------
// ����� �������� ������� ��������������
void __fastcall TMainForm::ts9_MasterShow(TObject *Sender)
{
//	WorkShape->ShowEmulation();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OnMyMessage(TMessage& Msg)
{

	if(WorkShape->DrawNewWeld(Msg.LParam) == false)		//����� ��� ���������.���� false - �� ���������
	{
		bbProgrammRun->Caption = "�������� ���������";
		bbShowEmulation->Caption = "������� �����";
	}

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn9Click(TObject *Sender)
{
//�������������� ���������
 //	SetRegimActivateProg();
/*	SetRegimEditProg();
	PageControl1->ActivePageIndex = 1;		//������������� �� �������� ���������
	WorkShape->MakeProgramm(eMacroProgGen->Text,StringGrid1);
	RenumerateGrid();		// �������������� ������
*/
	bMakeTipProgrammClick(NULL);

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BitBtn7Click(TObject *Sender)
{

//	BitBtn4->Visible = true;
//	BitBtn3->Visible = true;
//	PageControl1->ActivePageIndex = 3; onMouseWheelDown
	OptionForm->Show();


}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormMouseWheelDown(TObject *Sender,
	  TShiftState Shift, TPoint &MousePos, bool &Handled)
{
	if(PageControl1->ActivePageIndex == 8)		//��� �������� ���������
		WorkShape->SetMashtab(MousePos,0,eMashtab);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormMouseWheelUp(TObject *Sender, TShiftState Shift,
	  TPoint &MousePos, bool &Handled)
{
	if(PageControl1->ActivePageIndex == 8)		//��� �������� ���������
		WorkShape->SetMashtab(MousePos,1,eMashtab);
}
//---------------------------------------------------------------------------
// ��� ����� ���� ������ �� ���� �������������� �������
/*���
void __fastcall TMainForm::cbProcTypeMacroChange(TObject *Sender)
{
	evSExit(elp);
//	if(cbProcTypeMacro->ItemIndex == 1)	//������� �iG
	{
		GroupBox6->Visible = false;
		cbWire_enable->Visible = false;
		cbImpEnable->Visible = true;
		StaticText47->Visible = true;elp->Visible = true;StaticText26->Visible = true;	 // ��� ���������� �����
		StaticText47->Caption ="���������� ���������";StaticText26->Caption ="�";	 // ���������� ���������


		StaticText50->Visible = false;elg->Visible = false;StaticText48->Visible = false;// ��� ������� �����
	}
���  ������ ����� �� ������ MiG
	else                                //������� TiG
	{
		GroupBox6->Visible = true;
		cbWire_enable->Visible = true;
		cbImpEnable->Visible = false;
		StaticText47->Visible = false;elp->Visible = false;StaticText26->Visible = false; // ��� ���������� �� �����
		StaticText47->Caption ="��� ����������";StaticText26->Caption ="�";	 // ��� ���������� �� ����� ���������� ��� MiG
		StaticText50->Visible = true;elg->Visible = true;StaticText48->Visible = true;	 // ��� ������� �����
	}

}
*/
//---------------------------------------------------------------------------
// ��� ����� ���� ������ �� ���� �������������� ���������(��� �����)
void __fastcall TMainForm::TypeMacroChange()
{
//	cbImpEnableClick(NULL); ����� �� �� �����
	evSExit(Edit24);
//	if(TiG_MiG != 1)	//������� �iG
	{
		GroupBox20->Visible = false;
		CheckBox4->Visible = false;
		CheckBox1->Visible = true;
//		cbImpEnable->Visible = false;
		StaticText70->Visible = true;Edit24->Visible = true;StaticText71->Visible = true;	 // ��� ���������� �����
		StaticText70->Caption ="���������� ���������";StaticText71->Caption ="�";	 // ���������� ���������


		StaticText73->Visible = false;Edit26->Visible = false;StaticText67->Visible = false;// ��� ������� �����
//		EditMacroView->SetMacroFaceMiG();
	}
/*���  ������ ����� �� ������ MiG

	else                                //������� TiG
	{
		GroupBox20->Visible = true;
		CheckBox4->Visible = true;
		CheckBox1->Visible = false;
//		cbImpEnable->Visible = true;
		StaticText70->Visible = false;Edit24->Visible = false;StaticText71->Visible = false;	 // ��� ���������� �����
		StaticText70->Caption ="��� ����������";StaticText71->Caption ="�";	 // ��� ���������� �� ����� ���������� ��� MiG
		StaticText73->Visible = true;Edit26->Visible = true;StaticText67->Visible = true;	 // ��� ������� �����
		EditMacroView->SetMacroFaceTiG();
	}
*/	
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::Image6Click(TObject *Sender)
{

	WeldType = ((TComponent *)Sender)->Tag;
	switch(WeldType)
	{
	case 0:                         // �����
		Shape5->Visible = true;
		Shape6->Visible = false;
		Shape7->Visible = false;
		Shape1->Visible = false;
		PageControl1->ActivePageIndex = 9;
	break;
	case 1:                         //
		Shape5->Visible = false;
		Shape6->Visible = true;
		Shape7->Visible = false;
		Shape1->Visible = false;
		PageControl1->ActivePageIndex = 10;
	break;
	case 2:                         //
		Shape5->Visible = false;
		Shape6->Visible = false;
		Shape7->Visible = true;
		Shape1->Visible = false;
		PageControl1->ActivePageIndex = 11;
	break;
	case 3:                         //
		Shape5->Visible = false;
		Shape6->Visible = false;
		Shape7->Visible = false;
		Shape1->Visible = true;
		gbAutoMakePrg->Visible = (((TComponent *)Sender)->Tag == 3);
		WeldType = 3;
		PageControl1->ActivePageIndex = 7;

	break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn10Click(TObject *Sender)
{
	PageControl1->ActivePageIndex = 8;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button8Click(TObject *Sender)
{
	OpenFileForm->InitData(true);
	OpenFileForm->ShowModal();
	AnsiString MacroFileName = OpenFileForm->GetFileName();
	if(WeldType == 0)
	{
		eMacroFileName->Text = MacroFileName;
	}else if(WeldType == 1)		// ����� ���� �������� ���
	{
		eMacroFileName1->Text = MacroFileName;
	}else if(WeldType == 2)		// ����� ���� �������� ���
	{
		eMacroFileName2->Text = MacroFileName;
	}else if(WeldType == 3)		// ����� ������� ����������
	{
		if(((TBitBtn*)Sender)->Tag == 0)
			eMacroProgGenStart->Text = MacroFileName;
		else
			eMacroProgGenWork->Text = MacroFileName;
	}

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bMakeTipProgrammClick(TObject *Sender)
{
	AnsiString MacroFileName;
	WORD Key = VK_RETURN;
	if(WeldType == 0)
	{
		MacroFileName = eMacroFileName->Text;
		ShiftType(1);
	}
	else if(WeldType == 1)
	{
		MacroFileName = eMacroFileName1->Text;
		ShiftType(1);
	}else if(WeldType == 2)
	{
		MacroFileName = eMacroFileName2->Text;
		ShiftType(2);
	}else if(WeldType == 3)
	{
		MacroFileName = eMacroProgGenStart->Text;
		ShiftType(3);
	}

	if(MacroFileName == "")
		if (MessageBox(GetActiveWindow(),"������ �� ������!\n"
			"���������� ��� �������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;
	if (MessageBox(GetActiveWindow(),"����� ������� ����� ��������. ���� ��������������� ������ ���������, ��� ����� ��������!\n"
		"����������?","��������!",MB_YESNO|MB_ICONQUESTION) == ID_NO) return;
	if(WeldType != 3)
		Edit36KeyDown(Edit36, Key, (TShiftState)0);
	SetRegimEditProg();

	AnsiString wpName = "\\Program";
	StringGrid1->MakeNewMacroList(wpName);
	if(WeldType == 2)		// ���� 90 ��.
	  eAngle->Text = "90,0";
	else
	  eAngle->Text = "0,0";
	PageControl1->ActivePageIndex = 1;		//������������� �� �������� ���������
	if(WeldType == 3)
	{
		bbShowEmulationClick(NULL);	//������ ��������� ��������� �������
		WorkShape->MakeProgramm(MacroFileName,eMacroProgGenWork->Text,StringGrid1);
		bbProgrammRun->Enabled = true;
	}
	else
		MakeProgramm(MacroFileName);
	RenumerateGrid();		// �������������� ������



}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void  __fastcall TMainForm::MakeProgramm(AnsiString MacroName)
{
	int Line = 1;

	int Start =  Pos_Start;
	int Summa = 0;
	if(WeldType == 2)
		Start =  D_Start;
	if(WeldType == 1)		// ������������ �������(����� ���� �������� ���)
	{
		StringGrid1->RowCount = StepCount * LayerCount + (LayerCount-1)*1+1;
		Start =  D_Start;
	}
	else
		StringGrid1->RowCount = StepCount * LayerCount+1;
	for (int j=0; j< LayerCount; j++)
	{
		for (int i=0; i< StepCount; i++)
		{
			if(WeldType == 2)		// �������������� �������
			{
				StringGrid1->Cells[1][Line] = MacroName;
				StringGrid1->Cells[2][Line] = (double)(Start)/10;
				StringGrid1->Cells[3][Line] = (i != (StepCount-1))? (double)(StepSize)/10:0;
//				(i != (StepCount-1))? (double)(StepSize)/10:0;
				StringGrid1->Cells[4][Line] = "��";
				if(i == (StepCount-1))
				{
					StepSize = -StepSize;
					Start+= Thickness;
				}
//				else
//					Start+= StepSize;

			}else
			if(WeldType == 1)		// ����� ���� �������� ���
			{
				StringGrid1->Cells[1][Line] = MacroName;
				StringGrid1->Cells[2][Line] = (double)(Start)/10;
				StringGrid1->Cells[3][Line] = (i != (StepCount-1))? (double)(abs(StepSize))/10:0;
				StringGrid1->Cells[4][Line] = "��";
				if(i != (StepCount-1))
					Summa+= StepSize;

			}else
			{
				StringGrid1->Cells[1][Line] = MacroName;
				StringGrid1->Cells[2][Line] = (double)(Start)/10;
				StringGrid1->Cells[3][Line] = (i == (StepCount-1))&&(j != (LayerCount-1))? (double)(Thickness)/10:0;
				StringGrid1->Cells[4][Line] = "��";
				if(i == (StepCount-1))
					StepSize = -StepSize;
				else
					Start+= StepSize*2;
			}
			Line++;
		}
		if(WeldType == 1)		// ����� ���� �������� ���
		{
			if(j != (LayerCount-1))
			{
				Start+= Thickness;
				StringGrid1->Cells[1][Line] = MacroName;
				StringGrid1->Cells[2][Line] = (double)(Start)/10;
				StringGrid1->Cells[3][Line] = (double)(-Summa)/10;
				StringGrid1->Cells[4][Line] = "���";
				Summa = 0;
				Line++;
			}
		}/*else if(WeldType == 2)
		{
			if(j != (LayerCount-1))
			{
				Start+= Thickness;
				StringGrid1->Cells[1][Line] = MacroName;
				StringGrid1->Cells[2][Line] = (double)(Start)/10;
				StringGrid1->Cells[3][Line] = (double)(-Summa)/10;
				StringGrid1->Cells[4][Line] = "���";
				Summa = 0;
				Line++;
			}
		}  */
	}
}




void __fastcall TMainForm::Edit36KeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	DirOut = true;
	double	dStepSize;      // ������ ���� * 10 (��)
	double	dD_Start;       // ������� ���������
	double 	dPos_Start;     // �������/������� ���������
	double 	dPos_End;       // �������/������� ��������
	double 	dLayerCount;    // ���������� �����
	double 	dThickness;		// ������� ����

   if ((Key == VK_RETURN)){
		try {
			if(WeldType == 0)
			{
				DirOut = RadioGroup2->ItemIndex == 1;
				dStepSize = Edit36->Text.ToDouble();       	// ������ ���� * 10 (��)
				dD_Start = dPos_Start = DirOut?Edit15->Text.ToDouble():Edit19->Text.ToDouble();     		// �������/������� ���������
				dPos_End = DirOut?Edit19->Text.ToDouble():Edit15->Text.ToDouble();       		// �������/������� ��������
				dLayerCount = Edit35->Text.ToInt();       	// ���������� �����
				dThickness  = Edit48->Text.ToDouble();      // ������� ����
				if((DirOut && (dPos_Start >= dPos_End)) || (!DirOut && (dPos_Start <= dPos_End)))
					throw 1;
				StepCount = fabs(dPos_Start - dPos_End)/2/dStepSize+1;       	// ���������� ����� (���������� �������� � �� �������=>���� �������� �� 2)
				stSteps->Caption = StepCount;
				stRemainder->Caption = (double)((int)((fabs(dPos_Start - dPos_End)/2 - (StepCount-1)* dStepSize)*100))/100;
			}else if(WeldType == 1)
			{
				DirOut = RadioGroup3->ItemIndex == 1;
				dStepSize = Edit42->Text.ToDouble();       	// ������ ���� * 10 (��)
				dD_Start  = Edit38->Text.ToDouble();	   	// ������� ���������
				dPos_Start= 0;
				dPos_End  = Edit39->Text.ToDouble();   		// ������� ��������
				dLayerCount = Edit40->Text.ToInt();       	// ���������� �����
				if(dLayerCount != 1)
				{
					dLayerCount = 1;
					Edit40->Text = 1;
				}
				dThickness  = Edit49->Text.ToDouble();      // ������� ����
				if(DirOut == 1) dThickness  = -dThickness;
				StepCount = fabs(dPos_Start - dPos_End)/dStepSize+1;       	// ���������� �����
				stSteps1->Caption = StepCount;
				stRemainder1->Caption = (double)((int)((fabs(dPos_Start - dPos_End) - (StepCount-1)* dStepSize)*100))/100;

			}else if(WeldType == 2)
			{
				DirOut = RadioGroup4->ItemIndex == 0;
				double ss = dStepSize = Edit47->Text.ToDouble();       	// ������ ���� * 10 (��)
//				if(DirOut == 1) dStepSize = -dStepSize;
				dD_Start  = Edit43->Text.ToDouble();	   	// ������� ���������
				dPos_Start = 0;
				dPos_End = Edit44->Text.ToDouble();    		// ������� ��������
				dLayerCount = Edit45->Text.ToInt();       	// ���������� �����
				dThickness  = Edit50->Text.ToDouble();      // ������� ����
				StepCount = fabs(dPos_Start - dPos_End)/ss+1;       	// ���������� �����
				stSteps2->Caption = StepCount;
				stRemainder2->Caption = (double)((int)((fabs(dPos_Start - dPos_End) - (StepCount-1)* ss)*100))/100;

			}

			StepSize  = ((int)(dStepSize*10));       		// ������ ���� * 10 (��)
			D_Start   = ((int)(dD_Start*10));
			Pos_Start = ((int)(dPos_Start*10));     		// �������/������� ���������
			Pos_End   = ((int)(dPos_End*10));       		// �������/������� ��������
			LayerCount= dLayerCount;       					// ���������� �����
			Thickness = ((int)(dThickness*10));       		// ������� ����
			if(!DirOut) StepSize = -StepSize;

			double V = ((TEdit*)Sender)->Text.ToDouble();
			dPos_End = V;
			switch (((TEdit*)Sender)->Tag) {
			case 0:
			case 1:
			case 2:;
			case 10:;
			default:
				;
			}
		} catch (...) {
			MessageBox( Handle, "���������������� ����", "��������� ������������ ���������", MB_ICONINFORMATION );
		}
   }

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbExitToMainScreenClick(TObject *Sender)
{
		PageControl1->ActivePageIndex = 0;
}
//---------------------------------------------------------------------------




void __fastcall TMainForm::Image6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	WeldType = ((TComponent *)Sender)->Tag;
	if(WeldType == 0)                         // �����
	{
		Shape5->Visible = true;
		Shape6->Visible = false;
		Shape7->Visible = false;
		Shape1->Visible = false;
	}else if(WeldType == 1)
	{
		Shape5->Visible = false;
		Shape6->Visible = true;
		Shape7->Visible = false;
		Shape1->Visible = false;
	}else if(WeldType == 2)
	{
		Shape5->Visible = false;
		Shape6->Visible = false;
		Shape7->Visible = true;
		Shape1->Visible = false;
	}else if(WeldType == 3)
	{
		Shape5->Visible = false;
		Shape6->Visible = false;
		Shape7->Visible = false;
		Shape1->Visible = true;
	}

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ShiftType(int aType)  		// ��� ��������
{
	AnsiString ColsName0[] = {"#���","                                          ������","    �������","    ��������  ","   ���� ","  ������"};
	AnsiString ColsName1[] = {"#���","                                          ������","    �������","����.�������� ","   ���� ","  ������"};
	AnsiString ColsName2[] = {"#���","                                          ������","    �������","���. �������� ","   ���� ","  ������"};
	for(int i=0;i<StringGrid1->ColCount;i++)
		if(aType == 0)
			StringGrid1->Cols[i]->Text = ColsName0[i];
		else if((aType == 1)||((aType == 3)))
			StringGrid1->Cols[i]->Text = ColsName1[i];
		else
			StringGrid1->Cols[i]->Text = ColsName2[i];
}





void __fastcall TMainForm::bbProgrammRunClick(TObject *Sender)
{
	if(WorkShape->isEmulationGo() == false)
	{
		WORD Key = VK_RETURN;
		ed_H1KeyDown(Edit14, Key,(TShiftState)0);
		WorkShape->ShowRun();
		bbProgrammRun->Caption = "���� ������";
	}else
	{
		WorkShape->StopRun();
		bbProgrammRun->Caption = "�������� ���������";
	}

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button9Click(TObject *Sender)
{
	TPoint MousePos(20,60);
//	if((MousePos.x>10)&&(MousePos.x<(10+Width))  &&  (MousePos.y>50)&&(MousePos.x<(50+Height)))

	WorkShape->SetMashtab(MousePos,0,eMashtab);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button14Click(TObject *Sender)
{
	TPoint MousePos(20,60);
	WorkShape->SetMashtab(MousePos,1,eMashtab);
}
//---------------------------------------------------------------------------
/*

��������? ��������.
� ������ ����������� �����.
�������� ������ �� ���������.
������� ������� �������� -
������ �������� ���� -
������ ��� � �������� �����.
���� ���� ������ ������.
�� �� ��� � ����� ���������,
����� �������� ��� ���.
���� ��������� - ���� ����.
����� ��� ��������� ��������.
����� �� ������� �� ���.

325x176
*/





void __fastcall TMainForm::bbExitToChoiceScreenClick(TObject *Sender)
{
		PageControl1->ActivePageIndex = 8;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::sbConstructorClick(TObject *Sender)
{

		PanelProsessStatus->Visible = false;//true;
		StoryCaption = Caption;						// ������ ��� ����� ��������� �������� ����
		Caption = StoryCaption+"   �����������: NEW";
		PageControl1->ActivePageIndex = 12;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn2Click(TObject *Sender)
{
		PanelProsessStatus->Visible = true;
		Caption = StoryCaption;
		PageControl1->ActivePageIndex = 0;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::bbEditRenumerateClick(TObject *Sender)
{
	Line_Step = !Line_Step;

	if(Line_Step == false)
	{
		RenumerateGrid();		// �������������� ������
		StringGrid1->Cells[0][0] = "#���";
	}
	else		// ����������� ����
	{

		StringGrid1->Cells[0][0] = "#���";
		int StepIsNext=1;
		for(int i=1; i < StringGrid1->RowCount; i++)
		{
				StringGrid1->Cells[0][i] = StepIsNext;
				if((i < (StringGrid1->RowCount-1)) && (StringGrid1->Cells[3][i].ToDouble() > 0.0))
                	StepIsNext++;
		}

	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormResize(TObject *Sender)
{
  glViewport( 0, 0, PanelConstructor->Width, PanelConstructor->Height );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
//  glOrtho(-1,1, -1,1, 20,5);
  glOrtho(-1,1, -1,1, DepthXXX,12);
  gluLookAt(0,5,0, 0,0,0, 0,0,1);
  glMatrixMode( GL_MODELVIEW );

}
//---------------------------------------------------------------------------
BOOL bSetupPixelFormat(HDC hdc)
{
	  PIXELFORMATDESCRIPTOR pfd, *ppfd;
	  int pixelformat;
	  ppfd = &pfd;
	  ppfd->nSize = sizeof(PIXELFORMATDESCRIPTOR);
	  ppfd->nVersion = 1;
	  ppfd->dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	  ppfd->dwLayerMask = PFD_MAIN_PLANE;
	  ppfd->iPixelType = PFD_TYPE_RGBA;
	  ppfd->cColorBits = 16;
	  ppfd->cDepthBits = 16;
	  ppfd->cAccumBits = 0;
	  ppfd->cStencilBits = 0;
	  if ((pixelformat = ChoosePixelFormat(hdc, ppfd)) == 0)
	  {
		  MessageBox(NULL, "ChoosePixelFormat failed", "Error",	MB_OK);
		  return FALSE;
	  }
	  if (SetPixelFormat(hdc, pixelformat, ppfd) == FALSE)
	  {
		  MessageBox(NULL, "SetPixelFormat failed", "Error",	MB_OK);
		  return FALSE;
	  }
	  return TRUE;
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	 ghDC = GetDC(PanelConstructor->Handle);
	 if (!bSetupPixelFormat(ghDC))
		Close();
	 ghRC = wglCreateContext(ghDC);
	 wglMakeCurrent(ghDC, ghRC);
	 glClearColor(0.0, 0.0, 0.0, 0.0);
	 FormResize(Sender);
	 glEnable(GL_COLOR_MATERIAL);
	 glEnable(GL_DEPTH_TEST);
	 glEnable(GL_LIGHTING);
	 glEnable(GL_LIGHT0);
	   float p[4]={3,3,3,1},
			 d[3]={-1,-1,-3};
	 glLightfv(GL_LIGHT0,GL_POSITION,p);
	 glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,d);
}
//---------------------------------------------------------------------------
void TMainForm::Draw()
{
	glClear(GL_DEPTH_BUFFER_BIT ^ GL_COLOR_BUFFER_BIT);
	GLUquadricObj *quadObj;
	quadObj=gluNewQuadric();
	gluQuadricDrawStyle(quadObj, gleTp[GLenumTp]);//GLU_FILL);

//	display();

	glColor3f(1,0,0);

	gluCylinder (quadObj,0.2,0.3,0.2,20,20);
//	if(fl == 0)
		glTranslatef(0.0f, 0.0f, 0.5f);
	gluCylinder (quadObj,0.2,0.5,0.2,20,20);
	glTranslatef(0.0f, 0.0f, -0.5f);

//	gluSphere(quadObj, 0.5,20,20);
	glRotatef(3, 0,0,1);
	gluDeleteQuadric(quadObj);
	SwapBuffers(ghDC);
}


void __fastcall TMainForm::ts12_ConstructorShow(TObject *Sender)
{
	Draw();

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button5Click(TObject *Sender)
{
	Draw();
}
//---------------------------------------------------------------------------

