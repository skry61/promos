//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Simulate_PLC.h"
#include "Macro.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#include "ServiceClass.h"
TSimulPLC_Form *SimulPLC_Form;
//---------------------------------------------------------------------------
__fastcall TSimulPLC_Form::TSimulPLC_Form(TComponent* Owner)
	: TForm(Owner)
{
	GrafStatus = robNOT_READY;
	Imitator_Count  = 0;
	StartSimulator = false;
	SimulLookMacroView		= new TMacroView(
					  Edit23,ComboBox2,Edit31,Edit32,CheckBox14,rgAVC_Axis,
					  Edit24,Edit26,Edit29,Edit27,CheckBox21,
					  Edit16,CheckBox15,
					  Edit17,Edit20,Edit21,Edit22,CheckBox13,
					  eJOB_Number
	);
}
//---------------------------------------------------------------------------
__fastcall TSimulPLC_Form::~TSimulPLC_Form()
{
	delete SimulLookMacroView;
}

//---------------------------------------------------------------------------
void TSimulPLC_Form::Init()           // �������� ���
{
	*(WORD*)&BitControlByte = 0; // ������� ��������� ���������� � ����������
	GrafStatus = robSTARTING;
	SetEdit();
}

//---------------------------------------------------------------------------
void TSimulPLC_Form::SetBuffer(TLineToPLC *pLoadBuf)
{
	char Buf[300];
	LoadBuf[0] = *pLoadBuf;
	CodeBinToText(Buf,(char *)pLoadBuf,sizeof(TLineToPLC));
	StaticText1->Caption = AnsiString(Buf);
	SaveParam = 0;
}
//---------------------------------------------------------------------------
int	TSimulPLC_Form::GetWorkBuffer(TLineToPLC *pLoadBuf)
{
	*pLoadBuf = WorkBuf;
	return 0;
}

//---------------------------------------------------------------------------
TBitControlByte TSimulPLC_Form::GetProsControl()
{
	return BitControlByte;
}
//---------------------------------------------------------------------------
TBitStatusByte TSimulPLC_Form::GetProsStatusBits()
{
//	return BitStatusByte;
	return *(TBitStatusByte*)&BitControlByte;
}
//---------------------------------------------------------------------------
DWORD TSimulPLC_Form::GetProsStatusData()
{
//	return BitStatusByte;
	DWORD V = *(short*)&BitControlByte;
	return V  | (CurLine<<16);
}

//---------------------------------------------------------------------------
/*���
TBitStatusByteEx TSimulPLC_Form::GetProsStatusDataEx()
{
	return BitStatusByteEx;
}
*/
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlBufBusy()   {	BufBusy  = 1; }
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlPC_Ready()  {	PC_Ready = 1; }
//---------------------------------------------------------------------------
void TSimulPLC_Form::ReSetControlPC_Ready(){    PC_Ready = 0; }
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlPC_Err()	   {	PC_Err   = 1; }
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlPusk()           // ����
{
//	BitControlByte.Pusk = 1;
}

//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlStop()           // ����
{
 //	BitControlByte.Stop = 1;
}
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControlReset()          // �����
{
//	BitControlByte.Reset = 1;
}
//---------------------------------------------------------------------------
// ������� �� ����� �� PC
void TSimulPLC_Form::SetControlPause() 	    {	ToPausePC = 1; }
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControl_LasrKadr()  {	LasrKadr = 1;  }
//---------------------------------------------------------------------------
void TSimulPLC_Form::ClearControlSaveParam(){	SaveParam = 0; }

//---------------------------------------------------------------------------
void __fastcall TSimulPLC_Form::Timer1Timer(TObject *Sender)
{
	if(Fl_NewData)
	{
		Fl_NewData = false;
		DrawControl();
	}


	if(cbSimulOn->Checked)
	{
		switch (GrafStatus) {
		case  robNOT_READY:    	// ���������
			if(Imitator_Count++ == 5)
			{
				Imitator_Count = 0;
				GrafStatus = robREADY;
				cbRegim->ItemIndex = GrafStatus;
			}
			break;
		case  robREADY:     // ���������
			break;
		case  robSTARTING:  // ���������
			DataReady = 1;
			if(BufBusy)
			if(Imitator_Count++ == 3)
			{
				Imitator_Count = 0;
				GrafStatus = robSTART;
				cbRegim->ItemIndex = GrafStatus;
//				BufBusy = 0;
			}
			break;
		case  robSTART:     // ���������
			if(BufBusy||LasrKadr)
			if(Imitator_Count++ == 5)
			{
				char Buf[300];
				WorkBuf = LoadBuf[0];     		// ����� - ���������� � ������� �����     manager@promos-ls.ru
				CurLine = WorkBuf.Line;
				CodeBinToText(Buf,(char *)&WorkBuf,sizeof(TLineToPLC));
				StaticText4->Caption = AnsiString(Buf);
				SimulLookMacroView->LoadMacroFromRobot((TLineToPLC*)&WorkBuf);
				if(CurLine == 4)
					Imitator_Count = -10;
				else
					Imitator_Count = 0;
				if(BufBusy == 0)
				{
					GrafStatus = robFINISHING;
					cbRegim->ItemIndex = GrafStatus;
				}
				BufBusy = 0;
			}
			break;
		case  robPAUSING:   // ���������
			if(Imitator_Count++ == 5)
		{
			Imitator_Count = 0;
			GrafStatus = robPAUSE;
			cbRegim->ItemIndex = GrafStatus;
		}
			break;
		case  robPAUSE:     // ���������
			break;
		case  robFINISHING: // ���������
			if(Imitator_Count++ == 5)
		{
			Imitator_Count = 0;

			GrafStatus = robFINISH;
			cbRegim->ItemIndex = GrafStatus;
			Pusk = 0;
			LasrKadr = 0;
		}
			break;
		case  robFINISH:    // ���������
        	DataReady = 0;
			break;
		case  robERROR:     // ���������
			break;

		default:
			;
		}
	}

}
//---------------------------------------------------------------------------
void __fastcall TSimulPLC_Form::bJOGClick(TObject *Sender)
{
	if((GrafStatus ==  robNOT_READY)||
	   (GrafStatus ==  robPAUSE)|| (GrafStatus ==  robFINISH)|| (GrafStatus ==  robERROR))
		GrafStatus =  robREADY;

}
//---------------------------------------------------------------------------
void __fastcall TSimulPLC_Form::cbRegimClick(TObject *Sender)
{
	 GrafStatus =  cbRegim->ItemIndex ;
}
//---------------------------------------------------------------------------

/*
int GetProsStatus()
{
	return SimulPLC_Form->GetProsStatus();
}
*/

void __fastcall TSimulPLC_Form::Edit1KeyPress(TObject *Sender, char &Key)
{
   if ((Key == VK_RETURN)){
		try {
			Error = Edit1->Text.ToInt();
		} catch (...) {
			MessageBox( Handle, "���������������� ����", "��� ������", MB_ICONINFORMATION );
		}
   }

}
//---------------------------------------------------------------------------
// ������ ������ "����"
void __fastcall TSimulPLC_Form::bPuskClick(TObject *Sender)
{
	if((GrafStatus ==  robNOT_READY)|| (GrafStatus ==  robREADY)||
	   (GrafStatus ==  robPAUSE)|| (GrafStatus ==  robFINISH))
	{
		GrafStatus =  robSTARTING;
		Pusk = 1;
		LasrKadr = 0;
		CurLine = 0;
	}
}

//---------------------------------------------------------------------------
// ������ ������ "����"
void __fastcall TSimulPLC_Form::Button1Click(TObject *Sender){	GrafStatus =  robFINISHING;	Pusk = 0;}

//---------------------------------------------------------------------------
// �������� ��� ����������/���������
void __fastcall TSimulPLC_Form::CheckBox1Click(TObject *Sender)
{
	TCheckBox *cb = (TCheckBox *)Sender;
	if(HelpContext == 1)
	{
		HelpContext = 0;
		return;
	}
	*(short*)&BitControlByte &= ~(1 << cb->Tag);
	if(cb->Checked)
		*(short*)&BitControlByte |= (1 << cb->Tag);
	SetEdit();
}
//---------------------------------------------------------------------------
// �������������� ������� � ����������
void __fastcall TSimulPLC_Form::Edit23KeyPress(TObject *Sender, char &Key)
{
   if ((Key == VK_RETURN)){
		TEdit *eEd = (TEdit*)Sender;

		try {
			int Value = eEd->Text.ToDouble()*10;
			TLineToPLC  * pCurLineToPLC = &WorkBuf;
			switch(eEd->Tag)
			{
			//"[Feed]"
			case 0:
				pCurLineToPLC->Macro.Feed.vS 	 = Value;
				break;
			//"[Wire]"
			case 1:
				pCurLineToPLC->Macro.Wire.vdg	 = Value;
				break;
			//"[AVC]"
			case 2:
				pCurLineToPLC->Macro.AVC.V_AVC 	 = Value;
				break;
			case 3:
				pCurLineToPLC->Macro.AVC.Jump	 = Value;
				break;
			//"[Oscillation]");                                              // ����������
			case 4:
				pCurLineToPLC->Macro.Oscill.vpl	 = Value;
				break;
			case 5:
				pCurLineToPLC->Macro.Oscill.tpl	 = Value;
				break;
			case 6:
				pCurLineToPLC->Macro.Oscill.tpr	 = Value;
				break;
			case 7:
				pCurLineToPLC->Macro.Oscill.spl	 = Value;
				break;
			//"[Torch]"
			case 8:
				pCurLineToPLC->Macro.Torch.lp	 = Value;
				break;
			case 9:
				pCurLineToPLC->Macro.Torch.lg	 = Value;
				break;
			case 10:
				pCurLineToPLC->Macro.Torch.tp	 = Value;
				break;
			case 11:
				pCurLineToPLC->Macro.Torch.tg	 = Value;
				break;
			case 12:
				pCurLineToPLC->Macro.Feed.JOB_Number = Value;
				break;
			}
			SaveParam = 1;

		} catch (...) {
			MessageBox( Handle, "���������������� ����", "��� ������", MB_ICONINFORMATION );
		}
   }

}
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetControl(short *v)
{
	*(short*)&BitControlByte   = *v++;
//���	*(short*)&BitStatusByteEx = *v;
	Fl_NewData = true;

}
//---------------------------------------------------------------------------
void TSimulPLC_Form::DrawControl()
{
	short v = *(short*)&BitControlByte;

	BufBusy     = v & 1;
	StopWelding = v & 2;
	DriveReady  = v & 4;
	DataReady	= v & 8;
	LasrKadr    = v & 16;
	Pusk        = v & 32;
	ToPause     = v & 64;
	Pause       = v & 128;
	AlarmOn     = v & 0x100;
	ToPausePC   = v & 0x200;
	SaveParam   = v & 0x400;
	PC_Ready    = v & 0x800;
	WeldReady   = v & 0x1000;

	I_not_0     = v & 0x2000;
	PC_Err      = v & 0x4000;
	Weld_Err    = v & 0x8000;


}

//---------------------------------------------------------------------------
void __fastcall TSimulPLC_Form::cbSimulOnClick(TObject *Sender)
{
   Imitator_Count = 0;
}
//---------------------------------------------------------------------------
void TSimulPLC_Form::SetActivity(int RobotIndex,int SysIndex,int Cycle)
{
	ComboBox1->ItemIndex = RobotIndex;
	ComboBox3->ItemIndex = SysIndex;
	Edit3->Text = Cycle;
}

//---------------------------------------------------------------------------
double TSimulPLC_Form::Get_Cur_I()             // �������� ������� ���
{
	short V = 123;
	return (double)V;
}
//---------------------------------------------------------------------------
double TSimulPLC_Form::Get_Cur_B()             // �������� ������� ����������
{
	short V = 234;
	return (double)V/10;
}
//---------------------------------------------------------------------------
double TSimulPLC_Form::Get_Cur_V()             // �������� ������� �������� ���������
{
	short V = 345;
	return (double)V/10;
}
//---------------------------------------------------------------------------
double TSimulPLC_Form::Get_Cur_F()             // �������� ������� ������ ����
{
	short V = 123;
	return (double)V/10;
}

