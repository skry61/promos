//---------------------------------------------------------------------------


#pragma hdrstop

#include "Macro.h"
#include "IniFiles.hpp"
#include "SysPar.h"
//----------------------------------------------------------------------------
__fastcall TMacroView::TMacroView(TEdit * vS,TComboBox *Dir,TEdit * V_AVC,TEdit * Jump,TCheckBox * AVCEnable,TRadioGroup *RgAVC_Axis,
					  TEdit * lp,TEdit * lg,TEdit * tp,TEdit * tg, TCheckBox * Imp_Enable,
					  TEdit * vdg,TCheckBox * Wire_enable,
					  TEdit * vpl,TEdit * tpl,TEdit * tpr,TEdit * spl,TCheckBox * OscEnable,
					  TEdit * JOB
)
{
	//"[Feed]"
		evS 		= vS;
		cbDir		= Dir;

		//"[AVC]"
		eV_AVC		= V_AVC;
		eJump		= Jump;
		cbAVCEnable	= AVCEnable;
		rgAVC_Axis  = RgAVC_Axis;

		//"[Torch]"
		elp			= lp;
		elg			= lg;
		etp			= tp;
		etg			= tg;
		cbImp_Enable= Imp_Enable;
		eJOB		= JOB;

		//"[Wire]"
		evdg		= vdg;
		cbWire_enable= Wire_enable;

		//"[Oscillation]");
		evpl		= vpl;
		etpl		= tpl;
		etpr		= tpr;
		espl		= spl;
		cbOscEnable	= OscEnable;
		//------ ������� -------
//		cbProcTypeTiG_MiG = cbProcType;

		MacroPath = ".\\";
}

//----------------------------------------------------------------------------
int	__fastcall TMacroView::LoadRobotFromFile(AnsiString FileName,TLineToPLC * pCurLineToPLC)
{
	AnsiString Name;
	bool FlNewMacro = false;
	if(FileName == "init.mcr")
	{
		Name  = ChangeFileExt(Application->ExeName,".mcr");
		FlNewMacro = true;
	}
	else
		Name = FileName;
	memset(pCurLineToPLC,0,sizeof(TLineToPLC));
	if (FileExists(Name))
	{
		TMemIniFile *makro = NULL;
		try {
//			memset(pCurLineToPLC,0,sizeof(TLineToPLC));
			//�������� �������� �� INI-�����
			makro = new TMemIniFile(Name);
			//"[Feed]"
			pCurLineToPLC->Macro.Feed.vS 			= makro->ReadString("Feed","vS","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Feed.DirTable		= makro->ReadString("Feed","Dir","0").ToInt();
			//"[AVC]"
			pCurLineToPLC->Macro.AVC.V_AVC 			= makro->ReadString("AVC","V_AVC","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.AVC.Jump			= makro->ReadString("AVC","Jump","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Feed.Enable_AVC 	= makro->ReadString("AVC","AVC enable","Yes") == "Yes";
			pCurLineToPLC->Macro.Feed.AVC_H_V	 	= makro->ReadString("AVC","AVC Axis","Hor") == "Vert";
			//"[Torch]"
			pCurLineToPLC->Macro.Torch.lp	 = makro->ReadString("Torch","lp","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Torch.lg	 = makro->ReadString("Torch","lg","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Torch.tp	 = makro->ReadString("Torch","tp","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Torch.tg	 = makro->ReadString("Torch","tg","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Feed.Enable_ImpCur	= makro->ReadString("Torch","Imp Cur enable","Yes") == "Yes";// ���������� ���  ���������

			pCurLineToPLC->Macro.Feed.JOB_Number	= makro->ReadString("Torch","JOB","0").ToInt();
			//"[Wire]"
			pCurLineToPLC->Macro.Wire.vdg	 = makro->ReadString("Wire","vdg","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Feed.Enable_Wire	= makro->ReadString("Wire","Wire enable","Yes") == "Yes";

			//"[Oscillation]");                                              // ����������
			pCurLineToPLC->Macro.Oscill.vpl	 = makro->ReadString("Oscillation","vpl","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Oscill.tpl	 = makro->ReadString("Oscillation","tpl","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Oscill.tpr	 = makro->ReadString("Oscillation","tpr","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Oscill.spl	 = makro->ReadString("Oscillation","spl","0,0").ToDouble()*10;
			pCurLineToPLC->Macro.Feed.Enable_Oscill 	= makro->ReadString("Oscillation","Osc enable","Yes") == "Yes";
			delete makro; makro = NULL;
			return 0;
		} catch ( ... ) {
			if(makro)	delete makro;
			return 2;
		}
	}
	return 1;
}
//----------------------------------------------------------------------------
int	__fastcall TMacroView::LoadViewFromFile(AnsiString FileName)
{
	TLineToPLC CurLineToPLC;                            // ������� ����
//��� �� ������������	CurLineToPLC.Macro.Feed.ProcTiG_MiG = Fl_TiG_Mig;
	int Res = LoadRobotFromFile(FileName,&CurLineToPLC);
	if(Res == 0)
		Res = LoadMacroFromRobot(&CurLineToPLC);
	return Res;
}
//----------------------------------------------------------------------------
int	__fastcall TMacroView::SaveToFile(AnsiString FileName,AnsiString Comment)
{
		//��������� ��������� � INI-����
		TStringList *list = new TStringList;

		list->Add("[Feed]");                                   					// ������
			list->Values["vS"]		= evS->Text;
			list->Values["Dir"]		= cbDir->ItemIndex;

		list->Add("");

		list->Add("[AVC]");
			list->Values["V_AVC"]	= eV_AVC->Text;
			list->Values["Jump"]	= eJump->Text;
			list->Values["AVC enable"]	= cbAVCEnable->Checked?"Yes":"No";
			list->Values["AVC Axis"]= rgAVC_Axis->ItemIndex==1?"Vert":"Hor";

		list->Add("");

		list->Add("[Torch]");                                 					// �������
			list->Values["lp"]		= elp->Text;
			list->Values["lg"]		= elg->Text;
			list->Values["tp"]		= etp->Text;
			list->Values["tg"]		= etg->Text;
			list->Values["Imp Cur enable"]= cbImp_Enable->Checked?"Yes":"No";   // ���������� ���  ���������
			list->Values["JOB"]		= eJOB->Text;
		list->Add("");

		list->Add("[Wire]");                                                    // ���������
			list->Values["vdg"]		= evdg->Text;
			list->Values["Wire enable"]	= cbWire_enable->Checked?"Yes":"No";

		list->Add("");

		list->Add("[Oscillation]");                                             // ����������
			list->Values["vpl"]		= evpl->Text;
			list->Values["tpl"]		= etpl->Text;
			list->Values["tpr"]		= etpr->Text;
			list->Values["spl"]		= espl->Text;
			list->Values["Osc enable"]	= cbOscEnable->Checked?"Yes":"No";
		//------ ����������� -------
		list->Add("[Comments]");
		if(Comment == NULL)
			list->Add("Comment1=������ ������� �� �����������");
		else
			list->Add("Comment1="+ Comment);

		AnsiString wpName = "\\Program";
		AnsiString Name = SysPar->GetProjectPath()+wpName+"\\Macros\\"+FileName;

		list->SaveToFile(Name);
		list->Clear();
		delete list;
	return 0;
}
//----------------------------------------------------------------------------
void SetColor(AnsiString aS,TEdit * e)
{
	if(aS == e->Text)
		e->Color = clWindow;
	else
		e->Color = clYellow;
	e->Text = aS;
}
//----------------------------------------------------------------------------
//�������� �������� �� INI-�����
int	__fastcall TMacroView::LoadMacroFromRobot(TLineToPLC * pCurLineToPLC)
{
	try {
		AnsiString s;
		//"[Feed]"
		/*evS->Text	   */ s = ((float)pCurLineToPLC->Macro.Feed.vS)/10;  SetColor(s,evS);
	

		cbDir->ItemIndex= pCurLineToPLC->Macro.Feed.DirTable;
		//"[AVC]"
		/*eV_AVC->Text  */ s = ((float)pCurLineToPLC->Macro.AVC.V_AVC)/10; SetColor(s,eV_AVC);
		
		/*eJump->Text 	*/ s = ((float)pCurLineToPLC->Macro.AVC.Jump)/10;  SetColor(s,eJump);

		cbAVCEnable->Checked = pCurLineToPLC->Macro.Feed.Enable_AVC;
		rgAVC_Axis->ItemIndex= pCurLineToPLC->Macro.Feed.AVC_H_V;

		//"[Torch]"
		/*elp->Text   	*/ s= ((float)pCurLineToPLC->Macro.Torch.lp)/10; SetColor(s,elp);
		/*elg->Text	    */ s= ((float)pCurLineToPLC->Macro.Torch.lg)/10; SetColor(s,elg);
		/*etp->Text	    */ s= ((float)pCurLineToPLC->Macro.Torch.tp)/10; SetColor(s,etp);
		/*etg->Text	    */ s= ((float)pCurLineToPLC->Macro.Torch.tg)/10; SetColor(s,etg);
		cbImp_Enable->Checked 	= pCurLineToPLC->Macro.Feed.Enable_ImpCur;

		/*eJOB->Text    */ s= pCurLineToPLC->Macro.Feed.JOB_Number; 	 SetColor(s,eJOB);

		//"[Wire]"
		/*evdg->Text	*/ s = ((float)pCurLineToPLC->Macro.Wire.vdg)/10;    SetColor(s,evdg);
		cbWire_enable->Checked 	= pCurLineToPLC->Macro.Feed.Enable_Wire;

		//"[Oscillation]");                                              // ����������
		/*evpl->Text	*/ s = ((float)pCurLineToPLC->Macro.Oscill.vpl)/10; SetColor(s,evpl);
		/*etpl->Text	*/ s = ((float)pCurLineToPLC->Macro.Oscill.tpl)/10; SetColor(s,etpl);
		/*etpr->Text	*/ s = ((float)pCurLineToPLC->Macro.Oscill.tpr)/10; SetColor(s,etpr);
		/*espl->Text	*/ s = ((float)pCurLineToPLC->Macro.Oscill.spl)/10; SetColor(s,espl);
		cbOscEnable->Checked 	= pCurLineToPLC->Macro.Feed.Enable_Oscill;
		return 0;
		
	} catch ( ... ) {
		return 2;
	}
//	return 1;
}
//---------------------------------------------------------------------------

#pragma package(smart_init)
