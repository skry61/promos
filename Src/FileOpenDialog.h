//---------------------------------------------------------------------------

#ifndef FileOpenDialogH
#define FileOpenDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
//---------------------------------------------------------------------------
class TOpenFileForm : public TForm
{
__published:	// IDE-managed Components
	TValueListEditor *ValueListEditor1;
	TButton *Button1;
	TButton *Button2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ValueListEditor1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
private:	// User declarations
	bool 		FlCanSelected;
	AnsiString FileName;
	AnsiString Path;
	AnsiString Ext;
	AnsiString FullFileName;
	AnsiString Comment;
	TStringList *List;


public:		// User declarations
	__fastcall TOpenFileForm(TComponent* Owner);
	AnsiString __fastcall GetFileName(){return FileName;};
	AnsiString __fastcall GetFullFileName(){return FullFileName;};
	AnsiString __fastcall GetComment(){return Comment;};
	int __fastcall InitData(bool FlMacro);
};
//---------------------------------------------------------------------------
extern PACKAGE TOpenFileForm *OpenFileForm;
//---------------------------------------------------------------------------
#endif
