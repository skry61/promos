object SimulPLC_Form: TSimulPLC_Form
  Left = 0
  Top = 0
  Caption = #1057#1080#1084#1091#1083#1103#1090#1086#1088' '#1050#1086#1085#1090#1088#1086#1083#1083#1077#1088#1072
  ClientHeight = 587
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 89
    Top = 15
    Width = 530
    Height = 20
  end
  object Bevel2: TBevel
    Left = 88
    Top = 39
    Width = 530
    Height = 20
  end
  object StaticText1: TStaticText
    Left = 92
    Top = 17
    Width = 481
    Height = 17
    Caption = 
      '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 0' +
      '0 00 00 00 00 00 00 00 00 00 00'
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 8
    Top = 16
    Width = 79
    Height = 17
    Caption = #1041#1091#1092#1077#1088' '#1082#1072#1076#1088#1072' 0'
    TabOrder = 1
  end
  object StaticText7: TStaticText
    Left = 8
    Top = 41
    Width = 74
    Height = 17
    Caption = #1056#1072#1073#1086#1095#1080#1081' '#1082#1072#1076#1088
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 457
    Top = 64
    Width = 212
    Height = 94
    Caption = #1055#1086#1079#1080#1094#1080#1080
    TabOrder = 3
    object StaticText10: TStaticText
      Left = 3
      Top = 22
      Width = 82
      Height = 17
      Caption = #1055#1086#1079#1080#1094#1080#1103' '#1043#1086#1088#1080#1079'.'
      TabOrder = 0
    end
    object Edit4: TEdit
      Left = 84
      Top = 18
      Width = 59
      Height = 21
      TabOrder = 1
      Text = '0,0'
    end
    object StaticText11: TStaticText
      Left = 3
      Top = 45
      Width = 77
      Height = 17
      Caption = #1055#1086#1079#1080#1094#1080#1103' '#1042#1077#1088#1090'.'
      TabOrder = 2
    end
    object Edit5: TEdit
      Left = 84
      Top = 41
      Width = 59
      Height = 21
      TabOrder = 3
      Text = '0,0'
    end
    object StaticText12: TStaticText
      Left = 3
      Top = 68
      Width = 76
      Height = 17
      Caption = #1059#1075#1086#1083' '#1090#1077#1082#1091#1097#1080#1081
      TabOrder = 4
    end
    object Edit6: TEdit
      Left = 84
      Top = 64
      Width = 59
      Height = 21
      TabOrder = 5
      Text = '0,0'
    end
    object CheckBox2: TCheckBox
      Left = 148
      Top = 65
      Width = 49
      Height = 17
      Caption = #1040#1074#1090#1086' '
      TabOrder = 6
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 62
    Width = 433
    Height = 222
    Caption = ' '#1057#1080#1084#1091#1083#1103#1090#1086#1088' '
    TabOrder = 4
    object Label1: TLabel
      Left = 14
      Top = 118
      Width = 32
      Height = 13
      Caption = #1056#1077#1078#1080#1084
    end
    object lStatusByte: TLabel
      Left = 95
      Top = 21
      Width = 79
      Height = 13
      Caption = #1041#1072#1081#1090' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
    end
    object StaticText3: TStaticText
      Left = 95
      Top = 61
      Width = 65
      Height = 17
      Caption = #1050#1086#1076' '#1086#1096#1080#1073#1082#1080
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 127
      Top = 77
      Width = 48
      Height = 21
      TabOrder = 1
      Text = '0'
      OnKeyPress = Edit1KeyPress
    end
    object cbSimulOn: TCheckBox
      Left = 14
      Top = 20
      Width = 75
      Height = 17
      Caption = #1042#1082#1083#1102#1095#1080#1090#1100
      TabOrder = 2
      OnClick = cbSimulOnClick
    end
    object cbRegim: TComboBox
      Left = 14
      Top = 133
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 3
      Text = #1053#1077#1090
      OnClick = cbRegimClick
      Items.Strings = (
        #1053#1077#1090
        'JOG'
        #1053#1077' '#1075#1086#1090#1086#1074
        #1043#1086#1090#1086#1074
        #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1086#1075#1088#1072#1084#1084#1091
        #1048#1085#1080#1094#1080#1072#1083#1080#1079#1072#1094#1080#1103
        #1043#1086#1090#1086#1074' '#1082' '#1089#1090#1072#1088#1090#1091
        #1057#1090#1072#1088#1090
        #1057#1090#1086#1087
        #1054#1089#1090#1072#1085#1086#1074' '#1085#1077' '#1087#1072#1091#1079#1091
        #1055#1072#1091#1079#1072
        #1047#1072#1074#1077#1088#1096#1077#1085#1080#1077' '
        #1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1079#1072#1074#1077#1088#1096#1077#1085#1072
        #1054#1096#1080#1073#1082#1072)
    end
    object bJOG: TButton
      Left = 14
      Top = 39
      Width = 75
      Height = 25
      Caption = 'JOG'
      TabOrder = 4
      OnClick = bJOGClick
    end
    object bPusk: TButton
      Left = 14
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Pusk'
      TabOrder = 5
      OnClick = bPuskClick
    end
    object Button1: TButton
      Left = 14
      Top = 91
      Width = 75
      Height = 25
      Caption = #1057#1090#1086#1087
      TabOrder = 6
      OnClick = Button1Click
    end
    object CheckBox1: TCheckBox
      Left = 181
      Top = 13
      Width = 233
      Height = 17
      Caption = 'BufBusy -  PC : '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072' '#1079#1072#1085#1103#1090'   M200'
      TabOrder = 7
      OnClick = CheckBox1Click
    end
    object CheckBox3: TCheckBox
      Tag = 1
      Left = 181
      Top = 26
      Width = 233
      Height = 17
      Caption = 'StopWelding - '#1057#1074#1072#1088#1082#1072' '#1057#1090#1086#1087'                 '#1052'201'
      TabOrder = 8
      OnClick = CheckBox1Click
    end
    object CheckBox4: TCheckBox
      Tag = 2
      Left = 181
      Top = 39
      Width = 233
      Height = 17
      Caption = #1055#1088#1080#1074#1086#1076#1072' '#1075#1086#1090#1086#1074#1099'                                  '#1052'202'
      TabOrder = 9
      OnClick = CheckBox1Click
    end
    object CheckBox5: TCheckBox
      Tag = 3
      Left = 181
      Top = 53
      Width = 249
      Height = 17
      Caption = 'ReadyForNS -  '#1043#1086#1090'. '#1082' '#1089#1083#1077#1076'. '#1087#1088#1086#1093#1086#1076#1091'  '#1052'203'
      TabOrder = 10
      OnClick = CheckBox1Click
    end
    object CheckBox6: TCheckBox
      Tag = 4
      Left = 181
      Top = 66
      Width = 239
      Height = 17
      Caption = 'LasrKadr - PC : '#1055#1086#1089#1083#1077#1076#1085#1080#1081' '#1082#1072#1076#1088'          '#1052'204'
      TabOrder = 11
      OnClick = CheckBox1Click
    end
    object CheckBox7: TCheckBox
      Tag = 5
      Left = 181
      Top = 79
      Width = 233
      Height = 17
      Caption = 'Pusk -  '#1055#1091#1089#1082'  - '#1042#1082#1083#1102#1095#1077#1085#1072' '#1087#1086#1076#1072#1095#1072'        '#1052'205'
      TabOrder = 12
      OnClick = CheckBox1Click
    end
    object CheckBox8: TCheckBox
      Tag = 6
      Left = 181
      Top = 93
      Width = 233
      Height = 17
      Caption = 'ToPause -  '#1055#1077#1088#1077#1093#1086#1076' '#1085#1072' '#1055#1072#1091#1079#1091'              '#1052'206'
      TabOrder = 13
      OnClick = CheckBox1Click
    end
    object CheckBox9: TCheckBox
      Tag = 7
      Left = 181
      Top = 106
      Width = 233
      Height = 17
      Caption = 'Pause -  '#1055#1072#1091#1079#1072' '#1042#1050#1051'                               '#1052'207'
      TabOrder = 14
      OnClick = CheckBox1Click
    end
    object CheckBox10: TCheckBox
      Tag = 8
      Left = 181
      Top = 119
      Width = 233
      Height = 17
      Caption = 'AlarmOn -  '#1040#1074#1072#1088#1080#1103' '#1091#1089#1090#1072#1085#1086#1074#1082#1080'              M208'
      TabOrder = 15
      OnClick = CheckBox1Click
    end
    object CheckBox11: TCheckBox
      Tag = 9
      Left = 181
      Top = 133
      Width = 239
      Height = 17
      Caption = 'ToPausePC -  PC : '#1055#1077#1088#1077#1093#1086#1076' '#1085#1072' '#1055#1072#1091#1079#1091'  '#1052'209'
      TabOrder = 16
      OnClick = CheckBox1Click
    end
    object CheckBox12: TCheckBox
      Tag = 10
      Left = 181
      Top = 146
      Width = 239
      Height = 18
      Caption = 'SaveParam -  '#1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088#1099'   '#1052'210'
      TabOrder = 17
      OnClick = CheckBox1Click
    end
    object Edit2: TEdit
      Left = 127
      Top = 36
      Width = 47
      Height = 21
      BiDiMode = bdRightToLeft
      Enabled = False
      ParentBiDiMode = False
      TabOrder = 18
      Text = '0000'
    end
    object CheckBox16: TCheckBox
      Tag = 11
      Left = 181
      Top = 159
      Width = 239
      Height = 18
      Caption = #1055#1050' '#1075#1086#1090#1086#1074'                                                '#1052'211'
      TabOrder = 19
      OnClick = CheckBox1Click
    end
    object CheckBox17: TCheckBox
      Tag = 12
      Left = 181
      Top = 173
      Width = 239
      Height = 18
      Caption = #1057#1074#1072#1088#1082#1072' '#1075#1086#1090#1086#1074#1072'                                      '#1052'212'
      TabOrder = 20
      OnClick = CheckBox1Click
    end
    object CheckBox18: TCheckBox
      Tag = 13
      Left = 181
      Top = 186
      Width = 239
      Height = 18
      Caption = #1058#1086#1082' '#1073#1086#1083#1100#1096#1077' 0                                        '#1052'213'
      TabOrder = 21
      OnClick = CheckBox1Click
    end
    object CheckBox19: TCheckBox
      Tag = 14
      Left = 181
      Top = 200
      Width = 239
      Height = 18
      Caption = #1053#1072' '#1082#1086#1084#1087#1100#1102#1090#1077#1088#1077' '#1086#1096#1080#1073#1082#1072'                       '#1052'214'
      TabOrder = 22
      OnClick = CheckBox1Click
    end
    object CheckBox20: TCheckBox
      Tag = 15
      Left = 14
      Top = 155
      Width = 161
      Height = 18
      Caption = #1054#1096#1080#1073#1082#1072' '#1057#1074#1072#1088'.'#1040#1087#1087'.    '#1052'215'
      TabOrder = 23
      OnClick = CheckBox1Click
    end
    object CheckBox22: TCheckBox
      Tag = 16
      Left = 14
      Top = 170
      Width = 161
      Height = 18
      Caption = #1057#1090#1086#1083' 1                 '#1052'216/217'
      Checked = True
      State = cbChecked
      TabOrder = 24
      OnClick = CheckBox1Click
    end
    object CheckBox23: TCheckBox
      Tag = 18
      Left = 14
      Top = 185
      Width = 161
      Height = 18
      Caption = 'Tig/Mig                 '#1052'218/219'
      Checked = True
      State = cbChecked
      TabOrder = 25
      OnClick = CheckBox1Click
    end
    object CheckBox24: TCheckBox
      Tag = 20
      Left = 14
      Top = 200
      Width = 161
      Height = 18
      Caption = #1056#1077#1079#1077#1088#1074'                        '#1052'220'
      TabOrder = 26
      OnClick = CheckBox1Click
    end
  end
  object StaticText4: TStaticText
    Left = 91
    Top = 41
    Width = 481
    Height = 17
    Caption = 
      '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 0' +
      '0 00 00 00 00 00 00 00 00 00 00'
    TabOrder = 5
  end
  object Panel1: TPanel
    Left = 0
    Top = 288
    Width = 695
    Height = 345
    TabOrder = 6
    object Label3: TLabel
      Left = 40
      Top = 253
      Width = 110
      Height = 19
      Caption = #1058#1080#1087' '#1087#1088#1086#1094#1077#1089#1089#1072' :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object GroupBox18: TGroupBox
      Left = 8
      Top = 8
      Width = 340
      Height = 74
      Hint = #1056#1096#1090#1077#1056#1096#1090#1077#1056#1096#1090#1077
      Caption = ' '#1055#1086#1076#1072#1095#1072' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 0
      object Edit23: TEdit
        Left = 199
        Top = 13
        Width = 64
        Height = 24
        Hint = #1057#1082#1086#1088#1086#1089#1090#1100' '#1089#1074#1072#1088#1082#1080
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText64: TStaticText
        Left = 24
        Top = 17
        Width = 102
        Height = 20
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1089#1074#1072#1088#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object StaticText65: TStaticText
        Left = 271
        Top = 19
        Width = 45
        Height = 20
        Caption = #1089#1084'/'#1084#1080#1085
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object ComboBox2: TComboBox
        Left = 199
        Top = 39
        Width = 124
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 16
        ParentFont = False
        TabOrder = 3
        Text = #1055#1086' '#1095#1072#1089#1086#1074#1086#1081
        Items.Strings = (
          #1055#1086' '#1095#1072#1089#1086#1074#1086#1081
          #1055#1088#1086#1090#1080#1074' '#1095#1072#1089#1086#1074#1086#1081)
      end
      object StaticText66: TStaticText
        Left = 24
        Top = 43
        Width = 146
        Height = 20
        Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1074#1088#1072#1097#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
    end
    object GroupBox17: TGroupBox
      Left = 348
      Top = 8
      Width = 340
      Height = 143
      Caption = ' '#1054#1089#1094#1080#1083#1083#1103#1094#1080#1103' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      object StaticText56: TStaticText
        Left = 24
        Top = 17
        Width = 131
        Height = 20
        Hint = #1057#1082#1086#1088#1086#1089#1090#1100' '#1086#1089#1094#1080#1083#1083#1103#1094#1080#1080
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1086#1089#1094#1080#1083#1083#1103#1094#1080#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Edit17: TEdit
        Tag = 4
        Left = 196
        Top = 13
        Width = 64
        Height = 24
        Hint = #1057#1082#1086#1088#1086#1089#1090#1100' '#1086#1089#1094#1080#1083#1083#1103#1094#1080#1080
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 1
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText57: TStaticText
        Left = 268
        Top = 19
        Width = 31
        Height = 20
        Caption = #1084#1084'/'#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object StaticText58: TStaticText
        Left = 24
        Top = 43
        Width = 100
        Height = 20
        Hint = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1083#1077#1074#1072
        Caption = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1083#1077#1074#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object Edit20: TEdit
        Tag = 5
        Left = 196
        Top = 39
        Width = 64
        Height = 24
        Hint = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1083#1077#1074#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 4
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText59: TStaticText
        Left = 268
        Top = 43
        Width = 10
        Height = 20
        Caption = #1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object StaticText60: TStaticText
        Left = 268
        Top = 69
        Width = 10
        Height = 20
        Caption = #1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object Edit21: TEdit
        Tag = 6
        Left = 196
        Top = 65
        Width = 64
        Height = 24
        Hint = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1087#1088#1072#1074#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 7
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText61: TStaticText
        Left = 24
        Top = 70
        Width = 107
        Height = 20
        Hint = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1087#1088#1072#1074#1072
        Caption = #1047#1072#1076#1077#1088#1078#1082#1072' '#1089#1087#1088#1072#1074#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object StaticText62: TStaticText
        Left = 24
        Top = 98
        Width = 67
        Height = 20
        Hint = #1040#1084#1087#1083#1080#1090#1091#1076#1072
        Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object Edit22: TEdit
        Tag = 7
        Left = 196
        Top = 92
        Width = 64
        Height = 24
        Hint = #1040#1084#1087#1083#1080#1090#1091#1076#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 10
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText63: TStaticText
        Left = 268
        Top = 98
        Width = 20
        Height = 20
        Caption = #1084#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
      end
      object CheckBox13: TCheckBox
        Tag = 2
        Left = 24
        Top = 116
        Width = 176
        Height = 21
        Caption = #1054#1089#1094#1080#1083#1083#1103#1094#1080#1102' '#1088#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
      end
    end
    object GroupBox19: TGroupBox
      Left = 348
      Top = 151
      Width = 340
      Height = 146
      Caption = ' '#1043#1086#1088#1077#1083#1082#1072' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      object StaticText67: TStaticText
        Left = 266
        Top = 17
        Width = 12
        Height = 20
        Caption = 'A'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object StaticText70: TStaticText
        Left = 24
        Top = 17
        Width = 84
        Height = 20
        Caption = #1058#1086#1082' '#1080#1084#1087#1091#1083#1100#1089#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object Edit24: TEdit
        Tag = 8
        Left = 196
        Top = 13
        Width = 64
        Height = 24
        Hint = #1055#1091#1083#1100#1089#1080#1088#1091#1102#1097#1080#1081' '#1089#1074#1072#1088#1086#1095#1085#1099#1081' '#1090#1086#1082
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 0
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText71: TStaticText
        Left = 267
        Top = 43
        Width = 12
        Height = 20
        Caption = 'A'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object StaticText72: TStaticText
        Left = 267
        Top = 92
        Width = 18
        Height = 20
        Caption = #1084#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object Edit26: TEdit
        Tag = 9
        Left = 196
        Top = 39
        Width = 64
        Height = 24
        Hint = #1041#1072#1079#1086#1074#1099#1081' '#1089#1074#1072#1088#1086#1095#1085#1099#1081' '#1090#1086#1082
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 1
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText73: TStaticText
        Left = 24
        Top = 43
        Width = 79
        Height = 20
        Caption = #1058#1086#1082' '#1073#1072#1079#1086#1074#1099#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object StaticText74: TStaticText
        Left = 24
        Top = 92
        Width = 139
        Height = 20
        Caption = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1080#1084#1087#1091#1083#1089#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object Edit27: TEdit
        Tag = 11
        Left = 196
        Top = 114
        Width = 64
        Height = 24
        Hint = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1073#1072#1079#1086#1074#1086#1075#1086' '#1089#1074#1072#1088#1086#1095#1085#1086#1075#1086' '#1090#1086#1082#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 3
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText76: TStaticText
        Left = 267
        Top = 118
        Width = 18
        Height = 20
        Caption = #1084'c'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
      end
      object StaticText77: TStaticText
        Left = 24
        Top = 118
        Width = 126
        Height = 20
        Caption = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1087#1072#1091#1079#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
      end
      object Edit29: TEdit
        Tag = 10
        Left = 196
        Top = 88
        Width = 64
        Height = 24
        Hint = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1087#1091#1083#1100#1089#1080#1088#1091#1102#1097#1077#1075#1086' '#1089#1074#1072#1088#1086#1095#1085#1086#1075#1086' '#1090#1086#1082#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 2
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object CheckBox21: TCheckBox
        Tag = 2
        Left = 24
        Top = 66
        Width = 193
        Height = 21
        Caption = #1048#1084#1087#1091#1083#1100#1089#1085#1099#1081' '#1090#1086#1082'  '#1088#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
      end
    end
    object GroupBox20: TGroupBox
      Left = 8
      Top = 151
      Width = 340
      Height = 80
      Caption = 'AVC'
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      object Label11: TLabel
        Left = 24
        Top = 16
        Width = 136
        Height = 16
        Caption = #1053#1072#1087#1088#1103#1078#1077#1085#1080#1077' '#1089#1083#1077#1078#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 24
        Top = 39
        Width = 47
        Height = 16
        Caption = #1055#1086#1076#1089#1082#1086#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object CheckBox14: TCheckBox
        Tag = 1
        Left = 24
        Top = 59
        Width = 124
        Height = 16
        Hint = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1088#1072#1073#1086#1090#1091' '#1040#1056#1053#1044
        Caption = 'AVC '#1056#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Edit31: TEdit
        Tag = 2
        Left = 199
        Top = 13
        Width = 64
        Height = 24
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        TabOrder = 1
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object Edit32: TEdit
        Tag = 3
        Left = 199
        Top = 39
        Width = 63
        Height = 24
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        TabOrder = 2
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText78: TStaticText
        Left = 267
        Top = 42
        Width = 20
        Height = 20
        Caption = #1084#1084
        TabOrder = 3
      end
      object StaticText79: TStaticText
        Left = 267
        Top = 17
        Width = 11
        Height = 20
        Caption = #1042
        TabOrder = 4
      end
      object rgAVC_Axis: TRadioGroup
        Left = 293
        Top = 10
        Width = 40
        Height = 65
        Caption = #1054#1089#1100
        Items.Strings = (
          #1043
          #1042)
        TabOrder = 5
      end
    end
    object GroupBox12: TGroupBox
      Left = 8
      Top = 82
      Width = 340
      Height = 69
      Caption = ' '#1055#1088#1086#1074#1086#1083#1086#1082#1072' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      object StaticText53: TStaticText
        Left = 268
        Top = 20
        Width = 45
        Height = 20
        Caption = #1089#1084'/'#1084#1080#1085
        TabOrder = 0
      end
      object Edit16: TEdit
        Tag = 1
        Left = 199
        Top = 16
        Width = 64
        Height = 24
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 1
        Text = '0,1'
        OnKeyPress = Edit23KeyPress
      end
      object StaticText55: TStaticText
        Left = 24
        Top = 20
        Width = 170
        Height = 20
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080' '#1087#1088#1086#1074#1086#1083#1086#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object CheckBox15: TCheckBox
        Left = 24
        Top = 46
        Width = 201
        Height = 17
        Caption = #1055#1086#1076#1072#1095#1091' '#1087#1088#1086#1074#1086#1083#1086#1082#1080' '#1088#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
    object GroupBox3: TGroupBox
      Left = 248
      Top = 63
      Width = 340
      Height = 45
      Caption = 'JOB'
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      Visible = False
      object eJOB_Number: TEdit
        Tag = 12
        Left = 179
        Top = 18
        Width = 64
        Height = 24
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 0
        Text = '0'
        OnKeyPress = Edit23KeyPress
      end
      object JOB: TStaticText
        Left = 24
        Top = 17
        Width = 66
        Height = 20
        Caption = #1053#1086#1084#1077#1088' JOB'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
    object cbProcTypeSymul: TComboBox
      Left = 207
      Top = 253
      Width = 64
      Height = 21
      ItemHeight = 13
      TabOrder = 6
      Text = 'TiG'
      OnClick = cbRegimClick
      Items.Strings = (
        'TiG'
        'MiG')
    end
  end
  object GroupBox4: TGroupBox
    Left = 457
    Top = 158
    Width = 212
    Height = 121
    Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
    TabOrder = 7
    object Label2: TLabel
      Left = 3
      Top = 69
      Width = 60
      Height = 13
      Caption = #1040#1082#1090#1080#1074#1085#1086#1089#1090#1100
    end
    object StaticText5: TStaticText
      Left = 3
      Top = 22
      Width = 57
      Height = 17
      Caption = #1057#1086#1089#1090'. '#1056#1073#1090'.'
      TabOrder = 0
    end
    object StaticText6: TStaticText
      Left = 3
      Top = 45
      Width = 63
      Height = 17
      Caption = #1057#1086#1089#1090'. '#1057#1080#1089#1090'.'
      TabOrder = 1
    end
    object ComboBox1: TComboBox
      Left = 65
      Top = 18
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Text = 'robNONE'
      Items.Strings = (
        'robNONE'
        'robJOG'
        'robNOT_READY'
        'robREADY'
        'robWAIT_PROGRAMM'
        'robINIT'
        'robSTARTING'
        'robSTART'
        'robSTOP'
        'robPAUSING'
        'robPAUSE'
        'robFINISHING'
        'robFINISH'
        'robToERROR'
        'robERROR')
    end
    object ComboBox3: TComboBox
      Left = 65
      Top = 42
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 3
      Text = 'sysINIT'
      Items.Strings = (
        'sysINIT'
        'sysEDIT'
        'sysACTIVATION_WAIT'
        'sysToACTIVATED'
        'sysACTIVATED'
        'sysIN_PROSESS_EDITPROG'
        'sysIN_PROSESS_EDITMACRO'
        'sysIN_PROSESS_LOOK_GRAPH'
        'sysIN_PROSESS_LOOK_CUR_VALUE'
        'sysFINISHING_PROG'
        'sysEND_PROG'
        'sysPAUSE'
        'sysSTOP_OPERATOR'
        'sysERROR'
        'sysLAST')
    end
    object Edit3: TEdit
      Left = 65
      Top = 66
      Width = 145
      Height = 21
      TabOrder = 4
      Text = '???'
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 488
  end
end
