//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FileSaveDialog.h"
#include "SysPar.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSaveForm *SaveForm;
//---------------------------------------------------------------------------
__fastcall TSaveForm::TSaveForm(TComponent* Owner)
	: TForm(Owner)
{
	FileTypePRG = false;
	List = NULL;
}
//---------------------------------------------------------------------------
/*
void __fastcall TSaveForm::SetFileName(char * FN,char * FFN)
{
	FullFileName = AnsiString(FFN) ;
	FileName = AnsiString(FN) ;
	Edit_FileName->Text = FileName;
	Ext = ExtractFileExt(FullFileName);
//	FileName = ExtractFileName(FullFileName);
}
*/
//---------------------------------------------------------------------------



void __fastcall TSaveForm::Button1Click(TObject *Sender)
{

	FileName = 	Edit_FileName->Text;
	FullFileName = Path+FileName + Ext;
/*

	int Index;
	try
	{
		if(List!=NULL)
			delete List;
		List = new TStringList();
//		List->Clear();
		try
		{
			List->LoadFromFile(FullFileName);

			if(Ext == "prg")
			{
				if(!List->Find("[Comments]",Disp))
					RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				else
					if(Disp == 0)
					{
						RowCount = List->Strings[1].ToInt();               // ��������� ���������� �����
						Disp = 1;
					}
					else
						RowCount = List->Strings[0].ToInt();               // ��������� ���������� �����
				if((RowCount<2)||(RowCount>500))
				{
					ShowMessage("� ��������� ���������������� ���������� �����.");
					return;
				}
			}else
				RowCount = List->Count;               // ��������� ���������� �����

			SaveForm->SetList(&List);

			Memo1->Clear();
			for (int i=0; i<RowCount; i++)
			{
				AnsiString s = List->Strings[i+Disp];
				Memo1->Lines->Add(s);
			}

		}__finally
		{
 //			delete List;
		}

	} catch ( ... ) {
		ShowMessage("������ ��� �������� ��������� ������.");
		return;
	}





	if((List!=NULL)&&(*List!=NULL))
	{
//		(*List)->Add("[Comments]");

		if(!(*List)->Find("[Comments]",Index))
		{
//			(*List)->Insert(0,"[Comments]");
			(*List)->Add("[Comments]");
			(*List)->Add("Comment1="+ Edit_Comment->Text);
		}else
			(*List)->Values["Comment1"] = Edit_Comment->Text;
		(*List)->SaveToFile(FullFileName);

		delete *List; *List=NULL;List=NULL;

	}
*/
}
//---------------------------------------------------------------------------
int __fastcall TSaveForm::InitData(bool FlMacro,AnsiString aFileName)
{
	if(FlMacro)	{Caption = "��������� ������";    Label1->Caption  = "������:";}
	else{        Caption = "��������� ���������";	Label1->Caption  = "���������:";}

	Path = "\\Program";
	if(FlMacro) Path += "\\Macros";
	Path = SysPar->GetProjectPath() + Path+"\\";

	Ext = FlMacro?".mcr":".prg";

	FileName = aFileName;
	Edit_FileName->Text = FileName;
	FullFileName = Path+FileName + Ext;

/*
	TSearchRec search_rec;
	FlCanSelected = false;
	ValueListEditor1->Strings->Clear();
//	TStringList *S = new TStringList;
	try
	{
		AnsiString FullFileName = Path+"*." + Ext;
		int error_code = FindFirst(FullFileName, faAnyFile, search_rec);
		while (error_code == 0)
		{
			AnsiString ss = ExtractFileName(search_rec.Name).SubString(1,search_rec.Name.Pos(".")-1);
//			if (FileExists(search_rec.Name))
			{
				TMemIniFile *File = NULL;
				try {
					//�������� ����������� �� INI-�����
					File = new TMemIniFile(Path+search_rec.Name);
					//"[Comments]"
					ss = ss + "=" + File->ReadString("Comments","Comment1","����������� ����������");
					ValueListEditor1->Strings->Add(ss);

					delete File; File = NULL;
				} catch ( ... ) {
					if(File)	delete File;
					return 2;
				}
			}//else  		return;// 1;
			error_code = FindNext(search_rec);
		}
	}
	__finally
	{
		FindClose(search_rec);
		FlCanSelected = true;
	}
*/
	return 0;

}
//---------------------------------------------------------------------------

