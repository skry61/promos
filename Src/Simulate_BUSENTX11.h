//---------------------------------------------------------------------------

#ifndef Simulate_BUSENTX11H
#define Simulate_BUSENTX11H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "BASINTX11.h"
//---------------------------------------------------------------------------
class TBUSINTX11_Form : public TForm
{
__published:	// IDE-managed Components
	TBevel *Bevel1;
	TBevel *Bevel2;
	TStaticText *StaticText1;
	TStaticText *StaticText2;
	TStaticText *StaticText7;
	TGroupBox *GroupBox1;
	TStaticText *StaticText10;
	TEdit *I_Current;
	TStaticText *StaticText11;
	TEdit *U_Current;
	TStaticText *StaticText12;
	TEdit *Wire_FeedOut;
	TCheckBox *CheckBox2;
	TGroupBox *GroupBox2;
	TLabel *Label1;
	TLabel *lStatusByte;
	TStaticText *StaticText3;
	TEdit *ErrorCod;
	TCheckBox *cbSimulOn;
	TComboBox *cbRegim;
	TButton *bJOG;
	TButton *bPusk;
	TButton *Button1;
	TCheckBox *PUSK_Welding;
	TCheckBox *Robot_READY;
	TCheckBox *ShildingGas;
	TCheckBox *ImpWire;
	TCheckBox *InversImpWire;
	TCheckBox *PosSearch;
	TCheckBox *ERR_Reset;
	TCheckBox *ReleK2;
	TCheckBox *Simul_Weld;
	TCheckBox *WeldingMode0;
	TCheckBox *WeldingMode1;
	TEdit *Edit2;
	TStaticText *StaticText4;
	TPanel *Panel1;
	TGroupBox *GroupBox19;
	TStaticText *StaticText67;
	TStaticText *StaticText70;
	TEdit *MainCURRENTe;
	TStaticText *StaticText71;
	TStaticText *StaticText72;
	TEdit *SecondCURRENT;
	TStaticText *StaticText73;
	TStaticText *StaticText74;
	TEdit *T_Pause;
	TStaticText *StaticText76;
	TStaticText *StaticText77;
	TEdit *T_Pulse;
	TGroupBox *GroupBox20;
	TLabel *Label11;
	TLabel *Label12;
	TCheckBox *CheckBox14;
	TEdit *Edit31;
	TEdit *Edit32;
	TStaticText *StaticText78;
	TStaticText *StaticText79;
	TGroupBox *GroupBox12;
	TStaticText *StaticText53;
	TEdit *Wire_FeedIn;
	TStaticText *StaticText55;
	TCheckBox *CheckBox15;
	TTimer *Timer1;
	TCheckBox *CheckBox13;
	TCheckBox *CheckBox16;
	TStaticText *StaticText8;
	TStaticText *StaticText9;
	TBevel *Bevel4;
	TStaticText *StaticText5;
	TCheckBox *CURRENT_Flow;
	TCheckBox *ReadyForWelding;
	TCheckBox *ErrorOn;
	TCheckBox *MainCurrent;
	TCheckBox *Active;
	TStaticText *StaticText6;
	TEdit *JOB_Number;
	TCheckBox *U_Alarm;
private:	// User declarations
public:		// User declarations
	__fastcall TBUSINTX11_Form(TComponent* Owner);
	TLineToBUSINTX11   CurLineDataToBUSINTX11;          // ������� ����
	TLineFromBUSINTX11 CurLineDataFromBUSINTX11;        // ������� ����

	bool SetControl(TLineToBUSINTX11   aCurLineDataToBUSINTX11);    // ��������� �� �����������
	bool GetStatus(TLineFromBUSINTX11 *aCurLineDataFromBUSINTX11);  // ���������� ������ �������� � ����������
	bool GetControl(TLineToBUSINTX11 *aCurLineDataToBUSINTX11); // ��������� �� ����������
	bool SetStatus(TLineFromBUSINTX11 aCurLineDataFromBUSINTX11);     // ���������� � ���������� ���������

	bool SetControl(short *PLC_Data,short CtrlBits);    // ��������� �� �����������
	bool SetStatus(short *PLC_Data,short StatBits);     // C�������� �� �����������

};
//---------------------------------------------------------------------------
extern PACKAGE TBUSINTX11_Form *BUSINTX11_Form;
//---------------------------------------------------------------------------
#endif
