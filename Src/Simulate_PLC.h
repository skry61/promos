//---------------------------------------------------------------------------

#ifndef Simulate_PLCH
#define Simulate_PLCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Robot.h"
class TMacroView;
//---------------------------------------------------------------------------
class TSimulPLC_Form : public TForm
{
__published:	// IDE-managed Components
	TStaticText *StaticText1;
	TBevel *Bevel1;
	TStaticText *StaticText2;
	TStaticText *StaticText7;
	TTimer *Timer1;
	TGroupBox *GroupBox1;
	TStaticText *StaticText10;
	TEdit *Edit4;
	TStaticText *StaticText11;
	TEdit *Edit5;
	TStaticText *StaticText12;
	TEdit *Edit6;
	TCheckBox *CheckBox2;
	TGroupBox *GroupBox2;
	TStaticText *StaticText3;
	TEdit *Edit1;
	TCheckBox *cbSimulOn;
	TComboBox *cbRegim;
	TLabel *Label1;
	TButton *bJOG;
	TButton *bPusk;
	TButton *Button1;
	TBevel *Bevel2;
	TStaticText *StaticText4;
	TCheckBox *CheckBox1;
	TCheckBox *CheckBox3;
	TCheckBox *CheckBox4;
	TCheckBox *CheckBox5;
	TCheckBox *CheckBox6;
	TCheckBox *CheckBox7;
	TCheckBox *CheckBox8;
	TCheckBox *CheckBox9;
	TCheckBox *CheckBox10;
	TCheckBox *CheckBox11;
	TCheckBox *CheckBox12;
	TEdit *Edit2;
	TLabel *lStatusByte;
	TPanel *Panel1;
	TGroupBox *GroupBox18;
	TEdit *Edit23;
	TStaticText *StaticText64;
	TStaticText *StaticText65;
	TComboBox *ComboBox2;
	TStaticText *StaticText66;
	TGroupBox *GroupBox17;
	TStaticText *StaticText56;
	TEdit *Edit17;
	TStaticText *StaticText57;
	TStaticText *StaticText58;
	TEdit *Edit20;
	TStaticText *StaticText59;
	TStaticText *StaticText60;
	TEdit *Edit21;
	TStaticText *StaticText61;
	TStaticText *StaticText62;
	TEdit *Edit22;
	TStaticText *StaticText63;
	TCheckBox *CheckBox13;
	TGroupBox *GroupBox19;
	TStaticText *StaticText67;
	TStaticText *StaticText70;
	TEdit *Edit24;
	TStaticText *StaticText71;
	TStaticText *StaticText72;
	TEdit *Edit26;
	TStaticText *StaticText73;
	TStaticText *StaticText74;
	TEdit *Edit27;
	TStaticText *StaticText76;
	TStaticText *StaticText77;
	TEdit *Edit29;
	TGroupBox *GroupBox20;
	TLabel *Label11;
	TLabel *Label12;
	TCheckBox *CheckBox14;
	TEdit *Edit31;
	TEdit *Edit32;
	TStaticText *StaticText78;
	TStaticText *StaticText79;
	TGroupBox *GroupBox12;
	TStaticText *StaticText53;
	TEdit *Edit16;
	TStaticText *StaticText55;
	TCheckBox *CheckBox15;
	TCheckBox *CheckBox16;
	TCheckBox *CheckBox17;
	TCheckBox *CheckBox18;
	TCheckBox *CheckBox19;
	TCheckBox *CheckBox20;
	TGroupBox *GroupBox3;
	TEdit *eJOB_Number;
	TStaticText *JOB;
	TCheckBox *CheckBox21;
	TGroupBox *GroupBox4;
	TStaticText *StaticText5;
	TStaticText *StaticText6;
	TComboBox *ComboBox1;
	TComboBox *ComboBox3;
	TLabel *Label2;
	TEdit *Edit3;
	TCheckBox *CheckBox22;
	TCheckBox *CheckBox23;
	TCheckBox *CheckBox24;
	TComboBox *cbProcTypeSymul;
	TLabel *Label3;
	TRadioGroup *rgAVC_Axis;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall bJOGClick(TObject *Sender);
	void __fastcall cbRegimClick(TObject *Sender);
	void __fastcall Edit1KeyPress(TObject *Sender, char &Key);
	void __fastcall bPuskClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall Edit23KeyPress(TObject *Sender, char &Key);
	void __fastcall cbSimulOnClick(TObject *Sender);
private:	// User declarations
	bool StartSimulator;
	TMacroView *SimulLookMacroView;
	bool  Fl_NewData;		// ������ ����� ������ �� ��� � ��������� (� ������ ���������)

public:		// User declarations
	__fastcall TSimulPLC_Form(TComponent* Owner);
	__fastcall ~TSimulPLC_Form();
//---------------------------------------------------------------------------
	short	Pos_H;					// ������� ��������������
	short	Pos_V;					// ������� ������������
	short	Pos_Angle;				// ������� ��������
	TPosStruct PosHVT;				// ��������� ������� �� ���� ����
	short   Error;					// ��� ������

	TLineToPLC LoadBuf[2];  		// ������ - ����������
	TLineToPLC WorkBuf;     		// ������� �����
	TBitControlByte BitControlByte; // ������� ��������� ���������� � ����������
//	TBitStatusByteEx BitStatusByteEx;// ������� ��������� ���������� � ���������� (�������������)
//	TBitStatusByte  BitStatusByte;  // ������� ��������� ���������� �� �����������
	short   CurLine;				// ������� ������
	short	GrafStatus;         	// ��������� ����� ��������
    int Imitator_Count;
//---------------------------------------------------------------------------
	void Init();           			// �������� ���

	void SetBuffer(TLineToPLC *pLoadBuf);
	int	GetWorkBuffer(TLineToPLC *pLoadBuf);
	void DrawControl();

	TBitControlByte GetProsControl();
	TBitStatusByte  GetProsStatusBits();
	DWORD GetProsStatusData();
//	TBitStatusByteEx GetProsStatusDataEx();
	short  GetProsStatus() {return GrafStatus;}
	short  GetErrorCode() {return Error;}
	TPosStruct  GetPosition() {return PosHVT;}
	void SetEdit(){Edit2->Text = IntToHex(*(short*)&BitControlByte,4);}
	void SetEdit(TCheckBox *cb, int v){
		Edit2->Text = IntToHex(*(short*)&BitControlByte,4);
		cb->HelpContext = 1;
		cb->Checked = (v!=0);
		}
	void SetControl(short *v);

	void SetControlBufBusy();	    // 1� ����� �����
	void SetControlPusk();          // ����
	void SetControlStop();          // ����
	void SetControlReset();         // �����
	void SetControlPause();         // �����
	void SetControl_LasrKadr();
	void SetControlPC_Ready();
	void ReSetControlPC_Ready();
	void ClearControlSaveParam();
	void SetControlPC_Err();
	void SetActivity(int RobotIndex,int SysIndex,int Cycle);

	//--------------------------------
	void SetToPausePC(int v) {BitControlByte.ToPausePC = v; SetEdit(CheckBox11,v);}
	int  GetToPausePC() {return BitControlByte.ToPausePC;}

	void SetBufBusy           (int v) {BitControlByte.BufBusy = v; SetEdit(CheckBox1,v); }
	void SetStopWelding       (int v) {BitControlByte.StopWelding = v; SetEdit(CheckBox3,v);}
	void SetDriveReady        (int v) {BitControlByte.DriveReady = v; SetEdit(CheckBox4,v); }
	void SetDataReady 		  (int v) {BitControlByte.DataReady = v; SetEdit(CheckBox5,v);}
	void SetLasrKadr          (int v) {BitControlByte.LasrKadr = v; SetEdit(CheckBox6,v);}
	void SetPusk              (int v) {/*BitControlByte.Pusk = v; */SetEdit(CheckBox7,v);    }
	void SetToPause           (int v) {BitControlByte.ToPause = v; SetEdit(CheckBox8,v); }
	void SetPause             (int v) {BitControlByte.Pause = v; SetEdit(CheckBox9,v);   }
	void SetAlarmOn           (int v) {BitControlByte.AlarmOn = v; SetEdit(CheckBox10,v); }
	void SetSaveParam         (int v) {BitControlByte.SaveParam = v; SetEdit(CheckBox12,v); }
	void SetPC_Ready          (int v) {BitControlByte.PC_Ready = v; SetEdit(CheckBox16,v); }
	void SetWeldReady         (int v) {BitControlByte.WeldReady = v; SetEdit(CheckBox17,v); }
	void SetI_not_0           (int v) {BitControlByte.I_not_0 = v; SetEdit(CheckBox18,v); }
	void SetPC_Err            (int v) {BitControlByte.PC_Err = v; SetEdit(CheckBox19,v); }
	void SetWeld_Err          (int v) {BitControlByte.Weld_Err = v; SetEdit(CheckBox20,v); }

	int  GetBufBusy            () {return BitControlByte.BufBusy;}
	int  GetStopWelding        () {return BitControlByte.StopWelding;}
	int  GetDriveReady         () {return BitControlByte.DriveReady;}
	int  GetDataReady   	   () {return BitControlByte.DataReady;}
	int  GetLasrKadr           () {return BitControlByte.LasrKadr;}
	int  GetPusk               () {return BitControlByte.Pusk;}
	int  GetToPause            () {return BitControlByte.ToPause;}
	int  GetPause              () {return BitControlByte.Pause;}
	int  GetAlarmOn            () {return BitControlByte.AlarmOn;}
	int  GetSaveParam          () {return BitControlByte.SaveParam;}
	int  GetPC_Ready           () {return BitControlByte.PC_Ready;}
	int  GetWeldReady          () {return BitControlByte.WeldReady;}
	int  GetI_not_0            () {return BitControlByte.I_not_0;}
	int  GetPC_Err             () {return BitControlByte.PC_Err;}
	int  GetWeld_Err           () {return BitControlByte.Weld_Err;}
//	WORD GetWordPoint 		   () {return BitStatusByteEx.WordPoint;}
//	WORD GetTiG_MiG    		   () {return BitStatusByteEx.TiG_MiG;}
	double Get_Cur_I();           // �������� ������� ���
	double Get_Cur_B();           // �������� ������� ����������
	double Get_Cur_V();           // �������� ������� �������� ���������
	double Get_Cur_F();           // �������� ������� ������ ����


	//--------------------------------
//		BitControlByte.ToPausePC = 1;
	__property int ToPausePC   = {read=GetToPausePC, write=SetToPausePC, default=0};
	__property int BufBusy     = {read=GetBufBusy, write=SetBufBusy, default=0};
	__property int StopWelding = {read=GetStopWelding, write=SetStopWelding, default=0};
	__property int DriveReady  = {read=GetDriveReady, write=SetDriveReady, default=0};
	__property int DataReady   = {read=GetDataReady, write=SetDataReady, default=0};
	__property int LasrKadr    = {read=GetLasrKadr, write=SetLasrKadr, default=0};
	__property int Pusk        = {read=GetPusk, write=SetPusk, default=0};
	__property int ToPause     = {read=GetToPausePC, write=SetToPausePC, default=0};
	__property int Pause       = {read=GetPause, write=SetPause, default=0};
	__property int AlarmOn     = {read=GetAlarmOn, write=SetAlarmOn, default=0};
	__property int SaveParam   = {read=GetSaveParam, write=SetSaveParam, default=0};

	__property int PC_Ready    = {read=GetPC_Ready, write=SetPC_Ready, default=0};
	__property int WeldReady   = {read=GetWeldReady, write=SetWeldReady, default=0};

	__property int I_not_0     = {read=GetI_not_0, write=SetI_not_0, default=0};
	__property int PC_Err      = {read=GetPC_Err, write=SetPC_Err, default=0};
	__property int Weld_Err    = {read=GetWeld_Err, write=SetWeld_Err, default=0};
/*���
	__property WORD WordPoint  = {read=GetWordPoint, write=SetWordPoint, default=0};
	__property WORD TiG_MiG    = {read=GetTiG_MiG, write=SetTiG_MiG, default=0};
*/
//---------------------------------------------------------------------------
};
//---------------------------------------------------------------------------
extern PACKAGE TSimulPLC_Form *SimulPLC_Form;
extern GetProsStatus();
//---------------------------------------------------------------------------
#endif
