//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("MainScreen.cpp", MainForm);
USEFORM("EditScHead.cpp", EditScaleHead);
USEFORM("Simulate_PLC.cpp", SimulPLC_Form);
USEFORM("Simulate_BUSENTX11.cpp", BUSINTX11_Form);
USEFORM("..\Channal\Option.cpp", OptionForm);
USEFORM("..\Channal\Logs.cpp", ULogs);
USEFORM("PassWord.cpp", PasswordDlg);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TEditScaleHead), &EditScaleHead);
		Application->CreateForm(__classid(TULogs), &ULogs);
		Application->CreateForm(__classid(TOptionForm), &OptionForm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
