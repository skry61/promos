//---------------------------------------------------------------------------

#ifndef Form2H
#define Form2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>

enum {
	IPULSE_T,	// ������������ ��������� ���
	IGROUND_T,	// ������� ��������� ���
	U_T,		// ���������� ������
	U_W,		// ���������� ���������
	V_WELD,		// �������� ������
	V_WIRE,		// wire feed speed
	I_MOT,		// ��� ��������� ����� ������ (mA).
	V_STEP,		// �������� ���� (��/���).
	IM_T,		// ������� �������� ���������� ����(����� � �������)
	IM_W,		// ������� �������� ���� ���������(����� � �������)
	HEAT_INPUT,	// ������� ����� ����� ���/��
	GAS_FLOW,   // �������� ��������� ����
	TEMPERATUR, // �����������
	WIRE_DEPOS_RATE, // �������� ��������� ��������
	PILOT_GAS	// �� ������������.
};

extern TParamVisio ParamsVisio[5];

//---------------------------------------------------------------------------
class TEditScaleHead : public TForm
{
__published:	// IDE-managed Components
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TBitBtn *BitBtn3;
	TEdit *Edit1;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label4;
	TLabel *Label3;
	TLabel *Label5;
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn3Click(TObject *Sender);
private:	// User declarations
	TScaleHead *ScaleHead; 					// �������
public:		// User declarations
	__fastcall TEditScaleHead(TComponent* Owner);
	__fastcall void SetSender(TObject *Sender){ScaleHead = (TScaleHead *)Sender;}	// �������, ����� ��������� ������ ���� �������������� ����������
};
//---------------------------------------------------------------------------
extern PACKAGE TEditScaleHead *EditScaleHead;
//---------------------------------------------------------------------------
#endif
