//---------------------------------------------------------------------------


#pragma hdrstop
//---------------------------------------------------------------------------
#include "Service.h"
#include "IniFiles.hpp"
//---------------------------------------------------------------------------
TStringList* GetFileList(AnsiString Path1,AnsiString Ext)
{
	AnsiString FileName   = ChangeFileExt(Application->ExeName,".sys");
	AnsiString Path;
	TMemIniFile *sys = NULL;
	try {
		sys = new TMemIniFile(FileName);
		Path = sys->ReadString("Common","ProjectPath",          "c:\\Promos");
		delete sys; sys = NULL;
	} catch ( ... ) {
		if(sys)	delete sys;
		Path = "c:\\Promos";
	}
	Path += Path1+ "\\Macros\\";
	TSearchRec search_rec;
	TStringList *S = new TStringList;
	try
	{
		int error_code = FindFirst(Path+"*." + Ext, faAnyFile, search_rec);
		while ( 0 == error_code )
		{
			AnsiString ss = ExtractFileName(search_rec.Name).SubString(1,search_rec.Name.Pos(".")-1);
 //			while (ss.Pos(" "))	{ss.Insert("_",ss.Pos(" ")); ss.Delete(ss.Pos(" "),1);}
			S->Add(ss);
			error_code = FindNext(search_rec);
		}
	}
	__finally
	{
		FindClose(search_rec);
	}
	return S;
}

//------------------------------------------------------------------------------
// ������� �������� c �������� � �������
double _GradToRad(double GrMin)
{
	double GrInt = (int)GrMin;
	double GrFrac = GrMin - GrInt;
	return  GrToRd(GrInt+GrFrac*100/60);
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
