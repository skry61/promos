//---------------------------------------------------------------------------


#pragma hdrstop

#include "ProsessStatusLine.h"

__fastcall TProsessStatusLine::TProsessStatusLine(TPanel* PanelStat,TRobot	* aRobot)
{
	PanelProsessStatus = PanelStat;
	Robot	= aRobot;
}


TDateTime __fastcall TProsessStatusLine::SetParam(AnsiString Prefis, int aLine,int aLayer, int aPos,int aStep, TDateTime aStartTime)
{
	RunTime = Now() - aStartTime;
	StatusString = Prefis + "       ������: "  + StatusString.sprintf("%d", aLine )+
							"       ����: "    + StatusString.sprintf("%d", aLayer )+
							"       �������: " + StatusString.sprintf("%d.%d",aPos/10,abs(aPos)%10) +
							"       ��������: "+ StatusString.sprintf("%d.%d",aStep/10,abs(aStep)%10) +
							"       ����� ��������: "+  RunTime.TimeString();
	PanelProsessStatus->Caption = StatusString;
	return RunTime;
}

void __fastcall TProsessStatusLine::ReSetStatus()
{
	if(Robot->Status == robREADY)
		PanelProsessStatus->Caption = "����� � ������ ������ �������� ������";
	else if(Robot->Status == robERROR)
		PanelProsessStatus->Caption = "������ ������. ��� ������:" + Robot->GetLastError();
	else
		PanelProsessStatus->Caption = "����� �������� ������ � ��������";
}

void __fastcall TProsessStatusLine::ReSetActivity(int aSystemStatus, short Param, AnsiString Text)
{
	if(aSystemStatus == sysINIT)
		PanelProsessStatus->Caption = "��������� ��������� �������";
	else if(aSystemStatus <= sysERROR)
		PanelProsessStatus->Caption = "������ �������. ��� ������:" + Param + Text;
	else if(aSystemStatus == sysEND_PROG)
		PanelProsessStatus->Caption = "������ ���������. ������������ �������� : " +  RunTime.TimeString();
	else if(aSystemStatus == sysPAUSE)
		PanelProsessStatus->Caption = "�����."+StatusString;
//	else
//		PanelProsessStatus->Caption = "������ �������. ��� ������:" + Robot->GetLastError();
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
