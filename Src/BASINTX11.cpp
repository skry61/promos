//---------------------------------------------------------------------------


#pragma hdrstop
#include "Simulate_PLC.h"
#include "Simulate_BUSENTX11.h"
#include "BASINTX11.h"
#include "Option.h"
/*
//---------------------------------------------------------------------------
__fastcall TBUSintX11::TBUSintX11()//:dStart(10000,0,10)								// �����������
{
}
//---------------------------------------------------------------------------
__fastcall TBUSintX11::~TBUSintX11()//:dStart(10000,0,10)								// �����������
{
}
*/
//---------------------------------------------------------------------------
//      �������������� � BUSINTX11
//---------------------------------------------------------------------------
int TBUSintX11::SetData(TLineToBUSINTX11 *pUpLoadBuf)
{
	return 0;
}
//---------------------------------------------------------------------------
int	 TBUSintX11::GetData(TLineFromBUSINTX11 *pDownLoadBuf)
{
	return 0;
}
//---------------------------------------------------------------------------
int TBUSintX11::DownLoadBuffer(TMacroView *MacroView)
{
	return 0;

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
int	TBUSintX11::GetLastError()			// ������ ������ � ������������ ��
{
	int Er = ErrorCode; ErrorCode = 0;
	return Er;
}


//---------------------------------------------------------------------------
bool TBUSintX11::isPusk()              // ����  - �������� ������     �205
{
//	GetStatusData();
//	return BitStatusByte.Pusk;
	return 0;

}
//---------------------------------------------------------------------------
bool TBUSintX11::isStopWelding()              // ����� ���                   �207
{
//	GetStatusData();
//	return BitStatusByte.Pause;
	return 0;

}
//---------------------------------------------------------------------------
bool TBUSintX11::isAlarmOn()            // ������ ���������            M208
{
//	GetStatusData();
//	return BitStatusByte.AlarmOn;
	return 0;

}

void TBUSintX11::Handler() {

	if (Connected()==true) {
 //-----------------------------------------------------------------------------
		try
		{
			SliceCount++;  // ��� ���������� ��������
			CS_get->Enter();
			try
			{
				// ������� �� BUSINTX11
				if(BUSINTX11_Form)
					if(OptionForm->cbPLC_to_SimulX11->Checked)
					{

//						WriteWord(START_IO_ADDR, 0x0101);   // ������� : ����� ������ � BUSINTX11
//						WriteWord(START_IO_ADDR, 0x0101);   // ������� : ����� ������ � BUSINTX11
						ReadWArray(BUS_ADDR + BUS_ADDR_STAT, (short*)&CurLineDataFromBUSINTX11,sizeof(CurLineDataFromBUSINTX11)/2);
//						WriteWord(START_IO_ADDR, 0x0101);   // ������� : ����� ������ � BUSINTX11
						BUSINTX11_Form->SetStatus(CurLineDataFromBUSINTX11);
					}else
						BUSINTX11_Form->GetStatus(&CurLineDataFromBUSINTX11); // ����� ���� ��������� - �������� � ���������
				else
				{
//					WriteWord(START_IO_ADDR, 0x0101);   // ������� : ����� ������ � BUSINTX11
					ReadWArray(BUS_ADDR + BUS_ADDR_STAT, (short*)&CurLineDataFromBUSINTX11,sizeof(CurLineDataFromBUSINTX11)/2);
//					WriteWord(START_IO_ADDR, 0x0101);   // ������� : ����� ������ � BUSINTX11
				}
				// ������� � ����������
				PLCDriver->SetBUSINTX11Buf((short*)&CurLineDataFromBUSINTX11,sizeof(CurLineDataFromBUSINTX11));

				if(BUSINTX11_Form)
					if(OptionForm->cbPLC_to_SimulX11->Checked)
					{
						BUSINTX11_Form->GetControl(&CurLineDataToBUSINTX11);
						WriteWord(BUS_ADDR + BUS_ADDR_CONTROL, *(short*)&CurLineDataToBUSINTX11);   // ������� : ����� ������ � BUSINTX11
//�������� ��������
						if((SliceCount & 0xf) == 0) // ����� 16 ���
						{
							WriteWord(START_IO_ADDR+1, 0x0001);   // ������� : ���� �������� �� ������
							WriteWord(START_IO_ADDR  , 0x0101);   // ������� : ����� ������ � BUSINTX11
                        }
// ��� ��� �� �����						WriteWArray(BUS_ADDR + BUS_ADDR_CONTROL, (short*)&CurLineDataToBUSINTX11,sizeof(TLineToBUSINTX11)/2);
					}else
					{
						// ������� �� �����������
						if(PLCDriver->GetBUSINTX11Buf(&CurLineDataToBUSINTX11,sizeof(TLineToBUSINTX11)))
							BUSINTX11_Form->SetControl(CurLineDataToBUSINTX11); // ����� ���� ��������� - �������� � ���������
					}
				else
				{
					// ������� �� �����������
					if(PLCDriver->GetBUSINTX11Buf(&CurLineDataToBUSINTX11,sizeof(TLineToBUSINTX11)))
					{
						WriteWord(BUS_ADDR + BUS_ADDR_CONTROL, *(short*)&CurLineDataToBUSINTX11);   // ������� : ����� ������ � BUSINTX11
//�������� ��������
						if((SliceCount & 0xf) == 0) // ����� 16 ���
						{
							WriteWord(START_IO_ADDR+1, 0x0001);   // ������� : ���� �������� �� ������
							WriteWord(START_IO_ADDR  , 0x0101);   // ������� : ����� ������ � BUSINTX11
						}
// ��� ��� �� �����						WriteWArray(BUS_ADDR + BUS_ADDR_CONTROL, (short*)&CurLineDataToBUSINTX11,sizeof(TLineToBUSINTX11)/2);
					}
				}
			}
			__finally {CS_get->Leave();}

		}
		catch (int e)
		{
			ErrorCount++;
		}
	}
	Delay(DelayInterval);

}
//---------------------------------------------------------------------------
double TBUSintX11::Get_Cur_I()             // �������� ������� ���
{
	return (double)CurLineDataFromBUSINTX11.I_Current *1000/0x7fff;
}
//---------------------------------------------------------------------------
double TBUSintX11::Get_Cur_B()             // �������� ������� ����������
{
	return (double)CurLineDataFromBUSINTX11.U_Current *100/0x7fff;
}
//---------------------------------------------------------------------------
double TBUSintX11::Get_Cur_V()             // �������� ������� �������� ���������
{
	return (double)CurLineDataFromBUSINTX11.WireSpeed *40/0x7fff;
}

//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

