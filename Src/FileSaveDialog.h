//---------------------------------------------------------------------------

#ifndef FileSaveDialogH
#define FileSaveDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TSaveForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *Edit_FileName;
	TLabel *Label1;
	TEdit *Edit_Comment;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
	AnsiString FullFileName;
	AnsiString FileName;
	AnsiString Ext;
	bool FileTypePRG;

	TStringList **List;
	AnsiString  Path;



public:		// User declarations
	__fastcall TSaveForm(TComponent* Owner);
//	void __fastcall SetFileName(char * FN,char * FFN);
	void __fastcall SetList(TStringList **aList){List = aList;};
	int __fastcall InitData(bool FlMacro,AnsiString aFileName);
	AnsiString __fastcall GetFileName(){return FileName;};
	AnsiString __fastcall GetFullFileName(){return FullFileName;};
	AnsiString __fastcall GetComment(){return Edit_Comment->Text;};
	void __fastcall SetComment(AnsiString Comment){Edit_Comment->Text = Comment;}

};
//---------------------------------------------------------------------------
extern PACKAGE TSaveForm *SaveForm;
//---------------------------------------------------------------------------
#endif
