//---------------------------------------------------------------------------

#ifndef ProsessStatusLineH
#define ProsessStatusLineH
#include <ExtCtrls.hpp>
#include "Robot.h"
enum eNotActiv_Reason
{
	sysINIT,
	sysEDIT,
	sysACTIVATION_WAIT,
	sysToACTIVATED,
	sysACTIVATED,
	sysIN_PROSESS_EDITPROG,
	sysIN_PROSESS_EDITMACRO,
	sysIN_PROSESS_LOOK_GRAPH,
	sysIN_PROSESS_LOOK_CUR_VALUE,
	sysFINISHING_PROG,
	sysEND_PROG,
	sysPAUSE,
	sysSTOP_OPERATOR,
	sysERROR,
	sysLAST

};

class TProsessStatusLine
{
	private:
		TDateTime RunTime;
		TPanel *PanelProsessStatus;
		AnsiString StatusString;// = "                                                                                    ";
		TRobot	* Robot;		// ����� - ����������

	public:
		__fastcall TProsessStatusLine(TPanel* PanelStat,TRobot	* aRobot);
		__fastcall virtual ~TProsessStatusLine(){};
		TDateTime __fastcall SetParam(AnsiString Prefis,int aLine,int aLayer, int aPos,int aStep, TDateTime aRunTime);
		void __fastcall ReSetStatus();
		TDateTime  __fastcall GetRunTime() {return RunTime;};
//		void __fastcall ReSetActivity(int aSystemStatus);
		void __fastcall ReSetActivity(int aSystemStatus, short Param, AnsiString Text);


};
//---------------------------------------------------------------------------
#endif
