//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("MainScreen.cpp", MainForm);
USEFORM("EditScHead.cpp", EditScaleHead);
USEFORM("Simulate_PLC.cpp", SimulPLC_Form);
USEFORM("..\Channal\Option.cpp", OptionForm);
USEFORM("..\Channal\Logs.cpp", ULogs);
USEFORM("PassWord.cpp", PasswordDlg);
USEFORM("FileOpenDialog.cpp", OpenFileForm);
USEFORM("FileSaveDialog.cpp", SaveForm);
//---------------------------------------------------------------------------
const char *NamedMutex= "OneOnly";
HANDLE CheckInstance(const char *Name)
{
	HANDLE Mutex = CreateMutex(NULL, true,Name);
	int er = GetLastError();
	if (er) return 0;
	return Mutex;
}

//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
//-------------  ������ ���������� �������  ---------------------------------
	HANDLE Mutex = CheckInstance(NamedMutex);
	if (!Mutex)
	{
		ShowMessage("��������� ��� ��������!");
		ReleaseMutex(Mutex);
		return 1;
	}
//---------------------------------------------------------------------------
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TEditScaleHead), &EditScaleHead);
		Application->CreateForm(__classid(TOptionForm), &OptionForm);
		Application->CreateForm(__classid(TOpenFileForm), &OpenFileForm);
		Application->CreateForm(__classid(TSaveForm), &SaveForm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
