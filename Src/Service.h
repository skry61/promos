//---------------------------------------------------------------------------

#ifndef ServiceH
#define ServiceH
#include <Math.hpp>
#include <Math.h>

#include <Grids.hpp>
//---------------------------------------------------------------------------
enum eProgrammRegim					// ����� ������ ��������� - ��������������, ������ ���.
{
	EMPTY,                        	// ����� ���������
	EDIT,                        	// ����� ��������������
	ACT,                            // ����� "������"
	RESET,                        	// ����� "�����"
	NO_REGIM                        // ��� ������
};
typedef unsigned long	U32;
typedef unsigned short	U16;
typedef unsigned short	WORD;
typedef unsigned char	U8;
typedef unsigned char	BYTE;


TStringList* GetFileList(AnsiString Path,AnsiString Ext);
extern double _GradToRad(double GrMin);

#define Pi    M_PI

#define RdToGr(Rd)    (180.0/M_PI * (Rd)) /*������� ������ � �������  */
#define GrToRd(Gr)    (M_PI/180.0 * (Gr)) /*������� �������� � �������*/

//---------------------------------------------------------------------------
#endif
