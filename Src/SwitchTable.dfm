object SwitchTableForm: TSwitchTableForm
  Left = 0
  Top = 0
  Caption = #1042#1085#1080#1084#1072#1085#1080#1077' !!!'
  ClientHeight = 235
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 132
    Width = 596
    Height = 72
    Caption = 
      #1042' '#1076#1072#1085#1085#1086#1084' '#1088#1077#1078#1080#1084#1077' '#1087#1077#1088#1077#1082#1083#1102#1095#1077#1085#1080#1077' '#1088#1072#1073#1086#1095#1077#1075#1086' '#1089#1090#1086#1083#1072' '#1076#1083#1103' '#1090#1077#1082#1091#1097#1077#1075#1086' '#1086#1082#1085#1072' '#1085#1077 +
      #1074#1086#1079#1084#1086#1078#1085#1086'. '#1042#1099#1081#1076#1080#1090#1077' '#1080#1079' '#1076#1072#1085#1085#1086#1075#1086' '#1088#1077#1078#1080#1084#1072' '#1080#1083#1080' '#1086#1090#1084#1077#1085#1080#1090#1077' '#1087#1077#1088#1077#1082#1083#1102#1095#1077#1085#1080#1077'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -21
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 104
    Top = 8
    Width = 389
    Height = 36
    Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1085#1072#1103' '#1086#1087#1077#1088#1072#1094#1080#1103' !!!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -32
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 66
    Width = 462
    Height = 24
    Caption = #1054#1087#1077#1088#1072#1090#1086#1088' '#1087#1099#1090#1072#1077#1090#1089#1103' '#1087#1077#1088#1077#1082#1083#1102#1095#1080#1090#1100' '#1088#1072#1073#1086#1095#1080#1081' '#1089#1090#1086#1083'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -21
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 184
    Top = 99
    Width = 190
    Height = 27
    Caption = #1057#1090#1086#1083' 1 => '#1057#1090#1086#1083' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
end
