//---------------------------------------------------------------------------


#pragma hdrstop

#include "SysPar.h"
#include "IniFiles.hpp"

TSysPar *SysPar;

//----------------------------------------------------------------------------
_fastcall TSysPar::TSysPar(
	TEdit * I_StepCorr, TEdit * Start_Up,
	TEdit * fG0_Hor,TEdit * fG1_Hor,TEdit * fG0_Vert,TEdit * fG1_Vert,TEdit * fG0_Table,TEdit * fG1_Table,TEdit * fvStep,TEdit * TableCycle,
	TEdit * AVCsens,
	TEdit * osV_StepCorr,     TEdit * osS_StepCorr,
	TEdit * WireV_StepCorr,

	TEdit * PathText,
	TStaticText * wireMassaCommon,TStaticText * wireMassaShift,	TStaticText * wireMassaCurWeld,
	TStaticText * CommonWorkTime,
	TStaticText * Version,
	//��������
	TEdit * DelFeedWireStart, TEdit * DelFeedAxis,TEdit * DelFeedWireStop,TEdit * DelPowerOff
)
{
	//"[Feed]"
	   EditArray[0] 	 =		efG0_Hor 			= fG0_Hor;
	   EditArray[1] 	 =  	efG1_Hor			= fG1_Hor;
	   EditArray[2] 	 =  	efG0_Vert			= fG0_Vert;
	   EditArray[3] 	 =  	efG1_Vert			= fG1_Vert;
	   EditArray[4] 	 =  	efG0_Table			= fG0_Table;
	   EditArray[5] 	 =  	efG1_Table			= fG1_Table;
	   EditArray[6] 	 =  	efvStep				= fvStep;
	   EditArray[7] 	 =  	eTableCycle	  		= TableCycle;

		//"[AVC]"
	   EditArray[8] 	 =  	eAVCsens			= AVCsens;

		//"[Torch]"
	   EditArray[9] 	 =  	eI_StepCorr			= I_StepCorr;

		//"[Wire]"
	   EditArray[10]      =  	eWireV_StepCorr		= WireV_StepCorr;

		//"[Oscillation]");
	   EditArray[11] 	 =  	eosV_StepCorr		= osV_StepCorr;
	   EditArray[12] 	 =  	eosS_StepCorr		= osS_StepCorr;
//-------------------------------------------------
		//  [Common]
	   EditArray[13] 	 =  	ePathText		  	= PathText;
	   ewireMassaCommon  = wireMassaCommon;
	   ewireMassaShift   = wireMassaShift;
	   ewireMassaCurWeld = wireMassaCurWeld;

	   eCommonWorkTime   = CommonWorkTime;
	   eVersion          = Version;
//-------------------------------------------------
		//  [Delay]
	   EditArray[14] 	 =  	eDelFeedWireStart	= DelFeedWireStart;
	   EditArray[15] 	 =  	eDelFeedAxis		= DelFeedAxis;
	   EditArray[16] 	 =  	eDelFeedWireStop	= DelFeedWireStop;
	   EditArray[17] 	 =  	eDelPowerOff		= DelPowerOff;
		//"[Torch]"
	   EditArray[18] 	 =  	eStart_Up 			= Start_Up;
	   LoadFromFile();
}

//----------------------------------------------------------------------------
int TSysPar::LoadDefaultEdit()
{
	//"[Feed]"
	efG0_Hor->Text 				= "115,0";
	efG1_Hor->Text 				= "15,0";
	efG0_Vert->Text				= "115,0";
	efG1_Vert->Text				= "15,0";
	efG0_Table->Text  			= "12,0";
	efG1_Table->Text  			= "2,0";
	efvStep->Text				= "5,0";
	eTableCycle->Text	  		= "366,0";
	//"[AVC]"
	eAVCsens->Text				= "4,0";
	//"[Torch]"
	eI_StepCorr->Text			= "5,0";
	eStart_Up->Text				= "5,0";
	//"[Wire]"
	eWireV_StepCorr->Text		= "10,0";
	//"[Oscillation]"); ������
	eosV_StepCorr->Text			= "1,0";
	eosS_StepCorr->Text			= "0,5";
//-------------------------------------------------
	//  [Common]
	ewireMassaCommon->Caption   = "0,00";
	ewireMassaShift->Caption    = "0,00";
	ewireMassaCurWeld->Caption  = "0,00";

	eCommonWorkTime->Caption    = "0:00";
	eVersion->Caption           = "2,00";

	//  [Delay]
	eDelFeedWireStart->Text		= "10,0";
	eDelFeedAxis->Text			= "10,0";
	eDelFeedWireStop->Text		= "10,0";
	eDelPowerOff->Text			= "10,0";

	ProjectPath	   				= "c:\\Promos";
	return 1;

}

//----------------------------------------------------------------------------
int	TSysPar::LoadValue()
{
	//"[Feed]"
	SysPar.parfG0_Hor 			=	  efG0_Hor->Text.ToDouble()*10;
	SysPar.parfG0_Vert			=	  efG0_Vert->Text.ToDouble()*10;
	SysPar.parfG1_Hor 			=	  efG1_Hor->Text.ToDouble()*10;
	SysPar.parfG1_Vert			=	  efG1_Vert->Text.ToDouble()*10;
	SysPar.parfG0_Table			=	  efG0_Table->Text.ToDouble()*10;
	SysPar.parfG1_Table			=	  efG1_Table->Text.ToDouble()*10;
	SysPar.parfvStep			=	  efvStep->Text.ToDouble()*10;
	SysPar.parTableCycle	  	=	  eTableCycle->Text.ToDouble()*10;
	//"[AVC]"                         //"[AVC]"
	SysPar.parAVCsens			=	  eAVCsens->Text.ToDouble()*10;
	//"[Torch]"                       //"[Torch]"
	SysPar.parI_StepCorr		=	  eI_StepCorr->Text.ToDouble()*10;
	SysPar.parDelStart_Up		=     eStart_Up->Text.ToDouble()*10;
	//"[Wire]"                        //"[Wire]"
	SysPar.parWireV_StepCorr	= 	  eWireV_StepCorr->Text.ToDouble()*10;

	//"[Oscillation]");               //"[Oscillation]");
	SysPar.parosV_StepCorr		=	  eosV_StepCorr->Text.ToDouble()*10;
	SysPar.parosS_StepCorr		=	  eosS_StepCorr->Text.ToDouble()*10;
	//[Delay]

	SysPar.parDelFeedWireStart	=	 eDelFeedWireStart->Text.ToDouble()*10;  // ����� ������ ���������
	SysPar.parDelFeedAxis	   	=	 eDelFeedAxis->Text.ToDouble()*10;       // ������ ����
	SysPar.parDelFeedWireStop  	=	 eDelFeedWireStop->Text.ToDouble()*10;   // ���� ������ ���������
	SysPar.parDelPowerOff		=	 eDelPowerOff->Text.ToDouble()*10;       // ���������� ���������

	//----------------------------------  --------------------------
	//  [Common]                      //  [Common]
	ProjectPath	   				= 	  ePathText->Text;
	WorkPar.parwireMassaCommon  =	  ewireMassaCommon->Caption.ToDouble();
	WorkPar.parwireMassaShift   =	  ewireMassaShift->Caption.ToDouble();
	WorkPar.parwireMassaCurWeld =	  ewireMassaCurWeld->Caption.ToDouble();

//	WorkPar.parCommonWorkTime   =	  eCommonWorkTime->Caption.ToDouble();

	SysPar.parREADY   = 1;            //  ������� ��� ����������� - ��������� ���������  = 1
	return 1;

}
//----------------------------------------------------------------------------
int TSysPar::LoadDefault()
{
	LoadDefaultEdit();
	LoadValue();
	return 1;
}

//----------------------------------------------------------------------------
int	__fastcall TSysPar::LoadFromFile()
{
	TDateTime T;
	AnsiString FileName   = ChangeFileExt(Application->ExeName,".sys");
	//��������, ���� �� ����
	if (FileExists(FileName))
	{
//		TMemIniFile *sys = new TMemIniFile(FileName);
		TMemIniFile *sys = NULL;
		try {
			sys = new TMemIniFile(FileName);
			memset(&SysPar,0,sizeof(sSys));
			//�������� �������� �� INI-�����
			//"[Feed]"
			efG0_Hor->Text 				= sys->ReadString("Feed","fG0_Hor",         "115,0");
			efG0_Vert->Text				= sys->ReadString("Feed","fG0_Vert",		"115,0");
			efG1_Hor->Text 				= sys->ReadString("Feed","fG1_Hor",         "15,0");
			efG1_Vert->Text				= sys->ReadString("Feed","fG1_Vert",		"15,0");
			efG0_Table->Text 		 	= sys->ReadString("Feed","fG0_Table",       "12,0");
			efG1_Table->Text		 	= sys->ReadString("Feed","fG1_Table",		"2,0");
			efvStep->Text				= sys->ReadString("Feed","fvStep",			"5,0");
			eTableCycle->Text	  		= sys->ReadString("Feed","TableCycle",		"366,0");
			//"[AVC]"
			eAVCsens->Text				= sys->ReadString("AVC","AVCsens",			"4,0");
			//"[Torch]"
			eI_StepCorr->Text			= sys->ReadString("Torch","I_StepCorr",		"5,0");
			eStart_Up->Text  			= sys->ReadString("Torch","Start_Up",		"5,0");
			//"[Wire]"
			eWireV_StepCorr->Text	 	= sys->ReadString("Wire","WireV_StepCorr",	"10,0");
			//"[Oscillation]");
			eosV_StepCorr->Text			= sys->ReadString("Oscillation","osV_StepCorr", "1,0");
			eosS_StepCorr->Text			= sys->ReadString("Oscillation","osS_StepCorr", "0,5");
			//[Delay]
			eDelFeedWireStart->Text	  	= sys->ReadString("Delay","DelFeedWireStart", "10,0");  // ����� ������ ���������
			eDelFeedAxis->Text			= sys->ReadString("Delay","DelFeedAxis", "10,0");       // ������ ����
			eDelFeedWireStop->Text	  	= sys->ReadString("Delay","DelFeedWireStop", "10,0");   // ���� ������ ���������
			eDelPowerOff->Text			= sys->ReadString("Delay","DelPowerOff", "10,0");       // ���������� ���������
	//-------------------------------------------------
			//  [Common]
			ePathText->Text			    = sys->ReadString("Common","ProjectPath",          "c:\\Promos");
			eVersion->Caption           = sys->ReadString("Common","Version",         "2,00");


			FileName   = ChangeFileExt(Application->ExeName,".dat");
			//��������, ���� �� ����
			if (FileExists(FileName))
			{
				delete sys;
				sys = new TMemIniFile(FileName);
				ewireMassaCommon->Caption   = sys->ReadString("Common","wireMassaCommon", "0,00");
				ewireMassaShift->Caption    = "0,00";// ���������� � ������ ����� sys->ReadString("Common","wireMassaShift",  "0,00");
				ewireMassaCurWeld->Caption  = "0,00";// ���������� � ������ �����  sys->ReadString("Common","wireMassaCurWeld","0,00");

//				eCommonWorkTime->Caption    = sys->ReadString("Common","CommonWorkTime",  "0,00");
				T   = sys->ReadDateTime("Common","CommonWorkTime",  0.00);
				SetWorkTime(T);
			}

			delete sys; sys = NULL;
			LoadValue();
			return 0;
		} catch ( ... ) {
			if(sys)	delete sys;
			LoadDefault();
			return 2;
		}
	}
	LoadDefault();
	return 1;
}
//----------------------------------------------------------------------------
int	__fastcall TSysPar::SaveToFile()
{
	AnsiString FileName   = ChangeFileExt(Application->ExeName,".sys");
	//��������� ��������� � INI-����
	TStringList *list = new TStringList;

	list->Add("[Feed]");                                   					// ������
		list->Values["fG0_Hor"]			= efG0_Hor->Text;
		list->Values["fG0_Vert"]		= efG0_Vert->Text;
		list->Values["fG1_Hor"]			= efG1_Hor->Text;
		list->Values["fG1_Vert"]		= efG1_Vert->Text;
		list->Values["fG0_Table"]		= efG0_Table->Text;
		list->Values["fG1_Table"]		= efG1_Table->Text;
		list->Values["fvStep"]			= efvStep->Text;
		list->Values["TableCycle"]		= eTableCycle->Text;

	list->Add("");

	list->Add("[AVC]");
		list->Values["AVCsens"]			= eAVCsens->Text;

	list->Add("");

	list->Add("[Torch]");                                 					// �������
		list->Values["I_StepCorr"]		= eI_StepCorr->Text;
		list->Values["Start_Up"]		= eStart_Up->Text;
	list->Add("");

	list->Add("[Wire]");                                                    // ���������
		list->Values["WireV_StepCorr"]	= eWireV_StepCorr->Text;

	list->Add("");

	list->Add("[Oscillation]");                                             // ����������
		list->Values["osV_StepCorr"]   	= eosV_StepCorr->Text;
		list->Values["osS_StepCorr"]  	= eosS_StepCorr->Text;
	list->Add("[Delay]");                                              	    // ��������
			//[Delay]
		list->Values["DelFeedWireStart"] = eDelFeedWireStart->Text;			// ����� ������ ���������
		list->Values["DelFeedAxis"]		 = eDelFeedAxis->Text;				// ������ ����
		list->Values["DelFeedWireStop"]	 = eDelFeedWireStop->Text;			// ���� ������ ���������
		list->Values["DelPowerOff"]		 = eDelPowerOff->Text;				// ���������� ���������

	list->Add("");

	list->Add("[Common]");                                              	// �����
	//-------------------------------------------------
			//  [Common]
		list->Values["ProjectPath"]	   		= ePathText->Text;

	list->SaveToFile(FileName);
	list->Clear();
	delete list;
	return 0;
}

//----------------------------------------------------------------------------
int	__fastcall TSysPar::SaveToFileDat()
{
	AnsiString FileName   = ChangeFileExt(Application->ExeName,".dat");
	//��������� ��������� � INI-����
	TStringList *list = new TStringList;

	list->Add("[Common]");                                              	// �����
	//-------------------------------------------------
			//  [Common]
		list->Values["wireMassaCommon"]	    = ewireMassaCommon->Caption; // WorkPar.parwireMassaCommon;
		list->Values["wireMassaShift"]      = ewireMassaShift->Caption;  // WorkPar.parwireMassaShift;
		list->Values["wireMassaCurWeld"]    = ewireMassaCurWeld->Caption;// WorkPar.parwireMassaCurWeld;

		list->Values["CommonWorkTime"]	    = eCommonWorkTime->Caption;  // WorkPar.parCommonWorkTime;

	list->SaveToFile(FileName);
	list->Clear();
	delete list;
	return 0;
}

//----------------------------------------------------------------------------
sSys* __fastcall TSysPar::GetSysData()
{
	LoadValue();	// �������� � ���������
	return &SysPar;

}

//----------------------------------------------------------------------------
int	__fastcall TSysPar::SetWorkTime(TDateTime wt)
{
//	short h,m,s,mc;
//	wt.DecodeTime(&h,&m,&s,&mc);
	WorkPar.parCommonWorkTime += wt;
	eCommonWorkTime->Caption    = ((TDateTime)WorkPar.parCommonWorkTime).TimeString();
	return 0;
}
//----------------------------------------------------------------------------
//��������� ������ ����
int	__fastcall TSysPar::SetWorkGas(TDateTime Period,double GasSpeed)
{
	unsigned short h,m,s,mc;
	int v;
	Period.DecodeTime(&h,&m,&s,&mc);
	double wv = s * 1000 + mc;
	wv = wv * GasSpeed/60000;
	WorkPar.Add(wv);

	v = WorkPar.parwireMassaCommon * 100;
	ewireMassaCommon->Caption   = ((double)v)/100;

	v = WorkPar.parwireMassaShift * 100;
	ewireMassaShift->Caption    = ((double)v)/100;

	v = WorkPar.parwireMassaCurWeld * 100;
	ewireMassaCurWeld->Caption  = ((double)v)/100;

	return 0;
}
//----------------------------------------------------------------------------
//������ ������
void	__fastcall TSysPar::InitWelding()
{
	WorkPar.parwireMassaCurWeld = 00;
	ewireMassaCurWeld->Caption  = ((double)WorkPar.parwireMassaCurWeld)/100;
}

#pragma package(smart_init)
