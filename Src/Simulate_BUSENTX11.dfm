object BUSINTX11_Form: TBUSINTX11_Form
  Left = 0
  Top = 0
  Caption = #1057#1080#1084#1091#1083#1103#1090#1086#1088' BUSENTX11'
  ClientHeight = 435
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 124
    Top = 37
    Width = 186
    Height = 20
  end
  object Bevel2: TBevel
    Left = 124
    Top = 61
    Width = 186
    Height = 20
  end
  object Bevel4: TBevel
    Left = 124
    Top = 12
    Width = 34
    Height = 20
  end
  object StaticText1: TStaticText
    Left = 127
    Top = 39
    Width = 124
    Height = 17
    Caption = '00 00 00 00 00 00 00 00 '
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 8
    Top = 38
    Width = 104
    Height = 17
    Caption = #1055#1088#1086#1092#1080#1083#1100' IN(8 '#1073#1072#1081#1090')'
    TabOrder = 1
  end
  object StaticText7: TStaticText
    Left = 8
    Top = 63
    Width = 117
    Height = 17
    Caption = #1055#1088#1086#1092#1080#1083#1100' OUT(12'#1073#1072#1081#1090')'
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 311
    Top = 82
    Width = 212
    Height = 196
    Caption = #1058#1077#1082#1091#1097#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
    TabOrder = 3
    object StaticText10: TStaticText
      Left = 3
      Top = 22
      Width = 22
      Height = 17
      Caption = #1058#1086#1082
      TabOrder = 0
    end
    object I_Current: TEdit
      Left = 84
      Top = 18
      Width = 59
      Height = 21
      TabOrder = 1
      Text = '0,0'
    end
    object StaticText11: TStaticText
      Left = 3
      Top = 45
      Width = 67
      Height = 17
      Caption = #1053#1072#1087#1088#1103#1078#1077#1085#1080#1077
      TabOrder = 2
    end
    object U_Current: TEdit
      Left = 84
      Top = 41
      Width = 59
      Height = 21
      TabOrder = 3
      Text = '0,0'
    end
    object StaticText12: TStaticText
      Left = 3
      Top = 68
      Width = 83
      Height = 17
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1086#1074'.'
      TabOrder = 4
    end
    object Wire_FeedOut: TEdit
      Left = 84
      Top = 64
      Width = 59
      Height = 21
      TabOrder = 5
      Text = '0,0'
    end
    object CheckBox2: TCheckBox
      Left = 148
      Top = 65
      Width = 49
      Height = 17
      Caption = #1040#1074#1090#1086
      TabOrder = 6
    end
    object CheckBox13: TCheckBox
      Left = 149
      Top = 43
      Width = 49
      Height = 17
      Caption = #1040#1074#1090#1086
      TabOrder = 7
    end
    object CheckBox16: TCheckBox
      Left = 149
      Top = 20
      Width = 49
      Height = 17
      Caption = #1040#1074#1090#1086
      TabOrder = 8
    end
    object CURRENT_Flow: TCheckBox
      Tag = 4
      Left = 84
      Top = 91
      Width = 100
      Height = 17
      Caption = 'CURRENT_Flow'
      TabOrder = 9
    end
    object ReadyForWelding: TCheckBox
      Tag = 5
      Left = 84
      Top = 106
      Width = 100
      Height = 17
      Caption = 'ReadyForWelding'
      TabOrder = 10
    end
    object ErrorOn: TCheckBox
      Tag = 6
      Left = 84
      Top = 122
      Width = 100
      Height = 17
      Caption = 'ErrorOn'
      TabOrder = 11
    end
    object MainCurrent: TCheckBox
      Tag = 7
      Left = 84
      Top = 137
      Width = 100
      Height = 17
      Caption = 'MainCurrent'
      TabOrder = 12
    end
    object Active: TCheckBox
      Tag = 8
      Left = 84
      Top = 153
      Width = 100
      Height = 17
      Caption = 'Active'
      TabOrder = 13
    end
    object U_Alarm: TCheckBox
      Tag = 8
      Left = 84
      Top = 166
      Width = 100
      Height = 17
      Caption = 'U_Alarm'
      TabOrder = 14
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 82
    Width = 297
    Height = 196
    Caption = ' '#1057#1080#1084#1091#1083#1103#1090#1086#1088' '
    TabOrder = 4
    object Label1: TLabel
      Left = 14
      Top = 118
      Width = 32
      Height = 13
      Caption = #1056#1077#1078#1080#1084
    end
    object lStatusByte: TLabel
      Left = 88
      Top = 21
      Width = 86
      Height = 13
      Caption = #1057#1083#1086#1074#1086' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
    end
    object StaticText3: TStaticText
      Left = 14
      Top = 164
      Width = 65
      Height = 17
      Caption = #1050#1086#1076' '#1086#1096#1080#1073#1082#1080
      TabOrder = 0
    end
    object ErrorCod: TEdit
      Left = 85
      Top = 160
      Width = 72
      Height = 21
      TabOrder = 1
      Text = '0'
    end
    object cbSimulOn: TCheckBox
      Left = 14
      Top = 20
      Width = 70
      Height = 17
      Caption = #1042#1082#1083#1102#1095#1080#1090#1100
      TabOrder = 2
    end
    object cbRegim: TComboBox
      Left = 14
      Top = 133
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 3
      Text = #1053#1077#1090
      Items.Strings = (
        #1053#1077#1090
        'JOG'
        #1053#1077' '#1075#1086#1090#1086#1074
        #1043#1086#1090#1086#1074
        #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1086#1075#1088#1072#1084#1084#1091
        #1048#1085#1080#1094#1080#1072#1083#1080#1079#1072#1094#1080#1103
        #1043#1086#1090#1086#1074' '#1082' '#1089#1090#1072#1088#1090#1091
        #1057#1090#1072#1088#1090
        #1054#1089#1090#1072#1085#1086#1074' '#1085#1077' '#1087#1072#1091#1079#1091
        #1055#1072#1091#1079#1072
        #1047#1072#1074#1077#1088#1096#1077#1085#1080#1077' '
        #1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1079#1072#1074#1077#1088#1096#1077#1085#1072
        #1054#1096#1080#1073#1082#1072)
    end
    object bJOG: TButton
      Left = 14
      Top = 39
      Width = 75
      Height = 25
      Caption = 'JOG'
      TabOrder = 4
    end
    object bPusk: TButton
      Left = 14
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Pusk'
      TabOrder = 5
    end
    object Button1: TButton
      Left = 14
      Top = 91
      Width = 75
      Height = 25
      Caption = #1057#1090#1086#1087
      TabOrder = 6
    end
    object PUSK_Welding: TCheckBox
      Left = 181
      Top = 13
      Width = 100
      Height = 17
      Caption = 'PUSK_Welding'
      TabOrder = 7
    end
    object Robot_READY: TCheckBox
      Tag = 1
      Left = 181
      Top = 28
      Width = 100
      Height = 17
      Caption = 'Robot_READY'
      TabOrder = 8
    end
    object ShildingGas: TCheckBox
      Tag = 2
      Left = 181
      Top = 44
      Width = 100
      Height = 17
      Caption = 'ShildingGas'
      TabOrder = 9
    end
    object ImpWire: TCheckBox
      Tag = 3
      Left = 181
      Top = 59
      Width = 100
      Height = 17
      Caption = 'ImpWire'
      TabOrder = 10
    end
    object InversImpWire: TCheckBox
      Tag = 4
      Left = 181
      Top = 75
      Width = 100
      Height = 17
      Caption = 'InversImpWire'
      TabOrder = 11
    end
    object PosSearch: TCheckBox
      Tag = 5
      Left = 181
      Top = 90
      Width = 100
      Height = 17
      Caption = 'PosSearch'
      TabOrder = 12
    end
    object ERR_Reset: TCheckBox
      Tag = 6
      Left = 181
      Top = 106
      Width = 100
      Height = 17
      Caption = 'ERR_Reset'
      TabOrder = 13
    end
    object ReleK2: TCheckBox
      Tag = 7
      Left = 181
      Top = 121
      Width = 100
      Height = 17
      Caption = 'ReleK2'
      TabOrder = 14
    end
    object Simul_Weld: TCheckBox
      Tag = 8
      Left = 181
      Top = 137
      Width = 100
      Height = 17
      Caption = 'Simul_Weld'
      TabOrder = 15
    end
    object WeldingMode0: TCheckBox
      Tag = 9
      Left = 181
      Top = 152
      Width = 100
      Height = 17
      Caption = 'WeldingMode0'
      TabOrder = 16
    end
    object WeldingMode1: TCheckBox
      Tag = 10
      Left = 181
      Top = 168
      Width = 100
      Height = 18
      Caption = 'WeldingMode1'
      TabOrder = 17
    end
    object Edit2: TEdit
      Left = 127
      Top = 36
      Width = 47
      Height = 21
      BiDiMode = bdRightToLeft
      Enabled = False
      ParentBiDiMode = False
      TabOrder = 18
      Text = '0000'
    end
  end
  object StaticText4: TStaticText
    Left = 126
    Top = 63
    Width = 184
    Height = 17
    Caption = '00 00 00 00 00 00 00 00 00 00 00 00 '
    TabOrder = 5
  end
  object Panel1: TPanel
    Left = 0
    Top = 281
    Width = 523
    Height = 152
    TabOrder = 6
    object GroupBox19: TGroupBox
      Left = 311
      Top = 3
      Width = 212
      Height = 147
      Caption = ' '#1043#1086#1088#1077#1083#1082#1072' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      object StaticText67: TStaticText
        Left = 187
        Top = 17
        Width = 12
        Height = 20
        Caption = 'A'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object StaticText70: TStaticText
        Left = 24
        Top = 17
        Width = 84
        Height = 20
        Caption = #1058#1086#1082' '#1080#1084#1087#1091#1083#1100#1089#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object MainCURRENTe: TEdit
        Tag = 8
        Left = 117
        Top = 13
        Width = 64
        Height = 24
        Hint = #1055#1091#1083#1100#1089#1080#1088#1091#1102#1097#1080#1081' '#1089#1074#1072#1088#1086#1095#1085#1099#1081' '#1090#1086#1082
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 0
        Text = '0,1'
      end
      object StaticText71: TStaticText
        Left = 188
        Top = 43
        Width = 12
        Height = 20
        Caption = 'A'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object StaticText72: TStaticText
        Left = 188
        Top = 69
        Width = 18
        Height = 20
        Caption = #1084#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object SecondCURRENT: TEdit
        Tag = 9
        Left = 117
        Top = 39
        Width = 64
        Height = 24
        Hint = #1041#1072#1079#1086#1074#1099#1081' '#1089#1074#1072#1088#1086#1095#1085#1099#1081' '#1090#1086#1082
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 1
        Text = '0,1'
      end
      object StaticText73: TStaticText
        Left = 24
        Top = 43
        Width = 79
        Height = 20
        Caption = #1058#1086#1082' '#1073#1072#1079#1086#1074#1099#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object StaticText74: TStaticText
        Left = 24
        Top = 69
        Width = 76
        Height = 20
        Caption = #1044#1083'. '#1080#1084#1087#1091#1083#1089#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object T_Pause: TEdit
        Tag = 11
        Left = 117
        Top = 91
        Width = 64
        Height = 24
        Hint = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1073#1072#1079#1086#1074#1086#1075#1086' '#1089#1074#1072#1088#1086#1095#1085#1086#1075#1086' '#1090#1086#1082#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 3
        Text = '0,1'
      end
      object StaticText76: TStaticText
        Left = 188
        Top = 95
        Width = 18
        Height = 20
        Caption = #1084'c'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
      end
      object StaticText77: TStaticText
        Left = 24
        Top = 95
        Width = 63
        Height = 20
        Caption = #1044#1083'. '#1087#1072#1091#1079#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
      end
      object T_Pulse: TEdit
        Tag = 10
        Left = 117
        Top = 65
        Width = 64
        Height = 24
        Hint = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1087#1091#1083#1100#1089#1080#1088#1091#1102#1097#1077#1075#1086' '#1089#1074#1072#1088#1086#1095#1085#1086#1075#1086' '#1090#1086#1082#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 2
        Text = '0,1'
      end
      object StaticText6: TStaticText
        Left = 24
        Top = 121
        Width = 66
        Height = 20
        Caption = 'JOB '#1053#1086#1084#1077#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
      end
      object JOB_Number: TEdit
        Tag = 11
        Left = 117
        Top = 117
        Width = 64
        Height = 24
        Hint = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1073#1072#1079#1086#1074#1086#1075#1086' '#1089#1074#1072#1088#1086#1095#1085#1086#1075#1086' '#1090#1086#1082#1072
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 13
        Text = '01'
      end
    end
    object GroupBox20: TGroupBox
      Left = 8
      Top = 65
      Width = 297
      Height = 85
      Caption = 'AVC'
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      object Label11: TLabel
        Left = 24
        Top = 16
        Width = 95
        Height = 16
        Caption = #1053#1072#1087#1088'. '#1089#1083#1077#1078#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 24
        Top = 42
        Width = 47
        Height = 16
        Caption = #1055#1086#1076#1089#1082#1086#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object CheckBox14: TCheckBox
        Tag = 1
        Left = 24
        Top = 62
        Width = 124
        Height = 17
        Hint = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1088#1072#1073#1086#1090#1091' '#1040#1056#1053#1044
        Caption = 'AVC '#1056#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Edit31: TEdit
        Tag = 2
        Left = 158
        Top = 13
        Width = 64
        Height = 24
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        TabOrder = 1
        Text = '0,1'
      end
      object Edit32: TEdit
        Tag = 3
        Left = 158
        Top = 39
        Width = 63
        Height = 24
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        TabOrder = 2
        Text = '0,1'
      end
      object StaticText78: TStaticText
        Left = 226
        Top = 42
        Width = 20
        Height = 20
        Caption = #1084#1084
        TabOrder = 3
      end
      object StaticText79: TStaticText
        Left = 226
        Top = 17
        Width = 11
        Height = 20
        Caption = #1042
        TabOrder = 4
      end
    end
    object GroupBox12: TGroupBox
      Left = 8
      Top = 3
      Width = 297
      Height = 63
      Caption = ' '#1055#1088#1086#1074#1086#1083#1086#1082#1072' '
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      object StaticText53: TStaticText
        Left = 228
        Top = 17
        Width = 45
        Height = 20
        Caption = #1089#1084'/'#1084#1080#1085
        TabOrder = 0
      end
      object Wire_FeedIn: TEdit
        Tag = 1
        Left = 158
        Top = 13
        Width = 64
        Height = 24
        BiDiMode = bdRightToLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 1
        Text = '0,1'
      end
      object StaticText55: TStaticText
        Left = 23
        Top = 13
        Width = 124
        Height = 20
        Caption = #1057#1082'-'#1089#1090#1100' '#1087#1086#1076#1072#1095#1080' '#1087#1088#1086#1074'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object CheckBox15: TCheckBox
        Left = 24
        Top = 39
        Width = 133
        Height = 17
        Caption = #1055#1086#1076#1072#1095#1091' '#1088#1072#1079#1088#1077#1096#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  object StaticText8: TStaticText
    Left = 6
    Top = 14
    Width = 77
    Height = 17
    Caption = #1058#1077#1082#1091#1097#1080#1081' '#1082#1072#1076#1088
    TabOrder = 7
  end
  object StaticText9: TStaticText
    Left = 129
    Top = 14
    Width = 10
    Height = 17
    Alignment = taRightJustify
    Caption = '0'
    TabOrder = 8
  end
  object StaticText5: TStaticText
    Left = 197
    Top = 4
    Width = 166
    Height = 20
    Caption = 'EtherNetIP '#1087#1088#1086#1092#1080#1083#1100' '#8470' 0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'System'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
  end
  object Timer1: TTimer
    Left = 480
    Top = 8
  end
end
