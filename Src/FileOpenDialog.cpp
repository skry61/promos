//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FileOpenDialog.h"
#include "IniFiles.hpp"
#include "SysPar.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOpenFileForm *OpenFileForm;
//---------------------------------------------------------------------------
__fastcall TOpenFileForm::TOpenFileForm(TComponent* Owner)
	: TForm(Owner)
{
	FileName="";
}
//---------------------------------------------------------------------------
void __fastcall TOpenFileForm::Button1Click(TObject *Sender)
{
//	Close();
}
//---------------------------------------------------------------------------
int __fastcall TOpenFileForm::InitData(bool FlMacro)
{
	if(FlMacro)
	{
		Caption = "������� ������";
		ValueListEditor1->TitleCaptions->CommaText  = "������,��������";
	}
	else
	{
		Caption = "������� ���������";
		ValueListEditor1->TitleCaptions->CommaText = "���������,��������";
	}

	Path = "\\Program";
	if(FlMacro) Path += "\\Macros";
	Path = SysPar->GetProjectPath() + Path+"\\";

	Ext = FlMacro?"mcr":"prg";

	TSearchRec search_rec;
	FlCanSelected = false;
	ValueListEditor1->Strings->Clear();
	try
	{
		AnsiString FullFileName = Path+"*." + Ext;
		int error_code = FindFirst(FullFileName, faAnyFile, search_rec);
		while (error_code == 0)
		{
			AnsiString ss = ExtractFileName(search_rec.Name).SubString(1,search_rec.Name.Pos(".")-1);
//			if (FileExists(search_rec.Name))
			{
				TMemIniFile *File = NULL;
				try {
					//�������� ����������� �� INI-�����
					File = new TMemIniFile(Path+search_rec.Name);
					//"[Comments]"
					ss = ss + "=" + File->ReadString("Comments","Comment1","����������� ����������");
					ValueListEditor1->Strings->Add(ss);

					delete File; File = NULL;
				} catch ( ... ) {
					if(File)	delete File;
					return 2;
				}
			}//else  		return;// 1;
			error_code = FindNext(search_rec);
		}
	}
	__finally
	{
		FindClose(search_rec);
		FlCanSelected = true;
		ValueListEditor1SelectCell(NULL, ValueListEditor1->Selection.Left, ValueListEditor1->Selection.Top, FlCanSelected);
	}
	return 0;
}

//---------------------------------------------------------------------------

void __fastcall TOpenFileForm::ValueListEditor1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
	if(FlCanSelected == false) return;
	FileName = ValueListEditor1->Keys[ARow];
	Comment	 = ValueListEditor1->Cells[1][ARow];
	FullFileName = Path+FileName+"." + Ext;
}
//---------------------------------------------------------------------------

