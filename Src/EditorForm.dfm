object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 446
  ClientWidth = 866
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 24
    Top = 219
    Width = 665
    Height = 169
    ColCount = 8
    DefaultColWidth = 40
    RowCount = 20
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ParentFont = False
    TabOrder = 0
    OnClick = StringGrid1Click
    OnDrawCell = StringGrid1DrawCell
    OnKeyDown = StringGrid1KeyDown
    ColWidths = (
      40
      323
      40
      40
      40
      40
      40
      70)
  end
  object Memo1: TMemo
    Left = 728
    Top = 110
    Width = 81
    Height = 51
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 777
    Top = 208
    Width = 81
    Height = 33
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 777
    Top = 247
    Width = 81
    Height = 33
    Caption = #1064#1082#1072#1083#1072
    TabOrder = 3
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 777
    Top = 286
    Width = 81
    Height = 21
    TabOrder = 4
    Text = '0'
    OnKeyDown = Edit1KeyDown
  end
  object Edit2: TEdit
    Left = 777
    Top = 313
    Width = 81
    Height = 21
    TabOrder = 5
    Text = '0'
    OnKeyDown = Edit2KeyDown
  end
  object Edit3: TEdit
    Left = 777
    Top = 340
    Width = 81
    Height = 21
    TabOrder = 6
    Text = '0'
    OnKeyDown = Edit3KeyDown
  end
  object Edit4: TEdit
    Left = 777
    Top = 367
    Width = 81
    Height = 21
    TabOrder = 7
    Text = '0'
    OnKeyDown = Edit4KeyDown
  end
  object MainMenu1: TMainMenu
    Left = 88
    Top = 16
    object N1: TMenuItem
      Caption = #1087#1088#1080#1074#1077#1090
      object N3: TMenuItem
        Caption = #1055#1077#1088#1074#1099#1081
      end
    end
    object N2: TMenuItem
      Caption = #1054#1090#1074#1077#1090
      object N4: TMenuItem
        Caption = #1042#1090#1086#1088#1086#1081
      end
    end
  end
end
