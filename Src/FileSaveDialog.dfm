object SaveForm: TSaveForm
  Left = 0
  Top = 0
  Caption = #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077' '#1092#1072#1081#1083#1072
  ClientHeight = 103
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 16
    Width = 58
    Height = 13
    Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072':'
  end
  object Label2: TLabel
    Left = 320
    Top = 16
    Width = 71
    Height = 13
    Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081':'
  end
  object Edit_FileName: TEdit
    Left = 6
    Top = 32
    Width = 299
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = 'Prog1'
  end
  object Edit_Comment: TEdit
    Left = 320
    Top = 32
    Width = 516
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081' '#1085#1077' '#1074#1074#1077#1076#1077#1085
  end
  object Button1: TButton
    Left = 256
    Top = 62
    Width = 128
    Height = 36
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    ModalResult = 1
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 400
    Top = 62
    Width = 128
    Height = 36
    Caption = #1054#1090#1082#1072#1079
    ModalResult = 2
    TabOrder = 3
  end
end
