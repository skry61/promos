//---------------------------------------------------------------------------

#ifndef ServiceClassH
#define ServiceClassH
#include "SyncObjs.hpp"
#include <Buttons.hpp>
typedef unsigned long	U32;
typedef unsigned short	U16;
typedef unsigned short	WORD;
typedef unsigned char	U8;
typedef unsigned char	BYTE;

//����������� / �������������� �������
//------------------------------------------------------------------------------
// ������������ �� HEX � Bin �����
unsigned short  GetByte(char c);
//������������� ������
unsigned short  __fastcall DecodeTextToBin(char * Dest,char * Src, int Len);
//����������� ������
int __fastcall CodeBinToText(char * Dest,char * Src,int Len);
//------------------------------------------------------------------------------
//����������� ������ ModBus
int __fastcall CodeBinToTextModBus(char * Dest,char * Src,int Len);
//������������� ������
bool  __fastcall DecodeTextToBinModBus(char * Dest,char * Src, int Len);
void __fastcall ColorButtonTop(TBitBtn *BitBtn,TColor Color);

//==============================================================================

//------------------------------------------------
// ����� ������� ������ ��� ������ ���� ��� �����
//------------------------------------------------
class TCustomQueue : public TThreadList {

	virtual void DeleteConnector(TList *l){}
public:
	TCustomQueue() : TThreadList() {}
//	~TCustomQueue()  {Clear();}

	//������a �������
	void Clear();
};

//---------------------------------------------------------------------------
class TLogQueue : public TCustomQueue {

protected:
	AnsiString *Str;
	TObject *lAcceptor,*memAcceptor;
	virtual void show(){};
public:
	TLogQueue() : TCustomQueue() {}
	void LogList();
	void LogList(TObject *Acceptor);
	void AddList(AnsiString s);
	void SetAcceptor(TObject *Acceptor){lAcceptor = Acceptor;}
};
//---------------------------------------------------------------------------
class TLogMemoQueue : public TLogQueue {
	virtual void show();
public:
	void Init(TObject *Memo);
	TLogMemoQueue() : TLogQueue() {}
	bool Enable;
};
//extern TLogMemoQueue *LogQueue;

#endif
